package grand.app.moonshop.retrofitutils;

import android.util.Log;

import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;

import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.utils.LanguagesHelper;
import grand.app.moonshop.utils.storage.user.UserHelper;
import grand.app.moonshop.vollyutils.URLS;
import okhttp3.ConnectionPool;
import okhttp3.ConnectionSpec;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.TlsVersion;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import timber.log.Timber;

public class RetrofitConnectionHelper {
    public static Retrofit retrofit = null;
    public static int bufferSize = 256 * 1024;

    private static final String TAG = "RetrofitConnectionHelpe";
    public static Api webService() {
//        if (retrofit == null) {
//        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
//        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient.Builder okHttpClientBuilder = new OkHttpClient.Builder()
                //.addInterceptor(interceptor)
                .connectTimeout(100000, TimeUnit.SECONDS)
                .readTimeout(100000, TimeUnit.SECONDS)
                .writeTimeout(10000, TimeUnit.SECONDS);

//        try {
//            SSLSocketFactory sslSocketFactory = new TLSSocketFactory();
//            okHttpClientBuilder.sslSocketFactory(sslSocketFactory);
//        } catch (KeyManagementException e) {
//            Log.e(TAG, "KeyManagementException: "+e.getMessage() );
//            e.printStackTrace();
//        } catch (NoSuchAlgorithmException e) {
//            Log.e(TAG, "NoSuchAlgorithmException: "+e.getMessage() );
//            e.printStackTrace();
//        }
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
//        okHttpClientBuilder.connectionPool(new ConnectionPool(100,100,TimeUnit.SECONDS));
//        okHttpClientBuilder.addInterceptor(new ConnectivityInterceptor());
        okHttpClientBuilder.addInterceptor(logging);
        okHttpClientBuilder.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request request = chain.request();
                Request.Builder newRequest = request.newBuilder().header("Authorization", "secret-key");
                if (UserHelper.getUserId() != -1) {
                    newRequest.addHeader("jwt", UserHelper.getUserDetails().jwtToken);
                }
                newRequest.addHeader("Connection", "Keep-Alive");
                newRequest.addHeader("version", "1");
                newRequest.addHeader("lang", LanguagesHelper.getCurrentLanguage());
                return chain.proceed(newRequest.build());
            }
        });
        retrofit = new Retrofit.Builder()
                .baseUrl(URLS.BASE_URL)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
//                    .addConverterFactory(MoshiConverterFactory.create(moshi))
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClientBuilder.build())
                .build();
//        }
        return retrofit.create(Api.class);
    }

}
