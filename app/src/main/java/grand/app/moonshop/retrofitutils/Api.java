package grand.app.moonshop.retrofitutils;

import com.google.gson.JsonObject;

import java.util.List;
import java.util.Map;

import io.reactivex.Flowable;
import io.reactivex.Observable;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;
import retrofit2.http.Url;

public interface Api<T> {

    @Multipart
    @POST("{endPoint}")
    Call<JsonObject> upload(@Path(value = "endPoint", encoded = true) String url, @QueryMap Map<String, String> map, @Part MultipartBody.Part file);

    @POST("{endPoint}")
    Call<JsonObject> upload(@Path(value = "endPoint", encoded = true) String url, @QueryMap Map<String, String> map, @Body RequestBody file);

    @POST("{endPoint}")
    Call<JsonObject> upload(@Path(value = "endPoint", encoded = true) String url, @QueryMap Map<String, String> map);

    @POST("endPoint}")
    Observable<JsonObject> uploadRX(@Path(value = "endPoint", encoded = true)  String url, @QueryMap Map<String, String> map, @Part MultipartBody.Part file);

    @Multipart
    @POST
    Call<JsonObject> post(@Url String url, @QueryMap Map<String, String> map, @Part List<MultipartBody.Part> image);

    @POST
    Call<JsonObject> post(@Url String url, @QueryMap Map<String, String> map);
}


