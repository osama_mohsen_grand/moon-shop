package grand.app.moonshop.repository;

import com.android.volley.Request;

import androidx.lifecycle.MutableLiveData;
import grand.app.moonshop.models.base.StatusMsg;
import grand.app.moonshop.models.payment.PaymentGetRequest;
import grand.app.moonshop.models.payment.PaymentResponse;
import grand.app.moonshop.models.payment.UpdatePaymentRequest;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.vollyutils.ConnectionHelper;
import grand.app.moonshop.vollyutils.ConnectionListener;
import grand.app.moonshop.vollyutils.URLS;


public class PaymentRepository extends BaseRepository {
    PaymentResponse paymentResponse = null;
    public PaymentRepository(MutableLiveData<Object> mutableLiveData) {
        super(mutableLiveData);
    }
    public void getPayment() {
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if(!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    paymentResponse = (PaymentResponse) response;
                    if (paymentResponse != null) {
                        setMessage(paymentResponse.status,paymentResponse.msg);
                        if (paymentResponse.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.SUCCESS);
                        }else {
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                            setMessage(paymentResponse.status,paymentResponse.msg);
                        }
                    }
                }
            }
            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
                getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
            }
        }).requestJsonObject(Request.Method.POST, URLS.CREDIT, new PaymentGetRequest(), PaymentResponse.class);
    }

    public void updatePayment(UpdatePaymentRequest paymentRequest) {
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if(!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    StatusMsg statusMsg = (StatusMsg) response;
                    if (statusMsg != null) {
                        setMessage(statusMsg.status,statusMsg.msg);
                        if (statusMsg.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.PAYMENT);
                        }else {
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                            setMessage(paymentResponse.status,statusMsg.msg);
                        }
                    }
                }
            }
            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
                getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
            }
        }).requestJsonObject(Request.Method.POST, URLS.UPDATE_PAYMENT, paymentRequest, StatusMsg.class);
    }

    public PaymentResponse getPaymentResponse() {
        return paymentResponse;
    }
}



