package grand.app.moonshop.repository;

import com.android.volley.Request;

import java.util.ArrayList;

import androidx.lifecycle.MutableLiveData;
import grand.app.moonshop.models.album.AlbumResponse;
import grand.app.moonshop.models.base.StatusMsg;
import grand.app.moonshop.models.base.UserId;
import grand.app.moonshop.models.famous.advertise.FilterShopAdsResponse;
import grand.app.moonshop.models.institution.AddBranchRequest;
import grand.app.moonshop.models.institution.AddServiceRequest;
import grand.app.moonshop.models.institution.EditInstitutionDetailsRequest;
import grand.app.moonshop.models.institution.UpdateServiceRequest;
import grand.app.moonshop.models.notifications.NotificationResponse;
import grand.app.moonshop.models.shop.ShopDetails;
import grand.app.moonshop.models.shop.ShopDetailsResponse;
import grand.app.moonshop.models.user.DescriptionRequest;
import grand.app.moonshop.models.user.login.LoginResponse;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.utils.storage.user.UserHelper;
import grand.app.moonshop.vollyutils.ConnectionHelper;
import grand.app.moonshop.vollyutils.ConnectionListener;
import grand.app.moonshop.vollyutils.URLS;
import grand.app.moonshop.vollyutils.VolleyFileObject;
import timber.log.Timber;

public class InstitutionRepository extends BaseRepository {
    public ShopDetailsResponse shopDetailsResponse = new ShopDetailsResponse();
    public AlbumResponse albumResponse;
    public FilterShopAdsResponse filterShopAdsResponse;
    public StatusMsg statusMsg;
    public InstitutionRepository(MutableLiveData<Object> mutableLiveData) {
        super(mutableLiveData);

    }
    public void getService() {
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if(!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    shopDetailsResponse = (ShopDetailsResponse) response;
                    if (shopDetailsResponse != null) {
                        setMessage(shopDetailsResponse.status,shopDetailsResponse.msg);
                        if (shopDetailsResponse.status == Constants.RESPONSE_SUCCESS) {
                            Timber.e("success");
                            getmMutableLiveData().setValue(Constants.SUCCESS);
                        }else {
                            getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                        }
                    }
                }

            }
            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
                getmMutableLiveData().setValue(Constants.SERVER_ERROR);
            }
        }).requestJsonObject(Request.Method.GET, URLS.INSTITUTION_BRANCHES+"?type="+UserHelper.getUserDetails().type, null, ShopDetailsResponse.class);
    }

    public ShopDetailsResponse getShopDetailsResponse() {
        return shopDetailsResponse;
    }

    public void getAds() {
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if(!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    albumResponse = (AlbumResponse) response;
                    if (albumResponse != null) {
                        setMessage(albumResponse.status,albumResponse.msg);
                        if (albumResponse.status == Constants.RESPONSE_SUCCESS) {
                            Timber.e("success");
                            getmMutableLiveData().setValue(Constants.ADDS);
                        }else {
                            getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                        }
                    }
                }

            }
            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
                getmMutableLiveData().setValue(Constants.SERVER_ERROR);
            }
        }).requestJsonObject(Request.Method.GET, URLS.ADS+"?account_type="+UserHelper.getUserDetails().type, null, AlbumResponse.class);
    }

    public AlbumResponse getAlbumResponse() {
        return albumResponse;
    }

    public void getFamousList(String filter) {
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if(!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    filterShopAdsResponse = (FilterShopAdsResponse) response;
                    if (filterShopAdsResponse != null) {
                        setMessage(filterShopAdsResponse.status,filterShopAdsResponse.msg);
                        if (filterShopAdsResponse.status == Constants.RESPONSE_SUCCESS) {
                            Timber.e("success");
                            getmMutableLiveData().setValue(Constants.FILTER);
                        }else {
                            getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                        }
                    }
                }

            }
            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
                getmMutableLiveData().setValue(Constants.SERVER_ERROR);
            }
        }).requestJsonObject(Request.Method.GET, URLS.ADS+"?account_type="+UserHelper.getUserDetails().type+"&filter="+filter, null, FilterShopAdsResponse.class);
    }

    public void submitFilter(ArrayList<Integer> famous_list) {

        String famous = "";
        if(famous_list.size() > 0) famous = famous_list.get(0)+"";
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if(!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    albumResponse = (AlbumResponse) response;
                    if (albumResponse != null) {
                        setMessage(albumResponse.status,albumResponse.msg);
                        if (albumResponse.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.ADDS);
                        }else {
                            getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                        }
                    }
                }

            }
            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
                getmMutableLiveData().setValue(Constants.SERVER_ERROR);
            }
        }).requestJsonObject(Request.Method.GET, URLS.FILTER_WITH_FAMOUS+"?account_type="+UserHelper.getUserDetails().type+"&famous_id="+famous, null, AlbumResponse.class);
    }


    public void removeAlbumImage(int id) {
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if(!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    statusMsg = (StatusMsg) response;
                    if (statusMsg != null) {
                        setMessage(statusMsg.status,statusMsg.msg);
                        if (statusMsg.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.DELETE);
                        }else {
                            getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                        }
                    }
                }

            }
            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
                getmMutableLiveData().setValue(Constants.SERVER_ERROR);
            }
        }).requestJsonObject(Request.Method.DELETE, URLS.FAMOUS_DELETE_IMAGE +"/"+id,
                null, StatusMsg.class);
    }

    //if you want delete branch send delete_type 1 else if you want  delete service send delete type 2
    public void removeBranch(int id,int delete_type) {
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if(!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    statusMsg = (StatusMsg) response;
                    if (statusMsg != null) {
                        setMessage(statusMsg.status,statusMsg.msg);
                        if (statusMsg.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.DELETE);
                        }else {
                            getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                        }
                    }
                }

            }
            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
                getmMutableLiveData().setValue(Constants.SERVER_ERROR);
            }
        }).requestJsonObject(Request.Method.DELETE, URLS.INSTITUTION_DELETE +"/"+id+"?type="+UserHelper.getUserDetails().type+"&delete_type="+delete_type,
                null, StatusMsg.class);
    }

    public StatusMsg getStatusMsg() {
        return statusMsg;
    }

    public FilterShopAdsResponse getFilterShopAdsResponse() {
        return filterShopAdsResponse;
    }

    public void addBranch(AddBranchRequest addBranchRequest) {
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);

        ArrayList<VolleyFileObject> volleyFileObjects = new ArrayList<VolleyFileObject>();
        volleyFileObjects.add(addBranchRequest.volleyFileObject);


        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if(!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    statusMsg = (StatusMsg) response;
                    if (statusMsg != null) {
                        setMessage(statusMsg.status, statusMsg.msg);
                        if (statusMsg.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.SUCCESS);
                        } else {
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                            setMessage(statusMsg.status, statusMsg.msg);
                        }
                    }
                }

            }
            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
                getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
            }
        }).multiPartConnect(URLS.ADD_BRANCH,addBranchRequest,volleyFileObjects, StatusMsg.class);
    }

    public void editDetails(EditInstitutionDetailsRequest editInstitutionDetailsRequest) {
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if(!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    statusMsg = (StatusMsg) response;
                    if (statusMsg != null) {
                        setMessage(statusMsg.status,statusMsg.msg);
                        if (statusMsg.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.SUCCESS);
                        }else {
                            getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                        }
                    }
                }

            }
            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
                getmMutableLiveData().setValue(Constants.SERVER_ERROR);
            }
        }).requestJsonObject(Request.Method.POST, URLS.INSTITUTION_EDIT , editInstitutionDetailsRequest, StatusMsg.class);
    }

    public void addService(AddServiceRequest addServiceRequest) {
        ArrayList<VolleyFileObject> volleyFileObjects = new ArrayList<VolleyFileObject>();
        volleyFileObjects.add(addServiceRequest.volleyFileObject);

        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if(!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    statusMsg = (StatusMsg) response;
                    if (statusMsg != null) {
                        setMessage(statusMsg.status,statusMsg.msg);
                        if (statusMsg.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.SUCCESS);
                        }else {
                            getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                        }
                    }
                }

            }
            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
                getmMutableLiveData().setValue(Constants.SERVER_ERROR);
            }
        }).multiPartConnect( URLS.INSTITUTION_ADD_SERVICE , addServiceRequest,volleyFileObjects, StatusMsg.class);
    }

    public void updateService(UpdateServiceRequest updateServiceRequest) {

        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        ConnectionHelper connectionHelper = new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if(!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    statusMsg = (StatusMsg) response;
                    if (statusMsg != null) {
                        setMessage(statusMsg.status,statusMsg.msg);
                        if (statusMsg.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.SUCCESS);
                        }else {
                            getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                        }
                    }
                }

            }
            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
                getmMutableLiveData().setValue(Constants.SERVER_ERROR);
            }
        });
        if(updateServiceRequest.volleyFileObject == null){
            connectionHelper.requestJsonObject(Request.Method.POST,  URLS.INSTITUTION_ADD_SERVICE , updateServiceRequest, StatusMsg.class);
        }else{
            ArrayList<VolleyFileObject> volleyFileObjects = new ArrayList<VolleyFileObject>();
            volleyFileObjects.add(updateServiceRequest.volleyFileObject);
            connectionHelper.multiPartConnect( URLS.INSTITUTION_ADD_SERVICE , updateServiceRequest,volleyFileObjects, StatusMsg.class);
        }

    }

    public void updateDescription(DescriptionRequest descriptionRequest) {
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if (!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    statusMsg = (StatusMsg) response;
                    if (statusMsg != null) {
                        setMessage(statusMsg.status, statusMsg.msg);
                        if (statusMsg.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.DESCRIPTION_SUCCESS);
                        } else {
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                            setMessage(statusMsg.status, statusMsg.msg);
                        }
                    }
                }

            }

            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
                getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
            }
        }).requestJsonObject(Request.Method.POST, URLS.INSTITUTION_ADD_SERVICE, descriptionRequest, StatusMsg.class);
    }

}


