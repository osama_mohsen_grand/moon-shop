package grand.app.moonshop.repository;

import com.android.volley.Request;

import androidx.lifecycle.MutableLiveData;
import grand.app.moonshop.models.credit.CreditResponse;
import grand.app.moonshop.models.packagePayment.PackageResponse;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.utils.storage.user.UserHelper;
import grand.app.moonshop.vollyutils.ConnectionHelper;
import grand.app.moonshop.vollyutils.ConnectionListener;
import grand.app.moonshop.vollyutils.URLS;


public class CreditRepository extends BaseRepository {
    CreditResponse creditResponse = null;
    public CreditRepository(MutableLiveData<Object> mutableLiveData) {
        super(mutableLiveData);
        getCredit();
    }

    public void getCredit() {
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if(!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    creditResponse = (CreditResponse) response;
                    if (creditResponse != null) {
                        setMessage(creditResponse.status,creditResponse.msg);
                        if (creditResponse.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.SUCCESS);
                        }else {
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                        }
                    }
                }

            }
            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
                getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
            }
        }).requestJsonObject(Request.Method.GET, URLS.SHOPS_CREDIT_CARD+"?type="+ UserHelper.getUserDetails().type, null, CreditResponse.class);
    }

    public CreditResponse getCreditResponse() {
        return creditResponse;
    }
}



