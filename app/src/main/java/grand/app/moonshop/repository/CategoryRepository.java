package grand.app.moonshop.repository;

import com.android.volley.Request;

import androidx.lifecycle.MutableLiveData;
import grand.app.moonshop.models.category.IdNameRequest;
import grand.app.moonshop.models.home.HomeRequest;
import grand.app.moonshop.models.home.HomeResponse;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.utils.storage.user.UserHelper;
import grand.app.moonshop.vollyutils.ConnectionHelper;
import grand.app.moonshop.vollyutils.ConnectionListener;
import grand.app.moonshop.vollyutils.URLS;
import timber.log.Timber;


public class CategoryRepository extends BaseRepository {
    private HomeResponse homeResponse = null;
    public CategoryRepository(MutableLiveData<Object> mutableLiveData) {
        super(mutableLiveData);
    }
    public void home() {
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                Timber.e("done");
                if(!catchErrorResponse(response)) {
                    homeResponse = (HomeResponse) response;
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    homeResponse = (HomeResponse) response;
                    if (homeResponse != null) {
                        setMessage(homeResponse.status,homeResponse.msg);
                        if (homeResponse.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.SUCCESS);
                        }else {
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                            setMessage(homeResponse.status,homeResponse.msg);
                        }
                    }
                }
            }
            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
                getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
            }
        }).requestJsonObject(Request.Method.GET, URLS.HOME+"?type="+UserHelper.getUserDetails().type+"&flag=1", null, HomeResponse.class);
    }

    public HomeResponse getHomeResponse() {
        return homeResponse;
    }


    public void addOrEdit(IdNameRequest idNameRequest) {
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if(!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    homeResponse = (HomeResponse) response;
                    if (homeResponse != null) {
                        setMessage(homeResponse.status,homeResponse.msg);
                        if (homeResponse.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.SUCCESS);
                        }else {
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                            setMessage(homeResponse.status,homeResponse.msg);
                        }
                    }
                }
            }
            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
                getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
            }
        }).requestJsonObject(Request.Method.POST, URLS.ADD_CATEGORY, idNameRequest, HomeResponse.class);
    }

    public void delete(int id) {
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if(!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    homeResponse = (HomeResponse) response;
                    if (homeResponse != null) {
                        setMessage(homeResponse.status,homeResponse.msg);
                        if (homeResponse.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.DELETE);
                        }else {
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                            setMessage(homeResponse.status,homeResponse.msg);
                        }
                    }
                }
            }
            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
                getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
            }
        }).requestJsonObject(Request.Method.DELETE, URLS.DELETE_CATEGORY+"/"+id+"?type="+UserHelper.getUserDetails().type+"&flag=1", new Object(), HomeResponse.class);
    }
}



