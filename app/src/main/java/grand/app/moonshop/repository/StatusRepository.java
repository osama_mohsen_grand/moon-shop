package grand.app.moonshop.repository;

import com.android.volley.Request;

import java.util.ArrayList;

import androidx.lifecycle.MutableLiveData;
import grand.app.moonshop.models.app.Mutable;
import grand.app.moonshop.models.base.StatusMsg;
import grand.app.moonshop.models.home.HomeResponse;
import grand.app.moonshop.models.status.AddStatusRequest;
import grand.app.moonshop.models.status.StatusResponse;
import grand.app.moonshop.retrofitutils.Upload.ProgressRequestBody;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.utils.storage.user.UserHelper;
import grand.app.moonshop.utils.upload.FileOperations;
import grand.app.moonshop.vollyutils.ConnectionHelper;
import grand.app.moonshop.vollyutils.ConnectionListener;
import grand.app.moonshop.vollyutils.URLS;
import grand.app.moonshop.vollyutils.VolleyFileObject;
import timber.log.Timber;

public class StatusRepository extends BaseRepository {
    private StatusResponse statusResponse = null;
    private StatusMsg statusMsg;
    public StatusRepository(MutableLiveData<Object> mutableLiveData) {
        super(mutableLiveData);
    }
    public void getStatus() {
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if(!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    statusResponse = (StatusResponse) response;
                    if (statusResponse != null) {
                        setMessage(statusResponse.status,statusResponse.msg);
                        if (statusResponse.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.STATUS);
                        }else {
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                            setMessage(statusResponse.status,statusResponse.msg);
                        }
                    }
                }

            }
            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
                getmMutableLiveData().setValue(Constants.SERVER_ERROR);
            }
        }).requestJsonObject(Request.Method.GET, URLS.STATUS_LIST+"?type="+UserHelper.getUserDetails().type, new Object(), StatusResponse.class);
    }

    public void addStatus( ArrayList<VolleyFileObject> volleyFileObjects ,int type , int duration) {
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS_PERCENTAGE);

        AddStatusRequest addStatusRequest = new AddStatusRequest();
        addStatusRequest.setStatus_type(type);
        if(type == 2)
            addStatusRequest.setDuration(String.valueOf(duration));
//
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if(!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS_PERCENTAGE);
                    statusMsg = (StatusMsg) response;
                    if (statusMsg != null) {
                        setMessage(statusMsg.status,statusMsg.msg);
                        if (statusMsg.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.SUCCESS);
                        }else {
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                            setMessage(statusMsg.status,statusMsg.msg);
                        }
                    }
                }

            }
            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
                getmMutableLiveData().setValue(Constants.SERVER_ERROR);
            }
        }).multiPartConnectVideoDialog(URLS.ADD_STATUS,addStatusRequest,volleyFileObjects, StatusMsg.class,new ProgressRequestBody.UploadCallbacks() {
            @Override
            public void onProgressUpdate(int percentage) {
                getmMutableLiveData().setValue(new Mutable(Constants.PROGRESS_PERCENTAGE,percentage));
            }

            @Override
            public void onError() {

            }

            @Override
            public void onFinish() {

            }
        });

//        new ConnectionHelper(new ConnectionListener() {
//            @Override
//            public void onRequestSuccess(Object response) {
//                if(!catchErrorResponse(response)) {
////                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS_PERCENTAGE);
//                    statusMsg = (StatusMsg) response;
//                    if (statusMsg != null) {
//                        setMessage(statusMsg.status,statusMsg.msg);
//                        if (statusMsg.status == Constants.RESPONSE_SUCCESS) {
//                            getmMutableLiveData().setValue(Constants.SUCCESS);
//                        }else {
//                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
//                            setMessage(statusMsg.status,statusMsg.msg);
//                        }
//                    }
//                }
//
//            }
//            @Override
//            public void onRequestError(Object error) {
//                super.onRequestError(error);
//                getmMutableLiveData().setValue(Constants.SERVER_ERROR);
//            }
//        }).multiPartConnect(URLS.ADD_STATUS,addStatusRequest,volleyFileObjects, StatusMsg.class);
    }

    public void delete(int id) {
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if(!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    statusMsg = (StatusMsg) response;
                    if (statusMsg != null) {
                        setMessage(statusMsg.status,statusMsg.msg);
                        if (statusMsg.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.DELETED);
                        }else {
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                            setMessage(statusMsg.status,statusMsg.msg);
                        }
                    }
                }
            }
            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
                getmMutableLiveData().setValue(Constants.SERVER_ERROR);
            }
        }).requestJsonObject(Request.Method.DELETE, URLS.DELETE_STATUS+"/"+id+"?type="+UserHelper.getUserDetails().type, new Object(), HomeResponse.class);
    }

    public StatusResponse getStatusResponse() {
        return statusResponse;
    }

}



