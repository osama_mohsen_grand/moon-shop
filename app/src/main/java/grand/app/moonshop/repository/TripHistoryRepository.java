package grand.app.moonshop.repository;

import com.android.volley.Request;

import androidx.lifecycle.MutableLiveData;
import grand.app.moonshop.models.base.UserId;
import grand.app.moonshop.models.triphistory.TripHistoryResponse;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.utils.storage.user.UserHelper;
import grand.app.moonshop.vollyutils.ConnectionHelper;
import grand.app.moonshop.vollyutils.ConnectionListener;
import grand.app.moonshop.vollyutils.URLS;

public class TripHistoryRepository extends BaseRepository {
    TripHistoryResponse tripHistoryResponse = null;
    UserId userId = null;
    public TripHistoryRepository(MutableLiveData<Object> mutableLiveData) {
        super(mutableLiveData);
        userId = new UserId(UserHelper.getUserId());
        privacy();
    }
    public void privacy() {
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if(!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    tripHistoryResponse = (TripHistoryResponse) response;
                    if (tripHistoryResponse != null) {
                        setMessage(tripHistoryResponse.status,tripHistoryResponse.msg);
                        if (tripHistoryResponse.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.HISTORY);
                        }else {
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                            setMessage(tripHistoryResponse.status,tripHistoryResponse.msg);
                        }
                    }
                }

            }
            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
                getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
            }
        }).requestJsonObject(Request.Method.POST, URLS.TRIP_HISTORY, userId, TripHistoryResponse.class);
    }

    public TripHistoryResponse getTripHistory() {
        return tripHistoryResponse;
    }
}



