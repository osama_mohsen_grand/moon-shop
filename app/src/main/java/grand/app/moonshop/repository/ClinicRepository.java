package grand.app.moonshop.repository;

import com.android.volley.Request;

import java.util.ArrayList;

import androidx.lifecycle.MutableLiveData;
import grand.app.moonshop.models.base.StatusMsg;
import grand.app.moonshop.models.doctor.DoctorDetailsResponse;
import grand.app.moonshop.models.reservation.ReservationRequest;

import grand.app.moonshop.models.reservation.detatils.ReservationDetailsResponse;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.utils.storage.user.UserHelper;
import grand.app.moonshop.vollyutils.ConnectionHelper;
import grand.app.moonshop.vollyutils.ConnectionListener;
import grand.app.moonshop.vollyutils.URLS;

public class ClinicRepository extends BaseRepository {

    StatusMsg statusMsg;
    DoctorDetailsResponse doctorDetailsResponse;;
    public ClinicRepository(MutableLiveData<Object> mutableLiveData) {
        super(mutableLiveData);
    }

    public void doctorDetails(int id, String date) {
        String url = "?id=" + id;
        if (date != null && !date.equals(""))
            url += "&date=" + date;
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);

        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if (!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    doctorDetailsResponse = (DoctorDetailsResponse) response;
                    if (doctorDetailsResponse != null) {
                        setMessage(doctorDetailsResponse.status, doctorDetailsResponse.msg);
                        if (doctorDetailsResponse.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.DOCTOR_DETAILS);
                        } else {
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                            setMessage(doctorDetailsResponse.status, doctorDetailsResponse.msg);
                        }
                    }
                }
            }
        }).requestJsonObject(Request.Method.GET, URLS.CLINIC_DOCTOR_DETAILS + url, null, DoctorDetailsResponse.class);
    }

    //CLINIC_RESERVATION_ORDER

    public void makeReservation(ReservationRequest reservationRequest) {
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);

        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if (!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    statusMsg = (StatusMsg) response;
                    if (statusMsg != null) {
                        setMessage(statusMsg.status, statusMsg.msg);
                        if (statusMsg.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.SUCCESS);
                        } else {
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                            setMessage(statusMsg.status, statusMsg.msg);
                        }
                    }
                }
            }
        }).requestJsonObject(Request.Method.POST, URLS.CLINIC_RESERVATION_ORDER , reservationRequest, StatusMsg.class);
    }

    public DoctorDetailsResponse getDoctorDetailsResponse() {
        return doctorDetailsResponse;
    }
}



