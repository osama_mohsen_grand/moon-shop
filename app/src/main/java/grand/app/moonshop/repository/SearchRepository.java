package grand.app.moonshop.repository;

import com.android.volley.Request;

import java.util.List;

import androidx.lifecycle.MutableLiveData;
import grand.app.moonshop.models.famous.list.Famous;
import grand.app.moonshop.models.famous.list.FamousListResponse;
import grand.app.moonshop.models.famous.search.AlbumSearchResponse;
import grand.app.moonshop.models.famous.search.FamousSearchResponse;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.utils.storage.user.UserHelper;
import grand.app.moonshop.vollyutils.ConnectionHelper;
import grand.app.moonshop.vollyutils.ConnectionListener;
import grand.app.moonshop.vollyutils.URLS;


public class SearchRepository extends BaseRepository {

    AlbumSearchResponse albumSearchResponse = null;
    FamousSearchResponse famousSearchResponse = null;

    public SearchRepository(MutableLiveData<Object> mutableLiveData) {
        super(mutableLiveData);
    }

    public void album(String text) {
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if(!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    albumSearchResponse = (AlbumSearchResponse) response;
                    if (albumSearchResponse != null) {
                        setMessage(albumSearchResponse.status,albumSearchResponse.msg);
                        if (albumSearchResponse.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.SUCCESS);
                        }else {
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                        }
                    }
                }

            }
            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
                getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
            }
        }).requestJsonObject(Request.Method.GET, URLS.FAMOUS_ALBUM_SEARCH +"?name="+ text, null, AlbumSearchResponse.class);
    }

    public void famous(int type,String text) {
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if(!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    famousSearchResponse = (FamousSearchResponse) response;
                    if (famousSearchResponse != null) {
                        setMessage(famousSearchResponse.status,famousSearchResponse.msg);
                        if (famousSearchResponse.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.SUCCESS);
                        }else {
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                        }
                    }
                }

            }
            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
                getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
            }
        }).requestJsonObject(Request.Method.GET, URLS.FAMOUS_SEARCH +"?type="+type+"&name="+ text+"&country_id="+ UserHelper.retrieveKey(Constants.COUNTRY_ID), null, FamousSearchResponse.class);
    }

    public AlbumSearchResponse getAlbumSearchResponse() {
        return albumSearchResponse;
    }

    public FamousSearchResponse getFamousSearchResponse() {
        return famousSearchResponse;
    }
}



