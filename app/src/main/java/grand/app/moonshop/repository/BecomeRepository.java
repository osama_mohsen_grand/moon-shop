package grand.app.moonshop.repository;

import java.util.ArrayList;

import androidx.lifecycle.MutableLiveData;
import grand.app.moonshop.models.base.StatusMsg;
import grand.app.moonshop.models.become.BeShopRequest;
import grand.app.moonshop.models.shop.BeShopResponse;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.vollyutils.ConnectionHelper;
import grand.app.moonshop.vollyutils.ConnectionListener;
import grand.app.moonshop.vollyutils.URLS;
import grand.app.moonshop.vollyutils.VolleyFileObject;


public class BecomeRepository extends BaseRepository {
    private BeShopResponse beShopResponse = null;
    
    public BecomeRepository(MutableLiveData<Object> mutableLiveData) {
        super(mutableLiveData);
    }

    public void beShop(BeShopRequest beShopRequest, ArrayList<VolleyFileObject> volleyFileObjects) {
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if(!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    beShopResponse = (BeShopResponse) response;
                    if (beShopResponse != null) {
                        setMessage(beShopResponse.status,beShopResponse.msg);
                        if (beShopResponse.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.DONE);
                        }else {
                            setMessage(beShopResponse.status,beShopResponse.msg);
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                        }
                    }
                }

            }
            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
                getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
            }
        }).multiPartConnect(URLS.BE_SHOP, beShopRequest,volleyFileObjects, BeShopResponse.class);
    }

    public BeShopResponse getBeShopResponse() {
        return beShopResponse;
    }
}



