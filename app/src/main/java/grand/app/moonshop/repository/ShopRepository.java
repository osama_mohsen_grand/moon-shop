package grand.app.moonshop.repository;

import com.android.volley.Request;

import androidx.lifecycle.MutableLiveData;
import grand.app.moonshop.models.service.ServiceResponse;
import grand.app.moonshop.models.service.ShopServiceMethod;
import grand.app.moonshop.models.shop.ShopSizeColorResponse;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.vollyutils.ConnectionHelper;
import grand.app.moonshop.vollyutils.ConnectionListener;
import grand.app.moonshop.vollyutils.URLS;

public class ShopRepository extends BaseRepository {
    ShopSizeColorResponse shopSizeColorResponse = null;
    public ShopRepository(MutableLiveData<Object> mutableLiveData) {
        super(mutableLiveData);
    }

    public void getSizeColor() {
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if(!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    shopSizeColorResponse = (ShopSizeColorResponse) response;
                    if (shopSizeColorResponse != null) {
                        setMessage(shopSizeColorResponse.status,shopSizeColorResponse.msg);
                        if (shopSizeColorResponse.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.SIZE_COLOR);
                        }else {
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                            setMessage(shopSizeColorResponse.status,shopSizeColorResponse.msg);
                        }
                    }
                }
            }
            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
                getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
            }
        }).requestJsonObject(Request.Method.GET, URLS.SHOP_SIZE_COLOR, null, ShopSizeColorResponse.class);
    }

    public ShopSizeColorResponse getShopSizeColorResponse() {
        return shopSizeColorResponse;
    }
}
