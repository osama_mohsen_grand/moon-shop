package grand.app.moonshop.repository;

import android.content.Intent;

import com.android.volley.Request;

import androidx.lifecycle.MutableLiveData;
import grand.app.moonshop.models.base.StatusMsg;
import grand.app.moonshop.models.user.log.LogcatCrashRequest;
import grand.app.moonshop.models.user.UpdateTokenRequest;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.views.activities.BaseActivity;
import grand.app.moonshop.vollyutils.ConnectionHelper;
import grand.app.moonshop.vollyutils.ConnectionListener;
import grand.app.moonshop.vollyutils.MyApplication;
import grand.app.moonshop.vollyutils.URLS;

import static java.lang.System.exit;


public class FirebaseRepository extends BaseRepository {

    public FirebaseRepository(MutableLiveData<Object> mMutableLiveData) {
        super(mMutableLiveData);
    }

    public void updateToken(String token) {
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if(!catchErrorResponse(response)) {
                    StatusMsg statusMsg = (StatusMsg) response;
                    if (statusMsg.status == Constants.RESPONSE_402) {
                        if (getmMutableLiveData() != null) {
                            getmMutableLiveData().setValue(Constants.PACKAGES);
                            Intent intent = new Intent(MyApplication.getInstance(), BaseActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            MyApplication.getInstance().startActivity(intent);
                        }
                    } else if (getmMutableLiveData() != null)
                        getmMutableLiveData().setValue(Constants.SUCCESS);
                }
            }
            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
            }
        }).requestJsonObject(Request.Method.POST, URLS.FIREBASE, new UpdateTokenRequest(token), StatusMsg.class);
    }

    public void crashLog(String title , String message) {
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                exit(1);
            }
            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
            }
        }).requestJsonObject(Request.Method.POST, URLS.LOGCRASH, new LogcatCrashRequest(title,message), StatusMsg.class);
    }
}



