package grand.app.moonshop.repository;

import com.android.volley.Request;

import androidx.lifecycle.MutableLiveData;
import grand.app.moonshop.models.base.UserId;
import grand.app.moonshop.models.notifications.NotificationResponse;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.utils.storage.user.UserHelper;
import grand.app.moonshop.vollyutils.ConnectionHelper;
import grand.app.moonshop.vollyutils.ConnectionListener;
import grand.app.moonshop.vollyutils.URLS;

public class NotificationRepository extends BaseRepository {
    NotificationResponse notificationResponse = null;
    UserId userId = null;
    public NotificationRepository(MutableLiveData<Object> mutableLiveData) {
        super(mutableLiveData);
        int id = UserHelper.getUserId();
        userId = new UserId(id);
        getService();
    }
    public void getService() {
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if(!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    notificationResponse = (NotificationResponse) response;
                    if (notificationResponse != null) {
                        setMessage(notificationResponse.status,notificationResponse.msg);
                        if (notificationResponse.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.SUCCESS);
                        }else {
                            getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                        }
                    }
                }

            }
            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
                getmMutableLiveData().setValue(Constants.SERVER_ERROR);
            }
        }).requestJsonObject(Request.Method.GET, URLS.NOTIFICATION+"?type="+UserHelper.getUserDetails().type, userId, NotificationResponse.class);
    }


    public NotificationResponse getNotificationResponse() {
        return notificationResponse;
    }
}



