package grand.app.moonshop.repository;

import com.android.volley.Request;

import androidx.lifecycle.MutableLiveData;
import grand.app.moonshop.models.shop.ShopDetailsResponse;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.utils.storage.user.UserHelper;
import grand.app.moonshop.vollyutils.ConnectionHelper;
import grand.app.moonshop.vollyutils.ConnectionListener;
import grand.app.moonshop.vollyutils.URLS;
import timber.log.Timber;

public class GalleryRepository extends BaseRepository {
    public ShopDetailsResponse shopDetailsResponse = null;
    public GalleryRepository(MutableLiveData<Object> mutableLiveData) {
        super(mutableLiveData);

    }

    public void gallery(String FULL_URL) {
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if(!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    shopDetailsResponse = (ShopDetailsResponse) response;
                    if (shopDetailsResponse != null) {
                        setMessage(shopDetailsResponse.status,shopDetailsResponse.msg);
                        if (shopDetailsResponse.status == Constants.RESPONSE_SUCCESS) {
                            Timber.e("success");
                            getmMutableLiveData().setValue(Constants.ADDS);
                        }else {
                            getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                        }
                    }
                }

            }
            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
                getmMutableLiveData().setValue(Constants.SERVER_ERROR);
            }
        }).requestJsonObject(Request.Method.GET, FULL_URL, null, ShopDetailsResponse.class);
    }
}



