package grand.app.moonshop.repository;

import com.android.volley.Request;

import java.util.ArrayList;

import androidx.lifecycle.MutableLiveData;
import grand.app.moonshop.models.base.StatusMsg;
import grand.app.moonshop.models.base.UserId;
import grand.app.moonshop.models.reservation.beauty.AddBeautyServiceRequest;
import grand.app.moonshop.models.reservation.beauty.BeautyOrderDetailsResponse;
import grand.app.moonshop.models.reservation.beauty.BeautyServiceResponse;
import grand.app.moonshop.models.reservation.detatils.ReservationDetailsResponse;
import grand.app.moonshop.models.reservation.doctor.AddDoctorRequest;
import grand.app.moonshop.models.reservation.doctor.DoctorListResponse;
import grand.app.moonshop.models.reservation.doctor.SpecificationResponse;
import grand.app.moonshop.models.reservation.doctor.order.ReservationOrderActionRequest;
import grand.app.moonshop.models.reservation.doctor.order.ReservationOrderResponse;
import grand.app.moonshop.models.reservation.doctor.schedule.AddScheduleRequest;
import grand.app.moonshop.models.reservation.doctor.schedule.DoctorScheduleResponse;
import grand.app.moonshop.models.reservation.doctor.schedule.ScheduleResponse;
import grand.app.moonshop.models.shop.AddShopResponse;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.utils.storage.user.UserHelper;
import grand.app.moonshop.utils.upload.FileOperations;
import grand.app.moonshop.vollyutils.ConnectionHelper;
import grand.app.moonshop.vollyutils.ConnectionListener;
import grand.app.moonshop.vollyutils.URLS;
import grand.app.moonshop.vollyutils.VolleyFileObject;
import timber.log.Timber;

public class ReservationRepository extends BaseRepository {
    DoctorListResponse doctorListResponse = null;
    SpecificationResponse specificationResponse = null;
    DoctorScheduleResponse doctorScheduleResponse = null;
    ScheduleResponse scheduleResponse = null;
    ReservationOrderResponse reservationOrderResponse = null;
    BeautyServiceResponse beautyServiceResponse = null;
    BeautyOrderDetailsResponse beautyOrderDetailsResponse = null;
    StatusMsg statusMsg;
    public ReservationRepository(MutableLiveData<Object> mutableLiveData) {
        super(mutableLiveData);
    }
    public void getDoctorList() {
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if(!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    doctorListResponse = (DoctorListResponse) response;
                    if (doctorListResponse != null) {
                        getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                        setMessage(doctorListResponse.status,doctorListResponse.msg);
                        if (doctorListResponse.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.SUCCESS);
                        }else {
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                        }
                    }
                }

            }
            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
                getmMutableLiveData().setValue(Constants.SERVER_ERROR);
            }
        }).requestJsonObject(Request.Method.GET, URLS.DOCTOR_LIST+"?type="+UserHelper.getUserDetails().type, null, DoctorListResponse.class);
    }



    public void delete(Integer id) {
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if(!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    statusMsg = (StatusMsg) response;
                    if (statusMsg != null) {
                        setMessage(statusMsg.status,statusMsg.msg);
                        if (statusMsg.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.DELETED);
                        }else {
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                        }
                    }
                }
            }
            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
                getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
            }
        }).requestJsonObject(Request.Method.DELETE, URLS.DELETE_DOCTOR+"/"+id, new Object(), StatusMsg.class);
    }


    public void deleteBeauty(Integer id) {
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if(!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    statusMsg = (StatusMsg) response;
                    if (statusMsg != null) {
                        setMessage(statusMsg.status,statusMsg.msg);
                        if (statusMsg.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.DELETED);
                        }else {
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                        }
                    }
                }
            }
            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
                getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
            }
        }).requestJsonObject(Request.Method.DELETE, URLS.DELETE_CATEGORY+"/"+id+"?type="+UserHelper.getUserDetails().type, new Object(), StatusMsg.class);
    }

    public DoctorListResponse getDoctorListResponse() {
        return doctorListResponse;
    }

    public StatusMsg getStatusMsg() {
        return statusMsg;
    }

    public void addDoctor(AddDoctorRequest addDoctorRequest) {
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        ArrayList<VolleyFileObject> volleyFileObjects = new ArrayList<VolleyFileObject>();
        volleyFileObjects.add(addDoctorRequest.volleyFileObject);

        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if (!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    statusMsg = (StatusMsg) response;
                    if (statusMsg != null) {
                        setMessage(statusMsg.status,statusMsg.msg);
                        if (statusMsg.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.SUCCESS);
                        }else {
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                        }
                    }
                }

            }

            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
                getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
            }
        }).multiPartConnect(URLS.ADD_DOCTOR, addDoctorRequest, volleyFileObjects, StatusMsg.class);
    }


    public void getSpecification() {
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if (!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    specificationResponse = (SpecificationResponse) response;
                    if (specificationResponse != null) {
                        setMessage(specificationResponse.status,specificationResponse.msg);
                        if (specificationResponse.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.SPECIFICATION);
                        }else {
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                        }
                    }
                }

            }

            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
                getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
            }
        }).requestJsonObject(Request.Method.GET,URLS.DOCTOR_SPECIFICATION+"?type="+UserHelper.getUserDetails().type, null, SpecificationResponse.class);
    }


    public void getDoctorSchedule(int doctor_id) {
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if (!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    doctorScheduleResponse = (DoctorScheduleResponse) response;
                    if (doctorScheduleResponse != null) {
                        setMessage(doctorScheduleResponse.status,doctorScheduleResponse.msg);
                        if (doctorScheduleResponse.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.SCHEDULE);
                        }else {
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                        }
                    }
                }

            }

            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
                getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
            }
        }).requestJsonObject(Request.Method.GET,URLS.DOCTOR_SCHEDULE+"?type="+UserHelper.getUserDetails().type+"&doctor_id="+doctor_id, null, DoctorScheduleResponse.class);
    }


    public SpecificationResponse getSpecificationResponse() {
        return specificationResponse;
    }

    public DoctorScheduleResponse getDoctorScheduleResponse() {
        return doctorScheduleResponse;
    }

    public void addDoctorSchedule(AddScheduleRequest addScheduleRequest) {
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if (!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    scheduleResponse = (ScheduleResponse) response;
                    if (scheduleResponse != null) {
                        setMessage(scheduleResponse.status,scheduleResponse.msg);
                        if (scheduleResponse.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.ADD);
                        }else {
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                        }
                    }
                }

            }

            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
                getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
            }
        }).requestJsonObject(Request.Method.POST,URLS.DOCTOR_ADD_SCHEDULE, addScheduleRequest, ScheduleResponse.class);
    }

    public ScheduleResponse getScheduleResponse() {
        return scheduleResponse;
    }

    public void getReservations(String status) {
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if (!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    reservationOrderResponse = (ReservationOrderResponse) response;
                    if (reservationOrderResponse != null) {
                        setMessage(reservationOrderResponse.status,reservationOrderResponse.msg);
                        if (reservationOrderResponse.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.SUCCESS);
                        }else {
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                        }
                    }
                }

            }

            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
                getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
            }
        }).requestJsonObject(Request.Method.GET,URLS.DOCTOR_ORDERS+"?type="+UserHelper.getUserDetails().type+"&status="+status, null, ReservationOrderResponse.class);
    }

    public ReservationOrderResponse getReservationOrderResponse() {
        return reservationOrderResponse;
    }

    public void reservationOrderAction(ReservationOrderActionRequest reservationOrderActionRequest) {
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if(!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    statusMsg = (StatusMsg) response;
                    if (statusMsg != null) {
                        setMessage(statusMsg.status,statusMsg.msg);
                        if (statusMsg.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.CONFIRM);
                        }else {
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                        }
                    }
                }
            }
            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
                getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
            }
        }).requestJsonObject(Request.Method.POST, URLS.DOCTOR_ORDER_ACTION, reservationOrderActionRequest, StatusMsg.class);
    }

    public void beautyServices() {
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if(!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    beautyServiceResponse = (BeautyServiceResponse) response;
                    if (beautyServiceResponse != null) {
                        getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                        setMessage(beautyServiceResponse.status,beautyServiceResponse.msg);
                        if (beautyServiceResponse.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.SUCCESS);
                        }else {
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                        }
                    }
                }

            }
            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
                getmMutableLiveData().setValue(Constants.SERVER_ERROR);
            }
        }).requestJsonObject(Request.Method.GET, URLS.HOME+"?type="+UserHelper.getUserDetails().type, null, BeautyServiceResponse.class);
    }

    //ADD_SERVICES_BEAUTY


    public void addBeautyServices(AddBeautyServiceRequest addBeautyServiceRequest, VolleyFileObject volleyFileObject) {
        ArrayList<VolleyFileObject> volleyFileObjects = new ArrayList<>();
        volleyFileObjects.add(volleyFileObject);
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if(!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    statusMsg = (StatusMsg) response;
                    if (statusMsg != null) {
                        setMessage(statusMsg.status,statusMsg.msg);
                        if (statusMsg.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.SUCCESS);
                        }else {
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                        }
                    }
                }
            }
            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
                getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
            }
        }).multiPartConnect(URLS.ADD_SERVICES_BEAUTY, addBeautyServiceRequest,volleyFileObjects, StatusMsg.class);
    }

    public void getOrderDetailsBeauty(int id){
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if(!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    beautyOrderDetailsResponse = (BeautyOrderDetailsResponse) response;
                    if (beautyOrderDetailsResponse != null) {
                        getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                        Timber.e("beautyOrderDetailsResponse:"+beautyOrderDetailsResponse.msg);
                        setMessage(beautyOrderDetailsResponse.status,beautyOrderDetailsResponse.msg);
                        if (beautyOrderDetailsResponse.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.ORDER_DETAILS);
                        }else {
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                        }
                    }
                }

            }
            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
                getmMutableLiveData().setValue(Constants.SERVER_ERROR);
            }
        }).requestJsonObject(Request.Method.GET, URLS.BEAUTY_ORDER_DETAILS+"?order_id="+id+"&type="+UserHelper.getUserDetails().type, null, BeautyOrderDetailsResponse.class);
    }

    ReservationDetailsResponse reservationDetailsResponse;

    public void getReservationDetails(int order_id) {
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if(!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    reservationDetailsResponse = (ReservationDetailsResponse) response;
                    if (reservationDetailsResponse != null) {
                        setMessage(reservationDetailsResponse.status,reservationDetailsResponse.msg);
                        if (reservationDetailsResponse.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.RESERVATION_DETAILS);
                        }else {
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                            if(reservationDetailsResponse.status == Constants.RESPONSE_405)
                                getmMutableLiveData().setValue(Constants.DELETE);
                        }
                    }
                }

            }
        }).requestJsonObject(Request.Method.GET, URLS.CLINIC_RESERVATION_DETAILS+"?order_id="+ order_id+"&type="+ UserHelper.getUserDetails().type, null, ReservationDetailsResponse.class);
    }

    public ReservationDetailsResponse getReservationDetailsResponse() {
        return reservationDetailsResponse;
    }

    public BeautyServiceResponse getBeautyServiceResponse() {
        return beautyServiceResponse;
    }

    public BeautyOrderDetailsResponse getBeautyOrderDetailsResponse() {
        return beautyOrderDetailsResponse;
    }
}



