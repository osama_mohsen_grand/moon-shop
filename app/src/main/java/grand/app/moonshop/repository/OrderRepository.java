package grand.app.moonshop.repository;

import com.android.volley.Request;

import androidx.lifecycle.MutableLiveData;

import grand.app.moonshop.models.OrderConfirmRequest;
import grand.app.moonshop.models.base.StatusMsg;
import grand.app.moonshop.models.order.OrderListResponse;
import grand.app.moonshop.models.order.OrderRequest;
import grand.app.moonshop.models.order.details.OrderDetailsResponse;
import grand.app.moonshop.models.shipping.ShippingResponse;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.utils.storage.user.UserHelper;
import grand.app.moonshop.vollyutils.ConnectionHelper;
import grand.app.moonshop.vollyutils.ConnectionListener;
import grand.app.moonshop.vollyutils.URLS;

public class OrderRepository extends BaseRepository {

    StatusMsg statusMsg = null;
    ShippingResponse shippingResponse = null;
    OrderListResponse orderListResponse = null;
    OrderDetailsResponse orderDetailsResponse = null;

    public OrderRepository(MutableLiveData<Object> mutableLiveData) {
        super(mutableLiveData);
    }


    public void getShipping(){
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if(!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    shippingResponse = (ShippingResponse) response;
                    if (shippingResponse != null) {
                        setMessage(shippingResponse.status,shippingResponse.msg);
                        if (shippingResponse.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.SHIPPING);
                        }else {
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                            if(shippingResponse.status == Constants.RESPONSE_405)
                                getmMutableLiveData().setValue(Constants.DELETE);
                        }
                    }
                }

            }
        }).requestJsonObject(Request.Method.GET, URLS.ORDER_SHIPPING, null, ShippingResponse.class);
    }


    public void confirm(OrderRequest orderRequest){
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if(!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    statusMsg = (StatusMsg) response;
                    if (statusMsg != null) {
                        setMessage(statusMsg.status,statusMsg.msg);
                        if (statusMsg.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.SUCCESS);
                        }else {
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                            if(statusMsg.status == Constants.RESPONSE_405)
                                getmMutableLiveData().setValue(Constants.DELETE);
                        }
                    }
                }

            }
        }).requestJsonObject(Request.Method.POST, URLS.ORDER_CONFIRM, orderRequest, StatusMsg.class);

    }

    public StatusMsg getStatusMsg() {
        return statusMsg;
    }

    public ShippingResponse getShippingResponse() {
        return shippingResponse;
    }

    public void getOrders() {
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if(!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    orderListResponse = (OrderListResponse) response;
                    if (orderListResponse != null) {
                        setMessage(orderListResponse.status,orderListResponse.msg);
                        if (orderListResponse.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.ORDERS);
                        }else {
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                            if(orderListResponse.status == Constants.RESPONSE_405)
                                getmMutableLiveData().setValue(Constants.DELETE);
                        }
                    }
                }

            }
        }).requestJsonObject(Request.Method.GET, URLS.ORDER_LIST+"?type="+ UserHelper.getUserDetails().type, null, OrderListResponse.class);
    }


    public void getOrderDetails(int order_id) {
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if(!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    orderDetailsResponse = (OrderDetailsResponse) response;
                    if (orderDetailsResponse != null) {
                        setMessage(orderDetailsResponse.status,orderDetailsResponse.msg);
                        if (orderDetailsResponse.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.ORDER_DETAILS);
                        }else {
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                            if(orderDetailsResponse.status == Constants.RESPONSE_405)
                                getmMutableLiveData().setValue(Constants.DELETE);
                        }
                    }
                }

            }
        }).requestJsonObject(Request.Method.GET, URLS.ORDER_DETAILS+"?order_id="+ order_id+"&type="+ UserHelper.getUserDetails().type, null, OrderDetailsResponse.class);
    }


    public void confirmOrder(OrderConfirmRequest orderConfirmRequest) {
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if(!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    statusMsg = (StatusMsg) response;
                    if (statusMsg != null) {
                        setMessage(statusMsg.status,statusMsg.msg);
                        if (statusMsg.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.ORDER_SUBMIT);
                        }else {
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                            setMessage(statusMsg.status,statusMsg.msg);
                        }
                    }
                }

            }
        }).requestJsonObject(Request.Method.POST, URLS.ORDER_CONFIRM, orderConfirmRequest, StatusMsg.class);
    }


    public OrderDetailsResponse getOrderDetailsResponse() {
        return orderDetailsResponse;
    }

    public OrderListResponse getOrderListResponse() {
        return orderListResponse;
    }
}



