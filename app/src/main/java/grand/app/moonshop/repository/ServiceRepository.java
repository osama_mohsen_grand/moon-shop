package grand.app.moonshop.repository;

import com.android.volley.Request;

import androidx.lifecycle.MutableLiveData;

import grand.app.moonshop.models.accounttype.AcountTypeResponse;
import grand.app.moonshop.models.service.ServiceResponse;
import grand.app.moonshop.models.service.ShopServiceMethod;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.utils.storage.user.UserHelper;
import grand.app.moonshop.vollyutils.ConnectionHelper;
import grand.app.moonshop.vollyutils.ConnectionListener;
import grand.app.moonshop.vollyutils.URLS;


public class ServiceRepository extends BaseRepository {
    ServiceResponse serviceResponse = null;
    AcountTypeResponse acountTypeResponse=null;

    public ServiceRepository(MutableLiveData<Object> mutableLiveData) {
        super(mutableLiveData);
    }
    public void getServices(int type_id,int flag,int type) {

        ShopServiceMethod shopServiceMethod = new ShopServiceMethod(type_id);
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if(!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    serviceResponse = (ServiceResponse) response;
                    if (serviceResponse != null) {
                        setMessage(serviceResponse.status,serviceResponse.msg);
                        if (serviceResponse.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.SUCCESS);
                        }else {
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                            setMessage(serviceResponse.status,serviceResponse.msg);
                        }
                    }
                }
            }
            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
                getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
            }
        }).requestJsonObject(Request.Method.GET, URLS.SERVICES+"?account_type="+type+"&type="+type+"&flag="+flag, shopServiceMethod, ServiceResponse.class);
    }


    public void setService(int type_id) {
        ShopServiceMethod shopServiceMethod = new ShopServiceMethod(type_id);
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if(!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    serviceResponse = (ServiceResponse) response;
                    if (serviceResponse != null) {
                        setMessage(serviceResponse.status,serviceResponse.msg);
                        if (serviceResponse.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.SUCCESS);
                        }else {
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                            setMessage(serviceResponse.status,serviceResponse.msg);
                        }
                    }
                }
            }
            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
                getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
            }
        }).requestJsonObject(Request.Method.GET, URLS.SERVICES+"?account_type="+shopServiceMethod.account_type+"&type="+shopServiceMethod.type, shopServiceMethod, ServiceResponse.class);
    }

    public void getAccountTypes() {
        String endPoint = URLS.ACCOUNT_TYPES+ UserHelper.retrieveKey(Constants.COUNTRY_ID);
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if (!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    acountTypeResponse = (AcountTypeResponse) response;
                    if (acountTypeResponse != null) {
                        if (acountTypeResponse.status == Constants.RESPONSE_SUCCESS) {
                            UserHelper.saveAccountTypes(acountTypeResponse);
                            getmMutableLiveData().setValue(Constants.SUCCESS);
                        }
                    }
                }
            }
            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
                getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
            }
        }).requestJsonObject(Request.Method.GET, endPoint,null,  AcountTypeResponse.class);
    }

    public ServiceResponse getServiceResponse() {
        return serviceResponse;
    }

    public AcountTypeResponse getAcountTypeResponse() {
        return acountTypeResponse;
    }
}



