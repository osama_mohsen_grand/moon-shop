package grand.app.moonshop.repository;

import com.android.volley.Request;

import androidx.lifecycle.MutableLiveData;
import grand.app.moonshop.models.base.StatusMsg;
import grand.app.moonshop.models.packagePayment.AddPackageRequest;
import grand.app.moonshop.models.packagePayment.PackageResponse;
import grand.app.moonshop.models.user.profile.User;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.utils.storage.user.UserHelper;
import grand.app.moonshop.vollyutils.ConnectionHelper;
import grand.app.moonshop.vollyutils.ConnectionListener;
import grand.app.moonshop.vollyutils.URLS;


public class PackageRepository extends BaseRepository {
    PackageResponse packageResponse = null;
    StatusMsg statusMsg = null;
    public PackageRepository(MutableLiveData<Object> mutableLiveData) {
        super(mutableLiveData);
        
    }
    public void getPackages() {
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if(!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    packageResponse = (PackageResponse) response;
                    if (packageResponse != null) {
                        setMessage(packageResponse.status,packageResponse.msg);
                        if (packageResponse.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.PACKAGES);
                        }else {
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                        }
                    }
                }

            }
            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
                getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
            }
        }).requestJsonObject(Request.Method.GET, URLS.PACKAGES+"?type="+UserHelper.getUserDetails().type, null, PackageResponse.class);
    }


    public void addPackage(AddPackageRequest addPackageRequest) {
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if(!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    statusMsg = (StatusMsg) response;
                    if (statusMsg != null) {
                        setMessage(statusMsg.status,statusMsg.msg);
                        if (statusMsg.status == Constants.RESPONSE_SUCCESS) {
                            User user = UserHelper.getUserDetails();
                            user.packageId = addPackageRequest.package_id;
                            UserHelper.saveUserDetails(user);
                            getmMutableLiveData().setValue(Constants.ADD);
                        }else {
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                        }
                    }
                }

            }
            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
                getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
            }
        }).requestJsonObject(Request.Method.POST, URLS.ADD_PACKAGE, addPackageRequest, StatusMsg.class);
    }




    public PackageResponse getPackageResponse() {
        return packageResponse;
    }
}



