package grand.app.moonshop.repository;

import android.util.Log;

import com.android.volley.Request;

import java.util.ArrayList;

import androidx.lifecycle.MutableLiveData;

import grand.app.moonshop.models.base.StatusMsg;
import grand.app.moonshop.models.checknickresponse.CheckNickResponse;
import grand.app.moonshop.models.shop.AddShopResponse;
import grand.app.moonshop.models.user.DescriptionRequest;
import grand.app.moonshop.models.user.changepassword.ChangePasswordRequest;
import grand.app.moonshop.models.user.register.RegisterShopRequest;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.utils.storage.user.UserHelper;
import grand.app.moonshop.utils.upload.FileOperations;
import grand.app.moonshop.vollyutils.ConnectionHelper;
import grand.app.moonshop.vollyutils.ConnectionListener;
import grand.app.moonshop.vollyutils.URLS;
import grand.app.moonshop.vollyutils.VolleyFileObject;
import timber.log.Timber;

public class RegisterRepository extends BaseRepository {
    StatusMsg statusMsg = null;
    AddShopResponse addShopResponse = null;
    CheckNickResponse checkNickResponse =null;


    public RegisterRepository(MutableLiveData<Object> mutableLiveData) {
        super(mutableLiveData);
    }

    private static final String TAG = "RegisterRepository";
    public void registerUser(RegisterShopRequest registerShopRequest, ArrayList<String> imagesKeys, ArrayList<String> imagesPath) {
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);

        ArrayList<VolleyFileObject> volleyFileObjects = new ArrayList<VolleyFileObject>();


        for (int i = 0; i < imagesKeys.size(); i++) {
            Timber.e("key:" + imagesKeys.get(i) + ",value:" + imagesPath.get(i));
            volleyFileObjects.add(FileOperations.getVolleyFileObject(imagesKeys.get(i), imagesPath.get(i), Constants.FILE_TYPE_IMAGE));
        }


        String endPoint = URLS.REGISTER_SHOP;
        if (registerShopRequest.getType().equals(Constants.TYPE_FAMOUS_PEOPLE) || registerShopRequest.getType().equals(Constants.TYPE_PHOTOGRAPHER)) {
            endPoint = URLS.REGISTER;
            if(volleyFileObjects.size() > 0){
                volleyFileObjects.get(0).setParamName(Constants.IMAGE);//main user photo is image
            }
        }

        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if (!catchErrorResponse(response)) {
                    Log.d(TAG,"response"+response.toString());
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    addShopResponse = (AddShopResponse) response;
                    if (addShopResponse != null) {
                        setMessage(addShopResponse.status, addShopResponse.msg);
                        if (addShopResponse.status == Constants.RESPONSE_SUCCESS) {
                            UserHelper.saveUserDetails(addShopResponse.data);
                            if (addShopResponse.data.packageId == 0)
                                getmMutableLiveData().setValue(Constants.PACKAGES);
                            else
                                getmMutableLiveData().setValue(Constants.HOME);

//                            getmMutableLiveData().setValue(Constants.REGISTRATION);
                        } else {
                            if (addShopResponse.status == Constants.RESPONSE_401 && addShopResponse.suggestions != null) {
                                getmMutableLiveData().setValue(Constants.SUGGESTIONS);
                            } else
                                getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                        }
                    }
                }

            }

            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
                getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
            }
        }).multiPartConnect(endPoint, registerShopRequest, volleyFileObjects, AddShopResponse.class);
    }


    public void checkNickname(RegisterShopRequest registerShopRequest) {
        String endPoint = URLS.CHECK_NICK_NAME;

        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);

        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if (!catchErrorResponse(response)) {

                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);

                       checkNickResponse = (CheckNickResponse) response;
                    if (checkNickResponse != null) {
                        setMessage(checkNickResponse.status, checkNickResponse.msg);
                        if (checkNickResponse.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.REGISTRATION2);
                        } else {
                            addShopResponse = new AddShopResponse();
                            addShopResponse.suggestions = checkNickResponse.suggestions;
                            if (checkNickResponse.status == Constants.RESPONSE_406 &&  checkNickResponse.suggestions!= null) {
                                getmMutableLiveData().setValue(Constants.SUGGESTIONS);
                            } else
                                getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                        }
                    }
                }

            }

            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
                getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
            }
        }).requestJsonObject(Request.Method.POST, endPoint, registerShopRequest, CheckNickResponse.class);
    }





    public AddShopResponse getAddShopResponse() {
        return addShopResponse;
    }

    public StatusMsg getStatus() {
        return statusMsg;
    }
}



