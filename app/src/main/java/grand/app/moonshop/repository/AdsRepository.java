package grand.app.moonshop.repository;

import android.util.Log;

import com.android.volley.Request;

import java.util.ArrayList;
import java.util.List;

import androidx.lifecycle.MutableLiveData;
import grand.app.moonshop.models.ads.AdsDetailsResponse;
import grand.app.moonshop.models.ads.AdsResponse;
import grand.app.moonshop.models.adsCompanyFilter.AdsCompanyFilterResponse;
import grand.app.moonshop.models.app.AppMoon;
import grand.app.moonshop.models.base.StatusMsg;
import grand.app.moonshop.models.famous.advertise.FilterShopAdsResponse;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.utils.storage.user.UserHelper;
import grand.app.moonshop.viewmodels.ads.AddAdsCompanyRequest;
import grand.app.moonshop.vollyutils.ConnectionHelper;
import grand.app.moonshop.vollyutils.ConnectionListener;
import grand.app.moonshop.vollyutils.URLS;
import grand.app.moonshop.vollyutils.VolleyFileObject;
import timber.log.Timber;


public class AdsRepository extends BaseRepository {
    AdsResponse adsResponse;
    FilterShopAdsResponse filterShopAdsResponse;
    AdsCompanyFilterResponse adsCompanyFilterResponse;
    StatusMsg statusMsg;
    AdsDetailsResponse adsDetailsResponse;

    public AdsRepository(MutableLiveData<Object> mutableLiveData) {
        super(mutableLiveData);
    }

    public void getAds(String tab) {
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if (!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    adsResponse = (AdsResponse) response;
                    if (adsResponse != null) {
                        setMessage(adsResponse.status, adsResponse.msg);
                        if (adsResponse.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.SUCCESS);
                        } else {
                            getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                        }
                    }
                }
            }

            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
                getmMutableLiveData().setValue(Constants.SERVER_ERROR);
            }
        }).requestJsonObject(Request.Method.GET, URLS.FAMOUS_ALBUMS + "?tab=" + tab+"&account_type="+ AppMoon.getUserType(), null, AdsResponse.class);
    }

    public void getAdsCompanyFilter(int service_id) {
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);

        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if(!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    adsCompanyFilterResponse = (AdsCompanyFilterResponse) response;
                    if (adsCompanyFilterResponse != null) {
                        setMessage(adsCompanyFilterResponse.status,adsCompanyFilterResponse.msg);
                        if (adsCompanyFilterResponse.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.SUCCESS);
                        }else {
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                        }
                    }
                }
            }
        }).requestJsonObject(Request.Method.GET, URLS.ADS_FILTER+"?service_id="+service_id, null, AdsCompanyFilterResponse.class);
    }



    public void getAdsDynamicUrl(String url) {
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if (!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    adsResponse = (AdsResponse) response;
                    if (adsResponse != null) {
                        setMessage(adsResponse.status, adsResponse.msg);
                        if (adsResponse.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.SUCCESS);
                        } else {
                            getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                        }
                    }
                }
            }

            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
                getmMutableLiveData().setValue(Constants.SERVER_ERROR);
            }
        }).requestJsonObject(Request.Method.GET, url, null, AdsResponse.class);
    }

    public void getShopAds(String url) {
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if (!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    filterShopAdsResponse = (FilterShopAdsResponse) response;
                    Timber.e("filter:"+filterShopAdsResponse.data.size());
                    if (filterShopAdsResponse != null) {
                        setMessage(filterShopAdsResponse.status, filterShopAdsResponse.msg);
                        if (filterShopAdsResponse.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.FILTER);
                        } else {
                            getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                        }
                    }
                }
            }

            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
                getmMutableLiveData().setValue(Constants.SERVER_ERROR);
            }
        }).requestJsonObject(Request.Method.GET, URLS.FAMOUS_FILTER_SHOP_ADS+ url, null, FilterShopAdsResponse.class);
    }



    public static String convertArrayToStringMethod(ArrayList<Integer> data) {

        StringBuilder stringBuilder = new StringBuilder();
        if(data.size() > 0) {
            stringBuilder.append(data.get(0));
            for (int i = 1; i < data.size(); i++) {
                stringBuilder.append(","+data.get(i));

            }
        }
        Timber.e("shops_id:"+stringBuilder.toString());
        return stringBuilder.toString();

    }

    public void getShopAFilter(ArrayList<Integer> ids,int famous_id) {
        String result = convertArrayToStringMethod(ids);
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if (!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    adsResponse = (AdsResponse) response;
                    if (adsResponse != null) {
                        setMessage(adsResponse.status, adsResponse.msg);
                        if (adsResponse.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.SUCCESS);
                        } else {
                            getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                        }
                    }
                }
            }

            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
                getmMutableLiveData().setValue(Constants.SERVER_ERROR);
            }
        }).requestJsonObject(Request.Method.GET, URLS.FAMOUS_FILTER + "?shop_id=" + result+"&account_type="+AppMoon.getUserType()+"&famous_id="+famous_id, null, AdsResponse.class);
    }

    public AdsCompanyFilterResponse getAdsCompanyFilterResponse() {
        return adsCompanyFilterResponse;
    }

    public AdsResponse getAdsResponse() {
        return adsResponse;
    }

    public FilterShopAdsResponse getFilterShopAdsResponse() {
        return filterShopAdsResponse;
    }

    public void addAds(AddAdsCompanyRequest addAdsRequest, List<VolleyFileObject> volleyFileObjects) {
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if(!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    statusMsg = (StatusMsg) response;
                    if (statusMsg != null) {
                        setMessage(statusMsg.status,statusMsg.msg);
                        if (statusMsg.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.SUCCESS);
                        }else {
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                        }
                    }
                }

            }
            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
                getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
            }
        }).multiPartConnect(URLS.ADD_ADV,addAdsRequest,volleyFileObjects, StatusMsg.class);
    }

    public void updateAds(AddAdsCompanyRequest addAdsRequest, List<VolleyFileObject> volleyFileObjects) {
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        if(volleyFileObjects != null && volleyFileObjects.size() > 0){
            new ConnectionHelper(new ConnectionListener() {
                @Override
                public void onRequestSuccess(Object response) {
                    if(!catchErrorResponse(response)) {
                        getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                        statusMsg = (StatusMsg) response;
                        if (statusMsg != null) {
                            setMessage(statusMsg.status,statusMsg.msg);
                            if (statusMsg.status == Constants.RESPONSE_SUCCESS) {
                                getmMutableLiveData().setValue(Constants.SUCCESS);
                            }else {
                                getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                            }
                        }
                    }

                }
                @Override
                public void onRequestError(Object error) {
                    super.onRequestError(error);
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                }
            }).multiPartConnect(URLS.UPDATE_ADV,addAdsRequest,volleyFileObjects, StatusMsg.class);
        }else{
            new ConnectionHelper(new ConnectionListener() {
                @Override
                public void onRequestSuccess(Object response) {
                    if(!catchErrorResponse(response)) {
                        getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                        statusMsg = (StatusMsg) response;
                        if (statusMsg != null) {
                            setMessage(statusMsg.status,statusMsg.msg);
                            if (statusMsg.status == Constants.RESPONSE_SUCCESS) {
                                getmMutableLiveData().setValue(Constants.SUCCESS);
                            }else {
                                getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                            }
                        }
                    }

                }
                @Override
                public void onRequestError(Object error) {
                    super.onRequestError(error);
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                }
            }).requestJsonObject(Request.Method.POST,URLS.UPDATE_ADV,addAdsRequest, StatusMsg.class);
        }
    }

    public void getAdsDetails(int id,int type){
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);

        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if(!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    adsDetailsResponse = (AdsDetailsResponse) response;
                    if (adsDetailsResponse != null) {
                        setMessage(adsDetailsResponse.status,adsDetailsResponse.msg);
                        if (adsDetailsResponse.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.ADS_DETAILS);
                        }else {
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                        }
                    }
                }
            }
        }).requestJsonObject(Request.Method.GET, URLS.ADS_DETAILS+"?id="+id+"&type="+type, null, AdsDetailsResponse.class);
    }


    public void deleteImage(int id) {
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if(!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    statusMsg = (StatusMsg) response;
                    if (statusMsg != null) {
                        setMessage(statusMsg.status,statusMsg.msg);
                        if (statusMsg.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.DELETED);
                        }else {
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                        }
                    }
                }
            }
            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
                getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
            }
        }).requestJsonObject(Request.Method.DELETE, URLS.DELETE_PRODUCT+"/"+id+"?type="+ UserHelper.getUserDetails().type+"&delete_image=1", new Object(), StatusMsg.class);
    }


    private static final String TAG = "AdsRepository";
    public void deleteAds(int id) {
        Log.e(TAG, "deleteAds: "+id);
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if(!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    statusMsg = (StatusMsg) response;
                    if (statusMsg != null) {
                        setMessage(statusMsg.status,statusMsg.msg);
                        if (statusMsg.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.DELETED);
                        }else {
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                        }
                    }
                }
            }
            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
                getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
            }
        }).requestJsonObject(Request.Method.DELETE, URLS.DELETE_ADS+"/"+id+"?account_type="+ UserHelper.getUserDetails().type+"&type=2", new Object(), StatusMsg.class);
    }


    public AdsDetailsResponse getAdsDetailsResponse() {
        return adsDetailsResponse;
    }


}



