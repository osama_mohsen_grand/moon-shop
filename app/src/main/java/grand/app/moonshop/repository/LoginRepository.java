package grand.app.moonshop.repository;

import android.util.Log;

import com.android.volley.Request;

import java.util.ArrayList;

import androidx.lifecycle.MutableLiveData;
import grand.app.moonshop.models.base.StatusMsg;
import grand.app.moonshop.models.shop.AddShopResponse;
import grand.app.moonshop.models.user.changepassword.ChangePasswordRequest;
import grand.app.moonshop.models.user.forgetpassword.ForgetPasswordRequest;
import grand.app.moonshop.models.user.forgetpassword.ForgetPasswordResponse;
import grand.app.moonshop.models.user.login.FacebookLoginRequest;
import grand.app.moonshop.models.user.login.LoginRequest;
import grand.app.moonshop.models.user.login.LoginResponse;
import grand.app.moonshop.models.user.profile.ProfileShopRequest;
import grand.app.moonshop.models.user.type.ChangeTypeRequest;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.utils.storage.user.UserHelper;
import grand.app.moonshop.vollyutils.ConnectionHelper;
import grand.app.moonshop.vollyutils.ConnectionListener;
import grand.app.moonshop.vollyutils.URLS;
import grand.app.moonshop.vollyutils.VolleyFileObject;

public class LoginRepository extends BaseRepository {
    LoginResponse loginResponse = null;
    ForgetPasswordResponse forgetPasswordResponse;
    public LoginRepository(MutableLiveData<Object> mutableLiveData) {
        super(mutableLiveData);
    }

    public void loginUser(LoginRequest loginRequest) {
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if (!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    loginResponse = (LoginResponse) response;
                    if (loginResponse != null) {
                        setMessage(loginResponse.status, loginResponse.msg);
                        if (loginResponse.status == Constants.RESPONSE_SUCCESS) {
                            UserHelper.saveUserDetails(loginResponse.data);
                            if(loginResponse.data.packageId == 0)
                                getmMutableLiveData().setValue(Constants.PACKAGES);
                            else
                                getmMutableLiveData().setValue(Constants.HOME);
                        } else {
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                            setMessage(loginResponse.status, loginResponse.msg);
                        }
                    }
                }

            }

            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
                getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
            }
        }).requestJsonObject(Request.Method.POST, URLS.LOGIN, loginRequest, LoginResponse.class);
    }

    public LoginResponse getUser() {
        return loginResponse;
    }


    public StatusMsg statusMsg;

    public void changePassword(ChangePasswordRequest changePasswordRequest) {
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if (!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    statusMsg = (StatusMsg) response;
                    if (statusMsg != null) {
                        setMessage(statusMsg.status, statusMsg.msg);
                        if (statusMsg.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.SUCCESS);
                        } else {
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                            setMessage(statusMsg.status, statusMsg.msg);
                        }
                    }
                }

            }

            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
                getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
            }
        }).requestJsonObject(Request.Method.POST, URLS.FORGET_PASSWORD, changePasswordRequest, StatusMsg.class);
    }


    public void checkPhone(ForgetPasswordRequest forgetPasswordRequest) {
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if (!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    forgetPasswordResponse = (ForgetPasswordResponse) response;
                    if (forgetPasswordResponse != null) {
                        setMessage(forgetPasswordResponse.status, forgetPasswordResponse.msg);
                        if (forgetPasswordResponse.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.SUCCESS);
                        } else {
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                            setMessage(forgetPasswordResponse.status, forgetPasswordResponse.msg);
                        }
                    }
                }

            }
        }).requestJsonObject(Request.Method.POST, URLS.CHECK_PHONE, forgetPasswordRequest, ForgetPasswordResponse.class);
    }

    private static final String TAG = "LoginRepository";

    public void language() {
        getmMutableLiveData().setValue(Constants.LANGUAGE);
    }

    AddShopResponse addShopResponse;
    public void submitShopProfile(ProfileShopRequest profileRequest, VolleyFileObject volleyFileObject) {
        Log.d(TAG,"start submitShopProfile ");

        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        Log.d(TAG,"start submitShopProfile");
        ArrayList<VolleyFileObject> volleyFileObjects = new ArrayList<VolleyFileObject>();
        volleyFileObjects.add(volleyFileObject);

        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if(!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
//                    loginResponse = (LoginResponse) response;
//                    if (loginResponse != null) {
//                        setMessage(loginResponse.status, loginResponse.msg);
//                        if (loginResponse.status == Constants.RESPONSE_SUCCESS) {
//                            UserHelper.saveUserDetails(loginResponse.data);
//                            getmMutableLiveData().setValue(Constants.SUCCESS);
//                        } else {
//                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
//                            setMessage(loginResponse.status, loginResponse.msg);
//                        }
//                    }


                    addShopResponse = (AddShopResponse) response;
                    if (addShopResponse != null) {
                        setMessage(addShopResponse.status, addShopResponse.msg);
                        if (addShopResponse.status == Constants.RESPONSE_SUCCESS) {
                            UserHelper.saveUserDetails(addShopResponse.data);
                            getmMutableLiveData().setValue(Constants.SUCCESS);

//                            getmMutableLiveData().setValue(Constants.REGISTRATION);
                        } else {
                            if (addShopResponse.status == Constants.RESPONSE_401 && addShopResponse.suggestions != null) {
                                getmMutableLiveData().setValue(Constants.SUGGESTIONS);
                            } else
                                getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                        }
                    }

                }

            }
            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
                getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
            }
        }).multiPartConnect(URLS.UPDATE_PROFILE,profileRequest,volleyFileObjects, AddShopResponse.class);
    }

    public void submitShopProfile(ProfileShopRequest profileRequest) {
        Log.d(TAG,"start submitShopProfile");
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                Log.d(TAG,"start gee");
                if(!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
//                    loginResponse = (LoginResponse) response;
//                    if (loginResponse != null) {
//                        setMessage(loginResponse.status, loginResponse.msg);
//                        if (loginResponse.status == Constants.RESPONSE_SUCCESS) {
//                            UserHelper.saveUserDetails(loginResponse.data);
//                            getmMutableLiveData().setValue(Constants.SUCCESS);
//                        } else {
//                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
//                            setMessage(loginResponse.status, loginResponse.msg);
//                        }
//                    }

                    Log.d(TAG,"start");
                    addShopResponse = (AddShopResponse) response;
                    if (addShopResponse != null) {
                        Log.d(TAG,"not null");
                        setMessage(addShopResponse.status, addShopResponse.msg);
                        if (addShopResponse.status == Constants.RESPONSE_SUCCESS) {
                            Log.d(TAG,"RESPONSE_SUCCESS");
                            UserHelper.saveUserDetails(addShopResponse.data);
                            getmMutableLiveData().setValue(Constants.SUCCESS);

//                            getmMutableLiveData().setValue(Constants.REGISTRATION);
                        } else {
                            if (addShopResponse.status == Constants.RESPONSE_401 && addShopResponse.suggestions != null) {
                                getmMutableLiveData().setValue(Constants.SUGGESTIONS);
                            } else
                                getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                        }
                    }

                }

            }
            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
                Log.d(TAG,"error"+error);
                getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
            }
        }).requestJsonObject(Request.Method.POST, URLS.UPDATE_PROFILE,profileRequest, AddShopResponse.class);
    }

    public AddShopResponse getAddShopResponse() {
        return addShopResponse;
    }

    public void checkSocialIdExist(FacebookLoginRequest facebookLoginRequest) {
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                loginResponse = (LoginResponse) response;
                if (loginResponse != null) {
                    setMessage(loginResponse.status, loginResponse.msg);
                    if (loginResponse.status == Constants.RESPONSE_SUCCESS) {
                        UserHelper.saveUserDetails(loginResponse.data);
                        getmMutableLiveData().setValue(Constants.SUCCESS);
                    } else {
                        getmMutableLiveData().setValue(Constants.UNAUTHORIZED);
                    }
                }

            }
            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
                getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
            }
        }).requestJsonObject(Request.Method.POST, URLS.FACEBOOK_LOGIN,facebookLoginRequest, LoginResponse.class);
    }


    public ForgetPasswordResponse getForgetPasswordResponse() {
        return forgetPasswordResponse;
    }
}



