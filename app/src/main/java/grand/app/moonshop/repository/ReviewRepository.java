package grand.app.moonshop.repository;

import com.android.volley.Request;

import androidx.lifecycle.MutableLiveData;
import grand.app.moonshop.models.review.ReviewResponse;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.utils.storage.user.UserHelper;
import grand.app.moonshop.vollyutils.ConnectionHelper;
import grand.app.moonshop.vollyutils.ConnectionListener;
import grand.app.moonshop.vollyutils.URLS;

public class ReviewRepository extends BaseRepository {
    ReviewResponse reviewResponse = null;
    public ReviewRepository(MutableLiveData<Object> mutableLiveData) {
        super(mutableLiveData);
    }
    public void getReviews() {
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if(!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    reviewResponse = (ReviewResponse) response;
                    if (reviewResponse != null) {
                        setMessage(reviewResponse.status,reviewResponse.msg);
                        if (reviewResponse.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.SUCCESS);
                        }else {
                            getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                        }
                    }
                }

            }
            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
                getmMutableLiveData().setValue(Constants.SERVER_ERROR);
            }
        }).requestJsonObject(Request.Method.GET, URLS.SHOP_REVIEW+"?type="+UserHelper.getUserDetails().type, null, ReviewResponse.class);
    }

    public ReviewResponse getReviewResponse() {
        return reviewResponse;
    }
}



