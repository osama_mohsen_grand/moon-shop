package grand.app.moonshop.repository;

import com.android.volley.Request;

import androidx.lifecycle.MutableLiveData;
import grand.app.moonshop.models.base.StatusMsg;
import grand.app.moonshop.models.contact.ContactUsRequest;
import grand.app.moonshop.models.personalnfo.AccountInfoResponse;
import grand.app.moonshop.models.settings.SettingsResponse;
import grand.app.moonshop.models.settings.SettingsRequest;
import grand.app.moonshop.models.user.info.ProfileInfoRequest;
import grand.app.moonshop.models.user.info.ProfileInfoResponse;
import grand.app.moonshop.models.user.info.SocialMediaRequest;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.utils.storage.user.UserHelper;
import grand.app.moonshop.vollyutils.ConnectionHelper;
import grand.app.moonshop.vollyutils.ConnectionListener;
import grand.app.moonshop.vollyutils.URLS;


public class SettingsRepository extends CountryRepository {
    public SettingsResponse settingsResponse = null;
    public SettingsRepository(MutableLiveData<Object> mutableLiveData) {
        super(mutableLiveData);
    }

    StatusMsg statusMsg;

    public void contactUs(ContactUsRequest request) {
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if(!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    statusMsg = (StatusMsg) response;
                    if (statusMsg != null) {
                        setMessage(statusMsg.status,statusMsg.msg);
                        if (statusMsg.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.SUCCESS);
                        }else {
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                            setMessage(statusMsg.status,statusMsg.msg);
                        }
                    }
                }

            }
            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
                getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
            }
        }).requestJsonObject(Request.Method.POST, URLS.CONTACT_US, request, StatusMsg.class);
    }

    public SettingsResponse getSettingsResponse() {
        return settingsResponse;
    }

    public ProfileInfoResponse profileInfoResponse;
    public void accountInfo() {
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if(!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    profileInfoResponse = (ProfileInfoResponse) response;
                    if (profileInfoResponse != null) {
                        setMessage(profileInfoResponse.status,profileInfoResponse.msg);
                        if (profileInfoResponse.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.SUCCESS);
                        }else {
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                            setMessage(profileInfoResponse.status,profileInfoResponse.msg);
                        }
                    }
                }

            }
            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
                getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
            }
        }).requestJsonObject(Request.Method.GET, URLS.PROFILE_INFO+"?type="+ UserHelper.getUserDetails().type, null, ProfileInfoResponse.class);
    }

    public AccountInfoResponse accountInfoResponse;

    public void accountInfo(int id) {
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if (!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    accountInfoResponse = (AccountInfoResponse) response;
                    if (accountInfoResponse != null) {
                        setMessage(accountInfoResponse.status, accountInfoResponse.msg);
                        if (accountInfoResponse.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.ACCOUNT_INFO);
                        } else {
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                            setMessage(accountInfoResponse.status, accountInfoResponse.msg);
                        }
                    }
                }

            }

            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
                getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
            }
        }).requestJsonObject(Request.Method.GET, URLS.ACCOUNT_INFO + "id=" + id, null, AccountInfoResponse.class);
    }
    public void updateAccountInfo(ProfileInfoRequest request) {
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if(!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    statusMsg = (StatusMsg) response;
                    if (statusMsg != null) {
                        setMessage(statusMsg.status,statusMsg.msg);
                        if (statusMsg.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.UPDATE);
                        }else {
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                            setMessage(statusMsg.status,statusMsg.msg);
                        }
                    }
                }

            }
            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
                getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
            }
        }).requestJsonObject(Request.Method.POST, URLS.ADD_TRANSFER_GUIDE, request, StatusMsg.class);
    }

    public void updateSocial(SocialMediaRequest request) {
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if(!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    statusMsg = (StatusMsg) response;
                    if (statusMsg != null) {
                        setMessage(statusMsg.status,statusMsg.msg);
                        if (statusMsg.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.UPDATE);
                        }else {
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                            setMessage(statusMsg.status,statusMsg.msg);
                        }
                    }
                }

            }
            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
                getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
            }
        }).requestJsonObject(Request.Method.POST, URLS.ADD_SOCIAL_MEDIA, request, StatusMsg.class);
    }

    public void getSettings(int type) {
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        SettingsRequest settingsRequest = new SettingsRequest(type);

        try {
            settingsRequest.type = Integer.parseInt(UserHelper.getUserDetails().type);
        }catch (Exception e){
            e.getStackTrace();
        }
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if (!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    settingsResponse = (SettingsResponse) response;
                    if (settingsResponse != null) {
                        setMessage(settingsResponse.status, settingsResponse.msg);
                        if (settingsResponse.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.SUCCESS);
                        } else {
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                            setMessage(settingsResponse.status, settingsResponse.msg);
                        }
                    }
                }
            }

            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
                getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
            }
        }).requestJsonObject(Request.Method.POST, URLS.SETTINGS, settingsRequest  , SettingsResponse.class);
    }
}



