package grand.app.moonshop.repository;

import com.android.volley.Request;

import java.util.ArrayList;

import androidx.lifecycle.MutableLiveData;
import grand.app.moonshop.models.ads.CompanyAdsCategoriesResponse;
import grand.app.moonshop.models.app.AppMoon;
import grand.app.moonshop.models.app.Mutable;
import grand.app.moonshop.models.base.IdNameImage;
import grand.app.moonshop.models.base.StatusMsg;
import grand.app.moonshop.models.famous.album.FamousAddAlbumRequest;
import grand.app.moonshop.models.famous.album.FamousAddImageInsideAlbumRequest;
import grand.app.moonshop.models.famous.album.FamousAlbumImagesResponse;
import grand.app.moonshop.models.famous.FollowRequest;
import grand.app.moonshop.models.famous.advertise.AdvertiseDetailsResponse;
import grand.app.moonshop.models.famous.album.FamousEditAlbumRequest;
import grand.app.moonshop.models.famous.details.FamousDetailsResponse;
import grand.app.moonshop.models.famous.home.FamousHomeResponse;
import grand.app.moonshop.models.famous.list.FamousListResponse;
import grand.app.moonshop.models.famous.search.FamousSearchShopResponse;
import grand.app.moonshop.models.offer.OfferResponse;
import grand.app.moonshop.models.service.MainServiceResponse;
import grand.app.moonshop.models.user.profile.User;
import grand.app.moonshop.retrofitutils.Upload.ProgressRequestBody;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.utils.storage.user.UserHelper;
import grand.app.moonshop.vollyutils.ConnectionHelper;
import grand.app.moonshop.vollyutils.ConnectionListener;
import grand.app.moonshop.vollyutils.URLS;
import grand.app.moonshop.vollyutils.VolleyFileObject;

public class FamousRepository extends BaseRepository {
    FamousListResponse famousListResponse = null;
    FamousDetailsResponse famousDetailsResponse = null;
    FamousHomeResponse famousHomeResponse = null;
    AdvertiseDetailsResponse advertiseDetailsResponse;
    FamousSearchShopResponse famousSearchShopResponse;
    FamousAlbumImagesResponse famousAlbumImagesResponse;
    StatusMsg statusMsg;

    public FamousRepository(MutableLiveData<Object> mutableLiveData) {
        super(mutableLiveData);
    }


    public void addAlbum(FamousAddAlbumRequest famousAddAlbumRequest, ArrayList<VolleyFileObject> volleyFileObjects) {
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        ConnectionHelper connectionHelper = new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if (!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    statusMsg = (StatusMsg) response;
                    if (statusMsg != null) {
                        setMessage(statusMsg.status, statusMsg.msg);
                        if (statusMsg.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.SUCCESS);
                        } else {
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                            setMessage(statusMsg.status, statusMsg.msg);
                        }
                    }
                }

            }

            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
                getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
            }
        });
        if(volleyFileObjects.size() > 0)
            connectionHelper.multiPartConnect(URLS.FAMOUS_ADD_ALBUM, famousAddAlbumRequest, volleyFileObjects, StatusMsg.class);
        else
            connectionHelper.requestJsonObject(Request.Method.POST, URLS.FAMOUS_ADD_ALBUM, famousAddAlbumRequest, StatusMsg.class);
    }


    public void getAlbumImages(int id) {
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if (!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    famousAlbumImagesResponse = (FamousAlbumImagesResponse) response;
                    if (famousAlbumImagesResponse != null) {
                        setMessage(famousAlbumImagesResponse.status, famousAlbumImagesResponse.msg);
                        if (famousAlbumImagesResponse.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.SUCCESS);
                        } else {
                            getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                        }
                    }
                }

            }

            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
                getmMutableLiveData().setValue(Constants.SERVER_ERROR);
            }
        }).requestJsonObject(Request.Method.GET, URLS.ALBUM_IMAGES + "?album_id=" + id+"&type="+AppMoon.getUserType(),
                null, FamousAlbumImagesResponse.class);
    }


    public void home(String tab) {
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if (!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    famousHomeResponse = (FamousHomeResponse) response;
                    if (famousHomeResponse != null) {
                        setMessage(famousHomeResponse.status, famousHomeResponse.msg);
                        if (famousHomeResponse.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.SUCCESS);
                        } else {
                            getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                        }
                    }
                }

            }

            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
                getmMutableLiveData().setValue(Constants.SERVER_ERROR);
            }
        }).requestJsonObject(Request.Method.GET, URLS.FAMOUS_ALBUMS + "?tab=" + tab + "&account_type=" + AppMoon.getUserType(), null, FamousHomeResponse.class);
    }


    public void getFamousList(int type, int gender, int photographerTypeId) {
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if(!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    famousListResponse = (FamousListResponse) response;
                    if (famousListResponse != null) {
                        setMessage(famousListResponse.status,famousListResponse.msg);
                        if (famousListResponse.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.SUCCESS);
                        }else {
                            getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                        }
                    }
                }

            }
            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
                getmMutableLiveData().setValue(Constants.SERVER_ERROR);
            }
        }).requestJsonObject(Request.Method.GET, URLS.FAMOUS+"?type="+type+"&gender="+gender+"&country_id="+UserHelper.retrieveKey(Constants.COUNTRY_ID)+"&flag="+photographerTypeId, null, FamousListResponse.class);
    }


    public void getShopByName(String name,String category_id) {
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if (!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    famousSearchShopResponse = (FamousSearchShopResponse) response;
                    if (famousSearchShopResponse != null) {
                        setMessage(famousSearchShopResponse.status, famousSearchShopResponse.msg);
                        if (famousSearchShopResponse.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.SEARCH);
                        } else {
                            getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                        }
                    }
                }

            }

            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
                getmMutableLiveData().setValue(Constants.SERVER_ERROR);
            }
        }).requestJsonObject(Request.Method.GET, URLS.FAMOUS_SEARCH_SHOP + "?name=" + name+"&category_id="+category_id+"&type="+ UserHelper.getUserDetails().type, null, FamousSearchShopResponse.class);
    }


    public void getFamousDetails(int id, int tab) {
//        id = 4 ; type = 2; //TODO

        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if (!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    famousDetailsResponse = (FamousDetailsResponse) response;
                    if (famousDetailsResponse != null) {
                        setMessage(famousDetailsResponse.status, famousDetailsResponse.msg);
                        if (famousDetailsResponse.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.SUCCESS);
                        } else {
                            getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                        }
                    }
                }

            }

            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
                getmMutableLiveData().setValue(Constants.SERVER_ERROR);
            }
        }).requestJsonObject(Request.Method.GET, URLS.FAMOUS_DETAILS + "?id=" + id + "&tab=" + tab + "&account_type=" + UserHelper.getUserDetails().type, null, FamousDetailsResponse.class);
    }


    public void follow(int id) {
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if (!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    statusMsg = (StatusMsg) response;
                    if (statusMsg != null) {
                        setMessage(statusMsg.status, statusMsg.msg);
                        if (statusMsg.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.FAMOUS_FOLLOW);
                        } else {
                            getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                        }
                    }
                }

            }

            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
                getmMutableLiveData().setValue(Constants.SERVER_ERROR);
            }
        }).requestJsonObject(Request.Method.POST, URLS.FAMOUS_FOLLOW, new FollowRequest(id), StatusMsg.class);
    }

    public void getAdvertiseDetails(int id, int type) {
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if (!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    advertiseDetailsResponse = (AdvertiseDetailsResponse) response;
                    if (advertiseDetailsResponse != null) {
                        setMessage(advertiseDetailsResponse.status, advertiseDetailsResponse.msg);
                        if (advertiseDetailsResponse.status == Constants.RESPONSE_SUCCESS) {

                            getmMutableLiveData().setValue(Constants.SUCCESS);
                        } else {
                            getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                        }
                    }
                }

            }

            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
                getmMutableLiveData().setValue(Constants.SERVER_ERROR);
            }
        }).requestJsonObject(Request.Method.GET, URLS.ADVERTISE_DETAILS + "?id=" + id + "&type=" + type,
                null, AdvertiseDetailsResponse.class);
    }

    public AdvertiseDetailsResponse getAdvertiseDetailsResponse() {
        return advertiseDetailsResponse;
    }

    public FamousDetailsResponse getFamousDetailsResponse() {
        return famousDetailsResponse;
    }

    public FamousHomeResponse getFamousHomeResponse() {
        return famousHomeResponse;
    }

    public FamousListResponse getFamousListResponse() {
        return famousListResponse;
    }

    public StatusMsg getStatusMsg() {
        return statusMsg;
    }

    public FamousSearchShopResponse getFamousSearchShopResponse() {
        return famousSearchShopResponse;
    }

    public FamousAlbumImagesResponse getFamousAlbumImagesResponse() {
        return famousAlbumImagesResponse;
    }

    public void setFamousAlbumImagesResponse(FamousAlbumImagesResponse famousAlbumImagesResponse) {
        this.famousAlbumImagesResponse = famousAlbumImagesResponse;
    }

    public void removeAlbumImage(int id) {
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if (!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    statusMsg = (StatusMsg) response;
                    if (statusMsg != null) {
                        setMessage(statusMsg.status, statusMsg.msg);
                        if (statusMsg.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.DELETED);
                        } else {
                            getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                        }
                    }
                }

            }

            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
                getmMutableLiveData().setValue(Constants.SERVER_ERROR);
            }
        }).requestJsonObject(Request.Method.DELETE, URLS.FAMOUS_DELETE_IMAGE + "/" + id,
                null, StatusMsg.class);
    }


    //Add Image To Album
    public void addImageToAlbum(FamousAddImageInsideAlbumRequest famousAddImageInsideAlbumRequest, ArrayList<VolleyFileObject> volleyFileObject) {
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS_PERCENTAGE);
//        new ConnectionHelper(new ConnectionListener() {
//            @Override
//            public void onRequestSuccess(Object response) {
//                if (!catchErrorResponse(response)) {
//                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
//                    statusMsg = (StatusMsg) response;
//                    if (statusMsg != null) {
//                        setMessage(statusMsg.status, statusMsg.msg);
//                        if (statusMsg.status == Constants.RESPONSE_SUCCESS) {
//                            getmMutableLiveData().setValue(Constants.DONE);
//                        } else {
//                            getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
//                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
//                        }
//                    }
//                }
//
//            }
//
//            @Override
//            public void onRequestError(Object error) {
//                super.onRequestError(error);
//                getmMutableLiveData().setValue(Constants.SERVER_ERROR);
//            }
//        }).multiPartConnect(URLS.FAMOUS_EDIT_ALBUM,
//                famousAddImageInsideAlbumRequest, volleyFileObject, StatusMsg.class);



        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if(!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS_PERCENTAGE);
                    statusMsg = (StatusMsg) response;
                    if (statusMsg != null) {
                        setMessage(statusMsg.status, statusMsg.msg);
                        if (statusMsg.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.DONE);
                        } else {
                            getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                        }
                    }
                }

            }
            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
                getmMutableLiveData().setValue(Constants.SERVER_ERROR);
            }
        }).multiPartConnectVideoDialog(URLS.FAMOUS_EDIT_ALBUM,famousAddImageInsideAlbumRequest, volleyFileObject, StatusMsg.class,new ProgressRequestBody.UploadCallbacks() {
            @Override
            public void onProgressUpdate(int percentage) {
                getmMutableLiveData().setValue(new Mutable(Constants.PROGRESS_PERCENTAGE,percentage));
            }

            @Override
            public void onError() {

            }

            @Override
            public void onFinish() {

            }
        });
    }


    public void editAlbum(FamousEditAlbumRequest famousEditAlbumRequest, VolleyFileObject fileObject) {
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        ConnectionHelper connectionHelper = new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if (!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    statusMsg = (StatusMsg) response;
                    if (statusMsg != null) {
                        setMessage(statusMsg.status, statusMsg.msg);
                        if (statusMsg.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.SUCCESS);
                        } else {
                            getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                        }
                    }
                }

            }

            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
                getmMutableLiveData().setValue(Constants.SERVER_ERROR);
            }
        });
        if (fileObject == null) {
            connectionHelper.requestJsonObject(Request.Method.POST, URLS.FAMOUS_EDIT_ALBUM,
                    famousEditAlbumRequest, StatusMsg.class);
        }else {
            ArrayList<VolleyFileObject> volleyFileObjects = new ArrayList<>();
            volleyFileObjects.add(fileObject);
            connectionHelper.multiPartConnect(URLS.FAMOUS_EDIT_ALBUM,
                    famousEditAlbumRequest,volleyFileObjects, StatusMsg.class);
        }
    }

    public void getFamousDiscover(int type) {
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if (!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    famousAlbumImagesResponse = (FamousAlbumImagesResponse) response;
                    if (famousAlbumImagesResponse != null) {
                        setMessage(famousAlbumImagesResponse.status, famousAlbumImagesResponse.msg);
                        if (famousAlbumImagesResponse.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.SUCCESS);
                        } else {
                            getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                        }
                    }
                }

            }

            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
                getmMutableLiveData().setValue(Constants.SERVER_ERROR);
            }
        }).requestJsonObject(Request.Method.GET, URLS.FAMOUS_DISCOVER + "?type=" + type + "&country_id=" + UserHelper.retrieveKey(Constants.COUNTRY_ID),
                null, FamousAlbumImagesResponse.class);
    }

    public MainServiceResponse mainServiceResponse;

    public void mainService() {
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if (!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    mainServiceResponse = (MainServiceResponse) response;
                    if (mainServiceResponse != null) {
                        setMessage(mainServiceResponse.status, mainServiceResponse.msg);
                        if (mainServiceResponse.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.MAIN_SERVICE);
                        } else {
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                        }
                    }
                }
            }

            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
                getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
            }
        }).requestJsonObject(Request.Method.GET, URLS.MAIN_SERVICE + "?type=" + UserHelper.getUserDetails().type , null, MainServiceResponse.class);
    }

    public void deleteAlbum(int id) {
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if (!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    statusMsg = (StatusMsg) response;
                    if (statusMsg != null) {
                        setMessage(statusMsg.status, statusMsg.msg);
                        if (statusMsg.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.DELETED);
                        } else {
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                        }
                    }
                }
            }

            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
                getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
            }
        }).requestJsonObject(Request.Method.DELETE, URLS.DELETE_ADS + "/" + id + "?account_type=" + UserHelper.getUserDetails().type + "&type=1", new Object(), StatusMsg.class);
    }


    public CompanyAdsCategoriesResponse companyAdsCategoriesResponse;

    public void getCompanyAdsCategories(int famous_id, int id) {
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if(!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    companyAdsCategoriesResponse = (CompanyAdsCategoriesResponse) response;
                    if (companyAdsCategoriesResponse != null) {
                        setMessage(companyAdsCategoriesResponse.status,companyAdsCategoriesResponse.msg);
                        if (companyAdsCategoriesResponse.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.SUCCESS);
                        }else {
                            getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                        }
                    }
                }

            }
            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
                getmMutableLiveData().setValue(Constants.SERVER_ERROR);
            }
        }).requestJsonObject(Request.Method.GET, URLS.FAMOUS_COMPANY_ADS_CATEGORIES+"?id="+famous_id+"&category_id="+id ,
                null, CompanyAdsCategoriesResponse.class);
    }

}



