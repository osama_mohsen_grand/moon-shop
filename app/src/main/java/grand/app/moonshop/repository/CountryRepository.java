package grand.app.moonshop.repository;

import com.android.volley.Request;

import androidx.lifecycle.MutableLiveData;
import grand.app.moonshop.models.base.StatusMsg;
import grand.app.moonshop.models.chat.ChatGetRequest;
import grand.app.moonshop.models.chat.ChatRequest;
import grand.app.moonshop.models.country.CountriesResponse;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.utils.storage.user.UserHelper;
import grand.app.moonshop.vollyutils.ConnectionHelper;
import grand.app.moonshop.vollyutils.ConnectionListener;
import grand.app.moonshop.vollyutils.URLS;


public class CountryRepository extends BaseRepository {
    private CountriesResponse countriesResponse = null;
    public CountryRepository(MutableLiveData<Object> mutableLiveData) {
        super(mutableLiveData);
    }
    public void getCountries(boolean showProgress) {
        if(showProgress) getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if(!catchErrorResponse(response)) {
                    if(showProgress) getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    countriesResponse = (CountriesResponse) response;
                    if (countriesResponse != null) {
                        setMessage(countriesResponse.status,countriesResponse.msg);
                        if (countriesResponse.status == Constants.RESPONSE_SUCCESS) {
                            UserHelper.saveCountries(countriesResponse);
                            getmMutableLiveData().setValue(Constants.COUNTRIES);
                        }else {
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                            setMessage(countriesResponse.status,countriesResponse.msg);
                        }
                    }
                }

            }
            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
                getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
            }
        }).requestJsonObject(Request.Method.GET, URLS.GET_ALL_COUNTRIES, null, CountriesResponse.class);
    }

    public CountriesResponse getCountriesResponse() {
        return countriesResponse;
    }
}



