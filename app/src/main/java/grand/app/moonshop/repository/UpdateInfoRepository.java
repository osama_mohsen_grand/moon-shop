package grand.app.moonshop.repository;

import com.android.volley.Request;

import androidx.lifecycle.MutableLiveData;
import grand.app.moonshop.models.base.StatusMsg;
import grand.app.moonshop.models.base.UserId;
import grand.app.moonshop.models.user.forgetpassword.ForgetPasswordRequest;
import grand.app.moonshop.models.user.profile.ProfileRequest;
import grand.app.moonshop.models.user.profile.ProfileResponse;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.vollyutils.ConnectionHelper;
import grand.app.moonshop.vollyutils.ConnectionListener;
import grand.app.moonshop.vollyutils.URLS;

public class UpdateInfoRepository extends BaseRepository {
    ProfileResponse profileResponse = null;
    ProfileRequest profileRequest;
    StatusMsg statusMsg;
    ForgetPasswordRequest forgetPasswordRequest;
    UserId userId = null;
    public UpdateInfoRepository(MutableLiveData<Object> mutableLiveData, ProfileRequest profileRequest) {
        super(mutableLiveData);
        this.profileRequest = profileRequest;
    }

    public UpdateInfoRepository(MutableLiveData<Object> mutableLiveData, ForgetPasswordRequest forgetPasswordRequest) {
        super(mutableLiveData);
        this.forgetPasswordRequest = forgetPasswordRequest;
    }
    public void submit() {
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if(!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    profileResponse = (ProfileResponse) response;
                    if (profileResponse != null) {
                        setMessage(profileResponse.status,profileResponse.msg);
                        if (profileResponse.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.UPDATE_PROFILE);
                        }else {
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                            setMessage(profileResponse.status,profileResponse.msg);
                        }
                    }
                }

            }
            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
                getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
            }
        }).requestJsonObject(Request.Method.POST, URLS.UPDATE_PROFILE, profileRequest.getMap(), ProfileResponse.class);
    }

    public void forgetPassword(){
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if(!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    statusMsg = (StatusMsg) response;
                    if (statusMsg != null) {
                        setMessage(statusMsg.status,statusMsg.msg);
                        if (statusMsg.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.FORGET_PASSWORD);
                        }else {
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                            setMessage(statusMsg.status,statusMsg.msg);
                        }
                    }
                }

            }
            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
                getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
            }
        }).requestJsonObject(Request.Method.POST, URLS.CODE_SEND, forgetPasswordRequest, StatusMsg.class);
    }



    public ProfileResponse getData() {
        return profileResponse;
    }
}



