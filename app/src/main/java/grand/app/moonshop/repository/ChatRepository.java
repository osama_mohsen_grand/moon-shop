package grand.app.moonshop.repository;

import com.android.volley.Request;

import java.util.ArrayList;

import androidx.lifecycle.MutableLiveData;
import grand.app.moonshop.models.app.AppMoon;
import grand.app.moonshop.models.base.StatusMsg;
import grand.app.moonshop.models.chat.ChatRequest;
import grand.app.moonshop.models.chat.ChatSendResponse;
import grand.app.moonshop.models.chat.details.ChatDetailsResponse;
import grand.app.moonshop.models.chat.list.ChatListResponse;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.vollyutils.ConnectionHelper;
import grand.app.moonshop.vollyutils.ConnectionListener;
import grand.app.moonshop.vollyutils.URLS;
import grand.app.moonshop.vollyutils.VolleyFileObject;


public class ChatRepository extends BaseRepository {
    ChatListResponse chatListResponse = null;
    ChatDetailsResponse chatDetailsResponse = null;
    public ChatRepository(MutableLiveData<Object> mutableLiveData) {
        super(mutableLiveData);
    }
    public void getChatDetails(int id, int type) {
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if(!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    chatDetailsResponse = (ChatDetailsResponse) response;
                    if (chatDetailsResponse != null) {
                        setMessage(chatDetailsResponse.status,chatDetailsResponse.msg);
                        if (chatDetailsResponse.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.CHAT);
                        }else {
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                            setMessage(chatDetailsResponse.status,chatDetailsResponse.msg);
                        }
                    }
                }

            }
            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
                getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
            }
        }).requestJsonObject(Request.Method.GET, URLS.CHAT_DETAILS+"?order_id="+id+"&type="+ AppMoon.getUserType()+"&receiver_type="+type, null, ChatDetailsResponse.class);
    }


    public void getChatDetailsFromChatList(int id, int type) {
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if(!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    chatDetailsResponse = (ChatDetailsResponse) response;
                    if (chatDetailsResponse != null) {
                        setMessage(chatDetailsResponse.status,chatDetailsResponse.msg);
                        if (chatDetailsResponse.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.CHAT);
                        }else {
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                            setMessage(chatDetailsResponse.status,chatDetailsResponse.msg);
                        }
                    }
                }

            }
            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
                getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
            }
        }).requestJsonObject(Request.Method.GET, URLS.CHAT +"?id="+id+"&type="+ AppMoon.getUserType()+"&receiver_type="+type, null, ChatDetailsResponse.class);
    }


    public void getChatList() {
        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if(!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    chatListResponse = (ChatListResponse) response;
                    if (chatListResponse != null) {
                        setMessage(chatListResponse.status,chatListResponse.msg);
                        if (chatListResponse.status == Constants.RESPONSE_SUCCESS) {

                            getmMutableLiveData().setValue(Constants.CHAT_LIST);
                        }else {
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                            setMessage(chatListResponse.status,chatListResponse.msg);
                        }
                    }
                }

            }
            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
                getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
            }
        }).requestJsonObject(Request.Method.GET, URLS.CHAT_GET+"?account_type="+ AppMoon.getUserType(), null, ChatListResponse.class);
    }


    //here
    StatusMsg statusMsg;
    ChatSendResponse chatSendResponse;
    public void send(ChatRequest chatRequest, VolleyFileObject volleyFileObject,int id_chat) {
        ArrayList<VolleyFileObject> volleyFileObjects = null;
        if(volleyFileObject != null){
            volleyFileObjects = new ArrayList<>();
            volleyFileObjects.add(volleyFileObject);
        }
        String url = "";
        if(id_chat == -1)
            url = URLS.CHAT_DETAILS;
        else
            url = URLS.CHAT_SEND;

        getmMutableLiveData().setValue(Constants.SHOW_PROGRESS);
        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                if(!catchErrorResponse(response)) {
                    getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
                    chatSendResponse = (ChatSendResponse) response;
                    if (chatSendResponse != null) {
                        setMessage(chatSendResponse.status,chatSendResponse.msg);
                        if (chatSendResponse.status == Constants.RESPONSE_SUCCESS) {
                            getmMutableLiveData().setValue(Constants.CHAT_SEND);
                        }else {
                            getmMutableLiveData().setValue(Constants.ERROR_RESPONSE);
                            setMessage(statusMsg.status,statusMsg.msg);
                        }
                    }
                }

            }
            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
                getmMutableLiveData().setValue(Constants.HIDE_PROGRESS);
            }
        }).multiPartConnect(url, chatRequest,volleyFileObjects, ChatSendResponse.class);
    }

    public StatusMsg getStatusMsg() {
        return statusMsg;
    }

    public ChatListResponse getChatListResponse() {
        return chatListResponse;
    }

    public ChatDetailsResponse getChatDetailsResponse() {
        return chatDetailsResponse;
    }

    public ChatSendResponse getChatSendResponse() {
        return chatSendResponse;
    }
}



