package grand.app.moonshop.customviews.actionbar;

/**
 * Created by mohamedatef on 12/30/18.
 */


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Toast;

import androidx.core.view.GravityCompat;
import androidx.databinding.DataBindingUtil;
import androidx.drawerlayout.widget.DrawerLayout;
import grand.app.moonshop.R;
import grand.app.moonshop.customviews.menu.NavigationDrawerView;
import grand.app.moonshop.databinding.LayoutActionBarHomeBinding;
import grand.app.moonshop.models.app.AppMoon;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.utils.resources.ResourceManager;
import grand.app.moonshop.views.activities.BaseActivity;

public class HomeActionBarView extends RelativeLayout {
    LayoutActionBarHomeBinding layoutActionBarHomeBinding;
    NavigationDrawerView navigationDrawerView;
    DrawerLayout drawerLayout;
    ScrollView svMenu;

    public HomeActionBarView(Context context) {
        super(context);
        init();
    }


    private void init() {
        LayoutInflater layoutInflater = LayoutInflater.from(getContext());
        layoutActionBarHomeBinding = DataBindingUtil.inflate(layoutInflater, R.layout.layout_action_bar_home, this, true);
        setEvents();
    }

    private void setEvents() {
        layoutActionBarHomeBinding.imgHomeBarMenu.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                connectDrawer(HomeActionBarView.this.drawerLayout, false);
            }
        });
        layoutActionBarHomeBinding.imgHomeBarSearch.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), BaseActivity.class);
                if (AppMoon.getUserType().equals(Constants.TYPE_PHOTOGRAPHER) || AppMoon.getUserType().equals(Constants.TYPE_FAMOUS_PEOPLE)) {
                    intent.putExtra(Constants.PAGE, Constants.SEARCH_ALBUM);
                    getContext().startActivity(intent);
                } else if (AppMoon.getUserType().equals(Constants.TYPE_CONSUMER_MARKET) || AppMoon.getUserType().equals(Constants.TYPE_MARKET_SERVICE) ||
                        AppMoon.getUserType().equals(Constants.TYPE_RESERVATION_CLINIC) || AppMoon.getUserType().equals(Constants.TYPE_RESERVATION_BEAUTY) ) {
                    Bundle bundle = new Bundle();
                    if (navigationDrawerView.getAdapter().selectId.equals(Constants.FAMOUS)) {
                        bundle.putInt(Constants.TYPE, 4);
                    } else if (navigationDrawerView.getAdapter().selectId.equals(Constants.PHOTOGRAPHER)) {
                        bundle.putInt(Constants.TYPE, 5);
                    }
                    intent.putExtra(Constants.PAGE, Constants.SEARCH_FAMOUS);
                    intent.putExtra(Constants.BUNDLE, bundle);
                    getContext().startActivity(intent);
                }
            }
        });
    }

    public void setTitle(String title) {
        layoutActionBarHomeBinding.tvHomeBarText.setText(title);
    }

    public void connectDrawer(DrawerLayout drawerLayout, boolean firstConnect) {

        if (firstConnect) {
            this.drawerLayout = drawerLayout;
            return;
        } else {
            if (drawerLayout.isDrawerOpen(GravityCompat.END))
                drawerLayout.closeDrawer(GravityCompat.START);
            else
                drawerLayout.openDrawer(GravityCompat.START);
            svMenu.fullScroll(ScrollView.FOCUS_UP);
        }
    }


    public void setScroll(ScrollView svMenu) {
        this.svMenu = svMenu;
    }

    public void setSearchVisibility(String id) {
        if (AppMoon.searchVisibility(id))
            layoutActionBarHomeBinding.imgHomeBarSearch.setVisibility(VISIBLE);
        else
            layoutActionBarHomeBinding.imgHomeBarSearch.setVisibility(GONE);
    }

    public void setNavigation(NavigationDrawerView navigationDrawerView) {
        this.navigationDrawerView = navigationDrawerView;
    }
}
