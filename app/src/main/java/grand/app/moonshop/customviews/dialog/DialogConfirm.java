package grand.app.moonshop.customviews.dialog;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;


import androidx.appcompat.app.AlertDialog;
import androidx.databinding.DataBindingUtil;
import grand.app.moonshop.R;
import grand.app.moonshop.databinding.LayoutDialogRemoveBinding;
import grand.app.moonshop.utils.dialog.DialogHelperInterface;
import grand.app.moonshop.utils.resources.ResourceManager;


public class DialogConfirm {
    private Context context;
    private LayoutDialogRemoveBinding layoutDialogRemoveBinding = null;
    private String title = ResourceManager.getString(R.string.title), message = ResourceManager.getString(R.string.do_you_want_delete_product);
    private String actionText = ResourceManager.getString(R.string.remove_item);
    private String actionCancel = ResourceManager.getString(R.string.cancel);

    public DialogConfirm(Context context) {
        this.context = context;
        init();
    }

    public DialogConfirm setTitle(String title){
        this.title = title;
        return this;
    }

    public DialogConfirm setMessage(String message){
        this.message = message;
        return this;
    }

    public DialogConfirm setActionText(String actionText) {
        this.actionText = actionText;
        return this;
    }

    public DialogConfirm setActionCancel(String actionCancel) {
        this.actionCancel = actionCancel;
        return this;
    }


    private void init() {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        layoutDialogRemoveBinding  = DataBindingUtil.inflate(layoutInflater, R.layout.layout_dialog_remove, null, true);
    }

    public void show(DialogHelperInterface dialogHelperInterface){
        View view = layoutDialogRemoveBinding.getRoot();
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        AlertDialog dialog = builder.create();
        dialog.getWindow().getAttributes().windowAnimations = R.style.dialogAnimationUpBottom;
        dialog.setView(view);
        dialog.show();

        layoutDialogRemoveBinding.tvDialogTitle.setText(title);
        layoutDialogRemoveBinding.tvDialogMessage.setText(message);
        layoutDialogRemoveBinding.btnDialogSubmit.setText(actionText);
        layoutDialogRemoveBinding.btnDialogCancel.setText(actionCancel);
        layoutDialogRemoveBinding.btnDialogCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        layoutDialogRemoveBinding.imgDialogClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        layoutDialogRemoveBinding.btnDialogSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                dialogHelperInterface.OnClickListenerContinue(dialog,v);
            }
        });
    }
}
