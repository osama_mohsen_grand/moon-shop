package grand.app.moonshop.customviews.dialog;

import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;

import java.util.Calendar;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moonshop.R;
import grand.app.moonshop.databinding.ReservationDateTimeDialogBinding;
import grand.app.moonshop.models.reservation.doctor.schedule.Day;
import grand.app.moonshop.utils.helper.AppUtils;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.utils.dialog.DialogScheduleInterface;
import grand.app.moonshop.viewmodels.schedule.AddScheduleViewModel;
import timber.log.Timber;

public class DialogDateSchedule {

    ReservationDateTimeDialogBinding reservationDateTimeDialogBinding;
    AddScheduleViewModel addScheduleViewModel;
    private Context context;
    public TimePickerDialog timePickerDialog;
    public DialogDateSchedule(Context context) {
        this.context = context;
    }

    public void init(int doctor_id, Day day, int status, int _switch) {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        reservationDateTimeDialogBinding = DataBindingUtil.inflate(layoutInflater, R.layout.reservation_date_time_dialog, null, true);
        addScheduleViewModel = new AddScheduleViewModel(doctor_id, day.id, status, _switch);
        if(day.schedules.size() > 0){
            addScheduleViewModel.addScheduleRequest.from.add(day.schedules.get(0).from);
            addScheduleViewModel.addScheduleRequest.setFromText(day.schedules.get(0).from);

            addScheduleViewModel.addScheduleRequest.to.add(day.schedules.get(0).to);
            addScheduleViewModel.addScheduleRequest.setToText(day.schedules.get(0).to);

            addScheduleViewModel.addScheduleRequest.period.add(day.schedules.get(0).period);
            addScheduleViewModel.addScheduleRequest.setPeriodText(day.schedules.get(0).period);
        }
        if(day.schedules.size() > 1){
            addScheduleViewModel.addScheduleRequest.from.add(day.schedules.get(1).from);
            addScheduleViewModel.addScheduleRequest.setFromText2(day.schedules.get(1).from);

            addScheduleViewModel.addScheduleRequest.to.add(day.schedules.get(1).to);
            addScheduleViewModel.addScheduleRequest.setToText2(day.schedules.get(1).to);

            addScheduleViewModel.addScheduleRequest.period.add(day.schedules.get(1).period);
            addScheduleViewModel.addScheduleRequest.periodText2 = day.schedules.get(1).period;
        }
        addScheduleViewModel.notifyChange();
        reservationDateTimeDialogBinding.setAddScheduleViewModel(addScheduleViewModel);
    }

    /*
                if (timePickerDialog == null) {
                Calendar mcurrentTime = Calendar.getInstance();
                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                int minute = mcurrentTime.get(Calendar.MINUTE);

                timePickerDialog = new TimePickerDialog(textView.getContext(), (timePicker, selectedHour, selectedMinute) -> {
                    textView.setText(AppUtils.numberToDecimal(selectedHour) + ":" + AppUtils.numberToDecimal(selectedMinute));
                    time_server = AppUtils.numberToDecimal(selectedHour) + ":" + AppUtils.numberToDecimal(selectedMinute);
                }, hour, minute, true);//Yes 24 hour time

            }
            timePickerDialog.show();
     */


    public void show(DialogScheduleInterface dialogScheduleInterface) {
        View view = reservationDateTimeDialogBinding.getRoot();
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        AlertDialog dialog = builder.create();
        dialog.getWindow().getAttributes().windowAnimations = R.style.dialogAnimationUpBottom;
        dialog.setView(view);
        dialog.show();
        addScheduleViewModel.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                String action = (String) o;
                if (action.equals(Constants.SCHEDULE)) {
                    dialog.dismiss();
                    dialogScheduleInterface.OnClickListenerContinue(dialog, addScheduleViewModel.addScheduleRequest);
                }else if(action.equals(Constants.TIME_DIALOG)){
                    Calendar mcurrentTime = Calendar.getInstance();
                    int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                    int minute = mcurrentTime.get(Calendar.MINUTE);
                    timePickerDialog = new TimePickerDialog(context, (timePicker, selectedHour, selectedMinute) -> {
                        addScheduleViewModel.editText.setText(AppUtils.numberToDecimal(selectedHour) + ":" + AppUtils.numberToDecimal(selectedMinute));
                        Timber.e(addScheduleViewModel.addScheduleRequest.getFromText());
                        Timber.e(addScheduleViewModel.addScheduleRequest.getToText());
                    }, hour, minute, true);//Yes 24 hour time
                    timePickerDialog.show();
                }else if(action.equals(Constants.ERROR)){
//                    Toast.makeText(context, ""+, Toast.LENGTH_SHORT).show();
                }
            }
        });
        dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {
                dialog.dismiss();
                dialogScheduleInterface.OnClickListenerContinue(dialog,null);
            }
        });
    }
}
