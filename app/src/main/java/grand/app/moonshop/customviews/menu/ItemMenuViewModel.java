package grand.app.moonshop.customviews.menu;

import android.graphics.drawable.Drawable;
import android.widget.RelativeLayout;

import androidx.databinding.Bindable;
import androidx.databinding.BindingAdapter;
import androidx.databinding.ObservableBoolean;
import androidx.lifecycle.MutableLiveData;
import grand.app.moonshop.base.ParentBaseObservableViewModel;
import grand.app.moonshop.models.app.AppMoon;


public class ItemMenuViewModel extends ParentBaseObservableViewModel {
    private MenuModel menuModel = null;
    private int position = 0;
    public int height = 0;
    public ObservableBoolean visibleCount = new ObservableBoolean(false);


    public ItemMenuViewModel(MenuModel menuModel, int position) {
        this.menuModel = menuModel;
        this.position = position;
        if(!menuModel.price.equals("")){
            int price = Integer.parseInt(menuModel.price);
            if(price > 0) visibleCount.set(true);
        }
    }

    public boolean getHeight(){
        return AppMoon.hasAuthorizedMenu(menuModel.id);
    }

    @BindingAdapter("height")
    public static void setHeight(RelativeLayout relativeLayout , boolean visible){
        if(!visible){
            relativeLayout.getLayoutParams().height = 0;
        }
    }

    public void submitMenu(){
        mMutableLiveDataBaseObservable.setValue(position);
    }

    public MutableLiveData<Object> getOnGetDataListener() {
        if(mMutableLiveDataBaseObservable==null)mMutableLiveDataBaseObservable = new MutableLiveData<>();
        return mMutableLiveDataBaseObservable;
    }



    @Bindable
    public String getTitle(){
        return menuModel.title;
    }

    @Bindable
    public String getPrice(){
        return menuModel.price;
    }


    @Bindable
    public Drawable getIcon(){
        return menuModel.icon;
    }

}
