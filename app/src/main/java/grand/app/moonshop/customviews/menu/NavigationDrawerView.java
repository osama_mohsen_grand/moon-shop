package grand.app.moonshop.customviews.menu;

/**
 * Created by mohamedatef on 12/30/18.
 */

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import grand.app.moonshop.R;
import grand.app.moonshop.customviews.actionbar.HomeActionBarView;
import grand.app.moonshop.databinding.LayoutNavigationDrawerBinding;
import grand.app.moonshop.models.app.AppMoon;
import grand.app.moonshop.models.user.profile.User;
import grand.app.moonshop.utils.helper.AppUtils;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.utils.MovementHelper;
import grand.app.moonshop.utils.images.ImageLoaderHelper;
import grand.app.moonshop.utils.resources.ResourceManager;
import grand.app.moonshop.utils.storage.user.UserHelper;
import grand.app.moonshop.viewmodels.bank.AddTransferGuideFragment;
import grand.app.moonshop.views.fragments.ads.AdsFragment;
import grand.app.moonshop.views.fragments.become.BeShopFragment;
import grand.app.moonshop.views.fragments.category.HomeShopFragment;
import grand.app.moonshop.views.fragments.chat.ChatListFragment;
import grand.app.moonshop.views.fragments.common.CreditFragment;
import grand.app.moonshop.views.fragments.contactAndSupport.ContactUsFragment;
import grand.app.moonshop.views.fragments.contactAndSupport.TechnicalSupportFragment;
import grand.app.moonshop.views.fragments.famous.FamousDetailsMainFragment;
import grand.app.moonshop.views.fragments.famous.FamousListFragment;
import grand.app.moonshop.views.fragments.institution.HomeInstitutionFragment;
import grand.app.moonshop.views.fragments.institution.InstitutionBranchesListFragment;
import grand.app.moonshop.views.fragments.notification.NotificationFragment;
import grand.app.moonshop.views.fragments.offers.OfferFragment;
import grand.app.moonshop.views.fragments.order.MyOrdersFragment;
import grand.app.moonshop.views.fragments.profile.ProfileFragment;
import grand.app.moonshop.views.fragments.profile.ProfileSocialFragment;
import grand.app.moonshop.views.fragments.reservation.clinic.ReservationMainFragment;
import grand.app.moonshop.views.fragments.review.ReviewFragment;
import grand.app.moonshop.views.fragments.settings.LanguageFragment;
import grand.app.moonshop.views.fragments.settings.SettingsFragment;
import grand.app.moonshop.views.fragments.shop.InfoServiceDetailsFragment;
import grand.app.moonshop.views.fragments.status.StatusFragment;
import timber.log.Timber;


public class NavigationDrawerView extends RelativeLayout {
    public MutableLiveData<Object> mutableLiveData = new MutableLiveData<>();
    public LayoutNavigationDrawerBinding layoutNavigationDrawerBinding;
    Context context;
    HomeActionBarView homeActionBarView;

    public NavigationDrawerView(Context context) {
        super(context);
        this.context = context;
        init();
    }

    public NavigationDrawerView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        init();

    }

    public NavigationDrawerView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs);
        this.context = context;
        init();
    }

    private void init() {


//                MovementHelper.addFragment(getContext(),new Fragment(),"fda");
        LayoutInflater layoutInflater = LayoutInflater.from(getContext());
        layoutNavigationDrawerBinding = DataBindingUtil.inflate(layoutInflater, R.layout.layout_navigation_drawer, this, true);
        setHeader();
        setList();
        setEvents();
    }

    public void setHeader() {
        //set menu header
        if (AppMoon.showHeaderMenu()) {
            User user = UserHelper.getUserDetails();
            layoutNavigationDrawerBinding.tvNavigationUsername.setText(user.name);
            layoutNavigationDrawerBinding.tvNavigationAsFamous.setText(AppMoon.getUserType().equals(Constants.TYPE_RESERVATION_BEAUTY) ?
                    ResourceManager.getString(R.string.as_beauty) : ResourceManager.getString(R.string.as_famous));
            ImageLoaderHelper.ImageLoaderLoad(getContext(), user.image, layoutNavigationDrawerBinding.civMainUserImage);
            if (AppMoon.isFamous()) {
                layoutNavigationDrawerBinding.tvNavigationAsFamous.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_famous_star_primary, 0, R.drawable.ic_check_correct_primary, 0);
                layoutNavigationDrawerBinding.tvNavigationAsShop.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_shop_menu_primary, 0, 0, 0);
            } else {
                layoutNavigationDrawerBinding.tvNavigationAsFamous.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_famous_star_primary, 0, 0, 0);
                layoutNavigationDrawerBinding.tvNavigationAsShop.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_shop_menu_primary, 0, R.drawable.ic_check_correct_primary, 0);
            }
            layoutNavigationDrawerBinding.tvNavigationAsFamous.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    AppMoon.changeType(context);
                }
            });
            layoutNavigationDrawerBinding.tvNavigationAsShop.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    AppMoon.changeType(context);
                }
            });

        } else {
            Timber.e("Error");
            layoutNavigationDrawerBinding.llNavigationHeader.setVisibility(GONE);
            layoutNavigationDrawerBinding.llNavigationHeaderLine.setVisibility(GONE);
        }
    }

    MenuAdapter menuAdapter = null;
    List<MenuModel> menu = new ArrayList<>();

    private void setList() {
        //Add List Here
        menuAdapter = new MenuAdapter(menu);
        menu.add(new MenuModel(Constants.HOME, context.getString(R.string.label_home), ResourceManager.getDrawable(R.drawable.ic_home_primary), ""));
        menu.add(new MenuModel(Constants.PROFILE, context.getString(R.string.profile), ResourceManager.getDrawable(R.drawable.ic_profile_primary), ""));
        menu.add(new MenuModel(Constants.MY_ADS, context.getString(R.string.my_ads), ResourceManager.getDrawable(R.drawable.ic_speaker), ""));
        //menu.add(new MenuModel(Constants.STORY, context.getString(R.string.story), ResourceManager.getDrawable(R.drawable.ic_story_primary), ""));
        //menu.add(new MenuModel(Constants.CHAT, context.getString(R.string.chat_history), ResourceManager.getDrawable(R.drawable.ic_chat_menu_primary), ""));
//        menu.add(new MenuModel(Constants.GALLERY, context.getString(R.string.gallery), ResourceManager.getDrawable(R.drawable.ic_gallery), ""));
        if(!AppMoon.isFamous())
            menu.add(new MenuModel(Constants.BRANCHES, context.getString(R.string.branches), ResourceManager.getDrawable(R.drawable.ic_branch), ""));


        if(!UserHelper.getUserDetails().type.equals(Constants.TYPE_INSTITUTIONS))
            menu.add(new MenuModel(Constants.ADD_INFO_SERVICE, context.getString(R.string.add_info_and_service), ResourceManager.getDrawable(R.drawable.ic_support), ""));

        menu.add(new MenuModel(Constants.BANK_TRANSFER, context.getString(R.string.add_transfer_guide), ResourceManager.getDrawable(R.drawable.ic_bank_transfer), ""));

        if (UserHelper.getUserDetails().type.equals(Constants.TYPE_RESERVATION_CLINIC) ||
                UserHelper.getUserDetails().type.equals(Constants.TYPE_RESERVATION_BEAUTY))
            menu.add(new MenuModel(Constants.RESERVATION, context.getString(R.string.reservation), ResourceManager.getDrawable(R.drawable.ic_calender), ""));
        if(!UserHelper.getUserDetails().type.equals(Constants.TYPE_INSTITUTIONS) &&
                !UserHelper.getUserDetails().type.equals(Constants.TYPE_COMMERCIALS) &&
                !UserHelper.getUserDetails().type.equals(Constants.TYPE_ADVERTISING) &&
                !UserHelper.getUserDetails().type.equals(Constants.TYPE_RESERVATION_CLINIC))
            menu.add(new MenuModel(Constants.ORDERS, context.getString(R.string.orders), ResourceManager.getDrawable(R.drawable.ic_followers_primary), ""));

        menu.add(new MenuModel(Constants.REVIEW, context.getString(R.string.reviews), ResourceManager.getDrawable(R.drawable.ic_review), ""));

        menu.add(new MenuModel(Constants.PROFILE_SOCIAL, context.getString(R.string.social_media), ResourceManager.getDrawable(R.drawable.ic_social), ""));
//    }
        menu.add(new MenuModel(Constants.FAMOUS, context.getString(R.string.famous_people), ResourceManager.getDrawable(R.drawable.ic_famous), ""));
        //menu.add(new MenuModel(Constants.CREDIT, context.getString(R.string.credit_card), ResourceManager.getDrawable(R.drawable.ic_credit_primary), ""));
        //menu.add(new MenuModel(Constants.NOTIFICATION, context.getString(R.string.label_notification), ResourceManager.getDrawable(R.drawable.ic_notification_primary), ""));
        menu.add(new MenuModel(Constants.PHOTOGRAPHER, context.getString(R.string.photographer_people), ResourceManager.getDrawable(R.drawable.ic_photographer), ""));
        menu.add(new MenuModel(Constants.SUPPORT, context.getString(R.string.help_and_support), ResourceManager.getDrawable(R.drawable.ic_help_and_support_primary), ""));
        menu.add(new MenuModel(Constants.PRIVACY_POLICY, context.getString(R.string.label_privacy_policy), ResourceManager.getDrawable(R.drawable.ic_be_privacy_primary), ""));
        menu.add(new MenuModel(Constants.TERMS, context.getString(R.string.terms_and_conditions), ResourceManager.getDrawable(R.drawable.ic_terms), ""));
        menu.add(new MenuModel(Constants.FAQ, context.getString(R.string.faq), ResourceManager.getDrawable(R.drawable.ic_faq), ""));
        menu.add(new MenuModel(Constants.LANG, context.getString(R.string.label_language), ResourceManager.getDrawable(R.drawable.ic_language_primary), ""));
        menu.add(new MenuModel(Constants.SHARE, context.getString(R.string.label_share_app), ResourceManager.getDrawable(R.drawable.ic_share_primary), ""));
        menu.add(new MenuModel(Constants.RATE, context.getString(R.string.rate_app), ResourceManager.getDrawable(R.drawable.ic_star_primary), ""));
        menu.add(new MenuModel(Constants.BE_SHOP, context.getString(R.string.be_shop), ResourceManager.getDrawable(R.drawable.ic_shop_menu_primary), ""));

        menu.add(new MenuModel(Constants.CONTACT_US, context.getString(R.string.contact_us), ResourceManager.getDrawable(R.drawable.ic_contact), ""));
        menu.add(new MenuModel(Constants.TECHNICAL_SUPPORT, context.getString(R.string.technical_support), ResourceManager.getDrawable(R.drawable.ic_support), ""));


        menu.add(new MenuModel(Constants.LOGOUT, context.getString(R.string.label_logout), ResourceManager.getDrawable(R.drawable.ic_logout_primary), ""));
        AppUtils.initVerticalRV(layoutNavigationDrawerBinding.rvNavigationDrawerList, context, 1);

        layoutNavigationDrawerBinding.rvNavigationDrawerList.setAdapter(menuAdapter);
    }

    private static final String TAG = "NavigationDrawerView";

//    public void updateChatCount(int count) {
//
//        if (menuAdapter != null) menuAdapter.updateItemCount(Constants.CHAT, count);
//    }

//    public void updateNotificationCount() {
//
//        if (menuAdapter != null) {
//            int count = UserHelper.getUserDetails().notifications_count;
//            menuAdapter.updateItemCount(Constants.NOTIFICATION, count);
//        }
//    }

    public void updateOrdersCount() {

        if (menuAdapter != null) {
            int count = UserHelper.getUserDetails().order_count;
            menuAdapter.updateItemCount(Constants.ORDERS, count);
        }
    }

    public void homePage() {
        Log.d(TAG,"homePage func");
        //home
        if (UserHelper.getUserDetails().type.equals(Constants.TYPE_PHOTOGRAPHER) || UserHelper.getUserDetails().type.equals(Constants.TYPE_FAMOUS_PEOPLE)) {
            MovementHelper.replaceFragmentTag(context, new FamousDetailsMainFragment(), Constants.HOME, "");
        } else if (UserHelper.getUserDetails().type.equals(Constants.TYPE_INSTITUTIONS)) {
            MovementHelper.replaceFragmentTag(context, new HomeInstitutionFragment(), Constants.HOME, "");
        } else
            MovementHelper.replaceFragmentTag(context, new HomeShopFragment(), Constants.HOME, "");
//        MovementHelper.popAllFragments(context);
        mutableLiveData.setValue(context.getString(R.string.label_home));
        homeActionBarView.setSearchVisibility(Constants.HOME);
    }

    public void reservationPage() {
        MovementHelper.replaceFragmentTag(context, new ReservationMainFragment(), Constants.RESERVATION, "");
        MovementHelper.popAllFragments(context);
        mutableLiveData.setValue(context.getString(R.string.reservation));
    }

    public MenuAdapter getAdapter() {
        return menuAdapter;
    }

    //home - profile - ads - story - chat_history - gallery - orders - reviews - famous_people - credit_card
    //label_notification - photographer_people - help_and_support - label_privacy_policy - label_language
    // label_share_app - rate_app - be_shop - label_logout
    private void setEvents() {

        menuAdapter.mutableLiveDataAdapter.observeForever(new Observer<Integer>() {
            @Override
            public void onChanged(@Nullable Integer position) {

                // select menu
                int pos = position;
                Timber.e("pos:" + pos);
                String id = menu.get(pos).id;
                Timber.e("language:" + id);
                Intent intent = null;
                if (id.equals(Constants.HOME)) {
                    Log.d(TAG,"homePage Menu");

                    homePage();
                } else if (id.equals(Constants.PROFILE)) {
                    //profile
                    MovementHelper.replaceFragmentTag(context, new ProfileFragment(), Constants.PROFILE, "");
                    MovementHelper.popAllFragments(context);
                    mutableLiveData.setValue(context.getString(R.string.profile));
                } else if (id.equals(Constants.STORY)) {
                    //story
                    storyPage();
                } else if (id.equals(Constants.CHAT)) {
                    MovementHelper.addFragmentTag(context, new ChatListFragment(), Constants.CHAT, "");
                    MovementHelper.popAllFragments(context);
                    mutableLiveData.setValue(context.getString(R.string.chat));
                    menuAdapter.updateChatCount(pos);
                    User user = UserHelper.getUserDetails();
                    user.chats_count = 0;
                    UserHelper.saveUserDetails(user);
                } else if (id.equals(Constants.GALLERY)) {
                    //gallery
                    MovementHelper.replaceFragmentTag(context, new OfferFragment(), Constants.GALLERY, "");
                    MovementHelper.popAllFragments(context);
                    mutableLiveData.setValue(context.getString(R.string.album));
                }else if (id.equals(Constants.BRANCHES)) {
                    //gallery
                    MovementHelper.replaceFragmentTag(context, new InstitutionBranchesListFragment(), Constants.BRANCHES, "");
                    MovementHelper.popAllFragments(context);
                    mutableLiveData.setValue(context.getString(R.string.branches));
                }else if (id.equals(Constants.ADD_INFO_SERVICE)) {
                    //add info service
                    MovementHelper.replaceFragmentTag(context, new InfoServiceDetailsFragment(), Constants.ADD_INFO_SERVICE, "");
                    MovementHelper.popAllFragments(context);
                    mutableLiveData.setValue(context.getString(R.string.add_info_and_service));
                }else if (id.equals(Constants.BANK_TRANSFER)) {
                    //add info service
                    MovementHelper.replaceFragmentTag(context, new AddTransferGuideFragment(), Constants.BANK_TRANSFER, "");
                    MovementHelper.popAllFragments(context);
                    mutableLiveData.setValue(context.getString(R.string.add_transfer_guide));
                }
                else if (id.equals(Constants.ORDERS)) {
                    //gallery
                    User user = UserHelper.getUserDetails();
                    user.order_count = 0;
                    UserHelper.saveUserDetails(user);
                    updateOrdersCount();
                    MovementHelper.replaceFragmentTag(context, new MyOrdersFragment(), Constants.ORDERS, "");
                    MovementHelper.popAllFragments(context);
                    mutableLiveData.setValue(context.getString(R.string.orders));
                } else if (id.equals(Constants.RESERVATION)) {
                    //gallery
                    reservationPage();
                } else if (id.equals(Constants.MY_ADS)) {
                    myAdsPage();
                } else if (id.equals(Constants.CREDIT)) {
                    MovementHelper.replaceFragmentTag(context, new CreditFragment(), Constants.CREDIT, "");
                    MovementHelper.popAllFragments(context);
                    mutableLiveData.setValue(context.getString(R.string.credit_card));
                } else if (id.equals(Constants.NOTIFICATION)) {
                    //notifications
                    NotificationFragment notificationFragment = new NotificationFragment();
                    MovementHelper.replaceFragmentTag(context, notificationFragment, Constants.NOTIFICATION, "");
                    MovementHelper.popAllFragments(context);
                    mutableLiveData.setValue(context.getString(R.string.label_notification));

                } else if (id.equals(Constants.REVIEW)) {
                    //reviews
                    ReviewFragment reviewFragment = new ReviewFragment();
                    MovementHelper.replaceFragmentTag(context, reviewFragment, Constants.REVIEW, "");
                    MovementHelper.popAllFragments(context);
                    mutableLiveData.setValue(context.getString(R.string.reviews));

                } else if (id.equals(Constants.PROFILE_SOCIAL)) {
                    //reviews
                    ProfileSocialFragment fragment = new ProfileSocialFragment();
                    Bundle bundle = new Bundle();
                    bundle.putString(Constants.FROM,Constants.MENU);
                    fragment.setArguments(bundle);
                    MovementHelper.replaceFragmentTag(context, fragment, Constants.PROFILE_SOCIAL, "");
                    MovementHelper.popAllFragments(context);
                    mutableLiveData.setValue(context.getString(R.string.social_media));

                }else if (id.equals(Constants.FAMOUS)) {
                    //famous
                    FamousListFragment famousFragment = new FamousListFragment();
                    Bundle bundle = new Bundle();
                    bundle.putInt(Constants.TYPE, 4);
                    famousFragment.setArguments(bundle);
                    MovementHelper.replaceFragmentTag(context, famousFragment, Constants.FAMOUS, "");
                    MovementHelper.popAllFragments(context);
                    mutableLiveData.setValue(context.getString(R.string.famous_people));

                } else if (id.equals(Constants.PHOTOGRAPHER)) {
                    //photographer

                    FamousListFragment famousFragment = new FamousListFragment();
                    Bundle bundle = new Bundle();
                    bundle.putInt(Constants.TYPE, 5);
                    famousFragment.setArguments(bundle);
                    MovementHelper.replaceFragmentTag(context, famousFragment, Constants.FAMOUS, "");
                    MovementHelper.popAllFragments(context);
                    mutableLiveData.setValue(context.getString(R.string.photographer_people));

                } else if (id.equals(Constants.SUPPORT)) {
                    //support
                    SettingsFragment settingsFragment = new SettingsFragment();
                    Bundle bundle = new Bundle();
                    bundle.putInt(Constants.TYPE, Constants.TYPE_SUPPORT);
                    settingsFragment.setArguments(bundle);
                    MovementHelper.replaceFragmentTag(context, settingsFragment, Constants.SETTINGS, "");
                    MovementHelper.popAllFragments(context);
                    mutableLiveData.setValue(context.getString(R.string.help_and_support));
                } else if (id.equals(Constants.PRIVACY_POLICY)) {
                    //privacy
                    goToSettings(1,context.getString(R.string.label_privacy_policy));
                }else if (id.equals(Constants.TERMS)) {
                    //privacy
                    goToSettings(2,context.getString(R.string.terms_and_conditions));
                }else if (id.equals(Constants.FAQ)) {
                    //privacy
                    goToSettings(3,context.getString(R.string.faq));
                } else if (id.equals(Constants.LANG)) {
                    //language
                    MovementHelper.replaceFragmentTag(context, new LanguageFragment(), Constants.LANGUAGE, "");
                    MovementHelper.popAllFragments(context);
                    mutableLiveData.setValue(context.getString(R.string.label_language));
                } else if (id.equals(Constants.RATE)) {
                    //rate app
                    MovementHelper.startWebPage(context, AppUtils.getPlayStoreLink(context));
                } else if (id.equals(Constants.SHARE)) {
                    //share app
                    AppUtils.shareUrl(AppUtils.getPlayStoreLink(context), context);
                } else if (id.equals(Constants.BE_SHOP)) {
                    //logout
                    MovementHelper.replaceFragmentTag(getContext(), new BeShopFragment(), Constants.BE_SHOP, "");
                    MovementHelper.popAllFragments(context);
                    mutableLiveData.setValue(context.getString(R.string.be_shop));
                } else if (id.equals(Constants.CONTACT_US)) {
                    //logout
                    MovementHelper.replaceFragmentTag(context, new ContactUsFragment(), Constants.CONTACT_US, "");
                    MovementHelper.popAllFragments(context);
                    mutableLiveData.setValue(context.getString(R.string.contact_us));
                    homeActionBarView.setSearchVisibility(Constants.CONTACT_US);
                } else if (id.equals(Constants.TECHNICAL_SUPPORT)) {
                    //logout
                    MovementHelper.replaceFragmentTag(context, new TechnicalSupportFragment(), Constants.TECHNICAL_SUPPORT, "");
                    MovementHelper.popAllFragments(context);
                    mutableLiveData.setValue(context.getString(R.string.technical_support));
                    homeActionBarView.setSearchVisibility(Constants.TECHNICAL_SUPPORT);
                } else if (id.equals(Constants.LOGOUT)) {
                    //logout
                    mutableLiveData.setValue(Constants.LOGOUT);
                }
                homeActionBarView.setSearchVisibility(id);
//                layoutNavigationDrawerBinding.svMenu.fullScroll(ScrollView.FOCUS_UP);
            }
        });

    }

    private void goToSettings(int type,String name) {
        SettingsFragment settingsFragment = new SettingsFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(Constants.TYPE,type);
        settingsFragment.setArguments(bundle);
        MovementHelper.replaceFragmentTag(context, settingsFragment, Constants.SETTINGS, "");
        MovementHelper.popAllFragments(context);
        mutableLiveData.setValue(name);
    }

    public void myAdsPage() {
        MovementHelper.replaceFragmentTag(context, new AdsFragment(), Constants.MY_ADS, "");
        MovementHelper.popAllFragments(context);
        mutableLiveData.setValue(context.getString(R.string.my_ads));
    }

    public void storyPage(){
        MovementHelper.addFragmentWithDisAllowBackTag(context, new StatusFragment(), Constants.STORY, "");
        MovementHelper.popAllFragments(context);
        mutableLiveData.setValue(context.getString(R.string.story));
    }
    public void setActionBar(HomeActionBarView homeActionBarView) {
        this.homeActionBarView = homeActionBarView;
    }
}

/*

 */
