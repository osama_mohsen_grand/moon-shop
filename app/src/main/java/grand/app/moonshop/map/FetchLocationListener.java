package grand.app.moonshop.map;

import android.location.Location;

public interface FetchLocationListener {
    public void location(Location location);
}
