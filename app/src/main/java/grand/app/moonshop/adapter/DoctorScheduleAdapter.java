package grand.app.moonshop.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.RecyclerView;
import grand.app.moonshop.R;
import grand.app.moonshop.databinding.ItemDoctorScheduleBinding;
import grand.app.moonshop.databinding.ItemDoctorScheduleBinding;
import grand.app.moonshop.models.reservation.doctor.schedule.Day;
import grand.app.moonshop.viewmodels.reservation.doctors.ItemDoctorScheduleViewModel;
import grand.app.moonshop.viewmodels.reservation.doctors.ItemDoctorScheduleViewModel;


public class DoctorScheduleAdapter extends RecyclerView.Adapter<DoctorScheduleAdapter.DayView> {
    private List<Day> days;
    private LayoutInflater layoutInflater;
    private int position = -1;
    public MutableLiveData<Object> mMutableLiveData  = new MutableLiveData<>();;

    public DoctorScheduleAdapter(List<Day> days) {
        this.days = days;
    }


    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @NonNull
    @Override
    public DayView onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        if(layoutInflater == null){
            layoutInflater  = LayoutInflater.from(parent.getContext());
        }
        ItemDoctorScheduleBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.item_doctor_schedule,parent,false);
        return new DayView(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull DayView holder, final int position) {
        ItemDoctorScheduleViewModel itemDayViewModel = new ItemDoctorScheduleViewModel(days.get(position),position);
        holder.itemDoctorScheduleBinding.setItemDoctorScheduleViewModel(itemDayViewModel);
        setEvent(itemDayViewModel);
    }

    private void setEvent(ItemDoctorScheduleViewModel itemDoctorScheduleViewModel) {
        itemDoctorScheduleViewModel.mMutableLiveData.observeForever(new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object aVoid) {
                mMutableLiveData.setValue(aVoid);
            }
        });
    }


    @Override
    public int getItemCount() {
        return days.size();
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
        notifyDataSetChanged();
    }

    public void remove(int item_delete_position) {
        days.remove(item_delete_position);
        notifyDataSetChanged();
    }

    public void updateAll(List<Day> days) {
        this.days = days;
        notifyDataSetChanged();
    }

    public void update(int pos, Day day) {
        days.set(pos,day);
        notifyItemChanged(pos);
    }

    public class DayView extends RecyclerView.ViewHolder{

        private ItemDoctorScheduleBinding itemDoctorScheduleBinding;
        public DayView(@NonNull ItemDoctorScheduleBinding itemDoctorScheduleBinding) {
            super(itemDoctorScheduleBinding.getRoot());
            this.itemDoctorScheduleBinding = itemDoctorScheduleBinding;
        }
    }
}
