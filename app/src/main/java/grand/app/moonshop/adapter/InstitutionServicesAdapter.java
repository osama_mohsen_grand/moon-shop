package grand.app.moonshop.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.RecyclerView;
import grand.app.moonshop.R;
import grand.app.moonshop.databinding.ItemInstitutionServiceBinding;
import grand.app.moonshop.models.base.IdNameImage;
import grand.app.moonshop.viewmodels.institution.ItemInstitutionServiceViewModel;


public class InstitutionServicesAdapter extends RecyclerView.Adapter<InstitutionServicesAdapter.ServiceView> {
    private List<IdNameImage> services;
    private LayoutInflater layoutInflater;
    private int position = -1;
    private boolean allowDelete = false;
    public MutableLiveData<Object> mMutableLiveData  = new MutableLiveData<>();;

    public InstitutionServicesAdapter(List<IdNameImage> services,boolean allowDelete) {
        this.services = services;
        this.allowDelete = allowDelete;
    }


    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @NonNull
    @Override
    public ServiceView onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        if(layoutInflater == null){
            layoutInflater  = LayoutInflater.from(parent.getContext());
        }
        ItemInstitutionServiceBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.item_institution_service,parent,false);
        return new ServiceView(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ServiceView holder, final int position) {
        ItemInstitutionServiceViewModel itemInstitutionServiceViewModel = new ItemInstitutionServiceViewModel(services.get(position),position,allowDelete);
        holder.itemInstitutionServiceBinding.setItemInstitutionServiceViewModel(itemInstitutionServiceViewModel);
        setEvent(itemInstitutionServiceViewModel);
    }

    private void setEvent(ItemInstitutionServiceViewModel itemInstitutionServiceViewModel) {
        itemInstitutionServiceViewModel.mMutableLiveData.observeForever(new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object aVoid) {
                mMutableLiveData.setValue(aVoid);
            }
        });
    }


    @Override
    public int getItemCount() {
        return services.size();
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
        notifyDataSetChanged();
    }

    public void remove(int service_delete_position) {
        services.remove(service_delete_position);
        notifyDataSetChanged();
    }

    public class ServiceView extends RecyclerView.ViewHolder{

        private ItemInstitutionServiceBinding itemInstitutionServiceBinding;
        public ServiceView(@NonNull ItemInstitutionServiceBinding itemInstitutionServiceBinding) {
            super(itemInstitutionServiceBinding.getRoot());
            this.itemInstitutionServiceBinding = itemInstitutionServiceBinding;
        }
    }
}
