package grand.app.moonshop.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.RecyclerView;
import grand.app.moonshop.R;
import grand.app.moonshop.databinding.ItemSuggestBinding;
import grand.app.moonshop.models.base.IdName;
import grand.app.moonshop.viewmodels.suggest.ItemSuggestViewModel;

public class SuggestAdapter extends RecyclerView.Adapter<SuggestAdapter.IdNameView> {
    private List<IdName> suggestions;
    private LayoutInflater layoutInflater;
    private int position = -1;
    public MutableLiveData<Object> mMutableLiveData  = new MutableLiveData<>();;

    public SuggestAdapter(List<IdName> suggestions) {
        this.suggestions = suggestions;
    }


    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @NonNull
    @Override
    public IdNameView onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        if(layoutInflater == null){
            layoutInflater  = LayoutInflater.from(parent.getContext());
        }
        ItemSuggestBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.item_suggest,parent,false);
        return new IdNameView(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull SuggestAdapter.IdNameView holder, final int position) {
        ItemSuggestViewModel itemSuggestViewModel = new ItemSuggestViewModel(suggestions.get(position),position);
        holder.itemSuggestBinding.setItemSuggestViewModel(itemSuggestViewModel);
        setEvent(itemSuggestViewModel);
    }

    private void setEvent(ItemSuggestViewModel itemSuggestViewModel) {
        itemSuggestViewModel.mMutableLiveData.observeForever(new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object aVoid) {
                mMutableLiveData.setValue(aVoid);
            }
        });
    }

    @Override
    public int getItemCount() {
        return suggestions.size();
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
        notifyDataSetChanged();
    }

    public void updateAll(List<IdName> suggestions) {
        this.suggestions = suggestions;
        notifyDataSetChanged();
    }

    public void clear() {
        suggestions.clear();
        notifyDataSetChanged();
    }

    public class IdNameView extends RecyclerView.ViewHolder{

        private ItemSuggestBinding itemSuggestBinding;
        public IdNameView(@NonNull ItemSuggestBinding itemSuggestBinding) {
            super(itemSuggestBinding.getRoot());
            this.itemSuggestBinding = itemSuggestBinding;
        }
    }
}
