package grand.app.moonshop.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.RecyclerView;
import grand.app.moonshop.R;
import grand.app.moonshop.databinding.ItemDoctorListBinding;
import grand.app.moonshop.databinding.ItemDoctorListBinding;
import grand.app.moonshop.models.reservation.doctor.DoctorListModel;
import grand.app.moonshop.viewmodels.reservation.doctors.ItemDoctorListViewModel;


public class DoctorsAdapter extends RecyclerView.Adapter<DoctorsAdapter.DoctorListModelView> {
    private List<DoctorListModel> doctors;
    private LayoutInflater layoutInflater;
    private int position = -1;
    public MutableLiveData<Object> mMutableLiveData  = new MutableLiveData<>();;

    public DoctorsAdapter(List<DoctorListModel> doctors) {
        this.doctors = doctors;
    }


    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @NonNull
    @Override
    public DoctorListModelView onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        if(layoutInflater == null){
            layoutInflater  = LayoutInflater.from(parent.getContext());
        }
        ItemDoctorListBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.item_doctor_list,parent,false);
        return new DoctorListModelView(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull DoctorListModelView holder, final int position) {
        ItemDoctorListViewModel itemDoctorListModelViewModel = new ItemDoctorListViewModel(doctors.get(position),position);
        holder.itemDoctorListBinding.setItemDoctorListViewModel(itemDoctorListModelViewModel);
        setEvent(itemDoctorListModelViewModel);
    }

    private void setEvent(ItemDoctorListViewModel itemDoctorListModelViewModel) {
        itemDoctorListModelViewModel.mMutableLiveData.observeForever(new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object aVoid) {
                mMutableLiveData.setValue(aVoid);
            }
        });
    }


    @Override
    public int getItemCount() {
        return doctors.size();
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
        notifyDataSetChanged();
    }

    public void remove(int item_delete_position) {
        doctors.remove(item_delete_position);
        notifyDataSetChanged();
    }

    public class DoctorListModelView extends RecyclerView.ViewHolder{

        private ItemDoctorListBinding itemDoctorListBinding;
        public DoctorListModelView(@NonNull ItemDoctorListBinding itemDoctorListBinding) {
            super(itemDoctorListBinding.getRoot());
            this.itemDoctorListBinding = itemDoctorListBinding;
        }
    }
}
