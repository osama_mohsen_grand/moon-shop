package grand.app.moonshop.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.RecyclerView;
import grand.app.moonshop.R;
import grand.app.moonshop.databinding.ItemFilterAdsBinding;
import grand.app.moonshop.databinding.ItemFilterAdsBinding;
import grand.app.moonshop.models.app.Mutable;
import grand.app.moonshop.models.base.IdNameDescription;
import grand.app.moonshop.viewmodels.ads.ItemFilterAdsViewModel;
import timber.log.Timber;


public class FilterAdapter extends RecyclerView.Adapter<FilterAdapter.AlbumView> {
    public List<IdNameDescription> idNameDescriptions;
    private LayoutInflater layoutInflater;
    private int position = -1;
    public MutableLiveData<Object> mMutableLiveData  = new MutableLiveData<>();;
    public ArrayList<Integer> selected = new ArrayList<>();
    boolean allowMultiple;

    public FilterAdapter(List<IdNameDescription> idNameDescriptions,boolean allowMultiple) {
        this.idNameDescriptions = idNameDescriptions;
        this.allowMultiple = allowMultiple;
    }


    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @NonNull
    @Override
    public AlbumView onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        if(layoutInflater == null){
            layoutInflater  = LayoutInflater.from(parent.getContext());
        }
        ItemFilterAdsBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.item_filter_ads,parent,false);
        return new AlbumView(binding);
    }

    public boolean isExist(int id){
        Timber.e("selected:"+selected.contains(id));
        return selected.contains(id);
    }

    @Override
    public void onBindViewHolder(@NonNull AlbumView holder, final int position) {

        ItemFilterAdsViewModel itemFilterAdsViewModel = new ItemFilterAdsViewModel(idNameDescriptions.get(position),position,isExist(idNameDescriptions.get(position).id));
        holder.itemFilterAdsBinding.setItemFilterAdsViewModel(itemFilterAdsViewModel);
        setEvent(itemFilterAdsViewModel);
    }

    private void setEvent(ItemFilterAdsViewModel itemFilterAdsViewModel) {
        itemFilterAdsViewModel.mMutableLiveData.observeForever(new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object aVoid) {
                Mutable mutable = (Mutable) aVoid;
                int id = idNameDescriptions.get(mutable.position).id;
                if(allowMultiple) {
                    if (isExist(id)) {
                        selected.remove(selected.indexOf(id));
                    } else {
                        selected.add(id);
                    }
                }else{
                    selected.clear();
                    selected.add(id);
                }
                notifyDataSetChanged();
                mMutableLiveData.setValue(mutable);
            }
        });
    }


    @Override
    public int getItemCount() {
        return idNameDescriptions.size();
    }

    public int getPosition() {
        return position;
    }

    public void reset(){
        selected.clear();
        notifyDataSetChanged();
    }

    public void setPosition(int position) {
        this.position = position;
        notifyDataSetChanged();
    }

    public void update(List<IdNameDescription> idNameDescriptions) {
        this.idNameDescriptions = idNameDescriptions;
        notifyDataSetChanged();
    }

    public class AlbumView extends RecyclerView.ViewHolder{

        private ItemFilterAdsBinding itemFilterAdsBinding;
        public AlbumView(@NonNull ItemFilterAdsBinding itemFilterAdsBinding) {
            super(itemFilterAdsBinding.getRoot());
            this.itemFilterAdsBinding = itemFilterAdsBinding;
        }
    }
}
