package grand.app.moonshop.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.RecyclerView;
import grand.app.moonshop.R;
import grand.app.moonshop.databinding.ItemServiceBinding;
import grand.app.moonshop.models.service.Service;
import grand.app.moonshop.viewmodels.service.ItemServiceViewModel;

public class ServiceAdapter extends RecyclerView.Adapter<ServiceAdapter.ServiceView> {
    private List<Service> services;
    private LayoutInflater layoutInflater;
    private int position = -1;
    private  ArrayList<Integer> selected;
    public MutableLiveData<Object> mMutableLiveData  = new MutableLiveData<>();;

    public ServiceAdapter(List<Service> services, ArrayList<Integer> selected) {
        this.services = services;
        this.selected = selected;
    }


    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @NonNull
    @Override
    public ServiceView onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        if(layoutInflater == null){
            layoutInflater  = LayoutInflater.from(parent.getContext());
        }
        ItemServiceBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.item_service,parent,false);
        return new ServiceView(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ServiceView holder, final int position) {
        ItemServiceViewModel itemServiceViewModel = new ItemServiceViewModel(services.get(position),position,selected.contains(services.get(position).id));
        holder.itemServiceBinding.setItemServiceViewModel(itemServiceViewModel);
        setEvent(itemServiceViewModel);
    }

    private void setEvent(ItemServiceViewModel itemServiceViewModel) {
        itemServiceViewModel.mMutableLiveData.observeForever(new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object aVoid) {
                mMutableLiveData.setValue(aVoid);
            }
        });
    }


    @Override
    public int getItemCount() {
        return services.size();
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
        notifyDataSetChanged();
    }

    public void update(ArrayList<Integer> selected) {
        this.selected = new ArrayList<>(selected);
        notifyDataSetChanged();
    }

    public class ServiceView extends RecyclerView.ViewHolder{

        private ItemServiceBinding itemServiceBinding;
        public ServiceView(@NonNull ItemServiceBinding itemServiceBinding) {
            super(itemServiceBinding.getRoot());
            this.itemServiceBinding = itemServiceBinding;
        }
    }
}
