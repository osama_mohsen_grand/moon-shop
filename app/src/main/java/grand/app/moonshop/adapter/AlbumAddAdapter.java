package grand.app.moonshop.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.RecyclerView;
import grand.app.moonshop.R;
import grand.app.moonshop.databinding.ItemAddPhotoOrVideoBinding;
import grand.app.moonshop.viewmodels.famous.add.ItemAddPhotoOrVideoViewModel;
import grand.app.moonshop.vollyutils.VolleyFileObject;


public class AlbumAddAdapter extends RecyclerView.Adapter<AlbumAddAdapter.AddPhotoOrVideoView> {
    public List<VolleyFileObject> files;
    public String type;
    private LayoutInflater layoutInflater;
    private int position = -1;
    public MutableLiveData<Object> mMutableLiveData  = new MutableLiveData<>();;

    public AlbumAddAdapter(String type, List<VolleyFileObject> files) {
        this.type = type;
        this.files = files;
    }


    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @NonNull
    @Override
    public AddPhotoOrVideoView onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        if(layoutInflater == null){
            layoutInflater  = LayoutInflater.from(parent.getContext());
        }
        ItemAddPhotoOrVideoBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.item_add_photo_or_video,parent,false);
        return new AddPhotoOrVideoView(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull AddPhotoOrVideoView holder, final int position) {
        ItemAddPhotoOrVideoViewModel itemAddPhotoOrVideoViewModel = new ItemAddPhotoOrVideoViewModel(type,files.get(position),position);
        holder.itemPackageBinding.setItemFamousAddAlbumViewModel(itemAddPhotoOrVideoViewModel);
        setEvent(itemAddPhotoOrVideoViewModel);
    }

    private void setEvent(ItemAddPhotoOrVideoViewModel itemAddPhotoOrVideoViewModel) {
        itemAddPhotoOrVideoViewModel.mMutableLiveData.observeForever(new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object aVoid) {
                mMutableLiveData.setValue(aVoid);
            }
        });
    }


    @Override
    public int getItemCount() {
        return files.size();
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
        notifyDataSetChanged();
    }

    public void update(List<VolleyFileObject> files) {
        this.files = files;
        notifyDataSetChanged();
    }

    public class AddPhotoOrVideoView extends RecyclerView.ViewHolder{

        private ItemAddPhotoOrVideoBinding itemPackageBinding;
        public AddPhotoOrVideoView(@NonNull ItemAddPhotoOrVideoBinding itemPackageBinding) {
            super(itemPackageBinding.getRoot());
            this.itemPackageBinding = itemPackageBinding;
        }
    }
}
