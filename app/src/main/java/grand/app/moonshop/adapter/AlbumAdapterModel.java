package grand.app.moonshop.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.RecyclerView;
import grand.app.moonshop.R;
import grand.app.moonshop.databinding.ItemAlbumModelBinding;
import grand.app.moonshop.databinding.ItemAlbumModelBinding;
import grand.app.moonshop.models.album.AlbumModel;
import grand.app.moonshop.models.app.Mutable;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.utils.resources.ResourceManager;
import grand.app.moonshop.utils.storage.user.UserHelper;
import grand.app.moonshop.viewmodels.famous.ItemAlbumModelViewModel;
import grand.app.moonshop.viewmodels.famous.ItemAlbumViewModel;
import grand.app.moonshop.views.activities.BaseActivity;


public class AlbumAdapterModel extends RecyclerView.Adapter<AlbumAdapterModel.AlbumView> {
    public List<AlbumModel> imageVideos;
    private LayoutInflater layoutInflater;
    private int position = -1;
    public MutableLiveData<Object> mMutableLiveData  = new MutableLiveData<>();;
    public boolean allowAction;

    public AlbumAdapterModel(List<AlbumModel> imageVideos,boolean allowAction) {
        this.imageVideos = imageVideos;
        this.allowAction = allowAction;
    }


    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @NonNull
    @Override
    public AlbumView onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        if(layoutInflater == null){
            layoutInflater  = LayoutInflater.from(parent.getContext());
        }
        ItemAlbumModelBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.item_album_model,parent,false);
        return new AlbumView(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull AlbumView holder, final int position) {
        ItemAlbumModelViewModel itemAlbumViewModel = new ItemAlbumModelViewModel(imageVideos.get(position),position,allowAction);
        holder.itemAlbumBinding.setViewModel(itemAlbumViewModel);
        setEvent(holder.itemAlbumBinding.getRoot().getRootView().getContext(), itemAlbumViewModel);
    }

    private void setEvent(Context context , ItemAlbumModelViewModel ItemAlbumViewModel) {
        ItemAlbumViewModel.mMutableLiveData.observeForever(new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object aVoid) {
                mMutableLiveData.setValue(aVoid);
            }
        });
    }


    @Override
    public int getItemCount() {
        return imageVideos.size();
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
        notifyDataSetChanged();
    }

    public void update(List<AlbumModel> imageVideos) {
        this.imageVideos = imageVideos;
        notifyDataSetChanged();
    }

    public void remove(int category_delete_position) {
        imageVideos.remove(category_delete_position);
        notifyDataSetChanged();
    }

    public class AlbumView extends RecyclerView.ViewHolder{

        private ItemAlbumModelBinding itemAlbumBinding;
        public AlbumView(@NonNull ItemAlbumModelBinding itemAlbumBinding) {
            super(itemAlbumBinding.getRoot());
            this.itemAlbumBinding = itemAlbumBinding;
        }
    }
}
