package grand.app.moonshop.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.RecyclerView;
import grand.app.moonshop.R;
import grand.app.moonshop.databinding.ItemShopAblumBinding;
import grand.app.moonshop.models.ads.Datum;
import grand.app.moonshop.models.app.Mutable;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.viewmodels.shop.ItemShopAlbumViewModel;
import grand.app.moonshop.views.activities.BaseActivity;
import grand.app.moonshop.views.fragments.famous.FamousAlbumMainDetailsFragment;


public class ShopAlbumAdapter extends RecyclerView.Adapter<ShopAlbumAdapter.ShopAlbumView> {
    private List<Datum> data;
    private LayoutInflater layoutInflater;
    public int famous_id = -1;
    public MutableLiveData<Object> mMutableLiveData = new MutableLiveData<>();
    ;

    public ShopAlbumAdapter(List<Datum> data) {
        this.data = data;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @NonNull
    @Override
    public ShopAlbumView onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        if (layoutInflater == null) {
            layoutInflater = LayoutInflater.from(parent.getContext());
        }
        ItemShopAblumBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.item_shop_ablum, parent, false);
        return new ShopAlbumView(binding);
    }



    @Override
    public void onBindViewHolder(@NonNull ShopAlbumView holder, final int position) {
        ItemShopAlbumViewModel itemViewModel = new ItemShopAlbumViewModel(data.get(position), position);
        holder.itemAdsCompanyBinding.setViewModel(itemViewModel);
        setEvent(holder.itemAdsCompanyBinding.getRoot().getContext() , itemViewModel);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }


    private void setEvent(Context context , ItemShopAlbumViewModel itemViewModel) {
        itemViewModel.mMutableLiveData.observeForever(new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object aVoid) {
                Mutable mutable = (Mutable) aVoid;
                if(mutable.type.equals(Constants.ALBUM)) {
//                    Intent intent = new Intent(context, BaseActivity.class);
//                    intent.putExtra(Constants.PAGE, Constants.ALBUM_DETAILS);
//                    Bundle bundle = new Bundle();
//                    intent.putExtra(Constants.NAME_BAR, data.get(itemViewModel.position).name);
//                    intent.putExtra(Constants.BUNDLE, bundle);
//                    context.startActivity(intent);


                    Intent intent = new Intent(context, BaseActivity.class);
                    Bundle bundle = new Bundle();
                    intent.putExtra(Constants.PAGE, FamousAlbumMainDetailsFragment.class.getName());
                    intent.putExtra(Constants.NAME_BAR, data.get(itemViewModel.position).name);
                    bundle.putSerializable(Constants.ALBUM_ADS, data.get(itemViewModel.position));
                    bundle.putInt(Constants.ID, data.get(itemViewModel.position).id);
//                    bundle.putString(Constants.TYPE, type);
                    bundle.putString(Constants.TAB, "1");
//                }else{
//                    intent.putExtra(Constants.PAGE, FamousAlbumMainDetailsFragment.class.getName());
//                    bundle.putInt(Constants.ID, imageVideos.get(mutable.position).id);
//                    bundle.putString(Constants.TYPE, type);
//                    bundle.putString(Constants.TAB, "1");
//                }
                    intent.putExtra(Constants.BUNDLE, bundle);
                    context.startActivity(intent);


                }
            }
        });
    }

    public void update(List<Datum> data) {
        this.data = data;
        notifyDataSetChanged();
    }

    public class ShopAlbumView extends RecyclerView.ViewHolder {

        private ItemShopAblumBinding itemAdsCompanyBinding;

        public ShopAlbumView(@NonNull ItemShopAblumBinding itemAdsCompanyBinding) {
            super(itemAdsCompanyBinding.getRoot());
            this.itemAdsCompanyBinding = itemAdsCompanyBinding;
        }
    }
}
