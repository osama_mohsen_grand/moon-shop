package grand.app.moonshop.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.RecyclerView;
import grand.app.moonshop.R;
import grand.app.moonshop.databinding.ItemCompanyAdsCategoryBinding;
import grand.app.moonshop.models.album.Category;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.viewmodels.ads.ItemCompanyAdsCategoryViewModel;
import grand.app.moonshop.views.activities.BaseActivity;
import grand.app.moonshop.views.fragments.shop.ShopAlbumFragment;


public class CompanyAdsCategoriesAdapter extends RecyclerView.Adapter<CompanyAdsCategoriesAdapter.AdsCompanyView> {
    private List<Category> categories;
    private LayoutInflater layoutInflater;
    public int famous_id = -1;
    public MutableLiveData<Object> mMutableLiveData = new MutableLiveData<>();
    ;

    public CompanyAdsCategoriesAdapter(List<Category> categories) {
        this.categories = categories;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @NonNull
    @Override
    public AdsCompanyView onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        if (layoutInflater == null) {
            layoutInflater = LayoutInflater.from(parent.getContext());
        }
        ItemCompanyAdsCategoryBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.item_company_ads_category, parent, false);
        return new AdsCompanyView(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull AdsCompanyView holder, final int position) {
        ItemCompanyAdsCategoryViewModel itemViewModel = new ItemCompanyAdsCategoryViewModel(categories.get(position), position);
        holder.itemAdsCompanyBinding.setViewModel(itemViewModel);
        setEvent(holder.itemAdsCompanyBinding.getRoot().getContext() , itemViewModel);
    }

    @Override
    public int getItemCount() {
        return categories.size();
    }

    private static final String TAG = "CompanyAdsCategoriesAda";

    private void setEvent(Context context , ItemCompanyAdsCategoryViewModel itemViewModel) {
        itemViewModel.mMutableLiveData.observeForever(new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object aVoid) {
                Log.d(TAG,"adapter");
                Intent intent = new Intent(context, BaseActivity.class);
                intent.putExtra(Constants.PAGE, ShopAlbumFragment.class.getName());
                Bundle bundle = new Bundle();
                bundle.putInt(Constants.ID,categories.get(itemViewModel.position).id);
                bundle.putInt(Constants.FAMOUS_ID,famous_id);
                intent.putExtra(Constants.NAME_BAR, categories.get(itemViewModel.position).name);
                intent.putExtra(Constants.BUNDLE,bundle);
                context.startActivity(intent);
            }
        });
    }

    public void update(List<Category> categories) {
        this.categories = categories;
        notifyDataSetChanged();
    }

    public class AdsCompanyView extends RecyclerView.ViewHolder {

        private ItemCompanyAdsCategoryBinding itemAdsCompanyBinding;

        public AdsCompanyView(@NonNull ItemCompanyAdsCategoryBinding itemAdsCompanyBinding) {
            super(itemAdsCompanyBinding.getRoot());
            this.itemAdsCompanyBinding = itemAdsCompanyBinding;
        }
    }
}
