package grand.app.moonshop.adapter;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.RecyclerView;
import grand.app.moonshop.R;
import grand.app.moonshop.databinding.ItemAlbumImageBinding;
import grand.app.moonshop.models.app.Mutable;
import grand.app.moonshop.models.base.IdNameImage;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.viewmodels.famous.details.ItemAlbumImageViewModel;
import grand.app.moonshop.views.activities.BaseActivity;
import timber.log.Timber;


public class AlbumDetailsAdapter extends RecyclerView.Adapter<AlbumDetailsAdapter.AlbumImagesView> {
    public List<IdNameImage> idNameImages;
    private LayoutInflater layoutInflater;
    private int position = -1;
    public MutableLiveData<Object> mMutableLiveData  = new MutableLiveData<>();;
    public Boolean defaultSubmit = false;

    public AlbumDetailsAdapter(List<IdNameImage> idNameImages) {
        this.idNameImages = idNameImages;
    }


    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @NonNull
    @Override
    public AlbumImagesView onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        if(layoutInflater == null){
            layoutInflater  = LayoutInflater.from(parent.getContext());
        }
        ItemAlbumImageBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.item_album_image,parent,false);
        return new AlbumImagesView(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull AlbumImagesView holder, final int position) {
        ItemAlbumImageViewModel itemAlbumImagesViewModel = new ItemAlbumImageViewModel(idNameImages.get(position),position);
        holder.itemAlbumImageBinding.setViewModel(itemAlbumImagesViewModel);
        setEvent(holder,itemAlbumImagesViewModel);
    }

    private void setEvent(AlbumImagesView holder, ItemAlbumImageViewModel ItemAlbumImagesViewModel) {
        ItemAlbumImagesViewModel.mMutableLiveData.observeForever(new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object aVoid) {
                if(defaultSubmit){
                    Mutable mutable = (Mutable) aVoid;
                    if (mutable.type.equals(Constants.IMAGE)) {
                        Intent intent = new Intent(holder.itemView.getContext(), BaseActivity.class);
                        intent.putExtra(Constants.PAGE, Constants.ZOOM);
                        intent.putExtra(Constants.NAME_BAR, idNameImages.get(mutable.position).name);
                        Bundle bundle = new Bundle();
                        bundle.putString(Constants.IMAGE,idNameImages.get(mutable.position).image);
                        intent.putExtra(Constants.BUNDLE, bundle);
                        holder.itemView.getContext().startActivity(intent);
                    } else if (mutable.type.equals(Constants.VIDEO)) {
                        Intent intent = new Intent(holder.itemView.getContext(), BaseActivity.class);
                        intent.putExtra(Constants.PAGE, Constants.VIDEO);
                        intent.putExtra(Constants.NAME_BAR,idNameImages.get(mutable.position).name);
                        Bundle bundle = new Bundle();
//                    Timber.e("video:"+institutionAdsViewModel.getInstitutionRepository().getFilterShopAdsResponse()data.get(mutable.position).capture);
                        bundle.putString(Constants.VIDEO, idNameImages.get(mutable.position).capture);
                        intent.putExtra(Constants.BUNDLE, bundle);
                        holder.itemView.getContext().startActivity(intent);
                    }
                }else
                    mMutableLiveData.setValue(aVoid);
            }
        });
    }

    public void defaultSubmit(boolean defaultSubmit){
        this.defaultSubmit = defaultSubmit;
    }


    @Override
    public int getItemCount() {
        return idNameImages.size();
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
        notifyDataSetChanged();
    }

    public void remove(int item_deleted_position) {
        Timber.e("size_before:"+idNameImages.size());
        if(item_deleted_position != -1){
//            idNameImages.remove(item_deleted_position);
            Timber.e("size_after:"+idNameImages.size());
            notifyDataSetChanged();
        }
    }

    public void update(List<IdNameImage> idNameImages) {
        this.idNameImages = idNameImages;
        notifyDataSetChanged();
    }

    public class AlbumImagesView extends RecyclerView.ViewHolder{

        private ItemAlbumImageBinding itemAlbumImageBinding;
        public AlbumImagesView(@NonNull ItemAlbumImageBinding itemAlbumImageBinding) {
            super(itemAlbumImageBinding.getRoot());
            this.itemAlbumImageBinding = itemAlbumImageBinding;
        }
    }
}
