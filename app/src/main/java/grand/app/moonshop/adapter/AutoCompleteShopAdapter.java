package grand.app.moonshop.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.Toast;

import java.sql.Time;
import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.RecyclerView;
import grand.app.moonshop.R;
import grand.app.moonshop.databinding.ItemFamousShopSearchBinding;
import grand.app.moonshop.databinding.ItemFamousShopSearchBinding;
import grand.app.moonshop.models.famous.search.ShopSearchResult;
import grand.app.moonshop.viewmodels.famous.ItemShopSearchViewModel;
import grand.app.moonshop.viewmodels.famous.ItemShopSearchViewModel;
import timber.log.Timber;


public class AutoCompleteShopAdapter extends RecyclerView.Adapter<AutoCompleteShopAdapter.ShopSearch> {
    public List<ShopSearchResult> shopSearchResults;
    private LayoutInflater layoutInflater;
    private int position = -1;
    public MutableLiveData<Object> mMutableLiveData  = new MutableLiveData<>();;

    public AutoCompleteShopAdapter(List<ShopSearchResult> shopSearchResults) {
        this.shopSearchResults = shopSearchResults;
    }


    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @NonNull
    @Override
    public AutoCompleteShopAdapter.ShopSearch onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        if(layoutInflater == null){
            layoutInflater  = LayoutInflater.from(parent.getContext());
        }
        ItemFamousShopSearchBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.item_famous_shop_search,parent,false);
        return new AutoCompleteShopAdapter.ShopSearch(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull AutoCompleteShopAdapter.ShopSearch holder, final int position) {
        ItemShopSearchViewModel itemShopSearchModel = new ItemShopSearchViewModel(shopSearchResults.get(position),position);
        holder.itemFamousShopSearchBinding.setItemShopSearchViewModel(itemShopSearchModel);
        setEvent(itemShopSearchModel);
    }

    private void setEvent(ItemShopSearchViewModel itemShopSearchViewModel) {
        itemShopSearchViewModel.mMutableLiveData.observeForever(new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object aVoid) {
                mMutableLiveData.setValue(aVoid);
            }
        });
    }


    @Override
    public int getItemCount() {
        return shopSearchResults.size();
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
        notifyDataSetChanged();
    }

    public void update(List<ShopSearchResult> shopSearchResults) {
        this.shopSearchResults = shopSearchResults;
        Timber.e("searchResults:"+shopSearchResults.size());
        notifyDataSetChanged();
    }

    public class ShopSearch extends RecyclerView.ViewHolder{

        private ItemFamousShopSearchBinding itemFamousShopSearchBinding;
        public ShopSearch(@NonNull ItemFamousShopSearchBinding itemFamousShopSearchBinding) {
            super(itemFamousShopSearchBinding.getRoot());
            this.itemFamousShopSearchBinding = itemFamousShopSearchBinding;
        }
    }
    
}
