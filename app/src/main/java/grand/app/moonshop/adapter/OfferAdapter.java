package grand.app.moonshop.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.RecyclerView;
import grand.app.moonshop.R;
import grand.app.moonshop.databinding.ItemOfferBinding;
import grand.app.moonshop.databinding.ItemOfferBinding;
import grand.app.moonshop.models.offer.Offer;
import grand.app.moonshop.viewmodels.offer.ItemOfferViewModel;


public class OfferAdapter extends RecyclerView.Adapter<OfferAdapter.OfferView> {
    private List<Offer> data;
    private LayoutInflater layoutInflater;
    private int position = -1;
    public MutableLiveData<Object> mMutableLiveData  = new MutableLiveData<>();;

    public OfferAdapter(List<Offer> data) {
        this.data = data;
    }


    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @NonNull
    @Override
    public OfferView onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        if(layoutInflater == null){
            layoutInflater  = LayoutInflater.from(parent.getContext());
        }
        ItemOfferBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.item_offer,parent,false);
        return new OfferView(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull OfferView holder, final int position) {
        ItemOfferViewModel itemOfferViewModel = new ItemOfferViewModel(data.get(position),position);
        holder.itemOfferBinding.setItemOfferViewModel(itemOfferViewModel);
        setEvent(itemOfferViewModel);
    }

    private void setEvent(ItemOfferViewModel itemOfferViewModel) {
        itemOfferViewModel.mMutableLiveData.observeForever(new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object aVoid) {
                mMutableLiveData.setValue(aVoid);
            }
        });
    }


    @Override
    public int getItemCount() {
        return data.size();
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
        notifyDataSetChanged();
    }

    public void remove(int category_delete_position) {
        if(category_delete_position != -1){
            data.remove(category_delete_position);
            notifyDataSetChanged();
        }
    }

    public class OfferView extends RecyclerView.ViewHolder{

        private ItemOfferBinding itemOfferBinding;
        public OfferView(@NonNull ItemOfferBinding itemOfferBinding) {
            super(itemOfferBinding.getRoot());
            this.itemOfferBinding = itemOfferBinding;
        }
    }
}
