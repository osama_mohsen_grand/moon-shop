package grand.app.moonshop.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.RecyclerView;
import grand.app.moonshop.R;
import grand.app.moonshop.databinding.ItemHomeBinding;
import grand.app.moonshop.databinding.ItemProductBinding;
import grand.app.moonshop.models.product.Product;
import grand.app.moonshop.viewmodels.product.ItemProductViewModel;


public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.ProductView> {
    private List<Product> data;
    private LayoutInflater layoutInflater;
    private int position = -1;
    public MutableLiveData<Object> mMutableLiveData  = new MutableLiveData<>();;

    private static final String TAG = "CountryAdapter";
    public ProductAdapter(List<Product> data) {
        this.data = data;
    }


    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @NonNull
    @Override
    public ProductView onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        if(layoutInflater == null){
            layoutInflater  = LayoutInflater.from(parent.getContext());
        }
        ItemProductBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.item_product,parent,false);
        return new ProductView(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ProductView holder, final int position) {
        ItemProductViewModel itemProductViewModel = new ItemProductViewModel(data.get(position),position);
        holder.itemProductBinding.setItemProductViewModel(itemProductViewModel);
        setEvent(itemProductViewModel);
    }

    private void setEvent(ItemProductViewModel itemProductViewModel) {
        itemProductViewModel.mMutableLiveData.observeForever(new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object aVoid) {
                mMutableLiveData.setValue(aVoid);
            }
        });
    }


    @Override
    public int getItemCount() {
        return data.size();
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
        notifyDataSetChanged();
    }

    public void remove(int item_deleted_position) {
        if(item_deleted_position != -1){
            data.remove(item_deleted_position);
            notifyDataSetChanged();
        }
    }

    public class ProductView extends RecyclerView.ViewHolder{

        private ItemProductBinding itemProductBinding;
        public ProductView(@NonNull ItemProductBinding itemProductBinding) {
            super(itemProductBinding.getRoot());
            this.itemProductBinding = itemProductBinding;
        }
    }
}
