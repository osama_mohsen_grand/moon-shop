package grand.app.moonshop.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.RecyclerView;
import grand.app.moonshop.R;
import grand.app.moonshop.databinding.ItemBranchBinding;
import grand.app.moonshop.models.shop.Branch;
import grand.app.moonshop.viewmodels.branch.ItemBranchViewModel;


public class BranchAdapter extends RecyclerView.Adapter<BranchAdapter.BranchView> {
    private List<Branch> branches;
    private LayoutInflater layoutInflater;
    private int position = -1;
    private boolean allowDelete;
    public MutableLiveData<Object> mMutableLiveData  = new MutableLiveData<>();;

    public BranchAdapter(List<Branch> branches,boolean allowDelete) {
        this.branches = branches;
        this.allowDelete = allowDelete;
    }


    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @NonNull
    @Override
    public BranchView onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        if(layoutInflater == null){
            layoutInflater  = LayoutInflater.from(parent.getContext());
        }
        ItemBranchBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.item_branch,parent,false);
        return new BranchView(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull BranchView holder, final int position) {
        ItemBranchViewModel itemBranchViewModel = new ItemBranchViewModel(branches.get(position),position,allowDelete);
        holder.ItemBranchBinding.setItemBranchViewModel(itemBranchViewModel);
        setEvent(itemBranchViewModel);
    }

    private void setEvent(ItemBranchViewModel ItemBranchViewModel) {
        ItemBranchViewModel.mMutableLiveData.observeForever(new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object aVoid) {
                mMutableLiveData.setValue(aVoid);
            }
        });
    }


    @Override
    public int getItemCount() {
        return branches.size();
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
        notifyDataSetChanged();
    }

    public void remove(int category_delete_position) {
        branches.remove(category_delete_position);
        notifyDataSetChanged();
    }

    public class BranchView extends RecyclerView.ViewHolder{

        private ItemBranchBinding ItemBranchBinding;
        public BranchView(@NonNull ItemBranchBinding ItemBranchBinding) {
            super(ItemBranchBinding.getRoot());
            this.ItemBranchBinding = ItemBranchBinding;
        }
    }
}
