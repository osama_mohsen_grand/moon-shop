package grand.app.moonshop.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.RecyclerView;
import grand.app.moonshop.R;
import grand.app.moonshop.databinding.ItemImageBinding;
import grand.app.moonshop.databinding.ItemImageBinding;
import grand.app.moonshop.models.famous.details.Product;
import grand.app.moonshop.models.famous.details.Service;
import grand.app.moonshop.viewmodels.famous.ItemImageViewModel;
import grand.app.moonshop.viewmodels.famous.ItemImageViewModel;


public class FamousDetailsImageAdapter extends RecyclerView.Adapter<FamousDetailsImageAdapter.ImageView> {
    private List<Product> products;
    private LayoutInflater layoutInflater;
    public MutableLiveData<Object> mMutableLiveData  = new MutableLiveData<>();;

    public FamousDetailsImageAdapter(List<Product> products) {
        this.products = products;
    }


    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @NonNull
    @Override
    public ImageView onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        if(layoutInflater == null){
            layoutInflater  = LayoutInflater.from(parent.getContext());
        }
        ItemImageBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.item_image,parent,false);
        return new ImageView(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ImageView holder, final int position) {
        ItemImageViewModel itemImageViewModel = new ItemImageViewModel(products.get(0),position);
        holder.ItemImageBinding.setItemImageViewModel(itemImageViewModel);
        setEvent(itemImageViewModel);
    }

    private void setEvent(ItemImageViewModel itemImageViewModel) {
        itemImageViewModel.mMutableLiveData.observeForever(new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object aVoid) {
                mMutableLiveData.setValue(aVoid);
            }
        });
    }


    @Override
    public int getItemCount() {
        return products.size();
    }

    public void setList(List<Product> products) {
        this.products = products;
        notifyDataSetChanged();
    }

    public class ImageView extends RecyclerView.ViewHolder{

        private ItemImageBinding ItemImageBinding;
        public ImageView(@NonNull ItemImageBinding ItemImageBinding) {
            super(ItemImageBinding.getRoot());
            this.ItemImageBinding = ItemImageBinding;
        }
    }
}
