package grand.app.moonshop.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.RecyclerView;
import grand.app.moonshop.R;
import grand.app.moonshop.databinding.ItemHomeBinding;
import grand.app.moonshop.models.home.Datum;
import grand.app.moonshop.viewmodels.home.shop.ItemDepartmentViewModel;


public class HomeAdapter extends RecyclerView.Adapter<HomeAdapter.HomeView> {
    private List<Datum> data;
    private LayoutInflater layoutInflater;
    private int position = -1;
    public MutableLiveData<Object> mMutableLiveData  = new MutableLiveData<>();;

    private static final String TAG = "CountryAdapter";
    public HomeAdapter(List<Datum> data) {
        this.data = data;
    }


    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @NonNull
    @Override
    public HomeView onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        if(layoutInflater == null){
            layoutInflater  = LayoutInflater.from(parent.getContext());
        }
        ItemHomeBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.item_home,parent,false);
        return new HomeView(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull HomeView holder, final int position) {
        ItemDepartmentViewModel itemHomeViewModel = new ItemDepartmentViewModel(data.get(position),position);
        holder.itemHomeBinding.setItemDepartmentViewModel(itemHomeViewModel);
        setEvent(itemHomeViewModel);
    }

    private void setEvent(ItemDepartmentViewModel itemHomeViewModel) {
        itemHomeViewModel.mMutableLiveData.observeForever(new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object aVoid) {
                mMutableLiveData.setValue(aVoid);
            }
        });
    }


    @Override
    public int getItemCount() {
        return data.size();
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
        notifyDataSetChanged();
    }

    public void remove(int category_delete_position) {
        if(category_delete_position != -1){
            data.remove(category_delete_position);
            notifyDataSetChanged();
        }
    }

    public class HomeView extends RecyclerView.ViewHolder{

        private ItemHomeBinding itemHomeBinding;
        public HomeView(@NonNull ItemHomeBinding itemHomeBinding) {
            super(itemHomeBinding.getRoot());
            this.itemHomeBinding = itemHomeBinding;
        }
    }
}
