package grand.app.moonshop.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.RecyclerView;
import grand.app.moonshop.R;
import grand.app.moonshop.databinding.ItemAdsCompanyFilterBinding;
import grand.app.moonshop.models.adsCompanyFilter.AdsCompanyFilter;
import grand.app.moonshop.viewmodels.adsCompanyFilter.ItemAdsCompanyFilterViewModel;
import timber.log.Timber;


public class AdsCompanyFilterAdapter extends RecyclerView.Adapter<AdsCompanyFilterAdapter.AdsCompanyFilterView> {
    private List<AdsCompanyFilter> adsCompanyFilters;
    private LayoutInflater layoutInflater;
    public MutableLiveData<Object> mMutableLiveData = new MutableLiveData<>();
    boolean multipleChoice;
    public ArrayList<Integer> selected = new ArrayList<>();


    public AdsCompanyFilterAdapter(List<AdsCompanyFilter> adsCompanyFilters,boolean multipleChoice) {
        this.adsCompanyFilters = adsCompanyFilters;
        this.multipleChoice = multipleChoice;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @NonNull
    @Override
    public AdsCompanyFilterView onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        if (layoutInflater == null) {
            layoutInflater = LayoutInflater.from(parent.getContext());
        }
        ItemAdsCompanyFilterBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.item_ads_company_filter, parent, false);
        return new AdsCompanyFilterView(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull AdsCompanyFilterView holder, final int position) {
        ItemAdsCompanyFilterViewModel itemAdsCompanyFilterViewModel = new ItemAdsCompanyFilterViewModel(adsCompanyFilters.get(position), position,multipleChoice);
        holder.itemAdsCompanyFilterBinding.setItemAdsCompanyFilterViewModel(itemAdsCompanyFilterViewModel);
        setEvent(itemAdsCompanyFilterViewModel);
    }

    @Override
    public int getItemCount() {
        return adsCompanyFilters.size();
    }


    private void setEvent(ItemAdsCompanyFilterViewModel itemAdsCompanyFilterViewModel) {
        itemAdsCompanyFilterViewModel.mMutableLiveData.observeForever(new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object aVoid) {
                mMutableLiveData.setValue(aVoid);
                int pos = (int) aVoid;
                int id = adsCompanyFilters.get(pos).id;
                if(multipleChoice) {
                    if (isExist(id)) {
                        selected.remove(selected.indexOf(id));
                    } else {
                        selected.add(id);
                    }
                }else{
                    selected.clear();
                    selected.add(id);
                }
                notifyDataSetChanged();
            }
        });
    }


    public boolean isExist(int id){
        Timber.e("selected:"+selected.contains(id));
        return selected.contains(id);
    }

    public void update(List<AdsCompanyFilter> adsCompanyFilters) {
        this.adsCompanyFilters = adsCompanyFilters;
        notifyDataSetChanged();
    }

    public class AdsCompanyFilterView extends RecyclerView.ViewHolder {

        private ItemAdsCompanyFilterBinding itemAdsCompanyFilterBinding;

        public AdsCompanyFilterView(@NonNull ItemAdsCompanyFilterBinding itemAdsCompanyFilterBinding) {
            super(itemAdsCompanyFilterBinding.getRoot());
            this.itemAdsCompanyFilterBinding = itemAdsCompanyFilterBinding;
        }
    }
}
