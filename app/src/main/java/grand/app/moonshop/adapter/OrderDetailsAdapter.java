package grand.app.moonshop.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.RecyclerView;
import grand.app.moonshop.R;
import grand.app.moonshop.databinding.ItemOrderDetailsBinding;
import grand.app.moonshop.models.order.details.OrderProductDetails;
import grand.app.moonshop.viewmodels.order.details.ItemOrderDetailsViewModel;


public class OrderDetailsAdapter extends RecyclerView.Adapter<OrderDetailsAdapter.OrderView> {
    private List<OrderProductDetails> orderProductDetails;
    private LayoutInflater layoutInflater;
    public MutableLiveData<Object> mMutableLiveData = new MutableLiveData<>();

    public OrderDetailsAdapter(List<OrderProductDetails> orderProductDetails) {
        this.orderProductDetails = orderProductDetails;
    }


    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @NonNull
    @Override
    public OrderView onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        if (layoutInflater == null) {
            layoutInflater = LayoutInflater.from(parent.getContext());
        }
        ItemOrderDetailsBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.item_order_details, parent, false);
        return new OrderView(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull OrderView holder, final int position) {
        ItemOrderDetailsViewModel itemOrderViewModel = new ItemOrderDetailsViewModel(orderProductDetails.get(position), position);
        holder.itemOrderBinding.setItemOrderDetailsViewModel(itemOrderViewModel);
        setEvent(itemOrderViewModel);
    }

    private void setEvent(ItemOrderDetailsViewModel itemOrderViewModel) {
        itemOrderViewModel.mMutableLiveData.observeForever(new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object aVoid) {
                mMutableLiveData.setValue(aVoid);
            }
        });
    }


    @Override
    public int getItemCount() {
        return orderProductDetails.size();
    }

    public class OrderView extends RecyclerView.ViewHolder {

        private ItemOrderDetailsBinding itemOrderBinding;

        public OrderView(@NonNull ItemOrderDetailsBinding itemOrderBinding) {
            super(itemOrderBinding.getRoot());
            this.itemOrderBinding = itemOrderBinding;
        }
    }
}
