package grand.app.moonshop.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.RecyclerView;
import grand.app.moonshop.R;
import grand.app.moonshop.databinding.ItemAlbumBinding;
import grand.app.moonshop.databinding.ItemAlbumBinding;
import grand.app.moonshop.models.app.Mutable;
import grand.app.moonshop.models.famous.home.ImageVideo;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.viewmodels.famous.ItemAlbumViewModel;
import grand.app.moonshop.viewmodels.famous.ItemAlbumViewModel;
import grand.app.moonshop.views.activities.BaseActivity;


public class AlbumAdapter extends RecyclerView.Adapter<AlbumAdapter.AlbumView> {
    public List<ImageVideo> imageVideos;
    private LayoutInflater layoutInflater;
    private int position = -1;
    private boolean allowDelete,allowEdit;
    public MutableLiveData<Object> mMutableLiveData  = new MutableLiveData<>();;
    public boolean default_submit;

    public AlbumAdapter(List<ImageVideo> imageVideos,boolean default_submit) {
        this.imageVideos = imageVideos;
        this.default_submit = default_submit;
    }


    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @NonNull
    @Override
    public AlbumView onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        if(layoutInflater == null){
            layoutInflater  = LayoutInflater.from(parent.getContext());
        }
        ItemAlbumBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.item_album,parent,false);
        return new AlbumView(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull AlbumView holder, final int position) {
        ItemAlbumViewModel itemAlbumViewModel = new ItemAlbumViewModel(imageVideos.get(position),position);
        holder.itemAlbumBinding.setViewModel(itemAlbumViewModel);
        setEvent(holder);
    }

    private void setEvent(AlbumView view) {
        view.itemAlbumBinding.getViewModel().mMutableLiveData.observeForever(new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object aVoid) {
//                mMutableLiveData.setValue(aVoid);
                if(default_submit) {
                    Context context = view.itemAlbumBinding.getRoot().getContext();
                    Mutable mutable = (Mutable) aVoid;
                    if (mutable.type.equals(Constants.IMAGE)) {
                        Intent intent = new Intent(context, BaseActivity.class);
                        intent.putExtra(Constants.PAGE, Constants.ZOOM);
                        intent.putExtra(Constants.NAME_BAR, imageVideos.get(mutable.position).name);
                        Bundle bundle = new Bundle();
                        bundle.putString(Constants.IMAGE, imageVideos.get(mutable.position).image);
                        intent.putExtra(Constants.BUNDLE, bundle);
                        context.startActivity(intent);
                    } else if (mutable.type.equals(Constants.VIDEO)) {
                        Intent intent = new Intent(context, BaseActivity.class);
                        intent.putExtra(Constants.PAGE, Constants.VIDEO);
                        intent.putExtra(Constants.NAME_BAR, imageVideos.get(mutable.position).name);
                        Bundle bundle = new Bundle();
                        bundle.putString(Constants.VIDEO, imageVideos.get(mutable.position).image);
                        intent.putExtra(Constants.BUNDLE, bundle);
                        context.startActivity(intent);
                    }
                }
                mMutableLiveData.setValue(aVoid);

            }
        });
    }



    @Override
    public int getItemCount() {
        return imageVideos.size();
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
        notifyDataSetChanged();
    }

    public void update(List<ImageVideo> imageVideos) {
        this.imageVideos = imageVideos;
        notifyDataSetChanged();
    }

    public void remove(int category_delete_position) {
        imageVideos.remove(category_delete_position);
        notifyDataSetChanged();
    }

    public class AlbumView extends RecyclerView.ViewHolder{

        private ItemAlbumBinding itemAlbumBinding;
        public AlbumView(@NonNull ItemAlbumBinding itemAlbumBinding) {
            super(itemAlbumBinding.getRoot());
            this.itemAlbumBinding = itemAlbumBinding;
        }
    }
}
