package grand.app.moonshop.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.RecyclerView;
import grand.app.moonshop.R;
import grand.app.moonshop.databinding.ItemFamousDetailsCategoryBinding;
import grand.app.moonshop.models.famous.details.Service;
import grand.app.moonshop.viewmodels.famous.ItemFamousDetailsCategoryViewModel;


public class FamousDetailsCategoryAdapter extends RecyclerView.Adapter<FamousDetailsCategoryAdapter.ServiceView> {
    private List<Service> services;
    private LayoutInflater layoutInflater;
    private int selected = 0;
    public MutableLiveData<Object> mMutableLiveData  = new MutableLiveData<>();;

    public FamousDetailsCategoryAdapter(List<Service> services) {
        this.services = services;
    }


    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @NonNull
    @Override
    public ServiceView onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        if(layoutInflater == null){
            layoutInflater  = LayoutInflater.from(parent.getContext());
        }
        ItemFamousDetailsCategoryBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.item_famous_details_category,parent,false);
        return new ServiceView(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ServiceView holder, final int position) {
        ItemFamousDetailsCategoryViewModel itemServiceViewModel = new ItemFamousDetailsCategoryViewModel(services.get(position),position,position==selected);
        holder.itemServiceBinding.setItemFamousDetailsCategoryViewModel(itemServiceViewModel);
        setEvent(itemServiceViewModel);
    }

    private void setEvent(ItemFamousDetailsCategoryViewModel itemServiceViewModel) {
        itemServiceViewModel.mMutableLiveData.observeForever(new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object aVoid) {
                mMutableLiveData.setValue(aVoid);
            }
        });
    }


    @Override
    public int getItemCount() {
        return services.size();
    }

    public void setSelected(int selected) {
        this.selected = selected;
        notifyDataSetChanged();
    }

    public int getSelected() {
        return selected;
    }

    public class ServiceView extends RecyclerView.ViewHolder{

        private ItemFamousDetailsCategoryBinding itemServiceBinding;
        public ServiceView(@NonNull ItemFamousDetailsCategoryBinding itemServiceBinding) {
            super(itemServiceBinding.getRoot());
            this.itemServiceBinding = itemServiceBinding;
        }
    }
}
