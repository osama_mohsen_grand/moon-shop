package grand.app.moonshop.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.RecyclerView;
import grand.app.moonshop.R;
import grand.app.moonshop.databinding.ItemFilterAdsBinding;
import grand.app.moonshop.databinding.ItemSizeColorBinding;
import grand.app.moonshop.models.app.Mutable;
import grand.app.moonshop.models.base.IdName;
import grand.app.moonshop.models.base.IdNameDescription;
import grand.app.moonshop.viewmodels.sizeColor.ItemSizeColorViewModel;
import timber.log.Timber;


public class SizeColorAdapter extends RecyclerView.Adapter<SizeColorAdapter.SizeColorView> {
    public List<IdName> idNames;
    private LayoutInflater layoutInflater;
    private int position = -1;
    public MutableLiveData<Object> mMutableLiveData  = new MutableLiveData<>();;
    public ArrayList<Integer> selected = new ArrayList<>();
    boolean isColor;

    public SizeColorAdapter(List<IdName> idNames, boolean isColor) {
        this.idNames = idNames;
        this.isColor = isColor;
    }


    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @NonNull
    @Override
    public SizeColorView onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        if(layoutInflater == null){
            layoutInflater  = LayoutInflater.from(parent.getContext());
        }
        ItemSizeColorBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.item_size_color,parent,false);
        return new SizeColorView(binding);
    }

    public boolean isExist(int id){
        Timber.e("selected:"+selected.contains(id));
        return selected.contains(id);
    }

    @Override
    public void onBindViewHolder(@NonNull SizeColorView holder, final int position) {

        ItemSizeColorViewModel itemSizeColorViewModel = new ItemSizeColorViewModel(idNames.get(position),position,isColor);
        holder.itemSizeColorBinding.setItemSizeColorViewModel(itemSizeColorViewModel);
//        Timber.e("name:"+idNames.get(position).name);
        setEvent(itemSizeColorViewModel);
    }

    private void setEvent(ItemSizeColorViewModel itemSizeColorViewModel) {
        itemSizeColorViewModel.mMutableLiveData.observeForever(new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object aVoid) {
                mMutableLiveData.setValue(aVoid);
            }
        });
    }


    @Override
    public int getItemCount() {
        return idNames.size();
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
        notifyDataSetChanged();
    }

    public class SizeColorView extends RecyclerView.ViewHolder{

        private ItemSizeColorBinding itemSizeColorBinding;
        public SizeColorView(@NonNull ItemSizeColorBinding itemSizeColorBinding) {
            super(itemSizeColorBinding.getRoot());
            this.itemSizeColorBinding = itemSizeColorBinding;
        }
    }
}
