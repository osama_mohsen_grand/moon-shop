package grand.app.moonshop.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.RecyclerView;
import grand.app.moonshop.R;
import grand.app.moonshop.databinding.ItemChatListBinding;
import grand.app.moonshop.models.chat.list.ChatList;
import grand.app.moonshop.viewmodels.chat.list.ItemChatListViewModel;


public class ChatListAdapter extends RecyclerView.Adapter<ChatListAdapter.ChatView> {
    public List<ChatList> chats;
    private LayoutInflater layoutInflater;
    private int position = -1;
    public MutableLiveData<Object> mMutableLiveData  = new MutableLiveData<>();;

    public ChatListAdapter(List<ChatList> chats) {
        this.chats = chats;
    }


    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @NonNull
    @Override
    public ChatView onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        if(layoutInflater == null){
            layoutInflater  = LayoutInflater.from(parent.getContext());
        }
        ItemChatListBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.item_chat_list,parent,false);
        return new ChatView(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ChatView holder, final int position) {
        ItemChatListViewModel itemChatViewModel = new ItemChatListViewModel(chats.get(position),position);
        holder.itemChatListBinding.setItemChatListViewModel(itemChatViewModel);
        setEvent(itemChatViewModel);
    }

    private void setEvent(ItemChatListViewModel itemChatViewModel) {
        itemChatViewModel.mMutableLiveData.observeForever(new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object aVoid) {
                mMutableLiveData.setValue(aVoid);
                chats.get(itemChatViewModel.position).chatsCount = "0";
                notifyItemChanged(itemChatViewModel.position);
            }
        });
    }


    @Override
    public int getItemCount() {
        return chats.size();
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
        notifyDataSetChanged();
    }

    public void update(List<ChatList> chats) {
        this.chats = chats;
        notifyDataSetChanged();
    }

    public class ChatView extends RecyclerView.ViewHolder{

        private ItemChatListBinding itemChatListBinding;
        public ChatView(@NonNull ItemChatListBinding itemChatListBinding) {
            super(itemChatListBinding.getRoot());
            this.itemChatListBinding = itemChatListBinding;
        }
    }
}
