package grand.app.moonshop.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.github.islamkhsh.CardSliderAdapter;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.RecyclerView;
import grand.app.moonshop.R;
import grand.app.moonshop.databinding.ItemCountryBinding;
import grand.app.moonshop.databinding.ItemPackageBinding;
import grand.app.moonshop.models.packagePayment.Datum;
import grand.app.moonshop.viewmodels.packagePayment.ItemPackageViewModel;


public class PackageAdapter extends CardSliderAdapter<PackageAdapter.PackageView> {
    private List<Datum> packages;
    private LayoutInflater layoutInflater;
    private int position = -1;
    public MutableLiveData<Object> mMutableLiveData  = new MutableLiveData<>();;

    public PackageAdapter(List<Datum> packages) {
        this.packages = packages;
    }


    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @NonNull
    @Override
    public PackageView onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        if(layoutInflater == null){
            layoutInflater  = LayoutInflater.from(parent.getContext());
        }
        ItemPackageBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.item_package,parent,false);
        return new PackageView(binding);
    }

//    @Override
//    public void onBindViewHolder(@NonNull PackageView holder, final int position) {
//        ItemPackageViewModel itemPackageViewModel = new ItemPackageViewModel(packages.get(position),position);
//        holder.itemPackageBinding.setItemPackageViewModel(itemPackageViewModel);
//        setEvent(itemPackageViewModel);
//    }

    private void setEvent(ItemPackageViewModel itemPackageViewModel) {
        itemPackageViewModel.mMutableLiveData.observeForever(new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object aVoid) {
                int pos = (int) aVoid;
                mMutableLiveData.setValue(pos);
            }
        });
    }


    @Override
    public int getItemCount() {
        return packages.size();
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
        notifyDataSetChanged();
    }

    @Override
    public void bindVH(@NotNull PackageView holder, int position) {
        ItemPackageViewModel itemPackageViewModel = new ItemPackageViewModel(packages.get(position),position);
        holder.itemPackageBinding.setItemPackageViewModel(itemPackageViewModel);
        setEvent(itemPackageViewModel);
    }

    public class PackageView extends RecyclerView.ViewHolder{

        private ItemPackageBinding itemPackageBinding;
        public PackageView(@NonNull ItemPackageBinding itemPackageBinding) {
            super(itemPackageBinding.getRoot());
            this.itemPackageBinding = itemPackageBinding;
        }
    }
}
