package grand.app.moonshop.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.RecyclerView;
import grand.app.moonshop.R;
import grand.app.moonshop.databinding.ItemSettingsBinding;
import grand.app.moonshop.models.settings.Settings;
import grand.app.moonshop.viewmodels.settings.ItemSettingsViewModel;


public class SettingsAdapter extends RecyclerView.Adapter<SettingsAdapter.SettingsView> {
    private List<Settings> settingsList;
    private LayoutInflater layoutInflater;
    private int position = -1;
    public MutableLiveData<Object> mMutableLiveData  = new MutableLiveData<>();;

    public SettingsAdapter(List<Settings> settingsList) {
        this.settingsList = settingsList;
    }


    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @NonNull
    @Override
    public SettingsView onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        if(layoutInflater == null){
            layoutInflater  = LayoutInflater.from(parent.getContext());
        }
        ItemSettingsBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.item_settings,parent,false);
        return new SettingsView(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull SettingsView holder, final int position) {
        ItemSettingsViewModel itemSettingsViewModel = new ItemSettingsViewModel(settingsList.get(position),position);
        holder.itemBinding.setViewmodel(itemSettingsViewModel);
        setEvent(itemSettingsViewModel);
    }

    private void setEvent(ItemSettingsViewModel itemSettingsViewModel) {
        itemSettingsViewModel.mMutableLiveData.observeForever(new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object aVoid) {
                mMutableLiveData.setValue(aVoid);
            }
        });
    }


    @Override
    public int getItemCount() {
        return settingsList.size();
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
        notifyDataSetChanged();
    }

    public void remove(int item_delete_position) {
        settingsList.remove(item_delete_position);
        notifyDataSetChanged();
    }

    public void update(List<Settings> data) {
        this.settingsList = data;
        notifyDataSetChanged();
    }

    public class SettingsView extends RecyclerView.ViewHolder{

        private ItemSettingsBinding itemBinding;
        public SettingsView(@NonNull ItemSettingsBinding itemBinding) {
            super(itemBinding.getRoot());
            this.itemBinding = itemBinding;
        }
    }
}
