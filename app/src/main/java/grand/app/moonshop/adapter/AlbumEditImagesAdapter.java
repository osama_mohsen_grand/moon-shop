package grand.app.moonshop.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.RecyclerView;
import grand.app.moonshop.R;
import grand.app.moonshop.databinding.ItemAddPhotoOrVideoBinding;
import grand.app.moonshop.models.base.IdNameImage;
import grand.app.moonshop.viewmodels.famous.add.ItemAddPhotoOrVideoViewModel;
import grand.app.moonshop.vollyutils.VolleyFileObject;
import timber.log.Timber;


public class AlbumEditImagesAdapter extends RecyclerView.Adapter<AlbumEditImagesAdapter.AddPhotoOrVideoView> {
    public List<IdNameImage> idNameImages;
    public ArrayList<VolleyFileObject> volleyFileObject;
    public String type;
    private LayoutInflater layoutInflater;
    private int position = -1;
    public MutableLiveData<Object> mMutableLiveData  = new MutableLiveData<>();;

    public AlbumEditImagesAdapter(String type, List<IdNameImage> idNameImages) {
        this.type = type;
        this.idNameImages = idNameImages;
    }


    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @NonNull
    @Override
    public AddPhotoOrVideoView onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        if(layoutInflater == null){
            layoutInflater  = LayoutInflater.from(parent.getContext());
        }
        ItemAddPhotoOrVideoBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.item_add_photo_or_video,parent,false);
        return new AddPhotoOrVideoView(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull AddPhotoOrVideoView holder, final int position) {
        ItemAddPhotoOrVideoViewModel itemAddPhotoOrVideoViewModel = new ItemAddPhotoOrVideoViewModel(type,idNameImages.get(position),position,true);
        holder.itemPackageBinding.setItemFamousAddAlbumViewModel(itemAddPhotoOrVideoViewModel);
        setEvent(itemAddPhotoOrVideoViewModel);
    }

    private void setEvent(ItemAddPhotoOrVideoViewModel itemAddPhotoOrVideoViewModel) {
        itemAddPhotoOrVideoViewModel.mMutableLiveData.observeForever(new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object aVoid) {
                mMutableLiveData.setValue(aVoid);
            }
        });
    }


    @Override
    public int getItemCount() {
        return idNameImages.size();
    }

    public void addNewImageView(){
        IdNameImage idNameImage = new IdNameImage();
        idNameImage.image = "";
        idNameImage.volleyFileObject = null;
        idNameImages.add(idNameImage);
        notifyDataSetChanged();
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
        notifyDataSetChanged();
    }

    public void update(ArrayList<IdNameImage> idNameImages) {
        this.idNameImages = idNameImages;
        notifyDataSetChanged();
    }

    public void update(VolleyFileObject volleyFileObject, int new_image_position) {
        IdNameImage idNameImage = idNameImages.get(new_image_position);
        idNameImage.volleyFileObject = volleyFileObject;
        idNameImages.set(new_image_position,idNameImage);
        Timber.e("image_position:"+new_image_position);
        notifyDataSetChanged();
    }

    public void update(int new_image_position, VolleyFileObject volleyFileObject) {
        IdNameImage idNameImage = idNameImages.get(new_image_position);
        if(idNameImage == null) idNameImage = new IdNameImage();
        idNameImage.volleyFileObject = volleyFileObject;
        idNameImages.set(new_image_position,idNameImage);
        notifyItemChanged(new_image_position);
    }


    public void removeFrame(int category_delete_position) {
        this.idNameImages.remove(category_delete_position);
        notifyDataSetChanged();
    }


    public void remove(int category_delete_position) {
        IdNameImage idNameImage = idNameImages.get(category_delete_position);
        idNameImage.volleyFileObject = null;
        idNameImage.image = "";
        notifyItemChanged(category_delete_position);
    }

    public class AddPhotoOrVideoView extends RecyclerView.ViewHolder{

        private ItemAddPhotoOrVideoBinding itemPackageBinding;
        public AddPhotoOrVideoView(@NonNull ItemAddPhotoOrVideoBinding itemPackageBinding) {
            super(itemPackageBinding.getRoot());
            this.itemPackageBinding = itemPackageBinding;
        }
    }
}
