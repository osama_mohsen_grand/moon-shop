package grand.app.moonshop.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.RecyclerView;
import grand.app.moonshop.R;
import grand.app.moonshop.databinding.ItemNameSimpleBinding;
import grand.app.moonshop.models.base.IdName;
import grand.app.moonshop.viewmodels.app.ItemNameViewModel;


public class TextSimpleAdapter extends RecyclerView.Adapter<TextSimpleAdapter.IdNameView> {
    private List<IdName> idNames;
    private LayoutInflater layoutInflater;
    public int selected = -1;
    public MutableLiveData<Object> mMutableLiveData = new MutableLiveData<>();
    ;

    public TextSimpleAdapter(List<IdName> idNames) {
        this.idNames = idNames;
        if(idNames.size() > 0)
            selected = 0;
    }


    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @NonNull
    @Override
    public IdNameView onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        if (layoutInflater == null) {
            layoutInflater = LayoutInflater.from(parent.getContext());
        }
        ItemNameSimpleBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.item_name_simple, parent, false);
        return new IdNameView(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull IdNameView holder, final int position) {
        ItemNameViewModel itemIdNamePriceViewModel = new ItemNameViewModel(idNames.get(position), position, position == selected);
        holder.itemIdNameBinding.setItemNameViewModel(itemIdNamePriceViewModel);
        setEvent(itemIdNamePriceViewModel);
    }

    private void setEvent(ItemNameViewModel itemNameViewModel) {
        itemNameViewModel.mMutableLiveData.observeForever(new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object aVoid) {
                selected = (int) aVoid;
                notifyDataSetChanged();
                mMutableLiveData.setValue(aVoid);
            }
        });
    }


    @Override
    public int getItemCount() {
        return idNames.size();
    }

    public void update(List<IdName> idNames) {
        this.idNames = idNames;
        selected = 0;
        notifyDataSetChanged();
    }

    public class IdNameView extends RecyclerView.ViewHolder {

        private ItemNameSimpleBinding itemIdNameBinding;

        public IdNameView(@NonNull ItemNameSimpleBinding itemIdNameBinding) {
            super(itemIdNameBinding.getRoot());
            this.itemIdNameBinding = itemIdNameBinding;
        }
    }
}
