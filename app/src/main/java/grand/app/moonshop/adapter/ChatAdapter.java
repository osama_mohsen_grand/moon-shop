package grand.app.moonshop.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.RecyclerView;
import grand.app.moonshop.R;
import grand.app.moonshop.databinding.ItemChatBinding;
import grand.app.moonshop.databinding.ItemChatBinding;
import grand.app.moonshop.models.chat.details.ChatDetailsModel;
import grand.app.moonshop.models.chat.details.ChatDetailsResponse;
import grand.app.moonshop.viewmodels.chat.details.ItemChatViewModel;


public class ChatAdapter extends RecyclerView.Adapter<ChatAdapter.ChatView> {
    public List<ChatDetailsModel> chats;
    private LayoutInflater layoutInflater;
    private int position = -1;
    public MutableLiveData<Object> mMutableLiveData  = new MutableLiveData<>();;
    public String senderName , senderImage;

    public ChatAdapter(String senderName , String senderImage , List<ChatDetailsModel> chats) {
        this.chats = chats;
        this.senderName = senderName;
        this.senderImage = senderImage;
    }


    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @NonNull
    @Override
    public ChatView onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        if(layoutInflater == null){
            layoutInflater  = LayoutInflater.from(parent.getContext());
        }
        ItemChatBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.item_chat,parent,false);
        return new ChatView(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ChatView holder, final int position) {

        chats.get(position).senderImage = senderImage;
        chats.get(position).senderName = senderName;
        ItemChatViewModel itemChatViewModel = new ItemChatViewModel(chats.get(position),position);
        holder.itemChatBinding.setItemChatViewModel(itemChatViewModel);
        setEvent(itemChatViewModel);
    }

    private void setEvent(ItemChatViewModel ItemChatViewModel) {
        ItemChatViewModel.mMutableLiveData.observeForever(new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object aVoid) {
                mMutableLiveData.setValue(aVoid);
            }
        });
    }


    @Override
    public int getItemCount() {
        return chats.size();
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
        notifyDataSetChanged();
    }

    public void update(ChatDetailsModel chat) {
        this.chats.add(chat);
        notifyDataSetChanged();
    }

    public void add(ChatDetailsModel chat){
        this.chats.add(chat);
        notifyDataSetChanged();
    }

    public class ChatView extends RecyclerView.ViewHolder{

        private ItemChatBinding itemChatBinding;
        public ChatView(@NonNull ItemChatBinding itemChatBinding) {
            super(itemChatBinding.getRoot());
            this.itemChatBinding = itemChatBinding;
        }
    }
}
