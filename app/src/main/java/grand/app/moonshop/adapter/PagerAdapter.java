package grand.app.moonshop.adapter;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

public class PagerAdapter extends FragmentPagerAdapter {

    private static final int NUM_PAGES = 3;

    public PagerAdapter(FragmentManager fragmentManager) {
        super(fragmentManager);
    }

    @Override
    public int getCount() {
        return NUM_PAGES;
    }

    @Override
    public Fragment getItem(int position) {
//        if(position == 0)
//            return new Slide1Fragment();
//        else if(position == 1)
//            return new Slide2Fragment();
//        else
//            return new Slide3Fragment();
        return null;
    }
}