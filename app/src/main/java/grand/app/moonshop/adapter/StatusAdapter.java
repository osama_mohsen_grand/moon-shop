package grand.app.moonshop.adapter;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.RecyclerView;
import grand.app.moonshop.R;
import grand.app.moonshop.databinding.ItemStatusBinding;
import grand.app.moonshop.databinding.ItemStatusBinding;
import grand.app.moonshop.models.app.Mutable;
import grand.app.moonshop.models.status.Status;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.utils.resources.ResourceManager;
import grand.app.moonshop.viewmodels.status.ItemStatusViewModel;
import grand.app.moonshop.views.activities.BaseActivity;


public class StatusAdapter extends RecyclerView.Adapter<StatusAdapter.CountryView> {
    private List<Status> statusList;
    private LayoutInflater layoutInflater;
    private int position = -1;
    public MutableLiveData<Object> mMutableLiveData  = new MutableLiveData<>();;

    public StatusAdapter(List<Status> statusList) {
        this.statusList = statusList;
    }


    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @NonNull
    @Override
    public CountryView onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        if(layoutInflater == null){
            layoutInflater  = LayoutInflater.from(parent.getContext());
        }
        ItemStatusBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.item_status,parent,false);
        return new CountryView(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull CountryView holder, final int position) {
        ItemStatusViewModel itemCountryViewModel = new ItemStatusViewModel(statusList.get(position),position);
        holder.itemStatusBinding.setItemStatusViewModel(itemCountryViewModel);
        setEvent(holder , itemCountryViewModel);
    }

    private void setEvent(CountryView holder, ItemStatusViewModel itemCountryViewModel) {
        itemCountryViewModel.mMutableLiveData.observeForever(new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object aVoid) {
                Mutable mutable = (Mutable) aVoid;
                if(mutable.type.equals(Constants.SUBMIT)) {
                    Status status = statusList.get(itemCountryViewModel.position);
                    Log.e("image",status.mImage);
                    if (status.type == 1) {
                        Intent intent = new Intent(holder.itemStatusBinding.getRoot().getContext(), BaseActivity.class);
                        intent.putExtra(Constants.PAGE, Constants.ZOOM);
                        Bundle bundle = new Bundle();
                        bundle.putString(Constants.IMAGE, status.mImage);
                        bundle.putString(Constants.DATE, status.mCreatedAt);
                        intent.putExtra(Constants.BUNDLE,bundle);
                        holder.itemStatusBinding.getRoot().getContext().startActivity(intent);
                    } else {
                        Intent intent = new Intent(holder.itemStatusBinding.getRoot().getContext(), BaseActivity.class);
                        intent.putExtra(Constants.PAGE, Constants.VIDEO);
                        intent.putExtra(Constants.NAME_BAR, ResourceManager.getString(R.string.video));
                        Bundle bundle = new Bundle();
                        bundle.putString(Constants.VIDEO, status.mImage);
                        bundle.putString(Constants.DATE, status.mCreatedAt);
                        intent.putExtra(Constants.BUNDLE, bundle);
                        holder.itemStatusBinding.getRoot().getContext().startActivity(intent);
                    }
                }else
                    mMutableLiveData.setValue(aVoid);
            }
        });
    }


    @Override
    public int getItemCount() {
        return statusList.size();
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
        notifyDataSetChanged();
    }

    public void remove(int item_delete_position) {
        statusList.remove(item_delete_position);
        notifyDataSetChanged();
    }

    public class CountryView extends RecyclerView.ViewHolder{

        private ItemStatusBinding itemStatusBinding;
        public CountryView(@NonNull ItemStatusBinding itemStatusBinding) {
            super(itemStatusBinding.getRoot());
            this.itemStatusBinding = itemStatusBinding;
        }
    }
}
