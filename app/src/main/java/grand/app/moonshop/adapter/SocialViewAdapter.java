package grand.app.moonshop.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.RecyclerView;
import grand.app.moonshop.R;
import grand.app.moonshop.databinding.ItemSocialViewBinding;
import grand.app.moonshop.models.user.info.Profile;
import grand.app.moonshop.viewmodels.profile.ItemSocialViewModel;


public class SocialViewAdapter extends RecyclerView.Adapter<SocialViewAdapter.SocialView> {
    private List<Profile> notifications;
    private LayoutInflater layoutInflater;
    private int position = -1;
    public MutableLiveData<Object> mMutableLiveData  = new MutableLiveData<>();;

    public SocialViewAdapter(List<Profile> notifications) {
        this.notifications = notifications;
    }


    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @NonNull
    @Override
    public SocialView onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        if(layoutInflater == null){
            layoutInflater  = LayoutInflater.from(parent.getContext());
        }
        ItemSocialViewBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.item_social_view,parent,false);
        return new SocialView(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull SocialView holder, final int position) {
        ItemSocialViewModel itemViewModel = new ItemSocialViewModel(notifications.get(position),position,false);
        holder.itemSocialBinding.setViewmodel(itemViewModel);
        setEvent(holder.itemSocialBinding.getRoot().getRootView().getContext() , itemViewModel);
    }

    private static final String TAG = "SocialAdapter";

    private void setEvent(Context context , ItemSocialViewModel itemViewModel) {
        itemViewModel.mMutableLiveData.observeForever(new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object aVoid) {
                Profile profile = notifications.get((int)aVoid);
                Log.d(TAG,profile.type);
                if(profile.type.equals("whats")){
                    try {
                        Log.d(TAG,"whatsapp");
                        String url = "https://api.whatsapp.com/send?phone="+profile.media;
                        Intent i = new Intent(Intent.ACTION_VIEW);
                        i.setData(Uri.parse(url));
                        context.startActivity(i);

                    } catch (Exception e) {
                        Log.e("exc",e.getMessage().toString());
                        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=com.whatsapp&hl=en"));
                        context.startActivity(browserIntent);
                    }
                }else{
                    try {
                        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(profile.media));
                        context.startActivity(browserIntent);
                    } catch (Exception e) {
                        Log.d("exception",e.getMessage());
                    }

                }
            }
        });
    }


    @Override
    public int getItemCount() {
        return notifications.size();
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
        notifyDataSetChanged();
    }

    public void remove(int item_delete_position) {
        notifications.remove(item_delete_position);
        notifyDataSetChanged();
    }

    public void update(List<Profile> profile) {
        this.notifications = profile;
        notifyDataSetChanged();
    }

    public class SocialView extends RecyclerView.ViewHolder{

        private ItemSocialViewBinding itemSocialBinding;
        public SocialView(@NonNull ItemSocialViewBinding itemSocialBinding) {
            super(itemSocialBinding.getRoot());
            this.itemSocialBinding = itemSocialBinding;
        }
    }
}
