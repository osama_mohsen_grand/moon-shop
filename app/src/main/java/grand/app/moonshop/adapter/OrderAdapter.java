package grand.app.moonshop.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.RecyclerView;
import grand.app.moonshop.R;
import grand.app.moonshop.databinding.ItemMyOrderBinding;
import grand.app.moonshop.models.order.Order;
import grand.app.moonshop.viewmodels.order.ItemOrderViewModel;


public class OrderAdapter extends RecyclerView.Adapter<OrderAdapter.OrderView> {
    private List<Order> shipping;
    private LayoutInflater layoutInflater;
    public MutableLiveData<Object> mMutableLiveData = new MutableLiveData<>();

    public OrderAdapter(List<Order> shipping) {
        this.shipping = shipping;
    }


    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @NonNull
    @Override
    public OrderView onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        if (layoutInflater == null) {
            layoutInflater = LayoutInflater.from(parent.getContext());
        }
        ItemMyOrderBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.item_my_order, parent, false);
        return new OrderView(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull OrderView holder, final int position) {
        ItemOrderViewModel itemOrderViewModel = new ItemOrderViewModel(shipping.get(position), position);
        holder.itemOrderBinding.setItemOrderViewModel(itemOrderViewModel);
        setEvent(itemOrderViewModel);
    }

    private void setEvent(ItemOrderViewModel itemOrderViewModel) {
        itemOrderViewModel.mMutableLiveData.observeForever(new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object aVoid) {
                mMutableLiveData.setValue(aVoid);
                shipping.get(itemOrderViewModel.position).orderCount = "0";
                notifyItemChanged(itemOrderViewModel.position);
            }
        });
    }


    @Override
    public int getItemCount() {
        return shipping.size();
    }

    public class OrderView extends RecyclerView.ViewHolder {

        private ItemMyOrderBinding itemOrderBinding;

        public OrderView(@NonNull ItemMyOrderBinding itemOrderBinding) {
            super(itemOrderBinding.getRoot());
            this.itemOrderBinding = itemOrderBinding;
        }
    }
}
