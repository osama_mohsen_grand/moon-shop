package grand.app.moonshop.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.RecyclerView;
import grand.app.moonshop.R;
import grand.app.moonshop.databinding.ItemReservationBinding;
import grand.app.moonshop.databinding.ItemReservationBinding;
import grand.app.moonshop.models.app.Mutable;
import grand.app.moonshop.models.reservation.doctor.order.ReservationOrder;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.viewmodels.reservation.main.ItemReservationViewModel;
import timber.log.Timber;


public class ReservationAdapter extends RecyclerView.Adapter<ReservationAdapter.ReservationOrderView> {
    public List<ReservationOrder> reservationOrders;
    private LayoutInflater layoutInflater;
    private int position = -1;
    public String status;
    public MutableLiveData<Object> mMutableLiveData  = new MutableLiveData<>();;

    public ReservationAdapter(List<ReservationOrder> reservationOrders,String status) {
        this.reservationOrders = reservationOrders;
        this.status = status;
    }


    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @NonNull
    @Override
    public ReservationOrderView onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        if(layoutInflater == null){
            layoutInflater  = LayoutInflater.from(parent.getContext());
        }
        ItemReservationBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.item_reservation,parent,false);
        return new ReservationOrderView(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ReservationOrderView holder, final int position) {
        ItemReservationViewModel itemReservationOrderViewModel = new ItemReservationViewModel(reservationOrders.get(position),position,status);
        holder.itemAlbumBinding.setItemReservationViewModel(itemReservationOrderViewModel);
        setEvent(itemReservationOrderViewModel);
    }

    private void setEvent(ItemReservationViewModel ItemReservationViewModel) {
        ItemReservationViewModel.mMutableLiveData.observeForever(new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object aVoid) {
                mMutableLiveData.setValue(aVoid);
            }
        });
    }



    @Override
    public int getItemCount() {
        return reservationOrders.size();
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
        notifyDataSetChanged();
    }

    public void update(List<ReservationOrder> reservationOrders) {
        Timber.e("update list");
        this.reservationOrders = reservationOrders;
//        mMutableLiveData.setValue(new Mutable(Constants.ADD,0));
        notifyDataSetChanged();
    }

    public void remove(int category_delete_position) {
        reservationOrders.remove(category_delete_position);
        notifyDataSetChanged();
    }

    public void add(ReservationOrder reservationOrder) {
        reservationOrders.add(reservationOrder);
        notifyDataSetChanged();
    }

    public class ReservationOrderView extends RecyclerView.ViewHolder{

        private ItemReservationBinding itemAlbumBinding;

        public ReservationOrderView(@NonNull ItemReservationBinding itemAlbumBinding) {
            super(itemAlbumBinding.getRoot());
            this.itemAlbumBinding = itemAlbumBinding;
        }
    }
}
