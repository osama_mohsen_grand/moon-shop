package grand.app.moonshop.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.RecyclerView;
import grand.app.moonshop.R;
import grand.app.moonshop.databinding.ItemSocialBinding;
import grand.app.moonshop.models.user.info.Profile;
import grand.app.moonshop.viewmodels.profile.ItemSocialViewModel;


public class SocialAdapter extends RecyclerView.Adapter<SocialAdapter.ProfileView> {
    public List<Profile> profiles;
    private LayoutInflater layoutInflater;
    private int position = -1;
    public MutableLiveData<Object> mMutableLiveData  = new MutableLiveData<>();;

    public boolean editable = true;
    public SocialAdapter(List<Profile> profiles) {
        this.profiles = profiles;
    }


    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @NonNull
    @Override
    public ProfileView onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        if(layoutInflater == null){
            layoutInflater  = LayoutInflater.from(parent.getContext());
        }
        ItemSocialBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.item_social,parent,false);
        return new ProfileView(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ProfileView holder, final int position) {
        ItemSocialViewModel itemProfileViewModel = new ItemSocialViewModel(profiles.get(position),position,editable);
        holder.itemAlbumBinding.setViewModel(itemProfileViewModel);
        setEvent(itemProfileViewModel);
    }

    private void setEvent(ItemSocialViewModel ItemSocialViewModel) {
        ItemSocialViewModel.mMutableLiveData.observeForever(new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object aVoid) {
                mMutableLiveData.setValue(aVoid);
            }
        });
    }


    @Override
    public int getItemCount() {
        return profiles.size();
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
        notifyDataSetChanged();
    }

    public void update(List<Profile> profiles) {
        this.profiles = profiles;
        notifyDataSetChanged();
    }

    public void remove(int category_delete_position) {
        profiles.remove(category_delete_position);
        notifyDataSetChanged();
    }

    public void updateEditable(boolean editable) {
        this.editable = editable;
        notifyDataSetChanged();
    }

    public class ProfileView extends RecyclerView.ViewHolder{

        private ItemSocialBinding itemAlbumBinding;
        public ProfileView(@NonNull ItemSocialBinding itemAlbumBinding) {
            super(itemAlbumBinding.getRoot());
            this.itemAlbumBinding = itemAlbumBinding;
        }
    }
}
