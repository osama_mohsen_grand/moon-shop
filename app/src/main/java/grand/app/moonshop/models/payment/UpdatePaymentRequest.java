package grand.app.moonshop.models.payment;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import grand.app.moonshop.utils.storage.user.UserHelper;

public class UpdatePaymentRequest {
    @SerializedName("driver_id")
    @Expose
    public String driver_id;
    @SerializedName("type")
    @Expose
    public String type;

    public UpdatePaymentRequest(int type) {
        driver_id = String.valueOf(UserHelper.getUserId());
        this.type = String.valueOf(type);
    }

    public void setType(int type){
        this.type = String.valueOf(type);
    }
}
