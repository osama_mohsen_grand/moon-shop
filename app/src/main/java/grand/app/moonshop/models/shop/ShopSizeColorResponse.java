package grand.app.moonshop.models.shop;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import grand.app.moonshop.models.base.IdName;
import grand.app.moonshop.models.base.StatusMsg;

public class ShopSizeColorResponse extends StatusMsg {
    @SerializedName("sizes")
    @Expose
    public List<IdName> sizes = null;
    @SerializedName("colors")
    @Expose
    public List<IdName> colors = null;
}
