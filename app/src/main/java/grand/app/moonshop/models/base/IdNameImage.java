package grand.app.moonshop.models.base;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import grand.app.moonshop.vollyutils.VolleyFileObject;

public class IdNameImage implements Serializable {
    @SerializedName("id")
    @Expose
    public int id;

    @SerializedName("name")
    @Expose
    public String name;

    @SerializedName("image")
    @Expose
    public String image = "";

    @SerializedName("capture")
    @Expose
    public String capture = "";

    @SerializedName("type")
    @Expose
    public Integer type;

    @SerializedName("account_type")
    @Expose
    public String accountType;

    //in ads
    @SerializedName("shop_id")
    @Expose
    public Integer shopId;


    public transient VolleyFileObject volleyFileObject = null;
}
