package grand.app.moonshop.models.famous.album;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import grand.app.moonshop.models.base.IdNameImage;
import grand.app.moonshop.models.base.StatusMsg;

public class FamousAlbumImagesResponse extends StatusMsg {
    @SerializedName("data")
    @Expose
    public ArrayList<IdNameImage> data = new ArrayList<>();

    @SerializedName("allow_add")
    @Expose
    public boolean allow_add;
}
