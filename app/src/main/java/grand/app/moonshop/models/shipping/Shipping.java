package grand.app.moonshop.models.shipping;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Shipping {
    @SerializedName("id")
    @Expose
    public Integer id;
    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("phone")
    @Expose
    public String phone;
    @SerializedName("price")
    @Expose
    public String price;

    public Shipping(Integer id, String name, String price) {
        this.id = id;
        this.name = name;
        this.price = price;
    }
}
