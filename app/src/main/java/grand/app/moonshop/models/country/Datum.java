package grand.app.moonshop.models.country;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Datum {
    @SerializedName("id")
    @Expose
    public Integer id;
    @SerializedName("country_name")
    @Expose
    public String countryName = "";

    @SerializedName("country_image")
    @Expose
    public String countryImage = "";
    @SerializedName("currency")
    @Expose
    public String currency;
    @SerializedName("code")
    @Expose
    public String code;
    @SerializedName("cities")
    @Expose
    public List<City> cities = null;
}
