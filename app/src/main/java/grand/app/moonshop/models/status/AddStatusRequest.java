package grand.app.moonshop.models.status;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import grand.app.moonshop.utils.storage.user.UserHelper;

public class AddStatusRequest {
    @SerializedName("type")
    @Expose
    public String type = "";
    @SerializedName("status_type")
    @Expose
    public int status_type = 0;
    @SerializedName("duration")
    @Expose
    public String duration = "";


    public AddStatusRequest() {
        this.type = UserHelper.getUserDetails().type;
    }

    public void setStatus_type(int status_type) {
        this.status_type = status_type;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }
}
