package grand.app.moonshop.models.chat;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import grand.app.moonshop.models.base.StatusMsg;
import grand.app.moonshop.models.chat.details.ChatDetailsModel;

public class ChatSendResponse extends StatusMsg {

    @SerializedName("data")
    @Expose
    public ChatDetailsModel data;
}
