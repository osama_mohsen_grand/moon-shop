package grand.app.moonshop.models.reservation.beauty;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import grand.app.moonshop.models.base.StatusMsg;
import grand.app.moonshop.models.service.Service;
import grand.app.moonshop.viewmodels.reservation.beauty.BeautyAddServiceViewModel;

public class BeautyOrderDetailsResponse extends StatusMsg {
    @SerializedName("data")
    @Expose
    public BeautyOrderDetailsModel data;

}
