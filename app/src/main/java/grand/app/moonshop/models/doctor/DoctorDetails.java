package grand.app.moonshop.models.doctor;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DoctorDetails {
    @SerializedName("id")
    @Expose
    public Integer id;
    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("description")
    @Expose
    public String description;
    @SerializedName("image")
    @Expose
    public String image;
    @SerializedName("yearsExperience")
    @Expose
    public Integer yearsExperience;
    @SerializedName("doctor_specialties")
    @Expose
    public String doctorSpecialties;
    @SerializedName("rate")
    @Expose
    public float rate;
    @SerializedName("price")
    @Expose
    public String price;
    @SerializedName("day_id")
    @Expose
    public Integer dayId;
    @SerializedName("schedules")
    @Expose
    public List<Schedule> schedules = null;
    @SerializedName("reserved_time")
    @Expose
    public List<String> reservedTime = null;

}
