package grand.app.moonshop.models.shop;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BeShop {
    @SerializedName("id")
    @Expose
    public Integer id;
    @SerializedName("email")
    @Expose
    public String email;
    @SerializedName("phone")
    @Expose
    public String phone;
    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("nick_name")
    @Expose
    public String nickName;
    @SerializedName("image")
    @Expose
    public String image;
    @SerializedName("shop_service")
    @Expose
    public Integer shopService;
    @SerializedName("followers")
    @Expose
    public Integer followers;
    @SerializedName("shop_type")
    @Expose
    public String shopType;
    @SerializedName("is_shop")
    @Expose
    public Boolean isShop;
    @SerializedName("shop_famous_id")
    @Expose
    public Integer shopFamousId;
    @SerializedName("shop_famous_type")
    @Expose
    public Integer shopFamousType;
    @SerializedName("shop_famous_name")
    @Expose
    public String shopFamousName;
    @SerializedName("shop_famous_image")
    @Expose
    public String shopFamousImage;
    @SerializedName("package_id")
    @Expose
    public Integer packageId;
    @SerializedName("account_type")
    @Expose
    public Integer accountType;
    @SerializedName("rate")
    @Expose
    public String rate;
    @SerializedName("comments")
    @Expose
    public Integer comments;
    @SerializedName("country_id")
    @Expose
    public Integer countryId;
    @SerializedName("country_name")
    @Expose
    public String countryName;
    @SerializedName("city_id")
    @Expose
    public Integer cityId;
    @SerializedName("lat")
    @Expose
    public String lat;
    @SerializedName("lng")
    @Expose
    public String lng;
    @SerializedName("address")
    @Expose
    public String address;
    @SerializedName("jwt_token")
    @Expose
    public String jwtToken;
}
