package grand.app.moonshop.models.category;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import androidx.databinding.ObservableField;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.utils.Validate;
import grand.app.moonshop.utils.storage.user.UserHelper;

public class IdNameRequest {

    @SerializedName("id")
    @Expose
    public int id;

    @SerializedName("name")
    @Expose
    private String name = "";

    @SerializedName("type")
    @Expose
    private String type = "";

    public ObservableField nameError;

    public IdNameRequest(){
        nameError = new ObservableField();
        if(UserHelper.getUserId() != -1)
            type = UserHelper.getUserDetails().type;
    }

    public boolean isValid() {
        boolean valid = true;
        if (!Validate.isValid(name)) {
            nameError.set(Validate.error);
            valid = false;
        } else
            nameError.set(null);
        return valid;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
        nameError.set(null);
    }
}
