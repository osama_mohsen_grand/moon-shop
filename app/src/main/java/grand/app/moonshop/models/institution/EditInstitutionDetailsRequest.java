package grand.app.moonshop.models.institution;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import androidx.databinding.ObservableField;
import grand.app.moonshop.R;
import grand.app.moonshop.models.app.AppMoon;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.utils.Validate;
import grand.app.moonshop.utils.maputils.base.MapConfig;
import grand.app.moonshop.utils.resources.ResourceManager;
import grand.app.moonshop.utils.storage.user.UserHelper;
import grand.app.moonshop.vollyutils.MyApplication;
import grand.app.moonshop.vollyutils.VolleyFileObject;
import timber.log.Timber;

public class EditInstitutionDetailsRequest {

    @SerializedName("id")
    @Expose
    private int id;

    @SerializedName("name")
    @Expose
    private String name = "";

    @SerializedName("type")
    @Expose
    public String type = UserHelper.getUserDetails().type;

    @SerializedName("description")
    @Expose
    public String description = "";


    @SerializedName("lat")
    @Expose
    public double lat;

    @SerializedName("lng")
    @Expose
    public double lng;

    @SerializedName("edit_type")
    @Expose
    public int edit_type=1;


    @SerializedName("address")
    @Expose
    public transient String address = ResourceManager.getString(R.string.done_selected_address);

    public transient ObservableField nameError;
    public transient ObservableField descriptionError;
    public transient ObservableField addressError;

    public EditInstitutionDetailsRequest(int id, String name, String description, double lat, double lng) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.lat = lat;
        this.lng = lng;
        nameError = new ObservableField();
        descriptionError = new ObservableField();
        addressError = new ObservableField();
        if(lat != 0 && lng != 0){
            address = new MapConfig(MyApplication.getInstance(),null).getAddress(lat,lng);
        }
    }

    public boolean isValid() {
        boolean valid = true;
        if(!Validate.isValid(name)) {
            nameError.set(Validate.error);
            valid = false;
        }
        if(!Validate.isValid(description)) {
            descriptionError.set(Validate.error);
            valid = false;
        }
        return valid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
        nameError.set(null);
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
        descriptionError.set(null);
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
        addressError.set(null);
    }
}
