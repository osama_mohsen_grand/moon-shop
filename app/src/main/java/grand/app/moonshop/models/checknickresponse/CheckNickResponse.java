package grand.app.moonshop.models.checknickresponse;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import grand.app.moonshop.models.base.IdName;
import grand.app.moonshop.models.base.StatusMsg;

public class CheckNickResponse extends StatusMsg {

    @Expose
	@SerializedName("data")
    public List<IdName> suggestions;

}