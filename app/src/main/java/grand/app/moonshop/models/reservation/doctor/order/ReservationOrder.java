package grand.app.moonshop.models.reservation.doctor.order;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ReservationOrder {
    @SerializedName("id")
    @Expose
    public Integer id;
    @SerializedName("doctor_id")
    @Expose
    public Integer doctorId;
    @SerializedName("user_id")
    @Expose
    public Integer userId;
    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("image")
    @Expose
    public String image;
    @SerializedName("phone")
    @Expose
    public String phone;
    @SerializedName("doctor_name")
    @Expose
    public String doctorName;
    @SerializedName("doctor_image")
    @Expose
    public String doctorImage = "";
    @SerializedName("cost")
    @Expose
    public String cost;
    @SerializedName("day")
    @Expose
    public String day;
    @SerializedName("order_date")
    @Expose
    public String orderDate;
    @SerializedName("time")
    @Expose
    public String time;

    public boolean visible = false;
}
