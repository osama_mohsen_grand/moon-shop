package grand.app.moonshop.models.user.info;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import androidx.databinding.ObservableField;

public class Profile {

    @SerializedName("id")
    @Expose
    public Integer id;
    @SerializedName("media")
    @Expose
    public String media;
    @SerializedName("type")
    @Expose
    public String type;
    @SerializedName("image")
    @Expose
    public String image;

    public transient ObservableField mediaError = new ObservableField();
}
