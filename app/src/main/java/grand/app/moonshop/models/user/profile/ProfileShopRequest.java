package grand.app.moonshop.models.user.profile;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import androidx.databinding.ObservableField;
import grand.app.moonshop.models.app.AppMoon;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.utils.Validate;
import grand.app.moonshop.utils.storage.user.UserHelper;
import grand.app.moonshop.vollyutils.VolleyFileObject;
import timber.log.Timber;

public class ProfileShopRequest implements Serializable {

    public transient VolleyFileObject volleyFileObject = null;

    @SerializedName("type")
    @Expose
    private String type;

    @SerializedName("name")
    @Expose
    private String name = "";

    @SerializedName("nick_name")
    @Expose
    private String nickname = "";


    @SerializedName("email")
    @Expose
    private String email = "";

    @SerializedName("phone")
    @Expose
    private String phone = "";


    @SerializedName("lat")
    @Expose
    private double lat = 0;

    @SerializedName("lng")
    @Expose
    private double lng = 0;

    @SerializedName("address")
    @Expose
    private String address = "";

    public transient ObservableField nameError;
    public transient ObservableField emailError;
    public transient ObservableField phoneError;
    public transient ObservableField addressError;
    public transient ObservableField nickNameError;

    public ProfileShopRequest() {
        User user = UserHelper.getUserDetails();
        type = user.type;
        name = user.name;
        email = user.email;
        phone = user.phone;
        nickname = user.nickName;
        lat = Double.parseDouble(user.lat);
        lng = Double.parseDouble(user.lng);
        address = user.address;

        nameError = new ObservableField();
        phoneError = new ObservableField();
        emailError = new ObservableField();
        addressError = new ObservableField();
        nickNameError = new ObservableField();
    }


    public boolean isValid() {
        boolean valid = true;

        if(!Validate.isValid(name)) {
            nameError.set(Validate.error);
            valid = false;
            Timber.e("name:error");
        }

        if(!Validate.isValid(nickname)) {
            nickNameError.set(Validate.error);
            valid = false;
            Timber.e("name:error");
        }
        if(!Validate.isValid(phone, Constants.PHONE) && !UserHelper.getUserDetails().type.equals(Constants.TYPE_RESERVATION_CLINIC)) {
            phoneError.set(Validate.error);
            valid = false;
            Timber.e("phone:error");
        }
        if(!Validate.isValid(email, Constants.EMAIL)) {
            emailError.set(Validate.error);
            valid = false;
            Timber.e("email:error");
        }

        if(!Validate.isValid(address)) {
            addressError.set(Validate.error);
            valid = false;
            Timber.e("address:error");
        }
        Timber.e("valid_input:"+valid);
        return valid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
        nameError.set(null);
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
        phoneError.set(null);
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
        emailError.set(null);
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
        nickNameError.set(null);
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
        addressError.set(null);
    }

}
