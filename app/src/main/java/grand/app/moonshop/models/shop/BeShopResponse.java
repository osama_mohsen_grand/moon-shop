package grand.app.moonshop.models.shop;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import grand.app.moonshop.models.base.StatusMsg;

public class BeShopResponse extends StatusMsg {
    @SerializedName("data")
    @Expose
    public BeShop data;
}
