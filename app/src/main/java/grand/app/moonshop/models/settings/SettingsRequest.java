package grand.app.moonshop.models.settings;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import grand.app.moonshop.utils.storage.user.UserHelper;

public class SettingsRequest {
    @SerializedName("type")
    @Expose
    public int type=1;


    // 1: privacy, 2: terms , 3: faq
    @SerializedName("setting_type")
    @Expose
    public int settingType;

    public SettingsRequest(int setting_type) {
        this.settingType = setting_type;
    }
}
