package grand.app.moonshop.models.offer;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import grand.app.moonshop.models.app.AppMoon;

public class AdsConfirmRequest {
    @SerializedName("type")
    @Expose
    public String type = AppMoon.getUserType();

    @SerializedName("ads_id")
    @Expose
    public int ads_id;

    @SerializedName("status")
    @Expose
    public int status;

    public AdsConfirmRequest(int ads_id, int status) {
        this.ads_id = ads_id;
        this.status = status;
    }
}
