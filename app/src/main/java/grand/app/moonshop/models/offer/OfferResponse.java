
package grand.app.moonshop.models.offer;

import java.util.List;
import com.google.gson.annotations.SerializedName;

import grand.app.moonshop.models.base.IdNameImage;
import grand.app.moonshop.models.base.StatusMsg;

public class OfferResponse extends StatusMsg {

    @SerializedName("data")
    public List<Offer> mData;

}
