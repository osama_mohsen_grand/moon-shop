package grand.app.moonshop.models.famous;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.utils.storage.user.UserHelper;

public class FollowRequest {
    @SerializedName("to")
    @Expose
    public int to;
    @SerializedName("type")
    @Expose
    public int type;

    public FollowRequest(int to) {
        this.to = to;
        this.type = 3;
    }
}
