package grand.app.moonshop.models.shop;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

import grand.app.moonshop.models.base.IdNameImage;
import grand.app.moonshop.models.famous.home.ImageVideo;

public class ShopData implements Serializable {
    @SerializedName("details")
    @Expose
    public ShopDetails shop_details = new ShopDetails();

    @SerializedName("services")
    @Expose
    public List<IdNameImage> categories;

    @SerializedName("branches")
    @Expose
    public List<Branch> branches;


}


