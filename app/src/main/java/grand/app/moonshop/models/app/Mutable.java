package grand.app.moonshop.models.app;

import android.view.View;

public class Mutable {
    public String type;
    public String key = "";
    public int position;

    public View v = null;

    public Mutable(String type, int position) {
        this.type = type;
        this.position = position;
    }

    public Mutable(String type, int position, View v) {
        this.type = type;
        this.position = position;
        this.v = v;
    }

    public Mutable(String type, int position,String key) {
        this.type = type;
        this.position = position;
        this.key = key;
    }

}
