package grand.app.moonshop.models.chat;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import grand.app.moonshop.utils.storage.user.UserHelper;

public class ChatGetRequest {
    @SerializedName("user_id")
    @Expose
    public String user_id;

    @SerializedName("driver_id")
    @Expose
    public String driver_id;

    public ChatGetRequest(int user_id) {
        this.user_id = ""+user_id;
        this.driver_id = ""+ UserHelper.getUserId();
    }
}
