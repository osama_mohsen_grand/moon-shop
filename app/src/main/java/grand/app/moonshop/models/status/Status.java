
package grand.app.moonshop.models.status;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Status {

    @SerializedName("created_at")
    @Expose
    public String mCreatedAt;
    @SerializedName("id")
    @Expose
    public int mId;
    @SerializedName("image")
    @Expose
    public String mImage;
    @SerializedName("type")
    @Expose
    public int type;
}
