package grand.app.moonshop.models.famous.album;

import android.util.Log;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import androidx.databinding.ObservableField;
import grand.app.moonshop.R;
import grand.app.moonshop.models.app.AppMoon;
import grand.app.moonshop.utils.Validate;
import grand.app.moonshop.utils.resources.ResourceManager;
import grand.app.moonshop.utils.storage.user.UserHelper;
import timber.log.Timber;

public class FamousAddAlbumRequest {

    public transient String image = ResourceManager.getString(R.string.add_album_photos);
    @SerializedName("id")
    @Expose
    public String id = null;
    @SerializedName("type")
    @Expose
    public String type = "";
    @SerializedName("tab")
    @Expose
    public String tab;

    @SerializedName("name")
    @Expose
    String name = "";

    @SerializedName("account_type")
    @Expose
    String accountType = AppMoon.getUserType();

    @SerializedName("shop_id")
    @Expose
    public int shop_id = -1;


    @SerializedName("service_id")
    @Expose
    public String serviceId = null;

    @SerializedName("ads_expired")
    @Expose
    private String adsExpire= "";

    @SerializedName("category_id")
    @Expose
    public String categoryId = null;

    @SerializedName("service")
    @Expose
    transient String service = "";

    @SerializedName("category")
    @Expose
    transient String category = "";


    public transient ObservableField nameError,categoryError,serviceError,adsExpiredError;

    public FamousAddAlbumRequest() {
        nameError = new ObservableField();
        categoryError = new ObservableField();
        serviceError = new ObservableField();
        adsExpiredError = new ObservableField();
    }

    private static final String TAG = "FamousAddAlbumRequest";

    public boolean isValid() {
        boolean valid = true;
        if ( id == null && !Validate.isValid(name)) {
            Log.d(TAG,"name false");
            nameError.set(Validate.error);
            valid = false;
            Timber.e("type:error");
        }
        Log.d(TAG,AppMoon.isFamous()+"");
        if(AppMoon.isFamous() && id == null && !Validate.isValid(service)){
            Log.d(TAG,"service false");
            categoryError.set(Validate.error);
            valid = false;
        }
        if(AppMoon.isFamous() && id == null && !Validate.isValid(category)){
            Log.d(TAG,"category false");
            serviceError.set(Validate.error);
            valid = false;
        }
        if(AppMoon.isFamous() && !Validate.isValid(adsExpire)){
            Log.d(TAG,"adsExpire false");
            adsExpiredError.set(Validate.error);
            valid = false;
        }
        return valid;
    }

    public boolean isValidAlbum() {
        boolean valid = true;
        if ( id == null && !Validate.isValid(name)) {
            Log.d(TAG,"name false");
            nameError.set(Validate.error);
            valid = false;
            Timber.e("type:error");
        }
        return valid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
        nameError.set(null);
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
        serviceError.set(null);
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
        categoryError.set(null);
    }

    public String getAdsExpire() {
        return adsExpire;
    }

    public void setAdsExpire(String adsExpire) {
        this.adsExpire = adsExpire;
        adsExpiredError.set(null);
    }
}
