package grand.app.moonshop.models.reservation.doctor;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import grand.app.moonshop.models.base.IdName;
import grand.app.moonshop.models.base.StatusMsg;

public class SpecificationResponse extends StatusMsg {
    @SerializedName("data")
    @Expose
    public List<IdName> specifications;
}
