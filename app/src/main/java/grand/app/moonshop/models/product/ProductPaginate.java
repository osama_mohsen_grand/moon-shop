
package grand.app.moonshop.models.product;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import grand.app.moonshop.models.base.PaginationModel;

public class ProductPaginate extends PaginationModel {

    @SerializedName("data")
    @Expose
    public List<Product> products;

}
