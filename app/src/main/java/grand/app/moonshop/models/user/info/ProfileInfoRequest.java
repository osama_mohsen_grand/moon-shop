package grand.app.moonshop.models.user.info;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import androidx.databinding.ObservableField;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.utils.Validate;
import grand.app.moonshop.utils.storage.user.UserHelper;

public class ProfileInfoRequest {
    @SerializedName("id")
    @Expose
    public String id;
    @SerializedName("bank_name")
    @Expose
    public String bankName = "";
    @SerializedName("account_number")
    @Expose
    public String accountNumber = "";

    @SerializedName("country_id")
    @Expose
    public String country_id = "";


    @SerializedName("country")
    @Expose
    public transient String country = "";
    @SerializedName("city")
    @Expose
    public transient String city = "";

    @SerializedName("city_id")
    @Expose
    public String city_id = "";

    @SerializedName("region")
    @Expose
    public transient String region = "";

    @SerializedName("region_id")
    @Expose
    public String region_id = "";

    @SerializedName("notes")
    @Expose
    public String notes = "";

    @SerializedName("type")
    @Expose
    public String type = UserHelper.getUserDetails().type;


    public transient ObservableField bankNameError;
    public transient ObservableField accountNumberError;
    public transient ObservableField notesError;
    public transient ObservableField countryError;
    public transient ObservableField cityError;
    public transient ObservableField regionError;
//    public transient ObservableField subCategoryError;

    public ProfileInfoRequest() {
        bankNameError = new ObservableField();
        accountNumberError = new ObservableField();
        notesError = new ObservableField();
        countryError = new ObservableField();
        cityError = new ObservableField();
        regionError = new ObservableField();
    }

    public boolean isValid() {
        boolean valid = true;
        if (!Validate.isValid(bankName)) {
            bankNameError.set(Validate.error);
            valid = false;
        } else
            bankNameError.set(null);

        if (!Validate.isValid(accountNumber)) {
            accountNumberError.set(Validate.error);
            valid = false;
        } else
            accountNumberError.set(null);

        if (!Validate.isValid(notes)) {
            notesError.set(Validate.error);
            valid = false;
        } else
            notesError.set(null);


        if (!Validate.isValid(country)) {
            countryError.set(Validate.error);
            valid = false;
        } else
            countryError.set(null);


        if (!Validate.isValid(city)) {
            cityError.set(Validate.error);
            valid = false;
        } else
            cityError.set(null);

        if (!Validate.isValid(region)) {
            regionError.set(Validate.error);
            valid = false;
        } else
            regionError.set(null);



        return valid;
    }


    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
        countryError.set(null);
    }

    public String getCountry_id() {
        return country_id;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
        cityError.set(null);
    }

    public String getCity_id() {
        return city_id;
    }

    public void setCity_id(String city_id) {
        this.city_id = city_id;
        cityError.set(null);
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
        regionError.set(null);
    }

    public String getRegion_id() {
        return region_id;
    }

    public void setRegion_id(String region_id) {
        this.region_id = region_id;
        regionError.set(null);
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
        bankNameError.set(null);
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
        accountNumberError.set(null);
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
        notesError.set(null);
    }
}
