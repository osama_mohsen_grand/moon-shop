package grand.app.moonshop.models.user.forgetpassword;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import grand.app.moonshop.models.base.StatusMsg;


public class ForgetPasswordResponse extends StatusMsg {

    @SerializedName("data")
    @Expose
    public String data;

}
