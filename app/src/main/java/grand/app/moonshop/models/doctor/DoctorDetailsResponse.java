package grand.app.moonshop.models.doctor;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import grand.app.moonshop.models.base.StatusMsg;

public class DoctorDetailsResponse extends StatusMsg {
    @SerializedName("data")
    @Expose
    public DoctorDetails data;
}
