package grand.app.moonshop.models.chat.details;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import grand.app.moonshop.models.base.StatusMsg;
import grand.app.moonshop.models.chat.ChatDelegate;
import grand.app.moonshop.models.chat.ChatUser;

public class ChatDetailsResponse extends StatusMsg {
    @SerializedName("chats")
    @Expose
    public List<ChatDetailsModel> chats = null;

    @SerializedName("user")
    @Expose
    public ChatUser user = null;

    @SerializedName("delegate")
    @Expose
    public ChatDelegate delegate = null;

}
