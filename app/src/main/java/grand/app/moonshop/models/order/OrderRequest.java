package grand.app.moonshop.models.order;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import androidx.databinding.ObservableField;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.utils.Validate;
import timber.log.Timber;

public class OrderRequest {
    @SerializedName("payment_method")
    @Expose
    public String payment_method = "1";
    @SerializedName("company_id")
    @Expose
    public String company_id;
    @SerializedName("delivery_method")
    @Expose
    public String delivery_method = "1";

    @SerializedName("name")
    @Expose
    private String name = "";

    @SerializedName("phone")
    @Expose
    private String phone = "";

    @SerializedName("lat")
    @Expose
    private double lat = 0;

    @SerializedName("lng")
    @Expose
    private double lng = 0;

    public String address = "";

    public ObservableField nameError;
    public ObservableField phoneError;
    public ObservableField addressError;

    public OrderRequest() {
        nameError = new ObservableField();
        phoneError = new ObservableField();
        addressError = new ObservableField();
    }

    public boolean isValid() {
        boolean valid = true;
        if (!Validate.isValid(name)) {
            nameError.set(Validate.error);
            valid = false;
            Timber.e("name:error");
        }
        if (!Validate.isValid(phone, Constants.PHONE)) {
            phoneError.set(Validate.error);
            valid = false;
            Timber.e("phone:error");
        }

        if (!Validate.isValid(address)) {
            addressError.set(Validate.error);
            valid = false;
            Timber.e("address:error");
        }
        return valid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
        nameError.set(null);
    }


    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
        phoneError.set(null);
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
        addressError.set(null);
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }
}
