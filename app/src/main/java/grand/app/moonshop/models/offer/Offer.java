
package grand.app.moonshop.models.offer;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Offer implements Serializable {

    @SerializedName("id")
    @Expose
    public int mId;
    @SerializedName("type")
    @Expose
    public int mType;
    @SerializedName("image")
    @Expose
    public String image;
    @SerializedName("capture")
    @Expose
    public String capture;
}
