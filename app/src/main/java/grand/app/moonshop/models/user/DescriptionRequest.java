package grand.app.moonshop.models.user;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import grand.app.moonshop.utils.storage.user.UserHelper;

public class DescriptionRequest {
    @SerializedName("description")
    @Expose
    public String description;

    @SerializedName("type")
    @Expose
    public String type = UserHelper.getUserDetails().type;


    public DescriptionRequest(String description) {
        this.description = description;
    }
}
