package grand.app.moonshop.models.order;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import grand.app.moonshop.models.base.StatusMsg;

public class OrderListResponse extends StatusMsg {
    @SerializedName("data")
    @Expose
    public List<Order> orders;
}
