package grand.app.moonshop.models.chat;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import grand.app.moonshop.models.app.AppMoon;
import grand.app.moonshop.models.app.AppMoon;

public class ChatRequest {
    @SerializedName("id")
    @Expose
    public int id;

    @SerializedName("order_id")
    @Expose
    public int order_id;

    @SerializedName("receiver_id")
    @Expose
    public int receiver_id;

    
    @SerializedName("message")
    @Expose
    public String message;

    @SerializedName("receiver_type")
    @Expose
    public int receiver_type;

    public boolean firstChat;

    @SerializedName("type")
    @Expose
    public String type = AppMoon.getUserType();

    public ChatRequest(int order_id , int receiver_id, String message, int receiver_type,boolean first_chat) {
        this.id = order_id;
        this.order_id = order_id;
        this.receiver_id = receiver_id;
        this.message = message;
        this.receiver_type = receiver_type;
        this.firstChat = first_chat;
    }
}
