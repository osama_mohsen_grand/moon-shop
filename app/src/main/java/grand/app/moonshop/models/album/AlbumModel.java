
package grand.app.moonshop.models.album;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class AlbumModel implements Serializable {

    @SerializedName("id")
    @Expose
    public Integer id;
    @SerializedName("capture")
    @Expose
    public String capture = "";
    @SerializedName("image")
    @Expose
    public String image;
    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("type")
    @Expose
    public Integer type;
    @SerializedName("count")
    @Expose
    public Integer count;

    @SerializedName("video_count")
    @Expose
    public Integer videoCount;
}
