package grand.app.moonshop.models.user.info;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import grand.app.moonshop.utils.storage.user.UserHelper;

public class SocialMediaRequest {
    @SerializedName("email")
    @Expose
    public String email = "";
    @SerializedName("phone")
    @Expose
    public String phone = "";
    @SerializedName("website")
    @Expose
    public String website = "";

    @SerializedName("type")
    @Expose
    public String type = UserHelper.getUserDetails().type;

    @SerializedName("social_type")
    @Expose
    public ArrayList<Integer> socialIds = new ArrayList<>();

    @SerializedName("media")
    @Expose
    public ArrayList<String> socialMedia = new ArrayList<>();
}
