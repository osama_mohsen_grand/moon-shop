package grand.app.moonshop.models.ads;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import grand.app.moonshop.models.base.IdNameImage;
import grand.app.moonshop.models.base.StatusMsg;
import grand.app.moonshop.models.famous.home.ImageVideo;

public class AdsResponse extends StatusMsg {
    @SerializedName("data")
    @Expose
    public List<IdNameImage> data = null;
}