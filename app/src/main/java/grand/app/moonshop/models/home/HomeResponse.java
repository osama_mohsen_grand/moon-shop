package grand.app.moonshop.models.home;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

import grand.app.moonshop.models.base.StatusMsg;

public class HomeResponse extends StatusMsg implements Serializable {
    @SerializedName("data")
    @Expose
    public List<Datum> data = null;
}
