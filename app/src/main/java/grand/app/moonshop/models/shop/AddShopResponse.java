package grand.app.moonshop.models.shop;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import grand.app.moonshop.models.base.IdName;
import grand.app.moonshop.models.base.StatusMsg;
import grand.app.moonshop.models.user.profile.User;


public class AddShopResponse extends StatusMsg {

    @SerializedName("data")
    @Expose
    public List<IdName> suggestions;


    @SerializedName("shop")
    @Expose
    public User data;
}
