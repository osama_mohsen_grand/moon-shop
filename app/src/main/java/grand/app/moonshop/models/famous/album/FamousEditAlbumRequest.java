package grand.app.moonshop.models.famous.album;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import androidx.databinding.ObservableField;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.utils.Validate;
import grand.app.moonshop.utils.storage.user.UserHelper;

public class FamousEditAlbumRequest {

    @SerializedName("id")
    @Expose
    public int id;

    @SerializedName("name")
    @Expose
    private String name = "";

    @SerializedName("tab")
    @Expose
    private String tab = "";

    @SerializedName("type")
    @Expose
    public String type = UserHelper.getUserDetails().type;

    public ObservableField nameError;

    public FamousEditAlbumRequest(int id, String name,String tab) {
        this.id = id;
        this.name = name;
        this.tab = tab;
        nameError = new ObservableField();
    }

    public boolean isValid() {
        boolean valid = true;
        if (!Validate.isValid(name)) {
            nameError.set(Validate.error);
            valid = false;
        } else
            nameError.set(null);
        return valid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
        nameError.set(null);
    }
}
