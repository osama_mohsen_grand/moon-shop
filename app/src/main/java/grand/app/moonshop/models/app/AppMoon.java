package grand.app.moonshop.models.app;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import java.util.ArrayList;
import java.util.List;

import grand.app.moonshop.R;
import grand.app.moonshop.models.accounttype.AccountTypeItem;
import grand.app.moonshop.models.accounttype.AcountTypeResponse;
import grand.app.moonshop.models.country.Datum;
import grand.app.moonshop.models.user.profile.User;
import grand.app.moonshop.notification.NotificationGCMModel;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.utils.resources.ResourceManager;
import grand.app.moonshop.utils.storage.user.UserHelper;
import grand.app.moonshop.views.activities.MainActivity;
import timber.log.Timber;

public class AppMoon {
    public static ArrayList<AccountTypeModel> accountType = new ArrayList<>();

    public static ArrayList<AccountTypeModel> getAccountsType() {

        AcountTypeResponse acountTypeResponse  = UserHelper.getAccountTypes();

        accountType.clear();
        for(AccountTypeItem accountTypeItem : acountTypeResponse.getData()){
            accountType.add(new AccountTypeModel(accountTypeItem.getId(),accountTypeItem.getName(),accountTypeItem.getFlag()));
        }

        return accountType;
    }

    public static ArrayList<AccountTypeModel> getAccountsType(List<AccountTypeItem> data) {
        accountType.clear();

        for(int i=0; i<data.size(); i++) {
            accountType.add(new AccountTypeModel(data.get(i).getId()+"", data.get(i).getName(),data.get(i).getFlag()));
        }

        return accountType;
    }


    public static ArrayList<String> photographerList() {
        ArrayList<String> popUpModels = new ArrayList<>();
        popUpModels.add(ResourceManager.getString(R.string.all)); // 3
        popUpModels.add(ResourceManager.getString(R.string.designers)); // 1
        popUpModels.add(ResourceManager.getString(R.string.photographers)); // 2
        return popUpModels;
    }

    public static int getPhotograhperTypeId(String text) {
        if (text.equals(ResourceManager.getString(R.string.all))) return Constants.GENDER_ALL;
        if (text.equals(ResourceManager.getString(R.string.designers))) return Constants.DESIGNERS;
        else return Constants.PHOTOGRAPHERS;
    }


    public static ArrayList<String> getServiceType(boolean showData) {
        ArrayList<String> serviceType = new ArrayList<>();

          if (showData) {
            serviceType.add(ResourceManager.getString(R.string.sector));
              serviceType.add(ResourceManager.getString(R.string.wholesale));
            serviceType.add(ResourceManager.getString(R.string.wholesale_and_sector));

            }
//        if (id == 1) {
//            serviceType.add(ResourceManager.getString(R.string.wholesale));
//            serviceType.add(ResourceManager.getString(R.string.sector));
//            serviceType.add(ResourceManager.getString(R.string.wholesale_and_sector));
//            serviceType.add(ResourceManager.getString(R.string.home_consumer));
//        } else if (id == 2) {
//            serviceType.add(ResourceManager.getString(R.string.sector));
//            serviceType.add(ResourceManager.getString(R.string.wholesale_and_sector));
//            serviceType.add(ResourceManager.getString(R.string.home_service));
//        } else if (id == 3) {
//            serviceType.add(ResourceManager.getString(R.string.consumer));
//            serviceType.add(ResourceManager.getString(R.string.service));
//        }
        // 4 - 5 not have service type
        return serviceType;
    }

    public static String getServiceId(String typeSelect) {
        if (typeSelect.equals(ResourceManager.getString(R.string.sector)) || typeSelect.equals(ResourceManager.getString(R.string.consumer)))
            return "1";
        else if (typeSelect.equals(ResourceManager.getString(R.string.wholesale)) || typeSelect.equals(ResourceManager.getString(R.string.service)))
            return "2";
        else if (typeSelect.equals(ResourceManager.getString(R.string.wholesale_and_sector)))
            return "1,2";
        else if (typeSelect.equals(ResourceManager.getString(R.string.home_consumer)) || typeSelect.equals(ResourceManager.getString(R.string.home_service)))
            return "3";
        return "";
    }

    public static String getUserType() {
        Timber.e("type:" + UserHelper.getUserDetails().type);
        return UserHelper.getUserDetails().type;
    }

    public static boolean isValidAction(int id , String type) {
        return type.equals(getUserType()) && UserHelper.getUserDetails().id == id;
    }


    /*
            menu.add(new MenuModel(Constants.HOME, context.getString(R.string.label_home), ResourceManager.getDrawable(R.drawable.ic_home_primary), ""));
        menu.add(new MenuModel(Constants.PROFILE, context.getString(R.string.profile), ResourceManager.getDrawable(R.drawable.ic_profile_primary), ""));
        menu.add(new MenuModel(Constants.MY_ADS, context.getString(R.string.my_ads), ResourceManager.getDrawable(R.drawable.ic_story_primary), ""));
        menu.add(new MenuModel(Constants.STORY, context.getString(R.string.story), ResourceManager.getDrawable(R.drawable.ic_story_primary), ""));
        menu.add(new MenuModel(Constants.CHAT, context.getString(R.string.chat_history), ResourceManager.getDrawable(R.drawable.ic_chat_menu_primary), ""));
        menu.add(new MenuModel(Constants.GALLERY, context.getString(R.string.gallery), ResourceManager.getDrawable(R.drawable.ic_story_primary), ""));
        menu.add(new MenuModel(Constants.ORDERS, context.getString(R.string.orders), ResourceManager.getDrawable(R.drawable.ic_followers_primary), ""));
        menu.add(new MenuModel(Constants.REVIEW, context.getString(R.string.reviews), ResourceManager.getDrawable(R.drawable.ic_story_primary), ""));
        menu.add(new MenuModel(Constants.FAMOUS, context.getString(R.string.famous_people), ResourceManager.getDrawable(R.drawable.ic_story_primary), ""));
        menu.add(new MenuModel(Constants.CREDIT, context.getString(R.string.credit_card), ResourceManager.getDrawable(R.drawable.ic_credit_primary), ""));
        menu.add(new MenuModel(Constants.NOTIFICATION, context.getString(R.string.label_notification), ResourceManager.getDrawable(R.drawable.ic_notification_primary), ""));
        menu.add(new MenuModel(Constants.PHOTOGRAPHER, context.getString(R.string.photographer_people), ResourceManager.getDrawable(R.drawable.ic_story_primary), ""));
        menu.add(new MenuModel(Constants.SUPPORT, context.getString(R.string.help_and_support), ResourceManager.getDrawable(R.drawable.ic_help_and_support_primary), ""));
        menu.add(new MenuModel(Constants.PRIVACY_POLICY, context.getString(R.string.label_privacy_policy), ResourceManager.getDrawable(R.drawable.ic_be_privacy_primary), ""));
        menu.add(new MenuModel(Constants.LANG, context.getString(R.string.label_language), ResourceManager.getDrawable(R.drawable.ic_language_primary), ""));
        menu.add(new MenuModel(Constants.SHARE, context.getString(R.string.label_share_app), ResourceManager.getDrawable(R.drawable.ic_share_primary), ""));
        menu.add(new MenuModel(Constants.RATE, context.getString(R.string.rate_app), ResourceManager.getDrawable(R.drawable.ic_star_primary), ""));
        menu.add(new MenuModel(Constants.BE_SHOP, context.getString(R.string.be_shop), ResourceManager.getDrawable(R.drawable.ic_shop_menu_primary), ""));
        menu.add(new MenuModel(Constants.LOGOUT, context.getString(R.string.label_logout), ResourceManager.getDrawable(R.drawable.ic_logout_primary), ""));
        AppUtils.initVerticalRV(layoutNavigationDrawerBinding.rvNavigationDrawerList, context, 1);
     */
    private static final String TAG = "AppMoon";
    public static boolean hasAuthorizedMenu(String id) {
        boolean valid = true;
        Timber.e("id:" + id);
        if (id.equals(Constants.HOME)) {
            return true;
        }
        if (id.equals(Constants.PROFILE)) {
            return true;
        }
        if (id.equals(Constants.MY_ADS)) {
            if (!getUserType().equals(Constants.TYPE_PHOTOGRAPHER) &&
                    !getUserType().equals(Constants.TYPE_FAMOUS_PEOPLE)) {
                valid = false;
            } else valid = true;
        }
        if (id.equals(Constants.STORY)) {
            return true;
        }
        if (id.equals(Constants.GALLERY)) {
            if (!getUserType().equals(Constants.TYPE_MARKET_SERVICE) &&
                    !getUserType().equals(Constants.TYPE_CONSUMER_MARKET) &&
                    !getUserType().equals(Constants.TYPE_INDUSTRIES) &&
                    !getUserType().equals(Constants.TYPE_PHOTOGRAPHER) && !getUserType().equals(Constants.TYPE_FAMOUS_PEOPLE)) {
                valid = true;
            } else valid = false;
        }
        if (id.equals(Constants.ORDERS)) {
            if (!getUserType().equals(Constants.TYPE_PHOTOGRAPHER) && !getUserType().equals(Constants.TYPE_FAMOUS_PEOPLE)
//                    && !getUserType().equals(Constants.TYPE_RESERVATION_BEAUTY)
                    && !getUserType().equals(Constants.TYPE_COMMERCIALS) && !getUserType().equals(Constants.TYPE_INSTITUTIONS)) {
                valid = true;
            } else valid = false;
//            Log.d(TAG,"=>"+valid);
        }
        if (id.equals(Constants.REVIEW)) {
            if (!getUserType().equals(Constants.TYPE_PHOTOGRAPHER) && !getUserType().equals(Constants.TYPE_FAMOUS_PEOPLE)) {
                valid = true;
            } else valid = false;
        }
        if (id.equals(Constants.FAMOUS)) {
            if (!getUserType().equals(Constants.TYPE_PHOTOGRAPHER) && !getUserType().equals(Constants.TYPE_FAMOUS_PEOPLE)) {
                valid = true;
            } else valid = false;
        }
        if (id.equals(Constants.PHOTOGRAPHER)) {
            if (!getUserType().equals(Constants.TYPE_PHOTOGRAPHER) && !getUserType().equals(Constants.TYPE_FAMOUS_PEOPLE)) {
                valid = true;
            } else valid = false;
        }
        if (id.equals(Constants.BE_SHOP)) {
            if ((!getUserType().equals(Constants.TYPE_PHOTOGRAPHER) && !getUserType().equals(Constants.TYPE_FAMOUS_PEOPLE))
                    || (getUserType().equals(Constants.TYPE_PHOTOGRAPHER) && UserHelper.getUserDetails().isShop)
                    || (getUserType().equals(Constants.TYPE_FAMOUS_PEOPLE) && UserHelper.getUserDetails().isShop)
                    || (getUserType().equals(Constants.TYPE_RESERVATION_BEAUTY) && UserHelper.getUserDetails().isShop)
            ) {
                valid = false;
            } else valid = true;
        }
        if (id.equals(Constants.CHAT)) {
            return true;
        }
        if (id.equals(Constants.CREDIT)) {
            return true;
        }
        if (id.equals(Constants.SUPPORT)) {
            return true;
        }
        if (id.equals(Constants.NOTIFICATION)) {
            return true;
        }
        if (id.equals(Constants.LANG)) {
            return true;
        }
        if (id.equals(Constants.PRIVACY_POLICY)) {
            return true;
        }
        if (id.equals(Constants.SHARE)) {
            return true;
        }
        if (id.equals(Constants.RATE)) {
            return true;

        }
        if (id.equals(Constants.LOGOUT)) {
            return true;
        }
        return valid;
    }

    public static boolean searchVisibility(String id) {
        if (id.equals(Constants.HOME)) {
            if (AppMoon.getUserType().equals(Constants.TYPE_FAMOUS_PEOPLE) || AppMoon.getUserType().equals(Constants.TYPE_PHOTOGRAPHER))
                return true;
            else
                return false;
        } else if (id.equals(Constants.PHOTOGRAPHER) || id.equals(Constants.FAMOUS)) {
            return true;
        }
        return false;
    }

    public static ArrayList<String> genderList() {
        ArrayList<String> popUpModels = new ArrayList<>();
        popUpModels.add(ResourceManager.getString(R.string.all)); // 3
        popUpModels.add(ResourceManager.getString(R.string.men)); // 1
        popUpModels.add(ResourceManager.getString(R.string.women)); // 2
        return popUpModels;
    }

    public static int getGenderId(String text) {
        if (text.equals(ResourceManager.getString(R.string.all))) return Constants.GENDER_ALL;
        if (text.equals(ResourceManager.getString(R.string.men))) return Constants.GENDER_MALE;
        else return Constants.GENDER_WOMAN;
    }

    public static boolean showHeaderMenu() {
        if (UserHelper.getUserDetails().isShop) {
            return true;
        }
        return false;
    }


    public static Datum getCities(String countryId, List<Datum> countries) {
        for (Datum country : countries) {
            if (String.valueOf(country.id).equals(countryId)) {
                return country;
            }
        }
        return new Datum();
    }


    public static boolean isFamous() {
        if (AppMoon.getUserType().equals(Constants.TYPE_PHOTOGRAPHER) || AppMoon.getUserType().equals(Constants.TYPE_FAMOUS_PEOPLE)
                || (getUserType().equals(Constants.TYPE_PHOTOGRAPHER) && UserHelper.getUserDetails().isShop)
                || (getUserType().equals(Constants.TYPE_FAMOUS_PEOPLE) && UserHelper.getUserDetails().isShop))
            return true;
        return false;
    }

    //type - id;
    public static void changeType(Context context){
        User user = UserHelper.getUserDetails();

        user.prevId = user.id;
        user.prevType = user.type;
        user.prevImage = user.image;
        user.prevName = user.name;

        user.id = user.shop_famous_id;
        user.type = user.shop_famous_type;
        user.image = user.shop_famous_image;
        user.name = user.shop_famous_name;

        user.shop_famous_id = user.prevId;
        user.shop_famous_type = user.prevType;
        user.shop_famous_image = user.prevImage;
        user.shop_famous_name = user.prevName;


        UserHelper.saveUserDetails(user);
        ((MainActivity) context).finishAffinity();
        context.startActivity(new Intent(context,MainActivity.class));
    }

//    public static void beShop(Context context){
//        User user = UserHelper.getUserDetails();
//
//        user.prevId = user.id;
//        user.prevType = user.type;
//        user.id = user.shop_famous_id;
//        user.type = user.shop_famous_type;
//        user.shop_famous_id = user.prevId;
//        user.shop_famous_type = user.prevType;
//
//
//        UserHelper.saveUserDetails(user);
//        ((MainActivity) context).finishAffinity();
//        context.startActivity(new Intent(context,MainActivity.class));
//    }

    public static boolean allowSize() {
        if (UserHelper.getUserDetails().shopService == 1) return true;
        else if (UserHelper.getUserDetails().shopService == 2) return true;
        return false;
    }


    public static boolean allowColor() {
        if (UserHelper.getUserDetails().shopService == 1) return true;
        return false;
    }


    public static NotificationGCMModel getNotification(String mJsonString) {
        JsonParser parser = new JsonParser();
        JsonElement mJson = parser.parse(mJsonString);
        Gson gson = new Gson();
        return gson.fromJson(mJson, NotificationGCMModel.class);
    }



    public int getAccountTypeId(String name) {
        return accountType.indexOf(name) + 1;
    }


    public static String html(String guide) {
        String html = "";

        html += "<!doctype html>\n" +
                "<html>\n" +
                "<head>\n" +
                "    <link href='https://fonts.googleapis.com/css?family=Cairo&display=swap' rel='stylesheet'>\n" +
                "</head>\n" +
                "<style>    * {\n" +
                "        font-family: 'Cairo', sans-serif;\n" +
                "    }\n" +
                "\n" +
                "    body {\n" +
                "        font-family: 'Cairo', sans-serif;\n" +
                "        background-image: url('https://mnaskco.com/mnasekfiles/resources/assets/site/540.jpg');\n" +
                "    }\n" +
                "\n" +
                "    .rtl {\n" +
                "        direction: rtl;\n" +
                "    }</style>\n" +
                "    <body background=\"https://mnaskco.com/mnasekfiles/resources/assets/site/540.jpg\">\n" +
                "    <div style='text-align:center' class='rtl'><h2 style=\"font-family: 'Cairo', sans-serif;\"><font color='#5BC346'>\n" +
                "                " + ResourceManager.getString(R.string.app_name) + "</font></h2></div>\n" +
                "    <div style=\"margin: 0 auto; display: table;\">\n" +
                "\n" +
                "        <audio controls=\"controls\" controlsList=\"nodownload\">\n" +
                "            <source src=\"" + guide + "\" type=\"audio/mpeg\">\n" +
                "        </audio>\n" +
                "    </div>\n" +
                "    </body>\n" +
                "</html>\n";

        return html;
    }


}
