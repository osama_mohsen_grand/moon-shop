package grand.app.moonshop.models.accounttype;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.SerializedName;

import grand.app.moonshop.models.base.StatusMsg;

public class AcountTypeResponse extends StatusMsg implements Serializable {

	@SerializedName("data")
	private List<AccountTypeItem> data = new ArrayList<>();

	public void setData(List<AccountTypeItem> data){
		this.data = data;
	}

	public List<AccountTypeItem> getData(){
		return data;
	}

}