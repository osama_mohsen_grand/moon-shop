package grand.app.moonshop.models.institution;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import androidx.databinding.ObservableField;
import grand.app.moonshop.models.app.AppMoon;
import grand.app.moonshop.utils.Validate;
import grand.app.moonshop.utils.storage.user.UserHelper;
import grand.app.moonshop.vollyutils.VolleyFileObject;
import timber.log.Timber;

public class AddServiceRequest {

    public transient VolleyFileObject volleyFileObject = null;

    @SerializedName("name")
    @Expose
    private String name = "";

    @SerializedName("type")
    @Expose
    public String type = UserHelper.getUserDetails().type;

    public transient ObservableField nameError;

    public AddServiceRequest() {
        nameError = new ObservableField();
    }


    public boolean isValid() {
        boolean valid = true;

        if(!Validate.isValid(name)) {
            nameError.set(Validate.error);
            valid = false;
            Timber.e("address:error");
        }
        return valid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
        nameError.set(null);
    }
}
