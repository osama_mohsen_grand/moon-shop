
package grand.app.moonshop.models.review;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import grand.app.moonshop.models.base.StatusMsg;

public class ReviewResponse extends StatusMsg {

    @SerializedName("data")
    @Expose
    public List<Review> mData;

}
