package grand.app.moonshop.models.reservation.detatils;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import grand.app.moonshop.models.base.StatusMsg;


public class ReservationDetailsResponse extends StatusMsg {
    @SerializedName("data")
    @Expose
    public ReservationDetailsModel reservationDetailsModel;

}
