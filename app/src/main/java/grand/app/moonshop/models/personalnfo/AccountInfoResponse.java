package grand.app.moonshop.models.personalnfo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import androidx.databinding.ObservableField;
import grand.app.moonshop.models.base.IdNameImage;
import grand.app.moonshop.models.base.StatusMsg;
import grand.app.moonshop.models.user.info.Profile;
import grand.app.moonshop.viewmodels.profile.Transfer;

public class AccountInfoResponse extends StatusMsg implements Serializable {
    @SerializedName("name")
    @Expose
    public String name = "";
    @SerializedName("email")
    @Expose
    public String email = "";
    @SerializedName("image")
    @Expose
    public String image = "";
    @SerializedName("number")
    @Expose
    public String number = "";
    @SerializedName("followers")
    @Expose
    public String followers = "";
    @SerializedName("website")
    @Expose
    public String website = "";
    @SerializedName("description")
    @Expose
    public String description = "";

    @SerializedName("lat")
    @Expose
    public double lat = 0;
    @SerializedName("lng")
    @Expose
    public double lng = 0;

    @SerializedName("info_services")
    @Expose
    public List<IdNameImage> infoServices = null;
    @SerializedName("profile")
    @Expose
    public List<Profile> profile = new ArrayList<>();
    @SerializedName("transfer")
    @Expose
    public Transfer transfer = null;


    public transient ObservableField emailError = new ObservableField();
    public transient ObservableField numberError = new ObservableField();
    public transient ObservableField websiteError = new ObservableField();

    public AccountInfoResponse getAccountInfo(){
        return this;
    }

}
