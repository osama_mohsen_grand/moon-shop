package grand.app.moonshop.models.ads;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import grand.app.moonshop.models.base.IdNameImage;


public class AdsDetailsModel {

    @SerializedName("id")
    @Expose
    public Integer id;
    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("image")
    @Expose
    public String image;
    @SerializedName("description")
    @Expose
    public String description;
    @SerializedName("email")
    @Expose
    public String email;
    @SerializedName("phone")
    @Expose
    public String phone;
    @SerializedName("lat")
    @Expose
    public double lat;
    @SerializedName("lng")
    @Expose
    public double lng;
    @SerializedName("address")
    @Expose
    public String address;
    @SerializedName("price")
    @Expose
    public String price;
    @SerializedName("city")
    @Expose
    public String city;

    @SerializedName("city_id")
    @Expose
    public String city_id;
    @SerializedName("product_images")
    @Expose
    public List<IdNameImage> productImages = null;
}
