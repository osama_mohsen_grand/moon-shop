
package grand.app.moonshop.models.product;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import grand.app.moonshop.models.base.StatusMsg;

public class ProductsResponse extends StatusMsg {

    @SerializedName("data")
    @Expose
    public ProductPaginate mData;



}
