package grand.app.moonshop.models.order.details;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import grand.app.moonshop.models.base.StatusMsg;

public class OrderDetailsResponse extends StatusMsg {
    @SerializedName("data")
    @Expose
    public OrderDetails data;
}
