package grand.app.moonshop.models.settings;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import grand.app.moonshop.models.base.StatusMsg;


public class SettingsResponse extends StatusMsg {
    @SerializedName("data")
    @Expose
    public List<Settings> data;
}
