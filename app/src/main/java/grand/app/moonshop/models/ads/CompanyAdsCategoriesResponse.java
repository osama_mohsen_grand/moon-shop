package grand.app.moonshop.models.ads;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import grand.app.moonshop.models.base.StatusMsg;

public class CompanyAdsCategoriesResponse extends StatusMsg {
    @SerializedName("data")
    @Expose
    public List<Datum> data = null;
}
