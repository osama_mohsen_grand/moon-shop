
package grand.app.moonshop.models.famous.home;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import grand.app.moonshop.models.album.AlbumModel;
import grand.app.moonshop.models.base.StatusMsg;

public class FamousHomeResponse extends StatusMsg {

    @SerializedName("data")
    @Expose
    public List<AlbumModel> data = null;
}
