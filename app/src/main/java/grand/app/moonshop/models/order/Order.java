package grand.app.moonshop.models.order;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Order {
    @SerializedName("id")
    @Expose
    public Integer id;
    @SerializedName("order_number")
    @Expose
    public String orderNumber;
    @SerializedName("order_date")
    @Expose
    public String orderDate;
    @SerializedName("order_status")
    @Expose
    public String order_status;
    @SerializedName("order_count")
    @Expose
    public String orderCount = "0";

}
