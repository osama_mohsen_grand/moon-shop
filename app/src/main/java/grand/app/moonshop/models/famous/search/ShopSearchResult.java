package grand.app.moonshop.models.famous.search;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ShopSearchResult {

    @SerializedName("id")
    @Expose
    public Integer id;
    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("description")
    @Expose
    public String description;

    @SerializedName("type")
    @Expose
    public String type;

    public ShopSearchResult(Integer id, String name, String description) {
        this.id = id;
        this.name = name;
        this.description = description;
    }
}
