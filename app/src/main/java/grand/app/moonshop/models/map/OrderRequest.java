package grand.app.moonshop.models.map;

import com.google.android.gms.maps.model.Marker;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class OrderRequest {
    @SerializedName("type")
    @Expose
    public String type;
    @SerializedName("car_id")
    @Expose
    public int car_id;
    @SerializedName("user_id")
    @Expose
    public int user_id;
    @SerializedName("lat")
    @Expose
    public ArrayList<Double> lat;
    @SerializedName("lng")
    @Expose
    public ArrayList<Double> lng;
    @SerializedName("address")
    @Expose
    public ArrayList<String> address;
    @SerializedName("date")
    @Expose
    public String date;
    @SerializedName("time")
    @Expose
    public String time;

    @SerializedName("google_distance")
    @Expose
    public String google_distance;

    public OrderRequest(String type , int car_id , int user_id, ArrayList<Marker> markers, String google_distance){
        this.type = type;
        this.car_id = car_id;
        this.user_id = user_id;
        lat = new ArrayList<>();
        lng = new ArrayList<>();
        address = new ArrayList<>();
        for(Marker marker : markers){
            if(marker != null) {
                lat.add(marker.getPosition().latitude);
                lng.add(marker.getPosition().longitude);
                address.add(marker.getTitle());
            }
        }
        this.google_distance = google_distance;
    }

    public OrderRequest(String type, int car_id, int user_id, ArrayList<Double> lat, ArrayList<Double> lng, ArrayList<String> address) {
        this.type = type;
        this.car_id = car_id;
        this.user_id = user_id;
        this.lat = lat;
        this.lng = lng;
        this.address = address;
    }

    public OrderRequest(String type, int car_id, int user_id, ArrayList<Double> lat, ArrayList<Double> lng, ArrayList<String> address, String date, String time) {
        this.type = type;
        this.car_id = car_id;
        this.user_id = user_id;
        this.lat = lat;
        this.lng = lng;
        this.address = address;
        this.date = date;
        this.time = time;
    }
}
