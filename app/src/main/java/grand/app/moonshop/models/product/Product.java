
package grand.app.moonshop.models.product;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;


public class Product implements Serializable {

    @SerializedName("description")
    @Expose
    public String mDescription;
    @SerializedName("id")
    @Expose
    public int mId;
    @SerializedName("image")
    @Expose
    public String mImage;
    @SerializedName("name")
    @Expose
    public String mName;
    @SerializedName("price")
    @Expose
    public String mPrice;
    @SerializedName("type")
    @Expose
    public String mType;
    @SerializedName("rate")
    @Expose
    public Float rate;


}
