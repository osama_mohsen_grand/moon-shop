
package grand.app.moonshop.models.famous.album;

import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;
import grand.app.moonshop.base.ParentViewModel;
import grand.app.moonshop.models.app.Mutable;
import grand.app.moonshop.models.category.IdNameRequest;
import grand.app.moonshop.models.rate.RateRequest;
import grand.app.moonshop.repository.CategoryRepository;
import grand.app.moonshop.repository.FamousRepository;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.vollyutils.VolleyFileObject;

/**
 * Created by Gregory Rasmussen on 7/26/17.
 */
public class FamousEditAlbumViewModel extends ParentViewModel {
    public FamousEditAlbumRequest famousEditAlbumRequest;
    FamousRepository famousRepository;
    public String image = "";
    VolleyFileObject fileObject = null;

    public FamousEditAlbumViewModel(int id,String name,String tab,String image) {
        famousRepository = new FamousRepository(mMutableLiveData);
        famousEditAlbumRequest = new FamousEditAlbumRequest(id,name,tab);
        this.image = image;
    }

    public void changePhoto(){
        mMutableLiveData.setValue(Constants.SELECT_IMAGE);
    }

    public void submit() {
        if(famousEditAlbumRequest.isValid()){
            famousRepository.editAlbum(famousEditAlbumRequest,fileObject);
        }
    }

    public FamousRepository getFamousRepository() {
        return famousRepository;
    }

    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
    }

    public void setImage(VolleyFileObject volleyFileObject) {
        this.fileObject = volleyFileObject;
    }
}
