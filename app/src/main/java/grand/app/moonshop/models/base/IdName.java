package grand.app.moonshop.models.base;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class IdName {

    @Expose
    @SerializedName("id")
    public int id;

    @Expose
    @SerializedName("name")
    public String name;

}
