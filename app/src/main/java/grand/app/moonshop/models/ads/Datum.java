package grand.app.moonshop.models.ads;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

import grand.app.moonshop.models.base.IdNameImage;
import grand.app.moonshop.models.famous.home.ImageVideo;

//import grand.app.moonshop.models.album.ImageVideo;

public class Datum implements Serializable {
    @SerializedName("id")
    @Expose
    public Integer id;
    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("image")
    @Expose
    public String image;
    @SerializedName("count")
    @Expose
    public Integer imagesCount;
    @SerializedName("video_count")
    @Expose
    public Integer videoCount;


    @SerializedName("shop_type")
    @Expose
    public Integer shopType;
    @SerializedName("flag")
    @Expose
    public Integer flag;

    @SerializedName("ads")
    @Expose
    public List<IdNameImage> ads = null;
}
