package grand.app.moonshop.models.category;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class NameRequest {
    @SerializedName("name")
    @Expose
    public String name;

    public NameRequest(String name) {
        this.name = name;
    }
}
