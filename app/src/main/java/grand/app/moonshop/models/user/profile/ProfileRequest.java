package grand.app.moonshop.models.user.profile;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.HashMap;
import java.util.Map;

import grand.app.moonshop.utils.Constants;

public class ProfileRequest {
    @SerializedName("user_id")
    @Expose
    public int user_id = -1;
    Map<String,String> map =  new HashMap<String,String>();

    public ProfileRequest(int user_id) {
        this.user_id = user_id;
    }

    private static final String TAG = "ProfileRequest";

    public void updateRequest(String type , String message){
        map.clear();
        map.put("driver_id",""+user_id);
        if(type.equals(Constants.PHONE)) {
            map.put(Constants.PHONE,message);
        }
        if(type.equals(Constants.NAME_BAR)) {
            map.put(Constants.NAME_BAR,message);
        }
        if(type.equals(Constants.EMAIL)) {
            map.put(Constants.EMAIL,message);
        }
    }

    public Map<String, String> getMap() {
        return map;
    }
}

