package grand.app.moonshop.models.reservation.beauty;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import androidx.databinding.ObservableField;
import grand.app.moonshop.models.app.AppMoon;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.utils.Validate;
import grand.app.moonshop.utils.storage.user.UserHelper;

public class AddBeautyServiceRequest {
    @SerializedName("name")
    @Expose
    private String name = "";

    @SerializedName("price")
    @Expose
    private String price = "";

    @SerializedName("type")
    @Expose
    private String type = AppMoon.getUserType();


    public transient ObservableField nameError;
    public transient ObservableField priceError;

    public AddBeautyServiceRequest() {
        nameError = new ObservableField();
        priceError = new ObservableField();
    }


    public boolean isValid() {
        boolean valid = true;
        if (!Validate.isValid(name)) {
            nameError.set(Validate.error);
            valid = false;
        } else
            nameError.set(null);
        if (!Validate.isValid(price)) {
            priceError.set(Validate.error);
            valid = false;
        } else
            priceError.set(null);
        return valid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
        nameError.set(null);
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
        priceError.set(null);
    }
}
