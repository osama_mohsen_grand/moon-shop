package grand.app.moonshop.models.credit;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import grand.app.moonshop.models.base.StatusMsg;

public class CreditResponse extends StatusMsg {
    @SerializedName("data")
    @Expose
    public Credit data;
}
