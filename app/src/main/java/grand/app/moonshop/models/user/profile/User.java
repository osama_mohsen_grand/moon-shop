package grand.app.moonshop.models.user.profile;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import grand.app.moonshop.utils.Constants;

public class User {
    @SerializedName("id")
    @Expose
    public int id;
    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("description")
    @Expose
    public String description = "";
    @SerializedName("shop_famous_id")
    @Expose
    public int shop_famous_id;
    @SerializedName("shop_famous_name")
    @Expose
    public String shop_famous_name;
    @SerializedName("email")
    @Expose
    public String email;
    @SerializedName("phone")
    @Expose
    public String phone;
    @SerializedName("chats_count")
    @Expose
    public int chats_count = 0;

    @SerializedName("orders_count")
    @Expose
    public int order_count = 0;

    @SerializedName("notifications_count")
    @Expose
    public int notifications_count;


    @SerializedName("nick_name")
    @Expose
    public String nickName;
    @SerializedName("shop_service")
    @Expose
    public int shopService;
    @SerializedName("shop_type")
    @Expose
    public String shopType;
    @SerializedName("image")
    @Expose
    public String image;
    @SerializedName("shop_famous_image")
    @Expose
    public String shop_famous_image;

    @SerializedName("account_type")
    @Expose
    public String type;
    @SerializedName("shop_famous_type")
    @Expose
    public String shop_famous_type;
    @SerializedName("rate")
    @Expose
    public float rate;
    @SerializedName("comments")
    @Expose
    public int comments;
    @SerializedName("lat")
    @Expose
    public String lat;
    @SerializedName("lng")
    @Expose
    public String lng;
    @SerializedName("address")
    @Expose
    public String address;
    @SerializedName("jwt_token")
    @Expose
    public String jwtToken;

    @SerializedName("package_id")
    @Expose
    public int packageId;

    @SerializedName("is_shop")
    @Expose
    public boolean isShop = false;

    @SerializedName("kind")
    @Expose
    public String kind = Constants.FAMOUS;

    public String prevType = "";
    public int prevId = -1;
    public String prevImage = "";
    public String prevName = "";

}
