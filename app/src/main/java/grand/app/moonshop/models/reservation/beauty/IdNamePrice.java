package grand.app.moonshop.models.reservation.beauty;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class IdNamePrice {
    @SerializedName("id")
    @Expose
    public Integer id;
    @SerializedName("price")
    @Expose
    public Integer price;
    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("image")
    @Expose
    public String image;
}
