package grand.app.moonshop.models.user.log;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import grand.app.moonshop.models.app.AppMoon;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.utils.storage.user.UserHelper;

public class LogcatCrashRequest {
    @SerializedName("date")
    @Expose
    public String date;
    @SerializedName("message")
    @Expose
    public String message;

    @SerializedName("type")
    @Expose
    public int type = 1;

    @SerializedName("app")
    @Expose
    public int app = 1;

    @SerializedName("id")
    @Expose
    public String id = null;

    public LogcatCrashRequest(String date, String message) {
        this.date = date;
        this.message = message;
        if(UserHelper.isLogin())
            id = String.valueOf(UserHelper.getUserId());
    }
}
