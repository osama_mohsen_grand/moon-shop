
package grand.app.moonshop.models.notifications;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import grand.app.moonshop.models.base.StatusMsg;

public class NotificationResponse extends StatusMsg {

    @SerializedName("data")
    @Expose
    private List<Notification> mData;

    public List<Notification> getData() {
        return mData;
    }

    public void setData(List<Notification> data) {
        mData = data;
    }

}
