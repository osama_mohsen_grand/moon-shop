package grand.app.moonshop.models.triphistory;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import grand.app.moonshop.models.base.StatusMsg;


public class TripHistoryResponse extends StatusMsg {

    @SerializedName("data")
    @Expose
    public List<Datum> data = null;


    public class Datum {

        @SerializedName("id")
        @Expose
        public String id;
        @SerializedName("rate")
        @Expose
        public float rate;
        @SerializedName("total")
        @Expose
        public String total;
        @SerializedName("order_action")
        @Expose
        public String orderAction;
        @SerializedName("created_at")
        @Expose
        public String createdAt;
        @SerializedName("pickup_address")
        @Expose
        public String pickupAddress;
        @SerializedName("endtrip_address")
        @Expose
        public String endtripAddress;

    }
}
