package grand.app.moonshop.models.service;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

import grand.app.moonshop.models.base.StatusMsg;

public class ServiceResponse extends StatusMsg implements Serializable {
    @SerializedName("services")
    @Expose
    public List<Service> services = null;
}
