package grand.app.moonshop.models.famous.advertise;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import grand.app.moonshop.models.base.IdNameDescription;
import grand.app.moonshop.models.base.StatusMsg;

public class FilterShopAdsResponse extends StatusMsg {
    @SerializedName("data")
    @Expose
    public ArrayList<IdNameDescription> data;
}
