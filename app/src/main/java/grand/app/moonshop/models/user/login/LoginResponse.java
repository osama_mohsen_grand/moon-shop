package grand.app.moonshop.models.user.login;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import grand.app.moonshop.models.base.StatusMsg;
import grand.app.moonshop.models.user.profile.User;


public class LoginResponse extends StatusMsg {


    @SerializedName("data")
    @Expose
    public User data;

}
