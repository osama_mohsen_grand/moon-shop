package grand.app.moonshop.models.reservation.doctor;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import androidx.databinding.ObservableField;
import grand.app.moonshop.R;
import grand.app.moonshop.models.user.profile.User;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.utils.Validate;
import grand.app.moonshop.utils.resources.ResourceManager;
import grand.app.moonshop.utils.storage.user.UserHelper;
import grand.app.moonshop.vollyutils.VolleyFileObject;
import timber.log.Timber;

public class AddDoctorRequest {


    public transient VolleyFileObject volleyFileObject = null;

    @SerializedName("type")
    @Expose
    private String type;

    @SerializedName("name")
    @Expose
    private String name = "";


    @SerializedName("years_experience")
    @Expose
    private String years_experience = "";

    @SerializedName("fees")
    @Expose
    private String fees = "";


    @SerializedName("description")
    @Expose
    private String description = "";

    @SerializedName("specialties_id")
    @Expose
    private int specialties_id = -1;


    public transient ObservableField nameError;
    public transient ObservableField yearsExperienceError;
    public transient ObservableField feesError;
    public transient ObservableField specialtiesIdError;
    public transient ObservableField descriptionError;

    public AddDoctorRequest() {
        User user = UserHelper.getUserDetails();
        type = user.type;
        nameError = new ObservableField();
        yearsExperienceError = new ObservableField();
        feesError = new ObservableField();
        descriptionError = new ObservableField();
        specialtiesIdError = new ObservableField();
    }


    public boolean isValid() {
        boolean valid = true;

        if(!Validate.isValid(name)) {
            nameError.set(Validate.error);
            valid = false;
            Timber.e("name:error");
        }
        if(!Validate.isValid(years_experience, Constants.NUMBER)) {
            yearsExperienceError.set(Validate.error);
            valid = false;
            Timber.e("years_experience:error");
        }
        if(!Validate.isValid(fees)) {
            feesError.set(Validate.error);
            valid = false;
            Timber.e("feesError:error");
        }

        if(specialties_id == -1) {
            specialtiesIdError.set(ResourceManager.getString(R.string.this_field_is_requried));
            valid = false;
            Timber.e("specification:error");
        }
        if(!Validate.isValid(description)) {
            descriptionError.set(Validate.error);
            valid = false;
            Timber.e("specification:error");
        }
        Timber.e("valid_input:"+valid);
        return valid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
        nameError.set(null);
    }

    public String getYears_experience() {
        return years_experience;
    }

    public void setYears_experience(String years_experience) {
        this.years_experience = years_experience;
        yearsExperienceError.set(null);
    }

    public String getFees() {
        return fees;
    }

    public void setFees(String fees) {
        this.fees = fees;
        feesError.set(null);
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
        descriptionError.set(null);
    }

    public int getSpecialties_id() {
        return specialties_id;
    }

    public void setSpecialties_id(int specialties_id) {
        this.specialties_id = specialties_id;
        specialtiesIdError.set(null);
    }
}
