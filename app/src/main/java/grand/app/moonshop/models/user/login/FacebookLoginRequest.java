package grand.app.moonshop.models.user.login;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import grand.app.moonshop.utils.Constants;


public class FacebookLoginRequest {
    @SerializedName("facebook_id")
    @Expose
    private String facebook_id = "";
    @SerializedName("type")
    @Expose
    private String type = Constants.DEFAULT_TYPE;

    public FacebookLoginRequest(String facebook_id) {
        this.facebook_id = facebook_id;
    }
}
