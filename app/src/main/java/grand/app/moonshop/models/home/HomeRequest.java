package grand.app.moonshop.models.home;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.utils.storage.SharedPreferenceHelper;
import grand.app.moonshop.utils.storage.user.UserHelper;
import grand.app.moonshop.vollyutils.AppHelper;

public class HomeRequest {
    @SerializedName("country_id")
    @Expose
    public String country_id = "";


    public HomeRequest() {
        country_id = UserHelper.retrieveKey(Constants.COUNTRY_ID);
    }
}
