package grand.app.moonshop.models.credit;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Credit {
    @SerializedName("credit")
    @Expose
    public String credit = "";

    @SerializedName("balance")
    @Expose
    public String balance;
}
