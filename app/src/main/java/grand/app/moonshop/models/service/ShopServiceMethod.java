package grand.app.moonshop.models.service;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import grand.app.moonshop.utils.storage.user.UserHelper;

public class ShopServiceMethod {
    @SerializedName("account_type")
    @Expose
    public int account_type;

    @SerializedName("type")
    @Expose
    public String type = "";

    public ShopServiceMethod(int account_type) {
        this.account_type = account_type;
        type = UserHelper.getUserDetails().type;
    }
}
