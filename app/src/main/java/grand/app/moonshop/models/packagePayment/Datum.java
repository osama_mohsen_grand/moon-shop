
package grand.app.moonshop.models.packagePayment;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Datum {

    @SerializedName("id")
    @Expose
    public int mId;
    @SerializedName("name")
    @Expose
    public String mName;
    @SerializedName("package")
    @Expose
    public String mPackage;
    @SerializedName("price")
    @Expose
    public String mPrice;
    @SerializedName("price_discount")
    @Expose
    public String priceDiscount;
    @SerializedName("discount")
    @Expose
    public String discount;

    @SerializedName("description")
    @Expose
    public String description;

}
