package grand.app.moonshop.models.offer;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import grand.app.moonshop.models.app.AppMoon;
import grand.app.moonshop.vollyutils.VolleyFileObject;

public class AddOfferRequest {
    @SerializedName("type")
    @Expose
    public int type = -1;

    @SerializedName("account_type")
    @Expose
    public String accountType = AppMoon.getUserType();

    public transient VolleyFileObject volleyFileObject = null;


}
