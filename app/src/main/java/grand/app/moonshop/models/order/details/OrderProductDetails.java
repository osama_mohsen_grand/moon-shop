package grand.app.moonshop.models.order.details;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OrderProductDetails {
    @SerializedName("id")
    @Expose
    public Integer id;
    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("description")
    @Expose
    public String description;
    @SerializedName("qty")
    @Expose
    public Integer qty;
    @SerializedName("additions")
    @Expose
    public String additions;
    @SerializedName("special_request")
    @Expose
    public String specialRequest;
    @SerializedName("size")
    @Expose
    public String size;
    @SerializedName("color")
    @Expose
    public String color;

    @SerializedName("price")
    @Expose
    public String price;
    @SerializedName("price_addition")
    @Expose
    public String price_addition;

}
