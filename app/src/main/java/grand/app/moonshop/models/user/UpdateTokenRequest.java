package grand.app.moonshop.models.user;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import grand.app.moonshop.models.app.AppMoon;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.utils.storage.user.UserHelper;

public class UpdateTokenRequest {
    @SerializedName("type")
    @Expose
    public String type;
    @SerializedName("firebase_token")
    @Expose
    public String token;
    @SerializedName("lat")
    @Expose
    public String lat;
    @SerializedName("lng")
    @Expose
    public String lng;
    @SerializedName("address")
    @Expose
    public String address;
    public UpdateTokenRequest(String token) {
        this.type = AppMoon.getUserType();
        this.token = token;
        this.lat = UserHelper.retrieveKey(Constants.LAT);
        this.lng = UserHelper.retrieveKey(Constants.LNG);
        this.address = UserHelper.retrieveKey(Constants.USER_ADDRESS);
    }
}
