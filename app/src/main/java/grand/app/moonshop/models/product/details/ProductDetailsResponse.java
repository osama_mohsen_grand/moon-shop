
package grand.app.moonshop.models.product.details;

import com.google.gson.annotations.SerializedName;

import grand.app.moonshop.models.base.StatusMsg;

public class ProductDetailsResponse extends StatusMsg {

    @SerializedName("data")
    public ProductDetails mData;

}
