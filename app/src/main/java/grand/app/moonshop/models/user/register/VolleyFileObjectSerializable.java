package grand.app.moonshop.models.user.register;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import grand.app.moonshop.vollyutils.VolleyFileObject;

public class VolleyFileObjectSerializable implements Serializable {
    public Map<String, VolleyFileObject> volleyFileObjectMap = new HashMap<>();

}
