package grand.app.moonshop.models.reservation.doctor.order;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import grand.app.moonshop.utils.storage.user.UserHelper;

public class ReservationOrderActionRequest {
    @SerializedName("order_id")
    @Expose
    public int order_id;

    @SerializedName("status")
    @Expose
    public int status;

    @SerializedName("type")
    @Expose
    public String type = UserHelper.getUserDetails().type;
}
