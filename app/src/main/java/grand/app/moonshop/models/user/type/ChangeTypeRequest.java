package grand.app.moonshop.models.user.type;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ChangeTypeRequest {
    @SerializedName("flag")
    @Expose
    public int flag;

    public ChangeTypeRequest(int flag) {
        this.flag = flag;
    }
}
