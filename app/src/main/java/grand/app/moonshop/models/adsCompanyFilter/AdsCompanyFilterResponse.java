package grand.app.moonshop.models.adsCompanyFilter;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

import grand.app.moonshop.models.base.StatusMsg;


public class AdsCompanyFilterResponse extends StatusMsg implements Serializable {
    @SerializedName("data")
    @Expose
    public List<AdsCompanyFilter> data = null;

}
