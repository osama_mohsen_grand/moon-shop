package grand.app.moonshop.models.institution;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import androidx.databinding.ObservableField;
import grand.app.moonshop.utils.Validate;
import grand.app.moonshop.utils.storage.user.UserHelper;
import grand.app.moonshop.vollyutils.VolleyFileObject;
import timber.log.Timber;

public class UpdateServiceRequest extends AddServiceRequest{
    @SerializedName("id")
    @Expose
    public int id;


    public UpdateServiceRequest(int id , AddServiceRequest addServiceRequest) {
        this.id = id;
        this.setName(addServiceRequest.getName());
        this.volleyFileObject = addServiceRequest.volleyFileObject;
    }
}
