package grand.app.moonshop.models.reservation.doctor.schedule;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Day {
    @SerializedName("id")
    @Expose
    public Integer id;
    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("schedules")
    @Expose
    public List<Schedule> schedules = null;
    @SerializedName("switch")
    @Expose
    public Boolean _switch;
}
