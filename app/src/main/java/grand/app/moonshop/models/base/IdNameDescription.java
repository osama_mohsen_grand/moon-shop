package grand.app.moonshop.models.base;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class IdNameDescription {
    @SerializedName("id")
    @Expose
    public int id;

    @SerializedName("name")
    @Expose
    public String name;

    @SerializedName("image")
    @Expose
    public String image = "";

    @SerializedName("capture")
    @Expose
    public String capture = "";
}
