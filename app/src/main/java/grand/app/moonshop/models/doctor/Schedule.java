package grand.app.moonshop.models.doctor;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Schedule {
    @SerializedName("from")
    @Expose
    public String from;
    @SerializedName("to")
    @Expose
    public String to;
    @SerializedName("period")
    @Expose
    public int period;
    @SerializedName("day")
    @Expose
    public int day;
}
