package grand.app.moonshop.models.reservation.beauty;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import grand.app.moonshop.models.base.StatusMsg;
import grand.app.moonshop.models.service.Service;

public class BeautyOrderDetailsModel extends StatusMsg {
    @SerializedName("order_id")
    @Expose
    public Integer orderId;
    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("image")
    @Expose
    public String image;
    @SerializedName("phone")
    @Expose
    public String phone;
    @SerializedName("day")
    @Expose
    public String day;
    @SerializedName("order_date")
    @Expose
    public String orderDate;
    @SerializedName("fees")
    @Expose
    public Integer fees;
    @SerializedName("time")
    @Expose
    public String time;
    @SerializedName("services")
    @Expose
    public List<Service> services;
}
