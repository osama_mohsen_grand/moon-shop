package grand.app.moonshop.models.institution;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import androidx.databinding.ObservableField;
import grand.app.moonshop.models.app.AppMoon;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.utils.Validate;
import grand.app.moonshop.utils.storage.user.UserHelper;
import grand.app.moonshop.vollyutils.VolleyFileObject;
import timber.log.Timber;

public class AddBranchRequest {

    public transient VolleyFileObject volleyFileObject = null;

    @SerializedName("lat")
    @Expose
    private double lat = 0;

    @SerializedName("lng")
    @Expose
    private double lng = 0;

    @SerializedName("address")
    @Expose
    private String address = "";

    @SerializedName("type")
    @Expose
    public String type = UserHelper.getUserDetails().type;

    @SerializedName("account_type")
    @Expose
    public String accountType = AppMoon.getUserType();

    public transient ObservableField addressError;

    public AddBranchRequest() {
        addressError = new ObservableField();
    }


    public boolean isValid() {
        boolean valid = true;

        if(!Validate.isValid(address)) {
            addressError.set(Validate.error);
            valid = false;
            Timber.e("address:error");
        }
        return valid;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
        addressError.set(null);
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }
}
