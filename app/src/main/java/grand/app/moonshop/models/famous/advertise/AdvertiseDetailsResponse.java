package grand.app.moonshop.models.famous.advertise;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import grand.app.moonshop.models.base.StatusMsg;

public class AdvertiseDetailsResponse extends StatusMsg {

    @SerializedName("data")
    @Expose
    public Data data;
}
