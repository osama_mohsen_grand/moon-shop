package grand.app.moonshop.models.ads;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import grand.app.moonshop.models.base.StatusMsg;

public class AdsDetailsResponse extends StatusMsg implements Serializable {
    @SerializedName("data")
    @Expose
    public AdsDetailsModel data;
}
