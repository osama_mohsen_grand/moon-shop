package grand.app.moonshop.models.reservation.beauty;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import grand.app.moonshop.models.base.StatusMsg;

public class BeautyServiceResponse extends StatusMsg {
    @SerializedName("data")
    @Expose
    public List<IdNamePrice> services;
}
