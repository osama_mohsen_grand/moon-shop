package grand.app.moonshop.models.reservation.doctor.schedule;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import grand.app.moonshop.models.base.StatusMsg;

public class DoctorScheduleResponse extends StatusMsg {
    @SerializedName("data")
    @Expose
    public List<Day> days = null;
}
