package grand.app.moonshop.models.product;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import androidx.databinding.ObservableField;
import grand.app.moonshop.models.app.AppMoon;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.utils.Validate;
import grand.app.moonshop.utils.storage.user.UserHelper;

public class AddProductSectoralRequest {


    @SerializedName("id")
    @Expose
    private int id = -1;

    @SerializedName("category_id")
    @Expose
    private int categoryId;

    @SerializedName("type")
    @Expose
    private String type;

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("price")
    @Expose
    private String price;

    @SerializedName("price_after")
    @Expose
    public String priceAfter;

    @SerializedName("stock")
    @Expose
    private String stock;

    @SerializedName("description")
    @Expose
    private String description;

    @SerializedName("color_id")
    @Expose
    public int colorId;

    @SerializedName("size_id")
    @Expose
    public int sizeId;


    @SerializedName("size")
    @Expose
    private String size;

    @SerializedName("color")
    @Expose
    private String color;


    public ObservableField nameError, priceError, stockError, descriptionError,sizeError,colorError;



    public AddProductSectoralRequest(int categoryId, String name, String price, String stock, String description) {
        this.categoryId = categoryId;
        this.type = UserHelper.getUserDetails().shopType;
        this.name = name;
        this.price = price;
        this.stock = stock;
        this.description = description;

        sizeError = new ObservableField();
        colorError = new ObservableField();
        nameError = new ObservableField();
        priceError = new ObservableField();
        stockError = new ObservableField();
        descriptionError = new ObservableField();
    }

    public boolean isValid() {
        boolean valid = true;
        if (!Validate.isValid(name)) {
            nameError.set(Validate.error);
            valid = false;
        } else
            nameError.set(null);

        if (!Validate.isValid(price, Constants.NUMBER)) {
            priceError.set(Validate.error);
            valid = false;
        } else
            priceError.set(null);

        if (!Validate.isValid(stock, Constants.NUMBER)) {
            stockError.set(Validate.error);
            valid = false;
        } else
            stockError.set(null);

        if (!Validate.isValid(description)) {
            descriptionError.set(Validate.error);
            valid = false;
        } else
            descriptionError.set(null);

        if (!Validate.isValid(size) && AppMoon.allowSize()) {
            sizeError.set(Validate.error);
            valid = false;
        } else
            sizeError.set(null);


        if (!Validate.isValid(color)&& AppMoon.allowColor()) {
            colorError.set(Validate.error);
            valid = false;
        } else
            colorError.set(null);
        return valid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
        nameError.set(null);
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
        priceError.set(null);
    }

    public String getStock() {
        return stock;
    }

    public void setStock(String stock) {
        this.stock = stock;
        stockError.set(null);
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
        descriptionError.set(null);
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
        sizeError.set(null);
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
        colorError.set(null);
    }

    public void setId(int id) {
        this.id = id;
    }
}
