package grand.app.moonshop.models.user.forgetpassword;

import androidx.databinding.ObservableField;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.utils.Validate;

public class ForgetPasswordRequest {

    @SerializedName("type")
    @Expose
    public String type;

    @SerializedName("email")
    @Expose
    private String email = "";

    @SerializedName("phone")
    @Expose
    private String phone = "";

    @SerializedName("check_phone")
    @Expose
    private String check_phone = "";



    public transient ObservableField emailError;
    public ForgetPasswordRequest(boolean isForgetPassword,String input) {
        this.type = Constants.DEFAULT_TYPE;
        if(isForgetPassword) {
            check_phone = "1";//forget password
            email = input;
            phone = input;
        }else{
            check_phone = "2";//register and check phone
            phone = input;
            email = input;
        }
        emailError = new ObservableField();

    }

    public boolean validateInput() {
        boolean valid = true;
        if (!Validate.isValid(email)) {
            emailError.set(Validate.error);
            valid = false;
        }else
            emailError.set(null);

        return valid;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
        emailError.set(null);
    }
}
