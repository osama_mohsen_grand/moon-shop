package grand.app.moonshop.models.rate;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RateRequest {
    @SerializedName("trip_id")
    @Expose
    public String trip_id;

    @SerializedName("type")
    @Expose
    public String type = "driver";

    @SerializedName("rate")
    @Expose
    public int rate;

    @SerializedName("feedback")
    @Expose
    public String feedback;

    public RateRequest(String trip_id, int rate,String feedback) {
        this.trip_id = trip_id;
        this.rate = rate;
        this.feedback = feedback;
    }
}
