
package grand.app.moonshop.models.notifications;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Notification implements Serializable {



    @SerializedName("status")
    @Expose
    public int status;
    @SerializedName("ads_id")
    @Expose
    public int adsId;

    @SerializedName("days_count")
    @Expose
    public int daysCount;


    @SerializedName("created_at")
    @Expose
    public String mCreatedAt;
    @SerializedName("famous_name")
    @Expose
    public String famousName;

    @SerializedName("famous_image")
    @Expose
    public String famousImage;

    @SerializedName("image")
    @Expose
    public String image;



    @SerializedName("message")
    @Expose
    private String mMessage;
    @SerializedName("title")
    @Expose
    private String mTitle;

    public String getCreatedAt() {
        return mCreatedAt;
    }

    public void setCreatedAt(String createdAt) {
        mCreatedAt = createdAt;
    }

    public String getMessage() {
        return mMessage;
    }

    public void setMessage(String message) {
        mMessage = message;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

}
