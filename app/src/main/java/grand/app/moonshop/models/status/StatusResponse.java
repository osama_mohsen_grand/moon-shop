
package grand.app.moonshop.models.status;

import java.util.List;
import com.google.gson.annotations.SerializedName;

import grand.app.moonshop.models.base.StatusMsg;

public class StatusResponse extends StatusMsg {

    @SerializedName("data")
    private List<Status> mData;


    public List<Status> getData() {
        return mData;
    }

    public void setData(List<Status> data) {
        mData = data;
    }

}
