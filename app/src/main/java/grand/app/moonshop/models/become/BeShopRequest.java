package grand.app.moonshop.models.become;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import androidx.databinding.ObservableField;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.utils.Validate;
import grand.app.moonshop.utils.storage.user.UserHelper;
import timber.log.Timber;

public class BeShopRequest {


    @SerializedName("name")
    @Expose
    private String name = "";

    private String serviceText = "";

    @SerializedName("shop_service")
    @Expose
    private String shopService = "";

    @SerializedName("description")
    @Expose
    private String description = "";


    public ObservableField serviceTextError;
    public ObservableField nameError;
    public ObservableField descriptionError;

    public BeShopRequest() {
        serviceTextError = new ObservableField();
        nameError = new ObservableField();
        descriptionError = new ObservableField();
    }

    public boolean isValid() {
        boolean valid = true;
        if (!Validate.isValid(name)) {
            nameError.set(Validate.error);
            valid = false;
            Timber.e("type:error");
        }
        if (!Validate.isValid(shopService)) {
            serviceTextError.set(Validate.error);
            valid = false;
            Timber.e("type:error");
        }
        if (!Validate.isValid(description)) {
            descriptionError.set(Validate.error);
            valid = false;
            Timber.e("type:error");
        }
        return valid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
        nameError.set(null);
    }

    public String getServiceText() {
        return serviceText;
    }

    public void setServiceText(String serviceText) {
        this.serviceText = serviceText;
        serviceTextError.set(null);
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
        descriptionError.set(null);
    }

    public void setShopService(String shopService) {
        this.shopService = shopService;
    }

    public void clear() {
        name = "";
        description = "";
        shopService = "";
    }
}
