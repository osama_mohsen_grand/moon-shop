
package grand.app.moonshop.models.packagePayment;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import grand.app.moonshop.models.base.StatusMsg;


public class PackageResponse extends StatusMsg {

    @SerializedName("data")
    @Expose
    public List<Datum> mData;


}
