package grand.app.moonshop.models.service;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import grand.app.moonshop.models.base.IdName;

public class Datum {
    @SerializedName("id")
    @Expose
    public Integer id;
    @SerializedName("type")
    @Expose
    public Integer type;
    @SerializedName("flag")
    @Expose
    public Integer flag;
    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("categories")
    @Expose
    public List<IdName> categories = null;
}
