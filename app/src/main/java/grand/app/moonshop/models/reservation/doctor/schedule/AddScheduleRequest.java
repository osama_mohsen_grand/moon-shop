package grand.app.moonshop.models.reservation.doctor.schedule;

import android.widget.Toast;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import androidx.databinding.ObservableField;
import grand.app.moonshop.R;
import grand.app.moonshop.models.app.AppMoon;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.utils.Validate;
import grand.app.moonshop.utils.resources.ResourceManager;
import grand.app.moonshop.utils.storage.user.UserHelper;
import timber.log.Timber;

public class AddScheduleRequest {
    @SerializedName("doctor_id")
    @Expose
    public int doctor_id;

    @SerializedName("day")
    @Expose
    public int day;

    @SerializedName("from")
    @Expose
    public ArrayList<String> from = new ArrayList<>();

    @SerializedName("to")
    @Expose
    public ArrayList<String> to = new ArrayList<>();

    @SerializedName("period")
    @Expose
    public ArrayList<String> period = new ArrayList<>();

    @SerializedName("type")
    @Expose
    public String type;

    @SerializedName("status")
    @Expose
    public int status;

    @SerializedName("switch")
    @Expose
    public int _switch;


    private transient String fromText = "";
    public transient ObservableField fromTextError;
    public transient ObservableField fromText2Error;

    private transient String toText = "";
    public transient ObservableField toTextError;
    public transient ObservableField toText2Error;

    private transient String periodText = "";
    public transient ObservableField periodTextError;


    private transient String fromText2 = "";
    private transient String toText2 = "";
    public transient String periodText2 = "";

    public AddScheduleRequest(int doctor_id, int day, int status, int _switch) {
        this.doctor_id = doctor_id;
        this.day = day;
        type = UserHelper.getUserDetails().type;
        this.status = status;
        this._switch = _switch;

        fromTextError = new ObservableField();
        fromText2Error = new ObservableField();
        toTextError = new ObservableField();
        toText2Error = new ObservableField();
        periodTextError = new ObservableField();
    }

    public boolean isValid() {
        boolean valid = true;
        if (fromText.equals("")) {
            fromTextError.set(ResourceManager.getString(R.string.this_field_is_requried));
            valid = false;
            Timber.e("from:error");
        }
        if (toText.equals("")) {
            toTextError.set(ResourceManager.getString(R.string.this_field_is_requried));
            valid = false;
            Timber.e("to:error");
        }
        if (periodText.equals("")) {
            periodTextError.set(ResourceManager.getString(R.string.this_field_is_requried));
            valid = false;
            Timber.e("period:error");
        }
        if(valid){
            if(!fromText.equals("") && !toText.equals("") && !checkTimeValid(getFromText(),getToText())){
                fromTextError.set(ResourceManager.getString(R.string.time_not_valid));
                toTextError.set(ResourceManager.getString(R.string.time_not_valid));
                valid  =false;
            }
            if(!fromText2.equals("") && !toText2.equals("") && !checkTimeValid(fromText2,toText2)){
                fromText2Error.set(ResourceManager.getString(R.string.time_not_valid));
                toText2Error.set(ResourceManager.getString(R.string.time_not_valid));
                valid = false;
            }
        }

        return valid;
    }


    public boolean checkTimeValid(String fromTime ,String toTime){
        Date time1 = null;
        try {
            time1 = new SimpleDateFormat("HH:mm").parse(fromTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Calendar calendar1 = Calendar.getInstance();
        calendar1.setTime(time1);
        calendar1.add(Calendar.DATE, 1);

        Date time2 = null;
        try {
            time2 = new SimpleDateFormat("HH:mm").parse(toTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Calendar calendar2 = Calendar.getInstance();
        calendar2.setTime(time2);
        calendar2.add(Calendar.DATE, 1);
        if(time2.after(time1))
            return true;
        return false;
    }

    public String getFromText() {
        return fromText;
    }

    public void setFromText(String fromText) {
        this.fromText = fromText;
        fromTextError.set(null);
    }

    public String getToText() {
        return toText;
    }

    public void setToText(String toText) {
        this.toText = toText;
        toTextError.set(null);
    }

    public String getPeriodText() {
        return periodText;
    }

    public void setPeriodText(String periodText) {
        this.periodText = periodText;
        periodTextError.set(null);
    }

    public String getFromText2() {
        return fromText2;
    }

    public void setFromText2(String fromText2) {
        this.fromText2 = fromText2;
        fromText2Error.set(null);
    }

    public String getToText2() {
        return toText2;
    }

    public void setToText2(String toText2) {
        this.toText2 = toText2;
        toText2Error.set(null);
    }
}
