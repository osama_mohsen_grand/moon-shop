
package grand.app.moonshop.models.famous.list;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FamousData {

    @SerializedName("all_famous")
    @Expose
    public List<Famous> allFamous = null;
    @SerializedName("sliders")
    @Expose
    public List<Slider> sliders = null;


}
