
package grand.app.moonshop.models.product.details;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

import grand.app.moonshop.models.base.IdNameImage;

public class ProductDetails implements Serializable {

    public ProductDetails() {
    }
    @SerializedName("category_id")
    @Expose
    public int mCategoryId;
    @SerializedName("description")
    @Expose
    public String mDescription;
    @SerializedName("id")
    @Expose
    public int mId;
    @SerializedName("name")
    @Expose
    public String mName;
    @SerializedName("price")
    @Expose
    public String mPrice;
    @SerializedName("image")
    @Expose
    public String image;

    @SerializedName("images")
    @Expose
    public ArrayList<IdNameImage> images = new ArrayList<>();
    @SerializedName("stock")
    @Expose
    public int mStock;
    @SerializedName("wholesale_price")
    @Expose
    public String wholeSalePrice;
    @SerializedName("min_qty")
    @Expose
    public String minQty;
}
