package grand.app.moonshop.models.user.activation;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import grand.app.moonshop.models.base.StatusMsg;
import grand.app.moonshop.models.user.profile.User;


public class VerificationResponse extends StatusMsg {

    @SerializedName("data")
    @Expose
    public User data;


//    public class Result{
//        @SerializedName("driver")
//        @Expose
//        public Driver driver;
//    }

}
