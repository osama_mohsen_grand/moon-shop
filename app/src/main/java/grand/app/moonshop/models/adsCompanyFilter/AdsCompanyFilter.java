package grand.app.moonshop.models.adsCompanyFilter;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class AdsCompanyFilter  implements Serializable {
    @SerializedName("id")
    @Expose
    public Integer id;
    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("image")
    @Expose
    public String image;
    @SerializedName("sub_category")
    @Expose
    public List<AdsCompanyFilter> subCategory = null;
}
