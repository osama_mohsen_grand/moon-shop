package grand.app.moonshop.models.app;

public class AccountTypeModel {
    public String id = "-1";
    public String type = "";
    public String flag = "";

    public AccountTypeModel(String id, String type,String flag) {
        this.id = id;
        this.type = type;
        this.flag = flag;
    }
}
