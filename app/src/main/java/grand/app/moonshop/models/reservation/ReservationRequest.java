package grand.app.moonshop.models.reservation;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ReservationRequest {
    @SerializedName("type")
    @Expose
    public String type;


    @SerializedName("date")
    @Expose
    public String date;

    @SerializedName("service")
    @Expose
    public String service;

    @SerializedName("day_id")
    @Expose
    public int day_id;

    @SerializedName("period")
    @Expose
    public int period;

    @SerializedName("id")
    @Expose
    public int orderId;



    @SerializedName("payment_method")
    @Expose
    public int payment_method = 1;

    public ReservationRequest(String type, int orderId) {
        this.type = type;
        this.orderId = orderId;
    }
}
