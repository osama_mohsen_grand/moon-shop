package grand.app.moonshop.models.shop;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import androidx.databinding.ObservableField;
import grand.app.moonshop.R;
import grand.app.moonshop.utils.resources.ResourceManager;

public class ShopDetails implements Serializable {

    @SerializedName("id")
    @Expose
    public Integer id = -1;
    @SerializedName("name")
    @Expose
    public String name = "";
    @SerializedName("image")
    @Expose
    public String image = "";
    @SerializedName("description")
    @Expose
    public String description = "";
    @SerializedName("avg")
    @Expose
    public String avg;
    @SerializedName("mini_charge")
    @Expose
    public String miniCharge;
    @SerializedName("is_famous")
    @Expose
    public Boolean isFamous;
    @SerializedName("delivery")
    @Expose
    public String delivery;
    @SerializedName("followers_count")
    @Expose
    public String followersCount = "";
    @SerializedName("comment_count")
    @Expose
    public Integer commentCount = 0;
    @SerializedName("rate")
    @Expose
    public float rate = 0;
    @SerializedName("tags")
    @Expose
    public String tags;

    @SerializedName("type")
    @Expose
    public int type;

    @SerializedName("isFollow")
    @Expose
    public boolean isFollow;

    @SerializedName("lat")
    @Expose
    public Double lat;

    @SerializedName("lng")
    @Expose
    public Double lng;


}