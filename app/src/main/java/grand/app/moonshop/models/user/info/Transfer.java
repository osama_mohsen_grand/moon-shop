package grand.app.moonshop.models.user.info;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Transfer {
    @SerializedName("id")
    @Expose
    public String id;
    @SerializedName("bank_name")
    @Expose
    public String bankName = "";
    @SerializedName("account_number")
    @Expose
    public String accountNumber = "";
    @SerializedName("country")
    @Expose
    public String country = "";
    @SerializedName("city")
    @Expose
    public String city = "";
    @SerializedName("region")
    @Expose
    public String region = "";
    @SerializedName("notes")
    @Expose
    public String notes = "";
    @SerializedName("country_id")
    @Expose
    public String countryId;

    @SerializedName("city_id")
    @Expose
    public String cityId;

    @SerializedName("region_id")
    @Expose
    public String regionId;
}
