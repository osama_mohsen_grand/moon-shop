package grand.app.moonshop.models.chat.list;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import grand.app.moonshop.models.base.StatusMsg;


public class ChatListResponse extends StatusMsg {

    @SerializedName("data")
    @Expose
    public List<ChatList> data;
}
