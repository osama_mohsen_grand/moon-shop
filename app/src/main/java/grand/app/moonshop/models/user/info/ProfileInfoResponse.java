package grand.app.moonshop.models.user.info;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import grand.app.moonshop.models.base.StatusMsg;

public class ProfileInfoResponse extends StatusMsg {

    @SerializedName("email")
    @Expose
    public String email = "";
    @SerializedName("number")
    @Expose
    public String number = "";
    @SerializedName("website")
    @Expose
    public String website = "";
    @SerializedName("description")
    @Expose
    public String description;
    @SerializedName("profile")
    @Expose
    public List<Profile> profile;
    @SerializedName("transfer")
    @Expose
    public Transfer transfer = null;
}
