
package grand.app.moonshop.models.famous.home;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ImageVideo implements Serializable {

    @SerializedName("id")
    @Expose
    public Integer id;
    @SerializedName("capture")
    @Expose
    public String capture = "";
    @SerializedName("image")
    @Expose
    public String image;
    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("type")
    @Expose
    public Integer type;
    @SerializedName("count")
    @Expose
    public Integer count;


    @SerializedName("famous_id")
    @Expose
    public int famous_id;

    @SerializedName("video_count")
    @Expose
    public Integer videoCount;

    @SerializedName("account_type")
    @Expose
    public String accountType;

    //in ads
    @SerializedName("shop_id")
    @Expose
    public Integer shopId;

    @SerializedName("ads_expired")
    @Expose
    public Integer adsExpired;
}
