package grand.app.moonshop.vollyutils;

/**
 * Created by mohamedatef on 12/29/18.
 */

public class URLS {

    public static final String ADD_TRANSFER_GUIDE = "add-transfer-guide";
    public static final String ADS_CONFIRM = "ads-confirm";
    public static String BASE_URL = "https://www.moon4online.com/api/";

    public static String FIREBASE = "firebase";
    public static String LOGCRASH = "logcrash";

    public static String FACEBOOK_LOGIN = "facebook_login";

    public static String CHECK_NICK_NAME="shops/check-nick-name";
    public static String ACCOUNT_TYPES ="account-type?country_id=";

    public static String CHANGE_TYPE = "facebook_login";
    public static String CONTACT_US = "contact-us";
    public static String PROFILE_INFO = "account-info";
    public static String ADD_SOCIAL_MEDIA = "add-social-media";

    public static String ACCOUNT_INFO = "account-info?";
    public static String CLINIC_RESERVATION_ORDER = "clinics/reservation_order";

    public static String CLINIC_DOCTOR_DETAILS = "clinics/doctorSchedules";

    public static String LOGIN = "login";

    public static String REGISTER = "register";

    public static String REGISTER_SHOP = "shops/addShop";

    public static String GET_ALL_COUNTRIES = "getCountries";

    public static String CODE_SEND = "driver/code_send";

    public static String FORGET_PASSWORD = "changeForgetPassword";

    public static String CHANGE_PASSWORD = "changeForgetPassword";


    public static String HOME = "shops/Categories";

    public static String ADD_CATEGORY = "shops/addCategory";

    public static String ADD_BRANCH = "Institution/add_branch";


    public static String DELETE_CATEGORY = "shops/deleteCategory";
    public static String ADD_SERVICES_BEAUTY = "beauty/add_services";

    public static String SHOP_SIZE_COLOR = "shops/SizesColors";

    public static String CHECK_PHONE = "checkPhone";
    public static String ADS_FILTER = "companies/services_filter";
    public static final String ADS_DETAILS = "company/productDetails";

    public static final String ADD_ADV = "company/addProduct";

    public static final String UPDATE_ADV = "company/editProduct";

    public static String FAMOUS_COMPANY_ADS_CATEGORIES = "famous/famous-ads-filter";

    public static String DOCTOR_LIST = "clinics/index";
    public static String DELETE_DOCTOR = "clinics/deleteDoctor";
    public static String ADD_DOCTOR = "clinics/add_doctors";
    public static String DOCTOR_SPECIFICATION = "clinics/specialist";
    public static String DOCTOR_SCHEDULE = "clinics/doctor_schedule";
    public static String DOCTOR_ADD_SCHEDULE = "clinics/add_schedule";
    public static String DOCTOR_ORDERS = "clinics/orders/list";
    public static String DOCTOR_ORDER_ACTION = "clinics/confirmOrder";
    public static String BEAUTY_ORDER_DETAILS = "clinics/orderDetails";
    public static String CLINIC_RESERVATION_DETAILS = "clinics/orderDetails";


    public static String NOTIFICATION = "notification";

    public static String FAMOUS = "famous";

    public static String FAMOUS_ADD_ALBUM = "famous/addAlbum";

    public static String FAMOUS_ALBUMS = "famous/albums";

    public static String FAMOUS_FILTER = "famous/filter";

    public static String FAMOUS_FILTER_SHOP_ADS = "famous/filterShopAdds";

    public static String FILTER_WITH_FAMOUS ="shops/filterWithFamous";

    public static String INSTITUTION_DELETE ="Institution/deleteBranch";

    public static String INSTITUTION_EDIT ="Institution/editBranch";

    public static String INSTITUTION_ADD_SERVICE ="Institution/add_service";

    public static String ALBUM_IMAGES = "famous/albumsImages";

    public static String FAMOUS_SEARCH_SHOP = "famous/searchShop";

    public static String FAMOUS_DETAILS = "famous/details";

    public static String FAMOUS_FOLLOW = "famous/create_follow";

    public static String FAMOUS_DELETE_IMAGE = "famous/deleteImage";

    public static String FAMOUS_EDIT_ALBUM = "famous/edit_album";

    public static String FAMOUS_DISCOVER = "famous/discover";

    public static String FAMOUS_ALBUM_SEARCH = "famous/searchAlbum";

    public static String FAMOUS_SEARCH = "famous/searchFamous";

    public static String SHOPS_CREDIT_CARD = "shops/creditCard";

    public static final String BE_SHOP = "famous/addShop";

    public static String ADVERTISE_DETAILS = "famous/advertisement_details";


    public static String UPDATE_PROFILE = "profile/edit";

    public static String PACKAGES = "packages";

    public static String ADD_PACKAGE = "addPackage";

    public static String PRODUCTS = "shops/products";

    public static String PRODUCT_DETAILS = "shops/productDetails";

    public static String ADD_PRODUCT = "shops/addProduct";

    public static String EDIT_PRODUCT = "shops/editProduct";

    public static String DELETE_PRODUCT = "shops/deleteProduct";

    public static String STATUS_LIST = "shops/Status";

    public static String ADD_STATUS = "shops/addStatus";

    public static String DELETE_STATUS = "shops/deleteStatus";

    public static String SETTINGS = "settings";

    public static String TRIP_HISTORY = "driver/trip_history";


    public static String OFFERS = "shops/offers";

    public static String DELETE_OFFER = "shops/deleteOffer";

    public static String ADD_OFFER = "shops/addOffer";

    public static String SHOP_REVIEW = "shops/reviews";

    public static String TRIP_ARRIVE = "driver/arrive";

    public static String TRIP_CANCEL_AFTER_ACCEPT = "driver/cancel_trip_after_accept";

    public static String TRIP_DETAILS = "app/get_detail_trip";

    public static String CODE_CHECK = "driver/code_check";

    public static String RATE = "app/rate";

    public static String INSTITUTION_BRANCHES = "Institution/branches";

    public static String ADS = "shops/advertisements";


    public static String SERVICES = "shops_services";


    public static String CREDIT = "driver/credit";

    public static String UPDATE_PAYMENT = "driver/update_payment";

    public static String CHAT_GET = "chat/all_chats";

    public static String CHAT = "chat";
    public static String CHAT_DETAILS = "order/user/chat";

    public static String CHAT_SEND = "chat/create_chat";
    public static String ORDER_SHIPPING = "user/getShippingCompany";

    public static String ORDER_DETAILS = "order/user/orderDetails";
    public static String ORDER_LIST = "order/user/list";
    public static String ORDER_CONFIRM = "order/user/confirm";
    public static String DELETE_ADS = "famous/deleteImage";
    public static String MAIN_SERVICE = "famous/main-service-categories";



}
