package grand.app.moonshop.vollyutils;


import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.util.Objects;

import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.utils.upload.CompressObject;

public class VolleyFileObject implements Serializable {
    private String filePath = "", paramName;
    private int fileType;
    private Uri uri;
    private CompressObject compressObject;
    private File file;

    private static final String TAG = "VolleyFileObject";
    private String fileName = "";

    public VolleyFileObject(String paramName, String filePath, int fileType) {
        this.paramName = paramName;
        this.filePath = filePath;
        this.fileType = fileType;
        if (fileType == Constants.FILE_TYPE_IMAGE) {
            compressImage();
        }
        ///storage/emulated/0/DCIM/Camera/VID_٢٠٢١٠١١٣_١٥٣١٤٠.mp4
//        this.file = new File(filePath);
//
//        String[] parts = file.getAbsolutePath().split("/");
//        Log.d(TAG,""+parts.length);
//        if (parts.length > 0) {
//            String paths = parts[0];
//            Log.d(TAG,"paths:"+paths);
//
//            for (int i = 1; i < parts.length - 1; i++) {
//                paths += "/" + parts[i];
//                Log.d(TAG,"paths:"+paths);
//            }
//            Log.d(TAG,"-----------------");
//            Log.d(TAG,"paths:"+paths);
//            int x = parts[0].indexOf(".");
//            String value = parts[0].substring(x + 1);
//            Log.d(TAG, value);
//            File to = new File(paths, "application." + value);
//            file.renameTo(to);
//
//            Log.d(TAG, to.getPath());
//            Log.d(TAG, to.getAbsolutePath());
//            Log.d(TAG, "" + file.getPath());
//            Log.d(TAG, "" + file.getAbsolutePath());
//        }
//        try {
//
//            FileOutputStream newFile = new FileOutputStream (Environment.getRootDirectory()+"/moon/moon.mp4");
//            FileInputStream oldFile = new FileInputStream (filePath);
//
//            // Transfer bytes from in to out
//            byte[] buf = new byte[10048];
//            int len;
//            while ((len = oldFile.read(buf)) > 0) {
//                newFile.write(buf, 0, len);
//            }
//            newFile.close();
//            oldFile.close();
//        } catch (IOException e) {
//            // TODO Auto-generated catch block
//            e.printStackTrace();
//        }
    }

    public static void copy(File src, File dst) throws IOException {
        try (InputStream in = new FileInputStream(src)) {
            try (OutputStream out = new FileOutputStream(dst)) {
                // Transfer bytes from in to out
                byte[] buf = new byte[1024];
                int len;
                while ((len = in.read(buf)) > 0) {
                    out.write(buf, 0, len);
                }
            }
        }
    }


    public String getFileName() {
        return fileName;
    }

    public void setFileName(String file_name) {
        this.fileName = file_name;
    }

    public Bitmap getResizedBitmap(File file) {
        int maxSize = 800;
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        Bitmap bitmap = BitmapFactory.decodeFile(file.getAbsolutePath(), bmOptions);

        int width = bitmap.getWidth();
        int height = bitmap.getHeight();

        float bitmapRatio = (float) width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(bitmap, width, height, true);
    }

    public void compressImage() {
        File imageFile = new File(getFilePath());
        Bitmap bitmap = getResizedBitmap(imageFile);
        setBitmap(bitmap);
        OutputStream os;

        try {
            os = new FileOutputStream(imageFile);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, os);
            os.flush();
            os.close();
        } catch (Exception e) {
            Log.e("err_compress_image", e.getMessage());
        }
        setFilePath(imageFile.getAbsolutePath());
        setFile(imageFile);
        setBitmap(bitmap);
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFileType(int fileType) {
        this.fileType = fileType;
    }

    public int getFileType() {
        return fileType;
    }

    public String getParamName() {
        return paramName;
    }

    public void setParamName(String paramName) {
        this.paramName = paramName;
    }

    public void setCompressObject(CompressObject compressObject) {
        this.compressObject = compressObject;
    }

    public byte[] getBytes() {
        File file = new File(getFilePath());
        int size = (int) file.length();
        byte[] bytes = new byte[size];
        try {
            BufferedInputStream buf = new BufferedInputStream(new FileInputStream(file));
            buf.read(bytes, 0, bytes.length);
            buf.close();
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return bytes;
    }

    public long duration(Context context) {
        if (getUri() == null)
            setUri(Uri.fromFile((this.file)));

        MediaMetadataRetriever retriever = new MediaMetadataRetriever();
        retriever.setDataSource(context, getUri());
        String time = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
        if (time == null && !time.equals("")) return 0;
        return Long.parseLong(Objects.requireNonNull(retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION)));
    }

    private Bitmap bitmap;

    public void setBitmap(Bitmap bitmap) {
        this.bitmap = bitmap;
    }

    public Bitmap getBitmap() {
        return bitmap;
    }

    public CompressObject getCompressObject() {
        return compressObject;
    }

    public void setUri(Uri uri) {
        this.uri = uri;
    }

    public Uri getUri() {
        if (uri == null) {
            setUri(Uri.fromFile((this.file)));
        }
        return uri;
    }

    public void setFile(File file) {
        this.file = file;
    }

    public File getFile() {
        return file;
    }
}
