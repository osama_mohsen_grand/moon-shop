package grand.app.moonshop.vollyutils;


import android.content.res.Configuration;
import android.util.Log;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.MutableLiveData;
import androidx.multidex.MultiDexApplication;
import grand.app.moonshop.base.ApplicationComponent;
import grand.app.moonshop.repository.FirebaseRepository;
import grand.app.moonshop.utils.LanguagesHelper;
import grand.app.moonshop.utils.images.ImageLoaderHelper;
import timber.log.Timber;

/**
 * Created by Akshay Raj on 7/17/2016.
 * Snow Corporation Inc.
 * www.snowcorp.org
 */
public class MyApplication extends MultiDexApplication {
    public static final String TAG = MyApplication.class.getSimpleName();
    private RequestQueue mRequestQueue;

    private static MyApplication mInstance;

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
        new ImageLoaderHelper(this);
        LanguagesHelper.changeLanguage(MyApplication.getInstance(), LanguagesHelper.getCurrentLanguage());
        DataBindingUtil.setDefaultComponent(new ApplicationComponent());
//        crashLog();
    }

    public FirebaseRepository firebaseRepository = new FirebaseRepository(new MutableLiveData<>());

    private void crashLog() {
        Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
            @Override
            public void uncaughtException(Thread thread, Throwable e) {
                handleUncaughtException(thread, e);
            }
        });
    }


    public void handleUncaughtException(Thread thread, Throwable e) {
        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy HH:mm", Locale.ENGLISH);
        Date c = Calendar.getInstance().getTime();
        String formattedDate = df.format(c);
        try {
            firebaseRepository.crashLog(formattedDate, e.getMessage());
            Log.d(TAG, "error catch end");
        } catch (Exception ex) {

        }
    }

    @Override
    public void onConfigurationChanged(@NonNull Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        LanguagesHelper.changeLanguage(MyApplication.getInstance(), LanguagesHelper.getCurrentLanguage());
    }

    private static class CrashReportingTree extends Timber.Tree {
        @Override
        protected void log(int priority, String tag, @NonNull String message, Throwable t) {
            if (priority == Log.VERBOSE || priority == Log.DEBUG) {
                return;
            }
        }
    }

    public static synchronized MyApplication getInstance() {
        return mInstance;
    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }

        return mRequestQueue;
    }

}