package grand.app.moonshop.notification;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Random;

import androidx.core.app.NotificationCompat;
import grand.app.moonshop.R;
import grand.app.moonshop.models.app.AppMoon;
import grand.app.moonshop.models.user.profile.User;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.utils.storage.SharedPreferenceHelper;
import grand.app.moonshop.utils.storage.user.UserHelper;
import grand.app.moonshop.views.activities.BaseActivity;
import timber.log.Timber;

import static android.app.NotificationManager.IMPORTANCE_HIGH;
import static grand.app.moonshop.utils.Constants.CHANNEL_ID;


public class GCMNotificationIntentService extends FirebaseMessagingService {

    @Override
    public void onNewToken(String s) {
        super.onNewToken(s);
        SharedPreferenceHelper.saveKey("token", s);
        Log.e("token", s);
    }


    private static final String TAG = "GCMNotificationIntentSe";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        //ar_text , en_text
        Timber.e("notification: start Here");
        if (UserHelper.getUserId() != -1) {
            String details = remoteMessage.getData().get(Constants.MESSAGE);
            NotificationGCMModel notificationGCMModel = AppMoon.getNotification(details);
            String title = notificationGCMModel.title, message = notificationGCMModel.body;
            Intent intent = new Intent(this, BaseActivity.class);
            Bundle bundle = new Bundle();
            bundle.putSerializable(Constants.BUNDLE_NOTIFICATION, notificationGCMModel);
            intent.putExtra(Constants.BUNDLE_NOTIFICATION, bundle);
            sendBroadCast(notificationGCMModel);

            PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(),
                    0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
            NotificationCompat.Builder notification = new NotificationCompat.Builder(this);

            notification.setColor(getResources().getColor(R.color.colorPrimary));
            notification.setPriority(IMPORTANCE_HIGH);
            notification.setContentTitle(title);
            notification.setColor(getResources().getColor(R.color.colorPrimary));
            notification.setContentText(message);
            notification.setStyle(new NotificationCompat.BigTextStyle().bigText(message));
            notification.setBadgeIconType(NotificationCompat.BADGE_ICON_SMALL);
            //        notification.setAutoCancel(false);
            notification.setAutoCancel(true);
            notification.setShowWhen(true);


            try {
                Uri uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                Ringtone r = RingtoneManager.getRingtone(getApplicationContext(), uri);
                r.play();
            } catch (Exception e) {
                e.printStackTrace();
            }
            notification.setDefaults(Notification.DEFAULT_SOUND | Notification.DEFAULT_LIGHTS | Notification.DEFAULT_VIBRATE);
            notification.setContentIntent(pendingIntent);

            int currentApiVersion = Build.VERSION.SDK_INT;
            if (currentApiVersion >= Build.VERSION_CODES.LOLLIPOP) {
                notification
                        .setBadgeIconType(NotificationCompat.BADGE_ICON_SMALL)
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher));
            } else {
                notification.setSmallIcon(R.mipmap.ic_launcher);
            }

            int random = new Random().nextInt(10000000) + 1;
            //Android O
            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//                NotificationHelper helper = new NotificationHelper(getApplicationContext());
//                helper.createChannels(title, message, intent);
////                helper.createChannels(title, message, intent);
//                Notification.Builder builder = helper.getChannel(title, message, pendingIntent,random);
//                helper.getManager().notify(random, builder.build());
                show_Notification(title, message, pendingIntent,random);
            } else
                notificationManager.notify(random, notification.build());
        }

    }

    public void show_Notification(String title, String body, PendingIntent pendingIntent,int number) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            Intent intent = new Intent(getApplicationContext(), BaseActivity.class);
            String CHANNEL_ID = "MYCHANNEL";
            NotificationChannel notificationChannel = null;

            notificationChannel = new NotificationChannel(CHANNEL_ID, getString(R.string.app_name), NotificationManager.IMPORTANCE_LOW);

//            PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), 1, intent, 0);
            Notification notification = null;

            notification = new Notification.Builder(getApplicationContext(), CHANNEL_ID)
                    .setContentText(title)
                    .setContentTitle(body)
                    .setContentIntent(pendingIntent)
                    .addAction(android.R.drawable.sym_action_chat, title, pendingIntent)
                    .setChannelId(CHANNEL_ID)
                    .setNumber(number)
                    .setSmallIcon(R.drawable.ic_logo_original)
                    .build();

            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

            notificationManager.createNotificationChannel(notificationChannel);
            notificationManager.notify(1, notification);
        }

    }


    private void sendBroadCast(NotificationGCMModel notificationGCMModel) {
        if(notificationGCMModel.notification_type == 1 || notificationGCMModel.notification_type == 5) {
            Intent intent = null;
            Bundle bundle = new Bundle();
            bundle.putSerializable(Constants.BUNDLE_NOTIFICATION, notificationGCMModel);
            User user = UserHelper.getUserDetails();
            if (notificationGCMModel.notification_type == 1) {//new order
                intent = new Intent(Constants.ORDER);
                intent.putExtra(Constants.BUNDLE_NOTIFICATION, bundle);
                getApplicationContext().sendBroadcast(intent);
                user.order_count = notificationGCMModel.orderCount;
            } else if (notificationGCMModel.notification_type == 5) {//chat normal
                intent = new Intent(Constants.CHAT);
                intent.putExtra(Constants.BUNDLE_NOTIFICATION, bundle);
                user.chats_count = notificationGCMModel.chatCount;
            }
            UserHelper.saveUserDetails(user);
            getApplicationContext().sendBroadcast(intent);
        }
    }

}
