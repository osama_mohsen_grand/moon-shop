package grand.app.moonshop.views.fragments.famous;


import android.content.Context;
import android.content.Intent;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.theartofdev.edmodo.cropper.CropImage;
import com.yalantis.ucrop.UCrop;

import java.io.File;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moonshop.R;
import grand.app.moonshop.adapter.AutoCompleteShopAdapter;
import grand.app.moonshop.base.BaseFragment;
import grand.app.moonshop.databinding.FragmentFamousAddNewAdsBinding;
import grand.app.moonshop.models.base.IdName;
import grand.app.moonshop.models.famous.home.ImageVideo;
import grand.app.moonshop.models.service.Datum;
import grand.app.moonshop.models.service.MainServiceResponse;
import grand.app.moonshop.utils.helper.AppUtils;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.utils.PopUp.PopUpInterface;
import grand.app.moonshop.utils.PopUp.PopUpMenuHelper;
import grand.app.moonshop.utils.resources.ResourceManager;
import grand.app.moonshop.utils.storage.user.UserHelper;
import grand.app.moonshop.utils.upload.FileOperations;
import grand.app.moonshop.viewmodels.famous.add.FamousAddNewAddsViewModel;
import grand.app.moonshop.vollyutils.AppHelper;
import grand.app.moonshop.vollyutils.VolleyFileObject;
import timber.log.Timber;

import static android.app.Activity.RESULT_OK;
import static com.facebook.FacebookSdk.getCacheDir;


public class FamousAddAddsFragment extends BaseFragment {

    private FragmentFamousAddNewAdsBinding fragmentFamousAddNewAdsBinding;
    private FamousAddNewAddsViewModel famousAddNewAddsViewModel;
    private AutoCompleteShopAdapter autoCompleteShopAdapter;
    private Timer timer = null;
    public PopUpMenuHelper popUpMenuHelper = new PopUpMenuHelper();
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        fragmentFamousAddNewAdsBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_famous_add_new_ads, container, false);
        init();
        getData();
        setEvent();
        return fragmentFamousAddNewAdsBinding.getRoot();
    }

    private void getData() {
        if(getArguments() != null && getArguments().containsKey(Constants.ADS)){
            ImageVideo imageVideo = (ImageVideo) getArguments().getSerializable(Constants.ADS);
            famousAddNewAddsViewModel.famousAddAlbumRequest.id = imageVideo.id+"";
            famousAddNewAddsViewModel.famousAddAlbumRequest.shop_id = imageVideo.shopId;
            famousAddNewAddsViewModel.famousAddAlbumRequest.setAdsExpire(imageVideo.adsExpired+"");
            famousAddNewAddsViewModel.famousAddAlbumRequest.type = imageVideo.type+"";
            famousAddNewAddsViewModel.title = (imageVideo.type == 1 ? ResourceManager.getString(R.string.your_image_had_been_selected) : ResourceManager.getString(R.string.your_video_had_been_selected));
            famousAddNewAddsViewModel.isEdit.set(true);
            famousAddNewAddsViewModel.showPage(true);
//            famousAddNewAddsViewModel.notifyChange();
        }else{
            famousAddNewAddsViewModel.getFamousRepository().mainService();
        }
    }

    private void init() {
        famousAddNewAddsViewModel = new FamousAddNewAddsViewModel();
        AppUtils.initVerticalRV(fragmentFamousAddNewAdsBinding.rvFamousSearch, fragmentFamousAddNewAdsBinding.rvFamousSearch.getContext(), 1);
//        fragmentFamousAddNewAdsBinding.autoName.setAutoCompleteDelay(5000);
//        fragmentFamousAddNewAdsBinding.autoName.setLoadingIndicator(getActivity(),fragmentFamousAddNewAdsBinding.pbLoadingIndicator);
        autoCompleteShopAdapter = new AutoCompleteShopAdapter(new ArrayList<>());
        fragmentFamousAddNewAdsBinding.autoName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                fragmentFamousAddNewAdsBinding.rvFamousSearch.setVisibility(View.VISIBLE);
                famousAddNewAddsViewModel.selected = false;
                famousAddNewAddsViewModel.famousAddAlbumRequest.shop_id = -1;

            }
        });


        timer = new Timer();
        TimerTask hourlyTask = new TimerTask() {
            @Override
            public void run() {
                // your code here...
                getActivityBase().runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        // Stuff that updates the UI
                        famousAddNewAddsViewModel.callService();
                    }
                });

            }
        };

// schedule the task to run starting now and then every hour...
        timer.schedule(hourlyTask, 0l, 5000);

        fragmentFamousAddNewAdsBinding.rvFamousSearch.setAdapter(autoCompleteShopAdapter);
        setEventAdapter();
        fragmentFamousAddNewAdsBinding.setFamousAddNewAddsViewModel(famousAddNewAddsViewModel);
    }

    private void setEvent() {
        famousAddNewAddsViewModel.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                String action = (String) o;
                handleActions(action, famousAddNewAddsViewModel.getFamousRepository().getMessage());
                assert action != null;
                if(action.equals(Constants.IMAGE)){
                    pickImageDialogSelect();
                }else if(action.equals(Constants.VIDEO)){
                    pickVideoDialogSelect();
                }else if(action.equals(Constants.SEARCH)){
                    autoCompleteShopAdapter.update(famousAddNewAddsViewModel.getFamousRepository().getFamousSearchShopResponse().data);
//                    AppHelper.hideKeyboard(getActivityBase());
                    AppHelper.hideKeyboard(context,fragmentFamousAddNewAdsBinding.getRoot());
                }else if(action.equals(Constants.SUCCESS)){
                    UserHelper.saveKey(Constants.RELOAD,Constants.TRUE);
                    getActivityBase().finish();
                }else if(action.equals(Constants.ERROR)){
                    showError(famousAddNewAddsViewModel.baseError);
                }else if(action.equals(Constants.MAIN_SERVICE)){
                    famousAddNewAddsViewModel.showPage(true);
//                    setEventHandle();
                    initPopUp();
                }
            }
        });
    }

    public int service_position = -1;

    private void initPopUp() {
        fragmentFamousAddNewAdsBinding.edtAddAdsMainService.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                ArrayList<AccountTypeModel> accountTypeModels = AppMoon.getAccountType();
                ArrayList<String> popUpModels = new ArrayList<>();
                for(Datum datum : famousAddNewAddsViewModel.getFamousRepository().mainServiceResponse.data)
                    popUpModels.add(datum.name);
                popUpMenuHelper.openPopUp(getActivity(), fragmentFamousAddNewAdsBinding.edtAddAdsMainService, popUpModels, new PopUpInterface() {
                    @Override
                    public void submitPopUp(int position) {
                        Datum datum  = famousAddNewAddsViewModel.getFamousRepository().mainServiceResponse.data.get(position);
                        fragmentFamousAddNewAdsBinding.edtAddAdsMainService.setText(datum.name);
                        famousAddNewAddsViewModel.famousAddAlbumRequest.serviceId = datum.id+"";
                        service_position = position;
                        fragmentFamousAddNewAdsBinding.edtAddAdsMainCategory.setText("");
                        famousAddNewAddsViewModel.famousAddAlbumRequest.categoryId = "";
                    }
                });
            }
        });

        fragmentFamousAddNewAdsBinding.edtAddAdsMainCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(service_position > -1) {
                    ArrayList<String> popUpModels = new ArrayList<>();
                    for(IdName datum : famousAddNewAddsViewModel.getFamousRepository().mainServiceResponse.data.get(service_position).categories)
                        popUpModels.add(datum.name);

                    popUpMenuHelper.openPopUp(getActivity(), fragmentFamousAddNewAdsBinding.edtAddAdsMainCategory, popUpModels, new PopUpInterface() {
                        @Override
                        public void submitPopUp(int position) {
                            IdName datum = famousAddNewAddsViewModel.getFamousRepository().mainServiceResponse.data.get(service_position).categories.get(position);
                            fragmentFamousAddNewAdsBinding.edtAddAdsMainCategory.setText(datum.name);
                            famousAddNewAddsViewModel.famousAddAlbumRequest.categoryId = datum.id + "";
                        }
                    });
                }
            }
        });
    }

    private void setEventHandle() {
        MainServiceResponse response= famousAddNewAddsViewModel.getFamousRepository().mainServiceResponse;
        if(response.data.size() > 0){
            setServiceCategory(response.data.get(0));
        }
    }

    private void setServiceCategory(Datum datum) {
        fragmentFamousAddNewAdsBinding.edtAddAdsMainService.setText(datum.name);
        famousAddNewAddsViewModel.famousAddAlbumRequest.serviceId = datum.id+"";
        if(datum.categories.size() > 0){
            fragmentFamousAddNewAdsBinding.edtAddAdsMainService.setText(datum.categories.get(0).name);
            famousAddNewAddsViewModel.famousAddAlbumRequest.categoryId = datum.categories.get(0).id+"";
        }else{
            fragmentFamousAddNewAdsBinding.edtAddAdsMainCategory.setText("");
            famousAddNewAddsViewModel.famousAddAlbumRequest.categoryId = "";
        }
    }

    private void setEventAdapter() {
        autoCompleteShopAdapter.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                int pos = (int) o;
                AppHelper.hideKeyboard(getActivityBase());
                fragmentFamousAddNewAdsBinding.autoName.setText(famousAddNewAddsViewModel.getFamousRepository().getFamousSearchShopResponse().data.get(pos).name);
                famousAddNewAddsViewModel.famousAddAlbumRequest.shop_id = famousAddNewAddsViewModel.getFamousRepository().getFamousSearchShopResponse().data.get(pos).id;
                fragmentFamousAddNewAdsBinding.rvFamousSearch.setVisibility(View.GONE);
                famousAddNewAddsViewModel.selected = true;
            }
        });
    }

    private static final String TAG = "FamousAddAddsFragment";

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        //super method removed
        Timber.e("onActivityResult:%s", requestCode);
//        if (requestCode == Constants.FILE_TYPE_IMAGE) {
//            famousAddNewAddsViewModel.mainImage = FileOperations.getVolleyFileObject(getActivity(), data, "image", Constants.FILE_TYPE_IMAGE);
//            famousAddNewAddsViewModel.title = ResourceManager.getString(R.string.your_image_had_been_selected);
//            famousAddNewAddsViewModel.famousAddAlbumRequest.type = "1";
//        }


        if (requestCode == UCrop.REQUEST_CROP && data != null) {
            final Uri resultUri = UCrop.getOutput(data);
            if (resultUri != null) {
                famousAddNewAddsViewModel.mainImage = new VolleyFileObject(Constants.IMAGE, resultUri.getPath(), Constants.FILE_TYPE_IMAGE);
                famousAddNewAddsViewModel.title = ResourceManager.getString(R.string.your_image_had_been_selected);
                famousAddNewAddsViewModel.famousAddAlbumRequest.type = "1";
            }
        } else if (requestCode == Constants.FILE_TYPE_IMAGE) {
            VolleyFileObject volleyFileObject = FileOperations.getVolleyFileObject(getActivity(), data, "image", Constants.FILE_TYPE_IMAGE);
            File file = new File(getCacheDir(), Constants.IMAGE + ".png");
            UCrop.of(Uri.fromFile(volleyFileObject.getFile()), Uri.fromFile(file))
                    .start(context, FamousAddAddsFragment.this);
        }
        else if(requestCode == Constants.FILE_TYPE_VIDEO){


            VolleyFileObject volleyFileObject = FileOperations.getVolleyFileObject(getActivity(), data, Constants.IMAGE, Constants.FILE_TYPE_VIDEO);
            MediaMetadataRetriever retriever = new MediaMetadataRetriever();
//use one of overloaded setDataSource() functions to set your data source
            retriever.setDataSource(context, Uri.fromFile(volleyFileObject.getFile()));
            String time = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
            long timeInMillisec = Long.parseLong(time);
            int duration = (int) (timeInMillisec / 1000);
            Timber.e("duration:" + duration);
            Timber.e("file length:" + volleyFileObject.getFile().length());
            long fileSize = Integer.parseInt(String.valueOf((volleyFileObject.getFile().length() / 1024) / 1024));
            if (duration <= 30) {
                famousAddNewAddsViewModel.mainImage = FileOperations.getVolleyFileObject(getActivity(), data, "image", Constants.FILE_TYPE_VIDEO);
                long x = famousAddNewAddsViewModel.mainImage.duration(context);
                Log.d(TAG,"Ads:"+x);
            } else {
                toastInfo(getString(R.string.maximum_video_duration_sixty_second));
            }

////            if( > )
//            famousAddNewAddsViewModel.title = ResourceManager.getString(R.string.your_video_had_been_selected);
//            famousAddNewAddsViewModel.famousAddAlbumRequest.type = "2";
        }
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE && data != null) {

            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();
                UCrop.of(resultUri, resultUri).start(context, FamousAddAddsFragment.this);
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Toast.makeText(context, "" + getString(R.string.error_failed_pick_gallery_image), Toast.LENGTH_SHORT).show();
            }
        }

        famousAddNewAddsViewModel.notifyChange();
        super.onActivityResult(requestCode, resultCode, data);
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDestroy() {
        famousAddNewAddsViewModel.reset();
        if(timer != null)
            timer.cancel();
        super.onDestroy();
    }
}
