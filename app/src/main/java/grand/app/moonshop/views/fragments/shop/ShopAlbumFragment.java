package grand.app.moonshop.views.fragments.shop;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moonshop.R;
import grand.app.moonshop.adapter.ShopAlbumAdapter;
import grand.app.moonshop.base.BaseFragment;
import grand.app.moonshop.databinding.FragmentShopAlbumBinding;
import grand.app.moonshop.utils.helper.AppUtils;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.utils.storage.user.UserHelper;
import grand.app.moonshop.viewmodels.famous.ShopAlbumViewModel;

public class ShopAlbumFragment extends BaseFragment {

    private FragmentShopAlbumBinding binding;
    private ShopAlbumViewModel viewModel;
    private ShopAlbumAdapter adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        UserHelper.saveKey(Constants.RELOAD, Constants.FALSE);
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_shop_album, container, false);
        getData();
        bind();
        setEvent();
        return binding.getRoot();

    }

    private void getData() {
        viewModel = new ShopAlbumViewModel();
        if (getArguments() != null && getArguments().containsKey(Constants.ID) && getArguments().containsKey(Constants.FAMOUS_ID)) {
            viewModel.getCategoryDetails(getArguments().getInt(Constants.FAMOUS_ID),getArguments().getInt(Constants.ID));
        }
    }


    private void bind() {
        AppUtils.initVerticalRV(binding.rvCompanyAdsCategoryDetails, binding.rvCompanyAdsCategoryDetails.getContext(), 3);
        adapter = new ShopAlbumAdapter(new ArrayList<>());
        binding.rvCompanyAdsCategoryDetails.setAdapter(adapter);
        binding.setViewModel(viewModel);
    }

    private void setEvent() {
        viewModel.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                String action = (String) o;
                handleActions(action, viewModel.getFamousRepository().getMessage());
                assert action != null;
                if (action.equals(Constants.SUCCESS)) {
                    adapter.update(viewModel.getFamousRepository().companyAdsCategoriesResponse.data);
                    viewModel.notifyChange();
                }
            }
        });
    }
}