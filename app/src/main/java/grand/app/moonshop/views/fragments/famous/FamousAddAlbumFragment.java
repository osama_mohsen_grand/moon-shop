package grand.app.moonshop.views.fragments.famous;


import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.theartofdev.edmodo.cropper.CropImage;
import com.yalantis.ucrop.UCrop;

import java.io.File;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moonshop.R;
import grand.app.moonshop.adapter.AlbumAddAdapter;
import grand.app.moonshop.base.BaseFragment;
import grand.app.moonshop.base.ParentActivity;
import grand.app.moonshop.databinding.FragmentFamousAddAlbumBinding;
import grand.app.moonshop.models.app.Mutable;
import grand.app.moonshop.utils.helper.AppUtils;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.utils.resources.ResourceManager;
import grand.app.moonshop.utils.storage.user.UserHelper;
import grand.app.moonshop.utils.upload.FileOperations;
import grand.app.moonshop.viewmodels.famous.add.FamousAddAlbumViewModel;
import grand.app.moonshop.vollyutils.VolleyFileObject;
import timber.log.Timber;

import static android.app.Activity.RESULT_OK;
import static com.facebook.FacebookSdk.getCacheDir;


public class FamousAddAlbumFragment extends BaseFragment {

    FragmentFamousAddAlbumBinding fragmentFamousAddAlbumBinding;
    FamousAddAlbumViewModel famousAddAlbumViewModel;
    String type = "1";//1: ImageVideo , 2: Video
    AlbumAddAdapter albumAdapter = null;
    int FILE_TYPE = -1;
//    int MAX_ITEMS = 6;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        fragmentFamousAddAlbumBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_famous_add_album, container, false);
        getData();
        init();
        setEvent();
        return fragmentFamousAddAlbumBinding.getRoot();
    }

    private void getData() {
        if(getArguments() != null && getArguments().containsKey(Constants.TYPE))
            type = getArguments().getString(Constants.TYPE);
        Timber.e("type:"+type);
        if(type.equals("1"))
            FILE_TYPE =  Constants.FILE_TYPE_IMAGE;
        else
            FILE_TYPE =  Constants.FILE_TYPE_VIDEO;
    }

    private void init() {
        famousAddAlbumViewModel = new FamousAddAlbumViewModel(type);
        AppUtils.initVerticalRV(fragmentFamousAddAlbumBinding.rvFamousAlbum, fragmentFamousAddAlbumBinding.rvFamousAlbum.getContext(), 3);
        albumAdapter = new AlbumAddAdapter(type,famousAddAlbumViewModel.paths);
        fragmentFamousAddAlbumBinding.rvFamousAlbum.setAdapter(albumAdapter);
        albumAdapter.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                Mutable mutable = (Mutable) o;
                if(mutable != null) {
                    famousAddAlbumViewModel.imageType = "";
                    famousAddAlbumViewModel.position = mutable.position;
                    if (FILE_TYPE == Constants.FILE_TYPE_IMAGE)
                        FileOperations.pickImage(context, FamousAddAlbumFragment.this, FILE_TYPE);
                    else
                        FileOperations.pickVideo(context, FamousAddAlbumFragment.this, FILE_TYPE);
                }
            }
        });
        fragmentFamousAddAlbumBinding.setFamousAddAlbumViewModel(famousAddAlbumViewModel);
    }


    private void setEvent() {
        famousAddAlbumViewModel.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                String action = (String) o;
                handleActions(action, famousAddAlbumViewModel.getFamousRepository().getMessage());
                assert action != null;
                if(action.equals(Constants.IMAGE)){
//                    Timber.e("TYPE:"+FILE_TYPE);
//                    if (FILE_TYPE == Constants.FILE_TYPE_IMAGE)
                        FileOperations.pickImage(context, FamousAddAlbumFragment.this, FILE_TYPE);
//                    else
//                        FileOperations.pickVideo(context, FamousAddAlbumFragment.this, FILE_TYPE);
                }else if(action.equals(Constants.SUCCESS)){
                    Toast.makeText(context, ""+famousAddAlbumViewModel.getFamousRepository().getMessage(), Toast.LENGTH_SHORT).show();
                    UserHelper.saveKey(Constants.RELOAD,Constants.TRUE);
                    ((ParentActivity)context).finish();
                }else if(action.equals(Constants.ERROR)){
                    showError(famousAddAlbumViewModel.baseError);
                }
            }
        });
    }

    int next = -1;

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        //super method removed
        Timber.e("onActivityResult:" + requestCode);
        if (requestCode == Constants.FILE_TYPE_IMAGE || requestCode == Constants.FILE_TYPE_VIDEO) {
            Timber.e("type:"+famousAddAlbumViewModel.imageType);
            if(famousAddAlbumViewModel.imageType.equals(Constants.IMAGE)) {
//                famousAddAlbumViewModel.mainImage = FileOperations.getVolleyFileObject(getActivity(), data, "image", Constants.FILE_TYPE_IMAGE);
//                famousAddAlbumViewModel.famousAddAlbumRequest.image = ResourceManager.getString(R.string.done_selected_image);
//                famousAddAlbumViewModel.notifyChange();



                VolleyFileObject volleyFileObject = FileOperations.getVolleyFileObject(getActivity(), data, Constants.IMAGE, Constants.FILE_TYPE_IMAGE);
                File file = new File(getCacheDir(), Constants.IMAGE + ".png");
                UCrop.of(Uri.fromFile(volleyFileObject.getFile()), Uri.fromFile(file))
                        .start(context, FamousAddAlbumFragment.this);


            }else{
                VolleyFileObject volleyFileObject = FileOperations.getVolleyFileObject(getActivity(), data, "images["+famousAddAlbumViewModel.position+"]", FILE_TYPE);
                famousAddAlbumViewModel.paths.set(famousAddAlbumViewModel.position,volleyFileObject);

                albumAdapter.update(famousAddAlbumViewModel.paths);
                famousAddAlbumViewModel.notifyChange();
            }
        }
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE && data != null) {

            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();
                UCrop.of(resultUri, resultUri).start(context, FamousAddAlbumFragment.this);
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Toast.makeText(context, "" + getString(R.string.error_failed_pick_gallery_image), Toast.LENGTH_SHORT).show();
            }
        } else if (requestCode == UCrop.REQUEST_CROP && data != null) {

            final Uri resultUri = UCrop.getOutput(data);
            if (resultUri != null) {
                famousAddAlbumViewModel.mainImage = new VolleyFileObject(Constants.IMAGE, resultUri.getPath(), Constants.FILE_TYPE_IMAGE);
                famousAddAlbumViewModel.famousAddAlbumRequest.image = ResourceManager.getString(R.string.done_selected_image);
                fragmentFamousAddAlbumBinding.imgAddAlbum.setImageURI(resultUri);
                famousAddAlbumViewModel.notifyChange();
            }


        }


        super.onActivityResult(requestCode, resultCode, data);
    }


}
