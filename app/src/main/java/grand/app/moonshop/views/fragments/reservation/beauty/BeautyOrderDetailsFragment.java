package grand.app.moonshop.views.fragments.reservation.beauty;


import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moonshop.R;
import grand.app.moonshop.adapter.ServiceAdapter;
import grand.app.moonshop.base.BaseFragment;
import grand.app.moonshop.databinding.FragmentBeautyOrderDetailsBinding;
import grand.app.moonshop.utils.helper.AppUtils;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.viewmodels.reservation.beauty.BeautyOrderDetailsViewModel;

/**
 * A simple {@link Fragment} subclass.
 */
public class BeautyOrderDetailsFragment extends BaseFragment {

    private View rootView;
    private FragmentBeautyOrderDetailsBinding fragmentBeautyOrderDetailsBinding;
    private BeautyOrderDetailsViewModel beautyOrderDetailsViewModel;
    private ServiceAdapter serviceAdapter;
    int id = -1;


    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        fragmentBeautyOrderDetailsBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_beauty_order_details, container, false);
        getData();
        bind();
        setEvent();
        return rootView;
    }

    private void getData() {
        if(getArguments() != null && getArguments().containsKey(Constants.ID))
            id = getArguments().getInt(Constants.ID);
    }


    private void bind() {
        beautyOrderDetailsViewModel = new BeautyOrderDetailsViewModel(id);
        AppUtils.initVerticalRV(fragmentBeautyOrderDetailsBinding.rvServices, fragmentBeautyOrderDetailsBinding.rvServices.getContext(), 1);
        fragmentBeautyOrderDetailsBinding.setBeautyOrderDetailsViewModel(beautyOrderDetailsViewModel);
        rootView = fragmentBeautyOrderDetailsBinding.getRoot();
    }

    private void setEvent() {
        beautyOrderDetailsViewModel.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                String action = (String) o;
                handleActions(action, beautyOrderDetailsViewModel.getReservationRepository().getMessage());
                assert action != null;
                if (action.equals(Constants.ORDER_DETAILS)) {
//                    Toast.makeText(context, ""+beautyOrderDetailsViewModel.getReservationRepository().getBeautyOrderDetailsResponse().data.services.size(), Toast.LENGTH_SHORT).show();
                    beautyOrderDetailsViewModel.showPage(true);
                    serviceAdapter = new ServiceAdapter(beautyOrderDetailsViewModel.getReservationRepository().getBeautyOrderDetailsResponse().data.services,new ArrayList<>());
                    fragmentBeautyOrderDetailsBinding.rvServices.setAdapter(serviceAdapter);
                }
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        beautyOrderDetailsViewModel.reset();

    }
}
