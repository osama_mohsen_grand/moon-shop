package grand.app.moonshop.views.fragments.product;


import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.GridLayoutManager;
import grand.app.moonshop.R;
import grand.app.moonshop.adapter.ProductAdapter;
import grand.app.moonshop.base.BaseFragment;
import grand.app.moonshop.customviews.dialog.DialogConfirm;
import grand.app.moonshop.databinding.FragmentProductsBinding;
import grand.app.moonshop.models.app.Mutable;
import grand.app.moonshop.models.product.Product;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.utils.EndlessRecyclerViewScrollListener;
import grand.app.moonshop.utils.dialog.DialogHelperInterface;
import grand.app.moonshop.utils.resources.ResourceManager;
import grand.app.moonshop.utils.storage.user.UserHelper;
import grand.app.moonshop.viewmodels.product.ProductsViewModel;
import grand.app.moonshop.views.activities.BaseActivity;
import timber.log.Timber;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProductsFragment extends BaseFragment {

    View rootView;
    private FragmentProductsBinding fragmentProductsBinding;
    private ProductsViewModel productViewModel;
    private ProductAdapter productAdapter;
    private GridLayoutManager gridLayoutManager;
    int id = -1;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        fragmentProductsBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_products, container, false);
        getData();
        bind();
        setEvent();
        return rootView;
    }


    private void getData() {
        if (getArguments() != null && getArguments().containsKey(Constants.ID))
            id = getArguments().getInt(Constants.ID);

    }


    private void bind() {
        productViewModel = new ProductsViewModel(id);
        gridLayoutManager = new GridLayoutManager(context, 2);
//        AppUtils.initVerticalRV(fragmentProductsBinding.rvProducts, fragmentProductsBinding.rvProducts.getContext(), 2);
        fragmentProductsBinding.setProductsViewModel(productViewModel);
        rootView = fragmentProductsBinding.getRoot();
    }

    private void setEvent() {
        productViewModel.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                String action = (String) o;
                handleActions(action, productViewModel.getProductRepository().getMessage());
                assert action != null;
                if (action.equals(Constants.SUCCESS)) {
                    Timber.e("arrive");
                    if (productViewModel.page == 1) {
                        Timber.e("arrive Page 1");
                        productAdapter = new ProductAdapter(productViewModel.getProductRepository().getProductsResponse().mData.products);
                        if (productViewModel.getProductRepository().getProductsResponse().mData.products.size() == Constants.PAGINATION_SIZE)
                            setScrollList();// list have maximum pagenation 20
                        else
                            setProductAdapter(); // list < maximum pagination
                        setEventAdapter();
                    } else {
                        Timber.e("arrive Page more");
                        productAdapter.notifyItemRangeInserted(productAdapter.getItemCount(), productViewModel.getProductRepository().getProductsResponse().mData.products.size() - 1);
                    }
                } else if (action.equals(Constants.ADD)) {
                    int type = Integer.parseInt(UserHelper.getUserDetails().type);
                    if (type < 4 || type == Integer.parseInt(Constants.TYPE_RESERVATION_BEAUTY)) {
                        Intent intent = new Intent(context, BaseActivity.class);
                        Timber.e("type:" + UserHelper.getUserDetails().shopType);
                        if (UserHelper.getUserDetails().shopType.equals(Constants.TYPE_SECTORAL))
                            intent.putExtra(Constants.PAGE, Constants.ADD_PRODUCT_SECTORAL);
                        else if (UserHelper.getUserDetails().shopType.equals(Constants.TYPE_CONSUMER) ||
                                UserHelper.getUserDetails().shopType.equals(Constants.TYPE_CONSUMER_SECTORAL))
                            intent.putExtra(Constants.PAGE, Constants.ADD_PRODUCT_CONSUMER_SECTORAL);
                        intent.putExtra(Constants.NAME_BAR, ResourceManager.getString(R.string.add_product));
                        Bundle bundle = new Bundle();
                        bundle.putInt(Constants.ID, id);
                        intent.putExtra(Constants.BUNDLE, bundle);
                        startActivityForResult(intent, Constants.RELOAD_RESULT);
                    } else if (type == Integer.parseInt(Constants.TYPE_COMMERCIALS)) {
                        Intent intent = new Intent(context, BaseActivity.class);
                        intent.putExtra(Constants.PAGE, Constants.ADD_ADS_COMPANY);
                        intent.putExtra(Constants.NAME_BAR, ResourceManager.getString(R.string.add_new_advertisement));
                        Bundle bundle = new Bundle();
                        bundle.putInt(Constants.CATEGORY_ID, id);
                        intent.putExtra(Constants.BUNDLE, bundle);
                        startActivityForResult(intent, Constants.RELOAD_RESULT);
                    }
                } else if (action.equals(Constants.DELETED)) {
                    productAdapter.remove(productViewModel.item_delete_position);
                }
            }
        });

    }

    private void setScrollList() {
        fragmentProductsBinding.rvProducts.addOnScrollListener(new EndlessRecyclerViewScrollListener(gridLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount) {
                productViewModel.page++;
                productViewModel.getProductRepository().getProducts(id, productViewModel.page);
            }
        });
        setProductAdapter();
    }

    private static final String TAG = "ProductsFragment";

    private void setProductAdapter() {
        fragmentProductsBinding.rvProducts.setLayoutManager(gridLayoutManager);
        fragmentProductsBinding.rvProducts.setAdapter(productAdapter);
    }

    private void setEventAdapter() {
        productAdapter.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                Mutable mutable = (Mutable) o;
                Log.d(TAG, mutable.type);
                if (mutable.type.equals(Constants.EDIT)) {
                    Intent intent = new Intent(context, BaseActivity.class);
                    intent.putExtra(Constants.PAGE, Constants.ADD_CATEGORY);
                    intent.putExtra(Constants.NAME_BAR, ResourceManager.getString(R.string.edit_category));
                    Bundle bundle = new Bundle();
                    bundle.putInt(Constants.ID, productViewModel.getProductRepository().getProductsResponse().mData.products.get(mutable.position).mId);
                    bundle.putString(Constants.NAME, productViewModel.getProductRepository().getProductsResponse().mData.products.get(mutable.position).mName);
                    intent.putExtra(Constants.BUNDLE, bundle);
                    startActivityForResult(intent, Constants.RELOAD_RESULT);
                } else if (mutable.type.equals(Constants.DELETE)) {
                    new DialogConfirm(getActivity())
                            .setTitle(ResourceManager.getString(R.string.remove_product))
                            .setMessage(ResourceManager.getString(R.string.do_you_want_delete_product))
                            .setActionText(ResourceManager.getString(R.string.remove_item))
                            .setActionCancel(ResourceManager.getString(R.string.cancel))
                            .show(new DialogHelperInterface() {
                                @Override
                                public void OnClickListenerContinue(Dialog dialog, View view) {
                                    productViewModel.item_delete_position = mutable.position;
                                    productViewModel.getProductRepository().delete(productViewModel.getProductRepository().getProductsResponse().mData.products.get(mutable.position).mId);
                                }
                            });

                } else if (mutable.type.equals(Constants.PRODUCT)) {
                    Product product = productViewModel.getProductRepository().getProductsResponse().mData.products.get(mutable.position);
                    Log.d("product", product.mId + "");
                    Bundle bundle = new Bundle();
                    bundle.putInt(Constants.ID, product.mId);
                    Intent intent = new Intent(context, BaseActivity.class);
//                    Bundle options = ActivityOptionsCompat.makeSceneTransitionAnimation((ParentActivity)context,
//                            mutable.v, "image").toBundle();
                    if (UserHelper.getUserDetails().type.equals(Constants.TYPE_COMMERCIALS)) {
                        intent.putExtra(Constants.PAGE, Constants.ADD_ADS_COMPANY);
                        intent.putExtra(Constants.NAME_BAR, getString(R.string.product_details));
                        bundle.putInt(Constants.ID, product.mId);
                        bundle.putInt(Constants.CATEGORY_ID, id);
                    } else {
//                        intent.putExtra(Constants.PAGE, Constants.PRODUCT);
                        Log.d(TAG, "done:" + product.mDescription);
                        Log.d(TAG, "done:" + product.mId);
                        intent.putExtra(Constants.NAME_BAR, getString(R.string.product_details));
                        if (UserHelper.getUserDetails().shopType.equals(Constants.TYPE_SECTORAL))
                            intent.putExtra(Constants.PAGE, Constants.ADD_PRODUCT_SECTORAL);
                        else if (UserHelper.getUserDetails().shopType.equals(Constants.TYPE_CONSUMER) || UserHelper.getUserDetails().shopType.equals(Constants.TYPE_CONSUMER_SECTORAL))
                            intent.putExtra(Constants.PAGE, Constants.ADD_PRODUCT_CONSUMER_SECTORAL);
                        bundle.putInt(Constants.PRODUCT_ID, product.mId);
                        bundle.putInt(Constants.ID, id);
                    }
                    intent.putExtra(Constants.BUNDLE, bundle);
                    startActivityForResult(intent, Constants.RELOAD_RESULT);
                }
            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        //super method removed
        Timber.e("onActivityResult:" + requestCode);
        if (resultCode == Activity.RESULT_OK && requestCode == Constants.RELOAD_RESULT) {
            productViewModel.page = 1;
            productViewModel.getProductRepository().getProducts(id, productViewModel.page);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }


}
