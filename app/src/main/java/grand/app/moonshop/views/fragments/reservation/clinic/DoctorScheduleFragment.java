package grand.app.moonshop.views.fragments.reservation.clinic;


import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.databinding.DataBindingUtil;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moonshop.R;
import grand.app.moonshop.adapter.DoctorScheduleAdapter;
import grand.app.moonshop.base.BaseFragment;
import grand.app.moonshop.customviews.dialog.DialogDateSchedule;
import grand.app.moonshop.databinding.FragmentDoctorScheduleBinding;
import grand.app.moonshop.models.app.Mutable;
import grand.app.moonshop.models.reservation.doctor.schedule.AddScheduleRequest;
import grand.app.moonshop.models.reservation.doctor.schedule.Day;
import grand.app.moonshop.utils.helper.AppUtils;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.utils.dialog.DialogScheduleInterface;
import grand.app.moonshop.viewmodels.reservation.doctors.DoctorScheduleViewModel;
import timber.log.Timber;


public class DoctorScheduleFragment extends BaseFragment {

    private FragmentDoctorScheduleBinding fragmentDoctorScheduleBinding;
    private DoctorScheduleViewModel doctorScheduleViewModel;
    private DoctorScheduleAdapter doctorScheduleAdapter;
    private int id;
    int _switch = -1;

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        fragmentDoctorScheduleBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_doctor_schedule, container, false);
        getData();
        bind();
        setEvent();
        return fragmentDoctorScheduleBinding.getRoot();
    }

    private void getData() {
        if (getArguments() != null && getArguments().containsKey(Constants.ID))
            id = getArguments().getInt(Constants.ID);
    }

    private void bind() {
        doctorScheduleViewModel = new DoctorScheduleViewModel(id);
        AppUtils.initVerticalRV(fragmentDoctorScheduleBinding.rvDoctorsSchedule, fragmentDoctorScheduleBinding.rvDoctorsSchedule.getContext(), 1);
        fragmentDoctorScheduleBinding.setDoctorScheduleViewModel(doctorScheduleViewModel);
    }

    private void setEvent() {
        doctorScheduleViewModel.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                String action = (String) o;
                handleActions(action, doctorScheduleViewModel.getReservationRepository().getMessage());
                assert action != null;
                if (action.equals(Constants.SCHEDULE)) {
                    doctorScheduleViewModel.showPage(true);
                    Timber.e("" + doctorScheduleViewModel.getReservationRepository().getDoctorScheduleResponse().days);
                    doctorScheduleAdapter = new DoctorScheduleAdapter(doctorScheduleViewModel.getReservationRepository().getDoctorScheduleResponse().days);
                    fragmentDoctorScheduleBinding.rvDoctorsSchedule.setAdapter(doctorScheduleAdapter);
                    setEventAdapter();
                }
                if (action.equals(Constants.ADD)) {
                    toastMessage(doctorScheduleViewModel.getReservationRepository().getScheduleResponse().msg);
                    Timber.e("size:"+doctorScheduleViewModel.getReservationRepository().getDoctorScheduleResponse().days.get(doctorScheduleViewModel.position_updated)
                            .schedules.size());
                    if(doctorScheduleViewModel.getReservationRepository().getScheduleResponse().schedules != null) {
                        doctorScheduleViewModel.getReservationRepository().getDoctorScheduleResponse().days.get(doctorScheduleViewModel.position_updated)
                                .schedules = doctorScheduleViewModel.getReservationRepository().getScheduleResponse().schedules;
//                        doctorScheduleViewModel.getReservationRepository().getDoctorScheduleResponse().days.get(doctorScheduleViewModel.position_updated)._switch = true;
                    }else{
//                        doctorScheduleViewModel.getReservationRepository().getDoctorScheduleResponse().days.get(doctorScheduleViewModel.position_updated)._switch =
//                                !doctorScheduleViewModel.getReservationRepository().getDoctorScheduleResponse().days.get(doctorScheduleViewModel.position_updated)._switch;
                    }
//                    if(_switch == 1)
//                    doctorScheduleViewModel.getReservationRepository().getDoctorScheduleResponse().days.get(doctorScheduleViewModel.position_updated)._switch = true;
//                    else

                    doctorScheduleViewModel.getReservationRepository().getDoctorScheduleResponse().days.get(doctorScheduleViewModel.position_updated)._switch = (_switch == 0 ? false : true);

//                    doctorScheduleAdapter = null;
//                    fragmentDoctorScheduleBinding.rvDoctorsSchedule.setAdapter(doctorScheduleAdapter);
//                    doctorScheduleAdapter = new DoctorScheduleAdapter(doctorScheduleViewModel.getReservationRepository().getDoctorScheduleResponse().days);
//                    fragmentDoctorScheduleBinding.rvDoctorsSchedule.setAdapter(doctorScheduleAdapter);
//                    Timber.e("size_update:"+doctorScheduleViewModel.getReservationRepository().getDoctorScheduleResponse().days.get(doctorScheduleViewModel.position_updated)
//                            .schedules.size());
//                    setEventAdapter();
                    doctorScheduleAdapter.update(doctorScheduleViewModel.position_updated,doctorScheduleViewModel.getReservationRepository().getDoctorScheduleResponse().days.get(doctorScheduleViewModel.position_updated));
                }
            }
        });
    }


    private void setEventAdapter() {
        doctorScheduleAdapter.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                Mutable mutable = (Mutable) o;
                if(mutable != null) {
                    _switch = -1;
                    Day day = doctorScheduleViewModel.getReservationRepository().getDoctorScheduleResponse().days.get(mutable.position);

                    if (mutable.key.equals(Constants.TRUE)) {
                        _switch = 1;
                    } else if (mutable.key.equals(Constants.FALSE)) {
                        _switch = 0;
                    } else if (mutable.key.equals(Constants.EDIT)) {
                        _switch = (day._switch ? 1 : 0);
                    }
                    if (mutable.type.equals(Constants.DOCTOR_SCHEDULE)) {
                        doctorScheduleViewModel.position_updated = mutable.position;
                        DialogDateSchedule dialogDateSchedule = new DialogDateSchedule(context);
                        int status = (day.schedules.size() > 0 ? 2 : 1);
                        dialogDateSchedule.init(id, day, status, _switch);
                        dialogDateSchedule.show(new DialogScheduleInterface() {
                            @Override
                            public void OnClickListenerContinue(Dialog dialog, AddScheduleRequest addScheduleRequest) {
                                if (addScheduleRequest != null)
                                    doctorScheduleViewModel.addSchedule(addScheduleRequest);
                                else {
                                    doctorScheduleAdapter.update(doctorScheduleViewModel.position_updated, doctorScheduleViewModel.getReservationRepository().getDoctorScheduleResponse().days.get(doctorScheduleViewModel.position_updated));
                                }
                            }
                        });
                    } else if (mutable.type.equals(Constants.DOCTOR_SCHEDULE_SUBMIT)) {
                        doctorScheduleViewModel.position_updated = mutable.position;
                        AddScheduleRequest addScheduleRequest = new AddScheduleRequest(id, day.id, 2, _switch);
                        doctorScheduleViewModel.addSchedule(addScheduleRequest);
                    }
                }
            }
        });
    }


    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK && requestCode == Constants.RELOAD_RESULT) {
            doctorScheduleViewModel.getReservationRepository().getDoctorList();
        }

        super.onActivityResult(requestCode, resultCode, data);
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        doctorScheduleViewModel.reset();
    }
}
