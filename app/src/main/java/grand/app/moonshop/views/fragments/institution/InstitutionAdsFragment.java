package grand.app.moonshop.views.fragments.institution;


import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moonshop.R;
import grand.app.moonshop.adapter.AlbumAdapter;
import grand.app.moonshop.base.BaseFragment;
import grand.app.moonshop.customviews.dialog.DialogConfirm;
import grand.app.moonshop.databinding.FragmentInstitutionAdsBinding;
import grand.app.moonshop.models.app.Mutable;
import grand.app.moonshop.models.shop.ShopData;
import grand.app.moonshop.utils.helper.AppUtils;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.utils.dialog.BottomSheetDialogHelper;
import grand.app.moonshop.utils.dialog.DialogHelperInterface;
import grand.app.moonshop.utils.dialog.DialogHelperSelectedInterface;
import grand.app.moonshop.utils.resources.ResourceManager;
import grand.app.moonshop.utils.storage.user.UserHelper;
import grand.app.moonshop.viewmodels.institution.InstitutionAdsViewModel;
import grand.app.moonshop.views.activities.BaseActivity;

public class InstitutionAdsFragment extends BaseFragment {
    private FragmentInstitutionAdsBinding fragmentInstitutionAdsBinding;
    private InstitutionAdsViewModel institutionAdsViewModel;
    public String type_form = "", tab = "";
    private AlbumAdapter albumImagesAdapter;
    private BottomSheetDialogHelper bottomSheetDialogHelper;
    private ShopData shopData = null;
    boolean showDelete = true;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        UserHelper.saveKey(Constants.RELOAD, Constants.TRUE);
        fragmentInstitutionAdsBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_institution_ads, container, false);
        getData();
        bind();
        setEvent();
        return fragmentInstitutionAdsBinding.getRoot();

    }

    @Override
    public void onResume() {
        super.onResume();
        if (isReloadPage())
            institutionAdsViewModel.getAds();

    }

    private void getData() {
        if (getArguments() != null && getArguments().containsKey(Constants.INSTITUTIONS)) {
            shopData = (ShopData) getArguments().getSerializable(Constants.INSTITUTIONS);
        }
        if (getArguments() != null && getArguments().containsKey(Constants.TYPE)) {
            type_form = getArguments().getString(Constants.TYPE);
        }
        if (getArguments() != null && getArguments().containsKey(Constants.SHOW_DELETE)) {
            showDelete = getArguments().getBoolean(Constants.SHOW_DELETE,false);
        }
    }

    private void bind() {
        institutionAdsViewModel = new InstitutionAdsViewModel();
        bottomSheetDialogHelper = new BottomSheetDialogHelper(getActivityBase());
        AppUtils.initVerticalRV(fragmentInstitutionAdsBinding.rvAds, fragmentInstitutionAdsBinding.rvAds.getContext(), 3);
        fragmentInstitutionAdsBinding.setInstitutionAdsViewModel(institutionAdsViewModel);
    }


    private void setEvent() {
        institutionAdsViewModel.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                String action = (String) o;
                handleActions(action, institutionAdsViewModel.getInstitutionRepository().getMessage());
                assert action != null;
                if (action.equals(Constants.ADDS)) {
                    albumImagesAdapter = new AlbumAdapter(institutionAdsViewModel.getInstitutionRepository().getAlbumResponse().data,false);
                    fragmentInstitutionAdsBinding.rvAds.setAdapter(albumImagesAdapter);
                    setEventAdapter();
                } else if (action.equals(Constants.FILTER)) {
                    if (bottomSheetDialogHelper.filterDialog == null) {
                        bottomSheetDialogHelper.filterFamousShopAds(institutionAdsViewModel.getInstitutionRepository().getFilterShopAdsResponse().data, false, new DialogHelperSelectedInterface() {
                            @Override
                            public void onClickListener(Dialog dialog, String type, Object object) {
                                if (type.equals(Constants.ARRAY)) {
                                    ArrayList<Integer> list = (ArrayList<Integer>) object;
                                    if (list != null && list.size() > 0) {
                                        institutionAdsViewModel.selectFilter(list);
                                    }
                                }
                            }
                        });
                    } else {
                        bottomSheetDialogHelper.filterDialog.show();
                    }
                } else if (action.equals(Constants.DELETE)) {
                    toastMessage(institutionAdsViewModel.getInstitutionRepository().getMessage(), R.drawable.ic_check_white, R.color.colorPrimary);
                    albumImagesAdapter.remove(institutionAdsViewModel.category_delete_position);
                }
            }
        });
    }


    private void setEventAdapter() {
        albumImagesAdapter.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                Mutable mutable = (Mutable) o;
                if (mutable.type.equals(Constants.IMAGE)) {
                    Intent intent = new Intent(context, BaseActivity.class);
                    intent.putExtra(Constants.PAGE, Constants.ZOOM);
                    intent.putExtra(Constants.NAME_BAR, institutionAdsViewModel.getInstitutionRepository().getAlbumResponse().data.get(mutable.position).name);
                    Bundle bundle = new Bundle();
                    bundle.putString(Constants.IMAGE, institutionAdsViewModel.getInstitutionRepository().getAlbumResponse().data.get(mutable.position).image);
                    intent.putExtra(Constants.BUNDLE, bundle);
                    startActivityForResult(intent, Constants.RELOAD_RESULT);
                } else if (mutable.type.equals(Constants.VIDEO)) {
                    Intent intent = new Intent(context, BaseActivity.class);
                    intent.putExtra(Constants.PAGE, Constants.VIDEO);
                    intent.putExtra(Constants.NAME_BAR,institutionAdsViewModel.getInstitutionRepository().getAlbumResponse().data.get(mutable.position).name);
                    Bundle bundle = new Bundle();
//                    Timber.e("video:"+institutionAdsViewModel.getInstitutionRepository().getFilterShopAdsResponse()data.get(mutable.position).capture);
                    bundle.putString(Constants.VIDEO, institutionAdsViewModel.getInstitutionRepository().getAlbumResponse().data.get(mutable.position).capture);
                    intent.putExtra(Constants.BUNDLE, bundle);
                    startActivity(intent);
                } else if (mutable.type.equals(Constants.DELETE)) {
                    new DialogConfirm(getActivity())
                            .setTitle(ResourceManager.getString(R.string.remove_product))
                            .setMessage(ResourceManager.getString(R.string.do_you_want_delete_image))
                            .setActionText(ResourceManager.getString(R.string.remove_item))
                            .setActionCancel(ResourceManager.getString(R.string.cancel))
                            .show(new DialogHelperInterface() {
                                @Override
                                public void OnClickListenerContinue(Dialog dialog, View view) {
                                    institutionAdsViewModel.category_delete_position = mutable.position;
                                    institutionAdsViewModel.delete(institutionAdsViewModel.getInstitutionRepository().getFilterShopAdsResponse().data.remove(mutable.position).id);
                                }
                            });
                }
            }
        });
    }

}

