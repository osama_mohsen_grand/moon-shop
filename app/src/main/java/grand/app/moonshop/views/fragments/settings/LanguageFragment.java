package grand.app.moonshop.views.fragments.settings;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moonshop.R;
import grand.app.moonshop.base.BaseFragment;
import grand.app.moonshop.base.IAnimationSubmit;
import grand.app.moonshop.base.ParentActivity;
import grand.app.moonshop.databinding.FragmentLanguageBinding;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.utils.LanguagesHelper;
import grand.app.moonshop.utils.storage.user.UserHelper;
import grand.app.moonshop.viewmodels.language.LanguageViewModel;
import grand.app.moonshop.views.activities.BaseActivity;
import grand.app.moonshop.views.activities.MainActivity;
import grand.app.moonshop.vollyutils.MyApplication;
import timber.log.Timber;


public class LanguageFragment extends BaseFragment {
    View rootView;
    private FragmentLanguageBinding fragmentLanguageBinding;
    private LanguageViewModel languageViewModel;

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentLanguageBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_language, container, false);
        bind();
        rootView = fragmentLanguageBinding.getRoot();
        return rootView;
    }

    private void bind() {
        languageViewModel = new LanguageViewModel();
        setEvents();
        fragmentLanguageBinding.setLanguageViewModel(languageViewModel);
    }

    private void setEvents() {
        languageViewModel.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                String action = (String) o;
//                handleActions(action,languageViewModel.getPrivacyRepository().getMessage());
                assert action != null;
                Intent intent = null;
                if(action.equals(Constants.LANGUAGE)){
                    UserHelper.saveKey(Constants.LANGUAGE_HAVE,Constants.TRUE);
                    LanguagesHelper.setLanguage(languageViewModel.getLanguage());
                    LanguagesHelper.changeLanguage(context,languageViewModel.getLanguage());
                    LanguagesHelper.changeLanguage(MyApplication.getInstance(),languageViewModel.getLanguage());

                    toastMessage(getString(R.string.your_language_had_been_updated));
                    if(UserHelper.getUserId() == -1) {
                        intent = new Intent(context, BaseActivity.class);
                        intent.putExtra(Constants.PAGE, Constants.COUNTRIES);
                        ((ParentActivity)context).finishAffinity();
                        startActivity(intent);
                    }else{
                        ((ParentActivity)context).finishAffinity();
                        startActivity(new Intent(context, MainActivity.class));
                    }
                }else if(action.equals(Constants.ERROR)){
                    showError(languageViewModel.error);
                }
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        languageViewModel.reset();
    }


}
