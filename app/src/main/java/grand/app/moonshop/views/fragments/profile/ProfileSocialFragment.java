package grand.app.moonshop.views.fragments.profile;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moonshop.R;
import grand.app.moonshop.base.BaseFragment;
import grand.app.moonshop.databinding.FragmentProfileSocialBinding;
import grand.app.moonshop.utils.helper.AppUtils;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.utils.storage.user.UserHelper;
import grand.app.moonshop.viewmodels.profile.ProfileSocialViewModel;


public class ProfileSocialFragment extends BaseFragment {
    private FragmentProfileSocialBinding binding;
    public ProfileSocialViewModel viewModel;
    private static final String TAG = "ProfileSocialFragment";

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_profile_social, container, false);
        viewModel = new ProfileSocialViewModel();
        AppUtils.initVerticalRV(binding.rvServices, binding.rvServices.getContext(), 1);
        AppUtils.initHorizontalRV(binding.rvServicesView, binding.rvServicesView.getContext(), 1);
        binding.rvServices.setAdapter(viewModel.socialAdapter);
        binding.rvServicesView.setAdapter(viewModel.socialViewAdapter);
        binding.setViewModel(viewModel);
        if(getArguments() != null &&  getArguments().containsKey(Constants.FROM) && getArguments().getString(Constants.FROM).equals(Constants.MENU)){
            viewModel.repository.accountInfo(UserHelper.getUserId());
        }
        setEvent();
        return binding.getRoot();
    }

    private void setEvent() {
        viewModel.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                String action = (String) o;
                handleActions(action, viewModel.repository.getMessage());
                if (action.equals(Constants.SUCCESS)) {
//                    Log.d(TAG,"ACCOUNT INFO");
                    viewModel.profileResponse = viewModel.repository.profileInfoResponse;
                    viewModel.socialAdapter.update(viewModel.response.profile);
                    viewModel.showPage(true);
                } else if(action.equals(Constants.UPDATE)){
                    toastMessage(viewModel.repository.getMessage());
                }else if(action.equals(Constants.ACCOUNT_INFO)){
                    viewModel.setResponse(viewModel.repository.accountInfoResponse);
                    viewModel.socialAdapter.updateEditable(true);
                    viewModel.notifyChange();
                } else if (action.equals(Constants.EMAIL)) {
                    AppUtils.openEmail(context, viewModel.response.email, getString(R.string.app_name), "");
                } else if (action.equals(Constants.PHONE)) {
                    AppUtils.openPhone(context, viewModel.response.number);
                } else if (action.equals(Constants.WEB)) {
                    AppUtils.openBrowser(context, viewModel.response.website);
                }else if(action.equals(Constants.ERROR)){
                    showError(viewModel.baseError);
                }
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (viewModel != null) viewModel.reset();
    }


}
