package grand.app.moonshop.views.fragments.auth;


import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hbb20.CountryCodePicker;

import java.util.ArrayList;

import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moonshop.R;
import grand.app.moonshop.base.BaseFragment;
import grand.app.moonshop.base.ParentActivity;
import grand.app.moonshop.databinding.FragmentLoginBinding;
import grand.app.moonshop.databinding.FragmentPhoneVerificationBinding;
import grand.app.moonshop.models.app.AccountTypeModel;
import grand.app.moonshop.models.app.AppMoon;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.utils.PopUp.PopUpInterface;
import grand.app.moonshop.utils.PopUp.PopUpMenuHelper;
import grand.app.moonshop.viewmodels.user.LoginViewModel;
import grand.app.moonshop.viewmodels.user.PhoneVerificationViewModel;
import grand.app.moonshop.views.activities.BaseActivity;
import grand.app.moonshop.views.activities.MainActivity;
import timber.log.Timber;

/**
 * A simple {@link Fragment} subclass.
 */
public class PhoneVerificationFragment extends BaseFragment {
    private FragmentPhoneVerificationBinding fragmentPhoneVerificationBinding;
    private PhoneVerificationViewModel phoneVerificationViewModel;
    private boolean changePassword=false;

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        fragmentPhoneVerificationBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_phone_verification, container, false);


        if(getArguments()!=null&&getArguments().containsKey(Constants.CHANGE_PASSWORD))
            changePassword = getArguments().getBoolean(Constants.CHANGE_PASSWORD,false);


        bind();
        setEvent();



        Timber.e("FORGET_PASSWORD"+changePassword);

        return fragmentPhoneVerificationBinding.getRoot();
    }





    private void bind() {
        phoneVerificationViewModel = new PhoneVerificationViewModel(changePassword);
        fragmentPhoneVerificationBinding.ccp.clearFocus();
        phoneVerificationViewModel.cpp = "+"+fragmentPhoneVerificationBinding.ccp.getDefaultCountryCode();
        fragmentPhoneVerificationBinding.ccp.setCountryForNameCode(fragmentPhoneVerificationBinding.ccp.getDefaultCountryNameCode());
        fragmentPhoneVerificationBinding.ccp.setOnCountryChangeListener(() -> phoneVerificationViewModel.cpp = "+"+fragmentPhoneVerificationBinding.ccp.getSelectedCountryCode());
        fragmentPhoneVerificationBinding.setPhoneVerificationViewModel(phoneVerificationViewModel);
    }


    private void setEvent() {
        phoneVerificationViewModel.mMutableLiveData.observe((LifecycleOwner) context, o -> {
            String action = (String) o;
            Timber.e(phoneVerificationViewModel.getVerificationFirebaseSMSRepository().getMessage());
            handleActions(action, phoneVerificationViewModel.getVerificationFirebaseSMSRepository().getMessage());
            assert action != null;

            if(action.equals(Constants.WRITE_CODE)){

                toastMessage(phoneVerificationViewModel.getVerificationFirebaseSMSRepository().getMessage(),R.drawable.ic_check_white,R.color.colorPrimary);
                Intent intent = new Intent(context, BaseActivity.class);
                intent.putExtra(Constants.PAGE,Constants.VERIFICATION);
                Bundle bundle = getArguments();
                if(bundle == null)
                    bundle = new Bundle();
                bundle.putString(Constants.TYPE,Constants.REGISTRATION);
                bundle.putString(Constants.PHONE, phoneVerificationViewModel.cpp + phoneVerificationViewModel.getPhone());
                bundle.putString(Constants.VERIFY_ID,phoneVerificationViewModel.getVerificationFirebaseSMSRepository().getVerificationId());
                bundle.putString(Constants.NAME_BAR,"");
                bundle.putString(Constants.TYPE, changePassword?Constants.FORGET_PASSWORD:Constants.REGISTRATION);
                intent.putExtra(Constants.BUNDLE,bundle);

                ((ParentActivity)context).finish();
                startActivity(intent);



            }
        });

        phoneVerificationViewModel.mMutableLiveDataLogin.observe((LifecycleOwner) context, o -> {
            String action = (String) o;
            Timber.e(phoneVerificationViewModel.getLoginRepository().getMessage());
            handleActions(action, phoneVerificationViewModel.getLoginRepository().getMessage());
            assert action != null;
            if(action.equals(Constants.SUCCESS)){

                phoneVerificationViewModel.sendCode();
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(phoneVerificationViewModel != null) phoneVerificationViewModel.reset();
    }

}
