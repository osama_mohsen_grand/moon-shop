package grand.app.moonshop.views.fragments.auth;


import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moonshop.R;
import grand.app.moonshop.base.BaseFragment;
import grand.app.moonshop.base.ParentActivity;
import grand.app.moonshop.databinding.FragmentChangePasswordBinding;
import grand.app.moonshop.databinding.FragmentProfileBinding;
import grand.app.moonshop.models.country.Region;
import grand.app.moonshop.models.user.changepassword.ChangePasswordRequest;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.utils.PopUp.PopUpMenuHelper;
import grand.app.moonshop.utils.resources.ResourceManager;
import grand.app.moonshop.utils.upload.FileOperations;
import grand.app.moonshop.viewmodels.user.ChangePasswordViewModel;
import grand.app.moonshop.viewmodels.user.ProfileViewModel;
import grand.app.moonshop.views.activities.BaseActivity;
import grand.app.moonshop.views.activities.MapAddressActivity;
import grand.app.moonshop.vollyutils.VolleyFileObject;
import timber.log.Timber;

/**
 * A simple {@link Fragment} subclass.
 */
public class ChangePasswordFragment extends BaseFragment {

    View rootView;
    private FragmentChangePasswordBinding fragmentChangePasswordBinding;
    private ChangePasswordViewModel changePasswordViewModel;
    public String type = "";
    private boolean forgotPassword=false;

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentChangePasswordBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_change_password, container, false);
        getData();
        bind();
        setEvent();





        return fragmentChangePasswordBinding.getRoot();
    }

    private void getData() {
        if (getArguments() != null && getArguments().containsKey(Constants.TYPE)) {
            type = getArguments().getString(Constants.TYPE);
            forgotPassword = type.equals(Constants.FORGET_PASSWORD);
        }




    }

    private void bind() {

        if(forgotPassword){
            ChangePasswordRequest changePasswordRequest = new ChangePasswordRequest();

            if(getArguments()!=null&&getArguments().containsKey(Constants.PHONE)) {
                changePasswordRequest.setPhone(getArguments().getString(Constants.PHONE));
            }

            changePasswordViewModel = new ChangePasswordViewModel(changePasswordRequest);

        }else {
            changePasswordViewModel = new ChangePasswordViewModel(null);
        }



        fragmentChangePasswordBinding.setChangePasswordViewModel(changePasswordViewModel);
    }


    private void setEvent() {
        changePasswordViewModel.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                String action = (String) o;
                handleActions(action, changePasswordViewModel.getLoginRepository().getMessage());
                Timber.e(action);
                if (action.equals(Constants.SUCCESS)) {
                    toastMessage(changePasswordViewModel.getLoginRepository().getMessage(),R.drawable.ic_check_white,R.color.colorPrimary);
                    ((ParentActivity)context).finish();
                }
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        changePasswordViewModel.reset();

    }

}
