package grand.app.moonshop.views.fragments.institution;


import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moonshop.R;
import grand.app.moonshop.base.BaseFragment;
import grand.app.moonshop.databinding.FragmentHomeInstitutionBinding;
import grand.app.moonshop.models.app.TabModel;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.utils.storage.user.UserHelper;
import grand.app.moonshop.utils.tabLayout.SwapAdapter;
import grand.app.moonshop.viewmodels.institution.InstitutionViewModel;
import grand.app.moonshop.views.fragments.offers.OfferFragment;
import timber.log.Timber;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class HomeInstitutionFragment extends BaseFragment {


    private FragmentHomeInstitutionBinding fragmentHomeInstitutionBinding;
    public InstitutionViewModel institutionViewModel;
    public ArrayList<TabModel> tabModels = new ArrayList<>();
    SwapAdapter adapter = null;

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        fragmentHomeInstitutionBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_home_institution, container, false);
        bind();
        setEvent();
        return fragmentHomeInstitutionBinding.getRoot();
    }

    private void bind() {
        institutionViewModel = new InstitutionViewModel();
        fragmentHomeInstitutionBinding.setInstitutionViewModel(institutionViewModel);
    }

    public void setTabs() {
        tabModels.clear();
        fragmentHomeInstitutionBinding.viewpager.setAdapter(null);
        Bundle bundle = new Bundle();
        bundle.putSerializable(Constants.INSTITUTIONS, institutionViewModel.getInstitutionViewModel().getShopDetailsResponse().shopData);
        Timber.e("branches_size:"+ institutionViewModel.getInstitutionViewModel().getShopDetailsResponse().shopData.branches.size());
        InstitutionAdsFragment institutionAdsFragment = new InstitutionAdsFragment();
        bundle.putString(Constants.TYPE, Constants.ADDS);
        bundle.putBoolean(Constants.SHOW_DELETE,false);
        institutionAdsFragment.setArguments(bundle);
        tabModels.add(new TabModel(getString(R.string.adds), institutionAdsFragment));

        OfferFragment offerFragment = new OfferFragment();
        bundle.putString(Constants.TYPE, Constants.GALLERY);
        offerFragment.setArguments(bundle);
        tabModels.add(new TabModel(getString(R.string.album), offerFragment));

        if (UserHelper.getUserDetails().type.equals(Constants.TYPE_INSTITUTIONS)) {
            fragmentHomeInstitutionBinding.slidingTabs.setTabMode(TabLayout.MODE_FIXED);
            bundle = new Bundle();
            InstitutionDetailsFragment institutionDetailsFragment = new InstitutionDetailsFragment();
            bundle.putSerializable(Constants.SHOP_DETAILS,institutionViewModel.getInstitutionViewModel().getShopDetailsResponse().shopData);
            institutionDetailsFragment.setArguments(bundle);
            tabModels.add(0, new TabModel(getString(R.string.details), institutionDetailsFragment));

            InstitutionBranchesListFragment institutionBranchesListFragment = new InstitutionBranchesListFragment();
            institutionBranchesListFragment.setArguments(bundle);
            tabModels.add(1, new TabModel(getString(R.string.branches), institutionBranchesListFragment));
        }

        adapter = new SwapAdapter(getChildFragmentManager(), tabModels);
        fragmentHomeInstitutionBinding.viewpager.setAdapter(adapter);
        fragmentHomeInstitutionBinding.viewpager.setOffscreenPageLimit(0);
        fragmentHomeInstitutionBinding.slidingTabs.setupWithViewPager(fragmentHomeInstitutionBinding.viewpager);
    }

    private void setEvent() {
        institutionViewModel.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                String action = (String) o;
                assert action != null;
                handleActions(action, institutionViewModel.getInstitutionViewModel().getMessage());
                if (action.equals(Constants.SUCCESS)) {
                    Timber.e("SUCCESS TABS ");
                    setTabs();
                    institutionViewModel.showPage(true);
                }
            }
        });
    }



    @Override
    public void onDestroy() {
        super.onDestroy();
        institutionViewModel.reset();
    }

}
