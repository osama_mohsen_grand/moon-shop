
package grand.app.moonshop.views.fragments.ads;

import java.util.ArrayList;
import java.util.List;

import grand.app.moonshop.R;
import grand.app.moonshop.base.ParentViewModel;
import grand.app.moonshop.models.ads.AdsDetailsModel;
import grand.app.moonshop.models.ads.AdsDetailsResponse;
import grand.app.moonshop.models.base.IdNameImage;
import grand.app.moonshop.repository.AdsRepository;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.utils.maputils.base.MapConfig;
import grand.app.moonshop.utils.resources.ResourceManager;
import grand.app.moonshop.utils.storage.user.UserHelper;
import grand.app.moonshop.viewmodels.ads.AddAdsCompanyRequest;
import grand.app.moonshop.vollyutils.MyApplication;
import grand.app.moonshop.vollyutils.VolleyFileObject;
import timber.log.Timber;

/**
 * Created by Gregory Rasmussen on 7/26/17.
 */
public class AddAdvertisementCompanyViewModel extends ParentViewModel {
    public int category_delete_position = -1;
    public int position = -1;
    public int new_image_position = -1;

    private AdsRepository adsRepository;
    public AddAdsCompanyRequest addAdsRequest;
    public boolean isEdit = false;
    public List<VolleyFileObject> volleyFileObjects = new ArrayList<>();
    public AddAdvertisementCompanyViewModel(int id,int category_id) {
        adsRepository = new AdsRepository(mMutableLiveData);
        addAdsRequest = new AddAdsCompanyRequest();
        addAdsRequest.id = id;
        addAdsRequest.category_id = category_id;
        if(id != -1) {
            showPage(false);
            isEdit = true;
            adsRepository.getAdsDetails(id, Integer.parseInt(UserHelper.getUserDetails().type));
        }else
            showPage(true);
    }


    int counter = 0;
    public void submit() {
        if(isEdit){
            Timber.e("resultSize:"+getAdsRepository().getAdsDetailsResponse().data.productImages.size());
            if (addAdsRequest.isValid()) {
                List<IdNameImage> idNameImages = getAdsRepository().getAdsDetailsResponse().data.productImages;
                volleyFileObjects.clear();
                for(int i=0; i<idNameImages.size();i++){
                    if(idNameImages.get(i).volleyFileObject != null){
                        VolleyFileObject volleyFileObject = idNameImages.get(i).volleyFileObject;
                        volleyFileObject.setParamName("product_images["+counter+"]");
                        volleyFileObjects.add(volleyFileObject);
                        counter++;
                    }
                }
                adsRepository.updateAds(addAdsRequest, volleyFileObjects);
            }
        }else {
            if (addAdsRequest.isValid() && volleyFileObjects.size() > 0) {
                adsRepository.addAds(addAdsRequest, volleyFileObjects);
            } else if (addAdsRequest.isValid() && volleyFileObjects.size() == 0) {
                baseError = ResourceManager.getString(R.string.please_upload_advertisement_images);
                mMutableLiveData.setValue(Constants.ERROR);
            }
        }
    }


    public void selectImages(){
        mMutableLiveData.setValue(Constants.IMAGE);
    }

    public void addressSubmit(){
        mMutableLiveData.setValue(Constants.LOCATION);
    }


    public void setAddress(double lat, double lng) {
        addAdsRequest.setLat(lat);
        addAdsRequest.setLng(lng);
        addAdsRequest.setAddress(new MapConfig(MyApplication.getInstance(),null).getAddress(lat,lng));
//        addAdsRequest.setAddress(ResourceManager.getString(R.string.done_selected_address));
        notifyChange();
    }


    public AdsRepository getAdsRepository() {
        return adsRepository;
    }

    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
    }


    public void updateView(AdsDetailsModel data) {
        addAdsRequest.setName(data.name);
        addAdsRequest.setPhone(data.phone);
        addAdsRequest.setAddress(data.address);
        addAdsRequest.setLat(data.lat);
        addAdsRequest.setLng(data.lng);
        addAdsRequest.setEmail(data.email);
        addAdsRequest.setDescription(data.description);
        addAdsRequest.setPrice(data.price);
        notifyChange();
    }

    public void delete(int id) {
        adsRepository.deleteImage(id);
    }

}
