package grand.app.moonshop.views.fragments.institution;


import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moonshop.R;
import grand.app.moonshop.adapter.BranchAdapter;
import grand.app.moonshop.base.BaseFragment;
import grand.app.moonshop.customviews.dialog.DialogConfirm;
import grand.app.moonshop.databinding.FragmentInstitutionBranchesListBinding;
import grand.app.moonshop.models.app.Mutable;
import grand.app.moonshop.models.shop.Branch;
import grand.app.moonshop.models.shop.ShopData;
import grand.app.moonshop.utils.helper.AppUtils;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.utils.dialog.DialogHelperInterface;
import grand.app.moonshop.utils.resources.ResourceManager;
import grand.app.moonshop.viewmodels.branch.BranchesViewModel;
import grand.app.moonshop.views.activities.BaseActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class InstitutionBranchesListFragment extends BaseFragment {


    private FragmentInstitutionBranchesListBinding fragmentInstitutionBranchesListBinding;
    private BranchesViewModel branchesViewModel;
    private BranchAdapter branchAdapter;
    private ShopData shopData;


    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        fragmentInstitutionBranchesListBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_institution_branches_list, container, false);
        restorePage();
        bind();
        getData();
        setEvent();
        return fragmentInstitutionBranchesListBinding.getRoot();
    }

    private void setEvent() {
        branchesViewModel.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                String action = (String) o;
                handleActions(action, branchesViewModel.getInstitutionRepository().getMessage());
                assert action != null;
                if(action.equals(Constants.ADD_BRANCH)) {
                    Intent intent = new Intent(context,BaseActivity.class);
                    intent.putExtra(Constants.PAGE,Constants.ADD_BRANCH);
                    Bundle bundle = new Bundle();
                    bundle.putSerializable(Constants.SHOP_DETAILS,shopData);
                    intent.putExtra(Constants.BUNDLE,bundle);
                    startActivityForResult(intent,Constants.RELOAD_RESULT);
                }if (action.equals(Constants.DELETE)) {
                    toastMessage(branchesViewModel.getInstitutionRepository().getMessage(), R.drawable.ic_check_white, R.color.colorPrimary);
                    branchAdapter.remove(branchesViewModel.category_delete_position);
                }else if(action.equals(Constants.SUCCESS)){
                    setAdapter(branchesViewModel.institutionRepository.getShopDetailsResponse().shopData);
                }
            }
        });
    }

    private void getData() {

        if (getArguments() != null && getArguments().containsKey(Constants.SHOP_DETAILS)) {
            setAdapter((ShopData) getArguments().getSerializable(Constants.SHOP_DETAILS));
        }else{
            branchesViewModel.getBranches();
        }
    }

    public void setAdapter(ShopData shopData){
        this.shopData = shopData;
        if (shopData != null && shopData.branches != null) {
            branchAdapter = new BranchAdapter(shopData.branches, true);
            fragmentInstitutionBranchesListBinding.rvBranches.setAdapter(branchAdapter);
            setEventAdapter();
        }
    }

    private void bind() {
        branchesViewModel = new BranchesViewModel();
        AppUtils.initVerticalRV(fragmentInstitutionBranchesListBinding.rvBranches, fragmentInstitutionBranchesListBinding.rvBranches.getContext(), 1);
        fragmentInstitutionBranchesListBinding.setBranchesViewModel(branchesViewModel);
    }

    private void setEventAdapter() {
        branchAdapter.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                Mutable mutable = (Mutable) o;
                if (mutable != null) {
                    Branch branch = shopData.branches.get(mutable.position);
                    if (mutable.type.equals(Constants.SUBMIT)) {
//                        //                Intent intent = new Intent(context, MapAddressActivity.class);
////                intent.putExtra(Constants.LAT, shopData.branches.get(pos).lat);
////                intent.putExtra(Constants.LNG, shopData.branches.get(pos).lng);
////                startActivity(intent);
//                        Log.e("lat",).lat+"");
//                        Log.e("lng",shopData.branches.get(pos).lng+"");
                    } else if (mutable.type.equals(Constants.DELETE)) {
                        new DialogConfirm(getActivity())
                                .setTitle(ResourceManager.getString(R.string.remove_product))
                                .setMessage(ResourceManager.getString(R.string.do_you_want_delete_image))
                                .setActionText(ResourceManager.getString(R.string.remove_item))
                                .setActionCancel(ResourceManager.getString(R.string.cancel))
                                .show(new DialogHelperInterface() {
                                    @Override
                                    public void OnClickListenerContinue(Dialog dialog, View view) {
                                        branchesViewModel.category_delete_position = mutable.position;
                                        branchesViewModel.delete(branch.id);
                                    }
                                });
                    }
                }

            }
        });
    }

    private static final String TAG = "InstitutionBranchesList";

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        Log.d(TAG,"onActivityResult");
        if (requestCode == Constants.RELOAD_RESULT) {
            Log.d(TAG,"onActivityResult equal");
//            HomeInstitutionFragment frag = ((HomeInstitutionFragment) this.getParentFragment());
//            frag.institutionViewModel.getInstitutionViewModel().getService();
            if(branchesViewModel.institutionRepository.getShopDetailsResponse() != null && branchesViewModel.institutionRepository.getShopDetailsResponse().shopData != null)
                branchesViewModel.getBranches();
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onResume() {
        super.onResume();
//        if(isReloadPage()) {
//            HomeInstitutionFragment frag = ((HomeInstitutionFragment) this.getParentFragment());
//            frag.institutionViewModel.getInstitutionViewModel().getService();
//        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (branchesViewModel != null)
            branchesViewModel.reset();
    }
}
