package grand.app.moonshop.views.fragments.famous;


import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moonshop.R;
import grand.app.moonshop.adapter.AlbumAdapter;
import grand.app.moonshop.base.BaseFragment;
import grand.app.moonshop.databinding.FragmentAlbumSearchBinding;
import grand.app.moonshop.models.app.Mutable;
import grand.app.moonshop.utils.helper.AppUtils;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.utils.storage.user.UserHelper;
import grand.app.moonshop.viewmodels.search.AlbumSearchViewModel;
import grand.app.moonshop.views.activities.BaseActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class AlbumSearchFragment extends BaseFragment {

    private FragmentAlbumSearchBinding fragmentAlbumSearchBinding;
    public AlbumSearchViewModel albumSearchViewModel;
    AlbumAdapter albumAdapter;

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        UserHelper.saveKey(Constants.RELOAD,Constants.FALSE);
        fragmentAlbumSearchBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_album_search, container, false);
        bind();
        setEvent();
        return fragmentAlbumSearchBinding.getRoot();
    }

    private void bind() {
        albumSearchViewModel = new AlbumSearchViewModel();
        AppUtils.initVerticalRV(fragmentAlbumSearchBinding.rvAlbum, fragmentAlbumSearchBinding.rvAlbum.getContext(), 3);
        fragmentAlbumSearchBinding.setAlbumSearchViewModel(albumSearchViewModel);
    }

    @Override
    public void onResume() {
        super.onResume();
        if(UserHelper.retrieveKey(Constants.RELOAD).equals(Constants.TRUE) && albumSearchViewModel != null) {
            albumSearchViewModel.submitSearch();
            UserHelper.saveKey(Constants.RELOAD,Constants.TRUE);
        }
    }



    private void setEvent() {
        albumSearchViewModel.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                String action = (String) o;
                handleActions(action, albumSearchViewModel.getSearchRepository().getMessage());
                assert action != null;
                Intent intent = new Intent(context, BaseActivity.class);
                intent.putExtra(Constants.PAGE,Constants.ADD_ALBUM);
                Bundle bundle = new Bundle();

                if (action.equals(Constants.SUCCESS)) {
                    if(albumSearchViewModel.getSearchRepository().getAlbumSearchResponse().data.size() > 0) {
                        albumSearchViewModel.showPage(true);
                        albumSearchViewModel.noData.set(false);
                        albumAdapter = new AlbumAdapter(albumSearchViewModel.getSearchRepository().getAlbumSearchResponse().data,false);
                        fragmentAlbumSearchBinding.rvAlbum.setAdapter(albumAdapter);
                        setEventAdapter();
                    }else{
                        albumSearchViewModel.noData.set(true);
                    }
                }
            }
        });
    }


    private void setEventAdapter() {
        albumAdapter.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                submitAdapter(o,Constants.IMAGE);
            }
        });
    }

    public void submitAdapter(Object o,String type){
        Mutable mutable = (Mutable) o;
        if(mutable.type.equals(Constants.SUBMIT)) {
            Intent intent = new Intent(context, BaseActivity.class);
            intent.putExtra(Constants.PAGE, Constants.ALBUM_DETAILS);
            intent.putExtra(Constants.NAME_BAR, albumSearchViewModel.getSearchRepository().getAlbumSearchResponse().data.get(mutable.position).name);
            Bundle bundle = new Bundle();
            bundle.putInt(Constants.ID,albumSearchViewModel.getSearchRepository().getAlbumSearchResponse().data.get(mutable.position).id);
            bundle.putString(Constants.TYPE,type);
            bundle.putString(Constants.TAB, "1");
            intent.putExtra(Constants.BUNDLE, bundle);
            startActivityForResult(intent,Constants.RELOAD_RESULT);
        }
    }

    @Override
    public void onDestroy() {
        albumSearchViewModel.reset();
        super.onDestroy();
    }

}
