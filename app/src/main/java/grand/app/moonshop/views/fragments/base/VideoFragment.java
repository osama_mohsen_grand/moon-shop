package grand.app.moonshop.views.fragments.base;


import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.MediaController;

import com.bumptech.glide.Glide;
import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.LoadControl;
import com.google.android.exoplayer2.PlaybackPreparer;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.ProgressiveMediaSource;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultAllocator;
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory;
import com.google.android.exoplayer2.util.Util;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import grand.app.moonshop.R;
import grand.app.moonshop.base.BaseFragment;
import grand.app.moonshop.databinding.FragmentVideoBinding;
import grand.app.moonshop.databinding.FragmentZoomBinding;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.viewmodels.app.ZoomViewModel;
import grand.app.moonshop.views.activities.MainActivity;
import timber.log.Timber;

import static com.google.android.exoplayer2.Player.STATE_BUFFERING;
import static com.google.android.exoplayer2.Player.STATE_READY;

/**
 * A simple {@link Fragment} subclass.
 */
public class VideoFragment extends BaseFragment {


    //Minimum Video you want to buffer while Playing
    private int MIN_BUFFER_DURATION = 2000;
    //Max Video you want to buffer during PlayBack
    private int MAX_BUFFER_DURATION = 5000;
    //Min Video you want to buffer before start Playing it
    private int MIN_PLAYBACK_START_BUFFER = 1500;
    //Min video You want to buffer when user resumes video
    private int MIN_PLAYBACK_RESUME_BUFFER = 2000;

    FragmentVideoBinding fragmentVideoBinding;
    public String video = "";
    SimpleExoPlayer player;
    AsyncTask mMyTask;

    private static final String TAG = "VideoFragment";
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        fragmentVideoBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_video, container, false);
        getData();
//        mMyTask = new DownloadTask().execute(stringToURL());

        Glide
                .with(requireContext())
                .load(video)
                .centerCrop()
                .into(fragmentVideoBinding.image);


        LoadControl loadControl = new DefaultLoadControl.Builder()
                .setAllocator(new DefaultAllocator(true, 16))
                .setBufferDurationsMs(MIN_BUFFER_DURATION,
                        MAX_BUFFER_DURATION,
                        MIN_PLAYBACK_START_BUFFER,
                        MIN_PLAYBACK_RESUME_BUFFER)
                .setTargetBufferBytes(-1)
                .setPrioritizeTimeOverSizeThresholds(true).createDefaultLoadControl();


        TrackSelector trackSelector = new DefaultTrackSelector();


        player = ExoPlayerFactory.newSimpleInstance(getActivityBase(),trackSelector, loadControl);
        MediaSource mediaSource = new ProgressiveMediaSource.Factory(new DefaultHttpDataSourceFactory("exoplayer-codelab"))
                .createMediaSource(Uri.parse(video));



        fragmentVideoBinding.playerView.setPlayer(player);


        player.setPlayWhenReady(true);
        player.prepare(mediaSource, true, false);


//        Log.d(TAG, "onCreateView: ");
//
//        player.addListener(new ExoPlayer.EventListener() {
//            @Override
//            public void onLoadingChanged(boolean isLoading) {
//                Log.d(TAG, "onLoadingChanged: ");
//            }
//
//            @Override
//            public void onPlayerStateChanged(boolean playWhenReady, int state) {
//                if(state == STATE_BUFFERING) {
//                    fragmentVideoBinding.image.setVisibility(View.GONE);
//                }else if(state == STATE_READY){
//                    fragmentVideoBinding.progress.setVisibility(View.GONE);
//                    fragmentVideoBinding.playerView.setVisibility(View.VISIBLE);
//                }
//            }
//        });

        return fragmentVideoBinding.getRoot();
    }



    private void getData() {
        if (getArguments() != null && getArguments().containsKey(Constants.VIDEO)) {
            video = getArguments().getString(Constants.VIDEO);
        }
    }


//    private class DownloadTask extends AsyncTask<URL,Void,Bitmap>{
//        protected void onPreExecute(){
//            fragmentVideoBinding.progress.setVisibility(View.VISIBLE);
//        }
//        protected Bitmap doInBackground(URL...urls){
//            URL url = urls[0];
//            HttpURLConnection connection = null;
//            try{
//                connection = (HttpURLConnection) url.openConnection();
//                connection.connect();
//                InputStream inputStream = connection.getInputStream();
//                BufferedInputStream bufferedInputStream = new BufferedInputStream(inputStream);
//                return BitmapFactory.decodeStream(bufferedInputStream);
//            }catch(IOException e){
//                e.printStackTrace();
//            }
//            return null;
//        }
//        // When all async task done
//        protected void onPostExecute(Bitmap result){
//            // Hide the progress dialog
//            fragmentVideoBinding.progress.setVisibility(View.GONE);
//            if(result!=null){
//                fragmentVideoBinding.image.setImageBitmap(result);
//            } else {
//                // Notify user that an error occurred while downloading image
//            }
//        }
//    }
//    protected URL stringToURL() {
//        try {
//            URL url = new URL(video);
//            return url;
//        } catch (MalformedURLException e) {
//            e.printStackTrace();
//        }
//        return null;
//    }


    @Override
    public void onPause() {
        if (player != null) {
            player.stop();
            player.release();
            player.setPlayWhenReady(false);
        }
        player = null;
        super.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}
