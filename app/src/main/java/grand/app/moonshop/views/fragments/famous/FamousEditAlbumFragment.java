package grand.app.moonshop.views.fragments.famous;


import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.theartofdev.edmodo.cropper.CropImage;
import com.yalantis.ucrop.UCrop;

import java.io.File;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moonshop.R;
import grand.app.moonshop.base.BaseFragment;
import grand.app.moonshop.databinding.FragmentAddCategoryBinding;
import grand.app.moonshop.databinding.FragmentFamousEditAlbumBinding;
import grand.app.moonshop.models.famous.album.FamousEditAlbumViewModel;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.utils.storage.user.UserHelper;
import grand.app.moonshop.utils.upload.FileOperations;
import grand.app.moonshop.viewmodels.category.AddCategoryViewModel;
import grand.app.moonshop.views.fragments.chat.ChatFragment;
import grand.app.moonshop.vollyutils.VolleyFileObject;
import timber.log.Timber;

import static android.app.Activity.RESULT_OK;
import static com.facebook.FacebookSdk.getCacheDir;

/**
 * A simple {@link Fragment} subclass.
 */
public class FamousEditAlbumFragment extends BaseFragment {

    View rootView;
    private FragmentFamousEditAlbumBinding fragmentFamousEditAlbumBinding;
    private FamousEditAlbumViewModel famousEditAlbumViewModel;
    int id = -1;
    String name = "", tab = "", image = "";


    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        Log.d(TAG,"onCreateView");
        fragmentFamousEditAlbumBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_famous_edit_album, container, false);
        getData();
        bind();
        setEvent();
        return rootView;
    }

    private void getData() {
        if (getArguments() != null && getArguments().containsKey(Constants.ID))
            id = getArguments().getInt(Constants.ID);
        if (getArguments() != null && getArguments().containsKey(Constants.NAME))
            name = getArguments().getString(Constants.NAME);
        if (getArguments() != null && getArguments().containsKey(Constants.IMAGE))
            image = getArguments().getString(Constants.IMAGE);
        if (getArguments() != null && getArguments().containsKey(Constants.TAB))
            tab = getArguments().getString(Constants.TAB);
    }

    private static final String TAG = "FamousEditAlbumFragment";

    private void bind() {
        Log.d(TAG,image);
        famousEditAlbumViewModel = new FamousEditAlbumViewModel(id, name, tab, image);
        fragmentFamousEditAlbumBinding.setFamousEditAlbumViewModel(famousEditAlbumViewModel);
        rootView = fragmentFamousEditAlbumBinding.getRoot();
    }

    private void setEvent() {
        famousEditAlbumViewModel.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                String action = (String) o;
                handleActions(action, famousEditAlbumViewModel.getFamousRepository().getMessage());
                assert action != null;
                if (action.equals(Constants.SUCCESS)) {
                    toastMessage(famousEditAlbumViewModel.getFamousRepository().getMessage(), R.drawable.ic_check_white, R.color.colorPrimary);
                    UserHelper.saveKey(Constants.RELOAD, Constants.TRUE);
                } else if (action.equals(Constants.SELECT_IMAGE)) {
                    pickImageDialogSelect();
                }
            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        //super method removed
        Timber.e("onActivityResult:" + requestCode);
        if (requestCode == Constants.FILE_TYPE_IMAGE) {
            VolleyFileObject volleyFileObject1 = FileOperations.getVolleyFileObject(getActivity(), data, Constants.URL, Constants.FILE_TYPE_IMAGE);
            File file = new File(getCacheDir(), Constants.URL + ".png");
            UCrop.of(Uri.fromFile(volleyFileObject1.getFile()), Uri.fromFile(file))
                    .start(context, FamousEditAlbumFragment.this);

        }else  if (requestCode == UCrop.REQUEST_CROP && data != null) {
            final Uri resultUri = UCrop.getOutput(data);
            if (resultUri != null) {
                VolleyFileObject volleyFileObject = new VolleyFileObject(Constants.URL, resultUri.getPath(), Constants.FILE_TYPE_IMAGE);
                famousEditAlbumViewModel.setImage(volleyFileObject);
                fragmentFamousEditAlbumBinding.imgEditAlbum.setImageURI(resultUri);
            }
        }
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE && data != null) {

            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();
                UCrop.of(resultUri, resultUri).start(context, FamousEditAlbumFragment.this);
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Toast.makeText(context, "" + getString(R.string.error_failed_pick_gallery_image), Toast.LENGTH_SHORT).show();
            }
        }

        super.onActivityResult(requestCode, resultCode, data);
    }
}
