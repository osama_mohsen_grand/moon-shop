package grand.app.moonshop.views.fragments.ads;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.databinding.DataBindingUtil;
import grand.app.moonshop.R;
import grand.app.moonshop.adapter.CompanyAdsCategoriesAdapter;
import grand.app.moonshop.base.BaseFragment;
import grand.app.moonshop.databinding.FragmentCompanyAdsCategoriesBinding;
import grand.app.moonshop.models.album.AlbumModelCategories;
import grand.app.moonshop.utils.helper.AppUtils;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.utils.storage.user.UserHelper;

public class FamousCompanyAdsCategoriesFragment extends BaseFragment {

    private FragmentCompanyAdsCategoriesBinding binding;
    private CompanyAdsCategoriesViewModel viewModel;
    private CompanyAdsCategoriesAdapter adapter;
    AlbumModelCategories albumModel = null;

    private static final String TAG = "FamousCompanyAdsCategor";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        UserHelper.saveKey(Constants.RELOAD, Constants.FALSE);
        Log.d(TAG,"done");
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_company_ads_categories, container, false);
        getData();
        bind();
        return binding.getRoot();

    }

    private void getData() {
        viewModel = new CompanyAdsCategoriesViewModel();
        if (getArguments() != null && getArguments().containsKey(Constants.CATEGORIES)){
            Log.d(TAG,"has argument");
            albumModel = (AlbumModelCategories) getArguments().getSerializable(Constants.CATEGORIES);
            adapter = new CompanyAdsCategoriesAdapter(albumModel.categories);
            adapter.famous_id = getArguments().getInt(Constants.FAMOUS_ID);
        }
    }


    private void bind() {
        AppUtils.initVerticalRV(binding.rvCompanyAdsCategories, binding.rvCompanyAdsCategories.getContext(), 3);
        binding.rvCompanyAdsCategories.setAdapter(adapter);
        binding.setViewModel(viewModel);
    }

}
