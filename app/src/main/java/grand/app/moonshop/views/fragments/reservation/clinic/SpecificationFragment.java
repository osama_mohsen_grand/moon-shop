package grand.app.moonshop.views.fragments.reservation.clinic;


import android.os.Bundle;

import androidx.fragment.app.Fragment;
import grand.app.moonshop.R;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


/**
 * A simple {@link Fragment} subclass.
 */
public class SpecificationFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_specification, container, false);
    }

}
