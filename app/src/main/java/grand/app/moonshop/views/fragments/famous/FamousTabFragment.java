package grand.app.moonshop.views.fragments.famous;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import grand.app.moonshop.R;
import grand.app.moonshop.adapter.FamousDetailsCategoryAdapter;
import grand.app.moonshop.adapter.FamousDetailsImageAdapter;
import grand.app.moonshop.base.BaseFragment;
import grand.app.moonshop.databinding.FragmentFamousTabBinding;
import grand.app.moonshop.utils.helper.AppUtils;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.viewmodels.famous.FamousTabViewModel;


public class FamousTabFragment extends BaseFragment {
    private FragmentFamousTabBinding fragmentFamousTabBinding;
    private FamousTabViewModel famousTabViewModel;

    public int id = -1,type = -1;
    FamousDetailsCategoryAdapter famousDetailsCategoryAdapter;
    FamousDetailsImageAdapter famousDetailsImageAdapter;
    FamousDetailsMainFragment famousDetailsFragment;
    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        fragmentFamousTabBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_famous_tab, container, false);
        getData();
        bind();
        return fragmentFamousTabBinding.getRoot();
    }

    private void getData() {
        if(getArguments() != null && getArguments().containsKey(Constants.ID))
            id =  getArguments().getInt(Constants.ID);
        if(getArguments() != null && getArguments().containsKey(Constants.TYPE))
            type =  getArguments().getInt(Constants.TYPE);
    }

    private void bind() {
        famousTabViewModel = new FamousTabViewModel(id,type);
        AppUtils.initHorizontalRV(fragmentFamousTabBinding.rvFamousTabCategory, fragmentFamousTabBinding.rvFamousTabCategory.getContext(), 1);
        AppUtils.initVerticalRV(fragmentFamousTabBinding.rvFamousTabImages, fragmentFamousTabBinding.rvFamousTabImages.getContext(), 2);
        fragmentFamousTabBinding.setFamousTabViewModel(famousTabViewModel);
    }
}
