package grand.app.moonshop.views.fragments.famous;


import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moonshop.R;
import grand.app.moonshop.adapter.AlbumAdapter;
import grand.app.moonshop.base.BaseFragment;
import grand.app.moonshop.databinding.FragmentFamousAlbumsUserDetailsBinding;
import grand.app.moonshop.models.app.Mutable;
import grand.app.moonshop.models.famous.home.ImageVideo;
import grand.app.moonshop.utils.helper.AppUtils;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.viewmodels.famous.details.FamousAlbumsUserDetailsViewModel;
import grand.app.moonshop.views.activities.BaseActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class FamousAlbumsUserDetails extends BaseFragment {


    private FragmentFamousAlbumsUserDetailsBinding fragmentFamousAlbumUserDetailsBinding;
    private FamousAlbumsUserDetailsViewModel famousAlbumsUserDetailsViewModel;
    public int id = -1;
    public String type = "";
    int tab = -1;
    private AlbumAdapter albumAdapter;

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentFamousAlbumUserDetailsBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_famous_albums_user_details, container, false);
        getData();
        bind();
        setEvent();
        return fragmentFamousAlbumUserDetailsBinding.getRoot();
    }

    private void getData() {
        if (getArguments() != null && getArguments().containsKey(Constants.ID))
            id = getArguments().getInt(Constants.ID);
        if (getArguments() != null && getArguments().containsKey(Constants.TAB))
            tab = getArguments().getInt(Constants.TAB);
    }

    private void bind() {
        famousAlbumsUserDetailsViewModel = new FamousAlbumsUserDetailsViewModel(id, tab);
        AppUtils.initVerticalRV(fragmentFamousAlbumUserDetailsBinding.rvFamousAlbumDetails, fragmentFamousAlbumUserDetailsBinding.rvFamousAlbumDetails.getContext(), 3);
        fragmentFamousAlbumUserDetailsBinding.setFamousAlbumUserDetailsViewModel(famousAlbumsUserDetailsViewModel);
    }


    List<ImageVideo> imageVideos = null;

    private void setEvent() {
        famousAlbumsUserDetailsViewModel.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                String action = (String) o;
                handleActions(action, famousAlbumsUserDetailsViewModel.getFamousRepository().getMessage());
                assert action != null;
                if (action.equals(Constants.SUCCESS)) {
                    imageVideos = famousAlbumsUserDetailsViewModel.getFamousRepository().getFamousDetailsResponse().data;
                    if (imageVideos != null && imageVideos.size() > 0) {
                        albumAdapter = new AlbumAdapter(imageVideos,false);
                        fragmentFamousAlbumUserDetailsBinding.rvFamousAlbumDetails.setAdapter(albumAdapter);
                        setEventAdapter();
                    } else {
                        famousAlbumsUserDetailsViewModel.noData();
                    }
                    setData();

                }
            }
        });
    }

    private void setData() {
        FamousDetailsFragment frag = ((FamousDetailsFragment) this.getParentFragment());
        frag.setData(famousAlbumsUserDetailsViewModel.getFamousRepository().getFamousDetailsResponse());

    }

    private static final String TAG = "FamousAlbumsUserDetails";
    private void setEventAdapter() {
        albumAdapter.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                Mutable mutable = (Mutable) o;
                Intent intent = new Intent(context, BaseActivity.class);
                Bundle bundle = new Bundle();
                intent.putExtra(Constants.NAME_BAR, imageVideos.get(mutable.position).name);
//                if(AppMoon.getUserType().equals(Constants.TYPE_FAMOUS_PEOPLE) || AppMoon.getUserType().equals(Constants.TYPE_PHOTOGRAPHER) ) {
                intent.putExtra(Constants.PAGE, FamousAlbumMainDetailsFragment.class.getName());
                bundle.putInt(Constants.ID, imageVideos.get(mutable.position).id);
                bundle.putString(Constants.TYPE, type);
                bundle.putString(Constants.TAB, "1");
//                }else{
//                    intent.putExtra(Constants.PAGE, FamousAlbumMainDetailsFragment.class.getName());
//                    bundle.putInt(Constants.ID, imageVideos.get(mutable.position).id);
//                    bundle.putString(Constants.TYPE, type);
//                    bundle.putString(Constants.TAB, "1");
//                }
                intent.putExtra(Constants.BUNDLE, bundle);
                startActivity(intent);
            }
        });
    }
}
