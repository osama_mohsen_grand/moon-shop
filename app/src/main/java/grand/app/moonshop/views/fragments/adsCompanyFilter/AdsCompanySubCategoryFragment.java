package grand.app.moonshop.views.fragments.adsCompanyFilter;


import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moonshop.R;
import grand.app.moonshop.adapter.AdsCompanyFilterAdapter;
import grand.app.moonshop.base.BaseFragment;
import grand.app.moonshop.base.ParentActivity;
import grand.app.moonshop.databinding.FragmentAdsSubCompanyFilterBinding;
import grand.app.moonshop.models.adsCompanyFilter.AdsCompanyFilter;
import grand.app.moonshop.models.adsCompanyFilter.AdsCompanyFilterResponse;
import grand.app.moonshop.utils.helper.AppUtils;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.viewmodels.adsCompanyFilter.AdsCompanyFilterViewModel;
import timber.log.Timber;

/**
 * A simple {@link Fragment} subclass.
 */
public class AdsCompanySubCategoryFragment extends BaseFragment {


    View rootView;
    private FragmentAdsSubCompanyFilterBinding fragmentAdsSubCompanyFilterBinding;
    private AdsCompanyFilterViewModel adsCompanyFilterViewModel;
    private AdsCompanyFilterAdapter adsCompanyFilterAdapter;
    AdsCompanyFilterResponse adsCompanyFilterResponse = null;
    int service_id = -1, category_id = -1, position = -1;
    public String from = "";
    public boolean multiple_choice = true;

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentAdsSubCompanyFilterBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_ads_sub_company_filter, container, false);
        getData();
        bind();
        rootView = fragmentAdsSubCompanyFilterBinding.getRoot();
        return rootView;
    }

    private void getData() {
        if (getArguments() != null && getArguments().containsKey(Constants.ID))
            service_id = getArguments().getInt(Constants.ID);
        if (getArguments() != null && getArguments().containsKey(Constants.FROM))
            from = getArguments().getString(Constants.FROM);
        if (getArguments() != null && getArguments().containsKey(Constants.CATEGORY_ID))
            category_id = getArguments().getInt(Constants.CATEGORY_ID);
        if (getArguments() != null && getArguments().containsKey(Constants.MULTIPLE_CHOICE))
            multiple_choice = getArguments().getBoolean(Constants.MULTIPLE_CHOICE);
        if (getArguments() != null && getArguments().containsKey(Constants.POSITION))
            position = getArguments().getInt(Constants.POSITION);
        if (getArguments() != null && getArguments().containsKey(Constants.ADS_COMPANY_FILTER))
            adsCompanyFilterResponse = (AdsCompanyFilterResponse) getArguments().getSerializable(Constants.ADS_COMPANY_FILTER);
    }

    private void bind() {
        AppUtils.initVerticalRV(fragmentAdsSubCompanyFilterBinding.rvFilter, fragmentAdsSubCompanyFilterBinding.rvFilter.getContext(), 1);
        adsCompanyFilterViewModel = new AdsCompanyFilterViewModel();
        setAdapter(adsCompanyFilterResponse.data.get(position).subCategory);
        setEvents();
        fragmentAdsSubCompanyFilterBinding.setAdsCompanyFilterViewModel(adsCompanyFilterViewModel);


    }

    private void setEvents() {
        adsCompanyFilterViewModel.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                String action = (String) o;
                handleActions(action, adsCompanyFilterViewModel.getAdvertisementCompanyRepository().getMessage());
                assert action != null;
                if (action.equals(Constants.SUBMIT)) {
                    Intent intent = new Intent();
                    intent.putExtra(Constants.CATEGORY_ID,category_id);
                    intent.putExtra(Constants.SUB_CATEGORY_ID,adsCompanyFilterAdapter.selected);
                    getActivityBase().setResult(Constants.FILTER_RESULT, intent);
                    getActivityBase().finish();
                }
            }
        });
    }

    public void setAdapter(List<AdsCompanyFilter> adsCompanyFilters) {
        adsCompanyFilterAdapter = new AdsCompanyFilterAdapter(adsCompanyFilters, multiple_choice);
        adsCompanyFilterViewModel.showPage(true);
        fragmentAdsSubCompanyFilterBinding.rvFilter.setAdapter(adsCompanyFilterAdapter);
        setEventAdapter();
    }

    private void setEventAdapter() {
        adsCompanyFilterAdapter.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                int pos = (int) o;
            }
        });
    }


    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        //super method removed
        if (requestCode == Constants.FILTER_RESULT && resultCode == Constants.FILTER_RESULT) {
            Timber.e("CallService");
            Timber.e("cat:" + data.getIntExtra(Constants.CATEGORY_ID, -1));
            Timber.e("sub_cat:" + data.getIntExtra(Constants.SUB_CATEGORY_ID, -1));

            Intent intent = new Intent();
            intent.putExtra(Constants.CATEGORY_ID, data.getIntExtra(Constants.CATEGORY_ID, -1));
            intent.putExtra(Constants.SUB_CATEGORY_ID, data.getIntArrayExtra(Constants.SUB_CATEGORY_ID));
            ((ParentActivity) context).setResult(Constants.FILTER_RESULT, intent);
            ((ParentActivity) context).finish();//finishing activity


//            companiesDetailsViewModel.category_id = data.getIntExtra(Constants.CATEGORY_ID,-1);
//            companiesDetailsViewModel.sub_category_id = data.getIntExtra(Constants.SUB_CATEGORY_ID,-1);
//            resetAdapter();
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        adsCompanyFilterViewModel.reset();
    }
}
