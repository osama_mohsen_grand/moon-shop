package grand.app.moonshop.views.fragments.reservation.clinic;


import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import grand.app.moonshop.R;
import grand.app.moonshop.adapter.ReservationAdapter;
import grand.app.moonshop.databinding.FragmentReservationMainBinding;
import grand.app.moonshop.models.app.TabModel;
import grand.app.moonshop.models.reservation.doctor.order.ReservationOrder;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.utils.tabLayout.SwapAdapter;
import grand.app.moonshop.utils.tabLayout.SwapStateAdapter;
import grand.app.moonshop.viewmodels.home.shop.HomeShopViewModel;
import timber.log.Timber;

/**
 * A simple {@link Fragment} subclass.
 */
public class ReservationMainFragment extends Fragment {

    private FragmentReservationMainBinding fragmentReservationMainBinding;
    private HomeShopViewModel homeShopViewModel;
    public ArrayList<TabModel> tabModels = new ArrayList<>();
    ReservationFragment reservationHistoryFragment = null;

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentReservationMainBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_reservation_main, container, false);
        bind();
        return fragmentReservationMainBinding.getRoot();
    }


    //if  you want to get order recent or new  send status 0 else if you want to get previous order  send status 1
    private void bind() {
        ReservationFragment reservationRecentFragment = new ReservationFragment();
        Bundle bundle = new Bundle();
        bundle.putString(Constants.STATUS,"0");
        reservationRecentFragment.setArguments(bundle);

        reservationHistoryFragment = new ReservationFragment();
        bundle = new Bundle();
        bundle.putString(Constants.STATUS,"1");
        reservationHistoryFragment.setArguments(bundle);

        tabModels.add(new TabModel(getString(R.string.recent_appointments),reservationRecentFragment));
        tabModels.add(new TabModel(getString(R.string.appointments_history),reservationHistoryFragment));

        SwapStateAdapter adapter = new SwapStateAdapter(getChildFragmentManager(),tabModels);
        fragmentReservationMainBinding.viewpager.setAdapter(adapter);
        fragmentReservationMainBinding.viewpager.setOffscreenPageLimit(tabModels.size());
        fragmentReservationMainBinding.slidingTabs.setupWithViewPager(fragmentReservationMainBinding.viewpager);
    }

    public void moveToHistory(ReservationOrder reservationOrder){
        Timber.e("welcome here");
        ArrayList<ReservationOrder> reservationOrders = new ArrayList<>();
        if(reservationHistoryFragment.reservationAdapter == null) {
            reservationHistoryFragment.reservationAdapter = new ReservationAdapter(reservationOrders, "1");
        }
        reservationOrders.add(reservationOrder);
        reservationHistoryFragment.reservationAdapter.update(reservationOrders);
    }

}
