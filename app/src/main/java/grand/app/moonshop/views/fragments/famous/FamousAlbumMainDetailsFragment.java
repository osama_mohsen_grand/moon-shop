package grand.app.moonshop.views.fragments.famous;


import android.app.Dialog;
import android.content.Intent;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.abedelazizshe.lightcompressorlibrary.CompressionListener;
import com.abedelazizshe.lightcompressorlibrary.VideoCompressor;
import com.abedelazizshe.lightcompressorlibrary.VideoQuality;
import com.theartofdev.edmodo.cropper.CropImage;
import com.yalantis.ucrop.UCrop;

import java.io.File;
import java.util.ArrayList;

import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moonshop.R;
import grand.app.moonshop.adapter.AlbumDetailsAdapter;
import grand.app.moonshop.base.BaseFragment;
import grand.app.moonshop.customviews.dialog.DialogConfirm;
import grand.app.moonshop.databinding.FragmentFamousAlbumMainDetailsBinding;
import grand.app.moonshop.models.ads.Datum;
import grand.app.moonshop.models.app.Mutable;
import grand.app.moonshop.models.base.IdNameImage;
import grand.app.moonshop.models.famous.album.FamousAlbumImagesResponse;
import grand.app.moonshop.utils.helper.AppUtils;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.utils.dialog.DialogHelperInterface;
import grand.app.moonshop.utils.resources.ResourceManager;
import grand.app.moonshop.utils.storage.user.UserHelper;
import grand.app.moonshop.utils.upload.FileOperations;
import grand.app.moonshop.viewmodels.famous.details.FamousAlbumMainDetailsViewModel;
import grand.app.moonshop.views.activities.BaseActivity;
import grand.app.moonshop.vollyutils.VolleyFileObject;
import timber.log.Timber;

import static android.app.Activity.RESULT_OK;
import static com.facebook.FacebookSdk.getCacheDir;

/**
 * A simple {@link Fragment} subclass.
 */
public class FamousAlbumMainDetailsFragment extends BaseFragment {
    private FragmentFamousAlbumMainDetailsBinding fragmentFamousAlbumDetailsBinding;
    private FamousAlbumMainDetailsViewModel famousAlbumMainDetailsViewModel;
    public int id = -1,shop_id=-1;
    public String type = "",tab = "", albumName = "";
    private AlbumDetailsAdapter albumDetailsAdapter;
    private ArrayList<IdNameImage> data = null;

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentFamousAlbumDetailsBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_famous_album_main_details, container, false);
        getData();
        bind();
        setEvent();
        return fragmentFamousAlbumDetailsBinding.getRoot();
    }

    private void getData() {
        if (getArguments() != null && getArguments().containsKey(Constants.ID))
            id = getArguments().getInt(Constants.ID);
        if (getArguments() != null && getArguments().containsKey(Constants.TYPE))
            type = getArguments().getString(Constants.TYPE);
        if (getArguments() != null && getArguments().containsKey(Constants.TAB))
            tab = getArguments().getString(Constants.TAB);
        if (getArguments() != null && getArguments().containsKey(Constants.NAME))
            albumName = getArguments().getString(Constants.NAME);
        if (getArguments() != null && getArguments().containsKey(Constants.SHOP_ID))
            shop_id = getArguments().getInt(Constants.SHOP_ID);

        if (getArguments() != null && getArguments().containsKey(Constants.ALBUM_ADS)) {
            Datum datum = (Datum) getArguments().getSerializable(Constants.ALBUM_ADS);
            data = new ArrayList<>(datum.ads);
        }
    }

    private void bind() {
        AppUtils.initVerticalRV(fragmentFamousAlbumDetailsBinding.rvFamousAlbumDetails, fragmentFamousAlbumDetailsBinding.rvFamousAlbumDetails.getContext(), 3);
        albumDetailsAdapter = new AlbumDetailsAdapter(new ArrayList<>());
        setEventAdapter();
        fragmentFamousAlbumDetailsBinding.rvFamousAlbumDetails.setAdapter(albumDetailsAdapter);
        if(data == null) {
            famousAlbumMainDetailsViewModel = new FamousAlbumMainDetailsViewModel(id, type, tab,albumName);
        } else {
            famousAlbumMainDetailsViewModel = new FamousAlbumMainDetailsViewModel();
            FamousAlbumImagesResponse famousAlbumImagesResponse = new FamousAlbumImagesResponse();
            famousAlbumImagesResponse.data = data;
            famousAlbumMainDetailsViewModel.getFamousRepository().setFamousAlbumImagesResponse(famousAlbumImagesResponse);
            albumDetailsAdapter.update(data);
        }

        Log.d(TAG,"allowAdd:become "+famousAlbumMainDetailsViewModel.allow_add);

        fragmentFamousAlbumDetailsBinding.setFamousAlbumMainDetailsViewModel(famousAlbumMainDetailsViewModel);
    }

    private static final String TAG = "FamousAlbumMainDetailsF";
    private void setEvent() {
        famousAlbumMainDetailsViewModel.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                String action = "";
                if (o instanceof String) {
                    action = (String) o;
                    handleActions(action, famousAlbumMainDetailsViewModel.getFamousRepository().getMessage());
                    assert action != null;
                } else if (o instanceof Mutable) {
                    if (((Mutable) o).type.equals(Constants.PROGRESS_PERCENTAGE)) {
                        int percent = ((Mutable) o).position;
                        handleStringValue(Constants.PROGRESS_PERCENTAGE, percent);
                    }
                }

//                handleActions(action, famousAlbumMainDetailsViewModel.getFamousRepository().getMessage());
                assert action != null;
                if (action.equals(Constants.SUCCESS)) {
                    Log.d(TAG, "onChanged: success");
                    famousAlbumMainDetailsViewModel.setData();
                    albumDetailsAdapter.update(famousAlbumMainDetailsViewModel.getFamousRepository().getFamousAlbumImagesResponse().data);
                } else if (action.equals(Constants.IMAGE)) {
                    pickImageDialogSelect();
                } else if (action.equals(Constants.VIDEO)) {
                    pickVideoDialogSelect();
                } else if (action.equals(Constants.DELETE)) {
                    UserHelper.saveKey(Constants.RELOAD, Constants.TRUE);
                    toastMessage(famousAlbumMainDetailsViewModel.getFamousRepository().getMessage(), R.drawable.ic_check_white, R.color.colorPrimary);
                    Timber.e("position:" + famousAlbumMainDetailsViewModel.category_delete_position);
                    albumDetailsAdapter.notifyDataSetChanged();
                } else if (action.equals(Constants.DONE)) {
                    UserHelper.saveKey(Constants.RELOAD, Constants.TRUE);
                    famousAlbumMainDetailsViewModel.getFamousRepository().getAlbumImages(id);
                }else if(action.equals(Constants.DELETED)){
                    toastMessage(famousAlbumMainDetailsViewModel.getFamousRepository().getMessage(),R.drawable.ic_check_white,R.color.colorPrimary);
                    albumDetailsAdapter.remove(famousAlbumMainDetailsViewModel.category_delete_position);
                }
            }
        });
    }


    private void setEventAdapter() {
        albumDetailsAdapter.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                Mutable mutable = (Mutable) o;
                if (mutable.type.equals(Constants.IMAGE)) {
                    Intent intent = new Intent(context, BaseActivity.class);
                    intent.putExtra(Constants.PAGE, Constants.ZOOM);
                    Bundle bundle = new Bundle();
                    bundle.putString(Constants.IMAGE, famousAlbumMainDetailsViewModel.getFamousRepository().getFamousAlbumImagesResponse().data.get(mutable.position).image);
                    intent.putExtra(Constants.NAME_BAR, Constants.IMAGE);
                    intent.putExtra(Constants.BUNDLE, bundle);
                    startActivityForResult(intent, Constants.RELOAD_RESULT);
                } else if (mutable.type.equals(Constants.VIDEO)) {
                    Intent intent = new Intent(context, BaseActivity.class);
                    intent.putExtra(Constants.PAGE, Constants.VIDEO);
                    intent.putExtra(Constants.NAME_BAR, Constants.VIDEO);
                    Bundle bundle = new Bundle();
                    Timber.e("video:" + famousAlbumMainDetailsViewModel.getFamousRepository().getFamousAlbumImagesResponse().data.get(mutable.position).image);
                    bundle.putString(Constants.VIDEO, famousAlbumMainDetailsViewModel.getFamousRepository().getFamousAlbumImagesResponse().data.get(mutable.position).image);
                    intent.putExtra(Constants.BUNDLE, bundle);
                    startActivity(intent);
                } else if (mutable.type.equals(Constants.DELETE)) {
                    new DialogConfirm(getActivity())
                            .setTitle(ResourceManager.getString(R.string.remove_product))
                            .setMessage(ResourceManager.getString(R.string.do_you_want_delete_image))
                            .setActionText(ResourceManager.getString(R.string.remove_item))
                            .setActionCancel(ResourceManager.getString(R.string.cancel))
                            .show(new DialogHelperInterface() {
                                @Override
                                public void OnClickListenerContinue(Dialog dialog, View view) {
                                    famousAlbumMainDetailsViewModel.category_delete_position = mutable.position;
                                    famousAlbumMainDetailsViewModel.delete(famousAlbumMainDetailsViewModel.getFamousRepository().getFamousAlbumImagesResponse().data.remove(mutable.position).id);
                                }
                            });
                }
            }
        });
    }


    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        //super method removed
        int duration = 0;
        Timber.e("onActivityResult:" + requestCode);
        Log.d(TAG,"fileTypeRequest:"+requestCode);

        VolleyFileObject volleyFileObject = null;
        if (requestCode == Constants.FILE_TYPE_IMAGE) {
            VolleyFileObject volleyFileObject1 = FileOperations.getVolleyFileObject(getActivity(), data, "url", Constants.FILE_TYPE_IMAGE);
            File file = new File(getCacheDir(), Constants.IMAGE + ".png");
            UCrop.of(Uri.fromFile(volleyFileObject1.getFile()), Uri.fromFile(file))
                    .start(context, FamousAlbumMainDetailsFragment.this);
        } else if (requestCode == Constants.FILE_TYPE_VIDEO) {
            volleyFileObject = FileOperations.getVolleyFileObject(getActivity(), data, "url", Constants.FILE_TYPE_VIDEO);
            MediaMetadataRetriever retriever = new MediaMetadataRetriever();
//use one of overloaded setDataSource() functions to set your data source
            retriever.setDataSource(context, Uri.fromFile(volleyFileObject.getFile()));
            String time = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
            long timeInMillisec = Long.parseLong(time);
            duration = (int) (timeInMillisec / 1000);
            Timber.e("duration:" + duration);
            Timber.e("file length:" + volleyFileObject.getFile().length());
            long fileSize = Integer.parseInt(String.valueOf((volleyFileObject.getFile().length() / 1024) / 1024));
            if (duration <= 30) {
                famousAlbumMainDetailsViewModel.famousAddImageInsideAlbumRequest.type = "2";
            } else {
                volleyFileObject = null;
                toastInfo(getString(R.string.maximum_video_duration_sixty_second));
            }


        } else if (requestCode == UCrop.REQUEST_CROP && data != null) {
            final Uri resultUri = UCrop.getOutput(data);
            if (resultUri != null) {
                volleyFileObject = new VolleyFileObject("url", resultUri.getPath(), Constants.FILE_TYPE_IMAGE);
                famousAlbumMainDetailsViewModel.famousAddImageInsideAlbumRequest.type = "1";
            }
        }
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE && data != null) {

            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();
                UCrop.of(resultUri, resultUri).start(context, FamousAlbumMainDetailsFragment.this);
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Toast.makeText(context, "" + getString(R.string.error_failed_pick_gallery_image), Toast.LENGTH_SHORT).show();
            }
        }

        if (volleyFileObject != null){
//            Log.d(TAG,volleyFileObject.getFileType()+"");
//            if(volleyFileObject.getFileType() == Constants.FILE_TYPE_VIDEO) {
//                String path = FileOperations.getPath(context,data.getData());
//                Log.d(TAG,"path:"+path);
//                compressVideo(path,volleyFileObject, duration);
//            }else
                famousAlbumMainDetailsViewModel.addImage(volleyFileObject);
        }
        if (resultCode == RESULT_OK && requestCode == Constants.RELOAD_RESULT) {
            famousAlbumMainDetailsViewModel.getFamousRepository().getAlbumImages(id);
        }


        super.onActivityResult(requestCode, resultCode, data);
    }


    private void compressVideo(String path , VolleyFileObject volleyFileObject,int durationTime) {
        Log.d(TAG,"compressVideo");
//        String root = Environment.getExternalStorageDirectory().toString();
        File root = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES);
        String destPath = root + File.separator + "VIDEO_COMPRESSED.mp4";
//        String destPath = root + "/compress_video/";
//==========================//==========================//==========================//==========================//==========================
        if (volleyFileObject.getFile() != null) {
            Log.d(TAG,"compressVideo getFile");
            Log.d(TAG,"root:"+root);
//            String inputVideoPath = volleyFileObject.getFile().getPath();
            famousAlbumMainDetailsViewModel.mMutableLiveData.setValue(Constants.SHOW_PROGRESS_PERCENTAGE);
//            if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            VideoCompressor.start(volleyFileObject.getFile().getAbsolutePath(), destPath, new CompressionListener() {
                @Override
                public void onStart() {
                    // Compression start
                    Log.d(TAG, "onStart:");
                }

                @Override
                public void onSuccess() {
                    // On Compression success
                    Log.d(TAG, "onSuccess:");
                    requireActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            ArrayList<VolleyFileObject> volleyFileObjects = new ArrayList<>();
                            File file = new File(destPath);
                            famousAlbumMainDetailsViewModel.mMutableLiveData.setValue(Constants.HIDE_PROGRESS_PERCENTAGE);
                            volleyFileObject.setFile(file);
                            volleyFileObject.setFilePath(destPath);
                            volleyFileObjects.add(volleyFileObject);
                            famousAlbumMainDetailsViewModel.addImage(volleyFileObject);
                        }
                    });
                }

                @Override
                public void onFailure(String failureMessage) {
                    // On Failure
                    Log.d(TAG, "failure:" + failureMessage);
                }

                @Override
                public void onProgress(float finalPercent) {
                    Log.d(TAG, finalPercent + "");
                    // Update UI with progress value
                    requireActivity().runOnUiThread(new Runnable() {
                        public void run() {
                            famousAlbumMainDetailsViewModel.mMutableLiveData.setValue(
                                    new Mutable(Constants.PROGRESS_PERCENTAGE, (int) finalPercent));
                        }
                    });
                }

                @Override
                public void onCancelled() {
                    // On Cancelled
                    Log.d(TAG, "onCancelled:");
                }
            }, VideoQuality.VERY_HIGH, false, false);
        }
//            } else {
//                try {
//                    root.mkdir();
//                    root.createNewFile();
////                    ProgressDialog dialog = ProgressDialog.show(context, getString(R.string.please_wait), getString(R.string.please_wait), true);
//////                    ProgressDialog dialog = new ProgressDialog(requireContext());
////                    dialog.setCancelable(false);
////                    dialog.show();
//                    FFmpeg ffmpeg = FFmpeg.getInstance(requireActivity());
//                    try {
//                        //Load the binary
//                        ffmpeg.loadBinary(new LoadBinaryResponseHandler() {
//                            @Override
//                            public void onStart() {
//                            }
//
//                            @Override
//                            public void onFailure() {
//                            }
//
//                            @Override
//                            public void onSuccess() {
//                            }
//
//                            @Override
//                            public void onFinish() {
//
//                            }
//                        });
//                    } catch (FFmpegNotSupportedException e) {
//                        // Handle if FFmpeg is not supported by device
//                        Log.d("FFmpeg", e.getMessage());
//                    }
//                    try {
//                        // to execute "ffmpeg -version" command you just need to pass "-version"
////                                String outputPath = destPath;
//                        String[] commandArray = new String[]{};
//                        commandArray = new String[]{"-y", "-i", inputVideoPath, "-s", "720x480", "-r", "25",
//                                "-vcodec", "mpeg4", "-b:v", "300k", "-b:a", "48000", "-ac", "2", "-ar", "22050", destPath};
////                        final ProgressDialog dialog = new ProgressDialog(requireContext());
//                        ffmpeg.execute(commandArray, new ExecuteBinaryResponseHandler() {
//                            @Override
//                            public void onStart() {
//                                Log.e("FFmpeg", "onStart");
//                            }
//
//                            @Override
//                            public void onProgress(String message) {
//                                Log.e("FFmpeg onProgress? ", message);
//
//                                int start = message.indexOf("time=00:00:");
//                                int end = message.indexOf(" bitrate");
//                                if (start != -1 && end != -1) {
//                                    String duration = message.substring(start + 11, end);
//                                    try {
//                                        Log.d(TAG, "duration_message:" + duration);
//                                        float durationNumber = Float.parseFloat(duration);
//                                        Log.d(TAG, durationNumber + "");
//                                        float finalPercent = (durationNumber / durationTime) * 100;
//                                        Log.d(TAG, "finalPercent:" + finalPercent);
//
//                                        requireActivity().runOnUiThread(new Runnable() {
//                                            public void run() {
//                                                statusViewModel.mMutableLiveData.setValue(
//                                                        new Mutable(Constants.PROGRESS_PERCENTAGE, (int) finalPercent));
//                                            }
//                                        });
//
//
//                                    } catch (Exception ex) {
//                                        Log.d(TAG, "ex:" + ex.getMessage());
//                                    }
////                                    if (!duration.equals("")) {
////                                        try {
////                                            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
////                                            sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
////                                            int percent = (int) sdf.parse("1970-01-01 " + duration).getTime();
////                                            percent /= 1000;
////                                            percent /= durationTime;
////                                            percent *= 100;
////                                            int finalPercent = percent;
////                                            Log.d(TAG,"percent:"+finalPercent+"");
////                                            requireActivity().runOnUiThread(new Runnable() {
////                                                public void run() {
////                                                    statusViewModel.mMutableLiveData.setValue(
////                                                            new Mutable(Constants.PROGRESS_PERCENTAGE, finalPercent));
////                                                }
////                                            });
////
////                                        } catch (ParseException e) {
////                                            e.printStackTrace();
////                                        }
////                                    }
//                                }
//
//
////                                Pattern timePattern = Pattern.compile("(?<=time=)[\\d:.]*");
////                                Scanner sc = new Scanner(message);
////
////                                String match = sc.findWithinHorizon(timePattern, 0);
////                                if (match != null) {
////                                    String[] matchSplit = match.split(":");
////                                    if (duration != 0) {
////                                        float progress = (Integer.parseInt(matchSplit[0]) * 3600 +
////                                                Integer.parseInt(matchSplit[1]) * 60 +
////                                                Float.parseFloat(matchSplit[2])) / duration;
////                                        int showProgress = (int) (progress * 100000);
////                                        Log.d(TAG,showProgress+"");
////                                    }
////                                }
//
//                            }
//
//                            @Override
//                            public void onFailure(String message) {
//                                Log.e("FFmpeg onFailure? ", message);
//                            }
//
//                            @Override
//                            public void onSuccess(String message) {
//                                Log.e("FFmpeg onSuccess? ", message);
//
//                            }
//
//                            @Override
//                            public void onFinish() {
//                                LanguagesHelper.changeLanguage(requireContext(), LanguagesHelper.getCurrentLanguage());
//                                LanguagesHelper.changeLanguage(MyApplication.getInstance(), LanguagesHelper.getCurrentLanguage());
//
//                                statusViewModel.mMutableLiveData.setValue(Constants.HIDE_PROGRESS_PERCENTAGE);
//
//                                File file = new File(destPath);
//                                volleyFileObject.setFile(file);
//                                volleyFileObject.setFilePath(destPath);
//                                volleyFileObjects.add(volleyFileObject);
//                                statusViewModel.setImage(volleyFileObjects, 2, (int) durationTime);
////                            playVideoOnVideoView(Uri.parse(outputPath));
////                            isCompressed = true;
////                            count = count + 1;
//                            }
//                        });
//                    } catch (FFmpegCommandAlreadyRunningException e) {
//                        e.printStackTrace();
//                        Log.d(TAG, "exc:" + e.getMessage());
//                        // Handle if FFmpeg is already running
//                    }
//                } catch (IOException e) {
//                    e.printStackTrace();
//                    Log.d(TAG, "exc IOException:" + e.getMessage());
//                }
//            }
//        }
////==========================//==========================//==========================//==========================//==========================
    }

}
