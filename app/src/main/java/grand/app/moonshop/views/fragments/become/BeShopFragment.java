package grand.app.moonshop.views.fragments.become;


import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.theartofdev.edmodo.cropper.CropImage;
import com.yalantis.ucrop.UCrop;

import java.io.File;
import java.util.ArrayList;

import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moonshop.R;
import grand.app.moonshop.base.BaseFragment;
import grand.app.moonshop.base.ParentActivity;
import grand.app.moonshop.databinding.FragmentBeShopBinding;
import grand.app.moonshop.models.service.Service;
import grand.app.moonshop.models.user.profile.User;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.utils.PopUp.PopUpInterface;
import grand.app.moonshop.utils.PopUp.PopUpMenuHelper;
import grand.app.moonshop.utils.storage.user.UserHelper;
import grand.app.moonshop.utils.upload.FileOperations;
import grand.app.moonshop.viewmodels.become.BeShopViewModel;
import grand.app.moonshop.views.activities.MainActivity;
import grand.app.moonshop.vollyutils.VolleyFileObject;
import timber.log.Timber;

import static android.app.Activity.RESULT_OK;
import static com.facebook.FacebookSdk.getCacheDir;

/**
 * A simple {@link Fragment} subclass.
 */
public class BeShopFragment extends BaseFragment {

    View rootView;

    private FragmentBeShopBinding fragmentBeShopBinding;
    private BeShopViewModel beShopViewModel;
    public PopUpMenuHelper popUpMenuHelper = new PopUpMenuHelper();

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentBeShopBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_be_shop, container, false);
        bind();
        setEvent();
        return rootView;
    }

    private void bind() {
        beShopViewModel = new BeShopViewModel();
        fragmentBeShopBinding.setBeShopViewModel(beShopViewModel);
        initPopUp();
        rootView = fragmentBeShopBinding.getRoot();
    }

    private void initPopUp() {
        fragmentBeShopBinding.edtBeShopService.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (beShopViewModel.getServiceRepository().getServiceResponse() == null)
                    getServiceType(); // get Data and open popUpMenu
                else
                    popUp();//open popUpMenu without call service
            }
        });
    }


    public void getServiceType() {
        String type = Constants.TYPE_CONSUMER_MARKET;
        beShopViewModel.getServices(Integer.parseInt(type),0,1);
    }


    private void setEvent() {
        beShopViewModel.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                String action = (String) o;
                handleActions(action, beShopViewModel.getServiceRepository().getMessage());
                handleActions(action, beShopViewModel.getBecomeRepository().getMessage());
                Timber.e(action);
                if (action.equals(Constants.DONE)) {//submitSearch register
                    Intent intent = new Intent(context, MainActivity.class);
                    toastMessage(beShopViewModel.getBecomeRepository().getMessage(), R.drawable.ic_check_white,
                            R.color.colorPrimary);
                    ((ParentActivity) context).finishAffinity();
                    User user = UserHelper.getUserDetails();
                    user.isShop = true;
                    user.kind = Constants.FAMOUS;
                    //here

                    user.shop_famous_id = beShopViewModel.getBecomeRepository().getBeShopResponse().data.shopFamousId;
                    //shop_famous_type , shop_famous_name , shop_famous_image
                    user.shop_famous_type = beShopViewModel.getBecomeRepository().getBeShopResponse().data.shopFamousType+"";
                    user.shop_famous_name = beShopViewModel.getBecomeRepository().getBeShopResponse().data.shopFamousName;
                    user.shop_famous_image = beShopViewModel.getBecomeRepository().getBeShopResponse().data.shopFamousImage;

                    //finish
                    UserHelper.saveUserDetails(user);
                    beShopViewModel.beShopRequest.clear();
                    beShopViewModel.notifyChange();
                    fragmentBeShopBinding.imgBeShop.setImageResource(R.drawable.ic_user);//TODO
                    startActivity(intent);
                } else if (action.equals(Constants.SERVICE_SUCCESS)) {//get service type
                    //Log services
                } else if (action.equals(Constants.SELECT_IMAGE)) {//select image profile
                    pickImageDialogSelect();
                } else if (action.equals(Constants.ERROR)) {//ex: like image
                    showError(beShopViewModel.baseError);
                } else if (action.equals(Constants.SUCCESS)) {
                    popUp();
                }
            }
        });
    }

    public void popUp() {
        ArrayList<String> popUpModelsService = new ArrayList<>();
        for (Service service : beShopViewModel.getServiceRepository().getServiceResponse().services) {
            popUpModelsService.add(service.name);
        }
        popUpMenuHelper.openPopUp(getActivity(), fragmentBeShopBinding.edtBeShopService, popUpModelsService, new PopUpInterface() {
            @Override
            public void submitPopUp(int position) {
                fragmentBeShopBinding.edtBeShopService.setText(popUpModelsService.get(position));
                beShopViewModel.beShopRequest.setShopService(beShopViewModel.getServiceRepository().getServiceResponse().services.get(position).id + "");
            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        //super method removed
        Timber.e("onActivityResult:" + requestCode);
        if (requestCode == Constants.FILE_TYPE_IMAGE) {
            VolleyFileObject volleyFileObject1 = FileOperations.getVolleyFileObject(getActivity(), data, Constants.IMAGE, Constants.FILE_TYPE_IMAGE);
            File file = new File(getCacheDir(), Constants.IMAGE + ".png");
            UCrop.of(Uri.fromFile(volleyFileObject1.getFile()), Uri.fromFile(file))
                    .start(context, BeShopFragment.this);
        } else if (requestCode == UCrop.REQUEST_CROP && data != null) {
            final Uri resultUri = UCrop.getOutput(data);
            if (resultUri != null) {
                VolleyFileObject volleyFileObject = new VolleyFileObject(Constants.IMAGE, resultUri.getPath(), Constants.FILE_TYPE_IMAGE);
                beShopViewModel.setImage(volleyFileObject);
                fragmentBeShopBinding.imgBeShop.setImageURI(null);
                fragmentBeShopBinding.imgBeShop.setImageURI(Uri.parse(String.valueOf(new File(volleyFileObject.getFilePath()))));
            }
        }
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE && data != null) {

            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();
                UCrop.of(resultUri, resultUri).start(context, BeShopFragment.this);
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Toast.makeText(context, "" + getString(R.string.error_failed_pick_gallery_image), Toast.LENGTH_SHORT).show();
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        beShopViewModel.reset();

    }
}
