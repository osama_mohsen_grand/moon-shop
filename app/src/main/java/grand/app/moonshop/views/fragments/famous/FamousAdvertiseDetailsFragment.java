package grand.app.moonshop.views.fragments.famous;

import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moonshop.R;
import grand.app.moonshop.base.BaseFragment;
import grand.app.moonshop.databinding.FragmentFamousAdvertiseDetailsBinding;
import grand.app.moonshop.utils.helper.AppUtils;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.viewmodels.famous.FamousAdvertiseDetailsViewModel;
import grand.app.moonshop.views.activities.BaseActivity;


public class FamousAdvertiseDetailsFragment extends BaseFragment {
    private FragmentFamousAdvertiseDetailsBinding fragmentFamousAdvertiseDetailsBinding;
    private FamousAdvertiseDetailsViewModel famousProductDetailsViewModel;

    public int id = -1, type = -1;

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        fragmentFamousAdvertiseDetailsBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_famous_advertise_details, container, false);
        getData();
        bind();
        setEvent();
        return fragmentFamousAdvertiseDetailsBinding.getRoot();
    }

    private void getData() {
        if (getArguments() != null && getArguments().containsKey(Constants.ID))
            id = getArguments().getInt(Constants.ID);
        if (getArguments() != null && getArguments().containsKey(Constants.TYPE))
            type = getArguments().getInt(Constants.TYPE);
    }

    private void bind() {
        famousProductDetailsViewModel = new FamousAdvertiseDetailsViewModel(id,type);
        fragmentFamousAdvertiseDetailsBinding.setFamousAdvertiseDetailsViewModel(famousProductDetailsViewModel);
    }

    private void setEvent() {
        famousProductDetailsViewModel.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                String action = (String) o;
                handleActions(action, famousProductDetailsViewModel.getFamousRepository().getMessage());
                Intent intent = null;
                if (action != null) {
                    if (action.equals(Constants.SUCCESS)) {
                        famousProductDetailsViewModel.showPage(true);
                    }else if(action.equals(Constants.ZOOM)){
                        intent = new Intent(context, BaseActivity.class);
                        intent.putExtra(Constants.PAGE, Constants.ZOOM);
                        Bundle bundle = new Bundle();
                        bundle.putString(Constants.IMAGE, famousProductDetailsViewModel.getFamousRepository().getAdvertiseDetailsResponse().data.serviceImage);
                        intent.putExtra(Constants.BUNDLE, bundle);
                        ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(getActivity(), fragmentFamousAdvertiseDetailsBinding.imgFamousAdvertise, Constants.IMAGE);
                        startActivity(intent, options.toBundle());

                    }else if(action.equals(Constants.SHARE)){
                        if(getActivity() != null)
                        AppUtils.shareContent(getActivity(),famousProductDetailsViewModel.getFamousRepository().getAdvertiseDetailsResponse().data.shop,famousProductDetailsViewModel.getFamousRepository().getAdvertiseDetailsResponse().data.description);

                    }
                }
            }
        });
    }
}
