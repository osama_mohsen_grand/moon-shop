package grand.app.moonshopshop.views.fragments.ads;


import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moonshop.R;
import grand.app.moonshop.adapter.AlbumDetailsAdapter;
import grand.app.moonshop.base.BaseFragment;
import grand.app.moonshop.databinding.FragmentAdvertisementBinding;
import grand.app.moonshop.utils.helper.AppUtils;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.viewmodels.famous.ads.FamousAdsViewModel;

/**
 * A simple {@link Fragment} subclass.
 */
public class AdvertisementFragment extends BaseFragment {

    private Context context;
    private FragmentAdvertisementBinding binding;
    private FamousAdsViewModel viewModel;
    public int famous_id = -1,shop_id=-1;
    public String type = "", tab = "";
    //    private AlbumImagesAdapter albumImagesAdapter;
    private AlbumDetailsAdapter albumAdapter = new AlbumDetailsAdapter(new ArrayList<>());
    ArrayList<Integer> arrayList = new ArrayList<>();

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_advertisement, container, false);
        getData();
        bind();
        setEvent();
        binding.rvFamousAlbumDetails.setAdapter(albumAdapter);
        albumAdapter.defaultSubmit(true);
        return binding.getRoot();
    }

    private void getData() {
        if (getArguments() != null && getArguments().containsKey(Constants.FAMOUS_ID))
            famous_id = getArguments().getInt(Constants.FAMOUS_ID);
        if (getArguments() != null && getArguments().containsKey(Constants.SHOP_ID)) {
            shop_id = getArguments().getInt(Constants.SHOP_ID);
            arrayList.add(shop_id);
        }

    }

    private void bind() {
        AppUtils.initVerticalRV(binding.rvFamousAlbumDetails, binding.rvFamousAlbumDetails.getContext(), 3);
        viewModel = new FamousAdsViewModel();
        viewModel.famous_id = famous_id;
        if(famous_id != -1 && shop_id != -1) viewModel.submitFilter(arrayList);
        binding.setViewmodel(viewModel);
    }

    private void setEvent() {
        viewModel.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                String action = (String) o;
                handleActions(action, viewModel.getAdsRepository().getMessage());
                assert action != null;
                if (action.equals(Constants.SUCCESS)) {
                    albumAdapter.update(viewModel.getAdsRepository().getAdsResponse().data);
                }
            }
        });
    }



    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.context = context;
    }
}
