package grand.app.moonshop.views.fragments.product;


import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.theartofdev.edmodo.cropper.CropImage;
import com.yalantis.ucrop.UCrop;

import java.io.File;

import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moonshop.R;
import grand.app.moonshop.adapter.AlbumEditImagesAdapter;
import grand.app.moonshop.adapter.SizeColorAdapter;
import grand.app.moonshop.base.BaseFragment;
import grand.app.moonshop.customviews.dialog.DialogConfirm;
import grand.app.moonshop.databinding.FragmentAddProductSectoralBinding;
import grand.app.moonshop.models.app.Mutable;
import grand.app.moonshop.models.base.IdNameImage;
import grand.app.moonshop.models.product.details.ProductDetails;
import grand.app.moonshop.utils.helper.AppUtils;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.utils.dialog.DialogHelperInterface;
import grand.app.moonshop.utils.images.ImageLoaderHelper;
import grand.app.moonshop.utils.resources.ResourceManager;
import grand.app.moonshop.utils.upload.FileOperations;
import grand.app.moonshop.viewmodels.product.AddProductSectoralViewModel;
import grand.app.moonshop.vollyutils.VolleyFileObject;
import timber.log.Timber;

import static android.app.Activity.RESULT_OK;

/**
 * A simple {@link Fragment} subclass.
 */
public class AddProductSectoralFragment extends BaseFragment {

    View rootView;
    private FragmentAddProductSectoralBinding addProductSectoralBinding;
    private AddProductSectoralViewModel addProductSectoralViewModel;
    ProductDetails product = null;
    int id = -1;
    SizeColorAdapter sizeAdapter, colorAdapter;
    public AlbumEditImagesAdapter albumEditImagesAdapter;


    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        addProductSectoralBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_add_product_sectoral, container, false);

        getData();
        bind();
        setEvent();
        setImageAdapter();
        return rootView;
    }


    private void setImageAdapter() {
        albumEditImagesAdapter = new AlbumEditImagesAdapter("1", addProductSectoralViewModel.productImages);
        AppUtils.initVerticalRV(addProductSectoralBinding.rvImages, addProductSectoralBinding.rvImages.getContext(), 3);
        addProductSectoralBinding.rvImages.setAdapter(albumEditImagesAdapter);
        setEventAdapterImages();
    }

    private void setEventAdapterImages() {
        albumEditImagesAdapter.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                Mutable mutable = (Mutable) o;
                if (mutable.type.equals(Constants.SUBMIT)) {
                    IdNameImage idNameImage = albumEditImagesAdapter.idNameImages.get(mutable.position);
                    if (idNameImage == null || (idNameImage.image.equals("") && idNameImage.volleyFileObject == null)) {
                        addProductSectoralViewModel.new_image_position = mutable.position;
                        FileOperations.pickImage(context, AddProductSectoralFragment.this, Constants.FILE_TYPE_IMAGE);
                    } else if (idNameImage.volleyFileObject != null) {
                        addProductSectoralViewModel.new_image_position = mutable.position;
                        FileOperations.pickImage(context, AddProductSectoralFragment.this, Constants.FILE_TYPE_IMAGE);
                    }
                } else if (mutable.type.equals(Constants.DELETE)) {
                    IdNameImage idNameImage = albumEditImagesAdapter.idNameImages.get(mutable.position);
                    if (idNameImage.volleyFileObject == null) {
                        new DialogConfirm(getActivity())
                                .setTitle(ResourceManager.getString(R.string.remove_item))
                                .setMessage(ResourceManager.getString(R.string.do_you_want_delete_image))
                                .setActionText(ResourceManager.getString(R.string.remove_item))
                                .setActionCancel(ResourceManager.getString(R.string.cancel))
                                .show(new DialogHelperInterface() {
                                    @Override
                                    public void OnClickListenerContinue(Dialog dialog, View view) {
                                        addProductSectoralViewModel.category_delete_position = mutable.position;
                                        addProductSectoralViewModel.delete(Integer.parseInt(String.valueOf(addProductSectoralViewModel.getProductRepository().getProductDetailsResponse().mData.images.get(mutable.position).id)));
                                    }
                                });
                    } else {
                        albumEditImagesAdapter.remove(mutable.position);
                    }
                }
            }
        });
    }

    int product_id = -1;

    private void getData() {
        Log.d(TAG, "start getData");
        if (getArguments() != null && getArguments().containsKey(Constants.ID))
            id = getArguments().getInt(Constants.ID);
        if (getArguments() != null && getArguments().containsKey(Constants.PRODUCT)) {
            product = (ProductDetails) getArguments().getSerializable(Constants.PRODUCT);
        }
        if (getArguments() != null && getArguments().containsKey(Constants.PRODUCT_ID)) {
            product_id = getArguments().getInt(Constants.PRODUCT_ID);
            Log.d(TAG, "start" + product_id);
        }
    }

    private void bind() {
        addProductSectoralViewModel = new AddProductSectoralViewModel(id, product);
        if (product_id != -1) {
            addProductSectoralViewModel.show.set(false);
            addProductSectoralViewModel.getProductDetails(product_id);
        }else
            addProductSectoralViewModel.show.set(true);

        AppUtils.initVerticalRV(addProductSectoralBinding.rvSize, addProductSectoralBinding.rvSize.getContext(), 1);
        AppUtils.initVerticalRV(addProductSectoralBinding.rvColor, addProductSectoralBinding.rvColor.getContext(), 1);
        addProductSectoralBinding.rvSize.setVisibility(View.GONE);
        addProductSectoralBinding.rvColor.setVisibility(View.GONE);
        addProductSectoralBinding.setAddProductSectoralViewModel(addProductSectoralViewModel);

        rootView = addProductSectoralBinding.getRoot();
    }

    boolean cover = false;

    private void setEvent() {
        addProductSectoralViewModel.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                String action = (String) o;
                handleActions(action, addProductSectoralViewModel.getProductRepository().getMessage());
                assert action != null;
                if (action.equals(Constants.SUCCESS)) {
                    toastMessage(addProductSectoralViewModel.getProductRepository().getMessage(), R.drawable.ic_check_white, R.color.colorPrimary);
                    reloadPrevious();
                } else if (action.equals(Constants.SELECT_IMAGE)) {
                    cover = true;
//                    pickImageDialogSelect();
                    FileOperations.pickImage(context,AddProductSectoralFragment.this,Constants.FILE_TYPE_IMAGE_COVER);
                } else if (action.equals(Constants.ERROR)) {//ex: like image
                    showError(addProductSectoralViewModel.baseError);
                } else if (action.equals(Constants.DELETED)) {
//                    addAdvertisementViewModel.getAdsRepository().getAdsDetailsResponse().data.images.remove(addAdvertisementViewModel.category_delete_position);
                    albumEditImagesAdapter.remove(addProductSectoralViewModel.category_delete_position);
                    addProductSectoralViewModel.productImages.set(addProductSectoralViewModel.category_delete_position, null);
                } else if (action.equals(Constants.PRODUCT)) {
                    ImageLoaderHelper.ImageLoaderLoad(context,addProductSectoralViewModel.productRepository.getProductDetailsResponse().mData.image, addProductSectoralBinding.imgAddProduct);
                    addProductSectoralBinding.imgAddUploadImage.setVisibility(View.GONE);
                    addProductSectoralBinding.tvAddUpload.setVisibility(View.GONE);
                    addProductSectoralViewModel.updateProductUi();
                    albumEditImagesAdapter.update(addProductSectoralViewModel.productImages);
                }
            }
        });

        addProductSectoralViewModel.mMutableLiveDataShop.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                String action = (String) o;
                handleActions(action, addProductSectoralViewModel.getShopRepository().getMessage());
                if (action.equals(Constants.SIZE_COLOR)) {
                    sizeAdapter = new SizeColorAdapter(addProductSectoralViewModel.getShopRepository().getShopSizeColorResponse().sizes, false);
                    colorAdapter = new SizeColorAdapter(addProductSectoralViewModel.getShopRepository().getShopSizeColorResponse().colors, true);
                    addProductSectoralBinding.rvSize.setAdapter(sizeAdapter);
                    addProductSectoralBinding.rvColor.setAdapter(colorAdapter);
                    setEventAdapter();
                }
            }
        });


        addProductSectoralBinding.edtProductSizeSelect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addProductSectoralBinding.rvColor.setVisibility(View.GONE);
                addProductSectoralBinding.rvSize.setVisibility(View.VISIBLE);
            }
        });
        addProductSectoralBinding.edtProductColorSelect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addProductSectoralBinding.rvColor.setVisibility(View.VISIBLE);
                addProductSectoralBinding.rvSize.setVisibility(View.GONE);
            }
        });


    }

    private void setEventAdapter() {
        sizeAdapter.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                Mutable mutable = (Mutable) o;
                setSelect(mutable, Constants.SIZE);

            }
        });
        colorAdapter.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                Mutable mutable = (Mutable) o;
                setSelect(mutable, Constants.COLOR);
            }
        });
    }

    private void setSelect(Mutable mutable, String sizeColor) {
        if (sizeColor.equals(Constants.SIZE)) {
            addProductSectoralBinding.edtProductSizeSelect.setText(addProductSectoralViewModel.getShopRepository().getShopSizeColorResponse().sizes.get(mutable.position).name);
            addProductSectoralViewModel.addProductSectoralRequest.sizeId = addProductSectoralViewModel.getShopRepository().getShopSizeColorResponse().sizes.get(mutable.position).id;
        } else {
            addProductSectoralBinding.edtProductColorSelect.setText(addProductSectoralViewModel.getShopRepository().getShopSizeColorResponse().colors.get(mutable.position).name);
            addProductSectoralBinding.edtProductColorSelect.setTextColor(Color.parseColor(addProductSectoralViewModel.getShopRepository().getShopSizeColorResponse().colors.get(mutable.position).name));
            addProductSectoralViewModel.addProductSectoralRequest.colorId = addProductSectoralViewModel.getShopRepository().getShopSizeColorResponse().colors.get(mutable.position).id;
        }
        addProductSectoralBinding.rvSize.setVisibility(View.GONE);
        addProductSectoralBinding.rvColor.setVisibility(View.GONE);
    }

    private static final String TAG = "AddProductSectoralFragm";

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        //super method removed
        Timber.e("onActivityResult:" + requestCode);
        if (requestCode == Constants.FILE_TYPE_IMAGE) {
            VolleyFileObject volleyFileObject1 = FileOperations.getVolleyFileObject(getActivity(), data, "url", Constants.FILE_TYPE_IMAGE);
//            File file = new File(volleyFileObject1.getFilePath());
            File file = new File(volleyFileObject1.getFilePath());
//            getCacheDir(), Constants.IMAGE+addProductSectoralViewModel.new_image_position + ".png"
            UCrop.of(Uri.fromFile(volleyFileObject1.getFile()), Uri.fromFile(file))
                    .start(context, AddProductSectoralFragment.this);

        } else if (requestCode == UCrop.REQUEST_CROP && data != null) {
            Log.d(TAG, "doner");
            final Uri resultUri = UCrop.getOutput(data);
            if (resultUri != null) {
                if(cover){
                    cover = false;
                    VolleyFileObject volleyFileObject = new VolleyFileObject( Constants.IMAGE, resultUri.getPath(), Constants.FILE_TYPE_IMAGE);
                    addProductSectoralBinding.imgAddProduct.setImageURI(Uri.parse(String.valueOf(new File(volleyFileObject.getFilePath()))));
                    addProductSectoralViewModel.setImage(volleyFileObject);
                }else {
                    Log.d(TAG, addProductSectoralViewModel.new_image_position + "");
                    Log.d(TAG, addProductSectoralViewModel.productImages.size() + "");
                    int pos = addProductSectoralViewModel.new_image_position + 1 -
                            addProductSectoralViewModel.productImages.size();
                    VolleyFileObject volleyFileObject = new VolleyFileObject("product_images[" + pos + "]", resultUri.getPath(), Constants.FILE_TYPE_IMAGE);
                    Log.d("path", volleyFileObject.getFilePath());
                    albumEditImagesAdapter.update(addProductSectoralViewModel.new_image_position, volleyFileObject);
                    addProductSectoralViewModel.productImages.set(addProductSectoralViewModel.new_image_position,
                            albumEditImagesAdapter.idNameImages.get(addProductSectoralViewModel.new_image_position));
                }
            }
        }
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE && data != null) {
            Log.d(TAG, "CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE");
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();
                UCrop.of(resultUri, resultUri).start(context, AddProductSectoralFragment.this);
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Toast.makeText(context, "" + getString(R.string.error_failed_pick_gallery_image), Toast.LENGTH_SHORT).show();
            }
        }
//        else if (requestCode == Constants.FILE_TYPE_IMAGE_COVER) {
//            VolleyFileObject volleyFileObject = FileOperations.getVolleyFileObject(getActivity(), data, Constants.IMAGE, Constants.FILE_TYPE_IMAGE);
//            addProductSectoralBinding.imgAddProduct.setImageURI(Uri.parse(String.valueOf(new File(volleyFileObject.getFilePath()))));
//            addProductSectoralViewModel.setImage(volleyFileObject);
//
//        }
        super.onActivityResult(requestCode, resultCode, data);
    }


}
