package grand.app.moonshop.views.fragments.institution;


import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.theartofdev.edmodo.cropper.CropImage;
import com.yalantis.ucrop.UCrop;

import java.io.File;

import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moonshop.R;
import grand.app.moonshop.adapter.AlbumAddAdapter;
import grand.app.moonshop.base.BaseFragment;
import grand.app.moonshop.base.ParentActivity;
import grand.app.moonshop.databinding.FragmentAddBranchInstitutionBinding;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.utils.storage.user.UserHelper;
import grand.app.moonshop.utils.upload.FileOperations;
import grand.app.moonshop.viewmodels.branch.AddBranchInstitutionViewModel;
import grand.app.moonshop.views.activities.BaseActivity;
import grand.app.moonshop.views.activities.MainActivity;
import grand.app.moonshop.views.activities.MapAddressActivity;
import grand.app.moonshop.vollyutils.VolleyFileObject;
import timber.log.Timber;

import static android.app.Activity.RESULT_OK;
import static com.facebook.FacebookSdk.getCacheDir;

/**
 * A simple {@link Fragment} subclass.
 */
public class AddBranchInstitutionFragment extends BaseFragment {
    FragmentAddBranchInstitutionBinding fragmentAddBranchInstitutionBinding;
    AddBranchInstitutionViewModel addBranchInstitutionViewModel;
    AlbumAddAdapter albumAdapter = null;
    int FILE_TYPE = -1;

    //    int MAX_ITEMS = 6;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        fragmentAddBranchInstitutionBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_add_branch_institution, container, false);
        init();
        setEvent();
        return fragmentAddBranchInstitutionBinding.getRoot();
    }


    private void init() {
        addBranchInstitutionViewModel = new AddBranchInstitutionViewModel();
        fragmentAddBranchInstitutionBinding.setAddBranchInstitutionViewModel(addBranchInstitutionViewModel);
    }


    private void setEvent() {
        addBranchInstitutionViewModel.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                String action = (String) o;
                handleActions(action, addBranchInstitutionViewModel.getInstitutionRepository().getMessage());
                assert action != null;
                if (action.equals(Constants.IMAGE)) {
                    pickImageDialogSelect();
                } else if (action.equals(Constants.SUCCESS)) {
                    Toast.makeText(context, "" + addBranchInstitutionViewModel.getInstitutionRepository().getMessage(), Toast.LENGTH_SHORT).show();
                    if(UserHelper.getUserDetails().type.equals(Constants.TYPE_INSTITUTIONS)){
                        reloadPage();
                        ((BaseActivity) context).finishAffinity();
                        startActivity(new Intent(context, MainActivity.class));
                    }else {
                        Intent intent = new Intent();
                        getActivityBase().setResult(RESULT_OK, intent);
                        getActivityBase().finish();
                    }
                } else if (action.equals(Constants.ERROR)) {
                    showError(addBranchInstitutionViewModel.baseError);
                } else if (action.equals(Constants.LOCATION)) {
                    Intent intent = new Intent(context, MapAddressActivity.class);
                    startActivityForResult(intent, Constants.ADDRESS_RESULT);
                }
            }
        });
    }

    int next = -1;

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        //super method removed
        Timber.e("onActivityResult:" + requestCode);
        if (requestCode == Constants.FILE_TYPE_IMAGE) {
            VolleyFileObject volleyFileObject1 = FileOperations.getVolleyFileObject(getActivity(), data, "image", Constants.FILE_TYPE_IMAGE);
            File file = new File(getCacheDir(), Constants.IMAGE + ".png");
            UCrop.of(Uri.fromFile(volleyFileObject1.getFile()), Uri.fromFile(file))
                    .start(context, AddBranchInstitutionFragment.this);

        }else if (requestCode == UCrop.REQUEST_CROP && data != null) {
            final Uri resultUri = UCrop.getOutput(data);
            if (resultUri != null) {
                addBranchInstitutionViewModel.addBranchRequest.volleyFileObject =
                        new VolleyFileObject("image", resultUri.getPath(), Constants.FILE_TYPE_IMAGE);
                fragmentAddBranchInstitutionBinding.imgAddAlbum.setImageURI(Uri.parse(String.valueOf(new File(addBranchInstitutionViewModel.addBranchRequest.volleyFileObject.getFilePath()))));

            }
        }
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE && data != null) {

            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();
                UCrop.of(resultUri, resultUri).start(context, AddBranchInstitutionFragment.this);
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Toast.makeText(context, "" + getString(R.string.error_failed_pick_gallery_image), Toast.LENGTH_SHORT).show();
            }
        }

        if (resultCode == RESULT_OK && requestCode == Constants.ADDRESS_RESULT) {
            addBranchInstitutionViewModel.setAddress(data.getDoubleExtra("lat", 0), data.getDoubleExtra("lng", 0));
        }
        super.onActivityResult(requestCode, resultCode, data);
    }


}
