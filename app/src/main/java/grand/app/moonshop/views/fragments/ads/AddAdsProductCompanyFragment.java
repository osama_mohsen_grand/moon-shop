package grand.app.moonshop.views.fragments.ads;


import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.theartofdev.edmodo.cropper.CropImage;
import com.yalantis.ucrop.UCrop;

import java.io.File;
import java.util.ArrayList;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moonshop.MultipleImageSelect.activities.AlbumSelectActivity;
import grand.app.moonshop.MultipleImageSelect.models.Image;
import grand.app.moonshop.R;
import grand.app.moonshop.adapter.AlbumEditImagesAdapter;
import grand.app.moonshop.base.BaseFragment;
import grand.app.moonshop.customviews.dialog.DialogConfirm;
import grand.app.moonshop.databinding.FragmentAddAdsBinding;
import grand.app.moonshop.models.app.Mutable;
import grand.app.moonshop.models.base.IdNameImage;
import grand.app.moonshop.utils.helper.AppUtils;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.utils.dialog.DialogHelperInterface;
import grand.app.moonshop.utils.resources.ResourceManager;
import grand.app.moonshop.utils.upload.FileOperations;
import grand.app.moonshop.views.activities.MapAddressActivity;
import grand.app.moonshop.vollyutils.VolleyFileObject;
import timber.log.Timber;

import static android.app.Activity.RESULT_OK;
import static com.facebook.FacebookSdk.getCacheDir;

/**
 * ´
 * A simple {@link Fragment} subclass.
 */
public class AddAdsProductCompanyFragment extends BaseFragment {

    View rootView;
    private FragmentAddAdsBinding fragmentAddAdsBinding;
    private AddAdvertisementCompanyViewModel addAdvertisementViewModel;
    private int id = -1, category_id = -1;
    public AlbumEditImagesAdapter albumEditImagesAdapter;


    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentAddAdsBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_add_ads, container, false);
        getData();
        bind();
        setEvent();
        return rootView;
    }


    private static final String TAG = "RegisterCarFragment";

    private void getData() {
        if (getArguments() != null && getArguments().containsKey(Constants.ID))
            id = getArguments().getInt(Constants.ID);
        if (getArguments() != null && getArguments().containsKey(Constants.CATEGORY_ID)) {
            category_id = getArguments().getInt(Constants.CATEGORY_ID);
        }
        addAdvertisementViewModel = new AddAdvertisementCompanyViewModel(id,category_id);
        if(id != -1){
            AppUtils.initVerticalRV(fragmentAddAdsBinding.rvImages, fragmentAddAdsBinding.rvImages.getContext(), 3);
        }
    }

    private void bind() {
        fragmentAddAdsBinding.setAddAdvertisementViewModel(addAdvertisementViewModel);
        rootView = fragmentAddAdsBinding.getRoot();
    }

    private void setEvent() {
        addAdvertisementViewModel.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                String action = (String) o;
                handleActions(action, addAdvertisementViewModel.getAdsRepository().getMessage());
                assert action != null;
                if (action.equals(Constants.SUCCESS)) {
                    toastMessage(addAdvertisementViewModel.getAdsRepository().getMessage());
                    reloadPrevious();
//                    if(id == -1) {
//                        getActivityBase().finishAffinity();
//                        Intent intent = new Intent(context, MainActivity.class);
//                        intent.putExtra(Constants.PAGE, Constants.MY_ADS);
//                        startActivity(intent);
//                    }else{
//                    }
                } else if (action.equals(Constants.IMAGE)) {
                    Intent intent = new Intent(context, AlbumSelectActivity.class);
                    intent.putExtra(grand.app.moonshop.MultipleImageSelect.helpers.Constants.INTENT_EXTRA_LIMIT, 3);
                    startActivityForResult(intent, Constants.REQUEST_CODE);
                } else if (action.equals(Constants.LOCATION)) {
                    Intent intent = new Intent(context, MapAddressActivity.class);
                    startActivityForResult(intent, Constants.ADDRESS_RESULT);
                }else if(action.equals(Constants.ADS_DETAILS)){
                    addAdvertisementViewModel.showPage(true);
                    addAdvertisementViewModel.updateView(addAdvertisementViewModel.getAdsRepository().getAdsDetailsResponse().data);
                    fragmentAddAdsBinding.tvAddAdvertisementUploaded
                            .setText(getString(R.string.uploaded) + " " + addAdvertisementViewModel.getAdsRepository().getAdsDetailsResponse().data.productImages.size() + " " + getString(R.string.photos));
                    albumEditImagesAdapter = new AlbumEditImagesAdapter("1",addAdvertisementViewModel.getAdsRepository().getAdsDetailsResponse().data.productImages);
                    fragmentAddAdsBinding.rvImages.setAdapter(albumEditImagesAdapter);
                    albumEditImagesAdapter.addNewImageView();
                    setEventAdapter();
                }else if(action.equals(Constants.DELETED)){
//                    addAdvertisementViewModel.getAdsRepository().getAdsDetailsResponse().data.productImages.remove(addAdvertisementViewModel.category_delete_position);
                    albumEditImagesAdapter.removeFrame(addAdvertisementViewModel.category_delete_position);
                }
            }
        });
    }

    private void setEventAdapter() {
        albumEditImagesAdapter.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                Mutable mutable = (Mutable) o;
                if (mutable.type.equals(Constants.SUBMIT)) {
                    IdNameImage idNameImage = albumEditImagesAdapter.idNameImages.get(mutable.position);
                    if(idNameImage.image.equals("") && idNameImage.volleyFileObject == null) {
                        addAdvertisementViewModel.new_image_position = mutable.position;
                        FileOperations.pickImage(context, AddAdsProductCompanyFragment.this, Constants.FILE_TYPE_IMAGE);
                    }else if(idNameImage.volleyFileObject != null){
                        addAdvertisementViewModel.new_image_position = mutable.position;
                        FileOperations.pickImage(context, AddAdsProductCompanyFragment.this, Constants.FILE_TYPE_IMAGE);
                    }
                } else if (mutable.type.equals(Constants.DELETE)) {
                    IdNameImage idNameImage = albumEditImagesAdapter.idNameImages.get(mutable.position);
                    if(idNameImage.volleyFileObject == null) {
                        new DialogConfirm(getActivity())
                                .setTitle(ResourceManager.getString(R.string.remove_product))
                                .setMessage(ResourceManager.getString(R.string.do_you_want_delete_image))
                                .setActionText(ResourceManager.getString(R.string.remove_item))
                                .setActionCancel(ResourceManager.getString(R.string.cancel))
                                .show(new DialogHelperInterface() {
                                    @Override
                                    public void OnClickListenerContinue(Dialog dialog, View view) {
                                        addAdvertisementViewModel.category_delete_position = mutable.position;
                                        addAdvertisementViewModel.delete(addAdvertisementViewModel.getAdsRepository().getAdsDetailsResponse().data.productImages.get(mutable.position).id);
                                    }
                                });
                    }else{
                        albumEditImagesAdapter.removeFrame(mutable.position);
                    }
                }
            }
        });
    }


    int last_pos = -1;

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        //super method removed
        Timber.e("onActivityResult:" + requestCode);
        if (resultCode == Activity.RESULT_OK && requestCode == Constants.ADDRESS_RESULT) {
            addAdvertisementViewModel.setAddress(data.getDoubleExtra("lat", 0), data.getDoubleExtra("lng", 0));
        } else if (requestCode == Constants.REQUEST_CODE && resultCode == Activity.RESULT_OK && data != null) {
            //The array list has the image paths of the selected images
            try {
                ArrayList<Image> images = data.getParcelableArrayListExtra(Constants.INTENT_EXTRA_IMAGES);

                if (images.size() > 0) {
                    addAdvertisementViewModel.volleyFileObjects.clear();
                    fragmentAddAdsBinding.tvAddAdvertisementUploaded.setText(getString(R.string.uploaded) + " " + images.size() + " " + getString(R.string.photos));
                    for (int i = 0; i < images.size(); i++) {
                        addAdvertisementViewModel.volleyFileObjects.add(new VolleyFileObject("product_images[" + i + "]", images.get(i).path, Constants.FILE_TYPE_IMAGE));
                    }
                }

            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE && data != null) {

            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();
                UCrop.of(resultUri, resultUri).start(context, AddAdsProductCompanyFragment.this);
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Toast.makeText(context, "" + getString(R.string.error_failed_pick_gallery_image), Toast.LENGTH_SHORT).show();
            }
        } else if (requestCode == Constants.FILE_TYPE_IMAGE ) {
            VolleyFileObject volleyFileObject1 = FileOperations.getVolleyFileObject(getActivity(), data, "url", Constants.FILE_TYPE_IMAGE);
            File file = new File(getCacheDir(), Constants.IMAGE + ".png");
            UCrop.of(Uri.fromFile(volleyFileObject1.getFile()), Uri.fromFile(file)).start(context, AddAdsProductCompanyFragment.this);
        }else if (requestCode == UCrop.REQUEST_CROP && data != null) {
            final Uri resultUri = UCrop.getOutput(data);
            if (resultUri != null) {
                int pos = addAdvertisementViewModel.new_image_position +1 -  addAdvertisementViewModel.getAdsRepository().getAdsDetailsResponse().data.productImages.size();
                Timber.e("pos_new:"+addAdvertisementViewModel.new_image_position );
                Timber.e("pos_before:"+addAdvertisementViewModel.getAdsRepository().getAdsDetailsResponse().data.productImages.size());
                Timber.e("pos_final:"+pos);
                VolleyFileObject volleyFileObject = new VolleyFileObject("product_images["+ pos +"]", resultUri.getPath(), Constants.FILE_TYPE_IMAGE);
                albumEditImagesAdapter.update(volleyFileObject,addAdvertisementViewModel.new_image_position);
                if(addAdvertisementViewModel.new_image_position >= last_pos) {
                    albumEditImagesAdapter.addNewImageView();
                    last_pos = addAdvertisementViewModel.new_image_position;
                }
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
}
