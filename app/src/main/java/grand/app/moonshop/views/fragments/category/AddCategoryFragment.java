package grand.app.moonshop.views.fragments.category;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moonshop.R;
import grand.app.moonshop.base.BaseFragment;
import grand.app.moonshop.base.ParentActivity;
import grand.app.moonshop.databinding.FragmentAddCategoryBinding;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.viewmodels.category.AddCategoryViewModel;

/**
 * A simple {@link Fragment} subclass.
 */
public class AddCategoryFragment extends BaseFragment {

    View rootView;
    private FragmentAddCategoryBinding fragmentAddCategoryBinding;
    private AddCategoryViewModel addCategoryViewModel;
    int id = -1; String name = "";


    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        fragmentAddCategoryBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_add_category, container, false);
        getData();
        bind();
        setEvent();
        return rootView;
    }

    private void getData() {
        if(getArguments() != null && getArguments().containsKey(Constants.ID))
            id = getArguments().getInt(Constants.ID);
        if(getArguments() != null && getArguments().containsKey(Constants.NAME))
            name = getArguments().getString(Constants.NAME);
    }

    private void bind() {
        addCategoryViewModel = new AddCategoryViewModel(id,name);
        fragmentAddCategoryBinding.setAddCategoryViewModel(addCategoryViewModel);
        rootView = fragmentAddCategoryBinding.getRoot();
    }

    private void setEvent() {
        addCategoryViewModel.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                String action = (String) o;
                handleActions(action, addCategoryViewModel.getCategoryRepository().getMessage());
                assert action != null;
                if(action.equals(Constants.SUCCESS)){
                    toastMessage(addCategoryViewModel.getCategoryRepository().getMessage(),R.drawable.ic_check_white,R.color.colorPrimary);
                    reloadPrevious();
                }
            }
        });
    }

}
