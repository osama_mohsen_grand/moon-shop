package grand.app.moonshop.views.fragments.offers;


import android.content.Intent;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.theartofdev.edmodo.cropper.CropImage;
import com.yalantis.ucrop.UCrop;

import java.io.File;

import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moonshop.R;
import grand.app.moonshop.base.BaseFragment;
import grand.app.moonshop.databinding.FragmentAddOfferBinding;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.utils.resources.ResourceManager;
import grand.app.moonshop.utils.upload.FileOperations;
import grand.app.moonshop.viewmodels.offer.AddOfferViewModel;
import grand.app.moonshop.vollyutils.VolleyFileObject;
import timber.log.Timber;

import static android.app.Activity.RESULT_OK;
import static com.facebook.FacebookSdk.getCacheDir;
import static com.facebook.GraphRequest.TAG;

/**
 * A simple {@link Fragment} subclass.
 */
public class AddOfferFragment extends BaseFragment {


    View rootView;
    private FragmentAddOfferBinding fragmentAddOfferBinding;
    private AddOfferViewModel addOfferViewModel;


    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        fragmentAddOfferBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_add_offer, container, false);
        bind();
        setEvent();
        return rootView;
    }

    private void bind() {
        addOfferViewModel = new AddOfferViewModel();
        fragmentAddOfferBinding.setAddOfferViewModel(addOfferViewModel);
        rootView = fragmentAddOfferBinding.getRoot();
    }

    private static final String TAG = "SplashFragment";

    private void setEvent() {
        addOfferViewModel.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                String action = (String) o;
                handleActions(action, addOfferViewModel.getOfferRepository().getMessage());
                assert action != null;
                if (action.equals(Constants.ADD)) {
                    if (addOfferViewModel.addOfferRequest.volleyFileObject == null)
                        showError(ResourceManager.getString(R.string.please_select_image_or_video));
                    else
                        addOfferViewModel.getOfferRepository().addOffer(addOfferViewModel.addOfferRequest);
                } else if (action.equals(Constants.IMAGE)) {
                    pickImageDialogSelect();
                } else if (action.equals(Constants.VIDEO)) {
                    pickVideoDialogSelect();
                } else if (action.equals(Constants.SUCCESS)) {
                    toastMessage(addOfferViewModel.getOfferRepository().getMessage(), R.drawable.ic_check_white, R.color.colorPrimary);
                    reloadPrevious();
                }
            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        //super method removed
        Timber.e("onActivityResult:" + requestCode);
        if (requestCode == Constants.FILE_TYPE_IMAGE) {
            VolleyFileObject volleyFileObject1 = FileOperations.getVolleyFileObject(getActivity(), data, Constants.URL, Constants.FILE_TYPE_IMAGE);
            File file = new File(getCacheDir(), Constants.IMAGE + ".png");
            UCrop.of(Uri.fromFile(volleyFileObject1.getFile()), Uri.fromFile(file))
                    .start(context, AddOfferFragment.this);
        } else if (requestCode == UCrop.REQUEST_CROP && data != null) {
            final Uri resultUri = UCrop.getOutput(data);
            if (resultUri != null) {
                VolleyFileObject volleyFileObject = new VolleyFileObject(Constants.URL, resultUri.getPath(), Constants.FILE_TYPE_IMAGE);
                addOfferViewModel.addOfferRequest.type = 1;
                addOfferViewModel.addOfferRequest.volleyFileObject = volleyFileObject;
                fragmentAddOfferBinding.tvFilePath.setText(ResourceManager.getString(R.string.your_image_had_been_selected));
            }
        }
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();
                UCrop.of(resultUri, resultUri).start(context, AddOfferFragment.this);
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Toast.makeText(context, ""+getString(R.string.error_failed_pick_gallery_image), Toast.LENGTH_SHORT).show();
            }
        }

        //capture
        if (requestCode == Constants.FILE_TYPE_VIDEO) {
            VolleyFileObject volleyFileObject = FileOperations.getVolleyFileObject(getActivity(), data, Constants.URL, Constants.FILE_TYPE_VIDEO);

            MediaMetadataRetriever retriever = new MediaMetadataRetriever();
//use one of overloaded setDataSource() functions to set your data source
            retriever.setDataSource(context, Uri.fromFile(volleyFileObject.getFile()));
            String time = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
            long timeInMillisec = Long.parseLong(time);
            int duration = (int) (timeInMillisec / 1000);
            Timber.e("duration:" + duration);
            Timber.e("file length:" + volleyFileObject.getFile().length());
            long fileSize = Integer.parseInt(String.valueOf((volleyFileObject.getFile().length() / 1024) / 1024));
            if (duration <= 30) {
                addOfferViewModel.addOfferRequest.type = 2;
                addOfferViewModel.addOfferRequest.volleyFileObject = volleyFileObject;
                fragmentAddOfferBinding.tvFilePath.setText(ResourceManager.getString(R.string.your_video_had_been_selected));
            } else {
                toastInfo(getString(R.string.maximum_video_duration_sixty_second));
            }


        }
        super.onActivityResult(requestCode, resultCode, data);
    }

}
