package grand.app.moonshop.views.fragments.reservation.beauty;


import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.theartofdev.edmodo.cropper.CropImage;
import com.yalantis.ucrop.UCrop;

import java.io.File;

import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moonshop.R;
import grand.app.moonshop.base.BaseFragment;
import grand.app.moonshop.databinding.FragmentBeautyAddServiceBinding;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.utils.upload.FileOperations;
import grand.app.moonshop.viewmodels.reservation.beauty.BeautyAddServiceViewModel;
import grand.app.moonshop.vollyutils.VolleyFileObject;
import timber.log.Timber;

import static android.app.Activity.RESULT_OK;
import static com.facebook.FacebookSdk.getCacheDir;

/**
 * A simple {@link Fragment} subclass.
 */
public class BeautyAddServiceFragment extends BaseFragment {

    private View rootView;
    private FragmentBeautyAddServiceBinding fragmentBeautyAddServiceBinding;
    private BeautyAddServiceViewModel beautyAddServiceViewModel;

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentBeautyAddServiceBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_beauty_add_service, container, false);
        bind();
        setEvent();
        return rootView;
    }

    private void bind() {
        beautyAddServiceViewModel = new BeautyAddServiceViewModel();
        fragmentBeautyAddServiceBinding.setBeautyAddServiceViewModel(beautyAddServiceViewModel);
        rootView = fragmentBeautyAddServiceBinding.getRoot();
    }

    private void setEvent() {
        beautyAddServiceViewModel.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                String action = (String) o;
                handleActions(action, beautyAddServiceViewModel.getReservationRepository().getMessage());
                if (action.equals(Constants.SUCCESS)) {
                    toastMessage(beautyAddServiceViewModel.getReservationRepository().getMessage());
                    Intent intent = new Intent();
                    getActivityBase().setResult(RESULT_OK, intent);
                    getActivityBase().finish();
                } else if (action.equals(Constants.SELECT_IMAGE)) {
                    pickImageDialogSelect();
                } else if (action.equals(Constants.ERROR)) {
                    showError(beautyAddServiceViewModel.baseError);
                }
            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        //super method removed
        Timber.e("onActivityResult:" + requestCode);
        if (requestCode == Constants.FILE_TYPE_IMAGE) {
            VolleyFileObject volleyFileObject1 = FileOperations.getVolleyFileObject(getActivity(), data, Constants.IMAGE, Constants.FILE_TYPE_IMAGE);
            File file = new File(getCacheDir(), Constants.IMAGE + ".png");
            UCrop.of(Uri.fromFile(volleyFileObject1.getFile()), Uri.fromFile(file))
                    .start(context, BeautyAddServiceFragment.this);
        } if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE && data != null) {

            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();
                UCrop.of(resultUri, resultUri).start(context, BeautyAddServiceFragment.this);
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Toast.makeText(context, "" + getString(R.string.error_failed_pick_gallery_image), Toast.LENGTH_SHORT).show();
            }
        }
        else if (requestCode == UCrop.REQUEST_CROP && data != null) {
            final Uri resultUri = UCrop.getOutput(data);
            if (resultUri != null) {
                beautyAddServiceViewModel.volleyFileObject = new VolleyFileObject(Constants.IMAGE, resultUri.getPath(), Constants.FILE_TYPE_IMAGE);
                fragmentBeautyAddServiceBinding.appCompatImageView.setImageURI(null);
                fragmentBeautyAddServiceBinding.appCompatImageView.setImageURI(Uri.parse(String.valueOf(new File(beautyAddServiceViewModel.volleyFileObject.getFilePath()))));
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        beautyAddServiceViewModel.reset();

    }
}
