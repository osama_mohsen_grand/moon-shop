package grand.app.moonshop.views.fragments.product;


import android.app.Activity;
import android.app.ActivityOptions;
import android.app.Dialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moonshop.R;
import grand.app.moonshop.base.BaseFragment;
import grand.app.moonshop.customviews.dialog.DialogConfirm;
import grand.app.moonshop.databinding.FragmentProductBinding;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.utils.dialog.DialogHelperInterface;
import grand.app.moonshop.utils.resources.ResourceManager;
import grand.app.moonshop.utils.storage.user.UserHelper;
import grand.app.moonshop.viewmodels.product.ProductViewModel;
import grand.app.moonshop.views.activities.BaseActivity;
import timber.log.Timber;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProductFragment extends BaseFragment {

    View rootView;
    private FragmentProductBinding fragmentProductBinding;
    private ProductViewModel productViewModel;
    public int id = -1;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        fragmentProductBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_product, container, false);
        getData();
        bind();
        setEvent();
        return rootView;
    }


    private void getData() {
        if (getArguments().containsKey(Constants.ID))
            id = getArguments().getInt(Constants.ID);
    }


    private void bind() {
        productViewModel = new ProductViewModel(id);
        fragmentProductBinding.setProductViewModel(productViewModel);
        rootView = fragmentProductBinding.getRoot();
    }

    private void setEvent() {
        productViewModel.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                String action = (String) o;
                handleActions(action, productViewModel.getProductRepository().getMessage());
                assert action != null;
                Intent intent = new Intent(context, BaseActivity.class);
                if (action.equals(Constants.PRODUCT)) {
                    productViewModel.setPrice();
                    productViewModel.showPage(true);
                } else if (action.equals(Constants.ZOOM)) {
                    intent.putExtra(Constants.PAGE, Constants.ZOOM);
                    Bundle bundle = new Bundle();
                    bundle.putString(Constants.IMAGE, productViewModel.getProductRepository().getProductDetailsResponse().mData.image);
                    intent.putExtra(Constants.BUNDLE, bundle);
                    ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(getActivity(), fragmentProductBinding.imgProductDetails, Constants.IMAGE);
                    startActivity(intent, options.toBundle());
                } else if (action.equals(Constants.EDIT)) {
                    if (UserHelper.getUserDetails().shopType.equals(Constants.TYPE_SECTORAL))
                        intent.putExtra(Constants.PAGE, Constants.ADD_PRODUCT_SECTORAL);
                    else if (UserHelper.getUserDetails().shopType.equals(Constants.TYPE_CONSUMER) ||
                            UserHelper.getUserDetails().shopType.equals(Constants.TYPE_CONSUMER_SECTORAL))
                        intent.putExtra(Constants.PAGE, Constants.ADD_PRODUCT_CONSUMER_SECTORAL);
                    Bundle bundle = new Bundle();
                    bundle.putSerializable(Constants.PRODUCT, productViewModel.getProductRepository().getProductDetailsResponse().mData);
                    bundle.putInt(Constants.ID, productViewModel.getProductRepository().getProductDetailsResponse().mData.mCategoryId);
                    intent.putExtra(Constants.BUNDLE, bundle);
                    startActivityForResult(intent, Constants.RELOAD_RESULT);
                } else if (action.equals(Constants.DELETE)) {
                    new DialogConfirm(getActivity())
                            .setTitle(ResourceManager.getString(R.string.remove_product))
                            .setMessage(ResourceManager.getString(R.string.do_you_want_delete_product))
                            .setActionText(ResourceManager.getString(R.string.remove_item))
                            .setActionCancel(ResourceManager.getString(R.string.cancel))
                            .show(new DialogHelperInterface() {
                                @Override
                                public void OnClickListenerContinue(Dialog dialog, View view) {
                                    productViewModel.delete(id);
                                }
                            });
                }else if(action.equals(Constants.DELETED)){
                    reloadPrevious();
                }

            }
        });

    }


//    private boolean isEdit = false;

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        //super method removed
        Timber.e("onActivityResult:" + requestCode);
        if (resultCode == Activity.RESULT_OK && requestCode == Constants.RELOAD_RESULT) {
//            productViewModel.getProductRepository().getProductDetails(id);
//            isEdit = true;
            reloadPrevious();
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
}

