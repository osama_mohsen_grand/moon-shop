package grand.app.moonshop.views.activities;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ActivityManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;

import com.google.android.material.badge.BadgeDrawable;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.play.core.install.model.AppUpdateType;
import com.google.android.play.core.install.model.UpdateAvailability;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;

import grand.app.moonshop.R;
import grand.app.moonshop.base.BaseFragment;
import grand.app.moonshop.base.ParentActivity;
import grand.app.moonshop.customviews.actionbar.HomeActionBarView;
import grand.app.moonshop.customviews.menu.NavigationDrawerView;
import grand.app.moonshop.databinding.ActivityMainBinding;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.utils.LanguagesHelper;
import grand.app.moonshop.utils.MovementHelper;
import grand.app.moonshop.utils.dialog.DialogHelper;
import grand.app.moonshop.utils.storage.user.UserHelper;
import grand.app.moonshop.utils.updateApp.ImmediateUpdateActivity;
import grand.app.moonshop.views.fragments.category.HomeShopFragment;
import grand.app.moonshop.views.fragments.chat.ChatListFragment;
import grand.app.moonshop.views.fragments.common.CreditFragment;
import grand.app.moonshop.views.fragments.notification.NotificationFragment;
import grand.app.moonshop.views.fragments.status.StatusFragment;
import grand.app.moonshop.vollyutils.MyApplication;

import static grand.app.moonshop.utils.updateApp.ImmediateUpdateActivity.UPDATE_REQUEST_CODE;

public class MainActivity extends ParentActivity {
    public HomeActionBarView homeActionBarView = null;
    public NavigationDrawerView navigationDrawerView;
    public String message = "";

    ImmediateUpdateActivity immediateUpdateActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        immediateUpdateActivity = new ImmediateUpdateActivity(this);

//        checkForUpdate();
        LanguagesHelper.changeLanguage(this, LanguagesHelper.getCurrentLanguage());
        LanguagesHelper.changeLanguage(MyApplication.getInstance(), LanguagesHelper.getCurrentLanguage());
        initializeToken();
        initFacebook();
        bind();
        getData();
        setEvents();
        setBottomMenu();


    }

    private void getData() {
        if (getIntent() != null && getIntent().hasExtra(Constants.MESSAGE)) {
            message = getIntent().getStringExtra(Constants.MESSAGE);
//            new OrderCodeViewModel(this,message);
        }
    }


    ActivityMainBinding activityMainBinding;

    private View bind() {
        activityMainBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        homeActionBarView = new HomeActionBarView(this);
        navigationDrawerView = new NavigationDrawerView(this);
        activityMainBinding.llBaseContainer.addView(navigationDrawerView);
        navigationDrawerView.layoutNavigationDrawerBinding.llBaseActionBarContainer.addView(homeActionBarView, 0);
        homeActionBarView.setNavigation(navigationDrawerView);
        homeActionBarView.setScroll(navigationDrawerView.layoutNavigationDrawerBinding.svMenu);
        homeActionBarView.connectDrawer(navigationDrawerView.layoutNavigationDrawerBinding.dlMainNavigationMenu, true);
        navigationDrawerView.setActionBar(homeActionBarView);
        if (getIntent().hasExtra(Constants.PAGE)) {
            Log.d(TAG, "homePage Home");
            if (getIntent().getStringExtra(Constants.PAGE).equals(Constants.RESERVATION))
                navigationDrawerView.reservationPage();
            else if (getIntent().getStringExtra(Constants.PAGE).equals(Constants.MY_ADS)) {
                navigationDrawerView.myAdsPage();
            } else if (getIntent().getStringExtra(Constants.PAGE).equals(Constants.STORY)) {
                navigationDrawerView.storyPage();
            } else
                navigationDrawerView.homePage();
        } else {
            navigationDrawerView.homePage();
        }
        navigationDrawerView.getAdapter().notifyDataSetChanged();


        return activityMainBinding.getRoot();
    }


    private void setEvents() {
        navigationDrawerView.mutableLiveData.observe(this, new Observer<Object>() {
            @SuppressLint("WrongConstant")
            @Override
            public void onChanged(@Nullable Object object) {
                if (object instanceof String) {
                    String text = (String) object;
                    if (text.equals(Constants.LOGOUT)) {
                        logoutApp();
                    } else {
                        homeActionBarView.setTitle(text);
                        navigationDrawerView.layoutNavigationDrawerBinding.dlMainNavigationMenu.closeDrawer(Gravity.START);
                    }
                } else if (object instanceof Boolean) {
                    //profile
                    Intent intent = new Intent(MainActivity.this, BaseActivity.class);
                    intent.putExtra(Constants.PAGE, Constants.PROFILE);
                    startActivityForResult(intent, Constants.RESULT_PROFILE_RESPONSE);
                }
            }
        });
    }


    @Override
    public void onBackPressed() {

        ActivityManager mngr = (ActivityManager) getSystemService(ACTIVITY_SERVICE);

        List<ActivityManager.RunningTaskInfo> taskList = mngr.getRunningTasks(10);

        if (taskList.get(0).numActivities == 1 &&
                taskList.get(0).topActivity.getClassName().equals(this.getClass().getName())) {
            if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
                super.onBackPressed();
                return;
            }
            BaseFragment baseFragment = (BaseFragment) getSupportFragmentManager().findFragmentByTag(Constants.HOME);
            if (baseFragment != null) {
                int[] ids = {R.id.tv_dialog_close_app_yes, R.id.tv_dialog_close_app_no};
                DialogHelper.showDialogHelper(MainActivity.this, R.layout.dialog_close_app, ids, (dialog, view) -> {
                    switch (view.getId()) {
                        case R.id.tv_dialog_close_app_yes:
                            dialog.dismiss();
                            finish();
                            break;
                        case R.id.tv_dialog_close_app_no:
                            dialog.dismiss();
                            break;

                    }
                });
            } else {
                Log.d(TAG, "homePage back");
                navigationDrawerView.homePage();
            }
            return;

        } else
            finish();

    }

    private static final String TAG = "MainActivity";

    public void chatCount() {
        if(UserHelper.getUserDetails().chats_count != 0){
            activityMainBinding.bnvMainBottom.getOrCreateBadge(R.id.bottomNavigationChat)
                    .setNumber(UserHelper.getUserDetails().chats_count);
        }else {
            activityMainBinding.bnvMainBottom.removeBadge(R.id.bottomNavigationChat);
        }
    }



//    private void notificationCount() {
//        if (UserHelper.getUserId() != -1)
//            navigationDrawerView.updateNotificationCount();
//    }

    private void orderCount() {
        if (UserHelper.getUserId() != -1)
            navigationDrawerView.updateOrdersCount();
    }

    public void logoutApp() {
        int[] ids = {R.id.tv_dialog_close_app_yes, R.id.tv_dialog_close_app_no};
        DialogHelper.showDialogHelper(MainActivity.this, R.layout.dialog_logout_app, ids, (dialog, view) -> {
            switch (view.getId()) {
                case R.id.tv_dialog_close_app_yes:
                    dialog.dismiss();
                    finishAffinity();
                    UserHelper.clearUserDetails();
                    Intent intent = new Intent(MainActivity.this, BaseActivity.class);
                    intent.putExtra(Constants.PAGE, Constants.LOGIN);
                    startActivity(intent);
                    break;
                case R.id.tv_dialog_close_app_no:
                    dialog.dismiss();
                    break;

            }
        });
    }

    private void setBottomNavigationCounts(int bottomNavigationItemId, int count) {

        BadgeDrawable badgeDrawable = activityMainBinding.bnvMainBottom.getOrCreateBadge(bottomNavigationItemId);
        badgeDrawable.setVisible(count > 0);
        badgeDrawable.setNumber(count);
        badgeDrawable.setBadgeGravity(BadgeDrawable.TOP_END);
    }

    private void setBottomMenu() {


        activityMainBinding.bnvMainBottom.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                homeActionBarView.setTitle(item.getTitle() + "");

                if (item.getItemId() == R.id.bottomNavigationHomeId) {
                    MovementHelper.replaceFragmentTag(MainActivity.this, new HomeShopFragment(), Constants.HOME, "");
                    MovementHelper.popAllFragments(MainActivity.this);

                } else if (item.getItemId() == R.id.bottomNavigationChat) {
                    MovementHelper.replaceFragmentTag(MainActivity.this, new ChatListFragment(), Constants.CHAT_LIST, "");
                    MovementHelper.popAllFragments(MainActivity.this);
                } else if (item.getItemId() == R.id.bottomNavigationStory) {
                    MovementHelper.replaceFragmentTag(MainActivity.this, new StatusFragment(), Constants.CREDIT, "");
                    MovementHelper.popAllFragments(MainActivity.this);
                } else if (item.getItemId() == R.id.bottomNavigationBalanceId) {
                    MovementHelper.replaceFragmentTag(MainActivity.this, new CreditFragment(), Constants.STATUS, "");
                    MovementHelper.popAllFragments(MainActivity.this);
                } else if (item.getItemId() == R.id.bottomNavigationNotificationId) {
                    MovementHelper.replaceFragmentTag(MainActivity.this, new NotificationFragment(), Constants.NOTIFICATION, "");
                    MovementHelper.popAllFragments(MainActivity.this);
                }

                return true;
            }
        });
        if (UserHelper.getUserId() != -1) {

            setBottomNavigationCounts(R.id.bottomNavigationNotificationId, UserHelper.getUserDetails().notifications_count);
            setBottomNavigationCounts(R.id.bottomNavigationChat, UserHelper.getUserDetails().chats_count);

        }
    }

    private BroadcastReceiver notifyChat = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            chatCount();
        }
    };

    private BroadcastReceiver notifyOrder = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            navigationDrawerView.updateOrdersCount();
        }
    };

    @Override
    public void onPause() {
        super.onPause();
        unregisterReceiver(notifyChat);
        unregisterReceiver(notifyOrder);
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.e(TAG, "onResume: ");
        LanguagesHelper.changeLanguage(this, LanguagesHelper.getCurrentLanguage());
        LanguagesHelper.changeLanguage(MyApplication.getInstance(), LanguagesHelper.getCurrentLanguage());
        registerReceiver(notifyChat, new IntentFilter(Constants.CHAT));
        registerReceiver(notifyOrder, new IntentFilter(Constants.ORDER));
        chatCount();
//        notificationCount();
        orderCount();
        updateAuto();
    }

    private void updateAuto() {
        immediateUpdateActivity.getAppUpdateManager().getAppUpdateInfo().addOnSuccessListener(it -> {
            if (it.updateAvailability() == UpdateAvailability.DEVELOPER_TRIGGERED_UPDATE_IN_PROGRESS) {
                try {
                    immediateUpdateActivity.getAppUpdateManager().startUpdateFlowForResult(it, AppUpdateType.IMMEDIATE, this, UPDATE_REQUEST_CODE);
                } catch (IntentSender.SendIntentException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d(TAG, "requestCode:" + requestCode + ",result:" + resultCode);
        if (requestCode == Constants.RESULT_PROFILE_RESPONSE) {
            if (resultCode == Activity.RESULT_OK) {
                navigationDrawerView.setHeader();
            }
        }
        if (requestCode == UPDATE_REQUEST_CODE && resultCode == Activity.RESULT_CANCELED) {
            finish();
            Log.e(TAG, "onActivityResult: app download failed");
        }
    }//onActivityResult

}
