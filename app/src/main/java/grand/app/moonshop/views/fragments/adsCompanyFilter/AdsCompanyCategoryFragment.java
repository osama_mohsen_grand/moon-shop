package grand.app.moonshop.views.fragments.adsCompanyFilter;


import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moonshop.R;
import grand.app.moonshop.adapter.AdsCompanyFilterAdapter;
import grand.app.moonshop.base.BaseFragment;
import grand.app.moonshop.databinding.FragmentAdsCompanyFilterBinding;
import grand.app.moonshop.models.adsCompanyFilter.AdsCompanyFilter;
import grand.app.moonshop.models.adsCompanyFilter.AdsCompanyFilterResponse;
import grand.app.moonshop.utils.helper.AppUtils;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.utils.resources.ResourceManager;
import grand.app.moonshop.viewmodels.adsCompanyFilter.AdsCompanyFilterViewModel;
import grand.app.moonshop.views.activities.BaseActivity;
import timber.log.Timber;

/**
 * A simple {@link Fragment} subclass.
 */
public class AdsCompanyCategoryFragment extends BaseFragment {


    View rootView;
    private FragmentAdsCompanyFilterBinding fragmentAdsCompanyFilterBinding;
    private AdsCompanyFilterViewModel adsCompanyFilterViewModel;
    private AdsCompanyFilterAdapter adsCompanyFilterAdapter;
    AdsCompanyFilterResponse adsCompanyFilterResponse = null;
    int service_id = -1, category_id = -1, position = -1;
    public String from = "";
    public boolean multiple_choice = false;

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentAdsCompanyFilterBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_ads_company_filter, container, false);
        getData();
        bind();
        rootView = fragmentAdsCompanyFilterBinding.getRoot();
        return rootView;
    }

    private void getData() {
        if (getArguments() != null && getArguments().containsKey(Constants.ID))
            service_id = getArguments().getInt(Constants.ID);
        if (getArguments() != null && getArguments().containsKey(Constants.FROM))
            from = getArguments().getString(Constants.FROM);
        if (getArguments() != null && getArguments().containsKey(Constants.CATEGORY_ID))
            category_id = getArguments().getInt(Constants.CATEGORY_ID);
        if (getArguments() != null && getArguments().containsKey(Constants.POSITION))
            position = getArguments().getInt(Constants.POSITION);
        if (getArguments() != null && getArguments().containsKey(Constants.ADS_COMPANY_FILTER))
            adsCompanyFilterResponse = (AdsCompanyFilterResponse) getArguments().getSerializable(Constants.ADS_COMPANY_FILTER);
    }

    private void bind() {
        AppUtils.initVerticalRV(fragmentAdsCompanyFilterBinding.rvFilter, fragmentAdsCompanyFilterBinding.rvFilter.getContext(), 1);
        if (category_id == -1) {
            adsCompanyFilterViewModel = new AdsCompanyFilterViewModel(service_id);
        } else {
            adsCompanyFilterViewModel = new AdsCompanyFilterViewModel();
            setAdapter(adsCompanyFilterResponse.data.get(position).subCategory);
        }
        setEvents();
        fragmentAdsCompanyFilterBinding.setAdsCompanyFilterViewModel(adsCompanyFilterViewModel);


    }

    private void setEvents() {
        adsCompanyFilterViewModel.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                String action = (String) o;
                handleActions(action, adsCompanyFilterViewModel.getAdvertisementCompanyRepository().getMessage());
                assert action != null;
                if (action.equals(Constants.SUCCESS)) {
                    adsCompanyFilterResponse = adsCompanyFilterViewModel.getAdvertisementCompanyRepository().getAdsCompanyFilterResponse();
                    setAdapter(adsCompanyFilterResponse.data);
                }
            }
        });
    }

    public void setAdapter(List<AdsCompanyFilter> adsCompanyFilters) {
        adsCompanyFilterAdapter = new AdsCompanyFilterAdapter(adsCompanyFilters,multiple_choice);
        adsCompanyFilterViewModel.showPage(true);
        fragmentAdsCompanyFilterBinding.rvFilter.setAdapter(adsCompanyFilterAdapter);
        setEventAdapter();
    }

    private void setEventAdapter() {
        adsCompanyFilterAdapter.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                int pos = (int) o;
                Intent intent = new Intent(context, BaseActivity.class);
                intent.putExtra(Constants.NAME_BAR, ResourceManager.getString(R.string.service));
                Bundle bundle = getArguments() != null ? getArguments() : new Bundle();
                intent.putExtra(Constants.PAGE, Constants.ADS_COMPANY_SUB_CATEGORY);
                category_id = adsCompanyFilterResponse.data.get(pos).id;
                bundle.putInt(Constants.CATEGORY_ID, category_id);
                bundle.putInt(Constants.POSITION, pos);
                bundle.putSerializable(Constants.ADS_COMPANY_FILTER, adsCompanyFilterResponse);
                bundle.putBoolean(Constants.MULTIPLE_CHOICE, true);
                intent.putExtra(Constants.BUNDLE, bundle);
                getActivityBase().setResult(Constants.FILTER_RESULT, intent);
                startActivityForResult(intent, Constants.FILTER_RESULT);
            }
        });
    }



    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        //super method removed
        if (requestCode == Constants.FILTER_RESULT && resultCode == Constants.FILTER_RESULT) {
            Timber.e("CallService");
            Timber.e("cat:"+data.getIntExtra(Constants.CATEGORY_ID,-1));
            Timber.e("sub_cat:"+data.getIntExtra(Constants.SUB_CATEGORY_ID,-1));

//            ArrayList<Integer> sub_categories = data.getIntegerArrayListExtra(Constants.SUB_CATEGORY_ID);

            Intent intent = new Intent();
            intent.putExtra(Constants.CATEGORY_ID, data.getIntExtra(Constants.CATEGORY_ID,-1));
            intent.putExtra(Constants.SUB_CATEGORY_ID, data.getIntegerArrayListExtra(Constants.SUB_CATEGORY_ID));
            getActivityBase().setResult(Constants.FILTER_RESULT, intent);
            getActivityBase().finish();//finishing activity

//            companiesDetailsViewModel.category_id = data.getIntExtra(Constants.CATEGORY_ID,-1);
//            companiesDetailsViewModel.sub_category_id = data.getIntExtra(Constants.SUB_CATEGORY_ID,-1);
//            resetAdapter();
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        adsCompanyFilterViewModel.reset();
    }
}
