package grand.app.moonshop.views.fragments.shop;


import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moonshop.R;
import grand.app.moonshop.adapter.InstitutionServicesAdapter;
import grand.app.moonshop.base.BaseFragment;
import grand.app.moonshop.customviews.dialog.DialogConfirm;
import grand.app.moonshop.databinding.FragmentInfoServiceDetailsBinding;
import grand.app.moonshop.models.app.Mutable;
import grand.app.moonshop.utils.helper.AppUtils;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.utils.dialog.DialogHelperInterface;
import grand.app.moonshop.utils.resources.ResourceManager;
import grand.app.moonshop.viewmodels.shop.InfoServiceViewModel;
import grand.app.moonshop.views.activities.BaseActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class InfoServiceDetailsFragment extends BaseFragment {

    FragmentInfoServiceDetailsBinding binding;
    InfoServiceViewModel viewModel;
    private int id;
    InstitutionServicesAdapter institutionServicesAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_info_service_details, container, false);
        bind();
        setEvent();
        AppUtils.initVerticalRV(binding.rvServiceInstitution, binding.rvServiceInstitution.getContext(), 1);
        return binding.getRoot();
    }


    public void bind() {
        viewModel = new InfoServiceViewModel();
        binding.setInfoServiceViewModel(viewModel);
    }

    private void setEvent() {
        viewModel.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                String action = (String) o;
                assert action != null;
                handleActions(action, viewModel.institutionRepository.getMessage());
                if(action.equals(Constants.SUCCESS)){
                    institutionServicesAdapter = new InstitutionServicesAdapter(viewModel.institutionRepository.shopDetailsResponse.shopData.categories,true);
                    binding.rvServiceInstitution.setAdapter(institutionServicesAdapter);
                    setEventAdapter();
                    viewModel.showPage(true);
                }
                else if (action.equals(Constants.ADD_SERVICE)) {
                    Intent intent = new Intent(context, BaseActivity.class);
                    intent.putExtra(Constants.PAGE,Constants.ADD_SERVICE);
                    Bundle bundle = new Bundle();
                    bundle.putSerializable(Constants.SHOP_DETAILS,viewModel.institutionRepository.shopDetailsResponse.shopData);
                    intent.putExtra(Constants.BUNDLE,bundle);
                    startActivity(intent);
                }else if (action.equals(Constants.DELETE)) {
                    toastMessage(viewModel.institutionRepository.getMessage(), R.drawable.ic_check_white, R.color.colorPrimary);
                    institutionServicesAdapter.remove(viewModel.service_delete_position);
                }else if (action.equals(Constants.DESCRIPTION_SUCCESS)) {
                    toastMessage(viewModel.institutionRepository.getMessage(), R.drawable.ic_check_white, R.color.colorPrimary);
                }
            }
        });
    }

    private void setEventAdapter() {

        institutionServicesAdapter.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                Mutable mutable = (Mutable) o;
                int position = mutable.position;
                if(mutable.type.equals(Constants.DELETE)) {
                    new DialogConfirm(getActivity())
                            .setTitle(ResourceManager.getString(R.string.remove_service))
                            .setMessage(ResourceManager.getString(R.string.do_you_want_delete_service))
                            .setActionText(ResourceManager.getString(R.string.remove_item))
                            .setActionCancel(ResourceManager.getString(R.string.cancel))
                            .show(new DialogHelperInterface() {
                                @Override
                                public void OnClickListenerContinue(Dialog dialog, View view) {
                                    viewModel.service_delete_position = position;
                                    viewModel.delete(viewModel.institutionRepository.shopDetailsResponse.shopData.categories.get(position).id);
                                }
                            });
                }else if(mutable.type.equals(Constants.EDIT)){
                    Intent intent = new Intent(context, BaseActivity.class);
                    intent.putExtra(Constants.PAGE,Constants.ADD_SERVICE);
                    Bundle bundle = new Bundle();
                    bundle.putSerializable(Constants.SHOP_DETAILS,viewModel.institutionRepository.shopDetailsResponse.shopData);
                    bundle.putSerializable(Constants.CATEGORY,viewModel.institutionRepository.shopDetailsResponse.shopData.categories.get(position));
                    intent.putExtra(Constants.BUNDLE,bundle);
                    startActivity(intent);
                }
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        if(isReloadPage()) {
            viewModel.institutionRepository.getService();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (viewModel != null)
            viewModel.reset();
    }



}
