package grand.app.moonshop.views.fragments.reservation.clinic;


import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.theartofdev.edmodo.cropper.CropImage;
import com.yalantis.ucrop.UCrop;

import java.io.File;
import java.util.ArrayList;

import androidx.lifecycle.LifecycleOwner;
import grand.app.moonshop.R;
import grand.app.moonshop.base.BaseFragment;
import grand.app.moonshop.databinding.FragmentAddDoctorBinding;
import grand.app.moonshop.databinding.FragmentProfileBinding;
import grand.app.moonshop.models.app.AppMoon;
import grand.app.moonshop.models.base.IdName;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.utils.PopUp.PopUpInterface;
import grand.app.moonshop.utils.PopUp.PopUpMenuHelper;
import grand.app.moonshop.utils.images.ImageLoaderHelper;
import grand.app.moonshop.utils.resources.ResourceManager;
import grand.app.moonshop.utils.storage.user.UserHelper;
import grand.app.moonshop.utils.upload.FileOperations;
import grand.app.moonshop.viewmodels.reservation.doctors.AddDoctorViewModel;
import grand.app.moonshop.viewmodels.user.ProfileViewModel;
import grand.app.moonshop.views.activities.BaseActivity;
import grand.app.moonshop.views.activities.MainActivity;
import grand.app.moonshop.views.activities.MapAddressActivity;
import grand.app.moonshop.vollyutils.VolleyFileObject;
import timber.log.Timber;

import static android.app.Activity.RESULT_OK;
import static com.facebook.FacebookSdk.getCacheDir;

/**
 * A simple {@link Fragment} subclass.
 */
public class AddDoctorFragment extends BaseFragment {
    View rootView;
    private FragmentAddDoctorBinding fragmentAddDoctorBinding;
    private AddDoctorViewModel addDoctorViewModel;
    public String type = "";
    public PopUpMenuHelper popUpMenuHelper = new PopUpMenuHelper();

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentAddDoctorBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_add_doctor, container, false);
        bind();
        setEvent();
        return rootView;
    }


    private void bind() {
        addDoctorViewModel = new AddDoctorViewModel();
        fragmentAddDoctorBinding.setAddDoctorViewModel(addDoctorViewModel);
        rootView = fragmentAddDoctorBinding.getRoot();
    }


    private void setEvent() {
        addDoctorViewModel.mMutableLiveData.observe((LifecycleOwner) context, o -> {
            String action = (String) o;
            handleActions(action, addDoctorViewModel.getReservationRepository().getMessage());
            Timber.e(action);
            if (action.equals(Constants.SUCCESS)) {//submitSearch register
                toastMessage(addDoctorViewModel.getReservationRepository().getMessage(), R.drawable.ic_check_white, R.color.colorPrimary);
                Intent intent = new Intent();
                getActivityBase().setResult(RESULT_OK, intent);
                getActivityBase().finish();
            } else if (action.equals(Constants.SELECT_IMAGE)) {//select image profile
                pickImageDialogSelect();
            } else if (action.equals(Constants.SPECIFICATION)) {
                addDoctorViewModel.showPage(true);
            } else if (action.equals(Constants.ERROR))
                showError(addDoctorViewModel.baseError);
        });
        fragmentAddDoctorBinding.edtAddDoctorSpecification.setOnClickListener(view -> {
            ArrayList<String> popupSpecification = new ArrayList<>();
            for (IdName idName : addDoctorViewModel.getReservationRepository().getSpecificationResponse().specifications)
                popupSpecification.add(idName.name);
            popUpMenuHelper.openPopUp(getActivity(), fragmentAddDoctorBinding.edtAddDoctorSpecification, popupSpecification, new PopUpInterface() {
                @Override
                public void submitPopUp(int position) {
                    fragmentAddDoctorBinding.edtAddDoctorSpecification.setText(popupSpecification.get(position));
                    addDoctorViewModel.addDoctorRequest.setSpecialties_id(addDoctorViewModel.getReservationRepository().getSpecificationResponse().specifications.get(position).id);
                }
            });
        });
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        //super method removed
        Timber.e("onActivityResult:" + requestCode);
        if (requestCode == Constants.FILE_TYPE_IMAGE) {
            VolleyFileObject volleyFileObject1 = FileOperations.getVolleyFileObject(getActivity(), data, Constants.IMAGE, Constants.FILE_TYPE_IMAGE);
            File file = new File(getCacheDir(), Constants.IMAGE + ".png");
            UCrop.of(Uri.fromFile(volleyFileObject1.getFile()), Uri.fromFile(file))
                    .start(context, AddDoctorFragment.this);

//            fragmentAddDoctorBinding.imgProfileUser.setImageBitmap(volleyFileObject.getBitmap());
        }else if (requestCode == UCrop.REQUEST_CROP && data != null) {
            final Uri resultUri = UCrop.getOutput(data);
            if (resultUri != null) {
                VolleyFileObject volleyFileObject = new VolleyFileObject(Constants.IMAGE, resultUri.getPath(), Constants.FILE_TYPE_IMAGE);
                addDoctorViewModel.setImage(volleyFileObject);
                fragmentAddDoctorBinding.imgDoctor.setImageURI(null);
                fragmentAddDoctorBinding.imgDoctor.setImageURI(Uri.parse(String.valueOf(new File(volleyFileObject.getFilePath()))));
            }
        }if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE && data != null) {

            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();
                UCrop.of(resultUri, resultUri).start(context, AddDoctorFragment.this);
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Toast.makeText(context, "" + getString(R.string.error_failed_pick_gallery_image), Toast.LENGTH_SHORT).show();
            }
        }

        super.onActivityResult(requestCode, resultCode, data);
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        addDoctorViewModel.reset();

    }

}
