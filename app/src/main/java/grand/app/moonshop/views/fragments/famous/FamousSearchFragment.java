package grand.app.moonshop.views.fragments.famous;


import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moonshop.R;
import grand.app.moonshop.adapter.FamousAdapter;
import grand.app.moonshop.base.BaseFragment;
import grand.app.moonshop.databinding.FragmentFamousSearchBinding;
import grand.app.moonshop.models.famous.list.Famous;
import grand.app.moonshop.utils.helper.AppUtils;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.utils.storage.user.UserHelper;
import grand.app.moonshop.viewmodels.search.FamousSearchViewModel;
import grand.app.moonshop.views.activities.BaseActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class FamousSearchFragment extends BaseFragment {

    private FragmentFamousSearchBinding fragmentFamousSearchBinding;
    public FamousSearchViewModel famousSearchViewModel;
    FamousAdapter famousAdapter;
    int type = 4;

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        UserHelper.saveKey(Constants.RELOAD,Constants.FALSE);
        fragmentFamousSearchBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_famous_search, container, false);
        getData();
        bind();
        setEvent();
        return fragmentFamousSearchBinding.getRoot();
    }

    private void getData() {
        if (getArguments() != null && getArguments().containsKey(Constants.TYPE))
            type = getArguments().getInt(Constants.TYPE);
    }


    private void bind() {
        famousSearchViewModel = new FamousSearchViewModel(type);
        AppUtils.initVerticalRV(fragmentFamousSearchBinding.rvFamous, fragmentFamousSearchBinding.rvFamous.getContext(), 3);
        fragmentFamousSearchBinding.setFamousSearchViewModel(famousSearchViewModel);
    }

    @Override
    public void onResume() {
        super.onResume();
        if(UserHelper.retrieveKey(Constants.RELOAD).equals(Constants.TRUE) && famousSearchViewModel != null) {
            famousSearchViewModel.submitSearch();
            UserHelper.saveKey(Constants.RELOAD,Constants.TRUE);
        }
    }



    private void setEvent() {
        famousSearchViewModel.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                String action = (String) o;
                handleActions(action, famousSearchViewModel.getSearchRepository().getMessage());
                assert action != null;

                if (action.equals(Constants.SUCCESS)) {
                    if(famousSearchViewModel.getSearchRepository().getFamousSearchResponse().data.size() > 0) {
                        famousSearchViewModel.showPage(true);
                        famousSearchViewModel.noDataTextDisplay.set(false);
                        famousAdapter = new FamousAdapter(famousSearchViewModel.getSearchRepository().getFamousSearchResponse().data);
                        fragmentFamousSearchBinding.rvFamous.setAdapter(famousAdapter);
                        setEventAdapter();
                    }else{
                        famousSearchViewModel.noDataTextDisplay.set(true);
                    }
                }
            }
        });
    }


    private void setEventAdapter() {
        famousAdapter.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                int position = (int) o;
                Famous famous = famousSearchViewModel.getSearchRepository().getFamousSearchResponse().data.get(position);
                Intent intent = new Intent(context, BaseActivity.class);
                intent.putExtra(Constants.PAGE, Constants.FAMOUS_DETAILS);
                intent.putExtra(Constants.NAME_BAR, famous.getName());
                Bundle bundle = new Bundle();
                bundle.putInt(Constants.ID, famous.getmId());
                intent.putExtra(Constants.BUNDLE, bundle);
                startActivity(intent);
            }
        });
    }


    @Override
    public void onDestroy() {
        famousSearchViewModel.reset();
        super.onDestroy();
    }

}
