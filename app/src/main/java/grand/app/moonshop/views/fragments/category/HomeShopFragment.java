package grand.app.moonshop.views.fragments.category;


import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import grand.app.moonshop.R;
import grand.app.moonshop.base.BaseFragment;
import grand.app.moonshop.databinding.FragmentHomeShopBinding;
import grand.app.moonshop.models.app.TabModel;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.utils.storage.user.UserHelper;
import grand.app.moonshop.utils.tabLayout.SwapAdapter;
import grand.app.moonshop.viewmodels.home.shop.HomeShopViewModel;
import grand.app.moonshop.views.fragments.ads.AdsFragment;
import grand.app.moonshop.views.fragments.famous.FamousDetailsMainFragment;
import grand.app.moonshop.views.fragments.reservation.beauty.BeautyServicesFragment;
import grand.app.moonshop.views.fragments.reservation.clinic.DoctorListFragment;
import grand.app.moonshop.views.fragments.shop.DepartmentFragment;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeShopFragment extends BaseFragment {

    private FragmentHomeShopBinding fragmentHomeShopBinding;
    private HomeShopViewModel homeShopViewModel;
    public ArrayList<TabModel> tabModels = new ArrayList<>();

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentHomeShopBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_home_shop, container, false);
        bind();
        return fragmentHomeShopBinding.getRoot();
    }


    private void bind() {
        homeShopViewModel = new HomeShopViewModel();
        fragmentHomeShopBinding.setHomeShopViewModel(homeShopViewModel);
        if(UserHelper.getUserDetails().type.equals(Constants.TYPE_RESERVATION_CLINIC))
            tabModels.add(new TabModel(getString(R.string.doctors),new DoctorListFragment()));
        else if(UserHelper.getUserDetails().type.equals(Constants.TYPE_RESERVATION_BEAUTY)) {
            tabModels.add(new TabModel(getString(R.string.services), new BeautyServicesFragment()));
            tabModels.add(new TabModel(getString(R.string.department),new DepartmentFragment()));
        }else
            tabModels.add(new TabModel(getString(R.string.department),new DepartmentFragment()));
//        tabModels.add(new TabModel(getString(R.string.gallery),new OfferFragment()));

        tabModels.add(new TabModel(getString(R.string.album),new FamousDetailsMainFragment()));

        tabModels.add(new TabModel(getString(R.string.adds),new AdsFragment()));

        SwapAdapter adapter = new SwapAdapter(getChildFragmentManager(),tabModels);
        fragmentHomeShopBinding.viewpager.setAdapter(adapter);
        fragmentHomeShopBinding.viewpager.setOffscreenPageLimit(tabModels.size());
        fragmentHomeShopBinding.slidingTabs.setupWithViewPager(fragmentHomeShopBinding.viewpager);

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        homeShopViewModel.reset();

    }


}
