package grand.app.moonshop.views.fragments.review;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moonshop.R;
import grand.app.moonshop.adapter.ReviewAdapter;
import grand.app.moonshop.base.BaseFragment;
import grand.app.moonshop.databinding.FragmentReviewBinding;
import grand.app.moonshop.utils.helper.AppUtils;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.viewmodels.review.ReviewViewModel;

/**
 * A simple {@link Fragment} subclass.
 */
public class ReviewFragment extends BaseFragment {

    private FragmentReviewBinding fragmentReviewBinding;
    private ReviewViewModel reviewViewModel;
    private ReviewAdapter reviewAdapter;


    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        fragmentReviewBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_review, container, false);
        bind();
        setEvent();
        return fragmentReviewBinding.getRoot();
    }

    private void bind() {
        reviewViewModel = new ReviewViewModel();
        AppUtils.initVerticalRV(fragmentReviewBinding.rvReviews, fragmentReviewBinding.rvReviews.getContext(), 1);
        fragmentReviewBinding.setReviewViewModel(reviewViewModel);
    }

    private void setEvent() {
        reviewViewModel.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                String action = (String) o;
                handleActions(action, reviewViewModel.getReviewRepository().getMessage());
                assert action != null;
                if(action.equals(Constants.SUCCESS)){
                    if(reviewViewModel.getReviewRepository().getReviewResponse().mData.size() == 0)
                        reviewViewModel.noData();
                    else {
                        reviewAdapter = new ReviewAdapter(reviewViewModel.getReviewRepository().getReviewResponse().mData);
                        fragmentReviewBinding.rvReviews.setAdapter(reviewAdapter);
                    }
                }
            }
        });
    }


}
