package grand.app.moonshop.views.fragments.shop;


import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moonshop.R;
import grand.app.moonshop.adapter.HomeAdapter;
import grand.app.moonshop.base.BaseFragment;
import grand.app.moonshop.customviews.dialog.DialogConfirm;
import grand.app.moonshop.databinding.FragmentDepartmentBinding;
import grand.app.moonshop.models.app.Mutable;
import grand.app.moonshop.utils.helper.AppUtils;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.utils.dialog.DialogHelperInterface;
import grand.app.moonshop.utils.resources.ResourceManager;
import grand.app.moonshop.viewmodels.home.shop.DepartmentViewModel;
import grand.app.moonshop.views.activities.BaseActivity;
import timber.log.Timber;

/**
 * A simple {@link Fragment} subclass.
 */
public class DepartmentFragment extends BaseFragment  {


    private View rootView;
    private FragmentDepartmentBinding fragmentDepartmentBinding;
    private DepartmentViewModel departmentViewModel;
    private HomeAdapter homeAdapter;



    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        fragmentDepartmentBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_department, container, false);
        bind();
        setEvent();
        return rootView;
    }


    

    private void bind() {
        departmentViewModel = new DepartmentViewModel();
        AppUtils.initVerticalRV(fragmentDepartmentBinding.rvDepartment, fragmentDepartmentBinding.rvDepartment.getContext(), 1);
        fragmentDepartmentBinding.setDepartmentViewModel(departmentViewModel);
        rootView = fragmentDepartmentBinding.getRoot();
    }

    private void setEvent() {
        departmentViewModel.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                String action = (String) o;
                handleActions(action, departmentViewModel.getCategoryRepository().getMessage());
                assert action != null;
                if (action.equals(Constants.SUCCESS)) {
                    homeAdapter = new HomeAdapter(departmentViewModel.getCategoryRepository().getHomeResponse().data);
                    fragmentDepartmentBinding.rvDepartment.setAdapter(homeAdapter);
                    setEventAdapter();
                } else if (action.equals(Constants.ADD)) {
                    Intent intent = new Intent(context, BaseActivity.class);
                    intent.putExtra(Constants.PAGE, Constants.ADD_CATEGORY);
                    intent.putExtra(Constants.NAME_BAR, ResourceManager.getString(R.string.add_category));
//                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//                        startActivityForResult(intent, Constants.RELOAD_RESULT,ActivityOptions.makeSceneTransitionAnimation(getActivity()).toBundle());
//                    }else
                    startActivityForResult(intent, Constants.RELOAD_RESULT);


                } else if (action.equals(Constants.DELETE)) {
                    toastMessage(departmentViewModel.getCategoryRepository().getMessage(), R.drawable.ic_check_white, R.color.colorPrimary);
                    homeAdapter.remove(departmentViewModel.category_delete_position);
                }
            }
        });
    }


    private void setEventAdapter() {
        homeAdapter.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                Mutable mutable = (Mutable) o;
                if (mutable.type.equals(Constants.EDIT)) {
                    Intent intent = new Intent(context, BaseActivity.class);
                    intent.putExtra(Constants.PAGE, Constants.ADD_CATEGORY);
                    intent.putExtra(Constants.NAME_BAR, ResourceManager.getString(R.string.edit_category));
                    Bundle bundle = new Bundle();
                    bundle.putInt(Constants.ID, departmentViewModel.getCategoryRepository().getHomeResponse().data.get(mutable.position).id);
                    bundle.putString(Constants.NAME, departmentViewModel.getCategoryRepository().getHomeResponse().data.get(mutable.position).name);
                    intent.putExtra(Constants.BUNDLE, bundle);
                    startActivityForResult(intent, Constants.RELOAD_RESULT);
                } else if (mutable.type.equals(Constants.DELETE)) {

                    new DialogConfirm(getActivity())
                            .setTitle(ResourceManager.getString(R.string.remove))
                            .setMessage(ResourceManager.getString(R.string.do_you_want_delete_item))
                            .setActionText(ResourceManager.getString(R.string.remove_item))
                            .setActionCancel(ResourceManager.getString(R.string.cancel))
                            .show(new DialogHelperInterface() {
                                @Override
                                public void OnClickListenerContinue(Dialog dialog, View view) {
                                    departmentViewModel.category_delete_position = mutable.position;
                                    departmentViewModel.getCategoryRepository().delete(departmentViewModel.getCategoryRepository().getHomeResponse().data.get(mutable.position).id);
                                }
                            });
                } else if (mutable.type.equals(Constants.PRODUCTS)) {
                    departmentViewModel.category_delete_position = mutable.position;
                    int category_id = departmentViewModel.getCategoryRepository().getHomeResponse().data.get(mutable.position).id;
                    Bundle bundle = new Bundle();
                    bundle.putInt(Constants.ID, category_id);
                    if(departmentViewModel.getCategoryRepository().getHomeResponse() != null)
                        bundle.putSerializable(Constants.DEPARTMENTS,departmentViewModel.getCategoryRepository().getHomeResponse());
                    Intent intent = new Intent(context, BaseActivity.class);
                    intent.putExtra(Constants.PAGE, Constants.PRODUCTS);
                    intent.putExtra(Constants.BUNDLE, bundle);
                    intent.putExtra(Constants.NAME_BAR, getString(R.string.add_product));
                    startActivity(intent);
                }
            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        //super method removed
        Timber.e("onActivityResult:" + requestCode);
        if (resultCode == Activity.RESULT_OK && requestCode == Constants.RELOAD_RESULT) {
            departmentViewModel.getCategoryRepository().home();
        }
        super.onActivityResult(requestCode, resultCode, data);
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        departmentViewModel.reset();

    }

}
