
package grand.app.moonshop.views.fragments.ads;

import grand.app.moonshop.base.ParentViewModel;
import grand.app.moonshop.repository.FamousRepository;


/**
 * Created by Gregory Rasmussen on 7/26/17.
 */
public class CompanyAdsCategoriesViewModel extends ParentViewModel {

    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
    }
}
