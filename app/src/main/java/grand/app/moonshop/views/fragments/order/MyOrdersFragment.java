package grand.app.moonshop.views.fragments.order;


import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moonshop.R;
import grand.app.moonshop.adapter.OrderAdapter;
import grand.app.moonshop.base.BaseFragment;
import grand.app.moonshop.databinding.FragmentMyOrdersBinding;
import grand.app.moonshop.models.chat.list.ChatList;
import grand.app.moonshop.models.order.Order;
import grand.app.moonshop.models.shipping.Shipping;
import grand.app.moonshop.models.user.profile.User;
import grand.app.moonshop.utils.helper.AppUtils;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.utils.resources.ResourceManager;
import grand.app.moonshop.utils.storage.user.UserHelper;
import grand.app.moonshop.viewmodels.order.OrderListViewModel;
import grand.app.moonshop.views.activities.BaseActivity;
import grand.app.moonshop.views.activities.MainActivity;
import timber.log.Timber;

/**
 * A simple {@link Fragment} subclass.
 */
public class MyOrdersFragment extends BaseFragment {


    private FragmentMyOrdersBinding fragmentMyOrdersBinding;
    private OrderListViewModel orderListViewModel;
    private OrderAdapter orderAdapter;

 
    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        fragmentMyOrdersBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_my_orders, container, false);
        bind();
        setEvent();
        return fragmentMyOrdersBinding.getRoot();
    }

    private void bind() {
        orderListViewModel = new OrderListViewModel();
        AppUtils.initVerticalRV(fragmentMyOrdersBinding.rvMyOrders, fragmentMyOrdersBinding.rvMyOrders.getContext(), 1);
        fragmentMyOrdersBinding.setOrderListViewModel(orderListViewModel);
    }

    private void setEvent() {
        orderListViewModel.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                String action = (String) o;
                handleActions(action, orderListViewModel.getOrderRepository().getMessage());
                assert action != null;
                if (action.equals(Constants.ORDERS)) {
                    if(orderListViewModel.getOrderRepository().getOrderListResponse().orders.size() > 0) {
                        orderAdapter = new OrderAdapter(orderListViewModel.getOrderRepository().getOrderListResponse().orders);
                        fragmentMyOrdersBinding.rvMyOrders.setAdapter(orderAdapter);
                        setEventAdapter();
                    }else{
                        orderListViewModel.noData();
                    }
                }
            }
        });
    }

    private void setEventAdapter() {

        orderAdapter.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                int pos = (int) o;
                Order order = orderListViewModel.getOrderRepository().getOrderListResponse().orders.get(pos);
                int newCount = UserHelper.getUserDetails().chats_count - Integer.parseInt(order.orderCount);
                if (requireActivity() instanceof MainActivity) {
                    User user = UserHelper.getUserDetails();
                    user.order_count = newCount;
                    UserHelper.saveUserDetails(user);
                    MainActivity mainActivity = (MainActivity) requireActivity();
                    mainActivity.navigationDrawerView.updateOrdersCount();
                }

                Intent intent = new Intent(context, BaseActivity.class);
                intent.putExtra(Constants.PAGE,Constants.ORDER_DETAILS);
                Bundle bundle = new Bundle();
                bundle.putSerializable(Constants.ID,order.id);
                intent.putExtra(Constants.BUNDLE,bundle);
                intent.putExtra(Constants.NAME_BAR, ResourceManager.getString(R.string.order_details));
                startActivity(intent);

            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        orderListViewModel.reset();
    }

}
