package grand.app.moonshop.views.fragments.base;


import android.os.Bundle;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import grand.app.moonshop.R;
import grand.app.moonshop.databinding.FragmentZoomBinding;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.viewmodels.app.ZoomViewModel;

/**
 * A simple {@link Fragment} subclass.
 */
public class ZoomFragment extends Fragment {


    FragmentZoomBinding fragmentZoomBinding;
    public String image = "";
    private static final String TAG = "ZoomFragment";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        fragmentZoomBinding =   DataBindingUtil.inflate(inflater,R.layout.fragment_zoom, container, false);
        getData();
        Log.e("image2:",image);
        fragmentZoomBinding.setZoomViewModel(new ZoomViewModel(image));
        return fragmentZoomBinding.getRoot();
    }

    private void getData() {
        Log.d(TAG,"start");
        if(getArguments() != null && getArguments().containsKey(Constants.IMAGE)){
            image = getArguments().getString(Constants.IMAGE);
        }
        if(getArguments() != null && getArguments().containsKey(Constants.DATE)){
            String date = getArguments().getString(Constants.DATE);
            Log.d(TAG,date);
            fragmentZoomBinding.tvDate.setText(date);
            fragmentZoomBinding.tvDate.setVisibility(View.VISIBLE);
        }
    }

}
