package grand.app.moonshop.views.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.nostra13.universalimageloader.utils.L;

import java.util.concurrent.TimeUnit;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moonshop.R;
import grand.app.moonshop.base.BaseFragment;
import grand.app.moonshop.base.ParentActivity;
import grand.app.moonshop.databinding.FragmentSplashBinding;
import grand.app.moonshop.map.LocationLatLng;
import grand.app.moonshop.repository.FirebaseRepository;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.utils.maputils.location.GPSLocation;
import grand.app.moonshop.utils.maputils.location.LocationChangeListener;
import grand.app.moonshop.utils.resources.ResourceManager;
import grand.app.moonshop.utils.storage.user.UserHelper;
import grand.app.moonshop.viewmodels.common.SplashViewModel;
import grand.app.moonshop.views.activities.BaseActivity;
import grand.app.moonshop.views.activities.MainActivity;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import timber.log.Timber;


public class SplashFragment extends BaseFragment {
    View rootView;
    private FragmentSplashBinding fragmentSplashBinding;
    private SplashViewModel splashViewModel;
    private static final String TAG = "SplashFragment";

    boolean locationIsOpen = false;


    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        fragmentSplashBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_splash, container, false);
        bind();
        if (UserHelper.getUserId() == -1) {
            thread();
        } else {
            FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener((ParentActivity) context,
                    instanceIdResult -> {
                        String newToken = instanceIdResult.getToken();
                        UserHelper.saveKey(Constants.TOKEN, newToken);
                        splashViewModel.firebaseRepository.updateToken(newToken);
                    });
            setEventHandle();
        }
        return rootView;
    }


    private void bind() {
        splashViewModel = new SplashViewModel();
        fragmentSplashBinding.setSplashViewModel(splashViewModel);
        rootView = fragmentSplashBinding.getRoot();
    }


    private void setEventHandle() {
        splashViewModel.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                String action = (String) o;
                handleActions(action, splashViewModel.firebaseRepository.getMessage());
                Intent intent = null;
                if (action != null) {
                    if (action.equals(Constants.SUCCESS)) {
                        setEvent();
                    } else if (action.equals(Constants.PACKAGES)) {
                        intent = new Intent(context, BaseActivity.class);
                        intent.putExtra(Constants.PAGE, Constants.PACKAGES);
                        getActivityBase().finishAffinity();
                        startActivity(intent);
                    }
                }
            }
        });
    }

    Disposable disposable = null;

    private void thread() {
        disposable = Observable.interval(5, TimeUnit.SECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .doOnError(throwable -> L.e(TAG, "Throwable " + throwable.getMessage()))
                .subscribe(i -> setEvent());

    }


    private void setEvent() {
        try {
            Intent intent = null;

            Timber.e("language" + UserHelper.retrieveKey(Constants.LANGUAGE_HAVE));
            if (UserHelper.retrieveKey(Constants.LANGUAGE_HAVE).equals("")) {
                Timber.e("language");
                intent = new Intent(context, BaseActivity.class);
                intent.putExtra(Constants.PAGE, Constants.LANGUAGE);
                intent.putExtra(Constants.NAME_BAR, ResourceManager.getString(R.string.label_language));
            } else if (UserHelper.retrieveKey(Constants.COUNTRY_ID).equals("") || !UserHelper.isAccountTypes()) {
                Timber.e("country");
                intent = new Intent(context, BaseActivity.class);
                intent.putExtra(Constants.PAGE, Constants.COUNTRIES);
                intent.putExtra(Constants.NAME_BAR, ResourceManager.getString(R.string.country));
            } else if (UserHelper.getUserId() == -1) {
                ((ParentActivity) context).finish();
                intent = new Intent(context, BaseActivity.class);
                intent.putExtra(Constants.PAGE, Constants.LOGIN);
            } else if (UserHelper.getUserId() != -1) {
                requireActivity().finishAffinity();
                intent = new Intent(context, MainActivity.class);
            } else {
                Timber.e("not fetch location");
                intent = new Intent(context, BaseActivity.class);
                intent.putExtra(Constants.PAGE, Constants.LOCATION);
            }
            if (disposable != null) disposable.dispose();
            startActivity(intent);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }


}
