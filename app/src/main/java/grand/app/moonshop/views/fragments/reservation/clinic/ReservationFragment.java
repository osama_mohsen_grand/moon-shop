package grand.app.moonshop.views.fragments.reservation.clinic;


import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moonshop.R;
import grand.app.moonshop.adapter.ReservationAdapter;
import grand.app.moonshop.base.BaseFragment;
import grand.app.moonshop.customviews.dialog.DialogConfirm;
import grand.app.moonshop.databinding.FragmentReservationBinding;
import grand.app.moonshop.models.app.Mutable;
import grand.app.moonshop.models.reservation.doctor.order.ReservationOrder;
import grand.app.moonshop.utils.helper.AppUtils;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.utils.dialog.DialogHelperInterface;
import grand.app.moonshop.utils.resources.ResourceManager;
import grand.app.moonshop.viewmodels.reservation.main.ReservationViewModel;
import grand.app.moonshop.views.activities.BaseActivity;
import grand.app.moonshop.views.fragments.clinic.BookClinicFragment;

/**
 * A simple {@link Fragment} subclass.
 */
public class ReservationFragment extends BaseFragment {


    private FragmentReservationBinding fragmentReservationBinding;
    private ReservationViewModel reservationViewModel;
    public ReservationAdapter reservationAdapter;
    private ReservationMainFragment reservationMainFragment;
    String status = "";

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentReservationBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_reservation, container, false);
        getData();
        bind();
        setEvent();
        return fragmentReservationBinding.getRoot();
    }

    private void getData() {
        if (getArguments() != null && getArguments().containsKey(Constants.STATUS)) {
            status = getArguments().getString(Constants.STATUS);
            reservationMainFragment = ((ReservationMainFragment) this.getParentFragment());
        }
    }


    //if  you want to get order recent or new  send status 0 else if you want to get previous order  send status 1
    private void bind() {
        reservationViewModel = new ReservationViewModel(status);
        AppUtils.initVerticalRV(fragmentReservationBinding.rvReservation, fragmentReservationBinding.rvReservation.getContext(), 1);
        fragmentReservationBinding.setReservationViewModel(reservationViewModel);
    }

    private void setEvent() {
        reservationViewModel.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                String action = (String) o;
                handleActions(action, reservationViewModel.getReservationRepository().getMessage());
                assert action != null;
                if (action.equals(Constants.SUCCESS)) {
                    reservationViewModel.showPage(true);
                    reservationViewModel.noDataDisplay(reservationViewModel.getReservationRepository().getReservationOrderResponse().orders);
                    reservationAdapter = new ReservationAdapter(reservationViewModel.getReservationRepository().getReservationOrderResponse().orders, status);
                    fragmentReservationBinding.rvReservation.setAdapter(reservationAdapter);
                    setEventAdapter();
                } else if (action.equals(Constants.CONFIRM)) {
                    ReservationOrder reservationOrder = reservationViewModel.getReservationRepository().getReservationOrderResponse().orders.get(reservationViewModel.category_delete_position);
                    if (reservationMainFragment != null)
                        reservationMainFragment.moveToHistory(reservationOrder);

                    toastMessage(reservationViewModel.getReservationRepository().getMessage(), R.drawable.ic_check_white, R.color.colorPrimary);
                    reservationAdapter.remove(reservationViewModel.category_delete_position);
                }
            }
        });
    }


    private void setEventAdapter() {
        reservationAdapter.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                Mutable mutable = (Mutable) o;
                if (mutable.type.equals(Constants.CONFIRM)) {
                    reservationViewModel.category_delete_position = mutable.position;
                    ReservationOrder reservationOrder = reservationViewModel.getReservationRepository().getReservationOrderResponse().orders.get(mutable.position);
                    reservationViewModel.reservationOrderActionRequest.order_id = reservationOrder.id;
                    reservationViewModel.reservationOrderActionRequest.status = 1;
                    reservationViewModel.getReservationRepository().reservationOrderAction(reservationViewModel.reservationOrderActionRequest);
                } else if (mutable.type.equals(Constants.DELETE)) {
                    ReservationOrder reservationOrder = reservationViewModel.getReservationRepository().getReservationOrderResponse().orders.get(mutable.position);
                    new DialogConfirm(getActivity())
                            .setTitle(ResourceManager.getString(R.string.oops))
                            .setMessage(ResourceManager.getString(R.string.do_you_want_delete_order))
                            .setActionText(ResourceManager.getString(R.string.remove))
                            .setActionCancel(ResourceManager.getString(R.string.cancel))
                            .show(new DialogHelperInterface() {
                                @Override
                                public void OnClickListenerContinue(Dialog dialog, View view) {
                                    reservationViewModel.category_delete_position = mutable.position;
                                    reservationViewModel.reservationOrderActionRequest.order_id = reservationOrder.id;
                                    reservationViewModel.reservationOrderActionRequest.status = 0;
                                    reservationViewModel.getReservationRepository().reservationOrderAction(reservationViewModel.reservationOrderActionRequest);
                                }
                            });
                } else if (mutable.type.equals(Constants.SUBMIT)) {
                    Intent intent = new Intent(context, BaseActivity.class);
//                    intent.putExtra(Constants.PAGE,Constants.ORDER_DETAILS_BEAUTY);
                    intent.putExtra(Constants.PAGE, Constants.RESERVATION_DETAILS);
                    intent.putExtra(Constants.NAME_BAR, ResourceManager.getString(R.string.order_details));
                    Bundle bundle = new Bundle();
                    bundle.putInt(Constants.ID, reservationViewModel.getReservationRepository().getReservationOrderResponse().orders.get(mutable.position).id);
                    intent.putExtra(Constants.BUNDLE, bundle);
                    startActivity(intent);
                }else if(mutable.type.equals(Constants.ADD)){
                    reservationViewModel.noDataDisplay(reservationAdapter.reservationOrders);
                }else if(mutable.type.equals(Constants.EDIT)){
                    Intent intent = new Intent(context,BaseActivity.class);
                    intent.putExtra(Constants.PAGE, BookClinicFragment.class.getName());
                    intent.putExtra(Constants.NAME_BAR,ResourceManager.getString(R.string.edit));
                    Bundle bundle = new Bundle();
                    bundle.putInt(Constants.ID,reservationViewModel.getReservationRepository().getReservationOrderResponse().orders.get(mutable.position).id);
                    bundle.putInt(Constants.DOCTOR_ID,reservationViewModel.getReservationRepository().getReservationOrderResponse().orders.get(mutable.position).doctorId);
                    intent.putExtra(Constants.BUNDLE,bundle);
                    startActivity(intent);
                }
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        reservationViewModel.callService();
    }
}
