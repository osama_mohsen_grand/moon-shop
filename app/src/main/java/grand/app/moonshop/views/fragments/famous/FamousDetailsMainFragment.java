package grand.app.moonshop.views.fragments.famous;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moonshop.R;
import grand.app.moonshop.adapter.AlbumAdapterModel;
import grand.app.moonshop.base.BaseFragment;

import grand.app.moonshop.customviews.dialog.DialogConfirm;
import grand.app.moonshop.databinding.FragmentFamousMainDetailsBinding;
import grand.app.moonshop.models.app.Mutable;
import grand.app.moonshop.utils.helper.AppUtils;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.utils.dialog.DialogHelperInterface;
import grand.app.moonshop.utils.resources.ResourceManager;
import grand.app.moonshop.utils.storage.user.UserHelper;
import grand.app.moonshop.viewmodels.famous.details.FamousMainDetailsViewModel;
import grand.app.moonshop.views.activities.BaseActivity;
import timber.log.Timber;


public class FamousDetailsMainFragment extends BaseFragment {
    private FragmentFamousMainDetailsBinding famousMainDetailsBinding;
    public FamousMainDetailsViewModel famousDetailsViewModel;
    AlbumAdapterModel albumAdapterImages;

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        UserHelper.saveKey(Constants.RELOAD,Constants.FALSE);
        famousMainDetailsBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_famous_main_details, container, false);
        bind();
        setEvent();
        return famousMainDetailsBinding.getRoot();
    }



    private void bind() {
        famousDetailsViewModel = new FamousMainDetailsViewModel();
        AppUtils.initVerticalRV(famousMainDetailsBinding.rvAlbum, famousMainDetailsBinding.rvAlbum.getContext(), 3);
//        AppUtils.initVerticalRV(famousMainDetailsBinding.rvVideos, famousMainDetailsBinding.rvVideos.getContext(), 3);
//        famousMainDetailsBinding.rvAlbum.setNestedScrollingEnabled(false);
//        famousMainDetailsBinding.rvVideos.setNestedScrollingEnabled(false);
        famousMainDetailsBinding.setFamousDetailsViewModel(famousDetailsViewModel);
    }


    private void setEvent() {
        famousDetailsViewModel.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                String action = (String) o;
                handleActions(action, famousDetailsViewModel.getFamousRepository().getMessage());
                assert action != null;
                Intent intent = new Intent(context,BaseActivity.class);
                intent.putExtra(Constants.PAGE,Constants.ADD_ALBUM);
                Bundle bundle = new Bundle();

                if (action.equals(Constants.SUCCESS)) {
                    famousDetailsViewModel.showPage(true);
                    albumAdapterImages = new AlbumAdapterModel(famousDetailsViewModel.getFamousRepository().getFamousHomeResponse().data,true);
                    famousMainDetailsBinding.rvAlbum.setAdapter(albumAdapterImages);
                    setEventAdapter();
                } else if (action.equals(Constants.ADD_ALBUM)) {
                    intent.putExtra(Constants.NAME_BAR,ResourceManager.getString(R.string.add_album));
                    bundle.putString(Constants.TYPE,"1");
                    Timber.e("type:1");
                    intent.putExtra(Constants.BUNDLE,bundle);
                    startActivity(intent);
//            MovementHelper.addFragmentTag(this, new FamousAddAlbumFragment(), Constants.HOME, "");
                } else if (action.equals(Constants.DELETED)) {
                    toastMessage(famousDetailsViewModel.getFamousRepository().getMessage(), R.drawable.ic_check_white, R.color.colorPrimary);
                }
            }
        });
    }


    private void setEventAdapter() {
        albumAdapterImages.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                submitAdapter(o,Constants.IMAGE);
            }
        });

//        albumAdapterVideos.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
//            @Override
//            public void onChanged(@Nullable Object o) {
//                submitAdapter(o,Constants.VIDEO);
//            }
//        });
    }

    public void submitAdapter(Object o,String type){
        Mutable mutable = (Mutable) o;
        if(mutable.type.equals(Constants.EDIT)){
            Intent intent = new Intent(context, BaseActivity.class);
            intent.putExtra(Constants.PAGE,Constants.EDIT_ALBUM);
            intent.putExtra(Constants.NAME_BAR, ResourceManager.getString(R.string.edit_album));
            Bundle bundle = new Bundle();

            bundle.putString(Constants.NAME,famousDetailsViewModel.getFamousRepository().getFamousHomeResponse().data.get(mutable.position).name);
            bundle.putInt(Constants.ID, famousDetailsViewModel.getFamousRepository().getFamousHomeResponse().data.get(mutable.position).id);
            bundle.putString(Constants.IMAGE, famousDetailsViewModel.getFamousRepository().getFamousHomeResponse().data.get(mutable.position).image);
            bundle.putString(Constants.TAB, "1");
            intent.putExtra(Constants.BUNDLE, bundle);
            startActivityForResult(intent,Constants.RELOAD_RESULT);

        }else if(mutable.type.equals(Constants.SUBMIT)) {
            Intent intent = new Intent(context, BaseActivity.class);
            intent.putExtra(Constants.PAGE, Constants.ALBUM_DETAILS);
            Bundle bundle = new Bundle();
//            if(type.equals(Constants.IMAGE)) {
            intent.putExtra(Constants.NAME_BAR, famousDetailsViewModel.getFamousRepository().getFamousHomeResponse().data.get(mutable.position).name);
                bundle.putInt(Constants.ID, famousDetailsViewModel.getFamousRepository().getFamousHomeResponse().data.get(mutable.position).id);
//            }
            bundle.putString(Constants.TYPE,type);
            bundle.putString(Constants.TAB, "1");
            intent.putExtra(Constants.BUNDLE, bundle);
            startActivityForResult(intent,Constants.RELOAD_RESULT);
        }else if(mutable.type.equals(Constants.DELETE)){
            new DialogConfirm(getActivity())
                    .setTitle(ResourceManager.getString(R.string.remove_product))
                    .setMessage(ResourceManager.getString(R.string.do_you_want_delete_item))
                    .setActionText(ResourceManager.getString(R.string.remove_item))
                    .setActionCancel(ResourceManager.getString(R.string.cancel))
                    .show(new DialogHelperInterface() {
                        @Override
                        public void OnClickListenerContinue(Dialog dialog, View view) {
                            famousDetailsViewModel.category_type = type;
                            famousDetailsViewModel.category_delete_position = mutable.position;
//                            if(type.equals(Constants.IMAGE)) {
                                famousDetailsViewModel.delete(famousDetailsViewModel.getFamousRepository().getFamousHomeResponse().data.get(mutable.position).id);
                                albumAdapterImages.remove(mutable.position);
//                            }
                        }
                    });
        }
    }



    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        UserHelper.saveKey(Constants.RELOAD, Constants.FALSE);
    }

    @Override
    public void onResume() {
        super.onResume();
        if(UserHelper.retrieveKey(Constants.RELOAD).equals(Constants.TRUE) && famousDetailsViewModel != null)
            famousDetailsViewModel.callService();

    }

    @Override
    public void onDestroy() {
        famousDetailsViewModel.reset();
        super.onDestroy();
    }

}
