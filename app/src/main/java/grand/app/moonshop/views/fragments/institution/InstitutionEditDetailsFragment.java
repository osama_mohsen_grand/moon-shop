package grand.app.moonshop.views.fragments.institution;


import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moonshop.R;
import grand.app.moonshop.adapter.InstitutionServicesAdapter;
import grand.app.moonshop.base.BaseFragment;
import grand.app.moonshop.customviews.dialog.DialogConfirm;
import grand.app.moonshop.databinding.FragmentEditInstituationDetailsBinding;
import grand.app.moonshop.models.shop.ShopData;
import grand.app.moonshop.utils.helper.AppUtils;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.utils.dialog.DialogHelperInterface;
import grand.app.moonshop.utils.resources.ResourceManager;
import grand.app.moonshop.viewmodels.institution.InstitutionEditDetailsViewModel;
import grand.app.moonshop.views.activities.BaseActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class InstitutionEditDetailsFragment extends BaseFragment {
    FragmentEditInstituationDetailsBinding fragmentEditInstituationDetailsBinding;
    InstitutionEditDetailsViewModel institutionEditDetailsViewModel;
    private int id;
    private ShopData shopData;
    InstitutionServicesAdapter institutionServicesAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        fragmentEditInstituationDetailsBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_edit_instituation_details, container, false);
        AppUtils.initVerticalRV(fragmentEditInstituationDetailsBinding.rvServiceInstitution, fragmentEditInstituationDetailsBinding.rvServiceInstitution.getContext(), 1);
        getData();
        return fragmentEditInstituationDetailsBinding.getRoot();
    }


    private void getData() {
        if (getArguments() != null && getArguments().containsKey(Constants.ID)) { // shop_id
            id = getArguments().getInt(Constants.ID);
        }
        if (getArguments() != null && getArguments().containsKey(Constants.SHOP_DETAILS)) {
            shopData = (ShopData) getArguments().getSerializable(Constants.SHOP_DETAILS);
            institutionServicesAdapter = new InstitutionServicesAdapter(shopData.categories,true);
            fragmentEditInstituationDetailsBinding.rvServiceInstitution.setAdapter(institutionServicesAdapter);
            setEventAdapter();
        }
    }

    private void setEventAdapter() {

        institutionServicesAdapter.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                int position = (int) o;
                new DialogConfirm(getActivity())
                        .setTitle(ResourceManager.getString(R.string.remove_service))
                        .setMessage(ResourceManager.getString(R.string.do_you_want_delete_service))
                        .setActionText(ResourceManager.getString(R.string.remove_item))
                        .setActionCancel(ResourceManager.getString(R.string.cancel))
                        .show(new DialogHelperInterface() {
                            @Override
                            public void OnClickListenerContinue(Dialog dialog, View view) {
                                institutionEditDetailsViewModel.service_delete_position = position;
                                institutionEditDetailsViewModel.delete(shopData.categories.get(position).id);
                            }
                        });
            }
        });


    }

    public void bind() {
        institutionEditDetailsViewModel = new InstitutionEditDetailsViewModel(shopData.shop_details);
        fragmentEditInstituationDetailsBinding.setInstitutionEditDetailsViewModel(institutionEditDetailsViewModel);
    }

    private void setEvent() {
        institutionEditDetailsViewModel.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                String action = (String) o;
                assert action != null;
                handleActions(action, institutionEditDetailsViewModel.getInstitutionRepository().getMessage());
                if (action.equals(Constants.ADD_SERVICE)) {
                    Intent intent = new Intent(context, BaseActivity.class);
                    intent.putExtra(Constants.PAGE,Constants.ADD_SERVICE);
                    intent.putExtra(Constants.BUNDLE,getArguments());
                    startActivity(intent);
                }else if(action.equals(Constants.SUCCESS)){
                    toastMessage(institutionEditDetailsViewModel.getInstitutionRepository().getMessage());
                    reloadPage();
                    getActivityBase().finish();
                }else if (action.equals(Constants.DELETE)) {
                    toastMessage(institutionEditDetailsViewModel.getInstitutionRepository().getMessage(), R.drawable.ic_check_white, R.color.colorPrimary);
                    institutionServicesAdapter.remove(institutionEditDetailsViewModel.service_delete_position);
                    reloadPage();
                }
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        bind();
        setEvent();
        if(isReloadPage()){
            reloadPage();
            getActivityBase().finish();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (institutionEditDetailsViewModel != null)
            institutionEditDetailsViewModel.reset();
    }



}
