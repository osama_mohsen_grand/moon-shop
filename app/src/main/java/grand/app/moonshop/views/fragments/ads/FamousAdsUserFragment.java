package grand.app.moonshop.views.fragments.ads;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moonshop.R;
import grand.app.moonshop.adapter.CompanyAdsAdapter;
import grand.app.moonshop.base.BaseFragment;
import grand.app.moonshop.customviews.dialog.DialogConfirm;
import grand.app.moonshop.databinding.FragmentFamousAdsUserBinding;
import grand.app.moonshop.models.album.AlbumModelCategories;
import grand.app.moonshop.models.app.Mutable;
import grand.app.moonshop.utils.helper.AppUtils;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.utils.dialog.BottomSheetDialogHelper;
import grand.app.moonshop.utils.dialog.DialogHelperInterface;
import grand.app.moonshop.utils.resources.ResourceManager;
import grand.app.moonshop.utils.storage.user.UserHelper;
import grand.app.moonshop.viewmodels.famous.details.FamousAlbumsUserDetailsViewModel;
import grand.app.moonshop.views.activities.BaseActivity;

public class FamousAdsUserFragment extends BaseFragment {

    private FragmentFamousAdsUserBinding fragmentFamousAdsUserBinding;
    private FamousAlbumsUserDetailsViewModel famousAlbumsUserDetailsViewModel;
    private CompanyAdsAdapter companyAdsAdapter;
    BottomSheetDialogHelper bottomSheetDialogHelper;

    public int id = -1;
    int tab = -1;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                              Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        UserHelper.saveKey(Constants.RELOAD, Constants.FALSE);
        fragmentFamousAdsUserBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_famous_ads_user, container, false);
        getData();
        bind();
        setEvent();
        return fragmentFamousAdsUserBinding.getRoot();

    }

    private void getData() {
        if (getArguments() != null && getArguments().containsKey(Constants.ID))
            id = getArguments().getInt(Constants.ID);
        if (getArguments() != null && getArguments().containsKey(Constants.TAB))
            tab = getArguments().getInt(Constants.TAB);
    }


    private void bind() {
        bottomSheetDialogHelper = new BottomSheetDialogHelper(getActivityBase());
        famousAlbumsUserDetailsViewModel = new FamousAlbumsUserDetailsViewModel(id, tab);
        companyAdsAdapter = new CompanyAdsAdapter(new ArrayList<>());
        companyAdsAdapter.famous_id = id;
        fragmentFamousAdsUserBinding.rvFamousAlbumDetails.setAdapter(companyAdsAdapter);
        setEventAdapter();
        AppUtils.initVerticalRV(fragmentFamousAdsUserBinding.rvFamousAlbumDetails, fragmentFamousAdsUserBinding.rvFamousAlbumDetails.getContext(), 3);
        fragmentFamousAdsUserBinding.setFamousAlbumUserDetailsViewModel(famousAlbumsUserDetailsViewModel);
    }


    List<AlbumModelCategories> imageVideos = null;

    private static final String TAG = "FamousAdsUserFragment";
    private void setEvent() {
        famousAlbumsUserDetailsViewModel.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                String action = (String) o;
                handleActions(action, famousAlbumsUserDetailsViewModel.getFamousRepository().getMessage());
                Log.e(TAG, "onChanged: "+action );
                if (famousAlbumsUserDetailsViewModel.getAdsRepository() != null)
                    handleActions(action, famousAlbumsUserDetailsViewModel.getAdsRepository().getMessage());
                assert action != null;
                if (action.equals(Constants.SUCCESS)) {
                    imageVideos = famousAlbumsUserDetailsViewModel.getFamousRepository().getFamousDetailsResponse().ads;
                    if (imageVideos != null && imageVideos.size() > 0) {
                        Log.e(TAG, "size: "+imageVideos.size() );
                        companyAdsAdapter.update(imageVideos);
                    } else {
                        Log.e(TAG, "companyAdsAdapter: not data" );
                        famousAlbumsUserDetailsViewModel.noData();
                    }
                } else if (action.equals(Constants.FILTER)) {
                    if (bottomSheetDialogHelper.filterDialog == null) {
                        bottomSheetDialogHelper.filterFamousShopAds(famousAlbumsUserDetailsViewModel.getAdsRepository().getFilterShopAdsResponse().data, false, (dialog, type, object) -> {
//                            if (type.equals(Constants.ARRAY)) {
//                                ArrayList<Integer> list = (ArrayList<Integer>) object;
//                                if (list != null && list.size() > 0) {
//                                    famousAlbumsUserDetailsViewModel.submitFilter(list);
//                                }
//                            }

                            if (type.equals(Constants.ARRAY)) {
                                ArrayList<Integer> list = (ArrayList<Integer>) object;
                                Log.d(TAG,"list:"+list.size());
                                if (list != null && list.size() > 0) {
                                    Log.d(TAG,list.get(0).toString());
                                    Intent intent = new Intent(context, BaseActivity.class);
                                    intent.putExtra(Constants.NAME_BAR, getString(R.string.ads));
                                    intent.putExtra(Constants.PAGE, grand.app.moonshopshop.views.fragments.ads.AdvertisementFragment.class.getName());
                                    Bundle bundle = new Bundle();
                                    bundle.putInt(Constants.SHOP_ID,list.get(0));
                                    bundle.putInt(Constants.FAMOUS_ID,id);
                                    intent.putExtra(Constants.BUNDLE,bundle);
                                    startActivity(intent);
//                                    famousAlbumsUserDetailsViewModel.submitFilter(list);
                                }
                            }

                        });
                    } else {
                        bottomSheetDialogHelper.filterDialog.show();
                    }
                }
            }
        });
    }


    private void setEventAdapter() {
        companyAdsAdapter.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                Mutable mutable = (Mutable) o;
                if (mutable.type.equals(Constants.IMAGE)) {
                    Intent intent = new Intent(context, BaseActivity.class);
                    intent.putExtra(Constants.PAGE, Constants.ZOOM);
                    intent.putExtra(Constants.NAME_BAR, famousAlbumsUserDetailsViewModel.getFamousRepository().getFamousDetailsResponse().data.get(mutable.position).name);
                    Bundle bundle = new Bundle();
                    bundle.putString(Constants.IMAGE, famousAlbumsUserDetailsViewModel.getFamousRepository().getFamousDetailsResponse().data.get(mutable.position).image);
                    intent.putExtra(Constants.BUNDLE, bundle);
                    startActivityForResult(intent, Constants.RELOAD_RESULT);
                } else if (mutable.type.equals(Constants.VIDEO)) {
                    Intent intent = new Intent(context, BaseActivity.class);
                    intent.putExtra(Constants.PAGE, Constants.VIDEO);
                    intent.putExtra(Constants.NAME_BAR, famousAlbumsUserDetailsViewModel.getFamousRepository().getFamousDetailsResponse().data.get(mutable.position).name);
                    Bundle bundle = new Bundle();
                    bundle.putString(Constants.VIDEO, famousAlbumsUserDetailsViewModel.getFamousRepository().getFamousDetailsResponse().data.get(mutable.position).capture);
                    intent.putExtra(Constants.BUNDLE, bundle);
                    startActivity(intent);
                }else if(mutable.type.equals(Constants.EDIT)){

                }else if(mutable.type.equals(Constants.DELETE)){
                    new DialogConfirm(getActivity())
                            .setTitle(ResourceManager.getString(R.string.remove))
                            .setMessage(ResourceManager.getString(R.string.do_you_want_delete_image))
                            .setActionText(ResourceManager.getString(R.string.remove_item))
                            .setActionCancel(ResourceManager.getString(R.string.cancel))
                            .show(new DialogHelperInterface() {
                                @Override
                                public void OnClickListenerContinue(Dialog dialog, View view) {
                                    famousAlbumsUserDetailsViewModel.category_delete_position = mutable.position;
                                    famousAlbumsUserDetailsViewModel.delete(famousAlbumsUserDetailsViewModel.getFamousRepository().getFamousDetailsResponse().data.remove(mutable.position).id);
                                }
                            });
                }
            }
        });
    }

}
