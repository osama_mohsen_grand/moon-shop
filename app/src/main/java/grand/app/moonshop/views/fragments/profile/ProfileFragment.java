package grand.app.moonshop.views.fragments.profile;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.theartofdev.edmodo.cropper.CropImage;
import com.yalantis.ucrop.UCrop;

import java.io.File;

import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moonshop.R;
import grand.app.moonshop.adapter.SuggestAdapter;
import grand.app.moonshop.base.BaseFragment;
import grand.app.moonshop.databinding.FragmentProfileBinding;
import grand.app.moonshop.models.app.AppMoon;
import grand.app.moonshop.models.shop.AddShopResponse;
import grand.app.moonshop.utils.helper.AppUtils;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.utils.PopUp.PopUpMenuHelper;
import grand.app.moonshop.utils.images.ImageLoaderHelper;
import grand.app.moonshop.utils.resources.ResourceManager;
import grand.app.moonshop.utils.storage.user.UserHelper;
import grand.app.moonshop.utils.upload.FileOperations;
import grand.app.moonshop.viewmodels.user.ProfileViewModel;
import grand.app.moonshop.views.activities.BaseActivity;
import grand.app.moonshop.views.activities.MainActivity;
import grand.app.moonshop.views.activities.MapAddressActivity;
import grand.app.moonshop.vollyutils.VolleyFileObject;
import timber.log.Timber;

import static android.app.Activity.RESULT_OK;
import static com.facebook.FacebookSdk.getCacheDir;

/**
 *
 */
public class ProfileFragment extends BaseFragment {
    View rootView;
    private FragmentProfileBinding fragmentProfileBinding;
    private ProfileViewModel profileViewModel;
    public String type = "";
    public PopUpMenuHelper popUpMenuHelper = new PopUpMenuHelper();
    SuggestAdapter suggestAdapter = null;


    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentProfileBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_profile, container, false);
        getData();
        bind();
        setEvent();
        moveCursor();
        fragmentProfileBinding.edtProfilePhone.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() == 0) {
                    fragmentProfileBinding.edtProfilePhone.setText("+");
                    moveCursor();
                }
            }
        });

        return rootView;
    }

    private void getData() {
        if (getArguments() != null && getArguments().containsKey(Constants.TYPE)) {
            type = getArguments().getString(Constants.TYPE);
        }
    }

    private void bind() {
        profileViewModel = new ProfileViewModel();
        AppUtils.initHorizontalRV(fragmentProfileBinding.rvSuggestions, fragmentProfileBinding.rvSuggestions.getContext(), 1);
        fragmentProfileBinding.setProfileViewModel(profileViewModel);
        rootView = fragmentProfileBinding.getRoot();
    }

    public void moveCursor() {
        fragmentProfileBinding.edtProfilePhone.post(new Runnable() {
            @Override
            public void run() {
                fragmentProfileBinding.edtProfilePhone.setSelection(fragmentProfileBinding.edtProfilePhone.length());
            }
        });
    }


    private void setEvent() {
        profileViewModel.mMutableLiveData.observe((LifecycleOwner) context, o -> {
            String action = (String) o;
            handleActions(action, profileViewModel.getLoginRepository().getMessage());
            Timber.e(action);
            if (action.equals(Constants.SUCCESS)) {//submitSearch register
                toastMessage(profileViewModel.getLoginRepository().getMessage(), R.drawable.ic_check_white, R.color.colorPrimary);
                if (suggestAdapter != null) {
                    suggestAdapter.clear();
                    profileViewModel.notifyChange();
                }
                if (AppMoon.showHeaderMenu()) {
                    ((MainActivity) context).navigationDrawerView.layoutNavigationDrawerBinding.tvNavigationUsername.setText(profileViewModel.profileRequest.getName());
                    ImageLoaderHelper.ImageLoaderLoad(context, UserHelper.getUserDetails().image, ((MainActivity) context).navigationDrawerView.layoutNavigationDrawerBinding.civMainUserImage);
                }
            } else if (action.equals(Constants.SELECT_IMAGE)) {//select image profile
                pickImageDialogSelect();
            } else if (action.equals(Constants.LOCATIONS)) {//select location
                Timber.e("Locations");
                Intent intent = new Intent(context, MapAddressActivity.class);
                startActivityForResult(intent, Constants.ADDRESS_RESULT);
            } else if (action.equals(Constants.CHANGE_PASSWORD)) {
                Intent intent = new Intent(context, BaseActivity.class);
                intent.putExtra(Constants.PAGE, Constants.CHANGE_PASSWORD);
                intent.putExtra(Constants.NAME_BAR, ResourceManager.getString(R.string.change_password));
                startActivity(intent);
            } else if (action.equals(Constants.LANGUAGE)) {
                Intent intent = new Intent(context, BaseActivity.class);
                intent.putExtra(Constants.PAGE, Constants.LANGUAGE);
                intent.putExtra(Constants.NAME_BAR, getString(R.string.label_language));
                startActivity(intent);
            } else if (action.equals(Constants.COUNTRIES)) {
                Intent intent = new Intent(context, BaseActivity.class);
                intent.putExtra(Constants.PAGE, Constants.COUNTRIES);
                intent.putExtra(Constants.NAME_BAR, getString(R.string.country));
                startActivity(intent);
            } else if (action.equals(Constants.SUGGESTIONS)) {
                fragmentProfileBinding.nscrollCheckout.scrollTo(0, (int) fragmentProfileBinding.tplBeShopNickname.getY());
                AddShopResponse addShopResponse = profileViewModel.getLoginRepository().getAddShopResponse();
                fragmentProfileBinding.tplBeShopNickname.setError(addShopResponse.msg);
                if (suggestAdapter == null) {
                    suggestAdapter = new SuggestAdapter(addShopResponse.suggestions);
                    fragmentProfileBinding.rvSuggestions.setAdapter(suggestAdapter);
                    suggestAdapter.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
                        @Override
                        public void onChanged(@Nullable Object o) {
                            int pos = (int) o;
                            profileViewModel.profileRequest.setNickname(addShopResponse.suggestions.get(pos).name);
                            profileViewModel.notifyChange();
                        }
                    });
                } else {
                    suggestAdapter.updateAll(addShopResponse.suggestions);
                }
            }
        });
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        //super method removed
        Timber.e("onActivityResult:" + requestCode);
        if (requestCode == Constants.FILE_TYPE_IMAGE) {
            VolleyFileObject volleyFileObject1 = FileOperations.getVolleyFileObject(getActivity(), data, Constants.IMAGE, Constants.FILE_TYPE_IMAGE);
            File file = new File(getCacheDir(), Constants.IMAGE + ".png");
            UCrop.of(Uri.fromFile(volleyFileObject1.getFile()), Uri.fromFile(file)).start(context, ProfileFragment.this);
        }
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE && data != null) {

            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();
                UCrop.of(resultUri, resultUri).start(context, ProfileFragment.this);
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Toast.makeText(context, "" + getString(R.string.error_failed_pick_gallery_image), Toast.LENGTH_SHORT).show();
            }
        } else if (requestCode == UCrop.REQUEST_CROP && data != null) {
            final Uri resultUri = UCrop.getOutput(data);
            if (resultUri != null) {
                VolleyFileObject volleyFileObject = new VolleyFileObject(Constants.IMAGE, resultUri.getPath(), Constants.FILE_TYPE_IMAGE);
                profileViewModel.setImage(volleyFileObject);
                fragmentProfileBinding.imgProfileUser.setImageURI(null);
                fragmentProfileBinding.imgProfileUser.setImageURI(Uri.parse(String.valueOf(new File(volleyFileObject.getFilePath()))));
            }
        }
        if (resultCode == Activity.RESULT_OK && requestCode == Constants.ADDRESS_RESULT) {
            profileViewModel.setAddress(data.getDoubleExtra("lat", 0), data.getDoubleExtra("lng", 0));
        }
        super.onActivityResult(requestCode, resultCode, data);
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        profileViewModel.reset();

    }

}
