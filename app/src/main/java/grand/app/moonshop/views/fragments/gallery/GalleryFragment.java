package grand.app.moonshop.views.fragments.gallery;


import android.os.Bundle;

import androidx.databinding.DataBindingUtil;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import grand.app.moonshop.R;
import grand.app.moonshop.base.BaseFragment;
import grand.app.moonshop.databinding.FragmentInstitutionAdsBinding;
import grand.app.moonshop.models.shop.ShopData;
import grand.app.moonshop.utils.helper.AppUtils;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.viewmodels.gallery.GalleryViewModel;
import grand.app.moonshop.viewmodels.institution.InstitutionAdsViewModel;


/*
TODO : NEVER USED
 */

public class GalleryFragment extends BaseFragment {


    private FragmentInstitutionAdsBinding fragmentGalleryBinding;
    private InstitutionAdsViewModel institutionAdsViewModel;
    private GalleryViewModel galleryViewModel;
    ShopData shopData;
    String url = "";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        fragmentGalleryBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_gallery, container, false);
        getData();
        bind();
        setEvent();
        return fragmentGalleryBinding.getRoot();
    }

    private void setEvent() {
        if (getArguments() != null && getArguments().containsKey(Constants.URL)) {
            url = getArguments().getString(Constants.URL);
        }
    }

    private void bind() {
        galleryViewModel = new GalleryViewModel(url);
        AppUtils.initVerticalRV(fragmentGalleryBinding.rvAds, fragmentGalleryBinding.rvAds.getContext(), 3);
    }

    private void getData() {

    }

}
