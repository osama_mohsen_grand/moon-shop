package grand.app.moonshop.views.fragments.ads;


import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moonshop.R;
import grand.app.moonshop.adapter.AlbumDetailsAdapter;
import grand.app.moonshop.base.BaseFragment;
import grand.app.moonshop.customviews.dialog.DialogConfirm;
import grand.app.moonshop.databinding.FragmentAdsBinding;
import grand.app.moonshop.models.app.Mutable;
import grand.app.moonshop.models.shop.ShopData;
import grand.app.moonshop.utils.helper.AppUtils;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.utils.dialog.BottomSheetDialogHelper;
import grand.app.moonshop.utils.dialog.DialogHelperInterface;
import grand.app.moonshop.utils.dialog.DialogHelperSelectedInterface;
import grand.app.moonshop.utils.resources.ResourceManager;
import grand.app.moonshop.utils.storage.user.UserHelper;
import grand.app.moonshop.viewmodels.famous.ads.FamousAdsViewModel;
import grand.app.moonshop.views.activities.BaseActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class AdsFragment extends BaseFragment {

    private FragmentAdsBinding fragmentAdsBinding;
    private FamousAdsViewModel famousAdsViewModel;
    public String type_form = "", tab = "";
    private AlbumDetailsAdapter albumDetailsAdapter;
    private BottomSheetDialogHelper bottomSheetDialogHelper;
    private ShopData shopData = null;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        UserHelper.saveKey(Constants.RELOAD, Constants.FALSE);
        fragmentAdsBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_ads, container, false);
        getData();
        bind();
        setEvent();
        return fragmentAdsBinding.getRoot();

    }

    @Override
    public void onResume() {
        super.onResume();
        if (UserHelper.retrieveKey(Constants.RELOAD).equals(Constants.TRUE) && famousAdsViewModel != null)
            famousAdsViewModel.callService();

    }

    private void getData() {
        if (getArguments() != null && getArguments().containsKey(Constants.INSTITUTIONS)) {
            shopData = (ShopData) getArguments().getSerializable(Constants.INSTITUTIONS);
        }
        if (getArguments() != null && getArguments().containsKey(Constants.TYPE)) {
            type_form = getArguments().getString(Constants.TYPE);
        }
    }

    private void bind() {
        famousAdsViewModel = new FamousAdsViewModel();
        bottomSheetDialogHelper = new BottomSheetDialogHelper(getActivityBase());
        AppUtils.initVerticalRV(fragmentAdsBinding.rvFamousAlbumDetails, fragmentAdsBinding.rvFamousAlbumDetails.getContext(), 3);
        fragmentAdsBinding.setFamousAdsViewModel(famousAdsViewModel);
    }


    private void setEvent() {
        famousAdsViewModel.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                String action = (String) o;
                handleActions(action, famousAdsViewModel.getAdsRepository().getMessage());
                assert action != null;
                if (action.equals(Constants.SUCCESS)) {
                    albumDetailsAdapter = new AlbumDetailsAdapter(famousAdsViewModel.getAdsRepository().getAdsResponse().data);
                    fragmentAdsBinding.rvFamousAlbumDetails.setAdapter(albumDetailsAdapter);
                    setEventAdapter();
                } else if (action.equals(Constants.ADD)) {
                    Intent intent = new Intent(context, BaseActivity.class);
                    intent.putExtra(Constants.PAGE, Constants.ADD_ADS);
                    intent.putExtra(Constants.NAME_BAR, getString(R.string.add_new_advertisement));
                    startActivity(intent);
                } else if (action.equals(Constants.FILTER)) {
                    if (bottomSheetDialogHelper.filterDialog == null) {
                        bottomSheetDialogHelper.filterFamousShopAds(famousAdsViewModel.getAdsRepository().getFilterShopAdsResponse().data, true, new DialogHelperSelectedInterface() {
                            @Override
                            public void onClickListener(Dialog dialog, String type, Object object) {
                                if (type.equals(Constants.ARRAY)) {
                                    ArrayList<Integer> list = (ArrayList<Integer>) object;
                                    if (list != null && list.size() > 0) {
                                        famousAdsViewModel.submitFilter(list);
                                    }
                                }
                            }
                        });
                    } else {
                        bottomSheetDialogHelper.filterDialog.show();
                    }
                } else if (action.equals(Constants.DELETED)) {
                    toastMessage(famousAdsViewModel.getAdsRepository().getMessage(), R.drawable.ic_check_white, R.color.colorPrimary);
                    albumDetailsAdapter.remove(famousAdsViewModel.category_delete_position);
                }
            }
        });
    }


    private void setEventAdapter() {
        albumDetailsAdapter.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                Mutable mutable = (Mutable) o;
                if (mutable.type.equals(Constants.IMAGE)) {
                    Intent intent = new Intent(context, BaseActivity.class);
                    intent.putExtra(Constants.PAGE, Constants.ZOOM);
                    intent.putExtra(Constants.NAME_BAR, famousAdsViewModel.getAdsRepository().getAdsResponse().data.get(mutable.position).name);
                    Bundle bundle = new Bundle();
                    bundle.putString(Constants.IMAGE, famousAdsViewModel.getAdsRepository().getAdsResponse().data.get(mutable.position).image);
                    intent.putExtra(Constants.BUNDLE, bundle);
                    startActivityForResult(intent, Constants.RELOAD_RESULT);
                } else if (mutable.type.equals(Constants.VIDEO)) {
                    Intent intent = new Intent(context, BaseActivity.class);
                    intent.putExtra(Constants.PAGE, Constants.VIDEO);
                    intent.putExtra(Constants.NAME_BAR, famousAdsViewModel.getAdsRepository().getAdsResponse().data.get(mutable.position).name);
                    Bundle bundle = new Bundle();
//                    Timber.e("video:"+famousAdsViewModel.getAdsRepository().getAdsResponse().data.get(mutable.position).capture);
                    bundle.putString(Constants.VIDEO, famousAdsViewModel.getAdsRepository().getAdsResponse().data.get(mutable.position).capture);
                    intent.putExtra(Constants.BUNDLE, bundle);
                    startActivity(intent);
                } else if (mutable.type.equals(Constants.DELETE)) {
                    new DialogConfirm(getActivity())
                            .setTitle(ResourceManager.getString(R.string.remove_product))
                            .setMessage(ResourceManager.getString(R.string.do_you_want_delete_image))
                            .setActionText(ResourceManager.getString(R.string.remove_item))
                            .setActionCancel(ResourceManager.getString(R.string.cancel))
                            .show(new DialogHelperInterface() {
                                @Override
                                public void OnClickListenerContinue(Dialog dialog, View view) {
                                    famousAdsViewModel.category_delete_position = mutable.position;
                                    famousAdsViewModel.delete(famousAdsViewModel.getAdsRepository().getAdsResponse().data.get(mutable.position).id);
                                }
                            });
                } else if (mutable.type.equals(Constants.EDIT)) {
                    Intent intent = new Intent(context, BaseActivity.class);
                    intent.putExtra(Constants.PAGE, Constants.ADD_ADS);
                    intent.putExtra(Constants.NAME_BAR, ResourceManager.getString(R.string.edit_album));
                    Bundle bundle = new Bundle();
                    bundle.putString(Constants.NAME, famousAdsViewModel.getAdsRepository().getAdsResponse().data.get(mutable.position).name);
                    bundle.putSerializable(Constants.ADS, famousAdsViewModel.getAdsRepository().getAdsResponse().data.get(mutable.position));
                    intent.putExtra(Constants.BUNDLE, bundle);
                    startActivityForResult(intent, Constants.RELOAD_RESULT);
                }
            }
        });
    }

}
