package grand.app.moonshop.views.fragments.product;


import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.theartofdev.edmodo.cropper.CropImage;
import com.yalantis.ucrop.UCrop;

import java.io.File;

import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moonshop.R;
import grand.app.moonshop.adapter.AlbumEditImagesAdapter;
import grand.app.moonshop.adapter.SizeColorAdapter;
import grand.app.moonshop.base.BaseFragment;
import grand.app.moonshop.customviews.dialog.DialogConfirm;
import grand.app.moonshop.databinding.FragmentAddProductConsumerSectoralBinding;
import grand.app.moonshop.models.app.Mutable;
import grand.app.moonshop.models.base.IdNameImage;
import grand.app.moonshop.models.product.details.ProductDetails;
import grand.app.moonshop.utils.helper.AppUtils;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.utils.dialog.DialogHelperInterface;
import grand.app.moonshop.utils.images.ImageLoaderHelper;
import grand.app.moonshop.utils.resources.ResourceManager;
import grand.app.moonshop.utils.upload.FileOperations;
import grand.app.moonshop.viewmodels.product.AddProductConsumerSectoralViewModel;
import grand.app.moonshop.vollyutils.VolleyFileObject;
import timber.log.Timber;

import static android.app.Activity.RESULT_OK;

/**
 * A simple {@link Fragment} subclass.
 */
public class AddProductConsumerSectoralFragment extends BaseFragment {

    View rootView;
    private FragmentAddProductConsumerSectoralBinding fragmentAddProductConsumerSectoralBinding;
    private AddProductConsumerSectoralViewModel addProductConsumerViewModel;
    int id = -1;
    ProductDetails product = null;
    SizeColorAdapter sizeAdapter, colorAdapter;
    public AlbumEditImagesAdapter albumEditImagesAdapter;


    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentAddProductConsumerSectoralBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_add_product_consumer_sectoral, container, false);
        getData();
        bind();
        setEvent();
        setImageAdapter();
        return rootView;
    }

    private void setImageAdapter() {
        albumEditImagesAdapter = new AlbumEditImagesAdapter("1", addProductConsumerViewModel.productImages);
        AppUtils.initVerticalRV(fragmentAddProductConsumerSectoralBinding.rvImages, fragmentAddProductConsumerSectoralBinding.rvImages.getContext(), 3);
        fragmentAddProductConsumerSectoralBinding.rvImages.setAdapter(albumEditImagesAdapter);
        setEventAdapterImages();
    }


    private void setEventAdapterImages() {
        albumEditImagesAdapter.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                Mutable mutable = (Mutable) o;
                if (mutable.type.equals(Constants.SUBMIT)) {
                    IdNameImage idNameImage = albumEditImagesAdapter.idNameImages.get(mutable.position);
                    if (idNameImage == null || (idNameImage.image.equals("") && idNameImage.volleyFileObject == null)) {
                        addProductConsumerViewModel.new_image_position = mutable.position;
                        FileOperations.pickImage(context, AddProductConsumerSectoralFragment.this, Constants.FILE_TYPE_IMAGE);
                    } else if (idNameImage.volleyFileObject != null) {
                        addProductConsumerViewModel.new_image_position = mutable.position;
                        FileOperations.pickImage(context, AddProductConsumerSectoralFragment.this, Constants.FILE_TYPE_IMAGE);
                    }
                } else if (mutable.type.equals(Constants.DELETE)) {
                    IdNameImage idNameImage = albumEditImagesAdapter.idNameImages.get(mutable.position);
                    if (idNameImage.volleyFileObject == null) {
                        new DialogConfirm(getActivity())
                                .setTitle(ResourceManager.getString(R.string.remove_item))
                                .setMessage(ResourceManager.getString(R.string.do_you_want_delete_image))
                                .setActionText(ResourceManager.getString(R.string.remove_item))
                                .setActionCancel(ResourceManager.getString(R.string.cancel))
                                .show(new DialogHelperInterface() {
                                    @Override
                                    public void OnClickListenerContinue(Dialog dialog, View view) {
                                        addProductConsumerViewModel.category_delete_position = mutable.position;
                                        addProductConsumerViewModel.delete(Integer.parseInt(String.valueOf(addProductConsumerViewModel.getProductRepository().getProductDetailsResponse().mData.images.get(mutable.position).id)));
                                    }
                                });
                    } else {
                        albumEditImagesAdapter.remove(mutable.position);
                    }
                }
            }
        });
    }


    private void getData() {
        if (getArguments() != null && getArguments().containsKey(Constants.ID))
            id = getArguments().getInt(Constants.ID);
        if (getArguments() != null && getArguments().containsKey(Constants.PRODUCT)) {
            product = (ProductDetails) getArguments().getSerializable(Constants.PRODUCT);
        }
        if (getArguments() != null && getArguments().containsKey(Constants.PRODUCT_ID)) {
            product_id = getArguments().getInt(Constants.PRODUCT_ID);
        }
    }

    int product_id = -1;

    private void bind() {
        addProductConsumerViewModel = new AddProductConsumerSectoralViewModel(id, product);
        if (product_id != -1) {
            addProductConsumerViewModel.show.set(false);
            addProductConsumerViewModel.getProductDetails(product_id);
        } else
            addProductConsumerViewModel.show.set(true);
        AppUtils.initVerticalRV(fragmentAddProductConsumerSectoralBinding.rvSize, fragmentAddProductConsumerSectoralBinding.rvSize.getContext(), 1);
        AppUtils.initVerticalRV(fragmentAddProductConsumerSectoralBinding.rvColor, fragmentAddProductConsumerSectoralBinding.rvColor.getContext(), 1);
        fragmentAddProductConsumerSectoralBinding.rvSize.setVisibility(View.GONE);
        fragmentAddProductConsumerSectoralBinding.rvColor.setVisibility(View.GONE);


        fragmentAddProductConsumerSectoralBinding.setAddProductConsumerSectoralViewModel(addProductConsumerViewModel);
        rootView = fragmentAddProductConsumerSectoralBinding.getRoot();
    }

    boolean cover = false;

    private void setEvent() {
        Log.d(TAG,"event");
        addProductConsumerViewModel.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                String action = (String) o;
                handleActions(action, addProductConsumerViewModel.getProductRepository().getMessage());
                assert action != null;
                if (action.equals(Constants.SUCCESS)) {
                    toastMessage(addProductConsumerViewModel.getProductRepository().getMessage(), R.drawable.ic_check_white, R.color.colorPrimary);
                    reloadPrevious();
                } else if (action.equals(Constants.SELECT_IMAGE)) {
                    Log.d(TAG,"cover true");
                    cover = true;
//                    pickImageDialogSelect();
                    FileOperations.pickImage(context, AddProductConsumerSectoralFragment.this, Constants.FILE_TYPE_IMAGE);
                } else if (action.equals(Constants.ERROR)) {//ex: like image
                    showError(addProductConsumerViewModel.baseError);
                } else if (action.equals(Constants.DELETED)) {
//                    addAdvertisementViewModel.getAdsRepository().getAdsDetailsResponse().data.images.remove(addAdvertisementViewModel.category_delete_position);
                    albumEditImagesAdapter.remove(addProductConsumerViewModel.category_delete_position);
                    addProductConsumerViewModel.productImages.set(addProductConsumerViewModel.category_delete_position, null);
                } else if (action.equals(Constants.PRODUCT)) {

                    ImageLoaderHelper.ImageLoaderLoad(context, addProductConsumerViewModel.productRepository.getProductDetailsResponse().mData.image, fragmentAddProductConsumerSectoralBinding.imgAddProduct);

                    fragmentAddProductConsumerSectoralBinding.imgAddUploadImage.setVisibility(View.GONE);
                    fragmentAddProductConsumerSectoralBinding.tvAddUpload.setVisibility(View.GONE);
                    addProductConsumerViewModel.updateProductUi();
                    albumEditImagesAdapter.update(addProductConsumerViewModel.productImages);

                }
            }
        });

        addProductConsumerViewModel.mMutableLiveDataShop.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                String action = (String) o;
                handleActions(action, addProductConsumerViewModel.getShopRepository().getMessage());
                if (action.equals(Constants.SIZE_COLOR)) {
                    sizeAdapter = new SizeColorAdapter(addProductConsumerViewModel.getShopRepository().getShopSizeColorResponse().sizes, false);
                    colorAdapter = new SizeColorAdapter(addProductConsumerViewModel.getShopRepository().getShopSizeColorResponse().colors, true);
                    fragmentAddProductConsumerSectoralBinding.rvSize.setAdapter(sizeAdapter);
                    fragmentAddProductConsumerSectoralBinding.rvColor.setAdapter(colorAdapter);
                    setEventAdapter();
                }
            }
        });

        fragmentAddProductConsumerSectoralBinding.edtProductSizeSelect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG,"edtProductSizeSelect click");
                fragmentAddProductConsumerSectoralBinding.rvColor.setVisibility(View.GONE);
                fragmentAddProductConsumerSectoralBinding.rvSize.setVisibility(View.VISIBLE);
            }
        });
        fragmentAddProductConsumerSectoralBinding.edtProductColorSelect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG,"edtProductColorSelect click");
                fragmentAddProductConsumerSectoralBinding.rvColor.setVisibility(View.VISIBLE);
                fragmentAddProductConsumerSectoralBinding.rvSize.setVisibility(View.GONE);
            }
        });
    }

    private void setEventAdapter() {
        sizeAdapter.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                Mutable mutable = (Mutable) o;
                setSelect(mutable, Constants.SIZE);

            }
        });
        colorAdapter.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                Mutable mutable = (Mutable) o;
                setSelect(mutable, Constants.COLOR);
            }
        });
    }

    private void setSelect(Mutable mutable, String sizeColor) {
        if (sizeColor.equals(Constants.SIZE)) {
            fragmentAddProductConsumerSectoralBinding.edtProductSizeSelect.setText(addProductConsumerViewModel.getShopRepository().getShopSizeColorResponse().sizes.get(mutable.position).name);
            addProductConsumerViewModel.addProductConsumerSectoralRequest.sizeId = addProductConsumerViewModel.getShopRepository().getShopSizeColorResponse().sizes.get(mutable.position).id;
        } else {
            fragmentAddProductConsumerSectoralBinding.edtProductColorSelect.setText(addProductConsumerViewModel.getShopRepository().getShopSizeColorResponse().colors.get(mutable.position).name);
            fragmentAddProductConsumerSectoralBinding.edtProductColorSelect.setTextColor(Color.parseColor(addProductConsumerViewModel.getShopRepository().getShopSizeColorResponse().colors.get(mutable.position).name));
            addProductConsumerViewModel.addProductConsumerSectoralRequest.colorId = addProductConsumerViewModel.getShopRepository().getShopSizeColorResponse().colors.get(mutable.position).id;
        }
        fragmentAddProductConsumerSectoralBinding.rvSize.setVisibility(View.GONE);
        fragmentAddProductConsumerSectoralBinding.rvColor.setVisibility(View.GONE);
    }

    private static final String TAG = "AddProductConsumerSecto";

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        //super method removed
        Log.d(TAG,"requestCode: "+requestCode);
        Timber.e("onActivityResult:" + requestCode);
//        if (requestCode == Constants.FILE_TYPE_IMAGE) {
//            VolleyFileObject volleyFileObject1 = FileOperations.getVolleyFileObject(getActivity(), data, "url", Constants.FILE_TYPE_IMAGE);
//            File file = new File(getCacheDir(), Constants.IMAGE + ".png");
//            UCrop.of(Uri.fromFile(volleyFileObject1.getFile()), Uri.fromFile(file))
//                    .start(context, AddProductConsumerSectoralFragment.this);
//        }else if (requestCode == UCrop.REQUEST_CROP && data != null) {
//            final Uri resultUri = UCrop.getOutput(data);
//            if (resultUri != null) {
//                VolleyFileObject volleyFileObject = new VolleyFileObject(Constants.IMAGE, resultUri.getPath(), Constants.FILE_TYPE_IMAGE);
//                fragmentAddProductConsumerSectoralBinding.imgAddProduct.setImageURI(Uri.parse(String.valueOf(new File(volleyFileObject.getFilePath()))));
//                addProductConsumerViewModel.setImage(volleyFileObject);
//            }
//        }

        if (requestCode == Constants.FILE_TYPE_IMAGE) {
            VolleyFileObject volleyFileObject1 = FileOperations.getVolleyFileObject(getActivity(), data, "url", Constants.FILE_TYPE_IMAGE);
//            File file = new File(volleyFileObject1.getFilePath());
            File file = new File(volleyFileObject1.getFilePath());
//            getCacheDir(), Constants.IMAGE+addProductConsumerViewModel.new_image_position + ".png"
            UCrop.of(Uri.fromFile(volleyFileObject1.getFile()), Uri.fromFile(file))
                    .start(context, AddProductConsumerSectoralFragment.this);

        } else if (requestCode == UCrop.REQUEST_CROP && data != null) {
            Log.d(TAG, "doner");
            final Uri resultUri = UCrop.getOutput(data);
            if (cover) {
                cover = false;
//                VolleyFileObject volleyFileObject = FileOperations.getVolleyFileObject(getActivity(), data, Constants.IMAGE, Constants.FILE_TYPE_IMAGE);
                VolleyFileObject volleyFileObject = new VolleyFileObject(Constants.IMAGE, resultUri.getPath(), Constants.FILE_TYPE_IMAGE);
                fragmentAddProductConsumerSectoralBinding.imgAddProduct.setImageURI(null);
                fragmentAddProductConsumerSectoralBinding.imgAddProduct.setImageURI(Uri.parse(String.valueOf(new File(volleyFileObject.getFilePath()))));
                addProductConsumerViewModel.setImage(volleyFileObject);
            } else {

                if (resultUri != null) {
                    Log.d(TAG, addProductConsumerViewModel.new_image_position + "");
                    Log.d(TAG, addProductConsumerViewModel.productImages.size() + "");
                    int pos = addProductConsumerViewModel.new_image_position + 1 -
                            addProductConsumerViewModel.productImages.size();
                    VolleyFileObject volleyFileObject = new VolleyFileObject("product_images[" + pos + "]", resultUri.getPath(), Constants.FILE_TYPE_IMAGE);
                    Log.d("path", volleyFileObject.getFilePath());
                    albumEditImagesAdapter.update(addProductConsumerViewModel.new_image_position, volleyFileObject);
                    addProductConsumerViewModel.productImages.set(addProductConsumerViewModel.new_image_position,
                            albumEditImagesAdapter.idNameImages.get(addProductConsumerViewModel.new_image_position));
                }
            }
        }
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE && data != null) {

            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();
                UCrop.of(resultUri, resultUri).start(context, AddProductConsumerSectoralFragment.this);
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Toast.makeText(context, "" + getString(R.string.error_failed_pick_gallery_image), Toast.LENGTH_SHORT).show();
            }
        } else if (requestCode == Constants.FILE_TYPE_IMAGE_COVER) {
//            VolleyFileObject volleyFileObject = FileOperations.getVolleyFileObject(getActivity(), data, Constants.IMAGE, Constants.FILE_TYPE_IMAGE);
//            fragmentAddProductConsumerSectoralBinding.imgAddProduct.setImageURI(Uri.parse(String.valueOf(new File(volleyFileObject.getFilePath()))));
//            addProductConsumerViewModel.setImage(volleyFileObject);
        }


        super.onActivityResult(requestCode, resultCode, data);
    }

}
