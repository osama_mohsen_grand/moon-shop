package grand.app.moonshop.views.fragments;


import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moonshop.R;
import grand.app.moonshop.adapter.PackageAdapter;
import grand.app.moonshop.base.BaseFragment;
import grand.app.moonshop.base.ParentActivity;
import grand.app.moonshop.databinding.FragmentPackageBinding;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.viewmodels.packagePayment.PackageViewModel;
import grand.app.moonshop.views.activities.MainActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class PackageFragment extends BaseFragment {

    View rootView;
    private FragmentPackageBinding fragmentPackageBinding;
    private PackageViewModel packageViewModel;
    private PackageAdapter packageAdapter;




    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        fragmentPackageBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_package, container, false);
        bind();
        setEvent();
        return rootView;
    }

    private void bind() {
        packageViewModel = new PackageViewModel();
//        AppUtils.initVerticalRV(fragmentPackageBinding.rvPackages, fragmentPackageBinding.rvPackages.getContext(), 1);
        fragmentPackageBinding.setPackageViewModel(packageViewModel);
        rootView = fragmentPackageBinding.getRoot();
    }

    private static final String TAG = "SplashFragment";

    private void setEvent() {
        packageViewModel.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                String action = (String) o;
                handleActions(action, packageViewModel.getPackageRepository().getMessage());
                assert action != null;
                if(action.equals(Constants.PACKAGES)){
                    packageAdapter = new PackageAdapter(packageViewModel.getPackageRepository().getPackageResponse().mData);
                    fragmentPackageBinding.rvPackages.setAdapter(packageAdapter);
                    setEventAdapter();
                }else if(action.equals(Constants.ADD)){
                    ((ParentActivity)context).finishAffinity();
                    startActivity(new Intent(context, MainActivity.class));
                }
            }
        });
    }

    private void setEventAdapter() {
        packageAdapter.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                int position = (int) o;
                packageViewModel.addPackage(packageViewModel.getPackageRepository().getPackageResponse().mData.get(position).mId);
            }
        });
    }

}
