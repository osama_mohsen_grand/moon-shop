package grand.app.moonshop.views.fragments.chat;


import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moonshop.R;
import grand.app.moonshop.adapter.ChatListAdapter;
import grand.app.moonshop.base.BaseFragment;
import grand.app.moonshop.databinding.FragmentChatListBinding;
import grand.app.moonshop.models.app.Mutable;
import grand.app.moonshop.models.chat.list.ChatList;
import grand.app.moonshop.models.user.profile.User;
import grand.app.moonshop.utils.helper.AppUtils;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.utils.resources.ResourceManager;
import grand.app.moonshop.utils.storage.user.UserHelper;
import grand.app.moonshop.viewmodels.chat.list.ChatListViewModel;
import grand.app.moonshop.views.activities.BaseActivity;
import grand.app.moonshop.views.activities.MainActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class ChatListFragment extends BaseFragment {


    private View rootView;
    private FragmentChatListBinding fragmentChatBinding;
    private ChatListViewModel chatListViewModel;
    private ChatListAdapter chatListAdapter;


    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        fragmentChatBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_chat_list, container, false);
        bind();
        setEvent();
        return rootView;
    }


    private void bind() {
        chatListViewModel = new ChatListViewModel();
        AppUtils.initVerticalRV(fragmentChatBinding.rvChatList, fragmentChatBinding.rvChatList.getContext(), 1);
        fragmentChatBinding.setChatListViewModel(chatListViewModel);
        rootView = fragmentChatBinding.getRoot();
    }

    private void setEvent() {
        chatListViewModel.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                String action = (String) o;
                handleActions(action, chatListViewModel.getChatRepository().getMessage());
                assert action != null;
                if (action.equals(Constants.CHAT_LIST)) {

                    if (chatListViewModel.getChatRepository().getChatListResponse().data.size() == 0) {
                        chatListViewModel.noData();
                    } else {
                        chatListAdapter = new ChatListAdapter(chatListViewModel.getChatRepository().getChatListResponse().data);
                        fragmentChatBinding.rvChatList.setAdapter(chatListAdapter);
                        setEventAdapter();
                    }


                }
//                else if (action.equals(Constants.ADD)) {
//                    Intent intent = new Intent(context, BaseActivity.class);
//                    intent.putExtra(Constants.PAGE, Constants.ADD_CATEGORY);
//                    intent.putExtra(Constants.NAME_BAR, ResourceManager.getString(R.string.add_category));
////                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
////                        startActivityForResult(intent, Constants.RELOAD_RESULT,ActivityOptions.makeSceneTransitionAnimation(getActivity()).toBundle());
////                    }else
//                    startActivityForResult(intent, Constants.RELOAD_RESULT);
//
//
//                } else if (action.equals(Constants.DELETE)) {
//                    toastMessage(chatListViewModel.getCategoryRepository().getMessage(), R.drawable.ic_check_white, R.color.colorPrimary);
//                    chatListAdapter.remove(chatListViewModel.service_delete_position);
//                }
            }
        });
    }


    private void setEventAdapter() {
        chatListAdapter.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                Mutable mutable = (Mutable) o;
                if (mutable.type.equals(Constants.CHAT_DETAILS)) {
                    ChatList chatList = chatListViewModel.getChatRepository().getChatListResponse().data.get(mutable.position);
                    int newCount = UserHelper.getUserDetails().chats_count - Integer.parseInt(chatList.chatsCount);
                    if (requireActivity() instanceof MainActivity) {
                        User user = UserHelper.getUserDetails();
                        user.chats_count = newCount;
                        UserHelper.saveUserDetails(user);
                        MainActivity mainActivity = (MainActivity) requireActivity();
                        mainActivity.chatCount();
                    }
                    Intent intent = new Intent(context, BaseActivity.class);
                    intent.putExtra(Constants.PAGE, Constants.CHAT_DETAILS);
                    intent.putExtra(Constants.NAME_BAR, ResourceManager.getString(R.string.chat));
                    Bundle bundle = new Bundle();
                    bundle.putInt(Constants.ID_CHAT, chatList.id);
                    bundle.putInt(Constants.TYPE, chatList.type);
                    bundle.putString(Constants.NAME, chatList.name);
                    bundle.putString(Constants.IMAGE, chatList.image);
                    bundle.putBoolean(Constants.ALLOW_CHAT, true);
                    bundle.putBoolean(Constants.CHAT_FIRST, true);
                    intent.putExtra(Constants.BUNDLE, bundle);
                    startActivity(intent);
                }

            }
        });
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        chatListViewModel.reset();

    }

}
