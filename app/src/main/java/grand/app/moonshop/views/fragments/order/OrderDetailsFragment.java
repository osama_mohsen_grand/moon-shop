package grand.app.moonshop.views.fragments.order;


import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moonshop.R;
import grand.app.moonshop.adapter.OrderDetailsAdapter;
import grand.app.moonshop.base.BaseFragment;
import grand.app.moonshop.databinding.FragmentOrderDetailsBinding;
import grand.app.moonshop.models.order.details.OrderDetails;
import grand.app.moonshop.utils.helper.AppUtils;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.utils.resources.ResourceManager;
import grand.app.moonshop.viewmodels.order.details.OrderDetailsViewModel;
import grand.app.moonshop.views.activities.BaseActivity;
import grand.app.moonshop.views.activities.MapAddressActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class OrderDetailsFragment extends BaseFragment {

    View rootView;
    private FragmentOrderDetailsBinding fragmentOrderDetailsBinding;
    private OrderDetailsViewModel orderDetailsViewModel;
    OrderDetailsAdapter orderDetailsAdapter;
    int order_id = 0;

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        fragmentOrderDetailsBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_order_details, container, false);
        getData();
        bind();
        setEvent();
        return rootView;
    }

    private void getData() {
        if (getArguments() != null && getArguments().containsKey(Constants.ID))
            order_id = getArguments().getInt(Constants.ID);
    }

    private void bind() {
        orderDetailsViewModel = new OrderDetailsViewModel(order_id);
        fragmentOrderDetailsBinding.setOrderDetailsViewModel(orderDetailsViewModel);
        AppUtils.initVerticalRV(fragmentOrderDetailsBinding.rvOrderDetails, fragmentOrderDetailsBinding.rvOrderDetails.getContext(), 1);
        rootView = fragmentOrderDetailsBinding.getRoot();
    }

    private void setEvent() {
        orderDetailsViewModel.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                String action = (String) o;
                handleActions(action, orderDetailsViewModel.getOrderRepository().getMessage());
                if (action.equals(Constants.ORDER_DETAILS)) {
                    orderDetailsAdapter = new OrderDetailsAdapter(orderDetailsViewModel.getOrderRepository().getOrderDetailsResponse().data.products);
                    fragmentOrderDetailsBinding.rvOrderDetails.setAdapter(orderDetailsAdapter);
                    if (orderDetailsViewModel.getOrderRepository().getOrderDetailsResponse().data.delegate != null) {
                        orderDetailsViewModel.haveDelegate = true;
                    }
                    orderDetailsViewModel.setStatusChat();
                    orderDetailsViewModel.showPage(true);
                    orderDetailsViewModel.setShowStatus();




                } else if (action.equals(Constants.DELEGATE_LOCATION)) {
                    Intent intent = new Intent(context, MapAddressActivity.class);
                    intent.putExtra(Constants.LAT, Double.parseDouble(orderDetailsViewModel.getOrderRepository().getOrderDetailsResponse().data.delegate.lat));
                    intent.putExtra(Constants.LNG, Double.parseDouble(orderDetailsViewModel.getOrderRepository().getOrderDetailsResponse().data.delegate.lng));
                    startActivity(intent);
                } else if (action.equals(Constants.DELEGATE_LOCATION)) {
                    Intent intent = new Intent(context, MapAddressActivity.class);
                    intent.putExtra(Constants.LAT, Double.parseDouble(orderDetailsViewModel.getOrderRepository().getOrderDetailsResponse().data.delegate.lat));
                    intent.putExtra(Constants.LNG, Double.parseDouble(orderDetailsViewModel.getOrderRepository().getOrderDetailsResponse().data.delegate.lng));
                    startActivity(intent);
                } else if (action.equals(Constants.CHAT)) {
//                    Intent intent = new Intent(context, BaseActivity.class);
//                    intent.putExtra(Constants.PAGE, Constants.CHAT_DETAILS);
//                    intent.putExtra(Constants.NAME_BAR, ResourceManager.getString(R.string.chat));
//                    Bundle bundle = new Bundle();
//                    OrderDetails orderDetails = orderDetailsViewModel.getOrderRepository().getOrderDetailsResponse().data;
//                    bundle.putInt(Constants.ID, orderDetails.orderId);
//                    bundle.putInt(Constants.TYPE, orderDetails.delegate.type);
//                    bundle.putString(Constants.NAME, orderDetails.delegate.name);
//                    bundle.putString(Constants.IMAGE, orderDetails.delegate.image);
//                    intent.putExtra(Constants.BUNDLE, bundle);
//                    startActivity(intent);


                    Intent intent = new Intent(context, BaseActivity.class);
                    intent.putExtra(Constants.PAGE,Constants.CHAT_DETAILS);
                    intent.putExtra(Constants.NAME_BAR, ResourceManager.getString(R.string.chat));
                    Bundle bundle = new Bundle();
                    OrderDetails orderDetails = orderDetailsViewModel.getOrderRepository().getOrderDetailsResponse().data;
                    bundle.putInt(Constants.ID,orderDetails.orderId);
                    bundle.putInt(Constants.TYPE,orderDetails.delegate.type);
                    bundle.putString(Constants.NAME,orderDetails.delegate.name);
                    bundle.putString(Constants.IMAGE,orderDetails.delegate.image);
                    boolean allowChat = (orderDetailsViewModel.getOrderRepository().getOrderDetailsResponse().data.statusId==4 ? false : true);
                    bundle.putBoolean(Constants.ALLOW_CHAT,allowChat);
                    intent.putExtra(Constants.BUNDLE,bundle);
                    startActivity(intent);

                }
                else if (action.equals(Constants.CALL)) {
                    phoneCall();

                }
            }
        });
    }

    private void onCallBtnClick(){
        if (Build.VERSION.SDK_INT < 23) {
            phoneCall();
        }else {

            if (ActivityCompat.checkSelfPermission(requireActivity(),
                    Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {

                phoneCall();
            }else {
                final String[] PERMISSIONS_STORAGE = {Manifest.permission.CALL_PHONE};
                //Asking request Permissions
                ActivityCompat.requestPermissions(requireActivity(), PERMISSIONS_STORAGE, 9);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        boolean permissionGranted = false;
        switch(requestCode){
            case 9:
                permissionGranted = grantResults[0]== PackageManager.PERMISSION_GRANTED;
                break;
        }
        if(permissionGranted){
            phoneCall();
        }else {
            Toast.makeText(requireActivity(), "You don't assign permission.", Toast.LENGTH_SHORT).show();
        }
    }

    private void phoneCall(){
        if (ActivityCompat.checkSelfPermission(requireActivity(),
                Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {
            Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + orderDetailsViewModel.getOrderRepository().getOrderDetailsResponse().data.delegate.phone));
            startActivity(intent);
        }else{
            requestPermissions(new String[]{Manifest.permission.CALL_PHONE},9);
        }
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        if (orderDetailsViewModel != null) orderDetailsViewModel.reset();
    }


}
