package grand.app.moonshop.views.fragments.auth;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;

import com.theartofdev.edmodo.cropper.CropImage;
import com.yalantis.ucrop.UCrop;

import java.io.File;
import java.util.ArrayList;
import java.util.Objects;

import grand.app.moonshop.MultipleImageSelect.activities.AlbumSelectActivity;
import grand.app.moonshop.MultipleImageSelect.models.Image;
import grand.app.moonshop.R;
import grand.app.moonshop.base.BaseFragment;
import grand.app.moonshop.base.ParentActivity;
import grand.app.moonshop.customviews.facebook.FacebookModel;
import grand.app.moonshop.databinding.FragmentRegister2Binding;
import grand.app.moonshop.models.app.AccountTypeModel;
import grand.app.moonshop.models.app.AppMoon;
import grand.app.moonshop.models.service.Service;
import grand.app.moonshop.models.user.register.RegisterShopRequest;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.utils.PopUp.PopUpMenuHelper;
import grand.app.moonshop.utils.resources.ResourceManager;
import grand.app.moonshop.utils.upload.FileOperations;
import grand.app.moonshop.viewmodels.user.RegisterViewModel;
import grand.app.moonshop.views.activities.BaseActivity;
import grand.app.moonshop.views.activities.MainActivity;
import grand.app.moonshop.views.activities.MapAddressActivity;
import grand.app.moonshop.vollyutils.VolleyFileObject;
import timber.log.Timber;

import static android.app.Activity.RESULT_OK;
import static com.facebook.FacebookSdk.getCacheDir;

public class RegisterFragment2 extends BaseFragment {
    View rootView;

    private FragmentRegister2Binding fragmentRegister2Binding;
    private RegisterViewModel registerViewModel;
    public String type = "", phone = "";
    public PopUpMenuHelper popUpMenuHelper = new PopUpMenuHelper();
    public FacebookModel facebookModel;
    public int mainType = 0,mainFlag=0;

    //1=>accept , 2=>reject  , 3=>arrive  , 4=>start  , 5=> cancel after accept ,  6=>finished trip

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentRegister2Binding = DataBindingUtil.inflate(inflater, R.layout.fragment_register_2, container, false);
        getData();
        bind();
        setEvent();
        return rootView;
    }

    private void getData() {

        if (getArguments() != null && getArguments().containsKey(Constants.TYPE)) {
            type = getArguments().getString(Constants.TYPE);
        }
        if (getArguments() != null && getArguments().containsKey(Constants.PHONE)) {
            phone = getArguments().getString(Constants.PHONE);
        }
        if (getArguments() != null && getArguments().containsKey(Constants.FACEBOOK)) {
            facebookModel = (FacebookModel) getArguments().getSerializable(Constants.FACEBOOK);
        }
    }

    private void bind() {

        assert getArguments() != null;
        registerViewModel = new RegisterViewModel((RegisterShopRequest) getArguments().getSerializable(Constants.REGESTER_REQUEST));

        if (facebookModel != null) {

            registerViewModel.setSocial(facebookModel);
        }
        fragmentRegister2Binding.setRegisterViewModel(registerViewModel);
        initPopUp();

        registerViewModel.serviceRepository.getAccountTypes();

        rootView = fragmentRegister2Binding.getRoot();
    }

    private void pickImageDialog() {
        FileOperations.pickImage(context, RegisterFragment2.this, Constants.FILE_TYPE_IMAGE);
    }

    private void initPopUp() {

        fragmentRegister2Binding.edtRegisterAccountType.setOnClickListener(v -> {

            ArrayList<AccountTypeModel> accountTypeModels =
                    AppMoon.getAccountsType(registerViewModel.getServiceRepository().getAcountTypeResponse().getData());
            ArrayList<String> popUpModels = new ArrayList<>();
            for (AccountTypeModel accountTypeModel : accountTypeModels)
                popUpModels.add(accountTypeModel.type);

            popUpMenuHelper.openPopUp(getActivity(), fragmentRegister2Binding.edtRegisterAccountType, popUpModels, position -> {
                fragmentRegister2Binding.edtRegisterAccountType.setText(accountTypeModels.get(position).type);



                registerViewModel.changeVisibility();

                if (!accountTypeModels.get(position).id.equals(Constants.TYPE_FAMOUS_PEOPLE) &&
                        !accountTypeModels.get(position).id.equals(Constants.TYPE_PHOTOGRAPHER)) {

                    fragmentRegister2Binding.edtRegisterService.setText("");
                    registerViewModel.getServices(Integer.parseInt(accountTypeModels.get(position).id), Integer.parseInt(registerViewModel.getServiceRepository().getAcountTypeResponse().getData().get(position).getFlag()), Integer.parseInt(registerViewModel.getServiceRepository().getAcountTypeResponse().getData().get(position).getType()));

                }

                int type = Integer.parseInt(registerViewModel.getServiceRepository().getAcountTypeResponse().getData().get(position).getType());
                int flag = Integer.parseInt(registerViewModel.getServiceRepository().getAcountTypeResponse().getData().get(position).getFlag());

                mainType = type;
                mainFlag=flag;

                fragmentRegister2Binding.edtRegisterService.setVisibility(View.VISIBLE);
                fragmentRegister2Binding.tilRegisterService.setVisibility(View.VISIBLE);





                if (((type == 1 || type == 2) && flag != 3)) {
                    fragmentRegister2Binding.edtRegisterType.setVisibility(View.VISIBLE);
                    fragmentRegister2Binding.tiRegisterType.setVisibility(View.VISIBLE);
                    registerViewModel.request.setService_id("1");
                } else {
                    fragmentRegister2Binding.edtRegisterType.setVisibility(View.GONE);
                    fragmentRegister2Binding.tiRegisterType.setVisibility(View.GONE);
                }


                if (((type == 3) && (flag == 1 || flag == 2))) {
                    fragmentRegister2Binding.edtRegisterType.setVisibility(View.GONE);
                    fragmentRegister2Binding.tiRegisterType.setVisibility(View.GONE);
                    registerViewModel.request.setService_id("1");
                }

                if (type == 4 || type == 5) {
                    fragmentRegister2Binding.edtRegisterType.setVisibility(View.GONE);
                    fragmentRegister2Binding.tiRegisterType.setVisibility(View.GONE);
                    fragmentRegister2Binding.edtRegisterService.setVisibility(View.GONE);
                    fragmentRegister2Binding.tilRegisterService.setVisibility(View.GONE);
                }



                fragmentRegister2Binding.edtRegisterType.setText("");
                registerViewModel.request.setService_id("");
                registerViewModel.request.setType(""+type);


                if(flag==3){
                    registerViewModel.request.setService_id("3");
                }

                registerViewModel.request.setService_id(flag+"");

                registerViewModel.request.isHomeServiceOrConsumer.set((type == 1 || type == 2) && flag == 3);
                registerViewModel.isPhotographerOrFamous.set((type == 4 || type == 5));


                //getServiceType();
            });

        });

        fragmentRegister2Binding.edtRegisterType.setOnClickListener(v -> getServiceType());

        fragmentRegister2Binding.edtRegisterService.setOnClickListener(v -> {

            if (registerViewModel.getServiceRepository() != null && registerViewModel.getServiceRepository().getServiceResponse() != null) {
                if (!registerViewModel.request.getType().equals(Constants.TYPE_RESERVATION_CLINIC)) {

                    ArrayList<String> popUpModelsService = new ArrayList<>();
                    for (Service service : registerViewModel.getServiceRepository().getServiceResponse().services) {
                        popUpModelsService.add(service.name);
                    }
                    popUpMenuHelper.openPopUp(getActivity(), fragmentRegister2Binding.edtRegisterService, popUpModelsService, position -> {




                        if (((this.mainType == 1 || this.mainType == 2))&&this.mainFlag!=3) {

                            if (registerViewModel.getServiceRepository().getServiceResponse().services.get(position).type == 2) {
                                fragmentRegister2Binding.edtRegisterType.setVisibility(View.GONE);
                                fragmentRegister2Binding.tiRegisterType.setVisibility(View.GONE);
                            } else {
                                fragmentRegister2Binding.edtRegisterType.setVisibility(View.VISIBLE);
                                fragmentRegister2Binding.tiRegisterType.setVisibility(View.VISIBLE);
                            }
                        }

                        fragmentRegister2Binding.edtRegisterService.setText(popUpModelsService.get(position));
                        registerViewModel.request.setShops_services(String.valueOf(registerViewModel.getServiceRepository().getServiceResponse().services.get(position).id));


                    });
                } else {
                    Timber.e("Worked Done");
                    Intent intent = new Intent(context, BaseActivity.class);
                    intent.putExtra(Constants.PAGE, Constants.SERVICES_LIST);
                    Bundle bundle = new Bundle();
                    bundle.putIntegerArrayList(Constants.SELECTED, registerViewModel.request.getServiceIdList());
                    bundle.putSerializable(Constants.SERVICE, registerViewModel.getServiceRepository().getServiceResponse());
                    intent.putExtra(Constants.BUNDLE, bundle);
                    intent.putExtra(Constants.NAME_BAR, ResourceManager.getString(R.string.services));
                    startActivityForResult(intent, Constants.SERVICES_RESULT);

                }
            }
        });


        fragmentRegister2Binding.edtRegisterCategoryService.setOnClickListener(view -> {

            Intent intent = new Intent(context, BaseActivity.class);
            intent.putExtra(Constants.PAGE, Constants.ADS_COMPANY_FILTER);
            intent.putExtra(Constants.NAME_BAR, ResourceManager.getString(R.string.service));
            Bundle bundle = new Bundle();
            bundle.putString(Constants.FROM, Constants.ADS_COMPANY_FILTER);
            bundle.putInt(Constants.ID, Integer.parseInt(registerViewModel.request.getShops_services()));
            intent.putExtra(Constants.BUNDLE, bundle);
            startActivityForResult(intent, Constants.FILTER_RESULT);

        });
    }

    public void getServiceType() {


        if (!registerViewModel.request.getType().equals("0") && !registerViewModel.request.getType().equals("")) {

            ArrayList<String> popUpModelsType = AppMoon.getServiceType(true);

            fragmentRegister2Binding.tiRegisterType.setVisibility(View.VISIBLE);
            fragmentRegister2Binding.edtRegisterType.setVisibility(View.VISIBLE);
            popUpMenuHelper.openPopUp(getActivity(), fragmentRegister2Binding.edtRegisterType, popUpModelsType, position -> {
                String serviceType = AppMoon.getServiceId(popUpModelsType.get(position));
                registerViewModel.request.setService_id(serviceType);
                fragmentRegister2Binding.edtRegisterType.setText(popUpModelsType.get(position));

                if (position == 0) {
                    registerViewModel.request.setService_id("1");
                } else if (position == 1) {
                    registerViewModel.request.setService_id("2");
                } else {
                    registerViewModel.request.setService_id("1,2");
                }

                if (Integer.parseInt(registerViewModel.request.getType()) <= 2 && serviceType.equals("3")) {
                    registerViewModel.request.flag = "3";
                   // registerViewModel.request.setService_id("3");
                } else
                    registerViewModel.request.flag = "0";


                //registerViewModel.request.isHomeServiceOrConsumer.set(registerViewModel.request.flag != null);
            });


        }
    }


    private void setEvent() {
        registerViewModel.mMutableLiveData.observe((LifecycleOwner) context, o -> {
            String action = (String) o;
            handleActions(action, registerViewModel.getCountryRepository().getMessage());
            handleActions(action, registerViewModel.getRegisterRepository().getMessage());
            handleActions(action, registerViewModel.getServiceRepository().getMessage());
            Timber.e(action);

            switch (action) {
                case Constants.HOME: {//submitSearch register
                    toastMessage(registerViewModel.getRegisterRepository().getMessage(), R.drawable.ic_check_white, R.color.colorPrimary);
                    Intent intent = new Intent(context, MainActivity.class);
//                    intent.putExtra(Constants.PAGE, Constants.LOGIN);
                    ((ParentActivity) context).finishAffinity();
                    startActivity(intent);
                    break;
                }
                case Constants.PACKAGES: {//get service type
                    //Log services
                    Intent intent = new Intent(context, BaseActivity.class);
                    intent.putExtra(Constants.PAGE, Constants.PACKAGES);
                    ((AppCompatActivity) context).finishAffinity();
                    startActivity(intent);
                    break;
                }
                case Constants.SELECT_IMAGE: //select image profile
                    if (registerViewModel.image_select.equals(Constants.COMMERCIAL_IMAGE) || registerViewModel.image_select.equals(Constants.LICENCE_IMAGE)) {
                        Intent intent = new Intent(context, AlbumSelectActivity.class);
                        intent.putExtra(grand.app.moonshop.MultipleImageSelect.helpers.Constants.INTENT_EXTRA_LIMIT, 2);
                        startActivityForResult(intent, Constants.REQUEST_CODE);
                    } else
                        pickImageDialog();

                    break;
                case Constants.REGISTRATION3: {

                    Intent intent = new Intent(context, BaseActivity.class);
                    intent.putExtra(Constants.PAGE, Constants.REGISTRATION3);
                    intent.putExtra(Constants.NAME_BAR,getString(R.string.add_address));
                    Bundle bundle = new Bundle();
                    bundle.putString(Constants.TYPE, Constants.REGISTRATION);
                    bundle.putSerializable(Constants.USER, registerViewModel.request);
                    bundle.putStringArrayList(Constants.IMAGES_PATH, registerViewModel.request.imagesPath);
                    bundle.putStringArrayList(Constants.IMAGES_KEY, registerViewModel.request.imagesKey);
                    bundle.putString(Constants.VERIFY_ID, registerViewModel.getVerificationFirebaseSMSRepository().getVerificationId());
                    bundle.putString(Constants.NAME_BAR, "");
                    bundle.putString(Constants.TYPE, Constants.REGISTRATION);

                    bundle.putSerializable(Constants.REGESTER_REQUEST, registerViewModel.request);


                    intent.putExtra(Constants.BUNDLE, bundle);
                    startActivity(intent);
                    break;
                }
                case Constants.ERROR: //ex: like image
                    showError(registerViewModel.baseError);
                    break;
                case Constants.LOCATIONS: {//select location
                    Timber.e("Locations");
                    Intent intent = new Intent(context, MapAddressActivity.class);
                    startActivityForResult(intent, Constants.ADDRESS_RESULT);
                    break;
                }
                case Constants.WRITE_CODE: {
                    toastMessage(registerViewModel.getVerificationFirebaseSMSRepository().getMessage(), R.drawable.ic_check_white, R.color.colorPrimary);
                    Intent intent = new Intent(context, BaseActivity.class);
                    intent.putExtra(Constants.PAGE, Constants.VERIFICATION);
                    Bundle bundle = new Bundle();
                    bundle.putString(Constants.TYPE, Constants.REGISTRATION);
                    bundle.putSerializable(Constants.USER, registerViewModel.request);
                    bundle.putStringArrayList(Constants.IMAGES_PATH, registerViewModel.request.imagesPath);
                    bundle.putStringArrayList(Constants.IMAGES_KEY, registerViewModel.request.imagesKey);
                    bundle.putString(Constants.VERIFY_ID, registerViewModel.getVerificationFirebaseSMSRepository().getVerificationId());
                    bundle.putString(Constants.NAME_BAR, "");
                    bundle.putString(Constants.TYPE, Constants.REGISTRATION);
                    intent.putExtra(Constants.BUNDLE, bundle);
                    startActivity(intent);
                    break;
                }
                case Constants.TERMS: {
                    Intent intent = new Intent(context, BaseActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putInt(Constants.TYPE, Constants.TYPE_TERMS);
                    intent.putExtra(Constants.BUNDLE, bundle);
                    intent.putExtra(Constants.PAGE, Constants.SETTINGS);
                    intent.putExtra(Constants.NAME_BAR, getString(R.string.terms_and_privacy_policy));
                    startActivity(intent);
                    break;
                }
            }
        });
    }


    int counter = 0;


    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == UCrop.REQUEST_CROP && data != null) {
            final Uri resultUri = UCrop.getOutput(data);
            if (resultUri != null) {
                VolleyFileObject volleyFileObject = new VolleyFileObject(registerViewModel.image_select, resultUri.getPath(), Constants.FILE_TYPE_IMAGE);
                registerViewModel.setImage(registerViewModel.image_select, volleyFileObject);

            }
        }
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE && data != null) {

            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();
                UCrop.of(resultUri, resultUri).start(context, RegisterFragment2.this);
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Toast.makeText(context, "" + getString(R.string.error_failed_pick_gallery_image), Toast.LENGTH_SHORT).show();
            }
        } else if (requestCode == Constants.FILE_TYPE_IMAGE) {

            VolleyFileObject volleyFileObject = FileOperations.getVolleyFileObject(getActivity(), data, registerViewModel.image_select, Constants.FILE_TYPE_IMAGE);
            File file = new File(getCacheDir(), Constants.IMAGE + ".png");
            UCrop.of(Uri.fromFile(volleyFileObject.getFile()), Uri.fromFile(file))
                    .start(context, RegisterFragment2.this);
        }


        if (resultCode == Activity.RESULT_OK && requestCode == Constants.ADDRESS_RESULT) {
            registerViewModel.setAddress(data.getDoubleExtra("lat", 0), data.getDoubleExtra("lng", 0));
        }
        if (requestCode == Constants.REQUEST_CODE && resultCode == Activity.RESULT_OK && data != null) {
            //The array list has the image paths of the selected images
            try {
                ArrayList<Image> images = data.getParcelableArrayListExtra(Constants.INTENT_EXTRA_IMAGES);

                counter = images.size();
                if (registerViewModel.image_select.equals(Constants.COMMERCIAL_IMAGE)) {
                    if (counter == 2)
                        registerViewModel.request.setCommercial(getString(R.string.uploaded) + " " + counter + " " + getString(R.string.photos));
                    else
                        toastInfo(Objects.requireNonNull(fragmentRegister2Binding.tplBeShopCommerical.getHelperText()) + "");
                } else {
                    if (counter == 2)
                        registerViewModel.request.setLicense(getString(R.string.uploaded) + " " + counter + " " + getString(R.string.photos));
                    else
                        toastInfo(Objects.requireNonNull(fragmentRegister2Binding.tplBeShopLicence.getHelperText()) + "");
                }
                if (counter == 2) {
                    for (int i = 0; i < images.size(); i++) {
                        String paramName = registerViewModel.image_select + "[" + i + "]";
                        registerViewModel.setImage(paramName, new VolleyFileObject(paramName, images.get(i).path, Constants.FILE_TYPE_IMAGE));
                    }
                }
                registerViewModel.notifyChange();
            } catch (Exception ex) {
                ex.printStackTrace();

            }
        }
        if (requestCode == Constants.FILTER_RESULT && resultCode == Constants.FILTER_RESULT) {

            int category_id = data.getIntExtra(Constants.CATEGORY_ID, -1);
            registerViewModel.request.setCategory_id(category_id);
            ArrayList<Integer> subCategories = data.getIntegerArrayListExtra(Constants.SUB_CATEGORY_ID);

            registerViewModel.request.setSubCategory_id(subCategories);
            registerViewModel.notifyChange();
        }
        if (requestCode == Constants.SERVICES_RESULT && resultCode == Constants.SERVICES_RESULT) {
            this.registerViewModel.request.setServiceIdList(data.getIntegerArrayListExtra(Constants.SELECTED));
            fragmentRegister2Binding.edtRegisterService.setText(ResourceManager.getString(R.string.done_selected_service));
        }

        super.onActivityResult(requestCode, resultCode, data);

    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        registerViewModel.reset();

    }


}
