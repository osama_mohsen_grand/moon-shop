package grand.app.moonshop.views.fragments.reservation;


import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moonshop.R;
import grand.app.moonshop.adapter.TextSimpleAdapter;
import grand.app.moonshop.base.BaseFragment;
import grand.app.moonshop.databinding.FragmentReservationDetailsBinding;
import grand.app.moonshop.utils.helper.AppUtils;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.viewmodels.reservation.ReservationDetailsViewModel;
import grand.app.moonshop.views.activities.MainActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class ReservationDetailsFragment extends BaseFragment {
    View rootView;
    private FragmentReservationDetailsBinding fragmentReservationDetatilsBinding;
    private ReservationDetailsViewModel reservationDetailsViewModel;
    int order_id = 0,reserved_type = 0;
    TextSimpleAdapter textSimpleAdapter;

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        fragmentReservationDetatilsBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_reservation_details, container, false);
        getData();
        bind();
        setEvent();
        return rootView;
    }

    private void getData() {
        if (getArguments() != null && getArguments().containsKey(Constants.ID))
            order_id = getArguments().getInt(Constants.ID);
    }

    private void bind() {
        reservationDetailsViewModel = new ReservationDetailsViewModel(order_id);
        fragmentReservationDetatilsBinding.setReservationDetailsViewModel(reservationDetailsViewModel);
        AppUtils.initVerticalRV(fragmentReservationDetatilsBinding.rvServices, fragmentReservationDetatilsBinding.rvServices.getContext(), 1);
        rootView = fragmentReservationDetatilsBinding.getRoot();
    }

    private void setEvent() {
        reservationDetailsViewModel.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                String action = (String) o;
                handleActions(action, reservationDetailsViewModel.getReservationRepository().getMessage());
                if(action.equals(Constants.RESERVATION_DETAILS)){
                    reservationDetailsViewModel.setPrice(reservationDetailsViewModel.getReservationRepository().getReservationDetailsResponse().reservationDetailsModel.fees+"");
                    reservationDetailsViewModel.setSpecialist(reservationDetailsViewModel.getReservationRepository().getReservationDetailsResponse().reservationDetailsModel.doctorSpecialties+" ("+
                            reservationDetailsViewModel.getReservationRepository().getReservationDetailsResponse().reservationDetailsModel.yearsExperience+" "+getString(R.string.years)+")");
                    if(reservationDetailsViewModel.getReservationRepository().getReservationDetailsResponse().reservationDetailsModel.services != null){
                        textSimpleAdapter = new TextSimpleAdapter(reservationDetailsViewModel.getReservationRepository().getReservationDetailsResponse().reservationDetailsModel.services);
                        fragmentReservationDetatilsBinding.rvServices.setAdapter(textSimpleAdapter);
                    }
                    reservationDetailsViewModel.showPage(true);
                }else if(action.equals(Constants.CONFIRM)){
                    getActivityBase().finishAffinity();
                    Intent intent = new Intent(context, MainActivity.class);
                    intent.putExtra(Constants.PAGE,Constants.RESERVATION);
                    startActivity(intent);
                }
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (reservationDetailsViewModel != null) reservationDetailsViewModel.reset();
    }


}
