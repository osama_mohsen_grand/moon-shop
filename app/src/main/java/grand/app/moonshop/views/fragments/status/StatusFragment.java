
package grand.app.moonshop.views.fragments.status;


import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;


//import com.abedelazizshe.lightcompressorlibrary.CompressionListener;
//import com.abedelazizshe.lightcompressorlibrary.VideoCompressor;
//import com.abedelazizshe.lightcompressorlibrary.VideoQuality;
//import com.github.hiteshsondhi88.libffmpeg.ExecuteBinaryResponseHandler;
//import com.github.hiteshsondhi88.libffmpeg.FFmpeg;
//import com.github.hiteshsondhi88.libffmpeg.LoadBinaryResponseHandler;
//import com.github.hiteshsondhi88.libffmpeg.exceptions.FFmpegCommandAlreadyRunningException;
//import com.github.hiteshsondhi88.libffmpeg.exceptions.FFmpegNotSupportedException;
import com.abedelazizshe.lightcompressorlibrary.CompressionListener;
import com.abedelazizshe.lightcompressorlibrary.VideoCompressor;
import com.abedelazizshe.lightcompressorlibrary.VideoQuality;
import com.theartofdev.edmodo.cropper.CropImage;
//import com.vincent.videocompressor.VideoCompress;
import com.yalantis.ucrop.UCrop;
import com.yalantis.ucrop.UCropFragment;
import com.yalantis.ucrop.UCropFragmentCallback;

import java.io.File;
import java.util.ArrayList;

import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moonshop.R;
import grand.app.moonshop.adapter.StatusAdapter;
import grand.app.moonshop.base.BaseFragment;
import grand.app.moonshop.customviews.dialog.DialogConfirm;
import grand.app.moonshop.databinding.FragmentStatusBinding;
import grand.app.moonshop.models.app.Mutable;
import grand.app.moonshop.utils.helper.AppUtils;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.utils.LanguagesHelper;
import grand.app.moonshop.utils.dialog.DialogHelperInterface;
import grand.app.moonshop.utils.resources.ResourceManager;
import grand.app.moonshop.utils.upload.FileOperations;
import grand.app.moonshop.viewmodels.status.StatusViewModel;
import grand.app.moonshop.views.activities.MainActivity;
import grand.app.moonshop.vollyutils.MyApplication;
import grand.app.moonshop.vollyutils.VolleyFileObject;
import timber.log.Timber;

import static android.app.Activity.RESULT_OK;
import static com.facebook.FacebookSdk.getCacheDir;

/**
 * A simple {@link Fragment} subclass.
 */
public class StatusFragment extends BaseFragment implements UCropFragmentCallback {

    private FragmentStatusBinding fragmentStatusBinding;
    private StatusViewModel statusViewModel;
    private StatusAdapter statusAdapter;


    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        fragmentStatusBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_status, container, false);
        bind();
        setEvent();
        return fragmentStatusBinding.getRoot();
    }

    private void bind() {
        statusViewModel = new StatusViewModel();
        AppUtils.initVerticalRV(fragmentStatusBinding.rvHome, fragmentStatusBinding.rvHome.getContext(), 1);
        fragmentStatusBinding.setStatusViewModel(statusViewModel);
    }

    private void setEvent() {
        statusViewModel.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                String action = "";
                if (o instanceof String) {
                    action = (String) o;
                    handleActions(action, statusViewModel.getStatusRepository().getMessage());
                    assert action != null;
                } else if (o instanceof Mutable) {
                    if (((Mutable) o).type.equals(Constants.PROGRESS_PERCENTAGE)) {
                        int percent = ((Mutable) o).position;
                        handleStringValue(Constants.PROGRESS_PERCENTAGE, percent);
                    }
                }
                if (action.equals(Constants.STATUS)) {
                    statusAdapter = new StatusAdapter(statusViewModel.getStatusRepository().getStatusResponse().getData());
                    fragmentStatusBinding.rvHome.setAdapter(statusAdapter);
                    setEventAdapter();
                } else if (action.equals(Constants.ADD)) {
//                    FileOperations.pickMedia(context, StatusFragment.this, Constants.FILE_TYPE_IMAGE, Constants.FILE_TYPE_VIDEO);
                    FileOperations.pickMediaRecordCapture(context, StatusFragment.this, Constants.FILE_TYPE_IMAGE, Constants.FILE_TYPE_IMAGE, Constants.FILE_TYPE_VIDEO,
                            Constants.FILE_TYPE_VIDEO_CAPTURE, 15);
                } else if (action.equals(Constants.SUCCESS)) { // success add image
                    toastMessage(statusViewModel.getStatusRepository().getMessage(), R.drawable.ic_check_white, R.color.colorPrimary);
//                    statusViewModel.getStatusRepository().getStatus();
                    LanguagesHelper.changeLanguage(context, LanguagesHelper.getCurrentLanguage());
                    LanguagesHelper.setLanguage(LanguagesHelper.getCurrentLanguage());
                    LanguagesHelper.changeLanguage(MyApplication.getInstance(), LanguagesHelper.getCurrentLanguage());
                    requireActivity().finishAffinity();
                    Intent intent = new Intent(requireContext(), MainActivity.class);
                    intent.putExtra(Constants.PAGE, Constants.STORY);
                    requireActivity().startActivity(intent);
                } else if (action.equals(Constants.DELETED)) { // success add image
                    toastMessage(statusViewModel.getStatusRepository().getMessage(), R.drawable.ic_check_white, R.color.colorPrimary);
                    statusAdapter.remove(statusViewModel.item_delete_position);
                }
            }
        });
    }


    private void setEventAdapter() {
        statusAdapter.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                Mutable mutable = (Mutable) o;
                if (mutable.type.equals(Constants.DELETE)) {
                    statusViewModel.item_delete_position = mutable.position;
                    new DialogConfirm(getActivity())
                            .setTitle(ResourceManager.getString(R.string.remove_story))
                            .setMessage(ResourceManager.getString(R.string.do_you_want_delete_story))
                            .setActionText(ResourceManager.getString(R.string.remove_item))
                            .setActionCancel(ResourceManager.getString(R.string.cancel))
                            .show(new DialogHelperInterface() {
                                @Override
                                public void OnClickListenerContinue(Dialog dialog, View view) {
                                    statusViewModel.getStatusRepository().delete(statusViewModel.getStatusRepository().getStatusResponse().getData().get(mutable.position).mId);
                                }
                            });
                }
            }
        });
    }

    private ArrayList<VolleyFileObject> volleyFileObjects = new ArrayList<>();

    private static final String TAG = "StatusFragment";

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        //super method removed
        Timber.e("onActivityResult:" + requestCode);
        volleyFileObjects.clear();
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE && data != null) {

            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();
                UCrop.of(resultUri, resultUri).start(context, StatusFragment.this);
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Toast.makeText(context, "" + getString(R.string.error_failed_pick_gallery_image), Toast.LENGTH_SHORT).show();
            }
        }
        if (requestCode == UCrop.REQUEST_CROP && data != null) {
            final Uri resultUri = UCrop.getOutput(data);
            if (resultUri != null) {
                VolleyFileObject volleyFileObject = new VolleyFileObject(Constants.IMAGE, resultUri.getPath(), Constants.FILE_TYPE_IMAGE);
                volleyFileObjects.add(volleyFileObject);
                statusViewModel.setImage(volleyFileObjects, 1, 0);
            }
        } else if (requestCode == Constants.FILE_TYPE_IMAGE) {
            VolleyFileObject volleyFileObject = FileOperations.getVolleyFileObject(getActivity(), data, Constants.IMAGE, Constants.FILE_TYPE_IMAGE);

            File file = new File(getCacheDir(), Constants.IMAGE + ".png");

            UCrop.of(Uri.fromFile(volleyFileObject.getFile()), Uri.fromFile(file))
//                    .withAspectRatio(1890, 1000)
//                    .withMaxResultSize(500, 500)

                    .start(context, StatusFragment.this);
        } else if (requestCode == Constants.FILE_TYPE_VIDEO || requestCode == Constants.FILE_TYPE_VIDEO_CAPTURE) {

            VolleyFileObject volleyFileObject = FileOperations.getVolleyFileObject(getActivity(), data, Constants.IMAGE, Constants.FILE_TYPE_VIDEO);
            MediaMetadataRetriever retriever = new MediaMetadataRetriever();
//use one of overloaded setDataSource() functions to set your data source
            retriever.setDataSource(context, Uri.fromFile(volleyFileObject.getFile()));
            String time = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
            long timeInMillisec = Long.parseLong(time);
            int durationTime = (int) (timeInMillisec / 1000);
            Timber.e("duration:" + durationTime);
            Timber.e("file length:" + volleyFileObject.getFile().length());
            long fileSize = Integer.parseInt(String.valueOf((volleyFileObject.getFile().length() / 1024) / 1024));
            Log.d(TAG, "file_size_old:" + fileSize);
            if (durationTime <= 15) {
//                String filename=volleyFileObject.getFile().getAbsolutePath().substring(path.lastIndexOf("/")+1);
//                compressVideo(volleyFileObject,durationTime);
                volleyFileObjects.add(volleyFileObject);
                statusViewModel.setImage(volleyFileObjects, 2, (int) durationTime);

            } else {
                toastInfo(getString(R.string.maximum_video_duration_fifteen_second));
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void compressVideo(VolleyFileObject volleyFileObject,int durationTime) {
        File root = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES);
        String destPath = root + File.separator + "VIDEO_COMPRESSED.mp4";
//==========================//==========================//==========================//==========================//==========================
        if (volleyFileObject.getFile() != null) {
            String inputVideoPath = volleyFileObject.getFile().getAbsolutePath();
            statusViewModel.mMutableLiveData.setValue(Constants.SHOW_PROGRESS_PERCENTAGE);
//            if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            VideoCompressor.start(inputVideoPath, destPath, new CompressionListener() {
                @Override
                public void onStart() {
                    // Compression start
                    Log.d(TAG, "onStart:");
                }

                @Override
                public void onSuccess() {
                    // On Compression success
                    Log.d(TAG, "onSuccess:");
                    requireActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            File file = new File(destPath);
                            long fileSizeCompress = Integer.parseInt(String.valueOf((file.length() / 1024) / 1024));
                            Log.d(TAG, "file_size_new:" + fileSizeCompress);
                            statusViewModel.mMutableLiveData.setValue(Constants.HIDE_PROGRESS_PERCENTAGE);

                            volleyFileObject.setFile(file);
                            volleyFileObject.setFilePath(destPath);
                            volleyFileObjects.add(volleyFileObject);
                            statusViewModel.setImage(volleyFileObjects, 2, (int) durationTime);
                        }
                    });
                }

                @Override
                public void onFailure(String failureMessage) {
                    // On Failure
                    Log.d(TAG, "failure:" + failureMessage);
                }

                @Override
                public void onProgress(float finalPercent) {
                    Log.d(TAG, finalPercent + "");
                    // Update UI with progress value
                    requireActivity().runOnUiThread(new Runnable() {
                        public void run() {
                            statusViewModel.mMutableLiveData.setValue(
                                    new Mutable(Constants.PROGRESS_PERCENTAGE, (int) finalPercent));
                        }
                    });
                }

                @Override
                public void onCancelled() {
                    // On Cancelled
                    Log.d(TAG, "onCancelled:");
                }
            }, VideoQuality.HIGH, false, true);
        }
//            } else {
//                try {
//                    root.mkdir();
//                    root.createNewFile();
////                    ProgressDialog dialog = ProgressDialog.show(context, getString(R.string.please_wait), getString(R.string.please_wait), true);
//////                    ProgressDialog dialog = new ProgressDialog(requireContext());
////                    dialog.setCancelable(false);
////                    dialog.show();
//                    FFmpeg ffmpeg = FFmpeg.getInstance(requireActivity());
//                    try {
//                        //Load the binary
//                        ffmpeg.loadBinary(new LoadBinaryResponseHandler() {
//                            @Override
//                            public void onStart() {
//                            }
//
//                            @Override
//                            public void onFailure() {
//                            }
//
//                            @Override
//                            public void onSuccess() {
//                            }
//
//                            @Override
//                            public void onFinish() {
//
//                            }
//                        });
//                    } catch (FFmpegNotSupportedException e) {
//                        // Handle if FFmpeg is not supported by device
//                        Log.d("FFmpeg", e.getMessage());
//                    }
//                    try {
//                        // to execute "ffmpeg -version" command you just need to pass "-version"
////                                String outputPath = destPath;
//                        String[] commandArray = new String[]{};
//                        commandArray = new String[]{"-y", "-i", inputVideoPath, "-s", "720x480", "-r", "25",
//                                "-vcodec", "mpeg4", "-b:v", "300k", "-b:a", "48000", "-ac", "2", "-ar", "22050", destPath};
////                        final ProgressDialog dialog = new ProgressDialog(requireContext());
//                        ffmpeg.execute(commandArray, new ExecuteBinaryResponseHandler() {
//                            @Override
//                            public void onStart() {
//                                Log.e("FFmpeg", "onStart");
//                            }
//
//                            @Override
//                            public void onProgress(String message) {
//                                Log.e("FFmpeg onProgress? ", message);
//
//                                int start = message.indexOf("time=00:00:");
//                                int end = message.indexOf(" bitrate");
//                                if (start != -1 && end != -1) {
//                                    String duration = message.substring(start + 11, end);
//                                    try {
//                                        Log.d(TAG, "duration_message:" + duration);
//                                        float durationNumber = Float.parseFloat(duration);
//                                        Log.d(TAG, durationNumber + "");
//                                        float finalPercent = (durationNumber / durationTime) * 100;
//                                        Log.d(TAG, "finalPercent:" + finalPercent);
//
//                                        requireActivity().runOnUiThread(new Runnable() {
//                                            public void run() {
//                                                statusViewModel.mMutableLiveData.setValue(
//                                                        new Mutable(Constants.PROGRESS_PERCENTAGE, (int) finalPercent));
//                                            }
//                                        });
//
//
//                                    } catch (Exception ex) {
//                                        Log.d(TAG, "ex:" + ex.getMessage());
//                                    }
////                                    if (!duration.equals("")) {
////                                        try {
////                                            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
////                                            sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
////                                            int percent = (int) sdf.parse("1970-01-01 " + duration).getTime();
////                                            percent /= 1000;
////                                            percent /= durationTime;
////                                            percent *= 100;
////                                            int finalPercent = percent;
////                                            Log.d(TAG,"percent:"+finalPercent+"");
////                                            requireActivity().runOnUiThread(new Runnable() {
////                                                public void run() {
////                                                    statusViewModel.mMutableLiveData.setValue(
////                                                            new Mutable(Constants.PROGRESS_PERCENTAGE, finalPercent));
////                                                }
////                                            });
////
////                                        } catch (ParseException e) {
////                                            e.printStackTrace();
////                                        }
////                                    }
//                                }
//
//
////                                Pattern timePattern = Pattern.compile("(?<=time=)[\\d:.]*");
////                                Scanner sc = new Scanner(message);
////
////                                String match = sc.findWithinHorizon(timePattern, 0);
////                                if (match != null) {
////                                    String[] matchSplit = match.split(":");
////                                    if (duration != 0) {
////                                        float progress = (Integer.parseInt(matchSplit[0]) * 3600 +
////                                                Integer.parseInt(matchSplit[1]) * 60 +
////                                                Float.parseFloat(matchSplit[2])) / duration;
////                                        int showProgress = (int) (progress * 100000);
////                                        Log.d(TAG,showProgress+"");
////                                    }
////                                }
//
//                            }
//
//                            @Override
//                            public void onFailure(String message) {
//                                Log.e("FFmpeg onFailure? ", message);
//                            }
//
//                            @Override
//                            public void onSuccess(String message) {
//                                Log.e("FFmpeg onSuccess? ", message);
//
//                            }
//
//                            @Override
//                            public void onFinish() {
//                                LanguagesHelper.changeLanguage(requireContext(), LanguagesHelper.getCurrentLanguage());
//                                LanguagesHelper.changeLanguage(MyApplication.getInstance(), LanguagesHelper.getCurrentLanguage());
//
//                                statusViewModel.mMutableLiveData.setValue(Constants.HIDE_PROGRESS_PERCENTAGE);
//
//                                File file = new File(destPath);
//                                volleyFileObject.setFile(file);
//                                volleyFileObject.setFilePath(destPath);
//                                volleyFileObjects.add(volleyFileObject);
//                                statusViewModel.setImage(volleyFileObjects, 2, (int) durationTime);
////                            playVideoOnVideoView(Uri.parse(outputPath));
////                            isCompressed = true;
////                            count = count + 1;
//                            }
//                        });
//                    } catch (FFmpegCommandAlreadyRunningException e) {
//                        e.printStackTrace();
//                        Log.d(TAG, "exc:" + e.getMessage());
//                        // Handle if FFmpeg is already running
//                    }
//                } catch (IOException e) {
//                    e.printStackTrace();
//                    Log.d(TAG, "exc IOException:" + e.getMessage());
//                }
//            }
//        }
////==========================//==========================//==========================//==========================//==========================
    }


    private Context context;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onResume() {
        super.onResume();
        LanguagesHelper.changeLanguage(requireContext(), LanguagesHelper.getCurrentLanguage());
        LanguagesHelper.changeLanguage(MyApplication.getInstance(), LanguagesHelper.getCurrentLanguage());
    }

    /**
     * Return loader status
     *
     * @param showLoader
     */
    @Override
    public void loadingProgress(boolean showLoader) {

    }

    /**
     * Return cropping result or error
     *
     * @param result
     */
    @Override
    public void onCropFinish(UCropFragment.UCropResult result) {

    }
}
