package grand.app.moonshop.views.fragments.auth;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moonshop.base.BaseFragment;
import grand.app.moonshop.R;
import grand.app.moonshop.customviews.facebook.FacebookModel;
import grand.app.moonshop.customviews.facebook.FacebookResponseInterface;
import grand.app.moonshop.databinding.FragmentLoginBinding;
import grand.app.moonshop.models.app.AccountTypeModel;
import grand.app.moonshop.models.app.AppMoon;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.utils.PopUp.PopUpInterface;
import grand.app.moonshop.utils.PopUp.PopUpMenuHelper;
import grand.app.moonshop.viewmodels.user.LoginViewModel;
import grand.app.moonshop.views.activities.BaseActivity;
import grand.app.moonshop.views.activities.MainActivity;
import timber.log.Timber;

public class LoginFragment extends BaseFragment {
    View rootView;
    private FragmentLoginBinding fragmentLoginBinding;
    private LoginViewModel loginViewModel;
    public PopUpMenuHelper popUpMenuHelper = new PopUpMenuHelper();

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        fragmentLoginBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_login, container, false);
        bind();
        setEvent();
        return rootView;
    }

    private void bind() {
        loginViewModel = new LoginViewModel();
        fragmentLoginBinding.setLoginViewModel(loginViewModel);
        loginViewModel.serviceRepository.getAccountTypes();
        initPopUp();
        rootView = fragmentLoginBinding.getRoot();
    }

    private void initPopUp() {
        fragmentLoginBinding.edtLoginAccountType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ArrayList<AccountTypeModel> accountTypeModels = AppMoon.getAccountsType();

                ArrayList<String> popUpModels = new ArrayList<>();
                for (AccountTypeModel accountTypeModel : accountTypeModels)
                    popUpModels.add(accountTypeModel.type);

                popUpMenuHelper.openPopUp(getActivity(), fragmentLoginBinding.edtLoginAccountType, popUpModels, new PopUpInterface() {
                    @Override
                    public void submitPopUp(int position) {
                        fragmentLoginBinding.edtLoginAccountType.setText(accountTypeModels.get(position).type);
                        loginViewModel.loginRequest.setType(String.valueOf(loginViewModel.getServiceRepository().getAcountTypeResponse().getData().get(position).getType()));
                    }
                });
            }
        });


    }

    private static final String TAG = "LoginFragment";

    private void setEvent() {
        loginViewModel.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                Timber.e(loginViewModel.getLoginRepository().getMessage());
                Intent intent = new Intent(getActivity(), BaseActivity.class);
                Bundle bundle = new Bundle();
                String action = (String) o;
                handleActions(action, loginViewModel.getLoginRepository().getMessage());
                assert action != null;
                if(action.equals(Constants.HOME)){
                    ((AppCompatActivity)context).finishAffinity();
                    startActivity(new Intent(context, MainActivity.class));
                }else if(action.equals(Constants.PACKAGES)){
                    intent = new Intent(context, BaseActivity.class);
                    intent.putExtra(Constants.PAGE,Constants.PACKAGES);
                    ((AppCompatActivity)context).finishAffinity();
                    startActivity(intent);
                }else if(action.equals(Constants.REGISTRATION)){
                    intent = new Intent(context, BaseActivity.class);
                    intent.putExtra(Constants.PAGE,Constants.PHONE_VERIFICATION);
                    startActivity(intent);
                } else if (action.equals(Constants.UNAUTHORIZED)) {
                    bundle.putSerializable(Constants.FACEBOOK, loginViewModel.facebookModel);
                    intent.putExtra(Constants.BUNDLE, bundle);
                    intent.putExtra(Constants.PAGE, Constants.PHONE_VERIFICATION);
                    startActivity(intent);
                } else if (action.equals(Constants.FORGET_PASSWORD)) {
                    bundle.putBoolean(Constants.CHANGE_PASSWORD,true);
                    intent.putExtra(Constants.BUNDLE, bundle);
                    intent.putExtra(Constants.PAGE, PhoneVerificationFragment.class.getName());
                    startActivity(intent);
                }else if(action.equals(Constants.FACEBOOK)){
                    fragmentLoginBinding.customFacebookLogin.submitFacebook(new FacebookResponseInterface() {
                        @Override
                        public void response(FacebookModel facebookModel) {
                            loginViewModel.socialLogin(facebookModel);
                        }
                    });
                }
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (fragmentLoginBinding != null && fragmentLoginBinding.customFacebookLogin.callbackManager != null)
            fragmentLoginBinding.customFacebookLogin.callbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(loginViewModel != null) loginViewModel.reset();
    }

}
