package grand.app.moonshop.views.fragments.famous;


import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moonshop.R;
import grand.app.moonshop.adapter.AlbumDetailsAdapter;
import grand.app.moonshop.base.BaseFragment;
import grand.app.moonshop.databinding.FragmentFamousDiscoverBinding;
import grand.app.moonshop.models.app.Mutable;
import grand.app.moonshop.models.base.IdNameImage;
import grand.app.moonshop.utils.helper.AppUtils;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.viewmodels.famous.FamousDiscoverViewModel;
import grand.app.moonshop.views.activities.BaseActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class FamousDiscover extends BaseFragment {


    private FragmentFamousDiscoverBinding fragmentFamousDiscoverBinding;
    private FamousDiscoverViewModel famousDiscoverViewModel;
    private AlbumDetailsAdapter albumDetailsAdapter;
    int type = 4;


    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        fragmentFamousDiscoverBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_famous_discover, container, false);
        getData();
        bind();
        setEvent();
        return fragmentFamousDiscoverBinding.getRoot();
    }


    private void getData() {
        if (getArguments() != null && getArguments().containsKey(Constants.TYPE))
            type = getArguments().getInt(Constants.TYPE);
    }


    private void bind() {
        famousDiscoverViewModel = new FamousDiscoverViewModel(type);
        AppUtils.initVerticalRV(fragmentFamousDiscoverBinding.rvDiscover, fragmentFamousDiscoverBinding.rvDiscover.getContext(), 3);
        fragmentFamousDiscoverBinding.setFamousDiscoverViewModel(famousDiscoverViewModel);
    }

    private void setEvent() {
        famousDiscoverViewModel.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                String action = (String) o;
                handleActions(action, famousDiscoverViewModel.getFamousRepository().getMessage());
                assert action != null;
                if(action.equals(Constants.SUCCESS)){
                    if(famousDiscoverViewModel.getFamousRepository().getFamousAlbumImagesResponse().data.size() == 0){
                        famousDiscoverViewModel.noData();
                    }else {
                        albumDetailsAdapter = new AlbumDetailsAdapter(famousDiscoverViewModel.getFamousRepository().getFamousAlbumImagesResponse().data);
                        fragmentFamousDiscoverBinding.rvDiscover.setAdapter(albumDetailsAdapter);
                        setEventAdapter();
                    }
                }
            }
        });
    }

    private void setEventAdapter() {
        albumDetailsAdapter.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                Mutable mutable = (Mutable) o;
                if (mutable.type.equals(Constants.IMAGE)) {
                    IdNameImage idNameImage = famousDiscoverViewModel.getFamousRepository().getFamousAlbumImagesResponse().data.get(mutable.position);
                    Intent intent = new Intent(context, BaseActivity.class);
                    intent.putExtra(Constants.PAGE, Constants.ZOOM);
                    Bundle bundle = new Bundle();
                    bundle.putString(Constants.IMAGE, idNameImage.image);
                    intent.putExtra(Constants.NAME_BAR, Constants.IMAGE);
                    intent.putExtra(Constants.BUNDLE, bundle);
                    startActivityForResult(intent, Constants.RELOAD_RESULT);
                } else if (mutable.type.equals(Constants.VIDEO)) {
                    IdNameImage idNameImage = famousDiscoverViewModel.getFamousRepository().getFamousAlbumImagesResponse().data.get(mutable.position);
                    Intent intent = new Intent(context, BaseActivity.class);
                    intent.putExtra(Constants.PAGE, Constants.VIDEO);
                    intent.putExtra(Constants.NAME_BAR, Constants.VIDEO);
                    Bundle bundle = new Bundle();
                    bundle.putString(Constants.VIDEO, idNameImage.image);
                    intent.putExtra(Constants.BUNDLE, bundle);
                    startActivity(intent);
                }
            }
        });
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        famousDiscoverViewModel.reset();

    }



}
