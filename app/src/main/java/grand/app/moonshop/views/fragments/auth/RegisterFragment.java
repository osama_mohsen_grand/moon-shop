package grand.app.moonshop.views.fragments.auth;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import com.theartofdev.edmodo.cropper.CropImage;
import com.yalantis.ucrop.UCrop;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moonshop.MultipleImageSelect.activities.AlbumSelectActivity;
import grand.app.moonshop.R;
import grand.app.moonshop.adapter.SuggestAdapter;
import grand.app.moonshop.base.BaseFragment;
import grand.app.moonshop.base.ParentActivity;
import grand.app.moonshop.customviews.facebook.FacebookModel;
import grand.app.moonshop.databinding.FragmentRegisterBinding;
import grand.app.moonshop.models.country.Region;
import grand.app.moonshop.models.shop.AddShopResponse;
import grand.app.moonshop.utils.helper.AppUtils;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.utils.PopUp.PopUpMenuHelper;
import grand.app.moonshop.utils.images.ImageLoaderHelper;
import grand.app.moonshop.utils.upload.FileOperations;
import grand.app.moonshop.viewmodels.user.RegisterViewModel;
import grand.app.moonshop.views.activities.BaseActivity;
import grand.app.moonshop.views.activities.MainActivity;
import grand.app.moonshop.views.activities.MapAddressActivity;
import grand.app.moonshop.vollyutils.VolleyFileObject;
import timber.log.Timber;

import static android.app.Activity.RESULT_OK;
import static com.facebook.FacebookSdk.getCacheDir;

public class RegisterFragment extends BaseFragment {
    View rootView;

    private FragmentRegisterBinding fragmentRegisterBinding;
    private RegisterViewModel registerViewModel;
    public String type = "", phone = "";
    public PopUpMenuHelper popUpMenuHelper = new PopUpMenuHelper();
    public FacebookModel facebookModel;
    SuggestAdapter suggestAdapter = null;
    //1=>accept , 2=>reject  , 3=>arrive  , 4=>start  , 5=> cancel after accept ,  6=>finished trip

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentRegisterBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_register, container, false);
        getData();
        bind();
        setEvent();

        return rootView;
    }

    private void getData() {
        if (getArguments() != null && getArguments().containsKey(Constants.TYPE)) {
            type = getArguments().getString(Constants.TYPE);
        }
        if (getArguments() != null && getArguments().containsKey(Constants.PHONE)) {
            phone = getArguments().getString(Constants.PHONE);

            Timber.e("phone >> "+phone);

        }
        if (getArguments() != null && getArguments().containsKey(Constants.FACEBOOK)) {
            facebookModel = (FacebookModel) getArguments().getSerializable(Constants.FACEBOOK);
        }
    }

    private void bind() {
        registerViewModel = new RegisterViewModel( null);
        AppUtils.initHorizontalRV(fragmentRegisterBinding.rvSuggestions, fragmentRegisterBinding.rvSuggestions.getContext(), 1);
        if (facebookModel != null) {
            fragmentRegisterBinding.imgRegisterUser.setImageResource(android.R.color.transparent);
            ImageLoaderHelper.ImageLoaderLoad(context, facebookModel.image, fragmentRegisterBinding.imgRegisterUser);
            registerViewModel.setSocial(facebookModel);
        }
        fragmentRegisterBinding.setRegisterViewModel(registerViewModel);

        rootView = fragmentRegisterBinding.getRoot();
    }

    private void pickImageDialog() {
        FileOperations.pickImage(context, RegisterFragment.this, Constants.FILE_TYPE_IMAGE);
    }

    public List<Region> regions = new ArrayList<>();

    private void setEvent() {

        registerViewModel.mMutableLiveData.observe((LifecycleOwner) context, o -> {
            String action = (String) o;
            handleActions(action, registerViewModel.getCountryRepository().getMessage());
            handleActions(action, registerViewModel.getRegisterRepository().getMessage());
            handleActions(action, registerViewModel.getServiceRepository().getMessage());
            Timber.e(action);
            if (action.equals(Constants.HOME)) {//submitSearch register
                toastMessage(registerViewModel.getRegisterRepository().getMessage(), R.drawable.ic_check_white, R.color.colorPrimary);
                Intent intent = new Intent(context, MainActivity.class);
//                    intent.putExtra(Constants.PAGE, Constants.LOGIN);
                ((ParentActivity) context).finishAffinity();
                startActivity(intent);
            } else if (action.equals(Constants.PACKAGES)) {//get service type
                //Log services
                Intent intent = new Intent(context, BaseActivity.class);
                intent.putExtra(Constants.PAGE, Constants.PACKAGES);
                ((AppCompatActivity) context).finishAffinity();
                startActivity(intent);
            } else if (action.equals(Constants.SELECT_IMAGE)) {//select image profile
                if (registerViewModel.image_select.equals(Constants.COMMERCIAL_IMAGE) || registerViewModel.image_select.equals(Constants.LICENCE_IMAGE)) {
                    Intent intent = new Intent(context, AlbumSelectActivity.class);
                    intent.putExtra(grand.app.moonshop.MultipleImageSelect.helpers.Constants.INTENT_EXTRA_LIMIT, 2);
                    startActivityForResult(intent, Constants.REQUEST_CODE);
                } else
                    pickImageDialog();

            } else if (action.equals(Constants.ERROR)) {//ex: like image
                showError(registerViewModel.baseError);
            } else if (action.equals(Constants.LOCATIONS)) {//select location
                Timber.e("Locations");
                Intent intent = new Intent(context, MapAddressActivity.class);
                startActivityForResult(intent, Constants.ADDRESS_RESULT);
            }
            else if (action.equals(Constants.REGISTRATION2)) {
                Intent intent = new Intent(context, BaseActivity.class);
                intent.putExtra(Constants.PAGE, Constants.REGISTRATION2);
                intent.putExtra(Constants.NAME_BAR,getString(R.string.shop_info));
                Bundle bundle = new Bundle();
                bundle.putString(Constants.TYPE, Constants.REGISTRATION);
                bundle.putSerializable(Constants.USER, registerViewModel.request);
                bundle.putStringArrayList(Constants.IMAGES_PATH, registerViewModel.request.imagesPath);
                bundle.putStringArrayList(Constants.IMAGES_KEY, registerViewModel.request.imagesKey);
                bundle.putString(Constants.VERIFY_ID, registerViewModel.getVerificationFirebaseSMSRepository().getVerificationId());
                bundle.putString(Constants.NAME_BAR, "");
                bundle.putString(Constants.TYPE, Constants.REGISTRATION);

                bundle.putSerializable(Constants.REGESTER_REQUEST,registerViewModel.request);


                intent.putExtra(Constants.BUNDLE, bundle);
                startActivity(intent);
            }

            else if (action.equals(Constants.WRITE_CODE)) {
                toastMessage(registerViewModel.getVerificationFirebaseSMSRepository().getMessage(), R.drawable.ic_check_white, R.color.colorPrimary);
                Intent intent = new Intent(context, BaseActivity.class);
                intent.putExtra(Constants.PAGE, Constants.VERIFICATION);
                Bundle bundle = new Bundle();
                bundle.putString(Constants.TYPE, Constants.REGISTRATION);
                bundle.putSerializable(Constants.USER, registerViewModel.request);
                bundle.putStringArrayList(Constants.IMAGES_PATH, registerViewModel.request.imagesPath);
                bundle.putStringArrayList(Constants.IMAGES_KEY, registerViewModel.request.imagesKey);
                bundle.putString(Constants.VERIFY_ID, registerViewModel.getVerificationFirebaseSMSRepository().getVerificationId());
                bundle.putString(Constants.NAME_BAR, "");
                bundle.putString(Constants.TYPE, Constants.REGISTRATION);
                intent.putExtra(Constants.BUNDLE, bundle);
                startActivity(intent);
            }else if (action.equals(Constants.SUGGESTIONS)) {
                fragmentRegisterBinding.nscrollCheckout.scrollTo(0, (int) fragmentRegisterBinding.tplBeShopNickname.getY());
                AddShopResponse addShopResponse = registerViewModel.getRegisterRepository().getAddShopResponse();
                fragmentRegisterBinding.tplBeShopNickname.setError(addShopResponse.msg);
                if (suggestAdapter == null) {
                    suggestAdapter = new SuggestAdapter(addShopResponse.suggestions);
                    fragmentRegisterBinding.rvSuggestions.setAdapter(suggestAdapter);
                    suggestAdapter.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
                        @Override
                        public void onChanged(@Nullable Object o) {
                            int pos = (int) o;
                            registerViewModel.request.setNickName(addShopResponse.suggestions.get(pos).name);
                            registerViewModel.notifyChange();
                        }
                    });
                } else {
                    suggestAdapter.updateAll(addShopResponse.suggestions);
                }
            } else if (action.equals(Constants.TERMS)) {
                Intent intent = new Intent(context, BaseActivity.class);
                Bundle bundle = new Bundle();
                bundle.putInt(Constants.TYPE, Constants.TYPE_TERMS);
                intent.putExtra(Constants.BUNDLE, bundle);
                intent.putExtra(Constants.PAGE, Constants.SETTINGS);
                intent.putExtra(Constants.NAME_BAR, getString(R.string.terms_and_privacy_policy));
                startActivity(intent);
            }
        });

        registerViewModel.request.setPhone(phone);
    }


    int counter = 0;
    private static final String TAG = "RegisterFragment";

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        //super method removed


        if (requestCode == UCrop.REQUEST_CROP && data != null) {
            final Uri resultUri = UCrop.getOutput(data);
            if (resultUri != null) {
                VolleyFileObject volleyFileObject = new VolleyFileObject(registerViewModel.image_select, resultUri.getPath(), Constants.FILE_TYPE_IMAGE);
                registerViewModel.setImage(registerViewModel.image_select, volleyFileObject);
                if (registerViewModel.image_select.equals("shop_image")) {
                    fragmentRegisterBinding.imgRegisterUser.setImageURI(null);
                    fragmentRegisterBinding.imgRegisterUser.setImageURI(Uri.parse(String.valueOf(new File(volleyFileObject.getFilePath()))));
                }
            }
        }
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE && data != null) {

            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();
                UCrop.of(resultUri, resultUri).start(context, RegisterFragment.this);
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Toast.makeText(context, "" + getString(R.string.error_failed_pick_gallery_image), Toast.LENGTH_SHORT).show();
            }
        }
        else if (requestCode == Constants.FILE_TYPE_IMAGE) {
            Log.d(TAG,"FILE_TYPE_IMAGE");
            VolleyFileObject volleyFileObject = FileOperations.getVolleyFileObject(getActivity(), data, registerViewModel.image_select, Constants.FILE_TYPE_IMAGE);
            File file = new File(getCacheDir(), Constants.IMAGE + ".png");
            UCrop.of(Uri.fromFile(volleyFileObject.getFile()), Uri.fromFile(file))
                    .start(context, RegisterFragment.this);
        }


        if (resultCode == Activity.RESULT_OK && requestCode == Constants.ADDRESS_RESULT) {
            registerViewModel.setAddress(data.getDoubleExtra("lat", 0), data.getDoubleExtra("lng", 0));
        }

        if (requestCode == Constants.FILTER_RESULT && resultCode == Constants.FILTER_RESULT) {
            Timber.e("CallService");
            int category_id = data.getIntExtra(Constants.CATEGORY_ID, -1);
            registerViewModel.request.setCategory_id(category_id);
            ArrayList<Integer> subCategories = data.getIntegerArrayListExtra(Constants.SUB_CATEGORY_ID);
            Timber.e("categories:" + category_id);
            Timber.e("subCategories:" + subCategories.size());
            registerViewModel.request.setSubCategory_id(subCategories);
            registerViewModel.notifyChange();
        }


        super.onActivityResult(requestCode, resultCode, data);

    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        registerViewModel.reset();

    }


}
