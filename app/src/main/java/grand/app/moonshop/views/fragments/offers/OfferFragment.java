package grand.app.moonshop.views.fragments.offers;


import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moonshop.R;
import grand.app.moonshop.adapter.OfferAdapter;
import grand.app.moonshop.base.BaseFragment;
import grand.app.moonshop.customviews.dialog.DialogConfirm;
import grand.app.moonshop.databinding.FragmentOfferBinding;
import grand.app.moonshop.models.app.Mutable;
import grand.app.moonshop.utils.helper.AppUtils;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.utils.dialog.DialogHelperInterface;
import grand.app.moonshop.utils.resources.ResourceManager;
import grand.app.moonshop.viewmodels.offer.OfferViewModel;
import grand.app.moonshop.views.activities.BaseActivity;
import timber.log.Timber;

/**
 * A simple {@link Fragment} subclass.
 */
public class OfferFragment extends BaseFragment {

    private FragmentOfferBinding fragmentOfferBinding;
    private OfferViewModel offerViewModel;
    private OfferAdapter offerAdapter;


    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        fragmentOfferBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_offer, container, false);
        bind();
        setEvent();
        return fragmentOfferBinding.getRoot();
    }

    private void bind() {
        offerViewModel = new OfferViewModel();
        AppUtils.initVerticalRV(fragmentOfferBinding.rvOffers, fragmentOfferBinding.rvOffers.getContext(), 2);
        fragmentOfferBinding.setOfferViewModel(offerViewModel);
    }


    private void setEvent() {
        offerViewModel.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                String action = (String) o;
                handleActions(action, offerViewModel.getOfferRepository().getMessage());
                assert action != null;
                if(action.equals(Constants.SUCCESS)){
                    offerAdapter = new OfferAdapter(offerViewModel.getOfferRepository().getOfferResponse().mData);
                    fragmentOfferBinding.rvOffers.setAdapter(offerAdapter);
                    setEventAdapter();
                }else if(action.equals(Constants.ADD)){
                    Intent intent = new Intent(context, BaseActivity.class);
                    intent.putExtra(Constants.PAGE,Constants.Add_OFFER);
                    intent.putExtra(Constants.NAME_BAR, ResourceManager.getString(R.string.add_offer));
                    startActivityForResult(intent, Constants.RELOAD_RESULT);
                }else if(action.equals(Constants.DELETED)){
                    toastMessage(offerViewModel.getOfferRepository().getMessage(),R.drawable.ic_check_white,R.color.colorPrimary);
                    offerAdapter.remove(offerViewModel.item_delete_position);
                }
            }
        });
    }


    private void setEventAdapter() {
        offerAdapter.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                Mutable mutable = (Mutable) o;
                if(mutable.type.equals(Constants.IMAGE)){
                    Intent intent = new Intent(context, BaseActivity.class);
                    intent.putExtra(Constants.PAGE, Constants.ZOOM);
                    intent.putExtra(Constants.NAME_BAR,  getString(R.string.image));
                    Bundle bundle = new Bundle();
                    bundle.putString(Constants.IMAGE, offerViewModel.getOfferRepository().getOfferResponse().mData.get(mutable.position).image);
                    intent.putExtra(Constants.BUNDLE, bundle);
                    startActivity(intent);
                }
                else if(mutable.type.equals(Constants.DELETE)){

                    new DialogConfirm(getActivity())
                            .setTitle(ResourceManager.getString(R.string.delete_item))
                            .setMessage(ResourceManager.getString(R.string.do_you_want_delete_item))
                            .setActionText(ResourceManager.getString(R.string.continue_))
                            .setActionCancel(ResourceManager.getString(R.string.cancel))
                            .show(new DialogHelperInterface() {
                                @Override
                                public void OnClickListenerContinue(Dialog dialog, View view) {
                                    offerViewModel.item_delete_position = mutable.position;
                                    offerViewModel.getOfferRepository().delete(offerViewModel.getOfferRepository().getOfferResponse().mData.get(mutable.position).mId);
                                }
                            });
                } else if (mutable.type.equals(Constants.VIDEO)) {
                    Timber.e("mp4:"+offerViewModel.getOfferRepository().getOfferResponse().mData.get(mutable.position).image);
                    Intent intent = new Intent(context, BaseActivity.class);
                    intent.putExtra(Constants.PAGE, Constants.VIDEO);
                    intent.putExtra(Constants.NAME_BAR, getString(R.string.video));
                    Bundle bundle = new Bundle();
                    bundle.putString(Constants.VIDEO, offerViewModel.getOfferRepository().getOfferResponse().mData.get(mutable.position).image);
                    intent.putExtra(Constants.BUNDLE, bundle);
                    startActivity(intent);
                }
            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        //super method removed
        Timber.e("onActivityResult:" + requestCode);
        if (resultCode == Activity.RESULT_OK && requestCode == Constants.RELOAD_RESULT) {
            offerViewModel.getOfferRepository().getOffers();
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

}
