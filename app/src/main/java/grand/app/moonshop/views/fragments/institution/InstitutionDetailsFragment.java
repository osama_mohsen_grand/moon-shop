package grand.app.moonshop.views.fragments.institution;


import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moonshop.R;
import grand.app.moonshop.adapter.InstitutionServicesAdapter;
import grand.app.moonshop.base.BaseFragment;
import grand.app.moonshop.databinding.FragmentInstituationDetailsBinding;
import grand.app.moonshop.models.shop.ShopData;
import grand.app.moonshop.utils.helper.AppUtils;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.viewmodels.institution.InstitutionDetailsViewModel;
import grand.app.moonshop.views.activities.BaseActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class InstitutionDetailsFragment extends BaseFragment {

    FragmentInstituationDetailsBinding fragmentInstituationDetailsBinding;
    InstitutionDetailsViewModel institutionDetailsViewModel;
    private int id;
    private ShopData shopData;
    InstitutionServicesAdapter institutionServicesAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        fragmentInstituationDetailsBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_instituation_details, container, false);
        restorePage();
        AppUtils.initVerticalRV(fragmentInstituationDetailsBinding.rvServiceInstitution, fragmentInstituationDetailsBinding.rvServiceInstitution.getContext(), 1);
        getData();
        return fragmentInstituationDetailsBinding.getRoot();
    }


    private void getData() {
        if (getArguments() != null && getArguments().containsKey(Constants.ID)) { // shop_id
            id = getArguments().getInt(Constants.ID);
        }
        if (getArguments() != null && getArguments().containsKey(Constants.SHOP_DETAILS)) {
            shopData = (ShopData) getArguments().getSerializable(Constants.SHOP_DETAILS);
            institutionServicesAdapter = new InstitutionServicesAdapter(shopData.categories,false);
            fragmentInstituationDetailsBinding.rvServiceInstitution.setAdapter(institutionServicesAdapter);
        }
    }

    public void bind() {
        institutionDetailsViewModel = new InstitutionDetailsViewModel(shopData.shop_details);
        fragmentInstituationDetailsBinding.setInstitutionDetailsViewModel(institutionDetailsViewModel);
    }

    private void setEvent() {
        institutionDetailsViewModel.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                String action = (String) o;
                assert action != null;
                if (action.equals(Constants.EDIT)) {
                    Intent intent = new Intent(context, BaseActivity.class);
                    intent.putExtra(Constants.PAGE,Constants.EDIT_INSTITUTION);
                    Bundle bundle = new Bundle();
                    bundle.putSerializable(Constants.SHOP_DETAILS,shopData);
                    intent.putExtra(Constants.BUNDLE,bundle);
                    startActivity(intent);
                }
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        bind();
        setEvent();
        if(isReloadPage()) {
            HomeInstitutionFragment frag = ((HomeInstitutionFragment) this.getParentFragment());
            frag.institutionViewModel.getInstitutionViewModel().getService();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (institutionDetailsViewModel != null)
            institutionDetailsViewModel.reset();
    }


}
