package grand.app.moonshop.views.fragments.auth;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moonshop.R;
import grand.app.moonshop.base.BaseFragment;
import grand.app.moonshop.base.ParentActivity;
import grand.app.moonshop.databinding.FragmentVerificationCodeBinding;
import grand.app.moonshop.models.user.register.RegisterShopRequest;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.viewmodels.user.VerificationCodeViewModel;
import grand.app.moonshop.views.activities.BaseActivity;

public class VerificationCodeFragment extends BaseFragment {
    View rootView;
    private FragmentVerificationCodeBinding fragmentVerificationCodeBinding;
    private VerificationCodeViewModel verificationCodeViewModel;
    public String type= "",phone="", verify_id = "";

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentVerificationCodeBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_verification_code, container, false);
        getData();
        bind();
        setEvent();
        return rootView;
    }

    private static final String TAG = "VerificationCodeFragmen";
    private void getData() {
        if(getArguments()!= null && getArguments().containsKey(Constants.TYPE)){
            type = getArguments().getString(Constants.TYPE);



        }
        if (getArguments() != null && getArguments().containsKey(Constants.PHONE)) {
            phone = getArguments().getString(Constants.PHONE);
        }
        if (getArguments() != null && getArguments().containsKey(Constants.VERIFY_ID)) {
            verify_id = getArguments().getString(Constants.VERIFY_ID);
        }




    }

    private void bind() {
        verificationCodeViewModel = new VerificationCodeViewModel(verify_id);
        fragmentVerificationCodeBinding.setVerificationCodeViewModel(verificationCodeViewModel);
        rootView = fragmentVerificationCodeBinding.getRoot();
    }

    private void setEvent() {
        verificationCodeViewModel.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                String action = (String) o;
                handleActions(action, verificationCodeViewModel.getVerificationFirebaseSMSRepository().getMessage());
                Bundle bundle = getArguments();
                if(bundle == null)
                    bundle = new Bundle();
                assert action != null;
                if (action.equals(Constants.SUCCESS)) {

                    Intent intent = new Intent(getActivity(), BaseActivity.class);
                    if (type.equals(Constants.FORGET_PASSWORD)) {
                        intent.putExtra(Constants.PAGE, Constants.CHANGE_PASSWORD);
                        bundle.putString(Constants.PHONE, phone);
                        bundle.putString(Constants.TYPE, type);
                        bundle.putString(Constants.NAME_BAR, "");
                        intent.putExtra(Constants.BUNDLE, bundle);
                        startActivity(intent);

                    } else if (type.equals(Constants.REGISTRATION)) {

                        intent.putExtra(Constants.PAGE, Constants.REGISTRATION);
                        intent.putExtra(Constants.NAME_BAR,getString(R.string.personal_details));
                        bundle.putString(Constants.PHONE, phone);
                        intent.putExtra(Constants.BUNDLE, bundle);
                        ((ParentActivity) context).finish();
                        startActivity(intent);


                    }
                } else if (action.equals(Constants.REGISTRATION)) {
                    //done register
                    Intent intent = new Intent(getActivity(), BaseActivity.class);
                    toastMessage(verificationCodeViewModel.getRegisterRepository().getMessage(), R.drawable.ic_check_white, R.color.colorPrimary);
                    intent.putExtra(Constants.PAGE, Constants.LOGIN);
                    intent.putExtra(Constants.BUNDLE, bundle);
                    ((ParentActivity) context).finishAffinity();
                    startActivity(intent);
                }
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        verificationCodeViewModel.reset();

    }


}
