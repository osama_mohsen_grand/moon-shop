package grand.app.moonshop.views.activities;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;

import androidx.appcompat.app.AlertDialog;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import grand.app.moonshop.R;
import grand.app.moonshop.base.ParentActivity;
import grand.app.moonshop.base.ParentViewModel;
import grand.app.moonshop.customviews.actionbar.BackActionBarView;
import grand.app.moonshop.databinding.ActivityBaseBinding;
import grand.app.moonshop.databinding.DialogFamousOfferBinding;
import grand.app.moonshop.notification.NotificationGCMModel;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.utils.LanguagesHelper;
import grand.app.moonshop.utils.MovementHelper;
import grand.app.moonshop.utils.dialog.DialogHelper;
import grand.app.moonshop.utils.resources.ResourceManager;
import grand.app.moonshop.utils.storage.user.UserHelper;
import grand.app.moonshop.viewmodels.common.BaseViewModel;
import grand.app.moonshop.viewmodels.offer.DialogFamousOfferViewModel;
import grand.app.moonshop.views.fragments.PackageFragment;
import grand.app.moonshop.views.fragments.ads.AddAdsProductCompanyFragment;
import grand.app.moonshop.views.fragments.ads.AdsFragment;
import grand.app.moonshop.views.fragments.adsCompanyFilter.AdsCompanyCategoryFragment;
import grand.app.moonshop.views.fragments.adsCompanyFilter.AdsCompanySubCategoryFragment;
import grand.app.moonshop.views.fragments.auth.ChangePasswordFragment;
import grand.app.moonshop.views.fragments.auth.PhoneVerificationFragment;
import grand.app.moonshop.views.fragments.auth.RegisterFragment;
import grand.app.moonshop.views.fragments.auth.RegisterFragment2;
import grand.app.moonshop.views.fragments.auth.RegisterFragment3;
import grand.app.moonshop.views.fragments.auth.VerificationCodeFragment;
import grand.app.moonshop.views.fragments.base.NoConnectionFragment;
import grand.app.moonshop.views.fragments.base.VideoFragment;
import grand.app.moonshop.views.fragments.base.ZoomFragment;
import grand.app.moonshop.views.fragments.category.AddCategoryFragment;
import grand.app.moonshop.views.fragments.chat.ChatFragment;
import grand.app.moonshop.views.fragments.famous.AlbumSearchFragment;
import grand.app.moonshop.views.fragments.famous.FamousAddAddsFragment;
import grand.app.moonshop.views.fragments.famous.FamousAddAlbumFragment;
import grand.app.moonshop.views.fragments.famous.FamousAdvertiseDetailsFragment;
import grand.app.moonshop.views.fragments.famous.FamousAlbumMainDetailsFragment;
import grand.app.moonshop.views.fragments.famous.FamousDetailsFragment;
import grand.app.moonshop.views.fragments.famous.FamousDetailsMainFragment;
import grand.app.moonshop.views.fragments.famous.FamousDiscover;
import grand.app.moonshop.views.fragments.famous.FamousEditAlbumFragment;
import grand.app.moonshop.views.fragments.famous.FamousSearchFragment;
import grand.app.moonshop.views.fragments.institution.AddBranchInstitutionFragment;
import grand.app.moonshop.views.fragments.institution.AddServiceInstitutionFragment;
import grand.app.moonshop.views.fragments.institution.InstitutionEditDetailsFragment;
import grand.app.moonshop.views.fragments.location.OpenLocationFragment;
import grand.app.moonshop.views.fragments.notification.NotificationFragment;
import grand.app.moonshop.views.fragments.offers.AddOfferFragment;
import grand.app.moonshop.views.fragments.order.OrderDetailsFragment;
import grand.app.moonshop.views.fragments.product.AddProductConsumerSectoralFragment;
import grand.app.moonshop.views.fragments.product.AddProductSectoralFragment;
import grand.app.moonshop.views.fragments.product.ProductFragment;
import grand.app.moonshop.views.fragments.product.ProductsFragment;
import grand.app.moonshop.views.fragments.profile.ProfileFragment;
import grand.app.moonshop.views.fragments.reservation.ReservationDetailsFragment;
import grand.app.moonshop.views.fragments.reservation.beauty.BeautyAddServiceFragment;
import grand.app.moonshop.views.fragments.reservation.beauty.BeautyOrderDetailsFragment;
import grand.app.moonshop.views.fragments.reservation.clinic.AddDoctorFragment;
import grand.app.moonshop.views.fragments.reservation.clinic.DoctorScheduleFragment;
import grand.app.moonshop.views.fragments.reservation.clinic.ReservationMainFragment;
import grand.app.moonshop.views.fragments.service.ServiceListCheckFragment;
import grand.app.moonshop.views.fragments.settings.CountryFragment;
import grand.app.moonshop.views.fragments.settings.LanguageFragment;
import grand.app.moonshop.views.fragments.SplashFragment;
import grand.app.moonshop.views.fragments.auth.LoginFragment;
import grand.app.moonshop.views.fragments.settings.SettingsFragment;
import grand.app.moonshop.vollyutils.MyApplication;
import timber.log.Timber;

import static grand.app.moonshop.utils.dialog.DialogHelper.showDialog;

public class BaseActivity extends ParentActivity {

    ActivityBaseBinding activityBaseBinding;
    ParentViewModel baseViewModel;


    BackActionBarView backActionBarView;
    boolean notification_checked = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        getWindow().requestFeature(Window.FEATURE_CONTENT_TRANSITIONS);

//        LanguagesHelper.setLanguage(this,"ar");
        LanguagesHelper.changeLanguage(this, LanguagesHelper.getCurrentLanguage());
        LanguagesHelper.changeLanguage(MyApplication.getInstance(), LanguagesHelper.getCurrentLanguage());
        activityBaseBinding = DataBindingUtil.setContentView(this, R.layout.activity_base);
        getNotification();
        baseViewModel = new BaseViewModel();
        activityBaseBinding.setBaseViewModel(baseViewModel);
        Timber.e("Done");
        if (!notification_checked) {
            if (getIntent().getExtras() != null) {
                Timber.e("getIntent");
                if (getIntent().getExtras() != null) {
                    if (getIntent().hasExtra(Constants.PAGE)) {
                        Timber.e(getIntent().getStringExtra(Constants.PAGE));
                        String fragmentName = getIntent().getStringExtra(Constants.PAGE);
//                        liveData.setValue(new Mutable());
                        Timber.e("Fragment New Instance: "+fragmentName);
                        Fragment fragment = null;
                        try {
                            fragment = (Fragment) Class.forName(fragmentName).newInstance();
                            MovementHelper.addFragmentTag(this, getBundle(fragment), fragmentName, "");
                        } catch (IllegalAccessException | InstantiationException | ClassNotFoundException e) {
                            e.printStackTrace();
                        }

                        if (getIntent().getStringExtra(Constants.PAGE).equals(Constants.LOGIN)) {
                            MovementHelper.addFragment(this, new LoginFragment(), "");
                        } else if (getIntent().getStringExtra(Constants.PAGE).equals(Constants.FAILURE_CONNECTION)) {
                            MovementHelper.addFragment(this, getBundle(new NoConnectionFragment()), "");
                        } else if (getIntent().getStringExtra(Constants.PAGE).equals(Constants.LANGUAGE)) {
                            MovementHelper.addFragment(this, getBundle(new LanguageFragment()), "");
                        } else if (getIntent().getStringExtra(Constants.PAGE).equals(Constants.OPEN_LOCATION)) {
                            MovementHelper.addFragment(this, getBundle(new OpenLocationFragment()), "");
                        } else if (getIntent().getStringExtra(Constants.PAGE).equals(Constants.COUNTRIES)) {
                            MovementHelper.addFragment(this, getBundle(new CountryFragment()), "");
                        } else if (getIntent().getStringExtra(Constants.PAGE).equals(Constants.REGISTRATION)) {
                            MovementHelper.addFragment(this, getBundle(new RegisterFragment()), "");
                        } else if (getIntent().getStringExtra(Constants.PAGE).equals(Constants.REGISTRATION2)) {
                            Timber.e("REGO2");
                            MovementHelper.addFragment(this, getBundle(new RegisterFragment2()), "");
                        } else if (getIntent().getStringExtra(Constants.PAGE).equals(Constants.REGISTRATION3)) {
                            Timber.e("REGO3");
                            MovementHelper.addFragment(this, getBundle(new RegisterFragment3()), "");
                        }
                        else if (getIntent().getStringExtra(Constants.PAGE).equals(Constants.PHONE_VERIFICATION)) {
                            MovementHelper.addFragment(this, getBundle(new PhoneVerificationFragment()), "");
                        } else if (getIntent().getStringExtra(Constants.PAGE).equals(Constants.VERIFICATION)) {
                            MovementHelper.addFragment(this, getBundle(new VerificationCodeFragment()), "");
                        } else if (getIntent().getStringExtra(Constants.PAGE).equals(Constants.ADD_CATEGORY)) {
                            MovementHelper.addFragment(this, getBundle(new AddCategoryFragment()), "");
                        } else if (getIntent().getStringExtra(Constants.PAGE).equals(Constants.PACKAGES)) {
                            MovementHelper.addFragment(this, getBundle(new PackageFragment()), "");
                        } else if (getIntent().getStringExtra(Constants.PAGE).equals(Constants.PRODUCT)) {
                            MovementHelper.addFragment(this, getBundle(new ProductFragment()), "");
                        } else if (getIntent().getStringExtra(Constants.PAGE).equals(Constants.PRODUCTS)) {
                            MovementHelper.addFragment(this, getBundle(new ProductsFragment()), "");
                        } else if (getIntent().getStringExtra(Constants.PAGE).equals(Constants.ADD_PRODUCT_SECTORAL)) {
                            MovementHelper.addFragment(this, getBundle(new AddProductSectoralFragment()), "");
                        } else if (getIntent().getStringExtra(Constants.PAGE).equals(Constants.ADD_PRODUCT_CONSUMER_SECTORAL)) {
                            MovementHelper.addFragment(this, getBundle(new AddProductConsumerSectoralFragment()), "");
                        } else if (getIntent().getStringExtra(Constants.PAGE).equals(Constants.ZOOM)) {
                            MovementHelper.addFragment(this, getBundle(new ZoomFragment()), "");
                        } else if (getIntent().getStringExtra(Constants.PAGE).equals(Constants.VIDEO)) {
                            MovementHelper.addFragment(this, getBundle(new VideoFragment()), "");
                        } else if (getIntent().getStringExtra(Constants.PAGE).equals(Constants.Add_OFFER)) {
                            MovementHelper.addFragment(this, getBundle(new AddOfferFragment()), "");
                        } else if (getIntent().getStringExtra(Constants.PAGE).equals(Constants.FAMOUS_MAIN_DETAILS)) {
                            MovementHelper.addFragmentTag(this, getBundle(new FamousDetailsMainFragment()), Constants.FAMOUS_MAIN_DETAILS, "");
                        } else if (getIntent().getStringExtra(Constants.PAGE).equals(Constants.FAMOUS_DETAILS)) {
                            MovementHelper.addFragmentTag(this, getBundle(new FamousDetailsFragment()), Constants.FAMOUS_DETAILS, "");
                        } else if (getIntent().getStringExtra(Constants.PAGE).equals(Constants.FAMOUS_ADVERTISE)) {
                            MovementHelper.addFragment(this, getBundle(new FamousAdvertiseDetailsFragment()), "");
                        } else if (getIntent().getStringExtra(Constants.PAGE).equals(Constants.PROFILE)) {
                            MovementHelper.addFragment(this, getBundle(new ProfileFragment()), "");
                        } else if (getIntent().getStringExtra(Constants.PAGE).equals(Constants.CHANGE_PASSWORD)) {
                            MovementHelper.addFragment(this, getBundle(new ChangePasswordFragment()), "");
                        } else if (getIntent().getStringExtra(Constants.PAGE).equals(Constants.ADD_ALBUM)) {
                            MovementHelper.addFragment(this, getBundle(new FamousAddAlbumFragment()), "");
                        } else if (getIntent().getStringExtra(Constants.PAGE).equals(Constants.ALBUM_DETAILS)) {
                            MovementHelper.addFragmentWithDisAllowBackTag(this, getBundle(new FamousAlbumMainDetailsFragment()), fragmentName,"");
                        } else if (getIntent().getStringExtra(Constants.PAGE).equals(Constants.EDIT_ALBUM)) {
                            MovementHelper.addFragment(this, getBundle(new FamousEditAlbumFragment()), "");
                        } else if (getIntent().getStringExtra(Constants.PAGE).equals(Constants.ADD_ADS)) {
                            MovementHelper.addFragmentWithDisAllowBackTag(this, getBundle(new FamousAddAddsFragment()), fragmentName,"");
                        } else if (getIntent().getStringExtra(Constants.PAGE).equals(Constants.MY_ADS)) {
                            MovementHelper.addFragment(this, getBundle(new AdsFragment()), "");
                        } else if (getIntent().getStringExtra(Constants.PAGE).equals(Constants.SEARCH_ALBUM)) {
                            MovementHelper.addFragment(this, getBundle(new AlbumSearchFragment()), "");
                        } else if (getIntent().getStringExtra(Constants.PAGE).equals(Constants.SEARCH_FAMOUS)) {
                            MovementHelper.addFragment(this, getBundle(new FamousSearchFragment()), "");
                        } else if (getIntent().getStringExtra(Constants.PAGE).equals(Constants.DISCOVER)) {
                            MovementHelper.addFragment(this, getBundle(new FamousDiscover()), "");
                        } else if (getIntent().getStringExtra(Constants.PAGE).equals(Constants.CHAT_DETAILS)) {
                            MovementHelper.addFragment(this, getBundle(new ChatFragment()), "");
                        } else if (getIntent().getStringExtra(Constants.PAGE).equals(Constants.ORDER_DETAILS)) {
                            MovementHelper.replaceFragment(this, getBundle(new OrderDetailsFragment()), "");
                        } else if (getIntent().getStringExtra(Constants.PAGE).equals(Constants.ADD_BRANCH)) {
                            MovementHelper.addFragment(this, getBundle(new AddBranchInstitutionFragment()), "");
                        } else if (getIntent().getStringExtra(Constants.PAGE).equals(Constants.EDIT_INSTITUTION)) {
                            MovementHelper.addFragment(this, getBundle(new InstitutionEditDetailsFragment()), "");
                        } else if (getIntent().getStringExtra(Constants.PAGE).equals(Constants.ADD_SERVICE)) {
                            MovementHelper.addFragment(this, getBundle(new AddServiceInstitutionFragment()), "");
                        } else if (getIntent().getStringExtra(Constants.PAGE).equals(Constants.ADD_ADS_COMPANY)) {
                            MovementHelper.addFragment(this, getBundle(new AddAdsProductCompanyFragment()), "");
                        } else if (getIntent().getStringExtra(Constants.PAGE).equals(Constants.SERVICES_LIST)) {
                            MovementHelper.addFragment(this, getBundle(new ServiceListCheckFragment()), "");
                        } else if (getIntent().getStringExtra(Constants.PAGE).equals(Constants.ADD_DOCTOR)) {
                            MovementHelper.addFragment(this, getBundle(new AddDoctorFragment()), "");
                        } else if (getIntent().getStringExtra(Constants.PAGE).equals(Constants.DOCTOR_SCHEDULE)) {
                            MovementHelper.addFragment(this, getBundle(new DoctorScheduleFragment()), "");
                        } else if (getIntent().getStringExtra(Constants.PAGE).equals(Constants.ADD_BEAUTY_SERVICE)) {
                            MovementHelper.addFragment(this, getBundle(new BeautyAddServiceFragment()), "");
                        } else if (getIntent().getStringExtra(Constants.PAGE).equals(Constants.ORDER_DETAILS_BEAUTY)) {
                            MovementHelper.addFragment(this, getBundle(new BeautyOrderDetailsFragment()), "");
                        } else if (getIntent().getStringExtra(Constants.PAGE).equals(Constants.RESERVATION_DETAILS)) {
                            MovementHelper.addFragment(this, getBundle(new ReservationDetailsFragment()), "");
                        } else if (getIntent().getStringExtra(Constants.PAGE).equals(Constants.ADS_COMPANY_SUB_CATEGORY)) {
                            MovementHelper.addFragment(this, getBundle(new AdsCompanySubCategoryFragment()), "");
                        } else if (getIntent().getStringExtra(Constants.PAGE).equals(Constants.ADS_COMPANY_FILTER)) {
                            MovementHelper.addFragment(this, getBundle(new AdsCompanyCategoryFragment()), "");
                        } else if (getIntent().getStringExtra(Constants.PAGE).equals(Constants.SETTINGS)) {
                            MovementHelper.addFragment(this, getBundle(new SettingsFragment()), "");
                        }
//                        }
//                        //BUILD_MUSCLE_BEGINNER
//                        else if (getIntent().getStringExtra(Constants.PAGE).equals(Constants.DETAILS_PRICE)) {
//                            MovementHelper.addFragment(this, getBundle(new DetailsPriceFragment()), "");
//                        } else
//                            MovementHelper.addFragment(this, new SplashFragment(), "");
                    } else
                        MovementHelper.addFragment(this, new SplashFragment(), "");
                } else
                    MovementHelper.addFragment(this, new SplashFragment(), "");
            } else
                MovementHelper.addFragment(this, new SplashFragment(), "");
        }
    }


    private void setTitleName(Bundle bundle) {
        Timber.e("Title Name");
        if (getIntent().hasExtra(Constants.NAME_BAR)) {
            Timber.e("Title Name Done");
            backActionBarView = new BackActionBarView(this);
            backActionBarView.setTitle(getIntent().getStringExtra(Constants.NAME_BAR));
            activityBaseBinding.llBaseActionBarContainer.addView(backActionBarView);
        }
    }

    private void setTitleName(String name) {
        backActionBarView = new BackActionBarView(this);
        backActionBarView.setTitle(name);
        activityBaseBinding.llBaseActionBarContainer.addView(backActionBarView);
    }


    private Fragment getBundle(Fragment fragment) {
        Bundle bundle = getIntent().getBundleExtra(Constants.BUNDLE);
        fragment.setArguments(bundle);
        if (getIntent().hasExtra(Constants.NAME_BAR)) {
            setTitleName(bundle);
        }
        return fragment;
    }

    private static final String TAG = "BaseActivity";

    private void getNotification() {
        Timber.e("get Notification");
        if (getIntent().getExtras() != null && getIntent().hasExtra(Constants.BUNDLE_NOTIFICATION)) {

            Intent intent = new Intent(this, BaseActivity.class);
            Bundle bundle = getIntent().getBundleExtra(Constants.BUNDLE_NOTIFICATION);
            NotificationGCMModel notificationGCMModel = (NotificationGCMModel) bundle.getSerializable(Constants.BUNDLE_NOTIFICATION);
//            if(notificationGCMModel.notification_type == 1) {//new order
//                intent.putExtra(Constants.NAME_BAR, ResourceManager.getString(R.string.order_details));
//                intent.putExtra(Constants.PAGE, Constants.ORDER_DETAILS);
//                bundle.putInt(Constants.ID, notificationGCMModel.order_id);
//                bundle.putInt(Constants.STATUS, 1);
//                intent.putExtra(Constants.BUNDLE, bundle);
//                notification_checked = true;
//                startActivity(intent);
//            }else
            if (notificationGCMModel.notification_type == 2) {//chat order
                setTitleName(ResourceManager.getString(R.string.chat));
                bundle.putInt(Constants.ID, notificationGCMModel.order_id);
                bundle.putInt(Constants.TYPE, notificationGCMModel.sender_type);
                notification_checked = true;
                ChatFragment chatFragment = new ChatFragment();
                chatFragment.setArguments(bundle);
                MovementHelper.replaceFragment(this, chatFragment, "");
                return;
            } else if (notificationGCMModel.notification_type == 5) {//chat normal
                Timber.e("notification_type 5");
                notification_checked = true;
                ChatFragment chatFragment = new ChatFragment();
                bundle.putInt(Constants.ID_CHAT, notificationGCMModel.id);
                bundle.putInt(Constants.TYPE, notificationGCMModel.sender_type);
                bundle.putBoolean(Constants.ALLOW_CHAT, true);
                bundle.putBoolean(Constants.CHAT_FIRST, true);
                intent.putExtra(Constants.BUNDLE, bundle);
                chatFragment.setArguments(bundle);
                MovementHelper.replaceFragment(this, chatFragment, "");
                return;
            } else if (notificationGCMModel.notification_type == Integer.parseInt(Constants.TYPE_RESERVATION_CLINIC) || notificationGCMModel.notification_type == Integer.parseInt(Constants.TYPE_RESERVATION_BEAUTY)) {
                Timber.e("notification_type 9");
                setTitleName(ResourceManager.getString(R.string.reservation));
                notification_checked = true;
                MovementHelper.replaceFragment(this, new ReservationMainFragment(), "");
                return;
            } else if (notificationGCMModel.notification_type == Integer.parseInt(Constants.TYPE_ADVERTISING)) {
                notification_checked = true;
                MovementHelper.replaceFragment(this, new NotificationFragment(), "");
                return;
            }
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        LanguagesHelper.changeLanguage(MyApplication.getInstance(), LanguagesHelper.getCurrentLanguage());
        LanguagesHelper.changeLanguage(this, LanguagesHelper.getCurrentLanguage());
        super.onActivityResult(requestCode, resultCode, data);
        Log.d("requestCode", requestCode + "");
//        if (resultCode == RESULT_OK) {
//            Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fl_home_container);
//            assert fragment != null;
//            fragment.onActivityResult(requestCode, resultCode, data);
//        }
        //((LoginFragment) BaseActivity.this).onActivityResult(requestCode,resultCode,data);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
//        getWindow().setExitTransition(new Explode());
    }
}