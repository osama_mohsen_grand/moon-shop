package grand.app.moonshop.views.fragments.institution;


import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.theartofdev.edmodo.cropper.CropImage;
import com.yalantis.ucrop.UCrop;

import java.io.File;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moonshop.R;
import grand.app.moonshop.adapter.AlbumAddAdapter;
import grand.app.moonshop.base.ApplicationBinding;
import grand.app.moonshop.base.BaseFragment;
import grand.app.moonshop.base.ParentActivity;
import grand.app.moonshop.databinding.FragmentAddBranchInstitutionBinding;
import grand.app.moonshop.databinding.FragmentAddServiceInstitutionBinding;
import grand.app.moonshop.models.base.IdNameImage;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.utils.resources.ResourceManager;
import grand.app.moonshop.utils.storage.user.UserHelper;
import grand.app.moonshop.utils.upload.FileOperations;
import grand.app.moonshop.viewmodels.branch.AddBranchInstitutionViewModel;
import grand.app.moonshop.viewmodels.branch.AddServiceInstitutionViewModel;
import grand.app.moonshop.views.fragments.famous.FamousAddAlbumFragment;
import grand.app.moonshop.vollyutils.VolleyFileObject;
import timber.log.Timber;

import static android.app.Activity.RESULT_OK;
import static com.facebook.FacebookSdk.getCacheDir;

/**
 * A simple {@link Fragment} subclass.
 */
public class AddServiceInstitutionFragment extends BaseFragment {
    FragmentAddServiceInstitutionBinding fragmentAddBranchInstitutionBinding;
    AddServiceInstitutionViewModel addServiceInstitutionViewModel;
    AlbumAddAdapter albumAdapter = null;
    int FILE_TYPE = -1;

    //    int MAX_ITEMS = 6;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        fragmentAddBranchInstitutionBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_add_service_institution, container, false);
        init();
        getData();
        setEvent();
        return fragmentAddBranchInstitutionBinding.getRoot();
    }

    private void getData() {
        if(getArguments() != null && getArguments().containsKey(Constants.CATEGORY)){
            IdNameImage idNameImage = (IdNameImage) getArguments().getSerializable(Constants.CATEGORY);
            addServiceInstitutionViewModel.setData(idNameImage );
            Glide
                    .with(context)
                    .load(idNameImage.image)
                    .centerCrop()
//                .placeholder(R.drawable.progress_animation)
                    .into(fragmentAddBranchInstitutionBinding.imgAddAlbum);
//            fragmentAddBranchInstitutionBinding.imgAddBranchPhoto
        }
    }


    private void init() {
        addServiceInstitutionViewModel = new AddServiceInstitutionViewModel();
        fragmentAddBranchInstitutionBinding.setAddBranchInstitutionViewModel(addServiceInstitutionViewModel);
    }


    private void setEvent() {
        addServiceInstitutionViewModel.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                String action = (String) o;
                handleActions(action, addServiceInstitutionViewModel.getInstitutionRepository().getMessage());
                assert action != null;
                if (action.equals(Constants.IMAGE)) {
                    pickImageDialogSelect();
                } else if (action.equals(Constants.SUCCESS)) {
                    Toast.makeText(context, "" + addServiceInstitutionViewModel.getInstitutionRepository().getMessage(), Toast.LENGTH_SHORT).show();
                    reloadPage();
                    getActivityBase().finish();
                } else if (action.equals(Constants.ERROR)) {
                    showError(addServiceInstitutionViewModel.baseError);
                }
            }
        });
    }

    int next = -1;

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        //super method removed
        Timber.e("onActivityResult:" + requestCode);
        if (requestCode == Constants.FILE_TYPE_IMAGE) {
//            addServiceInstitutionViewModel.addServiceRequest.volleyFileObject = FileOperations.getVolleyFileObject(getActivity(), data, "image", Constants.FILE_TYPE_IMAGE);
//            fragmentAddBranchInstitutionBinding.imgAddAlbum.setImageURI(Uri.parse(String.valueOf(new File(addServiceInstitutionViewModel.addServiceRequest.volleyFileObject.getFilePath()))));



            VolleyFileObject volleyFileObject = FileOperations.getVolleyFileObject(getActivity(), data, Constants.IMAGE, Constants.FILE_TYPE_IMAGE);
            File file = new File(getCacheDir(), Constants.IMAGE + ".png");
            UCrop.of(Uri.fromFile(volleyFileObject.getFile()), Uri.fromFile(file))
                    .start(context, AddServiceInstitutionFragment.this);

        }
        if (requestCode == UCrop.REQUEST_CROP && data != null) {
            final Uri resultUri = UCrop.getOutput(data);
            if (resultUri != null) {
                addServiceInstitutionViewModel.addServiceRequest.volleyFileObject = new VolleyFileObject(Constants.IMAGE, resultUri.getPath(), Constants.FILE_TYPE_IMAGE);
                fragmentAddBranchInstitutionBinding.imgAddAlbum.setImageURI(resultUri);
//                famousAddAlbumViewModel.notifyChange();
            }
        }

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE && data != null) {

            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();
                UCrop.of(resultUri, resultUri).start(context, AddServiceInstitutionFragment.this);
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Toast.makeText(context, "" + getString(R.string.error_failed_pick_gallery_image), Toast.LENGTH_SHORT).show();
            }
        }


        super.onActivityResult(requestCode, resultCode, data);
    }


}
