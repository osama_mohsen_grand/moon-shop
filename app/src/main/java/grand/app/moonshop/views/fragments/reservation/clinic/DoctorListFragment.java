package grand.app.moonshop.views.fragments.reservation.clinic;


import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moonshop.R;
import grand.app.moonshop.adapter.DoctorsAdapter;
import grand.app.moonshop.base.BaseFragment;
import grand.app.moonshop.customviews.dialog.DialogConfirm;
import grand.app.moonshop.databinding.FragmentDoctorListBinding;
import grand.app.moonshop.models.app.Mutable;
import grand.app.moonshop.utils.helper.AppUtils;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.utils.dialog.DialogHelperInterface;
import grand.app.moonshop.utils.resources.ResourceManager;
import grand.app.moonshop.viewmodels.reservation.doctors.DoctorListViewModel;
import grand.app.moonshop.views.activities.BaseActivity;
import timber.log.Timber;

/**
 * A simple {@link Fragment} subclass.
 */
public class DoctorListFragment extends BaseFragment {

    private FragmentDoctorListBinding fragmentDoctorListBinding;
    private DoctorListViewModel doctorListViewModel;
    private DoctorsAdapter doctorsAdapter;


    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        fragmentDoctorListBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_doctor_list, container, false);
        bind();
        setEvent();
        return fragmentDoctorListBinding.getRoot();
    }

    private void bind() {
        doctorListViewModel = new DoctorListViewModel();
        AppUtils.initVerticalRV(fragmentDoctorListBinding.rvDoctors, fragmentDoctorListBinding.rvDoctors.getContext(), 1);
        fragmentDoctorListBinding.setDoctorListViewModel(doctorListViewModel);
    }

    private void setEvent() {
        doctorListViewModel.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                String action = (String) o;
                handleActions(action, doctorListViewModel.getReservationRepository().getMessage());
                assert action != null;
                if (action.equals(Constants.SUCCESS)) {
                    Timber.e("" + doctorListViewModel.getReservationRepository().getDoctorListResponse().doctors.size());
                    doctorsAdapter = new DoctorsAdapter(doctorListViewModel.getReservationRepository().getDoctorListResponse().doctors);
                    fragmentDoctorListBinding.rvDoctors.setAdapter(doctorsAdapter);
                    setEventAdapter();
                } else if (action.equals(Constants.DELETED)) {
                    toastMessage(doctorListViewModel.getReservationRepository().getMessage(), R.drawable.ic_check_white, R.color.colorPrimary);
                    doctorsAdapter.remove(doctorListViewModel.category_delete_position);
                } else if (action.equals(Constants.ADD)) {
                    Intent intent = new Intent(context, BaseActivity.class);
                    intent.putExtra(Constants.PAGE, Constants.ADD_DOCTOR);
                    intent.putExtra(Constants.NAME_BAR, ResourceManager.getString(R.string.add_doctor));
                    startActivityForResult(intent, Constants.RELOAD_RESULT);
                }
            }
        });
    }


    private void setEventAdapter() {
        doctorsAdapter.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                Mutable mutable = (Mutable) o;
                if (mutable.type.equals(Constants.EDIT)) {
                    Intent intent = new Intent(context, BaseActivity.class);
                    intent.putExtra(Constants.PAGE, Constants.DOCTOR_SCHEDULE);
                    intent.putExtra(Constants.NAME_BAR, ResourceManager.getString(R.string.date_and_time));
                    Bundle bundle = new Bundle();
                    bundle.putInt(Constants.ID, doctorListViewModel.getReservationRepository().getDoctorListResponse().doctors.get(mutable.position).id);
                    intent.putExtra(Constants.BUNDLE, bundle);
                    startActivity(intent);
                } else if (mutable.type.equals(Constants.DELETE)) {

                    new DialogConfirm(getActivity())
                            .setTitle(ResourceManager.getString(R.string.remove))
                            .setMessage(ResourceManager.getString(R.string.do_you_want_delete_doctor))
                            .setActionText(ResourceManager.getString(R.string.submit))
                            .setActionCancel(ResourceManager.getString(R.string.cancel))
                            .show(new DialogHelperInterface() {
                                @Override
                                public void OnClickListenerContinue(Dialog dialog, View view) {
                                    doctorListViewModel.category_delete_position = mutable.position;
                                    doctorListViewModel.getReservationRepository().delete(doctorListViewModel.getReservationRepository().getDoctorListResponse().doctors.get(mutable.position).id);
                                }
                            });


                }
            }
        });
    }


    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK && requestCode == Constants.RELOAD_RESULT) {
            doctorListViewModel.getReservationRepository().getDoctorList();
        }

        super.onActivityResult(requestCode, resultCode, data);
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        doctorListViewModel.reset();
    }
}
