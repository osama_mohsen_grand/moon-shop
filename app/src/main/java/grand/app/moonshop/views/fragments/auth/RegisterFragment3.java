package grand.app.moonshop.views.fragments.auth;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import java.util.ArrayList;
import java.util.List;
import grand.app.moonshop.MultipleImageSelect.activities.AlbumSelectActivity;
import grand.app.moonshop.R;
import grand.app.moonshop.adapter.SuggestAdapter;
import grand.app.moonshop.base.BaseFragment;
import grand.app.moonshop.base.ParentActivity;
import grand.app.moonshop.customviews.facebook.FacebookModel;
import grand.app.moonshop.databinding.FragmentRegister3Binding;
import grand.app.moonshop.models.app.AppMoon;
import grand.app.moonshop.models.country.City;
import grand.app.moonshop.models.country.Datum;
import grand.app.moonshop.models.country.Region;
import grand.app.moonshop.models.user.register.RegisterShopRequest;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.utils.PopUp.PopUpInterface;
import grand.app.moonshop.utils.PopUp.PopUpMenuHelper;
import grand.app.moonshop.utils.storage.user.UserHelper;
import grand.app.moonshop.utils.upload.FileOperations;
import grand.app.moonshop.viewmodels.user.RegisterViewModel;
import grand.app.moonshop.views.activities.BaseActivity;
import grand.app.moonshop.views.activities.MainActivity;
import grand.app.moonshop.views.activities.MapAddressActivity;
import timber.log.Timber;


public class RegisterFragment3 extends BaseFragment {
    View rootView;

    private FragmentRegister3Binding  fragmentRegister3Binding;
    private RegisterViewModel registerViewModel;
    public String type = "", phone = "";
    public PopUpMenuHelper popUpMenuHelper = new PopUpMenuHelper();
    public FacebookModel facebookModel;
    SuggestAdapter suggestAdapter = null;
    //1=>accept , 2=>reject  , 3=>arrive  , 4=>start  , 5=> cancel after accept ,  6=>finished trip

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentRegister3Binding = DataBindingUtil.inflate(inflater, R.layout.fragment_register_3, container, false);

        getData();
        bind();
        setEvent();
        getCountriesRegionListeners();
        return rootView;
    }

    private void getData() {
        
        if (getArguments() != null && getArguments().containsKey(Constants.TYPE)) {
            type = getArguments().getString(Constants.TYPE);
        }
        if (getArguments() != null && getArguments().containsKey(Constants.PHONE)) {

            phone = getArguments().getString(Constants.PHONE);

        }
        if (getArguments() != null && getArguments().containsKey(Constants.FACEBOOK)) {
            facebookModel = (FacebookModel) getArguments().getSerializable(Constants.FACEBOOK);
        }


    }

    private void bind() {

        registerViewModel = new RegisterViewModel(  (RegisterShopRequest) getArguments().getSerializable(Constants.REGESTER_REQUEST));

        Timber.e("phone >>4 "+phone);

         if (facebookModel != null) {

            registerViewModel.setSocial(facebookModel);
        }
        fragmentRegister3Binding.setRegisterViewModel(registerViewModel);

        rootView = fragmentRegister3Binding.getRoot();
    }

    private void pickImageDialog() {
        FileOperations.pickImage(context, RegisterFragment3.this, Constants.FILE_TYPE_IMAGE);
    }




    public List<Region> regions = new ArrayList<>();

    private void setEvent() {

        registerViewModel.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                String action = (String) o;
                handleActions(action, registerViewModel.getCountryRepository().getMessage());
                handleActions(action, registerViewModel.getRegisterRepository().getMessage());
                handleActions(action, registerViewModel.getServiceRepository().getMessage());
                Timber.e(action);
                if (action.equals(Constants.HOME)) {//submitSearch register
                    toastMessage(registerViewModel.getRegisterRepository().getMessage(), R.drawable.ic_check_white, R.color.colorPrimary);
                    Intent intent = new Intent(context, MainActivity.class);
//                    intent.putExtra(Constants.PAGE, Constants.LOGIN);
                    ((ParentActivity) context).finishAffinity();
                    startActivity(intent);
                } else if (action.equals(Constants.PACKAGES)) {//get service type
                    //Log services
                    Intent intent = new Intent(context, BaseActivity.class);
                    intent.putExtra(Constants.PAGE, Constants.PACKAGES);
                    ((AppCompatActivity) context).finishAffinity();
                    startActivity(intent);
                } else if (action.equals(Constants.SELECT_IMAGE)) {//select image profile
                    if (registerViewModel.image_select.equals(Constants.COMMERCIAL_IMAGE) || registerViewModel.image_select.equals(Constants.LICENCE_IMAGE)) {
                        Intent intent = new Intent(context, AlbumSelectActivity.class);
                        intent.putExtra(grand.app.moonshop.MultipleImageSelect.helpers.Constants.INTENT_EXTRA_LIMIT, 2);
                        startActivityForResult(intent, Constants.REQUEST_CODE);
                    } else
                        pickImageDialog();

                } else if (action.equals(Constants.ERROR)) {//ex: like image
                    showError(registerViewModel.baseError);
                } else if (action.equals(Constants.LOCATIONS)) {//select location
                    Timber.e("Locations");
                    Intent intent = new Intent(context, MapAddressActivity.class);
                    startActivityForResult(intent, Constants.ADDRESS_RESULT);
                } else if (action.equals(Constants.WRITE_CODE)) {
                    toastMessage(registerViewModel.getVerificationFirebaseSMSRepository().getMessage(), R.drawable.ic_check_white, R.color.colorPrimary);
                    Intent intent = new Intent(context, BaseActivity.class);
                    intent.putExtra(Constants.PAGE, Constants.VERIFICATION);
                    Bundle bundle = new Bundle();
                    bundle.putString(Constants.TYPE, Constants.REGISTRATION);
                    bundle.putSerializable(Constants.USER, registerViewModel.request);
                    bundle.putStringArrayList(Constants.IMAGES_PATH, registerViewModel.request.imagesPath);
                    bundle.putStringArrayList(Constants.IMAGES_KEY, registerViewModel.request.imagesKey);
                    bundle.putString(Constants.VERIFY_ID, registerViewModel.getVerificationFirebaseSMSRepository().getVerificationId());
                    bundle.putString(Constants.NAME_BAR, "");
                    bundle.putString(Constants.TYPE, Constants.REGISTRATION);
                    intent.putExtra(Constants.BUNDLE, bundle);
                    startActivity(intent);
                } else if (action.equals(Constants.TERMS)) {
                    Intent intent = new Intent(context, BaseActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putInt(Constants.TYPE, Constants.TYPE_TERMS);
                    intent.putExtra(Constants.BUNDLE, bundle);
                    intent.putExtra(Constants.PAGE, Constants.SETTINGS);
                    intent.putExtra(Constants.NAME_BAR, getString(R.string.terms_and_privacy_policy));
                    startActivity(intent);
                }
            }
        });
    }

    private void getCountriesRegionListeners(){
        fragmentRegister3Binding.edtRegisterCity.setOnClickListener(v -> {

            ArrayList<String> popUpCities = new ArrayList<>();
            if (registerViewModel.country != null) {
                Timber.e("country:" + registerViewModel.country.countryName);
                for (City city : registerViewModel.country.cities)
                    popUpCities.add(city.cityName);
                popUpMenuHelper.openPopUp(getActivity(), fragmentRegister3Binding.edtRegisterCity, popUpCities, new PopUpInterface() {
                    @Override
                    public void submitPopUp(int position) {
                        registerViewModel.request.setCity_id(String.valueOf(registerViewModel.country.cities.get(position).id));
                        fragmentRegister3Binding.edtRegisterCity.setText(registerViewModel.country.cities.get(position).cityName);
                        fragmentRegister3Binding.edtRegisterRegion.setText("");
                        regions = new ArrayList<>(registerViewModel.country.cities.get(position).regions);
                        Timber.e("regions" + regions.size());
                        getRegions();
                    }
                });
            }

        });

        fragmentRegister3Binding.edtRegisterRegion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getRegions();
            }
        });
    }

    private void getRegions() {
        ArrayList<String> popUpRegions = new ArrayList<>();
        for (Region region : regions)
            popUpRegions.add(region.regionName);
        Timber.e("size:" + popUpRegions.size());
        popUpMenuHelper.openPopUp(getActivity(), fragmentRegister3Binding.edtRegisterRegion, popUpRegions, new PopUpInterface() {
            @Override
            public void submitPopUp(int position) {
                registerViewModel.request.setRegion_id(String.valueOf(regions.get(position).id));
                fragmentRegister3Binding.edtRegisterRegion.setText(regions.get(position).regionName);
            }
        });

    }



    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        //super method removed
        Timber.e("onActivityResult:" + requestCode);
        if (resultCode == Activity.RESULT_OK && requestCode == Constants.ADDRESS_RESULT) {
            registerViewModel.setAddress(data.getDoubleExtra("lat", 0), data.getDoubleExtra("lng", 0));
        }
        super.onActivityResult(requestCode, resultCode, data);

    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        registerViewModel.reset();

    }


}
