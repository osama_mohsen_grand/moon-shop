package grand.app.moonshop.views.fragments.reservation.beauty;


import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.databinding.DataBindingUtil;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moonshop.R;
import grand.app.moonshop.adapter.BeautyAdapter;
import grand.app.moonshop.base.BaseFragment;
import grand.app.moonshop.customviews.dialog.DialogConfirm;
import grand.app.moonshop.databinding.FragmentBeautyServiceBinding;
import grand.app.moonshop.models.app.Mutable;
import grand.app.moonshop.utils.helper.AppUtils;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.utils.dialog.DialogHelperInterface;
import grand.app.moonshop.utils.resources.ResourceManager;
import grand.app.moonshop.viewmodels.reservation.beauty.BeautyServiceViewModel;
import grand.app.moonshop.views.activities.BaseActivity;
import timber.log.Timber;

public class BeautyServicesFragment extends BaseFragment {
    private View rootView;
    private FragmentBeautyServiceBinding fragmentBeautyServiceBinding;
    private BeautyServiceViewModel beautyServiceViewModel;
    private BeautyAdapter beautyAdapter;


    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        fragmentBeautyServiceBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_beauty_service, container, false);
        bind();
        setEvent();
        return rootView;
    }


    private void bind() {
        beautyServiceViewModel = new BeautyServiceViewModel();
        AppUtils.initVerticalRV(fragmentBeautyServiceBinding.rvReservation, fragmentBeautyServiceBinding.rvReservation.getContext(), 1);
        fragmentBeautyServiceBinding.setBeautyServiceViewModel(beautyServiceViewModel);
        rootView = fragmentBeautyServiceBinding.getRoot();
    }

    private void setEvent() {
        beautyServiceViewModel.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                String action = (String) o;
                handleActions(action, beautyServiceViewModel.getReservationRepository().getMessage());
                assert action != null;
                if (action.equals(Constants.SUCCESS)) {
                    beautyAdapter = new BeautyAdapter(beautyServiceViewModel.getReservationRepository().getBeautyServiceResponse().services);
                    fragmentBeautyServiceBinding.rvReservation.setAdapter(beautyAdapter);
                    setEventAdapter();
                } else if (action.equals(Constants.ADD)) {
                    Intent intent = new Intent(context, BaseActivity.class);
                    intent.putExtra(Constants.PAGE, Constants.ADD_BEAUTY_SERVICE);
                    intent.putExtra(Constants.NAME_BAR, ResourceManager.getString(R.string.add_service));
                    startActivityForResult(intent, Constants.RELOAD_RESULT);
                } else if (action.equals(Constants.DELETED)) {
                    toastMessage(beautyServiceViewModel.getReservationRepository().getMessage(), R.drawable.ic_check_white, R.color.colorPrimary);
                    beautyAdapter.remove(beautyServiceViewModel.category_delete_position);
                }
            }
        });
    }


    private void setEventAdapter() {
        beautyAdapter.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                Mutable mutable = (Mutable) o;
                if (mutable.type.equals(Constants.DELETE)) {

                    new DialogConfirm(getActivity())
                            .setTitle(ResourceManager.getString(R.string.remove))
                            .setMessage(ResourceManager.getString(R.string.do_you_want_delete_item))
                            .setActionText(ResourceManager.getString(R.string.remove_item))
                            .setActionCancel(ResourceManager.getString(R.string.cancel))
                            .show(new DialogHelperInterface() {
                                @Override
                                public void OnClickListenerContinue(Dialog dialog, View view) {
                                    beautyServiceViewModel.category_delete_position = mutable.position;
                                    beautyServiceViewModel.getReservationRepository().deleteBeauty(beautyServiceViewModel.getReservationRepository().getBeautyServiceResponse().services.get(mutable.position).id);
                                }
                            });
                }
            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        //super method removed
        Timber.e("onActivityResult:" + requestCode);
        if (resultCode == Activity.RESULT_OK && requestCode == Constants.RELOAD_RESULT) {
            beautyServiceViewModel.getReservationRepository().beautyServices();
        }
        super.onActivityResult(requestCode, resultCode, data);
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        beautyServiceViewModel.reset();

    }


}
