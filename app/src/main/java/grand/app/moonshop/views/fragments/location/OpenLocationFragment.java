package grand.app.moonshop.views.fragments.location;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.model.LatLng;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moonshop.R;
import grand.app.moonshop.base.BaseFragment;
import grand.app.moonshop.databinding.FragmentOpenLocationBinding;
import grand.app.moonshop.map.GPSAllowListener;
import grand.app.moonshop.map.GPSLocation;
import grand.app.moonshop.map.LocationLatLng;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.utils.maputils.location.LocationChangeListener;
import grand.app.moonshop.utils.storage.user.UserHelper;
import grand.app.moonshop.viewmodels.location.OpenLocationViewModel;
import timber.log.Timber;


public class OpenLocationFragment extends BaseFragment {

    private FragmentOpenLocationBinding fragmentOpenLocationBinding;
    private OpenLocationViewModel openLocationViewModel;
    LocationLatLng locationLatLng;
    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentOpenLocationBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_open_location, container, false);
        bind();
        return fragmentOpenLocationBinding.getRoot();
    }



    private void bind() {
        openLocationViewModel = new OpenLocationViewModel();
        setEvents();
        fragmentOpenLocationBinding.setOpenLocationViewModel(openLocationViewModel);
    }

    private void setEvents() {
        openLocationViewModel.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                String action = (String) o;
                assert action != null;
                if (action.equals(Constants.LOCATION_ENABLE)) {
                    enableLocation();
//                    ((ParentActivity) context).finishAffinity();
//                    startActivity(new Intent(context, MainActivity.class));
                }
            }
        });

    }

    public void enableLocation(){
        GPSLocation.EnableGPSAutoMatically(context, new GPSAllowListener() {
            @Override
            public void GPSStatus(boolean isOpen) {
//                ((ParentActivity) context).finishAffinity();
//                startActivity(new Intent(context, MainActivity.class));

                if (isOpen) {
                    Timber.e("start here");
                    locationLatLng = new LocationLatLng(getActivityBase(),OpenLocationFragment.this);
                    locationLatLng.getLocation(new LocationChangeListener() {
                        @Override
                        public void select(String address, LatLng latLng) {
                            Timber.e("start here fetch done");
                            UserHelper.saveKey(Constants.LAT,""+latLng.latitude);
                            UserHelper.saveKey(Constants.LNG,""+latLng.longitude);
                            Timber.e("lat:"+latLng.latitude);
                            Timber.e("lng:"+latLng.longitude);
                            UserHelper.saveKey(Constants.USER_ADDRESS,address);
                        }

                        @Override
                        public void error() {

                        }
                    });
                }else{
                    Timber.e("location GPS disable");
                }
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        fragmentOpenLocationBinding = null;
        locationLatLng = null;
        openLocationViewModel.reset();
    }


}
