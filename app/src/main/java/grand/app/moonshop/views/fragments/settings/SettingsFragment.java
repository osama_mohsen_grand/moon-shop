package grand.app.moonshop.views.fragments.settings;


import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moonshop.R;
import grand.app.moonshop.base.BaseFragment;
import grand.app.moonshop.databinding.FragmentSettingsBinding;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.viewmodels.app.SettingsViewModel;

/**
 * A simple {@link Fragment} subclass.
 */
public class SettingsFragment extends BaseFragment {

    private FragmentSettingsBinding fragmentSettingsBinding;
    private SettingsViewModel viewModel;
    private int type = -1;


    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        fragmentSettingsBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_settings, container, false);
        getData();
        bind();
        setEvent();
        return fragmentSettingsBinding.getRoot();
    }

    private void getData() {
        if(getArguments() != null && getArguments().containsKey(Constants.TYPE)){
            type = getArguments().getInt(Constants.TYPE);
        }
    }

    private void bind() {
        viewModel = new SettingsViewModel(type);
        fragmentSettingsBinding.setViewModel(viewModel);
    }

    private void setEvent() {
        viewModel.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                String action = (String) o;
                handleActions(action, viewModel.getSettingsRepository().getMessage());
                assert action != null;
                if(action.equals(Constants.SUCCESS)){ // success add image
                    viewModel.adapter.update(viewModel.getSettingsRepository().settingsResponse.data);
                }
            }
        });
    }


}
