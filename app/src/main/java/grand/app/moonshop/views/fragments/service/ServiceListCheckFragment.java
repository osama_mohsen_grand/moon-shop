package grand.app.moonshop.views.fragments.service;


import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moonshop.R;
import grand.app.moonshop.adapter.ServiceAdapter;
import grand.app.moonshop.base.BaseFragment;
import grand.app.moonshop.databinding.FragmentServiceListCheckBinding;
import grand.app.moonshop.models.service.Service;
import grand.app.moonshop.models.service.ServiceResponse;
import grand.app.moonshop.utils.helper.AppUtils;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.viewmodels.service.ServiceViewModel;

/**
 * A simple {@link Fragment} subclass.
 */
public class ServiceListCheckFragment extends BaseFragment {

    private FragmentServiceListCheckBinding fragmentServiceListCheckBinding;
    private ServiceViewModel serviceViewModel;
    private ServiceAdapter serviceAdapter;
    ServiceResponse serviceResponse;
    private ArrayList<Integer> selected = new ArrayList<>();

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        fragmentServiceListCheckBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_service_list_check, container, false);
        getData();
        bind();
        setEvent();
        return fragmentServiceListCheckBinding.getRoot();
    }


    private void getData() {
        AppUtils.initVerticalRV(fragmentServiceListCheckBinding.rvServices, fragmentServiceListCheckBinding.rvServices.getContext(), 1);
        if (getArguments() != null && getArguments().containsKey(Constants.SERVICE)) {
            this.serviceResponse = (ServiceResponse) getArguments().getSerializable(Constants.SERVICE);
        }
        if (getArguments() != null && getArguments().containsKey(Constants.SELECTED)) {
            this.selected = getArguments().getIntegerArrayList(Constants.SELECTED);
        }

        if (serviceResponse != null) {
            serviceAdapter = new ServiceAdapter(serviceResponse.services, selected);
            fragmentServiceListCheckBinding.rvServices.setAdapter(serviceAdapter);
            setEventAdapter();
        }

    }


    private void bind() {
        serviceViewModel = new ServiceViewModel();
        fragmentServiceListCheckBinding.setServiceViewModel(serviceViewModel);
    }


    private void setEventAdapter() {
        serviceAdapter.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                int pos = (int) o;
                Service service = serviceResponse.services.get(pos);
                if (selected.contains(service.id)) {
                    selected.remove(selected.indexOf(service.id));
                } else
                    selected.add(service.id);
//                Timber.e("selected:"+selected.toString());
                serviceAdapter.update(selected);
            }
        });
    }


    private void setEvent() {
        serviceViewModel.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                String action = (String) o;
                if(action.equals(Constants.SUBMIT)){
                    Intent intent=new Intent();
                    intent.putExtra(Constants.SELECTED,selected);
                    getActivityBase().setResult(Constants.SERVICES_RESULT,intent);
                    getActivityBase().finish();//finishing activity
                }
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        serviceViewModel.reset();
    }
}
