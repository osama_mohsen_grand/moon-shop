package grand.app.moonshop.views.fragments.common;


import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moonshop.R;
import grand.app.moonshop.base.BaseFragment;
import grand.app.moonshop.databinding.FragmentCreditBinding;
import grand.app.moonshop.repository.CreditRepository;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.viewmodels.common.CreditViewModel;
import grand.app.moonshop.views.activities.BaseActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class CreditFragment extends BaseFragment {

    private CreditViewModel creditViewModel;
    private FragmentCreditBinding fragmentCreditBinding;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        fragmentCreditBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_credit, container, false);
        Bind();
        setEvent();
        return fragmentCreditBinding.getRoot();

    }

    private void Bind() {
        creditViewModel = new CreditViewModel();
        fragmentCreditBinding.setCreditViewModel(creditViewModel);
        fragmentCreditBinding.tvCreditDays.setPaintFlags(fragmentCreditBinding.tvCreditDays.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        fragmentCreditBinding.tvCreditDays.setOnClickListener(v ->{
            Intent intent = new Intent(context, BaseActivity.class);
            intent.putExtra(Constants.PAGE,Constants.PACKAGES);
            getActivityBase().finishAffinity();
            startActivity(intent);
        });
    }

    private void setEvent() {

        creditViewModel.mMutableLiveData.observe((LifecycleOwner) context, o -> {
            String action = (String) o;
            handleActions(action, creditViewModel.getCreditRepository().getMessage());
            assert action != null;
            if(action.equals(Constants.SUCCESS)){
                creditViewModel.showPage(true);
                creditViewModel.setCreditResponse(creditViewModel.getCreditRepository().getCreditResponse());
                Log.e("RSPNS",creditViewModel.getCreditRepository().getCreditResponse().data.credit);
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        creditViewModel.reset();

    }


}
