package grand.app.moonshop.views.fragments.notification;


import android.app.Dialog;
import android.content.Context;
import android.graphics.Rect;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moonshop.R;
import grand.app.moonshop.adapter.NotificationAdapter;
import grand.app.moonshop.base.BaseFragment;
import grand.app.moonshop.databinding.DialogFamousOfferBinding;
import grand.app.moonshop.databinding.FragmentNotificationsBinding;
import grand.app.moonshop.models.notifications.Notification;
import grand.app.moonshop.utils.helper.AppUtils;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.viewmodels.notification.NotificationViewModel;
import grand.app.moonshop.viewmodels.offer.DialogFamousOfferViewModel;

/**
 * A simple {@link Fragment} subclass.
 */
public class NotificationFragment extends BaseFragment {

    private FragmentNotificationsBinding fragmentNotificationsBinding;
    private NotificationViewModel notificationViewModel;
    private NotificationAdapter notificationAdapter;


    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        fragmentNotificationsBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_notifications, container, false);
        bind();
        setEvent();
        return fragmentNotificationsBinding.getRoot();
    }

    private void bind() {
        notificationViewModel = new NotificationViewModel();
        AppUtils.initVerticalRV(fragmentNotificationsBinding.rvNotifications, fragmentNotificationsBinding.rvNotifications.getContext(), 1);
        fragmentNotificationsBinding.setNotificationViewModel(notificationViewModel);
    }

    private void setEvent() {
        notificationViewModel.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                String action = (String) o;
                handleActions(action, notificationViewModel.getNotificationRepository().getMessage());
                assert action != null;
                if (action.equals(Constants.SUCCESS)) {
                    if (notificationViewModel.getNotificationRepository().getNotificationResponse().getData().size() == 0) {
                        notificationViewModel.noData();
                    } else {
                        notificationAdapter = new NotificationAdapter(notificationViewModel.getNotificationRepository().getNotificationResponse().getData());
                        fragmentNotificationsBinding.rvNotifications.setAdapter(notificationAdapter);
                        setEventAdapter();
                    }
                }else if(action.equals(Constants.ADS_CONFIRM)){
                    if(showDialog != null && showDialog.isShowing()) showDialog.dismiss();
                    notificationAdapter.updateSelect();
                }
            }
        });
    }

    private static final String TAG = "NotificationFragment";

    private void setEventAdapter() {
        notificationAdapter.mMutableLiveData.observe(this, new Observer<Object>() {
            @Override
            public void onChanged(Object o) {
                int pos = (int) o;
                Notification model = notificationViewModel.getNotificationRepository().getNotificationResponse().getData().get(pos);
                if (model.status == 1) {

                    showDialog(model);
                }
            }
        });
    }


    Dialog showDialog;

    public void showDialog(Notification model) {
        Rect displayRectangle = new Rect();
        Window window = getActivityBase().getWindow();
        window.getDecorView().getWindowVisibleDisplayFrame(displayRectangle);
        DialogFamousOfferBinding binding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.dialog_famous_offer, null, false);
        DialogFamousOfferViewModel viewModel = new DialogFamousOfferViewModel(model,notificationViewModel.mMutableLiveData);
        binding.setViewmodel(viewModel);
        binding.getRoot().setMinimumWidth((int)(displayRectangle.width() * 0.9f));
        View view =binding.getRoot();
        AlertDialog.Builder builder = new AlertDialog.Builder(context, R.style.customDialog);
        builder.setView(view);
        showDialog = builder.create();
        showDialog.show();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        notificationViewModel.reset();
    }
}
