
package grand.app.moonshop.base;

import android.content.Context;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import java.util.List;

import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;
import androidx.databinding.ObservableInt;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.MutableLiveData;
import grand.app.moonshop.R;
import grand.app.moonshop.utils.resources.ResourceManager;
import grand.app.moonshop.vollyutils.MyApplication;
import io.reactivex.disposables.CompositeDisposable;
import libs.mjn.scaletouchlistener.ScaleTouchListener;


/**
 * Created by Gregory Rasmussen on 7/26/17.
 */
public abstract class ParentViewModel extends ParentBaseObservableViewModel {
    public MutableLiveData<Object> mMutableLiveData  = new MutableLiveData<>();;
    protected CompositeDisposable compositeDisposable = new CompositeDisposable();
    public ObservableField<String> noDataText = new ObservableField(ResourceManager.getString(R.string.there_are_no)+" "+ResourceManager.getString(R.string.data));
    public ObservableBoolean noDataTextDisplay = new ObservableBoolean(false);
    public ObservableBoolean show = new ObservableBoolean(false);


    public static ObservableInt progress;
    public String baseError = "";

    Context context = null;
    Animation hyperspaceJumpAnimation;

    public ParentViewModel(Context context) {
        this.context = context;

        hyperspaceJumpAnimation = AnimationUtils.loadAnimation(MyApplication.getInstance(), R.anim.bonuce);


        progress = new ObservableInt(View.GONE);
    }

    public ParentViewModel(){

    }


    public void noData(){
        noDataTextDisplay.set(true);
        notifyChange();
    }

    public void noDataDisplay(List<?> list){
        if(list.size() > 0) {
            noDataTextDisplay.set(false);
        }else{
            noDataTextDisplay.set(true);
        }
        notifyChange();
    }

    public void isEmptyList(List<?> results){
        if(results.size() > 0){

        }
    }

    public void showPage(boolean show){
        this.show.set(show);
        notifyChange();
    }


    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
    }

    protected void unSubscribeFromObservable() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed()) {
            compositeDisposable.dispose();
        }
    }
}
