package grand.app.moonshop.base;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.daimajia.numberprogressbar.NumberProgressBar;
import com.facebook.FacebookSdk;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.snackbar.BaseTransientBottomBar;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import es.dmoral.toasty.Toasty;
import grand.app.moonshop.R;
import grand.app.moonshop.repository.FirebaseRepository;
import grand.app.moonshop.utils.helper.AppUtils;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.utils.LanguagesHelper;
import grand.app.moonshop.utils.MyContextWrapper;
import grand.app.moonshop.utils.resources.ResourceManager;
import grand.app.moonshop.utils.storage.user.UserHelper;
import grand.app.moonshop.views.activities.BaseActivity;
import grand.app.moonshop.vollyutils.MyApplication;
import timber.log.Timber;

import static grand.app.moonshop.utils.Constants.FILE_TYPE_IMAGE;
import static grand.app.moonshop.utils.Constants.RESULT_PROFILE_RESPONSE;

//import com.facebook.FacebookSdk;
//import com.facebook.appevents.AppEventsLogger;


public class ParentActivity extends AppCompatActivity implements
        GoogleApiClient.OnConnectionFailedListener,
        GoogleApiClient.ConnectionCallbacks, LocationListener {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initializeLanguage();
        initializeProgress();
        initializeProgressDialogPercentage();
    }


    protected void initFacebook() {
        if (!FacebookSdk.isInitialized()) {
            FacebookSdk.setApplicationId("623615508394466");
            FacebookSdk.sdkInitialize(getApplicationContext());
        }
    }


    protected void initializeLanguage() {
        LanguagesHelper.getCurrentLanguage();
    }


    public void initializeToken() {
        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(ParentActivity.this,
                new OnSuccessListener<InstanceIdResult>() {
                    @Override
                    public void onSuccess(InstanceIdResult instanceIdResult) {
                        String newToken = instanceIdResult.getToken();
                        UserHelper.saveKey(Constants.TOKEN, newToken);
                        if (UserHelper.getUserId() != -1)
                            new FirebaseRepository(null).updateToken(newToken);
                    }
                });
    }


    BroadcastReceiver canceled = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent broadIntentExtra) {
            finishAffinity();
            startActivity(new Intent(ParentActivity.this, BaseActivity.class));
        }
    };

    @Override
    public void onResume() {
        super.onResume();
        registerReceiver(canceled, new IntentFilter(Constants.CANCELED));
        LanguagesHelper.changeLanguage(MyApplication.getInstance(), LanguagesHelper.getCurrentLanguage());
        LanguagesHelper.changeLanguage(this, LanguagesHelper.getCurrentLanguage());
    }

    @Override
    public void onPause() {
        super.onPause();
        unregisterReceiver(canceled);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.N_MR1) {
            super.attachBaseContext(MyContextWrapper.wrap(newBase, LanguagesHelper.getCurrentLanguage()));
        } else {
            super.attachBaseContext(newBase);
        }
    }


    protected Dialog dialogLoader, dialogProgressLoaderPercentage;


    public void initializeProgress() {
        View view = LayoutInflater.from(this).inflate(R.layout.loader_animation, null);
        final AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.customDialog);
        builder.setView(view);
        dialogLoader = builder.create();
    }

    public void initializeProgressDialogPercentage() {
        View view = LayoutInflater.from(this).inflate(R.layout.loader_progress_percentage, null);
        final AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.customDialog);
        builder.setView(view);
        builder.setCancelable(false);
        dialogProgressLoaderPercentage = builder.create();
    }


    public void showProgressDialogPercentage() {
        //show dialog
        if (dialogProgressLoaderPercentage != null && !this.isFinishing()) {
            Log.e("dialog", "HERE2");
            dialogProgressLoaderPercentage.show();
        }

    }

    public void hideProgressDialogPercentage() {
        if (dialogProgressLoaderPercentage != null && dialogProgressLoaderPercentage.isShowing() && !this.isFinishing())
            dialogProgressLoaderPercentage.dismiss();
    }

    public void showProgress() {
        //show dialog
        if (dialogLoader != null && !this.isFinishing()) {
            Log.e("dialog", "HERE2");
            dialogLoader.show();
        }

    }

    public void hideProgress() {
        hideProgressDialogPercentage();
        if (dialogLoader != null && dialogLoader.isShowing() && !this.isFinishing())
            dialogLoader.dismiss();
    }


    public void handleActions(String action, String baseError) {
        Timber.e("action:" + action);
        if (action.equals(Constants.SHOW_PROGRESS)) showProgress();
        else if (action.equals(Constants.HIDE_PROGRESS)) hideProgress();
        else if (action.equals(Constants.SHOW_PROGRESS_PERCENTAGE)) showProgressDialogPercentage();
        else if (action.equals(Constants.HIDE_PROGRESS_PERCENTAGE)) hideProgressDialogPercentage();
        else if (action.equals(Constants.ERROR_RESPONSE) && !baseError.equals(""))
            showError(baseError);
        else if (action.equals(Constants.SERVER_ERROR)) {
            hideProgress();
            showError(ResourceManager.getString(R.string.msg_server_error));
        } else if (action.equals(Constants.FAILURE_CONNECTION)) {
            hideProgress();
            noConnection();
        } else if (action.equals(Constants.LOGOUT)) {
            UserHelper.clearUserDetails();
            finishAffinity();
            Intent intent = new Intent(this, BaseActivity.class);
            intent.putExtra(Constants.PAGE, Constants.LOGIN);
            startActivity(intent);
        }
    }

    public void handleActions(String key, Object value) {
        if (key.equals(Constants.PROGRESS_PERCENTAGE)) {
            int percent = (int) value;
            setProgressPercentage(percent);
        }
    }

    private static final String TAG = "ParentActivity";

    private void setProgressPercentage(int percentage) {
        Log.d(TAG, "setProgressPercentage: " + percentage);
        if (dialogProgressLoaderPercentage != null && dialogProgressLoaderPercentage.isShowing()) {
            Log.d(TAG, "setProgressPercentage: Done");
            NumberProgressBar numberProgressBar = dialogProgressLoaderPercentage.findViewById(R.id.number_progress_bar);
            numberProgressBar.setProgress(percentage);
        }
    }

    public void noConnection() {
        Log.d(TAG, "no connection here");
//        Snackbar snackBar = Snackbar.make(findViewById(R.id.ll_base_container),
//                getString(R.string.please_check_connection), Snackbar.LENGTH_LONG);
//        View view = snackBar.getView();
//        view.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary));
//        TextView textView = view.findViewById(R.id.snackbar_text);
//        textView.setGravity(Gravity.CENTER_VERTICAL);
//        textView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_warning, 0, 0, 0);
//        snackBar.show();

        Snackbar snackbar = Snackbar
                .make(findViewById(R.id.ll_base_container), getString(R.string.please_check_connection), Snackbar.LENGTH_INDEFINITE)
                .setAction(getString(R.string.retry), new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                    }
                });

        snackbar.addCallback(new BaseTransientBottomBar.BaseCallback<Snackbar>() {
            @Override
            public void onShown(Snackbar transientBottomBar) {
                super.onShown(transientBottomBar);
                transientBottomBar.getView().findViewById(R.id.snackbar_action)
                        .setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                if (AppUtils.isNetworkAvailable()) {
                                    snackbar.dismiss();
                                    reloadCurrentPage();
//                                    int size = getSupportFragmentManager().getFragments().size();
//                                    String tag = getSupportFragmentManager().getFragments().get(size - 1).getTag();
//                                    Fragment fragment = getSupportFragmentManager().findFragmentByTag(tag);
//                                    if (fragment != null && fragment.isVisible()) {
//
//                                    }
                                }
                            }
                        });
            }
        });
        snackbar.setActionTextColor(Color.WHITE);
        View view = snackbar.getView();
        TextView textView = (TextView) view.findViewById(R.id.snackbar_text);
        view.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary));
        textView.setTextColor(Color.WHITE);
        snackbar.show();


    }

    public void reloadCurrentPage() {
        Log.d(TAG, "reloadPageStart");
        if (getSupportFragmentManager() != null && getSupportFragmentManager().getFragments() != null) {
            Log.d(TAG, "getSupportFragmentManager not null");
            int size = getSupportFragmentManager().getFragments().size();
            Log.d(TAG, "size:" + size);
            String tag = getSupportFragmentManager().getFragments().get(size - 1).getTag();
            Log.d(TAG, "tag:" + tag);
            Fragment fragment = getSupportFragmentManager().findFragmentByTag(tag);
            if (fragment != null) {
                Log.d(TAG, "getSupportFragmentManager fragment");
                final FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                ft.detach(fragment);
                ft.attach(fragment);
                ft.commit();
                return;
            }
        }
        finishAffinity();
        startActivity(new Intent(this, BaseActivity.class));
    }


    public void showError(String msg) {
        Timber.e(msg);
        if (msg.equals(Constants.ERROR_LOGIN_RESPONSE))
            msg = getString(R.string.please_login_first);
        Snackbar snackBar = Snackbar.make(findViewById(R.id.ll_base_container),
                msg, Snackbar.LENGTH_LONG);
        View view = snackBar.getView();
        view.setBackgroundColor(ContextCompat.getColor(this, R.color.colorBlack));
        TextView textView = view.findViewById(R.id.snackbar_text);
        textView.setGravity(Gravity.CENTER_VERTICAL);
        textView.setTextColor(ContextCompat.getColor(this, R.color.colorWhite));
        textView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_warning, 0, 0, 0);
        snackBar.show();
    }

    public void toastMessage(String message, int icon, int color) {
        Toasty.custom(this, message, icon, color, Toasty.LENGTH_SHORT, true, true).show();
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d("PrAc_requestCode", requestCode + "");
        Log.d("PrAc_resultCode", resultCode + "");

        try {
            if (resultCode == RESULT_OK && requestCode != RESULT_PROFILE_RESPONSE && data.getData() != null) {
                Log.d(TAG, "size:" + getSupportFragmentManager().getFragments().size() + "");
                if (getSupportFragmentManager().getFragments().size() > 0) {
                    int size = getSupportFragmentManager().getFragments().size();
                    Log.d(TAG, "size_again:" + size);
                    while (size > 0) {
                        Log.d(TAG, "size_again increaser ");
                        String tag = getSupportFragmentManager().getFragments().get(size - 1).getTag();
                        Log.d("tag_while:", tag);
                        Fragment fragment = getSupportFragmentManager().findFragmentByTag(tag);
                        if (fragment != null && fragment.isVisible()) {
                            Log.d(TAG, "tag:" + tag);
                            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.R && requestCode == FILE_TYPE_IMAGE) {
                                Uri uri = CropImage.getPickImageResultUri(fragment.getContext(),data);
                                CropImage.activity().setOutputUri(uri)
                                        .setCropShape(CropImageView.CropShape.OVAL)
                                        .setFixAspectRatio(true)
                                        .start(fragment.requireActivity());
                                fragment.onActivityResult(requestCode, resultCode, data);
                            }else
                                fragment.onActivityResult(requestCode, resultCode, data);
                            return;
                        }
                        size--;
                    }
                }
            }
        } catch (Exception ex) {
            Log.e("exceptionBase", ex.getMessage());
        }
    }


    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }


    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }
}
