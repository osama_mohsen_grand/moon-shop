package grand.app.moonshop.base;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.transition.DrawableCrossFadeFactory;
import com.daimajia.slider.library.Indicators.PagerIndicator;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.DefaultSliderView;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.ProgressiveMediaSource;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory;

import androidx.databinding.BindingAdapter;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;
import grand.app.moonshop.R;
import grand.app.moonshop.customviews.views.CustomWebView;
import grand.app.moonshop.customviews.views.HtmlAudioHelper;
import grand.app.moonshop.customviews.views.WebInterface;
import grand.app.moonshop.utils.helper.AppUtils;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.utils.images.zoom.ImageMatrixTouchHandler;
import grand.app.moonshop.views.activities.BaseActivity;
import grand.app.moonshop.vollyutils.AppHelper;
import grand.app.moonshop.vollyutils.MyApplication;
import libs.mjn.scaletouchlistener.ScaleTouchListener;
import timber.log.Timber;

import static com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade;

public class ApplicationBinding {


    @BindingAdapter("animation_submit")
    public static void animation(View view, IAnimationSubmit animationSubmit) {
        view.setOnTouchListener(new ScaleTouchListener() {
            @Override
            public void onClick(View v) {
                animationSubmit.animationSubmit();
            }
        });
    }

    @BindingAdapter("imageWH3")
    public static void imageWidth(View view, Integer widthW) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((FragmentActivity)view.getContext()).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        int widthInt = displayMetrics.widthPixels;
        view.getLayoutParams().width = widthInt/3;
        view.getLayoutParams().height = widthInt/3;
    }

    @BindingAdapter("width")
    public static void setWidth(LinearLayout linearLayout, int span) {
        if(span > 0) {
            int width = AppHelper.getScreenWidth(linearLayout.getContext()) / span;
            linearLayout.getLayoutParams().width = width;
        }
    }

    @BindingAdapter("txtColor")
    public static void setColor(TextView textView , int color){
        textView.setTextColor(color);
    }


    @BindingAdapter("width")
    public static void setWidth(RelativeLayout relativeLayout, int span) {
        if(span > 0) {
            int width = AppHelper.getScreenWidth(relativeLayout.getContext()) / span;
            relativeLayout.getLayoutParams().width = width;
        }
    }

    @BindingAdapter({"app:adapter", "app:span", "app:orientation"})
    public static void getItemsV2Binding(RecyclerView recyclerView, RecyclerView.Adapter<?> itemsAdapter, String spanCount, String orientation) {
        if (orientation.equals("1"))
            AppUtils.initVerticalRV(recyclerView, recyclerView.getContext(), Integer.parseInt(spanCount));
        else
            AppUtils.initHorizontalRV(recyclerView, recyclerView.getContext(), Integer.parseInt(spanCount));
        recyclerView.setAdapter(itemsAdapter);
    }



    @BindingAdapter("imageUrl")
    public static void loadImage(ImageView imageView, String image) {
        if(image != null) {
            if (!image.equals("")) {
//                ImageLoaderHelper.ImageLoaderLoad(imageView.getContext(), image, imageView);
                Glide
                        .with(imageView.getContext())
                        .load(image)
                        .centerCrop()
//                .placeholder(R.drawable.ic_video_default)
                        .into(imageView);
            } else {
                imageView.setImageResource(R.drawable.ic_logo_original);
            }
        }
    }

    @BindingAdapter("imageZoomUrl")
    public static void loadZoomImage(ImageView imageView, String image) {
        if(image != null && !image.equals("")) {
//            ImageLoaderHelper.ImageLoaderLoad(imageView.getContext(), image, imageView);
            Glide
                    .with(imageView.getContext())
                    .load(image)
                    .fitCenter()
//                .placeholder(R.drawable.progress_animation)
                    .into(imageView);

            imageView.setOnTouchListener(new ImageMatrixTouchHandler(MyApplication.getInstance()));
        }
    }


    @BindingAdapter("audio")
    public static void loadAudio(PlayerView playerView, String audio) {
        SimpleExoPlayer player = ExoPlayerFactory.newSimpleInstance(playerView.getContext());
        MediaSource mediaSource = new ProgressiveMediaSource.Factory(new DefaultHttpDataSourceFactory("exoplayer-codelab"))
                .createMediaSource(Uri.parse(audio));
        player.setPlayWhenReady(false);
        player.prepare(mediaSource, true, false);
        playerView.setPlayer(player);
    }

    @BindingAdapter("audioHtml")
    public static void media(final CustomWebView webView,String url) {
        if(url != null) {
            webView.getSettings().setMediaPlaybackRequiresUserGesture(false);
            webView.addJavascriptInterface(new WebInterface(webView.getContext()), "Android");
            String html = new HtmlAudioHelper().html(url);
            webView.loadData(html, "text/html", "UTF-8");
//            setExoPlayerView(simpleExoPlayerView,video);
        }

    }




    @BindingAdapter("color")
    public static void color(ImageView imageView, String color) {
        Timber.e("color:"+color);
        if(color != null && !color.equals("") && color.charAt(0) == '#')
            imageView.setBackgroundColor(Color.parseColor(color));
    }


    @BindingAdapter("rate")
    public static void setRate(final RatingBar ratingBar, float rate) {
        ratingBar.setRating(rate);
    }

    @BindingAdapter("images")
    public static void setImages(final SliderLayout sliderLayout, String[] images) {
        Log.e("slider","start");
        if(images != null) {
            sliderLayout.removeAllSliders();
            Log.e("setImageSlider", "" + images.length);
            try {
                for (int i = 0; i < images.length; i++) {
                    final String image = images[i];
                    try {

                        DefaultSliderView defaultSliderView;
                        defaultSliderView = new DefaultSliderView(sliderLayout.getContext());
                        defaultSliderView
                                .image(image)
                                .setScaleType(BaseSliderView.ScaleType.Fit);
                        defaultSliderView.bundle(new Bundle());
                        defaultSliderView.setOnSliderClickListener(new BaseSliderView.OnSliderClickListener() {
                            @Override
                            public void onSliderClick(BaseSliderView slider) {
                                Intent intent = new Intent(sliderLayout.getContext(), BaseActivity.class);
                                intent.putExtra(Constants.PAGE, Constants.ZOOM);
                                intent.putExtra(Constants.IMAGE, image);
                                sliderLayout.getContext().startActivity(intent);

                            }
                        });
                        sliderLayout.addSlider(defaultSliderView);
                        sliderLayout.setIndicatorVisibility(PagerIndicator.IndicatorVisibility.Invisible);
                    } catch (Exception e) {
                        Log.e("setImageSliderException", "" + e.getMessage());
                        //  Toast.makeText(getApplicationContext(),"",Toast.LENGTH_LONG).show();
                    }

                }

                sliderLayout.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);

                sliderLayout.getPagerIndicator().setDefaultIndicatorColor(sliderLayout.getContext().getResources().getColor(R.color.colorAccent), sliderLayout.getContext().getResources().getColor(R.color.colorSilver));
                sliderLayout.setIndicatorVisibility(PagerIndicator.IndicatorVisibility.Visible);
            } catch (Exception e) {
                Log.e("Exception", "" + e.getMessage());
                e.getStackTrace();
            }
        }
    }



    public static DrawableCrossFadeFactory factory =
            new DrawableCrossFadeFactory.Builder().setCrossFadeEnabled(true).build();

//    public static void loadImage(ImageView imageView,String imageUrl){
//        Glide.with(imageView.getContext())
//                .load(imageUrl)
//                .transition(withCrossFade(factory))
//                .diskCacheStrategy(DiskCacheStrategy.ALL)
//                .placeholder(R.mipmap.ic_launcher)
//                .into(imageView);
//    }

}
