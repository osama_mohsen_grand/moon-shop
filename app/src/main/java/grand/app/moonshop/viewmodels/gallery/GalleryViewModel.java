
package grand.app.moonshop.viewmodels.gallery;

import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;
import grand.app.moonshop.R;
import grand.app.moonshop.base.ParentViewModel;
import grand.app.moonshop.repository.SearchRepository;
import grand.app.moonshop.utils.resources.ResourceManager;


/**
 * Created by Gregory Rasmussen on 7/26/17.
 */
public class GalleryViewModel extends ParentViewModel {

    public String url = "";



    public GalleryViewModel(String url) {
        this.url = url;
    }

    public void add(){

    }

    public void filter(){

    }

    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
    }

}
