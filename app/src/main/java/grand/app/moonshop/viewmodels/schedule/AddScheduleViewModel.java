
package grand.app.moonshop.viewmodels.schedule;

import android.view.View;
import android.widget.EditText;

import java.util.ArrayList;
import java.util.List;

import grand.app.moonshop.R;
import grand.app.moonshop.base.ParentViewModel;
import grand.app.moonshop.models.ads.AdsDetailsModel;
import grand.app.moonshop.models.base.IdNameImage;
import grand.app.moonshop.models.reservation.doctor.schedule.AddScheduleRequest;
import grand.app.moonshop.repository.AdsRepository;
import grand.app.moonshop.repository.ReservationRepository;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.utils.resources.ResourceManager;
import grand.app.moonshop.utils.storage.user.UserHelper;
import grand.app.moonshop.viewmodels.ads.AddAdsCompanyRequest;
import grand.app.moonshop.vollyutils.VolleyFileObject;
import timber.log.Timber;

/**
 * Created by Gregory Rasmussen on 7/26/17.
 */
public class AddScheduleViewModel extends ParentViewModel {
    public int category_delete_position = -1;
    public int position = -1;
    public int new_image_position = -1;
    public EditText editText;
    private ReservationRepository reservationRepository;
    public AddScheduleRequest addScheduleRequest;
    public boolean isEdit = false;
    public List<VolleyFileObject> volleyFileObjects = new ArrayList<>();
    public AddScheduleViewModel(int doctor_id, int day_id,int status,int _switch) {
        reservationRepository = new ReservationRepository(mMutableLiveData);
        addScheduleRequest = new AddScheduleRequest(doctor_id,day_id,status,_switch);
    }

    public void selectTime(View editText){
        this.editText = (EditText) editText;
        mMutableLiveData.setValue(Constants.TIME_DIALOG);
    }

    public void submit() {
        if(addScheduleRequest.isValid()){
            mMutableLiveData.setValue(Constants.SCHEDULE);
        }
    }

    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
    }
}
