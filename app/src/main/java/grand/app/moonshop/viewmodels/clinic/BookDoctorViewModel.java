package grand.app.moonshop.viewmodels.clinic;

import android.util.Log;
import android.widget.RadioGroup;

import grand.app.moonshop.R;
import grand.app.moonshop.base.ParentViewModel;
import grand.app.moonshop.models.reservation.ReservationRequest;
import grand.app.moonshop.repository.ClinicRepository;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.utils.resources.ResourceManager;
import grand.app.moonshop.utils.storage.user.UserHelper;

/**
 * Created by Gregory Rasmussen on 7/26/17.
 */
public class BookDoctorViewModel extends ParentViewModel {

    public int doctor_id;
    public String text = "";
    ClinicRepository clinicRepository;
    public ReservationRequest reservationRequest;
    private String price="";
    public boolean allowCash = false, allowOnline = false, checkCash = false, checkOnline = false;

    public BookDoctorViewModel(int id,int doctor_id) {
        reservationRequest = new ReservationRequest(Constants.TYPE_RESERVATION_CLINIC,id);
        clinicRepository = new ClinicRepository(mMutableLiveData);
        this.doctor_id = doctor_id;
        callService();
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price+" "+ UserHelper.retrieveCurrency();
    }

    public void onSplitTypeChanged(RadioGroup radioGroup, int id) {
        if(id == R.id.rb_cash)
            reservationRequest.payment_method = 1;
        else if(id == R.id.rb_credit)
            reservationRequest.payment_method = 2;
    }

    public void reviews() {
        mMutableLiveData.setValue(Constants.REVIEW);
    }


    public void submit(){
        if(reservationRequest.service != null && !reservationRequest.service.equals("")) {
            clinicRepository.makeReservation(reservationRequest);
        }else{
            baseError = ResourceManager.getString(R.string.please_select_time_reservation);
            mMutableLiveData.setValue(Constants.ERROR);
        }
    }

    public void callService(){
        clinicRepository.doctorDetails(doctor_id,reservationRequest.date);
    }

    private static final String TAG = "BookDoctorViewModel";

    public ClinicRepository getClinicRepository() {
        return clinicRepository;
    }

    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
    }

}
