
package grand.app.moonshop.viewmodels.notification;

import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;
import grand.app.moonshop.R;
import grand.app.moonshop.base.ParentViewModel;
import grand.app.moonshop.repository.NotificationRepository;
import grand.app.moonshop.utils.resources.ResourceManager;


/**
 * Created by Gregory Rasmussen on 7/26/17.
 */
public class NotificationViewModel extends ParentViewModel {

    NotificationRepository notificationRepository;
    public ObservableField<String> tvNoData = new ObservableField(ResourceManager.getString(R.string.there_are_no)+" "+ResourceManager.getString(R.string.label_notification));
    private ObservableBoolean noData = new ObservableBoolean(false);

    public NotificationViewModel() {
        notificationRepository = new NotificationRepository(mMutableLiveData);
    }

    public void noData(){
        noData.set(true);
    }

    public ObservableBoolean getNoData() {
        return noData;
    }

    public NotificationRepository getNotificationRepository() {
        return notificationRepository;
    }

    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
    }

}
