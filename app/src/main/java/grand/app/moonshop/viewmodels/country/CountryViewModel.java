
package grand.app.moonshop.viewmodels.country;

import androidx.databinding.ObservableBoolean;
import grand.app.moonshop.base.ParentViewModel;
import grand.app.moonshop.repository.CountryRepository;
import grand.app.moonshop.repository.ServiceRepository;
import grand.app.moonshop.utils.Constants;

/**
 * Created by Gregory Rasmussen on 7/26/17.
 */
public class CountryViewModel extends ParentViewModel {

    private CountryRepository countryRepository;
    private ServiceRepository serviceRepository;
    public ObservableBoolean loadView = new ObservableBoolean(false);

    public CountryViewModel() {
        countryRepository = new CountryRepository(mMutableLiveData);
        serviceRepository = new ServiceRepository(mMutableLiveData);
        countryRepository.getCountries(true);
    }

    public void setLoadView(boolean isLoadView) {
        loadView.set(isLoadView);
        notifyChange();
    }

    public void submit() {
        mMutableLiveData.setValue(Constants.SUBMIT);
    }

    public void getAccountTypes(){
        serviceRepository.getAccountTypes();
    }

    public CountryRepository getCountryRepository() {
        return countryRepository;
    }

    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
    }

}
