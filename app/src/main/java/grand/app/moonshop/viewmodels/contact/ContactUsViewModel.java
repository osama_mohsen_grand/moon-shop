
package grand.app.moonshop.viewmodels.contact;

import androidx.databinding.ObservableField;
import grand.app.moonshop.base.ParentViewModel;
import grand.app.moonshop.models.base.StatusMsg;
import grand.app.moonshop.models.contact.ContactUsRequest;
import grand.app.moonshop.repository.SettingsRepository;

/**
 * Created by Gregory Rasmussen on 7/26/17.
 */
public class ContactUsViewModel extends ParentViewModel {
    public static ObservableField<String> type = new ObservableField<>("");
    public ContactUsRequest request ;
    private SettingsRepository repository;
    public StatusMsg statusMsg = null;


    public ContactUsViewModel() {
        request = new ContactUsRequest();
        repository = new SettingsRepository(mMutableLiveData);
        notifyChange();
    }
    public void send() {
        if(request.isValid()) {
            repository.contactUs(request); // send request
        }
    }


    public void sendSupport() {
        if(request.isValidSupport()) {
            repository.contactUs(request); // send request
        }
    }

    public SettingsRepository getRepository() {
        return repository;
    }

}
