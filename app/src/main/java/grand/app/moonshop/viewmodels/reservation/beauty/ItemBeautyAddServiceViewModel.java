package grand.app.moonshop.viewmodels.reservation.beauty;

import grand.app.moonshop.R;
import grand.app.moonshop.base.ParentViewModel;
import grand.app.moonshop.models.app.Mutable;
import grand.app.moonshop.models.reservation.beauty.AddBeautyService;
import grand.app.moonshop.models.reservation.beauty.IdNamePrice;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.utils.resources.ResourceManager;


public class ItemBeautyAddServiceViewModel extends ParentViewModel {

    public AddBeautyService addBeautyService;
    public int position;
    public ItemBeautyAddServiceViewModel(AddBeautyService addBeautyService, int position) {
        this.addBeautyService = addBeautyService;
        this.position = position;
    }

    public void delete(){
        mMutableLiveData.setValue(new Mutable(Constants.DELETE,position));
    }

    public void addImage(){mMutableLiveData.setValue(new Mutable(Constants.IMAGE,position));}

    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
    }
}
