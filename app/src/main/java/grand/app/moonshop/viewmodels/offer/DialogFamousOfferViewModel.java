package grand.app.moonshop.viewmodels.offer;

import androidx.databinding.ObservableBoolean;
import androidx.lifecycle.MutableLiveData;
import grand.app.moonshop.base.ParentViewModel;
import grand.app.moonshop.models.app.Mutable;
import grand.app.moonshop.models.notifications.Notification;
import grand.app.moonshop.models.offer.AdsConfirmRequest;
import grand.app.moonshop.models.reservation.doctor.order.ReservationOrder;
import grand.app.moonshop.notification.NotificationGCMModel;
import grand.app.moonshop.repository.OfferRepository;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.utils.storage.user.UserHelper;


public class DialogFamousOfferViewModel extends ParentViewModel {
    OfferRepository offerRepository;
    public Notification model;

    public DialogFamousOfferViewModel(Notification model, MutableLiveData<Object> mMutableLiveData) {
        this.model = model;
        offerRepository = new OfferRepository(mMutableLiveData);
    }

    //2: accept , 3: reject , 1: no_action
    public void accept() {
        offerRepository.famousOfferResponse(new AdsConfirmRequest(model.adsId,2));
    }

    public void reject() {
        offerRepository.famousOfferResponse(new AdsConfirmRequest(model.adsId,3));
    }

}
