
package grand.app.moonshop.viewmodels.reservation.beauty;

import java.util.List;

import grand.app.moonshop.R;
import grand.app.moonshop.base.ParentViewModel;
import grand.app.moonshop.models.reservation.beauty.AddBeautyService;
import grand.app.moonshop.models.reservation.beauty.AddBeautyServiceRequest;
import grand.app.moonshop.repository.ReservationRepository;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.utils.resources.ResourceManager;
import grand.app.moonshop.vollyutils.VolleyFileObject;
import timber.log.Timber;


/**
 * Created by Gregory Rasmussen on 7/26/17.
 */
public class BeautyAddServiceViewModel extends ParentViewModel {
    private ReservationRepository reservationRepository;
    public int category_delete_position = -1;
    public AddBeautyServiceRequest addBeautyServiceRequest;
    public VolleyFileObject volleyFileObject;

    public BeautyAddServiceViewModel() {
        reservationRepository = new ReservationRepository(mMutableLiveData);
        addBeautyServiceRequest = new AddBeautyServiceRequest();
    }

    public void addImage(){mMutableLiveData.setValue(Constants.SELECT_IMAGE);}

    public void addService(){
        mMutableLiveData.setValue(Constants.ADD);
    }

    public void submit(){
        if (volleyFileObject == null) {
            baseError = ResourceManager.getString(R.string.please_select_image);
            mMutableLiveData.setValue(Constants.ERROR);
        }else if(addBeautyServiceRequest.isValid()){
            reservationRepository.addBeautyServices(addBeautyServiceRequest,volleyFileObject);
        }
    }

    public ReservationRepository getReservationRepository() {
        return reservationRepository;
    }
}
