
package grand.app.moonshop.viewmodels.product;

import java.util.ArrayList;
import java.util.Arrays;

import androidx.databinding.ObservableBoolean;
import androidx.lifecycle.MutableLiveData;
import grand.app.moonshop.R;
import grand.app.moonshop.base.ParentViewModel;
import grand.app.moonshop.models.app.AppMoon;
import grand.app.moonshop.models.base.IdNameImage;
import grand.app.moonshop.models.product.AddProductConsumerSectoralRequest;
import grand.app.moonshop.models.product.details.ProductDetails;
import grand.app.moonshop.repository.ProductRepository;
import grand.app.moonshop.repository.ShopRepository;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.utils.resources.ResourceManager;
import grand.app.moonshop.vollyutils.VolleyFileObject;

/**            addProductConsumerSectoralRequest = new AddProductConsumerSectoralRequest(id, product.mName, product.mPrice,product.wholeSalePrice, String.valueOf(product.mStock),product.minQty, product.mDescription);

 * Created by Gregory Rasmussen on 7/26/17.
 */

public class AddProductConsumerSectoralViewModel extends ParentViewModel {
    public AddProductConsumerSectoralRequest addProductConsumerSectoralRequest;
    private ShopRepository shopRepository;
    public ProductRepository productRepository;
    public int category_id = -1;
    public ObservableBoolean loadedImage = new ObservableBoolean(true);
    public int category_delete_position = -1;
    public int new_image_position = -1;
    public ArrayList<IdNameImage> productImages = new ArrayList<>(Arrays.asList(null, null, null, null, null, null));
    public ArrayList<VolleyFileObject> volleyFileObject = new ArrayList<>();
    private VolleyFileObject volleyFileObjectImage = null;

    private boolean isEdit = false;

    public ObservableBoolean allowSize,allowColor;
    public MutableLiveData<Object> mMutableLiveDataShop = new MutableLiveData<>();;

    public AddProductConsumerSectoralViewModel(int id, ProductDetails product) {
        this.category_id = id;
        allowSize = new ObservableBoolean(AppMoon.allowSize());
        allowColor = new ObservableBoolean(AppMoon.allowColor());
        productRepository = new ProductRepository(mMutableLiveData);
        shopRepository = new ShopRepository(mMutableLiveDataShop);

        if(product == null) {
            addProductConsumerSectoralRequest = new AddProductConsumerSectoralRequest(id,"","","","","","");
            shopRepository.getSizeColor();
        }
    }

    public void getProductDetails(int id){
        isEdit = true;
        allowSize.set(false);
        allowColor.set(false);
        addProductConsumerSectoralRequest = new AddProductConsumerSectoralRequest(id,"","","","","","");
        addProductConsumerSectoralRequest.setId(id);
        productRepository.getProductDetails(id);
    }


    public void submit() {
        if(validImages()) {
            if ((addProductConsumerSectoralRequest.isValid() && volleyFileObject != null) || (addProductConsumerSectoralRequest.isValid() && isEdit)) {

//                ArrayList<VolleyFileObject> volleyFileObjects = new ArrayList<>();
                if(addProductConsumerSectoralRequest.isValid()) {
                    if (isEdit) {
                        if (volleyFileObject.size() == 0){
                            productRepository.editProduct(addProductConsumerSectoralRequest);
                        } else{
                            productRepository.editProduct(addProductConsumerSectoralRequest, volleyFileObject);
                        }
                    } else {
                        productRepository.addProduct(addProductConsumerSectoralRequest, volleyFileObject);
                    }
                }else {
                    baseError = ResourceManager.getString(R.string.please_complete_form);
                    mMutableLiveData.setValue(Constants.ERROR);
                }

            } else if (volleyFileObject == null) {
                baseError = ResourceManager.getString(R.string.please_select_image);
                mMutableLiveData.setValue(Constants.ERROR);
            }
        }else {
            baseError = ResourceManager.getString(R.string.please_upload_advertisement_images);
            mMutableLiveData.setValue(Constants.ERROR);
        }
    }

    public void selectImage(){
        mMutableLiveData.setValue(Constants.SELECT_IMAGE);
    }


    public ProductRepository getProductRepository() {
        return productRepository;
    }

    public ShopRepository getShopRepository() {
        return shopRepository;
    }

    public boolean validImages() {
        int pos = 0;
        volleyFileObject.clear();
        boolean valid = false;
        for (IdNameImage image : productImages) {
            if (image != null && (image.volleyFileObject != null || !image.image.equals(""))) {
                valid = true;
                if(image.volleyFileObject != null){
                    image.volleyFileObject.setParamName("product_images["+pos+"]");
                    volleyFileObject.add(image.volleyFileObject);
                    pos++;
                }
            }
        }
        if(volleyFileObjectImage != null) volleyFileObject.add(volleyFileObjectImage);
        if(volleyFileObjectImage == null && !isEdit){
            valid = false;
        }
        return valid;
    }



    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
    }

    public void delete(int mId) {
        productRepository.deleteImage(mId);
    }

    public void updateProductUi() {
        ProductDetails productDetails = productRepository.getProductDetailsResponse().mData;
        addProductConsumerSectoralRequest = new AddProductConsumerSectoralRequest(category_id,
                productDetails.mName, productDetails.mPrice,productDetails.wholeSalePrice, String.valueOf(productDetails.mStock),
                productDetails.minQty, productDetails.mDescription);
        addProductConsumerSectoralRequest.setId(productDetails.mId);
        int position = 0;
        for(IdNameImage idNameImage : productDetails.images){
            productImages.set(position,idNameImage);
            position++;
        }
        show.set(true);
        notifyChange();
    }

    public void setImage(VolleyFileObject volleyFileObjectImage) {
        this.volleyFileObjectImage = volleyFileObjectImage;
    }
}
