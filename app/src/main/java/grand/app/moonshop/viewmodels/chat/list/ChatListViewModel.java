
package grand.app.moonshop.viewmodels.chat.list;


import grand.app.moonshop.base.ParentViewModel;
import grand.app.moonshop.models.chat.ChatRequest;
import grand.app.moonshop.repository.ChatRepository;

/**
 * Created by Gregory Rasmussen on 7/26/17.
 */
public class ChatListViewModel extends ParentViewModel {

    ChatRepository chatRepository;
    public String text = "";

    public ChatListViewModel() {
        chatRepository = new ChatRepository(mMutableLiveData);
        chatRepository.getChatList();

    }

    public ChatRepository getChatRepository() {
        return chatRepository;
    }



    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
    }

}
