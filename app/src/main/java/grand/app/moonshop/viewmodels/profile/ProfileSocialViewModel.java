
package grand.app.moonshop.viewmodels.profile;

import java.util.ArrayList;

import androidx.databinding.ObservableBoolean;
import grand.app.moonshop.R;
import grand.app.moonshop.adapter.ServiceInfoAdapter;
import grand.app.moonshop.adapter.SocialAdapter;
import grand.app.moonshop.adapter.SocialViewAdapter;
import grand.app.moonshop.base.ParentViewModel;
import grand.app.moonshop.models.personalnfo.AccountInfoResponse;
import grand.app.moonshop.models.user.info.Profile;
import grand.app.moonshop.models.user.info.ProfileInfoResponse;
import grand.app.moonshop.models.user.info.SocialMediaRequest;
import grand.app.moonshop.repository.SettingsRepository;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.utils.resources.ResourceManager;

/**
 * Created by Gregory Rasmussen on 7/26/17.
 */
public class ProfileSocialViewModel extends ParentViewModel {

    int id = -1;
    public SettingsRepository repository;
    private ObservableBoolean noData = new ObservableBoolean(false);
    public AccountInfoResponse response = new AccountInfoResponse();
    public ProfileInfoResponse profileResponse = new ProfileInfoResponse();
    public ServiceInfoAdapter adapter;
    public SocialAdapter socialAdapter;
    public SocialViewAdapter socialViewAdapter;

    public ProfileSocialViewModel() {
        repository = new SettingsRepository(mMutableLiveData);
        adapter = new ServiceInfoAdapter(new ArrayList<>());
        socialAdapter = new SocialAdapter(new ArrayList<>());
        socialViewAdapter = new SocialViewAdapter(new ArrayList<>());
    }
    public ObservableBoolean getNoData() {
        return noData;
    }

    public void email(){
        mMutableLiveData.setValue(Constants.EMAIL);
    }

    public void number(){
        mMutableLiveData.setValue(Constants.PHONE);
    }

    public void website(){
        mMutableLiveData.setValue(Constants.WEB);
    }

    public void getData(){
        repository.accountInfo(id);
    }

    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
    }

    public void submit(){
        boolean valid = true;
        SocialMediaRequest request = new SocialMediaRequest();

//        if(response.email.trim().equals("")){
//            baseError = ResourceManager.getString(R.string.email_not_valid);
//            mMutableLiveData.setValue(Constants.ERROR);
//            valid = false;
//        }else if(response.number.trim().equals("")){
//            baseError = ResourceManager.getString(R.string.phone_not_valid);
//            mMutableLiveData.setValue(Constants.ERROR);
//            valid = false;
//        }else if(response.website.trim().equals("")){
//            baseError = ResourceManager.getString(R.string.this_field_is_required);
//            mMutableLiveData.setValue(Constants.ERROR);
//            valid = false;
//        }else {
//            baseError = "";
//            for (Profile profile : response.profile) {
//                request.socialIds.add(profile.id);
//                request.socialMedia.add(profile.media);
////                if(profile.media.trim().equals("")) {
////                    profile.mediaError.set(ResourceManager.getString(R.string.this_field_is_required));
////                    valid = false;
////                }
//            }
//        }
        if(valid){
            request.email = response.email;
            request.phone = response.number;
            request.website = response.website;
            repository.updateSocial(request);
        }
    }

    public void setId(int id) {
        this.id = id;
    }


    public void setResponse(AccountInfoResponse response) {
        this.response = response;
        adapter.update(response.infoServices);
        socialAdapter.editable = false;
        socialAdapter.update(response.profile);
        socialViewAdapter.update(response.profile);
        showPage(true);
    }

}
