
package grand.app.moonshop.viewmodels.user;

import android.widget.ImageView;

import java.util.ArrayList;
import java.util.Iterator;

import androidx.databinding.Bindable;
import androidx.databinding.BindingAdapter;
import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;
import grand.app.moonshop.R;
import grand.app.moonshop.base.ParentViewModel;
import grand.app.moonshop.models.base.StatusMsg;
import grand.app.moonshop.models.user.profile.ProfileResponse;
import grand.app.moonshop.models.user.profile.ProfileShopRequest;
import grand.app.moonshop.models.user.profile.User;
import grand.app.moonshop.models.user.register.RegisterShopRequest;
import grand.app.moonshop.models.user.register.VolleyFileObjectSerializable;
import grand.app.moonshop.repository.CountryRepository;
import grand.app.moonshop.repository.LoginRepository;
import grand.app.moonshop.repository.RegisterRepository;
import grand.app.moonshop.repository.ServiceRepository;
import grand.app.moonshop.repository.VerificationFirebaseSMSRepository;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.utils.images.ImageLoaderHelper;
import grand.app.moonshop.utils.maputils.base.MapConfig;
import grand.app.moonshop.utils.resources.ResourceManager;
import grand.app.moonshop.utils.storage.user.UserHelper;
import grand.app.moonshop.vollyutils.MyApplication;
import grand.app.moonshop.vollyutils.VolleyFileObject;


/**
 * Created by Gregory Rasmussen on 7/26/17.
 */
public class ProfileViewModel extends ParentViewModel {

    LoginRepository loginRepository;
    public ProfileShopRequest profileRequest = new ProfileShopRequest();
    public StatusMsg statusMsg = null;
    public ObservableBoolean isReservation = new ObservableBoolean(false);
    public String comments = "",description = "";
    public float rate = 0f;

    public ProfileViewModel() {
        loginRepository = new LoginRepository(mMutableLiveData);
        if(UserHelper.getUserDetails().type.equals(Constants.TYPE_RESERVATION_CLINIC)) {
            isReservation.set(true);
            comments = "("+UserHelper.getUserDetails().comments+")";
            description = UserHelper.getUserDetails().description;
            rate = UserHelper.getUserDetails().rate;
        }
    }

    
    public void submit(){
        if(profileRequest.isValid() ){
            if(profileRequest.volleyFileObject == null)
                loginRepository.submitShopProfile(profileRequest);
            else
                loginRepository.submitShopProfile(profileRequest,profileRequest.volleyFileObject);
        }
    }

    public String getImageUrl(){
        return UserHelper.getUserDetails().image;
    }

    public void selectImage(){
        mMutableLiveData.setValue(Constants.SELECT_IMAGE);
    }


    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
    }

    public void setImage(VolleyFileObject volleyFileObject) {
        profileRequest.volleyFileObject = volleyFileObject;
    }

    public void changeLanguageSubmit(){
        mMutableLiveData.setValue(Constants.LANGUAGE);
    }

    public void changeCountrySubmit(){
        mMutableLiveData.setValue(Constants.COUNTRIES);
    }

    public void changePassword(){
        mMutableLiveData.setValue(Constants.CHANGE_PASSWORD);
    }

    public void addressSubmit(){
        mMutableLiveData.setValue(Constants.LOCATIONS);
    }

    public void setAddress(double lat, double lng) {
        profileRequest.setLat(lat);
        profileRequest.setLng(lng);
        profileRequest.setAddress(new MapConfig(MyApplication.getInstance(),null).getAddress(lat,lng));
//        profileRequest.setAddress(ResourceManager.getString(R.string.done_selected_address));
        notifyChange();
    }


    public LoginRepository getLoginRepository() {
        return loginRepository;
    }
}
