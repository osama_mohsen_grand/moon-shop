package grand.app.moonshop.viewmodels.order;


import grand.app.moonshop.base.ParentViewModel;
import grand.app.moonshop.repository.OrderRepository;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.utils.storage.user.UserHelper;

public class OrderListViewModel extends ParentViewModel {
    private OrderRepository orderRepository;
    public OrderListViewModel() {

        orderRepository = new OrderRepository(mMutableLiveData);
        orderRepository.getOrders();
    }

    public void submit(){
        mMutableLiveData.setValue(Constants.SUBMIT);
    }

    public OrderRepository getOrderRepository() {
        return orderRepository;
    }

    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
    }

}
