
package grand.app.moonshop.viewmodels.review;

import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;
import grand.app.moonshop.R;
import grand.app.moonshop.base.ParentViewModel;
import grand.app.moonshop.repository.NotificationRepository;
import grand.app.moonshop.repository.ReviewRepository;
import grand.app.moonshop.utils.resources.ResourceManager;

/**
 * Created by Gregory Rasmussen on 7/26/17.
 */
public class ReviewViewModel extends ParentViewModel {

    ReviewRepository reviewRepository;
    public ObservableField<String> tvNoData = new ObservableField(ResourceManager.getString(R.string.there_are_no)+" "+ResourceManager.getString(R.string.reviews));
    private ObservableBoolean noData = new ObservableBoolean(false);

    public ReviewViewModel(){
        reviewRepository = new ReviewRepository(mMutableLiveData);
        reviewRepository.getReviews();
    }


    public void noData(){
        noData.set(true);
    }

    public ObservableBoolean getNoData() {
        return noData;
    }

    public ReviewRepository getReviewRepository() {
        return reviewRepository;
    }

    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
    }

}
