package grand.app.moonshop.viewmodels.shop;


import grand.app.moonshop.R;
import grand.app.moonshop.base.ParentViewModel;
import grand.app.moonshop.models.ads.Datum;
import grand.app.moonshop.models.app.Mutable;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.utils.resources.ResourceManager;

public class ItemShopAlbumViewModel extends ParentViewModel {
    public Datum model ;
    public int position = 0;
    public boolean selected = false;
    public int width = 0;
    public String imagesCount,videosCount;

    public ItemShopAlbumViewModel(Datum model, int position) {
        this.model = model;
        this.position = position;
        imagesCount = model.imagesCount + " "+ResourceManager.getString(R.string.photos);
        videosCount = model.videoCount + " "+ResourceManager.getString(R.string.video);
    }

    public void shopSubmit(){
        mMutableLiveData.setValue(new Mutable(Constants.SHOP,position));
    }

    public void submit(){
        mMutableLiveData.setValue(new Mutable(Constants.ALBUM,position));
    }
}
