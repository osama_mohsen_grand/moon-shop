
package grand.app.moonshop.viewmodels.offer;

import grand.app.moonshop.base.ParentViewModel;
import grand.app.moonshop.repository.CategoryRepository;
import grand.app.moonshop.repository.OfferRepository;
import grand.app.moonshop.utils.Constants;


/**
 * Created by Gregory Rasmussen on 7/26/17.
 */
public class OfferViewModel extends ParentViewModel {
    private OfferRepository offerRepository;
    public int item_delete_position = -1;

    public OfferViewModel() {
        offerRepository = new OfferRepository(mMutableLiveData);
        offerRepository.getOffers();
    }

    public void addOffer(){
        mMutableLiveData.setValue(Constants.ADD);
    }


    public OfferRepository getOfferRepository() {
        return offerRepository;
    }

    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
    }
}
