
package grand.app.moonshop.viewmodels.user;

import androidx.databinding.ObservableField;
import androidx.lifecycle.MutableLiveData;

import grand.app.moonshop.base.ParentViewModel;
import grand.app.moonshop.models.user.forgetpassword.ForgetPasswordRequest;
import grand.app.moonshop.repository.LoginRepository;
import grand.app.moonshop.repository.VerificationFirebaseSMSRepository;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.utils.Validate;
import timber.log.Timber;

/**
 * Created by Gregory Rasmussen on 7/26/17.
 */
public class PhoneVerificationViewModel extends ParentViewModel {
    VerificationFirebaseSMSRepository verificationFirebaseSMSRepository;
    public MutableLiveData<Object> mMutableLiveDataLogin  = new MutableLiveData<>();;
    LoginRepository loginRepository;

    public String cpp = "";
    private String phone = "";
    private String phoneCpp = "";
    public ObservableField phoneError;
    boolean changePassword=false;


    public PhoneVerificationViewModel(boolean changePassword) {
        this.changePassword=changePassword;
        verificationFirebaseSMSRepository = new VerificationFirebaseSMSRepository(mMutableLiveData);
        loginRepository = new LoginRepository(mMutableLiveDataLogin);
        phoneError = new ObservableField();
    }

    private boolean isValid(){
        boolean valid = true;
        if(!Validate.isValid(cpp+phone, Constants.PHONE)) {
            phoneError.set(Validate.error);
            valid = false;
            Timber.e("phone:error");
        }
        return valid;
    }

    public void verificationSubmit(){
        if(isValid()){
            phoneCpp = cpp+phone;
            Timber.e("phone:"+phoneCpp);
            Timber.e("changePassword"+changePassword);
            loginRepository.checkPhone(new ForgetPasswordRequest(changePassword,phoneCpp));
        }
    }

    public void sendCode(){
        verificationFirebaseSMSRepository.sendVerificationCode(phoneCpp);
    }


    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
        phoneError.set(null);
    }

    public VerificationFirebaseSMSRepository getVerificationFirebaseSMSRepository() {
        return verificationFirebaseSMSRepository;
    }

    public LoginRepository getLoginRepository() {
        return loginRepository;
    }
}
