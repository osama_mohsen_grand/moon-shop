
package grand.app.moonshop.viewmodels.famous.ads;

import java.util.ArrayList;

import androidx.databinding.ObservableBoolean;
import grand.app.moonshop.base.ParentViewModel;
import grand.app.moonshop.models.app.AppMoon;
import grand.app.moonshop.repository.AdsRepository;
import grand.app.moonshop.repository.FamousRepository;
import grand.app.moonshop.utils.Constants;


/**
 * Created by Gregory Rasmussen on 7/26/17.
 */
public class FamousAdsViewModel extends ParentViewModel {

    public int famous_id = -1;
    public int category_delete_position = -1;
    private AdsRepository adsRepository;
    private FamousRepository famousRepository;
    public ObservableBoolean visible;

    public FamousAdsViewModel() {
        adsRepository = new AdsRepository(mMutableLiveData);
        famousRepository = new FamousRepository(mMutableLiveData);
        visible = new ObservableBoolean(AppMoon.isFamous());
        callService();
    }

    public void callService(){
        adsRepository.getAds("2");
    }

    public void submitFilter(ArrayList<Integer> ids){
        adsRepository.getShopAFilter(ids,famous_id);
    }

    public AdsRepository getAdsRepository() {
        return adsRepository;
    }

    public FamousRepository getFamousRepository() {
        return famousRepository;
    }

    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
    }

    public void add(){
        mMutableLiveData.setValue(Constants.ADD);
    }

    public void filter(){
        if(getAdsRepository().getFilterShopAdsResponse() != null && getAdsRepository().getFilterShopAdsResponse().data != null )
            mMutableLiveData.setValue(Constants.FILTER);
        else
            adsRepository.getShopAds("?account_type="+AppMoon.getUserType());
    }

    public void delete(int id) {
        famousRepository.removeAlbumImage(id);
    }
}
