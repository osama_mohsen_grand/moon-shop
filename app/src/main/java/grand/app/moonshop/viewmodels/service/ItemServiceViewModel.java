package grand.app.moonshop.viewmodels.service;

import android.widget.TextView;

import androidx.databinding.BindingAdapter;
import grand.app.moonshop.R;
import grand.app.moonshop.base.ParentViewModel;
import grand.app.moonshop.models.service.Service;

public class ItemServiceViewModel extends ParentViewModel {
    public Service tag;
    public int position = 0;

    public ItemServiceViewModel(Service tag, int position, boolean contains) {
        this.tag = tag;
        this.position = position;
        if (contains)
            tag.image = "true";
        else tag.image = "false";
    }

    public Service getTextCheck() {
        return tag;
    }

    @BindingAdapter("textCheck")
    public static void setTextCheck(TextView textView, Service tag) {
        textView.setText(tag.name);
        if (tag.image.equals("true"))
            textView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_check_circle, 0, 0, 0);
        else
            textView.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
    }

    public void submit() {
        mMutableLiveData.setValue(position);
    }
}
