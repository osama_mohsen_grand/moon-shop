
package grand.app.moonshop.viewmodels.app;

import android.widget.ImageView;


import androidx.databinding.BindingAdapter;
import grand.app.moonshop.base.ParentViewModel;


/**
 * Created by Gregory Rasmussen on 7/26/17.
 */
public class ZoomViewModel extends ParentViewModel {

    public String image;
    public ZoomViewModel(String image) {
        this.image = image;
    }

    public String getZoomImageUrl() {
        return image;
    }


}
