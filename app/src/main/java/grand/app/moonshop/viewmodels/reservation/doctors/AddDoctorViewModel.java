
package grand.app.moonshop.viewmodels.reservation.doctors;

import android.widget.RadioGroup;

import java.util.ArrayList;
import java.util.Iterator;

import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;
import grand.app.moonshop.R;
import grand.app.moonshop.base.ParentViewModel;
import grand.app.moonshop.customviews.facebook.FacebookModel;
import grand.app.moonshop.models.base.StatusMsg;
import grand.app.moonshop.models.reservation.doctor.AddDoctorRequest;
import grand.app.moonshop.models.user.register.RegisterShopRequest;
import grand.app.moonshop.models.user.register.VolleyFileObjectSerializable;
import grand.app.moonshop.repository.CountryRepository;
import grand.app.moonshop.repository.RegisterRepository;
import grand.app.moonshop.repository.ReservationRepository;
import grand.app.moonshop.repository.ServiceRepository;
import grand.app.moonshop.repository.VerificationFirebaseSMSRepository;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.utils.resources.ResourceManager;
import grand.app.moonshop.vollyutils.VolleyFileObject;
import timber.log.Timber;

/**
 * Created by Gregory Rasmussen on 7/26/17.
 */
public class AddDoctorViewModel extends ParentViewModel {
    ReservationRepository reservationRepository;
    public AddDoctorRequest addDoctorRequest = new AddDoctorRequest();

    public AddDoctorViewModel() {
        reservationRepository = new ReservationRepository(mMutableLiveData);
        reservationRepository.getSpecification();

    }

    public void selectImage(){
        mMutableLiveData.setValue(Constants.SELECT_IMAGE);
    }


    public void submit(){
        if(addDoctorRequest.isValid()){
            if(addDoctorRequest.volleyFileObject != null)
                reservationRepository.addDoctor(addDoctorRequest);
            else{
                baseError = ResourceManager.getString(R.string.select_image);
                mMutableLiveData.setValue(Constants.ERROR);
            }

        }
    }

    public void setImage(VolleyFileObject volleyFileObject) {
        addDoctorRequest.volleyFileObject = volleyFileObject;
    }

    public ReservationRepository getReservationRepository() {
        return reservationRepository;
    }


    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
    }
}
