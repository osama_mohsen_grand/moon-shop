
package grand.app.moonshop.viewmodels.famous;

import grand.app.moonshop.base.ParentViewModel;
import grand.app.moonshop.repository.FamousRepository;


/**
 * Created by Gregory Rasmussen on 7/26/17.
 */
public class ShopAlbumViewModel extends ParentViewModel {
    private FamousRepository famousRepository;

    public ShopAlbumViewModel() {
        famousRepository = new FamousRepository(mMutableLiveData);
    }



    public FamousRepository getFamousRepository() {
        return famousRepository;
    }


    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
    }

    public void getCategoryDetails(int famous_id, int id) {
        famousRepository.getCompanyAdsCategories(famous_id,id);
    }
}
