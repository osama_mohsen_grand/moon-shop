
package grand.app.moonshop.viewmodels.user;

import android.util.Log;

import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;
import grand.app.moonshop.base.ParentViewModel;
import grand.app.moonshop.models.user.changepassword.ChangePasswordRequest;
import grand.app.moonshop.repository.LoginRepository;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.utils.storage.user.UserHelper;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Gregory Rasmussen on 7/26/17.
 */
public class ChangePasswordViewModel extends ParentViewModel {
    public ChangePasswordRequest changePasswordRequest;
    public boolean forgotPassword=false;
    LoginRepository loginRepository;

    public ChangePasswordViewModel(ChangePasswordRequest changePasswordRequest) {

        if(changePasswordRequest==null) {
            this.changePasswordRequest = new ChangePasswordRequest();
        }else {
            forgotPassword=true;
            this.changePasswordRequest=changePasswordRequest;
        }
        loginRepository = new LoginRepository(mMutableLiveData);
    }



    public void submit(){
        if(changePasswordRequest.validateInput()){
            loginRepository.changePassword(changePasswordRequest);
        }
    }

    public LoginRepository getLoginRepository() {
        return loginRepository;
    }

    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
    }

}
