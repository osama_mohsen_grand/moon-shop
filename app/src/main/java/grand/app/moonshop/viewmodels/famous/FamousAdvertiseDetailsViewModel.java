package grand.app.moonshop.viewmodels.famous;

import grand.app.moonshop.base.ParentViewModel;
import grand.app.moonshop.models.famous.details.Product;
import grand.app.moonshop.repository.FamousRepository;
import grand.app.moonshop.utils.Constants;

public class FamousAdvertiseDetailsViewModel extends ParentViewModel {
    FamousRepository famousRepository;

    public FamousAdvertiseDetailsViewModel(int id, int type) {
        this.famousRepository = new FamousRepository(mMutableLiveData);
        famousRepository.getAdvertiseDetails(id,type);
    }

    public String getImageUrl(){
        if(famousRepository.getAdvertiseDetailsResponse() != null)
            return famousRepository.getAdvertiseDetailsResponse().data.serviceImage;
        return "";
    }

    public void submitImage(){
        mMutableLiveData.setValue(Constants.ZOOM);
    }

    public void share(){
        mMutableLiveData.setValue(Constants.SHARE);
    }

    public FamousRepository getFamousRepository() {
        return famousRepository;
    }
}
