
package grand.app.moonshop.viewmodels.famous;

import android.util.Log;
import android.widget.LinearLayout;

import androidx.databinding.BindingAdapter;
import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;
import grand.app.moonshop.R;
import grand.app.moonshop.base.ParentViewModel;
import grand.app.moonshop.repository.FamousRepository;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.utils.resources.ResourceManager;


/**
 * Created by Gregory Rasmussen on 7/26/17.
 */
public class FamousListViewModel extends ParentViewModel {

    FamousRepository famousRepository;
    int type;
    int gender = Constants.GENDER_ALL;
    int photographerTypeId = Constants.GENDER_ALL;
    public ObservableField<String> genderText;
    public ObservableField<String> photographerType;
    public String[] images = new String[0];
    private int weight;
    public ObservableBoolean isPhotographer;

    public FamousListViewModel(int type) {
        famousRepository = new FamousRepository(mMutableLiveData);
        weight = (type == Integer.parseInt(Constants.TYPE_PHOTOGRAPHER) ? 3 : 2);
        isPhotographer = new ObservableBoolean(type == Integer.parseInt(Constants.TYPE_PHOTOGRAPHER));
        genderText = new ObservableField<>(ResourceManager.getString(R.string.kind));
        photographerType = new ObservableField<>(ResourceManager.getString(R.string.category));
        this.type = type;
        getFamousList();
    }

    public void getFamousList() {
        famousRepository.getFamousList(type, gender,photographerTypeId);
    }

    public void setGender(int genderId, String genderText) {
        this.gender = genderId;
        this.genderText.set(genderText);
//        images = new String[0];
//        famousRepository.getFamousListResponse().data.sliders.clear();
        getFamousList();
        notifyChange();
    }
    public void setPhotographer(int photographerTypeId, String photohrapherText) {
        this.photographerTypeId = photographerTypeId;
        this.photographerType.set(photohrapherText);
        notifyChange();
        getFamousList();
    }


    public int getWeight() {
        return weight;
    }


    @BindingAdapter("weight")
    public static void setWeight(LinearLayout linearLayout, int weight) {
        linearLayout.setWeightSum(weight);
    }


    public void discover() {
        mMutableLiveData.setValue(Constants.DISCOVER);
    }


    public FamousRepository getFamousRepository() {
        return famousRepository;
    }

    public String[] getImages() {
        if (famousRepository.getFamousListResponse() != null) {
            images = new String[famousRepository.getFamousListResponse().data.sliders.size()];
            for (int i = 0; i < famousRepository.getFamousListResponse().data.sliders.size(); i++)
                images[i] = famousRepository.getFamousListResponse().data.sliders.get(i).image;
        }
        return images;
    }

    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
    }

}
