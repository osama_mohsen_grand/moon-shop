
package grand.app.moonshop.viewmodels.famous.add;

import java.util.ArrayList;

import androidx.databinding.ObservableBoolean;
import grand.app.moonshop.R;
import grand.app.moonshop.base.ParentViewModel;
import grand.app.moonshop.models.famous.album.FamousAddAlbumRequest;
import grand.app.moonshop.repository.FamousRepository;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.utils.resources.ResourceManager;
import grand.app.moonshop.vollyutils.VolleyFileObject;

/**
 * Created by Gregory Rasmussen on 7/26/17.
 */
public class FamousAddNewAddsViewModel extends ParentViewModel {

    public String title = "";
    public String prevText = "";
    public String imageType = "";
    public VolleyFileObject mainImage;
    public boolean selected = false;


    public FamousAddAlbumRequest famousAddAlbumRequest;
    public ObservableBoolean isEdit = new ObservableBoolean(false);

    FamousRepository famousRepository ;


    public FamousAddNewAddsViewModel() {
        famousRepository = new FamousRepository(mMutableLiveData);
        famousAddAlbumRequest = new FamousAddAlbumRequest();
        famousAddAlbumRequest.tab = "2";
        title = ResourceManager.getString(R.string.add_photo_or_video);
    }

    public void addImage(){
        imageType = Constants.IMAGE;
        mMutableLiveData.setValue(Constants.IMAGE);
    }

    public void addVideo(){
        imageType = Constants.VIDEO;
        mMutableLiveData.setValue(Constants.VIDEO);
    }

    public boolean photoProfile(){
        if(mainImage != null || famousAddAlbumRequest.id != null) return true;
        baseError = ResourceManager.getString(R.string.please_select_image_or_video);
        mMutableLiveData.setValue(Constants.ERROR);
        return false;
    }

    public void submit() {
        if(famousAddAlbumRequest.shop_id == -1){
            baseError = ResourceManager.getString(R.string.please_select_your_shop);
            mMutableLiveData.setValue(Constants.ERROR);
        } else if(photoProfile() && famousAddAlbumRequest.isValid()){
            ArrayList<VolleyFileObject> volleyFileObjects = new ArrayList<>();
            if(mainImage != null ) volleyFileObjects.add(mainImage);
            famousRepository.addAlbum(famousAddAlbumRequest,volleyFileObjects);
        }
    }

    public FamousRepository getFamousRepository() {
        return famousRepository;
    }

    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
    }

    public void callService() {
        if(!prevText.equals(famousAddAlbumRequest.getName()) && !selected){
            if(famousAddAlbumRequest.categoryId !=null &&
                    !famousAddAlbumRequest.categoryId.equals("")) {
                prevText = famousAddAlbumRequest.getName();
                famousRepository.getShopByName(prevText,famousAddAlbumRequest.categoryId);

            }else {
                baseError = ResourceManager.getString(R.string.please_select_sub_category_first);
                mMutableLiveData.setValue(Constants.ERROR);
            }
        }
    }
}
