
package grand.app.moonshop.viewmodels.reservation.doctors;


import grand.app.moonshop.base.ParentViewModel;
import grand.app.moonshop.repository.ChatRepository;
import grand.app.moonshop.repository.ReservationRepository;
import grand.app.moonshop.utils.Constants;

/**
 * Created by Gregory Rasmussen on 7/26/17.
 */
public class DoctorListViewModel extends ParentViewModel {

    ReservationRepository reservationRepository;
    public int category_delete_position = -1;
    public String text = "";

    public DoctorListViewModel() {
        reservationRepository = new ReservationRepository(mMutableLiveData);
        reservationRepository.getDoctorList();

    }

    public ReservationRepository getReservationRepository() {
        return reservationRepository;
    }

    public void addDoctor(){
        mMutableLiveData.setValue(Constants.ADD);
    }

    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
    }

}
