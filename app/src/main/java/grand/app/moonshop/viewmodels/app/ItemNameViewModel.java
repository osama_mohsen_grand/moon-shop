package grand.app.moonshop.viewmodels.app;

import grand.app.moonshop.base.ParentViewModel;
import grand.app.moonshop.models.base.IdName;

public class ItemNameViewModel extends ParentViewModel {
    public boolean selected = false;
    public IdName text = null;
    public int position = 0;

    public ItemNameViewModel(IdName text, int position, boolean selected) {
        this.text = text;
        this.position = position;
        this.selected =  selected;
        notifyChange();
    }

    public void textSubmit(){
        mMutableLiveData.setValue(position);
    }
}
