
package grand.app.moonshop.viewmodels.reservation.main;


import grand.app.moonshop.base.ParentViewModel;
import grand.app.moonshop.models.reservation.doctor.order.ReservationOrderActionRequest;
import grand.app.moonshop.repository.ChatRepository;
import grand.app.moonshop.repository.ReservationRepository;

/**
 * Created by Gregory Rasmussen on 7/26/17.
 */
public class ReservationViewModel extends ParentViewModel {

    public int category_delete_position = -1;
    ReservationRepository reservationRepository;
    public ReservationOrderActionRequest reservationOrderActionRequest = new ReservationOrderActionRequest();
    public String text = "",status="";

    public ReservationViewModel(String status) {
        reservationRepository = new ReservationRepository(mMutableLiveData);
        this.status = status;
    }

    public void callService(){
        reservationRepository.getReservations(status);
    }

    public ReservationRepository getReservationRepository() {
        return reservationRepository;
    }

    public void submit(){

    }

    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
    }

}
