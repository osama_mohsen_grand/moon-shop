
package grand.app.moonshop.viewmodels.trip;

import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;
import grand.app.moonshop.R;
import grand.app.moonshop.base.ParentViewModel;
import grand.app.moonshop.repository.TripHistoryRepository;
import grand.app.moonshop.utils.resources.ResourceManager;


/**
 * Created by Gregory Rasmussen on 7/26/17.
 */
public class TripHistoryViewModel extends ParentViewModel {

    TripHistoryRepository tripHistoryRepository;
    public ObservableField<String> tvNoData = new ObservableField(ResourceManager.getString(R.string.there_are_no)+" "+ResourceManager.getString(R.string.ride_history));
    public ObservableBoolean noData = new ObservableBoolean(false);

    public TripHistoryViewModel() {
        tripHistoryRepository = new TripHistoryRepository(mMutableLiveData);
    }

    public void noData(){
        noData.set(true);
        notifyChange();
    }

    public TripHistoryRepository getTripHistoryRepository() {
        return tripHistoryRepository;
    }

    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
    }
}
