
package grand.app.moonshop.viewmodels.chat.details;


import grand.app.moonshop.base.ParentViewModel;
import grand.app.moonshop.models.chat.ChatGetRequest;
import grand.app.moonshop.models.chat.ChatRequest;
import grand.app.moonshop.repository.ChatRepository;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.vollyutils.VolleyFileObject;

/**
 * Created by Gregory Rasmussen on 7/26/17.
 */
public class ChatViewModel extends ParentViewModel {

    ChatRepository chatRepository;
    public String text = "";
    public int id,type,receiver_id,id_chat;
    public boolean allowChat,chatFirst;

    public ChatViewModel(int id, int type, boolean allowChat,boolean chatFirst,int id_chat) {
        chatRepository = new ChatRepository(mMutableLiveData);
        this.id = id;
        this.type = type;
        this.allowChat = allowChat;
        this.chatFirst = chatFirst;

        if(id_chat != -1)
            chatRepository.getChatDetailsFromChatList(id_chat,type);
        else
            chatRepository.getChatDetails(id,type);
    }


    public void sendMessage(){
        if(!text.trim().equals("")) {
            chatRepository.send(new ChatRequest(id,receiver_id,text,type,chatFirst),null,id_chat);
        }
    }


    public void setFile(VolleyFileObject volleyFileObject){
        chatRepository.send(new ChatRequest(id,receiver_id,text,type,true),volleyFileObject,id_chat);
    }
    
    public ChatRepository getChatRepository() {
        return chatRepository;
    }

    public void camera(){
        mMutableLiveData.setValue(Constants.SELECT_IMAGE);
    }

    public void setImage(VolleyFileObject volleyFileObject){
        chatRepository.send(new ChatRequest(id,receiver_id,text,type,chatFirst),volleyFileObject,id_chat);
    }

    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
    }
}
