
package grand.app.moonshop.viewmodels.profile;

import androidx.databinding.ObservableBoolean;
import grand.app.moonshop.base.ParentViewModel;
import grand.app.moonshop.models.country.CountriesResponse;
import grand.app.moonshop.models.user.info.ProfileInfoRequest;
import grand.app.moonshop.models.user.info.ProfileInfoResponse;
import grand.app.moonshop.repository.SettingsRepository;

/**
 * Created by Gregory Rasmussen on 7/26/17.
 */
public class AddTransferGuideViewModel extends ParentViewModel {

    public SettingsRepository repository;
    private ObservableBoolean noData = new ObservableBoolean(false);
    public ProfileInfoResponse response = new ProfileInfoResponse();
    public ProfileInfoRequest request = new ProfileInfoRequest();
    public CountriesResponse countriesResponse = null;

    public AddTransferGuideViewModel() {
        repository = new SettingsRepository(mMutableLiveData);
        repository.accountInfo();
    }
    public ObservableBoolean getNoData() {
        return noData;
    }

    public void submit(){
        if(request.isValid()){
            repository.updateAccountInfo(request);
        }
    }
    public void setData(ProfileInfoResponse response){
        this.response = response;
        request.country = response.transfer.country;
        request.city = response.transfer.city;
        request.region = response.transfer.region;
        request.notes = response.transfer.notes;
        request.bankName = response.transfer.bankName;
        request.accountNumber = response.transfer.accountNumber;
        request.country_id = response.transfer.countryId;
        request.city_id = response.transfer.cityId;
        request.region_id = response.transfer.regionId;
        request.id = response.transfer.id;
    }

    public void getCountries(){
        repository.getCountries(true);
    }

    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
    }

}
