package grand.app.moonshop.viewmodels.bank;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import grand.app.moonshop.R;
import grand.app.moonshop.base.BaseFragment;
import grand.app.moonshop.databinding.FragmentAddTransferGuideBinding;
import grand.app.moonshop.models.app.AppMoon;
import grand.app.moonshop.models.country.City;
import grand.app.moonshop.models.country.Datum;
import grand.app.moonshop.models.country.Region;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.utils.PopUp.PopUpInterface;
import grand.app.moonshop.utils.PopUp.PopUpMenuHelper;
import grand.app.moonshop.utils.storage.user.UserHelper;
import grand.app.moonshop.viewmodels.profile.AddTransferGuideViewModel;
import grand.app.moonshop.viewmodels.profile.ProfileSocialViewModel;
import timber.log.Timber;

public class AddTransferGuideFragment extends BaseFragment {
    View rootView;
    private FragmentAddTransferGuideBinding binding;
    private AddTransferGuideViewModel viewModel;
    private static final String TAG = "TransferGuideFragment";
    public PopUpMenuHelper popUpMenuHelper = new PopUpMenuHelper();

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_add_transfer_guide, container, false);
        getData();
        bind();
        setEvent();
        return rootView;
    }

    private void getData() {
        viewModel = new AddTransferGuideViewModel();
        binding.setViewmodel(viewModel);
    }

    private void bind() {
        rootView = binding.getRoot();
    }

    private void setEvent() {
        viewModel.mMutableLiveData.observe((LifecycleOwner) context, new Observer<Object>() {
            @Override
            public void onChanged(@Nullable Object o) {
                String action = (String) o;
                handleActions(action, viewModel.repository.getMessage());
                Log.d(TAG, action);
                if (action.equals(Constants.SUCCESS)) {
                    viewModel.setData(viewModel.repository.profileInfoResponse);
                    viewModel.showPage(true);
                } else if (action.equals(Constants.COUNTRIES)) {
                    viewModel.countriesResponse = viewModel.repository.getCountriesResponse();
                    setDropDown();
                    binding.edtTransferCountry.performClick();
                } else if(action.equals(Constants.UPDATE)){
                    toastMessage(viewModel.repository.getMessage());
                }
            }
        });
    }

    public List<Region> regions = new ArrayList<>();

    private void setDropDown() {
        binding.edtTransferCountry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ArrayList<String> popUpCountries = new ArrayList<>();
                for (Datum datum : viewModel.countriesResponse.data)
                    popUpCountries.add(datum.countryName);
                popUpMenuHelper.openPopUp(getActivity(), binding.edtTransferCountry, popUpCountries, new PopUpInterface() {
                    @Override
                    public void submitPopUp(int position) {
                        viewModel.request.setCountry(viewModel.countriesResponse.data.get(position).countryName);
                        viewModel.request.country_id = viewModel.countriesResponse.data.get(position).id+"";
                        binding.edtTransferCountry.setText(viewModel.countriesResponse.data.get(position).countryName);
                        binding.edtTransferCity.setText("");
                        binding.edtTransferRegion.setText("");
                        getCities();
                    }
                });
            }
        });
        binding.edtTransferCity.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                getCities();
            }
        });

        binding.edtTransferRegion.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                getRegions();
            }
        });
    }


    private void getCities() {
        if(viewModel.countriesResponse != null && !viewModel.request.country_id.equals("")) {
            ArrayList<String> popUpCities = new ArrayList<>();
            Datum country = AppMoon.getCities(viewModel.request.country_id, viewModel.countriesResponse.data);
            if (country != null) {
                Timber.e("country:" + country.countryName);
                for (City city : country.cities)
                    popUpCities.add(city.cityName);
                popUpMenuHelper.openPopUp(getActivity(), binding.edtTransferCity, popUpCities, new PopUpInterface() {
                    @Override
                    public void submitPopUp(int position) {
                        viewModel.request.setCity_id(String.valueOf(country.cities.get(position).id));
                        viewModel.request.city = country.cities.get(position).cityName;
                        binding.edtTransferCity.setText(country.cities.get(position).cityName);
                        binding.edtTransferRegion.setText("");
                        regions = new ArrayList<>(country.cities.get(position).regions);
                        Timber.e("regions" + regions.size());
                        getRegions();
                    }
                });
            }
        }
    }

    private void getRegions() {
        if(viewModel.countriesResponse != null && !viewModel.request.city_id.equals("")) {
            ArrayList<String> popUpRegions = new ArrayList<>();
            for (Region region : regions)
                popUpRegions.add(region.regionName);
            Timber.e("size:" + popUpRegions.size());
            popUpMenuHelper.openPopUp(getActivity(), binding.edtTransferRegion, popUpRegions, new PopUpInterface() {
                @Override
                public void submitPopUp(int position) {
                    viewModel.request.setRegion_id(String.valueOf(regions.get(position).id));
                    binding.edtTransferRegion.setText(regions.get(position).regionName);
                    viewModel.request.region = regions.get(position).regionName;
                }
            });
        }
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        if (viewModel != null) viewModel.reset();
    }

}