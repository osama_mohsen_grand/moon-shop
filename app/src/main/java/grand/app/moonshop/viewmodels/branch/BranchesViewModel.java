
package grand.app.moonshop.viewmodels.branch;

import grand.app.moonshop.base.ParentViewModel;
import grand.app.moonshop.repository.InstitutionRepository;
import grand.app.moonshop.utils.Constants;

/**
 * Created by Gregory Rasmussen on 7/26/17.
 */
public class BranchesViewModel extends ParentViewModel {
    public int category_delete_position = -1;
    public InstitutionRepository institutionRepository;

    public BranchesViewModel() {
        institutionRepository = new InstitutionRepository(mMutableLiveData);
    }

    public void getBranches(){
        institutionRepository.getService();
    }
    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
    }

    public void add(){
        mMutableLiveData.setValue(Constants.ADD_BRANCH);
    }

    public void delete(Integer id) {
        institutionRepository.removeBranch(id,1);
    }

    public InstitutionRepository getInstitutionRepository() {
        return institutionRepository;
    }
}
