package grand.app.moonshop.viewmodels.reservation.beauty;

import grand.app.moonshop.R;
import grand.app.moonshop.base.ParentViewModel;
import grand.app.moonshop.models.app.Mutable;
import grand.app.moonshop.models.reservation.beauty.IdNamePrice;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.utils.resources.ResourceManager;
import grand.app.moonshop.utils.storage.user.UserHelper;


public class ItemBeautyServiceViewModel extends ParentViewModel {

    public IdNamePrice service;
    public int position;
    public String price;

    public ItemBeautyServiceViewModel(IdNamePrice service , int position) {
        this.service = service;
        this.position = position;
        price = service.price+" "+ UserHelper.retrieveCurrency();
    }

    public void delete(){
        mMutableLiveData.setValue(new Mutable(Constants.DELETE,position));
    }


    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
    }
}
