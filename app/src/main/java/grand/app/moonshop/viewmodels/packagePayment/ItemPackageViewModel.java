package grand.app.moonshop.viewmodels.packagePayment;


import android.util.Log;

import grand.app.moonshop.base.ParentViewModel;
import grand.app.moonshop.models.app.Mutable;
import grand.app.moonshop.models.packagePayment.Datum;
import grand.app.moonshop.utils.Constants;

public class ItemPackageViewModel extends ParentViewModel {
    public Datum _package = null;
    public int position = 0;

    private static final String TAG = "ItemPackageViewModel";

    public ItemPackageViewModel(Datum _package, int position) {
        this._package = _package;
        this.position = position;
        Log.d(TAG, "ItemPackageViewModel: ");
        Log.d(TAG,""+_package.description);
    }

    public void _packageSubmit(){
        mMutableLiveData.setValue(position);
    }
}
