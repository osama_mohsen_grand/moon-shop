
package grand.app.moonshop.viewmodels.reservation.beauty;

import grand.app.moonshop.R;
import grand.app.moonshop.base.ParentViewModel;
import grand.app.moonshop.repository.ReservationRepository;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.utils.resources.ResourceManager;
import grand.app.moonshop.utils.storage.user.UserHelper;


/**
 * Created by Gregory Rasmussen on 7/26/17.
 */
public class BeautyOrderDetailsViewModel extends ParentViewModel {
    private ReservationRepository reservationRepository;
    public int category_delete_position = -1;
    private String price = "";

    public BeautyOrderDetailsViewModel(int id) {
        reservationRepository = new ReservationRepository(mMutableLiveData);
        reservationRepository.getOrderDetailsBeauty(id);
    }

    public String getPrice() {
        if(getReservationRepository().getBeautyOrderDetailsResponse() != null)
            price = getReservationRepository().getBeautyOrderDetailsResponse().data.fees + " "+ UserHelper.retrieveCurrency();
        return price;
    }

    public void addService(){
        mMutableLiveData.setValue(Constants.ADD);
    }


    public ReservationRepository getReservationRepository() {
        return reservationRepository;
    }
}
