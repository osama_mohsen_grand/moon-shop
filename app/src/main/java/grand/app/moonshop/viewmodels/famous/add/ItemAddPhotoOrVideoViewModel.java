package grand.app.moonshop.viewmodels.famous.add;


import android.graphics.Bitmap;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Log;
import android.webkit.MimeTypeMap;
import android.widget.ImageView;

import java.io.File;
import java.sql.Time;

import androidx.databinding.BindingAdapter;
import grand.app.moonshop.R;
import grand.app.moonshop.base.ParentViewModel;
import grand.app.moonshop.models.app.Mutable;
import grand.app.moonshop.models.base.IdNameImage;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.utils.resources.ResourceManager;
import grand.app.moonshop.vollyutils.VolleyFileObject;
import timber.log.Timber;

public class ItemAddPhotoOrVideoViewModel extends ParentViewModel {
    public VolleyFileObject imageUrlFile = null;
    public String title = "";
    public int position = 0;
    public String image = "";
    public boolean allowDelete = false,allowImageUrl=false,allowImageUriSrc=false;

    public ItemAddPhotoOrVideoViewModel(String type, VolleyFileObject image, int position) {
        this.position = position;
        this.imageUrlFile = image;
        if (type.equals("1"))
            title = ResourceManager.getString(R.string.add_photo);
        else
            title = ResourceManager.getString(R.string.add_video);
        allowImageUrl = false;
        allowImageUriSrc = true;
    }

    public ItemAddPhotoOrVideoViewModel(String type, IdNameImage idNameImage, int position, boolean allowDelete) {
        this.position = position;
        if (type.equals("1"))
            title = ResourceManager.getString(R.string.add_photo);
        else
            title = ResourceManager.getString(R.string.add_video);

        if(idNameImage != null) {
            this.allowDelete = allowDelete;
            this.image = idNameImage.image;
            imageUrlFile = idNameImage.volleyFileObject;
            if (imageUrlFile != null && idNameImage.image.equals("")) {
                allowImageUriSrc = true;
            } else if (idNameImage.image != null && !idNameImage.image.equals(""))
                allowImageUrl = true;
            if (imageUrlFile == null && (image == null || image.equals("")))
                this.allowDelete = false;
        }
        notifyChange();
    }

    public VolleyFileObject getImageUrlFile() {
        return imageUrlFile;
    }


    private static final String TAG = "ItemAddPhotoOrVideoView";

    @BindingAdapter("imageUrlFile")
    public static void loadImage(ImageView imageView, VolleyFileObject file) {
        try {
            if (file != null) {
                Timber.e("file not null:"+file.getFilePath());
                File file1 = new File(file.getFilePath());
                String extension = MimeTypeMap.getFileExtensionFromUrl(file1.getAbsolutePath());
                if(extension != null && !extension.equals("")) {
                    Timber.e("extension:extension");
                    String type = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
                    if(type!= null && !type.equals("")) {
                        if (!type.contains(Constants.IMAGE)) {
                            Timber.e("image not done HWERE");
                            Bitmap thumb = ThumbnailUtils.createVideoThumbnail(file1.getAbsolutePath(),
                                    MediaStore.Images.Thumbnails.MINI_KIND);
                            imageView.setImageResource(0);
                            imageView.setImageBitmap(thumb);
                        } else {
                            Timber.e("image done HWERE");
                            if(file.getFile().exists())
                                Log.d(TAG,"done exist");
                            else
                                Log.d(TAG,"done not exist");

                            imageView.setImageResource(0);
                            imageView.setImageDrawable(null);
                            imageView.setImageURI(Uri.parse(String.valueOf(file.getFile())));
                        }
                    }else{
                        Timber.e("IMAGE DONE");
                        imageView.setImageResource(0);
                        imageView.setImageURI(Uri.parse(String.valueOf(file1)));
                    }
                }else{
                    Timber.e("IMAGE DONE");
                    imageView.setImageResource(0);
                    imageView.setImageURI(Uri.parse(String.valueOf(file1)));
                }
            }else{
                imageView.setImageDrawable(null);
            }
        } catch (Exception ex) {
            Timber.e("exc"+ex.getMessage());
        }
    }


    public void addAlbumSubmit() {
        mMutableLiveData.setValue(new Mutable(Constants.SUBMIT,position));
    }

    public void deleteSubmit(){
        mMutableLiveData.setValue(new Mutable(Constants.DELETE,position));
    }
}
