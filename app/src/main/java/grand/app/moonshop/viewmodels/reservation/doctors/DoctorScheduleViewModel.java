
package grand.app.moonshop.viewmodels.reservation.doctors;


import grand.app.moonshop.base.ParentViewModel;
import grand.app.moonshop.models.reservation.doctor.schedule.AddScheduleRequest;
import grand.app.moonshop.repository.ReservationRepository;
import grand.app.moonshop.utils.Constants;
import timber.log.Timber;

/**
 * Created by Gregory Rasmussen on 7/26/17.
 */
public class DoctorScheduleViewModel extends ParentViewModel {

    ReservationRepository reservationRepository;
    public String text = "";
    public int position_updated = -1;

    public DoctorScheduleViewModel(int id) {
        reservationRepository = new ReservationRepository(mMutableLiveData);
        reservationRepository.getDoctorSchedule(id);
    }

    public ReservationRepository getReservationRepository() {
        return reservationRepository;
    }

    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
    }

    public void addSchedule(AddScheduleRequest addScheduleRequest) {
        addScheduleRequest.from.clear();
        addScheduleRequest.to.clear();
        addScheduleRequest.period.clear();
        addScheduleRequest.from.add(addScheduleRequest.getFromText());
        addScheduleRequest.to.add(addScheduleRequest.getToText());
        addScheduleRequest.period.add(addScheduleRequest.getPeriodText());
        if(!addScheduleRequest.getFromText2().equals("") && !addScheduleRequest.getToText2().equals("") && !addScheduleRequest.periodText2.equals("")) {
            addScheduleRequest.from.add(addScheduleRequest.getFromText2());
            addScheduleRequest.to.add(addScheduleRequest.getToText2());
            addScheduleRequest.period.add(addScheduleRequest.periodText2);
        }
        reservationRepository.addDoctorSchedule(addScheduleRequest);
    }
}
