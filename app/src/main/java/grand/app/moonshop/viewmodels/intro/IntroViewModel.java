
package grand.app.moonshop.viewmodels.intro;

import android.view.Gravity;

import androidx.databinding.BindingAdapter;
import androidx.fragment.app.FragmentManager;
import androidx.viewpager.widget.ViewPager;
import grand.app.moonshop.adapter.PagerAdapter;
import grand.app.moonshop.base.ParentViewModel;

/**
 * Created by Gregory Rasmussen on 7/26/17.
 */
public class IntroViewModel extends ParentViewModel {

    public FragmentManager fragmentManager;
    public IntroViewModel(FragmentManager fragmentManager) {
        this.fragmentManager = fragmentManager;
    }

    public FragmentManager getAdapter(){
        return fragmentManager;
    }

    @BindingAdapter("adapter")
    public static void adapter(ViewPager wPager, FragmentManager fragmentManager) {
        wPager.setAdapter(new PagerAdapter(fragmentManager));
        wPager.setEnabled(false);
        final ViewPager.LayoutParams layoutParams = new ViewPager.LayoutParams();
        layoutParams.width = ViewPager.LayoutParams.MATCH_PARENT;
        layoutParams.height = ViewPager.LayoutParams.WRAP_CONTENT;
        layoutParams.gravity = Gravity.BOTTOM;
    }

    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
    }

}
