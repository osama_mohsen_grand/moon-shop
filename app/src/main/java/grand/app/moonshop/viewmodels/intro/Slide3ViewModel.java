
package grand.app.moonshop.viewmodels.intro;

import grand.app.moonshop.base.ParentViewModel;
import grand.app.moonshop.utils.Constants;

/**
 * Created by Gregory Rasmussen on 7/26/17.
 */
public class Slide3ViewModel extends ParentViewModel {

    public Slide3ViewModel() {

    }
    public void submit(){
        mMutableLiveData.setValue(Constants.LOGIN);
    }

    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
    }

}
