//welcome here

package grand.app.moonshop.viewmodels.user;

import android.util.Log;
import android.widget.RadioGroup;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;

import androidx.lifecycle.MutableLiveData;
import grand.app.moonshop.R;
import grand.app.moonshop.base.ParentViewModel;
import grand.app.moonshop.customviews.facebook.FacebookModel;
import grand.app.moonshop.models.app.AppMoon;
import grand.app.moonshop.models.base.StatusMsg;
import grand.app.moonshop.models.country.Datum;
import grand.app.moonshop.models.user.register.RegisterShopRequest;
import grand.app.moonshop.repository.CountryRepository;
import grand.app.moonshop.repository.FirebaseRepository;
import grand.app.moonshop.repository.RegisterRepository;
import grand.app.moonshop.repository.ServiceRepository;
import grand.app.moonshop.repository.VerificationFirebaseSMSRepository;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.utils.maputils.base.MapConfig;
import grand.app.moonshop.utils.resources.ResourceManager;
import grand.app.moonshop.utils.storage.user.UserHelper;
import grand.app.moonshop.vollyutils.MyApplication;
import grand.app.moonshop.vollyutils.VolleyFileObject;
import timber.log.Timber;

/**
 * Created by Gregory Rasmussen on 7/26/17.
 */
public class RegisterViewModel extends ParentViewModel {
    public Datum country = AppMoon.getCities(UserHelper.retrieveKey(Constants.COUNTRY_ID), UserHelper.getCountries().data);
    VerificationFirebaseSMSRepository verificationFirebaseSMSRepository;
    public RegisterShopRequest request;


    public ObservableBoolean checkedValue = new ObservableBoolean(true);
    public ObservableBoolean isPhotographerOrFamous = new ObservableBoolean(false);

    public ObservableBoolean isCommercial = new ObservableBoolean(false);
    public ObservableBoolean isReservation = new ObservableBoolean(false);
    public ObservableField<String> uploadFrontAndBack = new ObservableField<>(ResourceManager.getString(R.string.please_upload_image_back_and_front));

    public ServiceRepository serviceRepository;
    RegisterRepository registerRepository;
    CountryRepository countryRepository;
    public StatusMsg statusMsg = null;
    public String image_select = "";

    private static final String[] VALUES = new String[]{"AB", "BC", "CD", "AE"};

    public static ObservableField<String> type = new ObservableField<>("");
    public ObservableBoolean showPage = new ObservableBoolean(false);


    public RegisterViewModel(RegisterShopRequest request) {
        Log.d(TAG, country.countryName);
        Log.d(TAG, country.code);
        Log.d(TAG, country.countryImage);
        if (request != null) {
            this.request = request;
        } else {
            this.request = new RegisterShopRequest();
        }
        this.request.initError();


        verificationFirebaseSMSRepository = new VerificationFirebaseSMSRepository(mMutableLiveData);

        registerRepository = new RegisterRepository(mMutableLiveData);
        serviceRepository = new ServiceRepository(mMutableLiveData);
        countryRepository = new CountryRepository(mMutableLiveData);
        countryRepository.setMessage(-1, "");
//        countryRepository.getCountries(true);


    }

    public void onSplitTypeChanged(RadioGroup radioGroup, int id) {
        if (id == R.id.male) {
            request.setGender("1");
        } else if (id == R.id.female) {
            request.setGender("2");
        }
    }


    public void getServices(int type_id, int flag, int type) {
        serviceRepository.setMessage(-1, "");
        serviceRepository.getServices(type_id, flag, type);
    }

    public RegisterRepository getRegisterRepository() {
        return registerRepository;
    }

    public ServiceRepository getServiceRepository() {
        return serviceRepository;
    }

    public CountryRepository getCountryRepository() {
        return countryRepository;
    }

    private static final String TAG = "RegisterViewModel";


    public void firstPage() {
        if (request.isValid1()) {
            if (request.images[0]) {
                registerRepository.checkNickname(request);
            } else {
                baseError = ResourceManager.getString(R.string.select_image_profile);
                mMutableLiveData.setValue(Constants.ERROR);
            }
        }
    }

    public void secondPage() {

//        if (((request.images[0] && request.images[1] && request.images[2]) ||
//                (request.images[0] && isPhotographerOrFamous.get() ||
//                        (request.isHomeServiceOrConsumer.get())))) {
//
//            if (request.isValid2()) {
//                registerRepository.checkUserData(request);
//            }
//
//        }

        if (request.isValid2()) {
            mMutableLiveData.setValue(Constants.REGISTRATION3);
        }

    }

    FirebaseRepository firebaseRepository = new FirebaseRepository(new MutableLiveData<>());
    public void signUp() {
        try {
            if (request.isValid3()) {
                if (checkedValue.get()) {
                    registerRepository.setMessage(-1, "");
                    countryRepository.setMessage(-1, "");
                    serviceRepository.setMessage(-1, "");
                    Log.d(TAG, "" + request.getType());
                    if (request.getType().equals(Constants.TYPE_RESERVATION_CLINIC) || request.getType().equals(Constants.TYPE_RESERVATION_BEAUTY)) {
                        try {
                            if (request.phone_list != null) {
                                request.phone_list.clear();
                                request.phone_list.add(request.getPhone());
                            }
                        } catch (Exception ex) {
                            ex.getMessage();
                        }
                        if (request.phone2 != null && !request.phone2.equals(""))
                            request.phone_list.add(request.phone2);
                        if (request.phone3 != null && !request.phone3.equals(""))
                            request.phone_list.add(request.phone3);
                    }
                    registerRepository.registerUser(request, request.imagesKey, request.imagesPath);
                } else {
                    baseError = ResourceManager.getString(R.string.please_complete_form);
                    mMutableLiveData.setValue(Constants.ERROR);
                }
            }
        }catch (Exception ex){
            SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy HH:mm", Locale.ENGLISH);
            Date c = Calendar.getInstance().getTime();
            String formattedDate = df.format(c);
            firebaseRepository.crashLog(formattedDate , ex.getMessage());
            baseError = "Error and crash log";
            mMutableLiveData.setValue(Constants.ERROR);
        }

    }

    public void selectImage(String image) {
        image_select = image;
        mMutableLiveData.setValue(Constants.SELECT_IMAGE);
    }


    public void terms() {
        mMutableLiveData.setValue(Constants.TERMS);
    }

    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
    }

    public void setImage(String paramName, VolleyFileObject volleyFileObject) {


        if (image_select.equals("shop_image")) {
            //Done Select ImageVideo
            request.images[0] = true;
        } else if (image_select.equals(Constants.COMMERCIAL_IMAGE)) {
            request.images[1] = true;
//            beShopRequest.setCommercial(ResourceManager.getString(R.string.done_selected_image));
//            notifyChange();
        } else if (image_select.equals(Constants.LICENCE_IMAGE)) {
            request.images[2] = true;
//            beShopRequest.setLicense(ResourceManager.getString(R.string.done_selected_image));
//            notifyChange();
        }

        request.imagesKey.add(paramName);
        request.imagesPath.add(volleyFileObject.getFilePath());
    }

    public void addressSubmit() {
        mMutableLiveData.setValue(Constants.LOCATIONS);
    }

    public VerificationFirebaseSMSRepository getVerificationFirebaseSMSRepository() {
        return verificationFirebaseSMSRepository;
    }

    public void changeVisibility() {
        Timber.e("type_change_vi" + request.getType());
        if (request.getType().equals(Constants.TYPE_PHOTOGRAPHER) || request.getType().equals(Constants.TYPE_FAMOUS_PEOPLE))
            isPhotographerOrFamous.set(true);
        else
            isPhotographerOrFamous.set(false);

        if (request.getType().equals(Constants.TYPE_COMMERCIALS)) {
            isCommercial.set(true);
        } else
            isCommercial.set(false);

        if (request.getType().equals(Constants.TYPE_RESERVATION_CLINIC) || request.getType().equals(Constants.TYPE_RESERVATION_BEAUTY)) {
            isReservation.set(true);
        } else
            isReservation.set(false);


        notifyChange();
    }

    public void setAddress(double lat, double lng) {
        request.setLat(lat);
        request.setLng(lng);
        request.setAddress(new MapConfig(MyApplication.getInstance(), null).getAddress(lat, lng));
//        registerShopRequest.setAddress(ResourceManager.getString(R.string.done_selected_address));
        notifyChange();
    }

    public void setShowPage(boolean showPage) {
        this.showPage.set(showPage);
//        notifyChange();
    }

    public void setSocial(FacebookModel facebookModel) {
        request.setName(facebookModel.name);
        request.setEmail(facebookModel.email);
        request.facebook_id = facebookModel.social_id;
        request.facebook_image = facebookModel.image;
        notifyChange();
    }
}
