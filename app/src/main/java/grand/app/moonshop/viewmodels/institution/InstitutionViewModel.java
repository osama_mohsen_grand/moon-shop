
package grand.app.moonshop.viewmodels.institution;

import android.view.Gravity;

import androidx.databinding.BindingAdapter;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.MutableLiveData;
import androidx.viewpager.widget.ViewPager;
import grand.app.moonshop.R;
import grand.app.moonshop.adapter.PagerAdapter;
import grand.app.moonshop.base.ParentViewModel;
import grand.app.moonshop.repository.InstitutionRepository;
import grand.app.moonshop.utils.resources.ResourceManager;

/**
 * Created by Gregory Rasmussen on 7/26/17.
 */
public class InstitutionViewModel extends ParentViewModel {

    public InstitutionRepository institutionRepository;

    public String followCount = "",image = "",name="",commentCount="";
    public float rate = 0;

    public InstitutionViewModel() {
        institutionRepository = new InstitutionRepository(mMutableLiveData);
        institutionRepository.getService();
    }

    public String getName() {
        if(institutionRepository.shopDetailsResponse != null)
            name = institutionRepository.shopDetailsResponse.shopData.shop_details.name;
        return name;
    }

    public String getImage(){
        if(institutionRepository.shopDetailsResponse != null)
            image = institutionRepository.shopDetailsResponse .shopData.shop_details.image;
        return image;
    }

    public String getFollowCount() {
        if(institutionRepository.shopDetailsResponse != null)
            followCount = ResourceManager.getString(R.string.followers)+ " "+institutionRepository.shopDetailsResponse .shopData.shop_details.followersCount+" "+ResourceManager.getString(R.string.people);
        return followCount;
    }

    public String getCommentCount() {
        if(institutionRepository.shopDetailsResponse != null)
            commentCount = institutionRepository.shopDetailsResponse .shopData.shop_details.commentCount+"";
        return commentCount;
    }

    public float getRate() {
        if(institutionRepository.shopDetailsResponse != null)
            rate = institutionRepository.shopDetailsResponse .shopData.shop_details.rate;
        return rate;
    }

    public InstitutionRepository getInstitutionViewModel() {
        return institutionRepository;
    }
}
