
package grand.app.moonshop.viewmodels.famous.details;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.databinding.BindingAdapter;
import de.hdodenhof.circleimageview.CircleImageView;
import grand.app.moonshop.R;
import grand.app.moonshop.base.ParentViewModel;
import grand.app.moonshop.models.famous.details.FamousDetailsResponse;
import grand.app.moonshop.models.personalnfo.AccountInfoResponse;
import grand.app.moonshop.repository.FamousRepository;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.utils.resources.ResourceManager;
import grand.app.moonshop.utils.storage.user.UserHelper;
import grand.app.moonshop.views.activities.BaseActivity;


/**
 * Created by Gregory Rasmussen on 7/26/17.
 */
public class FamousDetailsViewModel extends ParentViewModel {
    public AccountInfoResponse accountInfoResponse = null;


//    public FamousDetailsResponse famousDetailsResponse = new FamousDetailsResponse();
//
//    private FamousRepository famousRepository;
//    public int id = -1, type = -1;
//    public String name = "", followerCount;
//    public String followers = "";
//    public boolean isFollow = false, allowChat = false,hasShop =false;
//
//
//    public FamousDetailsViewModel(int type) {
//        famousRepository = new FamousRepository(mMutableLiveData);
//        this.type = type;
//        if (type == Integer.parseInt(Constants.TYPE_PHOTOGRAPHER)) {
//            allowChat = true;
//        }
//    }
//
//
//    public void setData(int id, String name, String image, String followerCount, int isFollow) {
//        this.id = id;
//        this.name = name;
//        this.followerCount = followerCount;
//        if (isFollow == 1) this.isFollow = true;
//        hasShop = famousDetailsResponse.hasShop;
//        setFollowerCount();
//        notifyChange();
//    }
//
//    public void setFollowerCount() {
//        followers = followerCount + " " + ResourceManager.getString(R.string.followers);
//    }
//
//
//    public void chatSubmit() {
//        if (UserHelper.getUserId() == -1) {
//            baseError = ResourceManager.getString(R.string.please_login_first);
//            mMutableLiveData.setValue(Constants.ERROR);
//        } else
//            mMutableLiveData.setValue(Constants.CHAT_DETAILS);
//    }
//
//    public void shopSubmit(){
//        mMutableLiveData.setValue(Constants.SHOP_DETAILS);
//    }
//    public void shareSubmit(){
//        mMutableLiveData.setValue(Constants.SHARE);
//    }
//
//    public FamousRepository getFamousRepository() {
//        return famousRepository;
//    }
//
//    public void reset() {
//        unSubscribeFromObservable();
//        compositeDisposable = null;
//    }

    private FamousRepository famousRepository;
    public String name = "";
    public String followers = "";
    private String image = "";

    public FamousDetailsViewModel() {
        famousRepository = new FamousRepository(mMutableLiveData);
    }

    public String getImage(){
        return image;
    }


    public void setData(String name ,String image, String followerCount){
        this.name = name;
        this.image = image;
        followers = followerCount+" "+ResourceManager.getString(R.string.followers);
        notifyChange();
    }


    public void chatSubmit(){
        mMutableLiveData.setValue(Constants.CHAT_DETAILS);
    }

    public void shareSubmit(){
        mMutableLiveData.setValue(Constants.SHARE_CONTENT);
    }

    public FamousRepository getFamousRepository() {
        return famousRepository;
    }

    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
    }


}
