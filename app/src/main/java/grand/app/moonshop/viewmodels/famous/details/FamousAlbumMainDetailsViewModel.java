
package grand.app.moonshop.viewmodels.famous.details;

import android.util.Log;

import java.util.ArrayList;

import androidx.databinding.ObservableBoolean;
import grand.app.moonshop.base.ParentViewModel;
import grand.app.moonshop.models.app.AppMoon;
import grand.app.moonshop.models.famous.album.FamousAddImageInsideAlbumRequest;
import grand.app.moonshop.repository.CategoryRepository;
import grand.app.moonshop.repository.FamousRepository;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.vollyutils.VolleyFileObject;


/**
 * Created by Gregory Rasmussen on 7/26/17.
 */
public class FamousAlbumMainDetailsViewModel extends ParentViewModel {
    public boolean allow_add = true;
    private FamousRepository famousRepository;
    public FamousAddImageInsideAlbumRequest famousAddImageInsideAlbumRequest;
    public int category_delete_position = -1;
    public ObservableBoolean visible = new ObservableBoolean(false);
    public String albumName = "";

    public FamousAlbumMainDetailsViewModel(int id, String type, String tab,String albumName) {
        type = (type.equals(Constants.IMAGE) ? "1" : "2");
        famousAddImageInsideAlbumRequest = new FamousAddImageInsideAlbumRequest(id,type,tab);
        getAlbumDetails(id);
        this.albumName = albumName;
//        if(!visible.get() && allowAdd) visible.set(allowAdd);
    }

    //have list that mean make page visible
    public FamousAlbumMainDetailsViewModel() {
        init();
    }

    public void init(){
        famousRepository = new FamousRepository(mMutableLiveData);
    }

    public void getAlbumDetails(int id){
        init();
        famousRepository.getAlbumImages(id);
    }



    public void addImage(){
        mMutableLiveData.setValue(Constants.IMAGE);
    }

    public void addVideo(){
        mMutableLiveData.setValue(Constants.VIDEO);
    }

    //add image im album
    public void addImage(VolleyFileObject volleyFileObject) {
        ArrayList<VolleyFileObject> volleyFileObjects = new ArrayList<>(); volleyFileObjects.add(volleyFileObject);
        famousRepository.addImageToAlbum(famousAddImageInsideAlbumRequest,volleyFileObjects);
    }

    //delete image from album
    public void delete(int id) {
        famousRepository.removeAlbumImage(id);
    }

    private static final String TAG = "FamousAlbumMainDetailsV";
    public void setData(){
        visible.set(famousRepository.getFamousAlbumImagesResponse().allow_add);
    }

    public FamousRepository getFamousRepository() {
        return famousRepository;
    }

    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
    }
}
