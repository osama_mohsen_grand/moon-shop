package grand.app.moonshop.viewmodels.suggest;


import grand.app.moonshop.base.ParentViewModel;
import grand.app.moonshop.models.base.IdName;

public class ItemSuggestViewModel extends ParentViewModel {
    public IdName idName ;
    public int position = 0;


    public ItemSuggestViewModel(IdName idName, int position) {
        this.idName = idName;
        this.position = position;
        notifyChange();
    }


    public void submit(){
        mMutableLiveData.setValue(position);
    }
}
