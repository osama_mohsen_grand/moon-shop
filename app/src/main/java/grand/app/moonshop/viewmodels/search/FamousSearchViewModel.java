
package grand.app.moonshop.viewmodels.search;

import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;
import grand.app.moonshop.R;
import grand.app.moonshop.base.ParentViewModel;
import grand.app.moonshop.repository.SearchRepository;
import grand.app.moonshop.utils.resources.ResourceManager;


/**
 * Created by Gregory Rasmussen on 7/26/17.
 */
public class FamousSearchViewModel extends ParentViewModel {

    int type;
    public String text = "";
    public SearchRepository searchRepository;

    public FamousSearchViewModel(int type) {
        searchRepository = new SearchRepository(mMutableLiveData);
        noDataText = new ObservableField(ResourceManager.getString(R.string.there_are_no)+" "+ResourceManager.getString(R.string.result));
        this.type = type;
    }

    public void submitSearch(){
        if(!text.trim().equals(""))
            searchRepository.famous(type,text);
    }

    public SearchRepository getSearchRepository() {
        return searchRepository;
    }

    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
    }

}
