package grand.app.moonshop.viewmodels.product;


import android.view.View;

import androidx.databinding.Bindable;
import grand.app.moonshop.base.ParentViewModel;
import grand.app.moonshop.models.app.Mutable;
import grand.app.moonshop.models.home.Datum;
import grand.app.moonshop.models.product.Product;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.utils.storage.user.UserHelper;

public class ItemProductViewModel extends ParentViewModel {
    public Product product = null;
    public int position = 0;
    public String price = "";

    public ItemProductViewModel(Product product, int position) {
        this.product = product;
        this.position = position;
        price = product.mPrice + " "+ UserHelper.retrieveCurrency();
    }

    @Bindable
    public Float getRating(){
        return product.rate;
    }


    public String getImageUrl(){
        return product.mImage;
    }

    public void submit(View v){
        Mutable mutable = new Mutable(Constants.PRODUCT,position,v);
        mMutableLiveData.setValue(mutable);
    }

    public void editSubmit(){
        mMutableLiveData.setValue(new Mutable(Constants.EDIT,position));
    }

    public void deleteSubmit(){
        mMutableLiveData.setValue(new Mutable(Constants.DELETE,position));
    }

}
