
package grand.app.moonshop.viewmodels.service;

import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;
import grand.app.moonshop.R;
import grand.app.moonshop.base.ParentViewModel;
import grand.app.moonshop.repository.NotificationRepository;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.utils.resources.ResourceManager;


/**
 * Created by Gregory Rasmussen on 7/26/17.
 */
public class ServiceViewModel extends ParentViewModel {

    public ServiceViewModel() {
    }

    public void submit(){
        mMutableLiveData.setValue(Constants.SUBMIT);
    }

    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
    }

}
