
package grand.app.moonshop.viewmodels.home.shop;

import grand.app.moonshop.base.ParentViewModel;
import grand.app.moonshop.repository.CategoryRepository;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.utils.maputils.base.MapConfig;


/**
 * Created by Gregory Rasmussen on 7/26/17.
 */
public class DepartmentViewModel extends ParentViewModel {
    private CategoryRepository categoryRepository;
    public int category_delete_position = -1;

    public DepartmentViewModel() {
        categoryRepository = new CategoryRepository(mMutableLiveData);
        categoryRepository.home();
    }

    public void addDepartment(){
        mMutableLiveData.setValue(Constants.ADD);
    }

    public CategoryRepository getCategoryRepository() {
        return categoryRepository;
    }


}
