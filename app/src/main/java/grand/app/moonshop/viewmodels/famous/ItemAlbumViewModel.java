package grand.app.moonshop.viewmodels.famous;


import android.graphics.Bitmap;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Log;
import android.webkit.MimeTypeMap;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import java.io.File;

import androidx.databinding.BindingAdapter;
import androidx.databinding.ObservableBoolean;
import grand.app.moonshop.R;
import grand.app.moonshop.base.ParentViewModel;
import grand.app.moonshop.models.app.AppMoon;
import grand.app.moonshop.models.app.Mutable;
import grand.app.moonshop.models.famous.home.ImageVideo;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.utils.resources.ResourceManager;
import grand.app.moonshop.utils.storage.user.UserHelper;
import grand.app.moonshop.vollyutils.AppHelper;
import timber.log.Timber;

public class ItemAlbumViewModel extends ParentViewModel {
    public ImageVideo model;
    public int width = 0;
    public int position = 0;
    public ObservableBoolean allowAction;

    public String photos,videos;
    private static final String TAG = "ItemAlbumViewModel";

    public ItemAlbumViewModel(ImageVideo model, int position) {
        this.position = position;
        this.model = model;
        photos = ResourceManager.getString(R.string.photos)+" "+model.count;
        videos = ResourceManager.getString(R.string.videos)+" "+model.videoCount;
        Log.d(TAG,model.shopId+"");
        Log.d(TAG,model.accountType+"");
        Log.d(TAG,AppMoon.getUserType()+"");
        Log.d(TAG, UserHelper.getUserDetails().id+"");
        allowAction = new ObservableBoolean(AppMoon.isValidAction(model.shopId,model.accountType));
    }


    public void edit() {
        mMutableLiveData.setValue(new Mutable(Constants.EDIT, position));
    }

    public void delete() {
        mMutableLiveData.setValue(new Mutable(Constants.DELETE, position));
    }

    public void submit() {
        mMutableLiveData.setValue(new Mutable(Constants.SUBMIT, position));
    }
}
