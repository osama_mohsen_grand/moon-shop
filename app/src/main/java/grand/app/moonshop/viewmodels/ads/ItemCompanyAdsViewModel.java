package grand.app.moonshop.viewmodels.ads;


import grand.app.moonshop.R;
import grand.app.moonshop.base.ParentViewModel;
import grand.app.moonshop.models.album.AlbumModel;
import grand.app.moonshop.models.album.AlbumModelCategories;
import grand.app.moonshop.utils.resources.ResourceManager;

public class ItemCompanyAdsViewModel extends ParentViewModel {
    public AlbumModelCategories model ;
    public int position = 0;
    public boolean selected = false;
    public int width = 0;
    public String count;                               

    public ItemCompanyAdsViewModel(AlbumModelCategories model, int position) {
        this.model = model;
        this.position = position;
        count = model.shopsCount + " "+ResourceManager.getString(R.string.shop);
    }

    public void submit(){
        mMutableLiveData.setValue(position);
    }
}
