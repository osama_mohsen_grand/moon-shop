
package grand.app.moonshop.viewmodels.product;

import androidx.lifecycle.MutableLiveData;
import grand.app.moonshop.base.ParentViewModel;
import grand.app.moonshop.repository.CategoryRepository;
import grand.app.moonshop.repository.ProductRepository;
import grand.app.moonshop.utils.Constants;


/**
 * Created by Gregory Rasmussen on 7/26/17.
 */
public class ProductsViewModel extends ParentViewModel {
    public int page = 1;
    private ProductRepository productRepository;
    public int item_delete_position = -1;

    public ProductsViewModel(int category_id) {
//        mMutableLiveData =  new MutableLiveData<>();;
        productRepository = new ProductRepository(mMutableLiveData);
        productRepository.getProducts(category_id,page);
    }

    public void addProduct(){
        mMutableLiveData.setValue(Constants.ADD);
    }

    public ProductRepository getProductRepository() {
        return productRepository;
    }

    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
    }
}
