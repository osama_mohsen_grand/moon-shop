
package grand.app.moonshop.viewmodels.offer;

import androidx.databinding.ObservableField;
import androidx.databinding.ObservableInt;
import grand.app.moonshop.base.ParentViewModel;
import grand.app.moonshop.models.offer.AddOfferRequest;
import grand.app.moonshop.models.rate.RateRequest;
import grand.app.moonshop.repository.OfferRepository;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.vollyutils.VolleyFileObject;

/**
 * Created by Gregory Rasmussen on 7/26/17.
 */
public class AddOfferViewModel extends ParentViewModel {
    private OfferRepository offerRepository;
    public AddOfferRequest addOfferRequest;
    public AddOfferViewModel() {
        addOfferRequest = new AddOfferRequest();
        offerRepository = new OfferRepository(mMutableLiveData);
    }

    public void selectImage(){
        mMutableLiveData.setValue(Constants.IMAGE);
    }
    public void selectVideo(){
        mMutableLiveData.setValue(Constants.VIDEO);
    }

    public void submit() {
        mMutableLiveData.setValue(Constants.ADD);
    }

    public OfferRepository getOfferRepository() {
        return offerRepository;
    }

    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
    }

}
