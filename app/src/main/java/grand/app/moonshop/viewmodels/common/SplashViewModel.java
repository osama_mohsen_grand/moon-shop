
package grand.app.moonshop.viewmodels.common;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import grand.app.moonshop.base.ParentActivity;
import grand.app.moonshop.base.ParentViewModel;
import grand.app.moonshop.repository.CountryRepository;
import grand.app.moonshop.repository.FirebaseRepository;
import grand.app.moonshop.repository.LoginRepository;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.utils.storage.user.UserHelper;


/**
 * Created by Gregory Rasmussen on 7/26/17.
 */
public class SplashViewModel extends ParentViewModel {

    CountryRepository countryRepository;
    public FirebaseRepository firebaseRepository;
    public SplashViewModel() {
        countryRepository = new CountryRepository(mMutableLiveData);
        firebaseRepository = new FirebaseRepository(mMutableLiveData);
//        countryRepository.getCountries(false);
    }

    public CountryRepository getCountryRepository() {
        return countryRepository;
    }

}
