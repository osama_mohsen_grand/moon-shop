package grand.app.moonshop.viewmodels.branch;

import androidx.databinding.ObservableBoolean;
import grand.app.moonshop.base.ParentViewModel;
import grand.app.moonshop.models.app.Mutable;
import grand.app.moonshop.models.shop.Branch;
import grand.app.moonshop.utils.Constants;

public class ItemBranchViewModel extends ParentViewModel {
    public Branch branch ;
    private String price;
    public int position = 0;
    public int width = 0;
    public ObservableBoolean allowDelete;

    public ItemBranchViewModel(Branch branch, int position,boolean allowDelete) {
        this.branch = branch;
        this.position = position;
        this.allowDelete = new ObservableBoolean(allowDelete);
        notifyChange();
    }

    public String getImageUrl(){
        return branch.image;
    }

    public void submit(){
        mMutableLiveData.setValue(new Mutable(Constants.SUBMIT,position));
    }

    public void delete(){mMutableLiveData.setValue(new Mutable(Constants.DELETE,position));}
}
