package grand.app.moonshop.viewmodels.order;

import grand.app.moonshop.R;
import grand.app.moonshop.base.ParentViewModel;
import grand.app.moonshop.models.order.Order;
import grand.app.moonshop.utils.resources.ResourceManager;


public class ItemOrderViewModel extends ParentViewModel {
    public Order order = null;
    public int position = 0;
    public String orderNo;

    public ItemOrderViewModel(Order order, int position) {
        this.order = order;
        orderNo =  ResourceManager.getString(R.string.order_number_quote)+" "+order.orderNumber;
        this.position = position;
    }


    public void submit(){

        mMutableLiveData.setValue(position);
    }


}
