package grand.app.moonshop.viewmodels.famous.details;


import android.widget.RelativeLayout;

import androidx.databinding.BindingAdapter;
import androidx.databinding.ObservableBoolean;
import grand.app.moonshop.base.ParentViewModel;
import grand.app.moonshop.models.app.AppMoon;
import grand.app.moonshop.models.app.Mutable;
import grand.app.moonshop.models.base.IdNameImage;
import grand.app.moonshop.models.home.Datum;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.vollyutils.AppHelper;

public class ItemAlbumImageViewModel extends ParentViewModel {
    public IdNameImage model = null;
    public ObservableBoolean play;
    public ObservableBoolean allowAction;
    public String type;
    public int position;

    public ItemAlbumImageViewModel(IdNameImage model, int position) {
        this.model = model;
        this.position = position;
        play = new ObservableBoolean(model.type == 2);
        allowAction = new ObservableBoolean(AppMoon.isValidAction(model.shopId,model.accountType));
        type = (model.type == 1 ? Constants.IMAGE : Constants.VIDEO);
    }

    public void submit(){
        mMutableLiveData.setValue(new Mutable(model.type == 1 ? Constants.IMAGE : Constants.VIDEO,position));
    }

    public void delete(){
        mMutableLiveData.setValue(new Mutable(Constants.DELETE,position));
    }

    public String getImageUrl() {
        return  (model.type == 1 ? model.image : model.capture);

    }
}
