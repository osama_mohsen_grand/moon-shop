
package grand.app.moonshop.viewmodels.search;

import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;
import grand.app.moonshop.R;
import grand.app.moonshop.base.ParentViewModel;
import grand.app.moonshop.repository.SearchRepository;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.utils.resources.ResourceManager;


/**
 * Created by Gregory Rasmussen on 7/26/17.
 */
public class AlbumSearchViewModel extends ParentViewModel {

    public String text = "";
    public SearchRepository searchRepository;
    public ObservableField<String> tvNoData = new ObservableField(ResourceManager.getString(R.string.there_are_no)+" "+ResourceManager.getString(R.string.result));
    public ObservableBoolean noData = new ObservableBoolean(true);



    public AlbumSearchViewModel() {
        searchRepository = new SearchRepository(mMutableLiveData);
    }

    public void submitSearch(){
        if(!text.trim().equals(""))
            searchRepository.album(text);
    }

    public SearchRepository getSearchRepository() {
        return searchRepository;
    }

    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
    }

}
