
package grand.app.moonshop.viewmodels.famous;

import grand.app.moonshop.base.ParentViewModel;
import grand.app.moonshop.repository.FamousRepository;
import grand.app.moonshop.utils.Constants;


/**
 * Created by Gregory Rasmussen on 7/26/17.
 */
public class FamousTabViewModel extends ParentViewModel {

    FamousRepository famousRepository;

    public FamousTabViewModel(int id , int type) {
        famousRepository = new FamousRepository(mMutableLiveData);
        famousRepository.getFamousDetails(id,type);
    }

    public void zoom(){
        mMutableLiveData.setValue(Constants.ZOOM);
    }


    public FamousRepository getFamousRepository() {
        return famousRepository;
    }

    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
    }

}
