
package grand.app.moonshop.viewmodels.famous.add;

import android.util.Log;

import java.util.ArrayList;
import java.util.Arrays;

import grand.app.moonshop.R;
import grand.app.moonshop.base.ParentViewModel;
import grand.app.moonshop.models.famous.album.FamousAddAlbumRequest;
import grand.app.moonshop.repository.FamousRepository;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.utils.resources.ResourceManager;
import grand.app.moonshop.vollyutils.VolleyFileObject;

/**
 * Created by Gregory Rasmussen on 7/26/17.
 */
public class FamousAddAlbumViewModel extends ParentViewModel {
    private static final String TAG = "FamousAddAlbumViewModel";

    public int position = -1;
    public String imageType = "";
    public VolleyFileObject mainImage;
    public ArrayList<VolleyFileObject> paths = new ArrayList<>(Arrays.asList(null,null,null,null,null,null));
//    public ArrayList<VolleyFileObject> paths = new ArrayList<>();
    public ArrayList<VolleyFileObject> submitPath = new ArrayList<>();

    public FamousAddAlbumRequest famousAddAlbumRequest;
    FamousRepository famousRepository ;


    public FamousAddAlbumViewModel(String type) {
        famousRepository = new FamousRepository(mMutableLiveData);
        famousAddAlbumRequest = new FamousAddAlbumRequest();
        famousAddAlbumRequest.type = type;
        famousAddAlbumRequest.tab = "1";
    }

    public void profileImage(){
        imageType = Constants.IMAGE;
        mMutableLiveData.setValue(Constants.IMAGE);
    }

    public boolean photoProfile(){
        if(mainImage != null) return true;
        baseError = ResourceManager.getString(R.string.please_select_album_photo);
//        Toast.makeText(MyApplication.getInstance(), ""+ResourceManager.getString(R.string.please_select_album_photo), Toast.LENGTH_SHORT).show();
        mMutableLiveData.setValue(Constants.ERROR);
        Log.d(TAG,"profile false");
        return false;
    }

    public boolean pathsValid(){
        for(VolleyFileObject path: paths) {
            if (path != null) return true;
        }
        if(famousAddAlbumRequest.type.equals("1"))
            baseError = ResourceManager.getString(R.string.please_select_one_image_at_least);
//        Toast.makeText(MyApplication.getInstance(), ""+ResourceManager.getString(R.string.please_select_one_image_at_least), Toast.LENGTH_SHORT).show();
        else if(famousAddAlbumRequest.type.equals("2"))
            baseError = ResourceManager.getString(R.string.please_select_one_video_at_least);
//        Toast.makeText(MyApplication.getInstance(), ""+ResourceManager.getString(R.string.please_select_one_video_at_least), Toast.LENGTH_SHORT).show();
        mMutableLiveData.setValue(Constants.ERROR);
        return false;
    }

    public void submit() {
        if(famousAddAlbumRequest.isValidAlbum() && photoProfile()){
//            if(pathsValid()){
                submitPath.clear();
                submitPath.add(mainImage);
//                int counter = 0;
//                for(VolleyFileObject path : paths) {
//                    if (path != null) {
//                        path.setParamName("images["+counter+"]");
//                        submitPath.add(path);
//                        counter++;
//                    }
//                }
                famousRepository.addAlbum(famousAddAlbumRequest,submitPath);
//            }
        }
    }

    public FamousRepository getFamousRepository() {
        return famousRepository;
    }

    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
    }
}
