package grand.app.moonshop.viewmodels.institution;


import grand.app.moonshop.base.ParentViewModel;
import grand.app.moonshop.models.institution.EditInstitutionDetailsRequest;
import grand.app.moonshop.models.shop.ShopDetails;
import grand.app.moonshop.repository.InstitutionRepository;
import grand.app.moonshop.repository.ProductRepository;
import grand.app.moonshop.utils.Constants;

public class InstitutionEditDetailsViewModel extends ParentViewModel {
    private InstitutionRepository institutionRepository;
    public int service_delete_position = -1;
    public ShopDetails shopDetails;
    public EditInstitutionDetailsRequest editInstitutionDetailsRequest;
    public InstitutionEditDetailsViewModel(ShopDetails shopDetails) {
        editInstitutionDetailsRequest = new EditInstitutionDetailsRequest(shopDetails.id,shopDetails.name,shopDetails.description,shopDetails.lat,shopDetails.lng);
        institutionRepository = new InstitutionRepository(mMutableLiveData);
        this.shopDetails = shopDetails;
    }

    public void addService(){
        mMutableLiveData.setValue(Constants.ADD_SERVICE);
    }

    public void submit(){
        institutionRepository.editDetails(editInstitutionDetailsRequest);
    }

    public void delete(int id){
        institutionRepository.removeBranch(id,2);
    }

    public void addressSubmit(){
        mMutableLiveData.setValue(Constants.LOCATION);
    }

    public InstitutionRepository getInstitutionRepository() {
        return institutionRepository;
    }
}
