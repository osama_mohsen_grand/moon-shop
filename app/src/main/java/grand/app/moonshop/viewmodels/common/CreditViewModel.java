
package grand.app.moonshop.viewmodels.common;

import grand.app.moonshop.base.ParentViewModel;
import grand.app.moonshop.models.credit.CreditResponse;
import grand.app.moonshop.repository.CountryRepository;
import grand.app.moonshop.repository.CreditRepository;


/**
 * Created by Gregory Rasmussen on 7/26/17.
 */
public class CreditViewModel extends ParentViewModel {

    CreditRepository creditRepository;
    CreditResponse creditResponse = new CreditResponse();

    public CreditViewModel() {
        creditRepository = new CreditRepository(mMutableLiveData);
        creditRepository.getCredit();
    }

    public CreditResponse getCreditResponse() {
        return creditResponse;
    }

    public void setCreditResponse(CreditResponse creditResponse) {
        this.creditResponse = creditResponse;
    }

    public CreditRepository getCreditRepository() {
        return creditRepository;
    }
}
