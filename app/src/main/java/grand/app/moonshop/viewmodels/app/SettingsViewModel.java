
package grand.app.moonshop.viewmodels.app;

import java.util.ArrayList;

import grand.app.moonshop.adapter.SettingsAdapter;
import grand.app.moonshop.base.ParentViewModel;
import grand.app.moonshop.repository.SettingsRepository;


/**
 * Created by Gregory Rasmussen on 7/26/17.
 */
public class SettingsViewModel extends ParentViewModel {

    private SettingsRepository settingsRepository;
    public SettingsAdapter adapter;

    /* if type = 1 link terms and  if type = 2 link about us */

    public SettingsViewModel(int type) {
        settingsRepository = new SettingsRepository(mMutableLiveData);
        adapter = new SettingsAdapter(new ArrayList<>());
        settingsRepository.getSettings(type);
    }


    public SettingsRepository getSettingsRepository() {
        return settingsRepository;
    }

    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
    }

}
