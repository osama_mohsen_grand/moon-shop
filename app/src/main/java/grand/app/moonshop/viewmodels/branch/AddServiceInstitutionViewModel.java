
package grand.app.moonshop.viewmodels.branch;

import android.util.Log;

import grand.app.moonshop.R;
import grand.app.moonshop.base.ParentViewModel;
import grand.app.moonshop.models.base.IdNameImage;
import grand.app.moonshop.models.institution.AddBranchRequest;
import grand.app.moonshop.models.institution.AddServiceRequest;
import grand.app.moonshop.models.institution.UpdateServiceRequest;
import grand.app.moonshop.repository.InstitutionRepository;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.utils.resources.ResourceManager;

/**
 * Created by Gregory Rasmussen on 7/26/17.
 */
public class AddServiceInstitutionViewModel extends ParentViewModel {
    private InstitutionRepository institutionRepository;
    public AddServiceRequest addServiceRequest;
    public IdNameImage idNameImage = null;
    public AddServiceInstitutionViewModel() {
        addServiceRequest = new AddServiceRequest();
        institutionRepository = new InstitutionRepository(mMutableLiveData);
    }

    private static final String TAG = "AddServiceInstitutionVi";

    public void submit() {
        if(addServiceRequest.isValid() && idNameImage != null){
            UpdateServiceRequest updateServiceRequest = new UpdateServiceRequest(idNameImage.id,addServiceRequest);
//            Log.d(TAG,""+updateServiceRequest)
            institutionRepository.updateService(updateServiceRequest);
        }else if(addServiceRequest.isValid() && addServiceRequest.volleyFileObject != null){
            institutionRepository.addService(addServiceRequest);
        }else if(addServiceRequest.isValid() && addServiceRequest.volleyFileObject == null){
            baseError = ResourceManager.getString(R.string.select_add_branch_photo);
            mMutableLiveData.setValue(Constants.ERROR);
        }
    }

    public void selectImage(){
        mMutableLiveData.setValue(Constants.IMAGE);
    }


    public InstitutionRepository getInstitutionRepository() {
        return institutionRepository;
    }

    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
    }

    public void setData(IdNameImage idNameImage) {
        this.idNameImage = idNameImage;
        addServiceRequest.setName(idNameImage.name);
    }
}
