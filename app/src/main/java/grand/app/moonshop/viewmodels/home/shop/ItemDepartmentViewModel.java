package grand.app.moonshop.viewmodels.home.shop;


import grand.app.moonshop.base.ParentViewModel;
import grand.app.moonshop.models.app.Mutable;
import grand.app.moonshop.models.home.Datum;
import grand.app.moonshop.utils.Constants;

public class ItemDepartmentViewModel extends ParentViewModel {
    public Datum category = null;
    public int position = 0;

    public ItemDepartmentViewModel(Datum category, int position) {
        this.category = category;
        this.position = position;
    }

    public void submit(){
        mMutableLiveData.setValue(new Mutable(Constants.PRODUCTS,position));
    }

    public void editSubmit(){
        mMutableLiveData.setValue(new Mutable(Constants.EDIT,position));
    }

    public void deleteSubmit(){
        mMutableLiveData.setValue(new Mutable(Constants.DELETE,position));
    }

    public void categorySubmit(){
        mMutableLiveData.setValue(position);
    }
}
