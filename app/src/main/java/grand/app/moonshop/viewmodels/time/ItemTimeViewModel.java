package grand.app.moonshop.viewmodels.time;


import android.graphics.drawable.Drawable;
import android.widget.TextView;

import androidx.databinding.BindingAdapter;
import grand.app.moonshop.R;
import grand.app.moonshop.base.ParentViewModel;
import grand.app.moonshop.models.reservation.DateTimeModel;
import grand.app.moonshop.utils.resources.ResourceManager;

public class ItemTimeViewModel extends ParentViewModel {
    public DateTimeModel dateTimeModel ;
    public int position = 0;
    public int width = 0;
    public boolean checked,exist;

    public ItemTimeViewModel(DateTimeModel dateTimeModel, int position, boolean checked , boolean exist) {
        this.dateTimeModel = dateTimeModel;
        this.position = position;
        this.checked = checked;
        this.exist = exist;
        notifyChange();
    }

    public Drawable getBgColor(){
        if(exist)
            return ResourceManager.getDrawable(R.drawable.border_background_black_5);
        else if(checked)
            return ResourceManager.getDrawable(R.drawable.border_background_primary_5);
        else
            return ResourceManager.getDrawable(R.drawable.border_background_primary_strock_5);
    }

    public int getColor(){
        if(exist || checked)
            return ResourceManager.getColor(R.color.colorWhite);
        else
            return ResourceManager.getColor(R.color.colorPrimary);
    }

    @BindingAdapter("bgColor")
    public static void setBgColor(TextView textView, Drawable drawable) {
        textView.setBackground(drawable);
    }

    public void submit(){
        if(!exist)
            mMutableLiveData.setValue(position);
    }
}
