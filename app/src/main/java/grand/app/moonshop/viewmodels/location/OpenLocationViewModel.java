
package grand.app.moonshop.viewmodels.location;

import grand.app.moonshop.base.ParentViewModel;
import grand.app.moonshop.utils.Constants;

/**
 * Created by Gregory Rasmussen on 7/26/17.
 */
public class OpenLocationViewModel extends ParentViewModel {


    public OpenLocationViewModel() {

        notifyChange();
    }

    public void submit() {
        mMutableLiveData.setValue(Constants.LOCATION_ENABLE);
    }

    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
    }

}
