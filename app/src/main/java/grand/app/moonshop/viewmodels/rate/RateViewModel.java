
package grand.app.moonshop.viewmodels.rate;

import android.widget.ImageView;

import androidx.databinding.Bindable;
import androidx.databinding.BindingAdapter;
import androidx.databinding.ObservableField;
import androidx.databinding.ObservableInt;
import grand.app.moonshop.base.ParentViewModel;
import grand.app.moonshop.models.rate.RateRequest;
import grand.app.moonshop.utils.images.ImageLoaderHelper;

/**
 * Created by Gregory Rasmussen on 7/26/17.
 */
public class RateViewModel extends ParentViewModel {
    public RateRequest rateRequest ;
    public ObservableInt rating = new ObservableInt(0);
    public String total = "";
    public String tripId = "";
    public String name = "";
    public String image = "";
    //tripDetailsResponse.data.user.name
    public static ObservableField<String> text = new ObservableField<>("");


    public RateViewModel(String tripId,String name,String image,String total) {
        text.set("");
        this.tripId = tripId;
        this.name = name;
        this.image = image;
        this.total = total;
        notifyChange();
    }

    public void showPayment(){
        notifyChange();
    }

    public void sendSubmit() {
        rateRequest = new RateRequest(tripId,rating.get(),text.get());
    }


    @Bindable
    public String getImageUrl(){
        return image;
    }

    @BindingAdapter("imageUrl")
    public static void loadImage(ImageView imageView, String image) {
        ImageLoaderHelper.ImageLoaderLoad(imageView.getContext(),image,imageView);
    }



    

    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
    }

}
