
package grand.app.moonshop.viewmodels.product;

import java.util.ArrayList;
import java.util.Arrays;

import androidx.databinding.ObservableBoolean;
import androidx.lifecycle.MutableLiveData;
import grand.app.moonshop.R;
import grand.app.moonshop.base.ParentViewModel;
import grand.app.moonshop.models.app.AppMoon;
import grand.app.moonshop.models.base.IdNameImage;
import grand.app.moonshop.models.product.AddProductConsumerSectoralRequest;
import grand.app.moonshop.models.product.AddProductSectoralRequest;
import grand.app.moonshop.models.product.Product;
import grand.app.moonshop.models.product.details.ProductDetails;
import grand.app.moonshop.repository.ProductRepository;
import grand.app.moonshop.repository.ShopRepository;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.utils.resources.ResourceManager;
import grand.app.moonshop.vollyutils.VolleyFileObject;
import timber.log.Timber;

/**
 * Created by Gregory Rasmussen on 7/26/17.
 */
public class AddProductSectoralViewModel extends ParentViewModel {
    public AddProductSectoralRequest addProductSectoralRequest;
    public ProductRepository productRepository;
    private ShopRepository shopRepository;
    public int category_id = -1;
    public ObservableBoolean loadedImage = new ObservableBoolean(true);
    private boolean isEdit = false;
    public ObservableBoolean allowSize,allowColor;

    public int category_delete_position = -1;
    public int new_image_position = -1;
    public ArrayList<IdNameImage> productImages = new ArrayList<>(Arrays.asList(null, null, null, null, null, null));
    public ArrayList<VolleyFileObject> volleyFileObject = new ArrayList<>();
    public MutableLiveData<Object> mMutableLiveDataShop = new MutableLiveData<>();;
    private VolleyFileObject volleyFileObjectImage = null;

    public AddProductSectoralViewModel(int id, ProductDetails product) {
        this.category_id = id;
        productRepository = new ProductRepository(mMutableLiveData);
        shopRepository = new ShopRepository(mMutableLiveDataShop);
        allowSize = new ObservableBoolean(AppMoon.allowSize());
        allowColor = new ObservableBoolean(AppMoon.allowColor());
        if(product == null) {
            addProductSectoralRequest = new AddProductSectoralRequest(id, "", "", "", "");
            shopRepository.getSizeColor();
        }
    }

    public void getProductDetails(int id){
        isEdit = true;
        allowSize.set(false);
        allowColor.set(false);
        addProductSectoralRequest = new AddProductSectoralRequest(id, "", "", "", "");
        addProductSectoralRequest.setId(id);
        productRepository.getProductDetails(id);
    }



    public void submit() {
        if(validImages()) {
            if ((addProductSectoralRequest.isValid() && volleyFileObject != null) || (addProductSectoralRequest.isValid() && isEdit)) {

//                ArrayList<VolleyFileObject> volleyFileObjects = new ArrayList<>();
                if(addProductSectoralRequest.isValid()) {
                    if (isEdit) {
                        if (volleyFileObject.size() == 0){
                            productRepository.editProduct(addProductSectoralRequest);
                        } else{
                            productRepository.editProduct(addProductSectoralRequest, volleyFileObject);
                        }
                    } else {
                        productRepository.addProduct(addProductSectoralRequest, volleyFileObject);
                    }
                }else {
                    baseError = ResourceManager.getString(R.string.please_complete_form);
                    mMutableLiveData.setValue(Constants.ERROR);
                }

            } else if (volleyFileObject == null) {
                baseError = ResourceManager.getString(R.string.please_select_image);
                mMutableLiveData.setValue(Constants.ERROR);
            }
        }else {
            baseError = ResourceManager.getString(R.string.please_upload_advertisement_images);
            mMutableLiveData.setValue(Constants.ERROR);
        }
    }

    public boolean validImages() {
        int pos = 0;
        volleyFileObject.clear();
        boolean valid = false;
        for (IdNameImage image : productImages) {
            if (image != null && (image.volleyFileObject != null || !image.image.equals(""))) {
                valid = true;
                if(image.volleyFileObject != null){
                    image.volleyFileObject.setParamName("product_images["+pos+"]");
                    volleyFileObject.add(image.volleyFileObject);
                    pos++;
                }
            }
        }
        if(volleyFileObjectImage != null) volleyFileObject.add(volleyFileObjectImage);
        if(volleyFileObjectImage == null && !isEdit){
            valid = false;
        }
        return valid;
    }

    

    public void selectImage(){
        mMutableLiveData.setValue(Constants.SELECT_IMAGE);
    }


    public ProductRepository getProductRepository() {
        return productRepository;
    }

    public ShopRepository getShopRepository() {
        return shopRepository;
    }


    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
    }

    public void delete(int mId) {
        productRepository.deleteImage(mId);
    }

    public void updateProductUi() {
        ProductDetails productDetails = productRepository.getProductDetailsResponse().mData;
//        AddProductSectoralRequest(id, product.mName, product.mPrice, String.valueOf(product.mStock), product.mDescription);
        addProductSectoralRequest = new AddProductSectoralRequest(category_id,
                productDetails.mName, productDetails.mPrice,String.valueOf(productDetails.mStock), productDetails.mDescription);
        addProductSectoralRequest.setId(productDetails.mId);
        int position = 0;
        for(IdNameImage idNameImage : productDetails.images){
            productImages.set(position,idNameImage);
            position++;
        }
        show.set(true);
        notifyChange();
    }

    public void setImage(VolleyFileObject volleyFileObjectImage) {
        this.volleyFileObjectImage = volleyFileObjectImage;
    }
}
