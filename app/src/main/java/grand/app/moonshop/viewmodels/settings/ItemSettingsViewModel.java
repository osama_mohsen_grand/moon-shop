package grand.app.moonshop.viewmodels.settings;


import android.view.View;

import androidx.databinding.ObservableBoolean;
import grand.app.moonshop.base.ParentViewModel;
import grand.app.moonshop.models.settings.Settings;

public class ItemSettingsViewModel extends ParentViewModel {
    public Settings model = null;
    public String image;
    public int position = 0;
    public ObservableBoolean messageVisible = new ObservableBoolean(false);

    public ItemSettingsViewModel(Settings model, int position) {
        this.model = model;
        this.position = position;
    }

    private static final String TAG = "ItemPrivacyViewModel";
    public int rotation = 0;

    public void submit(View view) {
        rotation += 180;
        view.animate().rotation(rotation).setDuration(200).start();
        messageVisible.set(!messageVisible.get());
        notifyChange();
    }
}
