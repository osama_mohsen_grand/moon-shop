package grand.app.moonshop.viewmodels.reservation;


import androidx.databinding.ObservableBoolean;
import grand.app.moonshop.R;
import grand.app.moonshop.base.ParentViewModel;
import grand.app.moonshop.models.reservation.doctor.order.ReservationOrderActionRequest;
import grand.app.moonshop.repository.ReservationRepository;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.utils.resources.ResourceManager;
import grand.app.moonshop.utils.storage.user.UserHelper;
import timber.log.Timber;

public class ReservationDetailsViewModel extends ParentViewModel {
    private ReservationRepository reservationRepository;
    private String specialist = "";
    private String price="";
    public ObservableBoolean isClinic = new ObservableBoolean(true);

    public ReservationDetailsViewModel(int order_id) {
        reservationRepository = new ReservationRepository(mMutableLiveData);
        reservationRepository.getReservationDetails(order_id);
        Timber.e("type:"+UserHelper.getUserDetails().type);
        if(UserHelper.getUserDetails().type.equals(Constants.TYPE_RESERVATION_BEAUTY)){
            isClinic.set(false);
        }
    }

    public ReservationRepository getReservationRepository() {
        return reservationRepository;
    }

    public String getSpecialist() {
        return specialist;
    }

    public void setSpecialist(String specialist) {
        this.specialist = specialist;
    }

    public void finish(){
        ReservationOrderActionRequest reservationOrderActionRequest = new ReservationOrderActionRequest();
        reservationOrderActionRequest.status = 2;
        reservationOrderActionRequest.order_id = reservationRepository.getReservationDetailsResponse().reservationDetailsModel.orderId;
        reservationRepository.reservationOrderAction(reservationOrderActionRequest);
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price+" "+UserHelper.retrieveCurrency();
    }


    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
    }
}
