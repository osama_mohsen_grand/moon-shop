
package grand.app.moonshop.viewmodels.packagePayment;

import grand.app.moonshop.base.ParentViewModel;
import grand.app.moonshop.models.packagePayment.AddPackageRequest;
import grand.app.moonshop.repository.CategoryRepository;
import grand.app.moonshop.repository.PackageRepository;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.utils.storage.user.UserHelper;


/**
 * Created by Gregory Rasmussen on 7/26/17.
 */
public class PackageViewModel extends ParentViewModel {
    private PackageRepository packageRepository;

    public PackageViewModel() {
        packageRepository = new PackageRepository(mMutableLiveData);
        packageRepository.getPackages();
    }

    public String getImageUrl(){
        return UserHelper.getUserDetails().image;
    }

    public void addPackage(int id){
        AddPackageRequest packageRequest = new AddPackageRequest(id);
        packageRepository.addPackage(packageRequest);

    }

    public PackageRepository getPackageRepository() {
        return packageRepository;
    }

    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
    }
}
