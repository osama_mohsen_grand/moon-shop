package grand.app.moonshop.viewmodels.famous;

import grand.app.moonshop.base.ParentViewModel;
import grand.app.moonshop.models.famous.details.Product;

public class ItemImageViewModel extends ParentViewModel {
    public Product product = null;
    public String image;
    public int position = 0;

    public ItemImageViewModel(Product product, int position) {
        this.product = product;
        this.position = position;
        notifyChange();
    }

    public String getImageUrl(){
        return product.image;
    }

    public void imageSubmit(){
        mMutableLiveData.setValue(position);
    }
}
