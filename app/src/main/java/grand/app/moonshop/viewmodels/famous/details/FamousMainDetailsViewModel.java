
package grand.app.moonshop.viewmodels.famous.details;

import androidx.databinding.ObservableBoolean;
import grand.app.moonshop.R;
import grand.app.moonshop.base.ParentViewModel;
import grand.app.moonshop.repository.FamousRepository;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.utils.resources.ResourceManager;
import timber.log.Timber;


/**
 * Created by Gregory Rasmussen on 7/26/17.
 */
public class FamousMainDetailsViewModel extends ParentViewModel {

    public int category_delete_position;
    public String category_type;
    private FamousRepository famousRepository;

    public FamousMainDetailsViewModel() {
        famousRepository = new FamousRepository(mMutableLiveData);
        callService();
    }

    public void callService(){
        famousRepository.home("1");
    }

    public void addAlbum(){
        mMutableLiveData.setValue(Constants.ADD_ALBUM);
    }

    public FamousRepository getFamousRepository() {
        return famousRepository;
    }


    public void delete(int id){
        famousRepository.deleteAlbum(id);
    }

    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
    }

}
