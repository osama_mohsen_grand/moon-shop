package grand.app.moonshop.viewmodels.notification;


import grand.app.moonshop.base.ParentViewModel;
import grand.app.moonshop.models.notifications.Notification;

public class ItemNotificationViewModel extends ParentViewModel {
    public Notification notification = null;
    public String image;
    public int position = 0;

    public ItemNotificationViewModel(Notification notification, int position) {
        this.notification = notification;
        this.position = position;
        notifyChange();
    }

    public void notificationSubmit(){
        mMutableLiveData.setValue(position);
    }
}
