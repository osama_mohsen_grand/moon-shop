package grand.app.moonshop.viewmodels.ads;


import android.widget.RelativeLayout;

import androidx.databinding.BindingAdapter;
import androidx.databinding.ObservableBoolean;
import grand.app.moonshop.R;
import grand.app.moonshop.base.ParentViewModel;
import grand.app.moonshop.models.app.Mutable;
import grand.app.moonshop.models.base.IdNameDescription;
import grand.app.moonshop.models.famous.home.ImageVideo;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.utils.resources.ResourceManager;
import grand.app.moonshop.vollyutils.AppHelper;

public class ItemFilterAdsViewModel extends ParentViewModel {
    public ObservableBoolean selected;
    public int position = 0;
    public IdNameDescription data;

    public ItemFilterAdsViewModel(IdNameDescription data, int position,boolean isSelected) {
        this.position = position;
        this.data = data;
        selected = new ObservableBoolean(isSelected);
    }

    public void submit(){
        mMutableLiveData.setValue(new Mutable(Constants.SUBMIT,position));
    }
}
