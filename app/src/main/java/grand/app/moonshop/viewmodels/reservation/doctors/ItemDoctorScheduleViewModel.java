package grand.app.moonshop.viewmodels.reservation.doctors;

import android.widget.CompoundButton;

import androidx.appcompat.widget.SwitchCompat;
import androidx.databinding.BindingAdapter;
import grand.app.moonshop.base.ParentViewModel;
import grand.app.moonshop.models.app.Mutable;
import grand.app.moonshop.models.reservation.doctor.schedule.Day;
import grand.app.moonshop.utils.Constants;
import timber.log.Timber;


public class ItemDoctorScheduleViewModel extends ParentViewModel {

    public Day day;
    public int position;

    public ItemDoctorScheduleViewModel(Day day, int position) {
        this.day = day;
        this.position = position;
    }

    public void changeButton() {
        mMutableLiveData.setValue(new Mutable((day.schedules.size() > 0 ? Constants.DOCTOR_SCHEDULE_SUBMIT : Constants.DOCTOR_SCHEDULE), position, (day._switch ? Constants.FALSE : Constants.TRUE)));
    }

    public void edit() {
        mMutableLiveData.setValue(new Mutable(Constants.DOCTOR_SCHEDULE, position, Constants.EDIT));
    }

    @BindingAdapter("changeCheck")
    public static void loadImage(SwitchCompat switchCompat, boolean checked) {
        switchCompat.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                switchCompat.setChecked(isChecked);
            }
        });
    }

    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
    }
}
