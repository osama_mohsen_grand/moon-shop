package grand.app.moonshop.viewmodels.profile;


import android.text.InputType;
import android.view.View;
import android.widget.RelativeLayout;

import androidx.databinding.BindingAdapter;
import androidx.databinding.ObservableBoolean;
import grand.app.moonshop.R;
import grand.app.moonshop.base.IAnimationSubmit;
import grand.app.moonshop.base.ParentViewModel;
import grand.app.moonshop.customviews.views.CustomEditText;
import grand.app.moonshop.models.app.AppMoon;
import grand.app.moonshop.models.app.Mutable;
import grand.app.moonshop.models.user.info.Profile;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.utils.resources.ResourceManager;
import grand.app.moonshop.vollyutils.AppHelper;
import libs.mjn.scaletouchlistener.ScaleTouchListener;

public class ItemSocialViewModel extends ParentViewModel {
    public Profile model;
    public int position = 0;
    public ObservableBoolean editable;

    public ItemSocialViewModel(Profile profile, int position,boolean editable) {
        this.position = position;
        this.model = profile;
        this.editable = new ObservableBoolean(editable);
    }


    public void submit(){
        mMutableLiveData.setValue(position);
    }

    public Profile getType(){
        return model;
    }

    @BindingAdapter("inputType")
    public static void animation(CustomEditText editText, Profile profile) {
        if(profile.type.equals("whats")){
            editText.setInputType(InputType.TYPE_CLASS_PHONE);
            editText.setHint(ResourceManager.getString(R.string.add_phone_with_country_code));
        }else
            editText.setInputType(InputType.TYPE_CLASS_TEXT);
    }
}
