
package grand.app.moonshop.viewmodels.app;


import grand.app.moonshop.base.ParentViewModel;
import grand.app.moonshop.utils.helper.AppUtils;
import grand.app.moonshop.utils.Constants;


/**
 * Created by Gregory Rasmussen on 7/26/17.
 */
public class NoConnectionViewModel extends ParentViewModel {

    public void reload() {
        if (AppUtils.isNetworkAvailable()) {
            mMutableLiveData.setValue(Constants.RELOAD);
        } else {
            mMutableLiveData.setValue(Constants.FAILURE_CONNECTION);
        }
    }

    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
    }

}
