package grand.app.moonshop.viewmodels.sizeColor;


import android.util.Log;
import android.view.View;

import androidx.databinding.ObservableBoolean;
import grand.app.moonshop.base.ParentViewModel;
import grand.app.moonshop.models.app.Mutable;
import grand.app.moonshop.models.base.IdName;
import grand.app.moonshop.utils.Constants;

public class ItemSizeColorViewModel extends ParentViewModel {
    public IdName idName = null;
    public int position = 0;
    public ObservableBoolean isColor;

    private static final String TAG = "ItemSizeColorViewModel";

    public ItemSizeColorViewModel(IdName idName, int position, boolean isColor) {
        this.idName = idName;
        this.position = position;
        Log.d(TAG,"isColor:"+true);
        Log.d(TAG,"idName:"+idName.name);
        this.isColor = new ObservableBoolean(isColor);
    }

    public String getColor(){
        return idName.name;
    }

    public void submit(){
        if(isColor.get())
            mMutableLiveData.setValue(new Mutable(Constants.COLOR,position));
        else
            mMutableLiveData.setValue(new Mutable(Constants.SIZE,position));
    }
    public void deleteSubmit(){
        mMutableLiveData.setValue(new Mutable(Constants.DELETE,position));
    }

}
