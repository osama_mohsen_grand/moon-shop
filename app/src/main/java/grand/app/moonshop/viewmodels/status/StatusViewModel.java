
package grand.app.moonshop.viewmodels.status;

import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import androidx.databinding.ObservableBoolean;
import grand.app.moonshop.base.ParentViewModel;
import grand.app.moonshop.repository.CountryRepository;
import grand.app.moonshop.repository.StatusRepository;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.utils.upload.RequestBodyUtil;
import grand.app.moonshop.vollyutils.VolleyFileObject;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by Gregory Rasmussen on 7/26/17.
 */
public class StatusViewModel extends ParentViewModel {

    private StatusRepository statusRepository;
    public int item_delete_position = -1;

    public StatusViewModel() {
        statusRepository = new StatusRepository(mMutableLiveData);
        statusRepository.getStatus();
    }

    public void addStatus() {
        mMutableLiveData.setValue(Constants.ADD);
    }

    public StatusRepository getStatusRepository() {
        return statusRepository;
    }

    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
    }

    public void setImage(ArrayList<VolleyFileObject> volleyFileObjects, int type , int duration) {
        statusRepository.addStatus(volleyFileObjects,type,duration);

//        OkHttpClient client = new OkHttpClient();
//
//        MediaType MEDIA_TYPE_MARKDOWN
//                = MediaType.parse("text/x-markdown; charset=utf-8");
//
//        InputStream inputStream = getAssets().open("README.md");
//
//        RequestBody requestBody = RequestBodyUtil.create(MEDIA_TYPE_MARKDOWN, inputStream);
//        Request request = new Request.Builder()
//                .url("https://api.github.com/markdown/raw")
//                .post(requestBody)
//                .build();
//
//        Response response = null;
//        try {
//            response = client.newCall(request).execute();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        if (!response.isSuccessful()){
//
//        }

//        Log.d("POST", response.body().string());


    }
}
