package grand.app.moonshop.viewmodels.institution;


import grand.app.moonshop.base.ParentViewModel;
import grand.app.moonshop.models.shop.ShopDetails;
import grand.app.moonshop.repository.ProductRepository;
import grand.app.moonshop.utils.Constants;

public class InstitutionDetailsViewModel extends ParentViewModel {
    private ProductRepository productRepository;
    public ShopDetails shopDetails;
    public InstitutionDetailsViewModel(ShopDetails shopDetails) {
        productRepository = new ProductRepository(mMutableLiveData);
        this.shopDetails = shopDetails;
    }

    public void editSubmit(){
        mMutableLiveData.setValue(Constants.EDIT);
    }

}
