package grand.app.moonshop.viewmodels.famous;


import java.sql.Time;

import grand.app.moonshop.R;
import grand.app.moonshop.base.ParentViewModel;
import grand.app.moonshop.models.famous.home.ImageVideo;
import grand.app.moonshop.models.famous.search.ShopSearchResult;
import grand.app.moonshop.utils.resources.ResourceManager;
import timber.log.Timber;

public class ItemShopSearchViewModel extends ParentViewModel {
    public ShopSearchResult shopSearchResult;
    public int position;

    public ItemShopSearchViewModel(ShopSearchResult shopSearchResult, int position) {
        this.position = position;
        this.shopSearchResult = shopSearchResult;
    }


    public void submit() {
        mMutableLiveData.setValue(position);
    }
}
