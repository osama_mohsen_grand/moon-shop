package grand.app.moonshop.viewmodels.status;


import android.widget.ImageView;

import com.bumptech.glide.Glide;

import androidx.databinding.BindingAdapter;
import grand.app.moonshop.R;
import grand.app.moonshop.base.ParentViewModel;
import grand.app.moonshop.models.app.Mutable;
import grand.app.moonshop.models.home.Datum;
import grand.app.moonshop.models.status.Status;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.utils.images.ImageLoaderHelper;
import grand.app.moonshop.utils.resources.ResourceManager;

public class ItemStatusViewModel extends ParentViewModel {
    public Status status = null;
    public int position = 0;
    public String type= "";

    public ItemStatusViewModel(Status status, int position) {
        this.status = status;
        this.position = position;
        if(status.type == 1){
            type = "("+ResourceManager.getString(R.string.image)+")";
        }else
            type = "("+ResourceManager.getString(R.string.video)+")";
    }

    public Status getImageUrlStatus(){
        return status;
    }


    @BindingAdapter("imageUrlStatus")
    public static void loadImage(ImageView imageView, Status status) {
        if(!status.mImage.equals("")) {
//            if(status.type == 1){
//                ImageLoaderHelper.ImageLoaderLoad(imageView.getContext(), status.mImage, imageView);
//            }else {
                Glide.with(imageView.getContext())
                        .load(status.mImage)
                        .placeholder(R.drawable.ic_video_default)
                        .into(imageView);


//            }
        }else{
            imageView.setImageResource(R.drawable.ic_logo_original);
        }
    }

    public void deleteSubmit(){
        mMutableLiveData.setValue(new Mutable(Constants.DELETE,position));
    }

    public void submit(){
        mMutableLiveData.setValue(new Mutable(Constants.SUBMIT,position));
    }
}
