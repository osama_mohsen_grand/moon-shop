package grand.app.moonshop.viewmodels.country;

import grand.app.moonshop.base.ParentViewModel;
import grand.app.moonshop.models.country.Datum;
import grand.app.moonshop.utils.Constants;
import timber.log.Timber;


public class ItemCountryViewModel extends ParentViewModel {
    private Datum country = null;
    private int position = 0;
    private boolean selected;
    public ItemCountryViewModel(Datum country, int position,boolean selected) {
        this.country = country;
        this.position = position;
        this.selected = selected;
    }

    public void submit(){
        mMutableLiveData.setValue(position);
    }

    public String getImageUrl(){
        return country.countryImage;
    }

    public Datum getCountry() {
        return country;
    }

    public boolean isSelected() {
        return selected;
    }
}
