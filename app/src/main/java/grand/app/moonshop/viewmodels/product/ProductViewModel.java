package grand.app.moonshop.viewmodels.product;


import grand.app.moonshop.base.ParentViewModel;
import grand.app.moonshop.repository.ProductRepository;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.utils.storage.user.UserHelper;

public class ProductViewModel extends ParentViewModel {

    private ProductRepository productRepository;
    public String price = "";

    public ProductViewModel(int id) {
        productRepository = new ProductRepository(mMutableLiveData);
        productRepository.getProductDetails(id);
    }
    public void setPrice(){
        this.price = productRepository.getProductDetailsResponse().mData.mPrice + " "+ UserHelper.retrieveCurrency();
    }

    public String getImageUrl(){
        String image = "";
        if(productRepository.getProductDetailsResponse() != null)
            image = productRepository.getProductDetailsResponse().mData.image;
        return image;
    }

    public void zoom(){
        mMutableLiveData.setValue(Constants.ZOOM);
    }

    public void editSubmit(){
        mMutableLiveData.setValue(Constants.EDIT);
    }

    public void deleteSubmit(){
        mMutableLiveData.setValue(Constants.DELETE);
    }

    public ProductRepository getProductRepository() {
        return productRepository;
    }

    public void delete(int id) {
        productRepository.delete(id);
    }
}
