
package grand.app.moonshop.viewmodels.user;

import androidx.databinding.ObservableField;
import grand.app.moonshop.base.ParentViewModel;
import grand.app.moonshop.customviews.facebook.FacebookModel;
import grand.app.moonshop.models.user.login.FacebookLoginRequest;
import grand.app.moonshop.models.user.login.LoginRequest;
import grand.app.moonshop.repository.LoginRepository;
import grand.app.moonshop.repository.ServiceRepository;
import grand.app.moonshop.utils.Constants;

/**
 * Created by Gregory Rasmussen on 7/26/17.
 */
public class LoginViewModel extends ParentViewModel {
    public LoginRequest loginRequest ;
    LoginRepository loginRepository;
    public FacebookModel facebookModel;
    public static ObservableField<String> type = new ObservableField<>("");
    public ServiceRepository serviceRepository;

    public LoginViewModel() {
        loginRequest = new LoginRequest();
        loginRepository = new LoginRepository(mMutableLiveData);
        serviceRepository = new ServiceRepository(mMutableLiveData);
    }

    public void loginSubmit() {
        if(loginRequest.isValid()) {
            loginRepository.loginUser(loginRequest);
        }
    }

    public void getServices(int type_id,int flag,int type) {
        serviceRepository.setMessage(-1, "");
        serviceRepository.getServices(type_id,flag,type);
    }


    public void loginFacebookSubmit(){
        mMutableLiveData.setValue(Constants.FACEBOOK);
    }

    public void signUp() {

        mMutableLiveData.setValue(Constants.REGISTRATION);
    }

    public void forgetPassword() {
        mMutableLiveData.setValue(Constants.FORGET_PASSWORD);
    }

    public LoginRepository getLoginRepository() {
        return loginRepository;
    }

    public void socialLogin(FacebookModel facebookModel) {
        this.facebookModel = facebookModel;
        loginRepository.checkSocialIdExist(new FacebookLoginRequest(facebookModel.social_id));
    }

    public ServiceRepository getServiceRepository() {
        return serviceRepository;
    }


    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
    }

}
