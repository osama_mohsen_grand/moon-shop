package grand.app.moonshop.viewmodels.famous;

import android.widget.ImageView;

import androidx.databinding.BindingAdapter;
import androidx.databinding.ObservableBoolean;
import grand.app.moonshop.base.ParentViewModel;
import grand.app.moonshop.models.famous.details.Service;
import grand.app.moonshop.utils.images.ImageLoaderHelper;
import timber.log.Timber;

public class ItemFamousDetailsCategoryViewModel extends ParentViewModel {
    public Service service = null;
    public ObservableBoolean selected = new ObservableBoolean(false);
    public int position;

    public ItemFamousDetailsCategoryViewModel(Service service, int position,boolean selected) {
        Timber.e(""+service.name);
        this.service = service;
        this.position = position;
        this.selected.set(selected);
    }
    public String getImageUrl() {
        return service.image;
    }
    
    public void famousDetailsCategorySubmit(){
        mMutableLiveData.setValue(position);
    }
}
