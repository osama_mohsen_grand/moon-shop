package grand.app.moonshop.viewmodels.offer;


import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import androidx.databinding.BindingAdapter;
import androidx.databinding.ObservableBoolean;
import grand.app.moonshop.R;
import grand.app.moonshop.base.ParentViewModel;
import grand.app.moonshop.models.app.Mutable;
import grand.app.moonshop.models.base.IdNameImage;
import grand.app.moonshop.models.home.Datum;
import grand.app.moonshop.models.offer.Offer;
import grand.app.moonshop.models.status.Status;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.utils.images.ImageLoaderHelper;
import grand.app.moonshop.utils.storage.user.UserHelper;

public class ItemOfferViewModel extends ParentViewModel {
    public Offer offer = null;
    public int position = 0;
    public ObservableBoolean play,allowEdit;

    public ItemOfferViewModel(Offer offer, int position) {
        this.offer = offer;
        this.position = position;
        play = new ObservableBoolean(false);
        allowEdit = new ObservableBoolean(false);
        if (offer.capture != null && !offer.capture.equals(""))
            play.set(true);
        else
            play.set(false);

        if (UserHelper.getUserDetails().type.equals(Constants.TYPE_INSTITUTIONS))
            allowEdit.set(false);
    }

    public Offer getImageUrlStatus(){
        return offer;
    }

    @BindingAdapter("imageUrlStatus")
    public static void loadImage(ImageView imageView, Offer offer) {
        if(!offer.image.equals("")) {
            if(offer.mType == 2){
                Glide.with(imageView.getContext())
                        .load(offer.image)
                        .into(imageView);
            }else {
                ImageLoaderHelper.ImageLoaderLoad(imageView.getContext(), offer.image, imageView);
            }
        }else{
            imageView.setImageResource(R.drawable.ic_logo_original);
        }
    }


    public String getImageUrl() {
        if(offer.mType == 2)
            return offer.capture;
        return offer.image;
    }

    public void submit() {
        if (offer.capture != null && !offer.capture.equals(""))
            mMutableLiveData.setValue(new Mutable(Constants.VIDEO, position));
        else
            mMutableLiveData.setValue(new Mutable(Constants.IMAGE, position));
    }
    public void deleteSubmit(){
        mMutableLiveData.setValue(new Mutable(Constants.DELETE,position));
    }
}
