
package grand.app.moonshop.viewmodels.category;

import androidx.databinding.ObservableField;
import androidx.databinding.ObservableInt;
import grand.app.moonshop.base.ParentViewModel;
import grand.app.moonshop.models.category.IdNameRequest;
import grand.app.moonshop.models.rate.RateRequest;
import grand.app.moonshop.repository.CategoryRepository;
import timber.log.Timber;

/**
 * Created by Gregory Rasmussen on 7/26/17.
 */
public class AddCategoryViewModel extends ParentViewModel {
    public RateRequest rateRequest ;
    public IdNameRequest idNameRequest = new IdNameRequest();
    CategoryRepository categoryRepository;

    //tripDetailsResponse.data.user.name
    public static ObservableField<String> text = new ObservableField<>("");


    public AddCategoryViewModel(int id, String name) {
        if(id != -1 && !name.equals("")) {
            idNameRequest.id = id;
            idNameRequest.setName(name);
        }
        categoryRepository = new CategoryRepository(mMutableLiveData);
    }

    public void submit() {
        if(idNameRequest.isValid()){
            categoryRepository.addOrEdit(idNameRequest);
        }
    }

    public CategoryRepository getCategoryRepository() {
        return categoryRepository;
    }

    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
    }

}
