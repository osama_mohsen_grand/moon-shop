
package grand.app.moonshop.viewmodels.schedule;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.os.Build;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import androidx.annotation.RequiresApi;
import androidx.databinding.BindingAdapter;
import androidx.lifecycle.MutableLiveData;
import grand.app.moonshop.base.ParentViewModel;
import grand.app.moonshop.utils.helper.AppUtils;
import grand.app.moonshop.utils.Constants;


/**
 * Created by Gregory Rasmussen on 7/26/17.
 */


public class ScheduleViewModel extends ParentViewModel {
    public MutableLiveData<Object> mMutableScheduleLiveData = new MutableLiveData<>();

    public static String date_server = "";
    public static String time_server = "";
    public static String date_select = "";
    public static String time = "";

    public ScheduleViewModel() {
        date_select = DateFormat.getDateInstance(DateFormat.MEDIUM).format(new Date());
        time_server = time = android.text.format.DateFormat.format("hh:mm", new Date()).toString();
    }

    public String getTime() {
        return time;
    }

    public static DatePickerDialog datePickerDialog;
    public static TimePickerDialog timePickerDialog;

    @BindingAdapter("time")
    public static void setTime(TextView textView, String time) {
        textView.setText(time);
        textView.setOnClickListener(v -> {
            if (timePickerDialog == null) {
                Calendar mcurrentTime = Calendar.getInstance();
                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                int minute = mcurrentTime.get(Calendar.MINUTE);

                timePickerDialog = new TimePickerDialog(textView.getContext(), (timePicker, selectedHour, selectedMinute) -> {
                    textView.setText(AppUtils.numberToDecimal(selectedHour) + ":" + AppUtils.numberToDecimal(selectedMinute));
                    time_server = AppUtils.numberToDecimal(selectedHour) + ":" + AppUtils.numberToDecimal(selectedMinute);
                }, hour, minute, true);//Yes 24 hour time

            }
            timePickerDialog.show();
        });
    }

    public String getDate() {
        return date_select;
    }

    private static final String TAG = "ScheduleViewModel";

    @RequiresApi(api = Build.VERSION_CODES.O)
    @BindingAdapter("date")
    public static void setDate(TextView textView, String date) {
        textView.setText(date);
        textView.setOnClickListener(v -> {
            if (datePickerDialog == null) {
                datePickerDialog = AppUtils.initCalender(textView.getContext(), (datePicker, year, month, day) -> {

                    Calendar calendar = Calendar.getInstance();
                    calendar.set(year, month, day);
                    SimpleDateFormat format = new SimpleDateFormat("MM-dd-yyyy");
                    String strDate = format.format(calendar.getTime());
                    date_server = AppUtils.dateConvert(year, month, day);
                    try {
                        Date newDate = format.parse(strDate);
                        date_select = DateFormat.getDateInstance(DateFormat.MEDIUM).format(newDate);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    textView.setText(date_select);
                });
            }
            datePickerDialog.show();
        });
    }

    public void orderSubmit() {
        mMutableScheduleLiveData.setValue(Constants.SCHEDULE);
    }

    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
    }

}
