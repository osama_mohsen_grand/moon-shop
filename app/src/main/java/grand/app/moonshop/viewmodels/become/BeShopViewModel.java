
package grand.app.moonshop.viewmodels.become;

import java.util.ArrayList;
import java.util.Iterator;

import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;
import grand.app.moonshop.R;
import grand.app.moonshop.base.ParentViewModel;
import grand.app.moonshop.models.base.StatusMsg;
import grand.app.moonshop.models.become.BeShopRequest;
import grand.app.moonshop.models.user.register.RegisterShopRequest;
import grand.app.moonshop.models.user.register.VolleyFileObjectSerializable;
import grand.app.moonshop.repository.BecomeRepository;
import grand.app.moonshop.repository.CountryRepository;
import grand.app.moonshop.repository.RegisterRepository;
import grand.app.moonshop.repository.ServiceRepository;
import grand.app.moonshop.repository.VerificationFirebaseSMSRepository;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.utils.resources.ResourceManager;
import grand.app.moonshop.vollyutils.VolleyFileObject;

/**
 * Created by Gregory Rasmussen on 7/26/17.
 */
public class BeShopViewModel extends ParentViewModel {
    public BeShopRequest beShopRequest = new BeShopRequest();
    ServiceRepository serviceRepository;
    BecomeRepository becomeRepository;
    ArrayList<VolleyFileObject> volleyFileObjects = new ArrayList<>();


    public BeShopViewModel() {
        serviceRepository = new ServiceRepository(mMutableLiveData);
        becomeRepository = new BecomeRepository(mMutableLiveData);
    }


    public void getServices(int type_id,int flag,int type) {
        serviceRepository.getServices(type_id,flag,type);
    }

    public BecomeRepository getBecomeRepository() {
        return becomeRepository;
    }

    public ServiceRepository getServiceRepository() {
        return serviceRepository;
    }


    public void addShop() {
        if(volleyFileObjects.size() == 0){
            baseError = ResourceManager.getString(R.string.select_shop_image);
            mMutableLiveData.setValue(Constants.ERROR);
        }
        if (beShopRequest.isValid() && volleyFileObjects.size() > 0) {
            becomeRepository.beShop(beShopRequest,volleyFileObjects);
        }
    }
    public void selectImage() {
        mMutableLiveData.setValue(Constants.SELECT_IMAGE);
    }


    public void setImage(VolleyFileObject volleyFileObject) {
        this.volleyFileObjects.add(0,volleyFileObject);
    }

    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
    }

}
