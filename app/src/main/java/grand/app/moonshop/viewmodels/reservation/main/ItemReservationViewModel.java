package grand.app.moonshop.viewmodels.reservation.main;

import androidx.databinding.ObservableBoolean;
import grand.app.moonshop.R;
import grand.app.moonshop.base.ParentViewModel;
import grand.app.moonshop.models.app.Mutable;
import grand.app.moonshop.models.reservation.doctor.order.ReservationOrder;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.utils.resources.ResourceManager;
import grand.app.moonshop.utils.storage.user.UserHelper;


public class ItemReservationViewModel extends ParentViewModel {

    public ReservationOrder order;
    public int position;
    public ObservableBoolean isAllowAction = new ObservableBoolean(false), isBeauty = new ObservableBoolean(false);
    public String cost;

    public ItemReservationViewModel(ReservationOrder order, int position, String status) {
        this.order = order;
        this.position = position;
        if (status.equals("0"))
            this.isAllowAction.set(true);
        if (UserHelper.getUserDetails().type.equals(Constants.TYPE_RESERVATION_BEAUTY)) {
            this.isBeauty = new ObservableBoolean(true);
        }
        cost = order.cost + " " + UserHelper.retrieveCurrency();
        notifyChange();
    }

    public void delete() {
        mMutableLiveData.setValue(new Mutable(Constants.DELETE, position));
    }

    public void submit() {
//        if (UserHelper.getUserDetails().type.equals(Constants.TYPE_RESERVATION_BEAUTY))
        mMutableLiveData.setValue(new Mutable(Constants.SUBMIT, position));
    }

    public void confirm() {
        mMutableLiveData.setValue(new Mutable(Constants.CONFIRM, position));
    }

    public void edit() {
        mMutableLiveData.setValue(new Mutable(Constants.EDIT, position));
    }

    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
    }
}
