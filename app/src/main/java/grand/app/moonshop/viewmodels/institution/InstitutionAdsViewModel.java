
package grand.app.moonshop.viewmodels.institution;

import java.util.ArrayList;

import grand.app.moonshop.base.ParentViewModel;

import grand.app.moonshop.repository.InstitutionRepository;
import grand.app.moonshop.utils.Constants;

/**
 * Created by Gregory Rasmussen on 7/26/17.
 */
public class InstitutionAdsViewModel extends ParentViewModel {

    private InstitutionRepository institutionRepository;
    public String filter = "1";
    public int category_delete_position = -1;
    public InstitutionAdsViewModel(){
        institutionRepository = new InstitutionRepository(mMutableLiveData);
    }

    public void filterSubmit(){
        institutionRepository.getFamousList(filter);
    }

    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
    }

    public void getAds() {
        institutionRepository.getAds();
    }

    public InstitutionRepository getInstitutionRepository() {
        return institutionRepository;
    }

    public void selectFilter(ArrayList<Integer> list) {
        institutionRepository.submitFilter(list);
    }

    public void delete(int id) {
        institutionRepository.removeAlbumImage(id);
    }
}
