package grand.app.moonshop.viewmodels.famous;


import grand.app.moonshop.R;
import grand.app.moonshop.base.ParentViewModel;
import grand.app.moonshop.models.album.AlbumModel;
import grand.app.moonshop.models.app.Mutable;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.utils.resources.ResourceManager;

public class ItemAlbumModelViewModel extends ParentViewModel {
    public AlbumModel model;
    public int position = 0;
    public String imagesCount,videosCount,count;
    public boolean allowAction;

    public ItemAlbumModelViewModel(AlbumModel model, int position, boolean allowAction) {
        this.position = position;
        this.model = model;
        this.allowAction = allowAction;
        count = model.count+" "+ ResourceManager.getString(R.string.photos)+" - "+model.videoCount +" "+ResourceManager.getString(R.string.video);
//        imagesCount = model.imagesCount + " "+ResourceManager.getString(R.string.photos);
//        videosCount = model.videosCount + " "+ResourceManager.getString(R.string.video);
    }

    public void edit(){
        mMutableLiveData.setValue(new Mutable(Constants.EDIT,position));
    }

    public void delete(){
        mMutableLiveData.setValue(new Mutable(Constants.DELETE,position));
    }

    public void submit(){
        mMutableLiveData.setValue(new Mutable(Constants.SUBMIT,position));
    }
}
