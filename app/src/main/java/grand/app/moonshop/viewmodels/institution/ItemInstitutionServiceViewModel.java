package grand.app.moonshop.viewmodels.institution;

import grand.app.moonshop.base.ParentViewModel;
import grand.app.moonshop.models.app.Mutable;
import grand.app.moonshop.models.base.IdNameImage;
import grand.app.moonshop.utils.Constants;


public class ItemInstitutionServiceViewModel extends ParentViewModel {
    public IdNameImage idNameImage ;
    public int position = 0;

    public int width = 0;
    public boolean allowDelete = false;

    public ItemInstitutionServiceViewModel(IdNameImage idNameImage, int position,boolean allowDelete) {
        this.idNameImage = idNameImage;
        this.position = position;
        this.allowDelete = allowDelete;
        notifyChange();
    }

    public String getImageUrl(){
        return idNameImage.image;
    }

    public void submit(){
        mMutableLiveData.setValue(new Mutable(Constants.DELETE,position));
    }

    public void edit(){
        mMutableLiveData.setValue(new Mutable(Constants.EDIT,position));
    }
}
