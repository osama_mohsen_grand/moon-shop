package grand.app.moonshop.viewmodels.adsCompanyFilter;

import grand.app.moonshop.base.ParentViewModel;
import grand.app.moonshop.models.adsCompanyFilter.AdsCompanyFilter;


public class ItemAdsCompanyFilterViewModel extends ParentViewModel {
    public AdsCompanyFilter adsCompanyFilter = null;
    private int position = 0;
    public boolean multipleCheck = false;
    public ItemAdsCompanyFilterViewModel(AdsCompanyFilter adsCompanyFilter, int position,boolean multipleCheck) {
        this.adsCompanyFilter = adsCompanyFilter;
        this.position = position;
        this.multipleCheck = multipleCheck;
    }

    public void submit(){
        mMutableLiveData.setValue(position);
    }

    public String getImageUrl(){
        return adsCompanyFilter.image;
    }

    public AdsCompanyFilter getCountry() {
        return adsCompanyFilter;
    }
}
