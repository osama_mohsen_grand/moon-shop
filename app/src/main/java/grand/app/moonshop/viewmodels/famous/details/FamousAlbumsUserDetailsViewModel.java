
package grand.app.moonshop.viewmodels.famous.details;

import android.util.Log;
import android.widget.Toast;

import java.util.ArrayList;

import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;
import grand.app.moonshop.R;
import grand.app.moonshop.base.ParentViewModel;
import grand.app.moonshop.models.app.AppMoon;
import grand.app.moonshop.models.famous.album.FamousAddImageInsideAlbumRequest;
import grand.app.moonshop.repository.AdsRepository;
import grand.app.moonshop.repository.FamousRepository;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.utils.resources.ResourceManager;
import grand.app.moonshop.vollyutils.VolleyFileObject;
import timber.log.Timber;


/**
 * Created by Gregory Rasmussen on 7/26/17.
 */
public class FamousAlbumsUserDetailsViewModel extends ParentViewModel {
    public int category_delete_position;
    private FamousRepository famousRepository;
    private AdsRepository adsRepository;
    private int id;

    public FamousAlbumsUserDetailsViewModel(int id, int tab) {
        famousRepository = new FamousRepository(mMutableLiveData);
        adsRepository = new AdsRepository(mMutableLiveData);
        famousRepository.getFamousDetails(id,tab);
        this.id = id;
    }

    public void filter(){
        adsRepository.getShopAds("?id="+id+"&type=1");
    }

    public void submitFilter(ArrayList<Integer> ids){
        adsRepository.getShopAFilter(ids,id);
    }

    public FamousRepository getFamousRepository() {
        return famousRepository;
    }

    public AdsRepository getAdsRepository() {
        return adsRepository;
    }

    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
    }

    //https://moon-iet.com/api/famous/deleteImage/(ad_id)?account_type=\(account_type)&type=2
    public void delete(Integer id) {
        Timber.e("DELETE "+id);
//        adsRepository.deleteAds(id);
    }
}
