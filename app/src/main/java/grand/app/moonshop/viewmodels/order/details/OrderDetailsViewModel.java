package grand.app.moonshop.viewmodels.order.details;


import androidx.databinding.ObservableBoolean;

import grand.app.moonshop.R;
import grand.app.moonshop.base.ParentViewModel;
import grand.app.moonshop.models.OrderConfirmRequest;
import grand.app.moonshop.repository.OrderRepository;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.utils.resources.ResourceManager;
import grand.app.moonshop.utils.storage.user.UserHelper;

public class OrderDetailsViewModel extends ParentViewModel {
    private OrderRepository orderRepository;
    public boolean haveDelegate = false,allowChat = false;
    public ObservableBoolean showStatus ;
    private String subTotal = "", delivery = "", total = "",imageUrl = "",name="";

    public OrderConfirmRequest orderConfirmRequest;
    public Integer order_id;
    public OrderDetailsViewModel(int order_id) {
        showStatus = new ObservableBoolean(false);
        orderRepository = new OrderRepository(mMutableLiveData);
        orderRepository.getOrderDetails(order_id);
        orderConfirmRequest = new OrderConfirmRequest(order_id);
        this.order_id = order_id;
    }

    public String getSubTotal() {
        if (orderRepository.getOrderDetailsResponse() != null)
            subTotal = orderRepository.getOrderDetailsResponse().data.subTotal + " " + UserHelper.retrieveCurrency();
        return subTotal;
    }

    public String getImageUrl(){
        if (orderRepository.getOrderDetailsResponse() != null && haveDelegate)
            imageUrl = orderRepository.getOrderDetailsResponse().data.delegate.image;
        return imageUrl;
    }
    public String getName(){
        if (orderRepository.getOrderDetailsResponse() != null && haveDelegate)
            name = orderRepository.getOrderDetailsResponse().data.delegate.name;
        return name;
    }

    public String getDelivery() {
        if (orderRepository.getOrderDetailsResponse() != null)
            delivery = orderRepository.getOrderDetailsResponse().data.delivery + " " + UserHelper.retrieveCurrency();
        return delivery;
    }

    public String getTotal() {
        if (orderRepository.getOrderDetailsResponse() != null)
            total = orderRepository.getOrderDetailsResponse().data.totalPrice + " " + UserHelper.retrieveCurrency();
        return total;
    }

    public void chatSubmit(){
        mMutableLiveData.setValue(Constants.CHAT);
    }

    public void callSubmit(){
        mMutableLiveData.setValue(Constants.CALL);
    }

    public void delegateLocation(){
        mMutableLiveData.setValue(Constants.DELEGATE_LOCATION);
    }

    public OrderRepository getOrderRepository() {
        return orderRepository;
    }

    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
    }
    public void setStatusChat() {
        if(orderRepository.getOrderDetailsResponse().data.statusId < 2){
            allowChat = false;
        }else{
            allowChat = true;
        }
    }


    public void setShowStatus() {

        if(orderRepository.getOrderDetailsResponse().data.statusId < 4){
            this.showStatus.set(true);
        }else{
            this.showStatus.set(false);
        }

    }



    public void orderSubmit(int status){
        if(status != 0) {
            orderConfirmRequest.status = status;
        }
        orderRepository.confirmOrder(orderConfirmRequest);
    }


}
