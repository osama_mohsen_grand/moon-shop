package grand.app.moonshop.viewmodels.reservation.doctors;

import grand.app.moonshop.base.ParentViewModel;
import grand.app.moonshop.models.app.Mutable;
import grand.app.moonshop.models.reservation.doctor.DoctorListModel;
import grand.app.moonshop.utils.Constants;


public class ItemDoctorListViewModel extends ParentViewModel {

    public DoctorListModel doctor;
    public int position;
//    public ObservableBoolean imageVisible = new ObservableBoolean(true);
//    private int layout;
//
//    public Drawable backgroundColor;
//
    public ItemDoctorListViewModel(DoctorListModel doctor , int position) {
        this.doctor = doctor;
        this.position = position;
    }

    public void submit(){
        mMutableLiveData.setValue(new Mutable(Constants.CHAT_DETAILS,position));
    }

    public void delete(){
        mMutableLiveData.setValue(new Mutable(Constants.DELETE,position));
    }

    public void edit(){
        mMutableLiveData.setValue(new Mutable(Constants.EDIT,position));
    }

    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
    }
}
