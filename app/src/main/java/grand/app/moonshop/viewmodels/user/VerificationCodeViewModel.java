package grand.app.moonshop.viewmodels.user;

import java.util.ArrayList;

import grand.app.moonshop.base.ParentViewModel;
import grand.app.moonshop.models.user.activation.VerificationRequest;
import grand.app.moonshop.models.user.register.VolleyFileObjectSerializable;
import grand.app.moonshop.models.user.register.RegisterShopRequest;
import grand.app.moonshop.repository.RegisterRepository;
import grand.app.moonshop.repository.VerificationFirebaseSMSRepository;
import grand.app.moonshop.utils.Constants;


/**
 * Created by Gregory Rasmussen on 7/26/17.
 */
public class VerificationCodeViewModel extends ParentViewModel {

    public VerificationRequest activeRequest = new VerificationRequest();
    String verify_id;
    VerificationFirebaseSMSRepository verificationFirebaseSMSRepository;
    RegisterRepository registerRepository;
    public VerificationCodeViewModel(String verify_id) {
        this.verify_id = verify_id;
        verificationFirebaseSMSRepository = new VerificationFirebaseSMSRepository(mMutableLiveData);
        registerRepository = new RegisterRepository(mMutableLiveData);
    }

    public void verificationSubmit(){
        if(activeRequest.isValid()) {
            verificationFirebaseSMSRepository.verifyCode(verify_id,activeRequest.code);
        }
    }

    public VerificationFirebaseSMSRepository getVerificationFirebaseSMSRepository() {
        return verificationFirebaseSMSRepository;
    }

    public RegisterRepository getRegisterRepository() {
        return registerRepository;
    }

    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
    }


}