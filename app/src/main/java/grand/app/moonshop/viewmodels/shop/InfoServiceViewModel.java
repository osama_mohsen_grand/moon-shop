package grand.app.moonshop.viewmodels.shop;


import androidx.lifecycle.MutableLiveData;
import grand.app.moonshop.base.ParentViewModel;
import grand.app.moonshop.models.shop.ShopDetails;
import grand.app.moonshop.models.user.DescriptionRequest;
import grand.app.moonshop.repository.InstitutionRepository;
import grand.app.moonshop.repository.ProductRepository;
import grand.app.moonshop.repository.RegisterRepository;
import grand.app.moonshop.utils.Constants;

public class InfoServiceViewModel extends ParentViewModel {
    public InstitutionRepository institutionRepository;
    public int service_delete_position;


    public InfoServiceViewModel() {
        institutionRepository = new InstitutionRepository(mMutableLiveData);
        institutionRepository.getService();
    }

    public void submit(){
        if(!institutionRepository.shopDetailsResponse.shopData.shop_details.description.trim().equals("")){
            institutionRepository.updateDescription(new DescriptionRequest(institutionRepository.shopDetailsResponse.shopData.shop_details.description));
        }
    }

    public void addService(){
        mMutableLiveData.setValue(Constants.ADD_SERVICE);
    }

    public void delete(int id) {
        institutionRepository.removeBranch(id,2);
    }
}
