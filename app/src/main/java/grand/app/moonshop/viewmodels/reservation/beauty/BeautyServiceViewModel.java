
package grand.app.moonshop.viewmodels.reservation.beauty;

import grand.app.moonshop.base.ParentViewModel;
import grand.app.moonshop.repository.CategoryRepository;
import grand.app.moonshop.repository.ReservationRepository;
import grand.app.moonshop.utils.Constants;


/**
 * Created by Gregory Rasmussen on 7/26/17.
 */
public class BeautyServiceViewModel extends ParentViewModel {
    private ReservationRepository reservationRepository;
    public int category_delete_position = -1;

    public BeautyServiceViewModel() {
        reservationRepository = new ReservationRepository(mMutableLiveData);
        reservationRepository.beautyServices();
    }

    public void addService(){
        mMutableLiveData.setValue(Constants.ADD);
    }


    public ReservationRepository getReservationRepository() {
        return reservationRepository;
    }
}
