package grand.app.moonshop.viewmodels.review;


import grand.app.moonshop.base.ParentViewModel;
import grand.app.moonshop.models.review.Review;

public class ItemReviewViewModel extends ParentViewModel {
    public Review review = null;
    public String image;
    public int position = 0;

    public ItemReviewViewModel(Review review, int position) {
        this.review = review;
        this.position = position;
        notifyChange();
    }

    public void reviewSubmit(){
        mMutableLiveData.setValue(position);
    }
}
