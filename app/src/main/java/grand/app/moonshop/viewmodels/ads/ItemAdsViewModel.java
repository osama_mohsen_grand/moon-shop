package grand.app.moonshop.viewmodels.ads;


import android.widget.RelativeLayout;

import androidx.databinding.BindingAdapter;
import androidx.databinding.ObservableBoolean;
import grand.app.moonshop.base.ParentViewModel;
import grand.app.moonshop.models.app.Mutable;
import grand.app.moonshop.models.base.IdNameImage;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.utils.storage.user.UserHelper;
import grand.app.moonshop.vollyutils.AppHelper;

public class ItemAdsViewModel extends ParentViewModel {
    public IdNameImage idNameImage = null;
    public int position = 0;
    public ObservableBoolean play,allowEdit;

    public ItemAdsViewModel(IdNameImage idNameImage, int position) {
        this.idNameImage = idNameImage;
        this.position = position;
        play = new ObservableBoolean(false);
        allowEdit = new ObservableBoolean(false);
        if (idNameImage.capture != null && !idNameImage.capture.equals(""))
            play.set(true);
        else
            play.set(false);

        if (UserHelper.getUserDetails().type.equals(Constants.TYPE_INSTITUTIONS))
            allowEdit.set(false);
    }

    public String getImageUrl() {
        if(idNameImage.capture != null && !idNameImage.capture.equals(""))
            return idNameImage.capture;
        return idNameImage.image;
    }

    public void submit() {
        if (idNameImage.capture != null && !idNameImage.capture.equals(""))
            mMutableLiveData.setValue(new Mutable(Constants.VIDEO, position));
        else
            mMutableLiveData.setValue(new Mutable(Constants.IMAGE, position));
    }
    public void deleteSubmit(){
        mMutableLiveData.setValue(new Mutable(Constants.DELETE,position));
    }

    public void video() {
        mMutableLiveData.setValue(new Mutable(Constants.VIDEO, position));
    }


    public int getWidth() {
        return 0;
    }

    @BindingAdapter("width")
    public static void setWidth(RelativeLayout relativeLayout, int w) {
        int width = AppHelper.getScreenWidth(relativeLayout.getContext()) / 3;
        relativeLayout.getLayoutParams().width = width;
    }

}
