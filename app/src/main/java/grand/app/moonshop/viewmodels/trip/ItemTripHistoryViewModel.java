package grand.app.moonshop.viewmodels.trip;


import android.widget.RatingBar;

import androidx.databinding.Bindable;
import grand.app.moonshop.base.ParentViewModel;
import grand.app.moonshop.models.triphistory.TripHistoryResponse;


public class ItemTripHistoryViewModel extends ParentViewModel {
    public TripHistoryResponse.Datum trips = null;
    public String image;
    public int position = 0;

    public ItemTripHistoryViewModel(TripHistoryResponse.Datum trips, int position) {
        this.trips = trips;
        this.position = position;
        notifyChange();
    }

    @Bindable
    public Float getRating(){
        return trips.rate;
    }


    public static void setRating(RatingBar rating , Float rate){
        rating.setRating(rate);
    }
}
