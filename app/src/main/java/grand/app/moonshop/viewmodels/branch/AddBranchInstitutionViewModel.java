
package grand.app.moonshop.viewmodels.branch;

import grand.app.moonshop.R;
import grand.app.moonshop.base.ParentViewModel;
import grand.app.moonshop.models.institution.AddBranchRequest;
import grand.app.moonshop.models.offer.AddOfferRequest;
import grand.app.moonshop.repository.InstitutionRepository;
import grand.app.moonshop.repository.OfferRepository;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.utils.maputils.base.MapConfig;
import grand.app.moonshop.utils.resources.ResourceManager;
import grand.app.moonshop.vollyutils.MyApplication;
import grand.app.moonshop.vollyutils.VolleyFileObject;

/**
 * Created by Gregory Rasmussen on 7/26/17.
 */
public class AddBranchInstitutionViewModel extends ParentViewModel {
    private InstitutionRepository institutionRepository;
    public AddBranchRequest addBranchRequest;
    public AddBranchInstitutionViewModel() {
        addBranchRequest = new AddBranchRequest();
        institutionRepository = new InstitutionRepository(mMutableLiveData);
    }

    public void submit() {
        if(addBranchRequest.isValid() && addBranchRequest.volleyFileObject != null){
            institutionRepository.addBranch(addBranchRequest);
        }else if(addBranchRequest.isValid() && addBranchRequest.volleyFileObject == null){
            baseError = ResourceManager.getString(R.string.select_add_branch_photo);
            mMutableLiveData.setValue(Constants.ERROR);
        }
    }

    public void selectImage(){
        mMutableLiveData.setValue(Constants.IMAGE);
    }

    public void addressSubmit(){
        mMutableLiveData.setValue(Constants.LOCATION);
    }


    public void setAddress(double lat, double lng) {
        addBranchRequest.setLat(lat);
        addBranchRequest.setLng(lng);
        addBranchRequest.setAddress(new MapConfig(MyApplication.getInstance(),null).getAddress(lat,lng));
        notifyChange();
    }
    
    
    public InstitutionRepository getInstitutionRepository() {
        return institutionRepository;
    }

    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
    }

}
