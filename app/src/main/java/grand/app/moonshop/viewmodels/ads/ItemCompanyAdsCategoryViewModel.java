package grand.app.moonshop.viewmodels.ads;


import grand.app.moonshop.R;
import grand.app.moonshop.base.ParentViewModel;
import grand.app.moonshop.models.album.Category;
import grand.app.moonshop.utils.resources.ResourceManager;

public class ItemCompanyAdsCategoryViewModel extends ParentViewModel {
    public Category model ;
    public int position = 0;
    public boolean selected = false;
    public int width = 0;
    public String count;

    public ItemCompanyAdsCategoryViewModel(Category model, int position) {
        this.model = model;
        this.position = position;
        count = model.shopsCount + " "+ResourceManager.getString(R.string.shop);
    }

    public void submit(){
        mMutableLiveData.setValue(position);
    }
}
