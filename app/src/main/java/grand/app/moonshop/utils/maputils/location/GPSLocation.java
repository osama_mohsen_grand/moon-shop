package grand.app.moonshop.utils.maputils.location;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.IntentSender;
import android.location.Location;
import android.location.LocationManager;
import android.util.Log;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;

public class GPSLocation {

    private static final String TAG = "GPSLocation";

    public static GoogleApiClient googleApiClient = null;

    public static PendingResult<LocationSettingsResult> addGoogleAPI(final Activity context) {

        googleApiClient = new GoogleApiClient.Builder(context)
                .addApi(LocationServices.API)
//                    .addConnectionCallbacks((GoogleApiClient.ConnectionCallbacks) context)
//                    .addOnConnectionFailedListener((GoogleApiClient.OnConnectionFailedListener) context)
                .addConnectionCallbacks((GoogleApiClient.ConnectionCallbacks) context)
                .addOnConnectionFailedListener((GoogleApiClient.OnConnectionFailedListener) context).build();
        googleApiClient.connect();
        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(1000);
        locationRequest.setFastestInterval(1000);
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest);
        builder.setAlwaysShow(true); // this is the key ingredient
        PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi
                .checkLocationSettings(googleApiClient, builder.build());
        return result;

    }

    public static void EnableGPSAutoMatically(final Activity context, final GPSAllowListener gpsAllowListener) {
        Log.e(TAG, "EnableGPSAutoMatically: ");
        PendingResult<LocationSettingsResult> result = addGoogleAPI(context);
        Log.e("gps", "enable_gps_auto");
        if (result != null) {
            Log.e("result", "null");
            result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
                @Override
                public void onResult(LocationSettingsResult result) {
                    final Status status = result.getStatus();
                    Log.e("status", "status");
                    final LocationSettingsStates state = result
                            .getLocationSettingsStates();
                    Log.e("update_location", "start here");

                    switch (status.getStatusCode()) {
                        case LocationSettingsStatusCodes.SUCCESS:
                            Log.e("start_update_location", "SUCCESS");
                            gpsAllowListener.GPSStatus(true);
//                            startService(new Intent(MapRequestOrderActivity.this, SendLocationService.class));
                            break;
                        case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                            try {
                                Log.e("start_update_location", "RESOLUTION_REQUIRED");
                                gpsAllowListener.GPSStatus(true);
                                status.startResolutionForResult((Activity) context, 1000);
                            } catch (IntentSender.SendIntentException e) {
                                // Ignore the error.
                            }
                            break;
                        case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                            Log.e("un_avaliable", "unavaliable");
                            gpsAllowListener.GPSStatus(false);
                            // LocationPosition settings are not satisfied. However, we have
                            // no way to fix the
                            // settings so we won't show the dialog.
                            break;
                    }
                }
            });
        }
    }


    public static boolean checkGps(Context context) {

        LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        return (locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER) && locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER));
    }

    @SuppressLint("MissingPermission")
    public static Location getLocation(final Activity context) {
        LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        Location locationGPS = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        Location locationNet = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

        long GPSLocationTime = 0;
        if (null != locationGPS) {
            GPSLocationTime = locationGPS.getTime();
        }

        long NetLocationTime = 0;

        if (null != locationNet) {
            NetLocationTime = locationNet.getTime();
        }

        if (0 < GPSLocationTime - NetLocationTime) {
            return locationGPS;
        } else {
            return locationNet;
        }

    }
}
