package grand.app.moonshop.utils.maputils.direction;

public interface DirectionDistance {
    public void receive(int distanceM);
}
