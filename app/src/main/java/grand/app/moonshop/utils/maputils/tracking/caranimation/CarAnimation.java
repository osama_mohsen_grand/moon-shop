package grand.app.moonshop.utils.maputils.tracking.caranimation;

import android.animation.ValueAnimator;
import android.app.Activity;
import android.view.animation.LinearInterpolator;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.jakewharton.rxrelay2.PublishRelay;

import java.util.List;

import grand.app.moonshop.R;
import grand.app.moonshop.utils.maputils.base.MapConfig;

public class CarAnimation {
    private static final String TAG = "CarAnimation";
    private Activity activity;
    public int emission = 0;
    public MapConfig mapConfig;
    public Marker marker;
    private PublishRelay<LatLng> latLngPublishRelay = PublishRelay.create();

    private float v;

    public CarAnimation(Activity activity , MapConfig mapConfig, Marker marker) {
        this.activity = activity;
        this.mapConfig = mapConfig;
        this.marker = marker;
    }

    public void animateCarOnMap(final List<LatLng> latLngs) {
        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        for (LatLng latLng : latLngs) {
            builder.include(latLng);
        }
        LatLngBounds bounds = builder.build();
        CameraUpdate mCameraUpdate = CameraUpdateFactory.newLatLngBounds(bounds, 2);
        mapConfig.getGoogleMap().animateCamera(mCameraUpdate);
        if (emission == 1) {
            marker = mapConfig.getGoogleMap().addMarker(new MarkerOptions().position(latLngs.get(0))
                    .flat(true)
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_pin_car3)));
        }
        marker.setPosition(latLngs.get(0));
        ValueAnimator valueAnimator = ValueAnimator.ofFloat(0, 1);
        valueAnimator.setDuration(1000);
        valueAnimator.setInterpolator(new LinearInterpolator());
        valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                v = valueAnimator.getAnimatedFraction();
                double lng = v * latLngs.get(1).longitude + (1 - v)
                        * latLngs.get(0).longitude;
                double lat = v * latLngs.get(1).latitude + (1 - v)
                        * latLngs.get(0).latitude;
                LatLng newPos = new LatLng(lat, lng);
                marker.setPosition(newPos);
                marker.setAnchor(0.5f, 0.5f);
                marker.setRotation(getBearing(latLngs.get(0), newPos));
                mapConfig.getGoogleMap().animateCamera(CameraUpdateFactory.newCameraPosition
                        (new CameraPosition.Builder().target(newPos)
                                .zoom(15.5f).build()));
            }
        });
        valueAnimator.start();
    }

    /**
     * Initialize the MQTT helper to connect to the broker and subscribe to the topic
     */

    /**
     * Converts the LatLng string as (28.612, 77.6545) to LatLng type.
     *
     * @param latLngPair String representing latitude and longitude pair of form (28.612, 77.6545).
     * @return The LatLng type of the string.
     */
    private LatLng convertStringToLatLng(String latLngPair) {
        String[] latLng = latLngPair.split(",");
        double latitude = Double.parseDouble(latLng[0].substring(1, latLng[0].length()));
        double longitude = Double.parseDouble(latLng[1].substring(0, latLng[1].length() - 1));
        return new LatLng(latitude, longitude);
    }



    private float getBearing(LatLng begin, LatLng end) {
        double lat = Math.abs(begin.latitude - end.latitude);
        double lng = Math.abs(begin.longitude - end.longitude);

        if (begin.latitude < end.latitude && begin.longitude < end.longitude)
            return (float) (Math.toDegrees(Math.atan(lng / lat)));
        else if (begin.latitude >= end.latitude && begin.longitude < end.longitude)
            return (float) ((90 - Math.toDegrees(Math.atan(lng / lat))) + 90);
        else if (begin.latitude >= end.latitude && begin.longitude >= end.longitude)
            return (float) (Math.toDegrees(Math.atan(lng / lat)) + 180);
        else if (begin.latitude < end.latitude && begin.longitude >= end.longitude)
            return (float) ((90 - Math.toDegrees(Math.atan(lng / lat))) + 270);
        return -1;
    }

}
