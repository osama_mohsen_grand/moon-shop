package grand.app.moonshop.utils.dialog;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;

import com.google.android.material.bottomsheet.BottomSheetDialog;

import java.util.ArrayList;
import java.util.List;

import androidx.appcompat.widget.SearchView;
import androidx.databinding.DataBindingUtil;
import grand.app.moonshop.R;
import grand.app.moonshop.adapter.FilterAdapter;
import grand.app.moonshop.databinding.CustomFilterBinding;
import grand.app.moonshop.models.base.IdNameDescription;
import grand.app.moonshop.utils.helper.AppUtils;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.utils.resources.ResourceManager;

public class BottomSheetDialogHelper {

    Activity activity;

    private static final String TAG = "BottomSheetDialogHelper";

    public BottomSheetDialogHelper(Activity activity) {
        this.activity = activity;
    }

    public BottomSheetDialog filterDialog = null;
    CustomFilterBinding customFilterBinding = null;
    FilterAdapter filterAdapter;
    List<IdNameDescription> AllResult,dataSearch;

    public void initRecyclerViewStaggered(){
//        customFilterBinding.rvFilterAds.setHasFixedSize(true);
//        customFilterBinding.rvFilterAds.setItemViewCacheSize(30);
//        customFilterBinding.rvFilterAds.setDrawingCacheEnabled(true);
//        StaggeredGridLayoutManager staggeredGridLayoutManager = new StaggeredGridLayoutManager(4, StaggeredGridLayoutManager.VERTICAL);
//        customFilterBinding.rvFilterAds.setLayoutManager(staggeredGridLayoutManager);
//        customFilterBinding.rvFilterAds.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        AppUtils.initHorizontalRV(customFilterBinding.rvFilterAds, customFilterBinding.rvFilterAds.getContext(), 1);
    }
    public void filterFamousShopAds(List<IdNameDescription> data,boolean allowMultipleSelected, DialogHelperSelectedInterface dialogHelperInterface) {

        if (filterDialog == null) {
            LayoutInflater layoutInflater = LayoutInflater.from(activity);
            customFilterBinding = DataBindingUtil.inflate(layoutInflater, R.layout.custom_filter, null, true);
//            setStyle(DialogFragment.STYLE_NORMAL, R.style.DialogStyle);

            customFilterBinding.search.setActivated(true);
            customFilterBinding.search.setQueryHint(ResourceManager.getString(R.string.search_hint));
            customFilterBinding.search.onActionViewExpanded();
            customFilterBinding.search.setIconified(false);
            customFilterBinding.search.clearFocus();

            customFilterBinding.search.setOnQueryTextListener(new SearchView.OnQueryTextListener(){

                @Override
                public boolean onQueryTextSubmit(String s) {
                    return false;
                }

                @Override
                public boolean onQueryTextChange(String s) {
                    if (AllResult != null) {
                        if (!s.equals("")) {
                            dataSearch = new ArrayList<>();
                            for (IdNameDescription item : AllResult) {
                                if (item.name.contains(s))
                                    dataSearch.add(item);
                            }
                        } else {
                            dataSearch = new ArrayList<>(AllResult);
                        }
                        filterAdapter.update(dataSearch);
                    }
                    return false;
                }
            });


            initRecyclerViewStaggered();
            View customView = customFilterBinding.getRoot();
            filterDialog = new BottomSheetDialog(activity);

            filterAdapter = new FilterAdapter(data,allowMultipleSelected);
//            filterAdapter.mMutableLiveData.observeForever(new Observer<Object>() {
//                @Override
//                public void onChanged(@Nullable Object o) {
//                    dialogHelperInterface.onClickListener(filterDialog, Constants.,o);
//                }
//            });
            customFilterBinding.tvReset.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    filterAdapter.reset();
                }
            });
            customFilterBinding.imgDialogClose.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    filterDialog.dismiss();
                }
            });

            customFilterBinding.tvFilterSubmit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(filterAdapter.selected.size() == 0)
                        Toast.makeText(activity, ""+activity.getString(R.string.please_select_at_least_one_filter), Toast.LENGTH_SHORT).show();
                    else{
                        filterDialog.dismiss();
                        dialogHelperInterface.onClickListener(filterDialog,Constants.ARRAY,filterAdapter.selected);
                    }
                }
            });
            filterDialog.setContentView(customView);


            customFilterBinding.rvFilterAds.setAdapter(filterAdapter);
        }
        filterDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        filterDialog.show();
    }


}
