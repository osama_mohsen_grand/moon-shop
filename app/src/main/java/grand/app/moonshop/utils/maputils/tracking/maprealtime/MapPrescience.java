package grand.app.moonshop.utils.maputils.tracking.maprealtime;

import android.location.Location;

import com.firebase.geofire.GeoFire;
import com.firebase.geofire.GeoLocation;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import grand.app.moonshop.utils.maputils.base.MapConfig;
import grand.app.moonshop.utils.storage.user.UserHelper;
import io.reactivex.annotations.NonNull;

public class MapPrescience {

    public static String driver_tbl = "Drivers";
    public static String user_driver_tbl = "DriversInformation";
    DatabaseReference onlineRef , currentUserRef;
    DatabaseReference drivers, userDrivers;
    GeoFire geoFire;
    MapConfig mapConfig;
    boolean removeConnection = false;

    public MapPrescience(MapConfig mapConfig) {
        this.mapConfig = mapConfig;
    }

    public void connect(){
        initDriverLocation();
        onlineRef = FirebaseDatabase.getInstance().getReference().child(".info/connected");
        currentUserRef = FirebaseDatabase.getInstance().getReference(driver_tbl)
                .child(String.valueOf(UserHelper.getUserId()));
        onlineRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                //remove value when disconnected
                if(removeConnection)
                    currentUserRef.onDisconnect().removeValue();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void initDriverLocation(){
        drivers = FirebaseDatabase.getInstance().getReference(driver_tbl);
        userDrivers = FirebaseDatabase.getInstance().getReference(user_driver_tbl);
        geoFire = new GeoFire(drivers);
    }

    public void updateDriverLocation(Location location, MapPrescienceInterface mapPrescienceInterface){
        geoFire.setLocation(String.valueOf(UserHelper.getUserId()),
                new GeoLocation(location.getLatitude(), location.getLongitude()), new GeoFire.CompletionListener() {
                    @Override
                    public void onComplete(String key, DatabaseError error) {
                        mapPrescienceInterface.update();
                    }
                });
    }

    public void setRemoveConnection(boolean removeConnection) {
        this.removeConnection = removeConnection;
    }

    public DatabaseReference getCurrentUserRef() {
        return currentUserRef;
    }
}
