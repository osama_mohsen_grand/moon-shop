package grand.app.moonshop.utils.tabLayout;

import android.os.Bundle;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import grand.app.moonshop.R;
import grand.app.moonshop.models.app.TabModel;
import grand.app.moonshop.models.famous.details.Service;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.utils.resources.ResourceManager;

public class SwapAdapter extends FragmentPagerAdapter
{
    public int id;
    private ArrayList<TabModel> tabs;
    public SwapAdapter(final FragmentManager fm, ArrayList<TabModel> tabs) {
        super(fm, FragmentPagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        this.tabs = tabs;
    }

    @NotNull
    @Override
    public Fragment getItem(int position) {
        if(position >= 0)
            return tabs.get(position).fragment;
        return null;
    }

    @Override
    public int getCount() {
        return tabs.size();
    }




    @Override
    public CharSequence getPageTitle(int position) {
        if(position >=  0)
            return tabs.get(position).name;
        return null;
    }


}