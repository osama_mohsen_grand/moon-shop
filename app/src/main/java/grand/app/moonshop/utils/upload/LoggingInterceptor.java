package grand.app.moonshop.utils.upload;

import java.io.IOException;

import afu.org.checkerframework.checker.nullness.qual.NonNull;
import grand.app.moonshop.BuildConfig;
import okhttp3.Interceptor;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;

public class LoggingInterceptor implements Interceptor {

    private final Interceptor mLoggingInterceptor;

    private LoggingInterceptor() {
        mLoggingInterceptor = new HttpLoggingInterceptor()
                //.setLogLevel(RestAdapter.LogLevel.FULL)
                .setLevel(BuildConfig.DEBUG ? HttpLoggingInterceptor.Level.BODY : HttpLoggingInterceptor.Level.NONE);
    }

    @NonNull
    public static Interceptor create() {
        return new LoggingInterceptor();
    }

    @Override
    public Response intercept(Interceptor.Chain chain) throws IOException {
        return mLoggingInterceptor.intercept(chain);
    }

}
