package grand.app.moonshop.utils.maputils.tracking;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.location.Location;
import android.os.Looper;
import android.util.Log;

import com.firebase.geofire.GeoFire;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;

import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.utils.maputils.base.MapConfig;
import grand.app.moonshop.utils.maputils.location.MapActivityHelper;
import grand.app.moonshop.utils.maputils.permission.MapPermissionActivity;
import grand.app.moonshop.utils.maputils.tracking.maprealtime.MapPrescience;
import grand.app.moonshop.utils.maputils.tracking.maprealtime.MapPrescienceInterface;
import grand.app.moonshop.utils.maputils.tracking.token.FireRealTime;
import grand.app.moonshop.utils.storage.user.UserHelper;
import io.reactivex.disposables.Disposable;

public class FireTracking implements
        GoogleMap.OnMyLocationChangeListener,
        LocationListener {
    private static final String TAG = "FireTracking";
    public com.google.android.gms.location.LocationCallback locationCallback;
    public Location driverLocation;
    Activity activity;
    MapPermissionActivity mapPermission;
    MapConfig mapConfig;
    Marker mCurrent;
    public String id = "";
    DatabaseReference onlineRef, currentUserRef;
    DatabaseReference drivers, userDrivers;
    GeoFire geoFire;
    private Disposable latLngDisposable;

    public FusedLocationProviderClient fusedLocationProviderClient;
    private MapActivityHelper mapActivityHelper;
    MapPrescience mapPrescience;
    boolean validUpdateFirebaseLocation = false;

    public FireTracking(Activity activity, MapConfig mapConfig, MapActivityHelper mapActivityHelper) {
        this.activity = activity;
        id = String.valueOf(UserHelper.getUserId());//TODO here change no to user_id
        this.mapConfig = mapConfig;
        this.mapPermission = new MapPermissionActivity(activity);
        this.mapActivityHelper = (mapActivityHelper != null ? mapActivityHelper : new MapActivityHelper(activity, mapConfig));
        new FireRealTime();//update fire-token in Database (real-time & grand)
        mapPrescience = new MapPrescience(mapConfig);
        init();
    }

    @SuppressLint("MissingPermission")
    private void init() {
        if (mapPermission.validLocationPermission()) {
            mapPrescience.connect();
            fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(activity);//install fused track location
            buildLocationCallBack();
            mapConfig.createLocationRequest();
            mapActivityHelper.getmGoogleApiClient();
            setUpLocation();
            fusedLocationProviderClient.requestLocationUpdates(mapConfig.getmLocationRequest(), locationCallback, Looper.myLooper());
        } else {
            Log.e(TAG, "init: invalid enable location permission");
            mapPermission.makeRequestPermission(Constants.LOCATION_REQUEST);
        }
    }

    public void startUpdateLocation(){
        if(mapPermission.validLocationPermission()) {
            validUpdateFirebaseLocation = true;
            displayLocation();
        }
    }

    private void buildLocationCallBack() {
        LocationServices.getFusedLocationProviderClient(activity);
        locationCallback = new com.google.android.gms.location.LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                driverLocation = locationResult.getLocations().get(locationResult.getLocations().size() - 1);
                displayLocation();
            }
        };
    }

    private void setUpLocation() {
        if (!mapPermission.validLocationPermission()) {
            mapPermission.makeRequestPermission(Constants.LOCATION_REQUEST);
        } else {
            displayLocation();
        }
    }

    @SuppressLint("MissingPermission")
    private void displayLocation() {
        Log.e(TAG, "displayLocation: location");
        if (mapPermission.validLocationPermission()) {
            Log.e(TAG, "displayLocation: valid");
            fusedLocationProviderClient.getLastLocation().addOnSuccessListener(new OnSuccessListener<Location>() {
                @Override
                public void onSuccess(Location location) {
                    Log.e(TAG, "displayLocation_onSuccess: " + location);
                    if (location != null) {
                        driverLocation = location;
                        LatLng latLng = new LatLng(driverLocation.getLatitude(), driverLocation.getLongitude());
                        mapConfig.saveLastLocation(latLng);//save last location
                        mapPrescience.updateDriverLocation(driverLocation, new MapPrescienceInterface() {
                            @Override
                            public void update() {

                            }
                        });

                    }
                }
            });
        }
    }


    @Override
    public void onLocationChanged(Location location) {
        this.driverLocation = location;
        displayLocation();
    }

    @Override
    public void onMyLocationChange(Location location) {
        this.driverLocation = location;
        displayLocation();
    }

    public void stopTracking(){
        if (fusedLocationProviderClient != null && locationCallback != null)
            fusedLocationProviderClient.removeLocationUpdates(locationCallback);
        if(mapPrescience != null) {
            mapPrescience.setRemoveConnection(true);
            mapPrescience.getCurrentUserRef().onDisconnect().removeValue();
        }
    }
}
