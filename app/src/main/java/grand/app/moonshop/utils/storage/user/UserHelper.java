package grand.app.moonshop.utils.storage.user;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.google.gson.Gson;

import grand.app.moonshop.models.accounttype.AcountTypeResponse;
import grand.app.moonshop.models.country.CountriesResponse;
import grand.app.moonshop.models.user.login.LoginResponse;
import grand.app.moonshop.models.user.login.Trip;
import grand.app.moonshop.models.user.profile.User;
import grand.app.moonshop.utils.Constants;
import grand.app.moonshop.utils.storage.SharedPreferenceHelper;
import grand.app.moonshop.vollyutils.MyApplication;


/**
 * Created by mohamedatef on 1/12/19.
 */

public class UserHelper {

    public static int getUserId() {
        return  SharedPreferenceHelper.getSharedPreferenceInstance().getInt("id", -1);
    }

    public static Boolean isLogin() {
        return  SharedPreferenceHelper.getSharedPreferenceInstance().getInt("id", -1) != -1;
    }

    public static void clearUserId() {
        SharedPreferences.Editor prefsEditor = SharedPreferenceHelper.getSharedPreferenceInstance().edit();
        prefsEditor.putInt("id", -1);
        prefsEditor.commit();
    }

    public static void saveUserId() {
        SharedPreferences.Editor prefsEditor = SharedPreferenceHelper.getSharedPreferenceInstance().edit();
        prefsEditor.putInt("id", 1);
        prefsEditor.commit();
    }


    public static void saveUserDetails(User userModel) {
        SharedPreferences.Editor prefsEditor = SharedPreferenceHelper.getSharedPreferenceInstance().edit();
        Gson gson = new Gson();
        String json = gson.toJson(userModel);
        prefsEditor.putString("userDetails", json);
        prefsEditor.putInt("id", userModel.id);
        Log.e("userDetails",json.toString());
        prefsEditor.commit();
    }

    public static void saveTripDetails(Trip trip) {
        SharedPreferences.Editor prefsEditor = SharedPreferenceHelper.getSharedPreferenceInstance().edit();
        Gson gson = new Gson();
        String json = gson.toJson(trip);
        prefsEditor.putString("tripDetails", json);
        Log.e("tripDetails",json.toString());
        prefsEditor.commit();
    }


    public static void saveCountries(CountriesResponse countriesResponse) {
        SharedPreferences.Editor prefsEditor = SharedPreferenceHelper.getSharedPreferenceInstance().edit();
        Gson gson = new Gson();
        String json = gson.toJson(countriesResponse);
        prefsEditor.putString("countriesResponse", json);
        Log.e("countriesResponse",json.toString());
        prefsEditor.commit();
    }


    public static void saveAccountTypes(AcountTypeResponse accountTypes) {
        SharedPreferences.Editor prefsEditor = SharedPreferenceHelper.getSharedPreferenceInstance().edit();
        Gson gson = new Gson();
        String json = gson.toJson(accountTypes);
        prefsEditor.putString("accountTypesResponse", json);
        prefsEditor.putBoolean("accountTypes", true);
        Log.e("accountTypes",json.toString());
        prefsEditor.commit();
    }


    public static AcountTypeResponse getAccountTypes() {
        Gson gson = new Gson();
        String json =  SharedPreferenceHelper.getSharedPreferenceInstance().getString("accountTypesResponse", "");
        AcountTypeResponse accountType = gson.fromJson(json, AcountTypeResponse.class);
        if (accountType == null) {
            return new AcountTypeResponse();
        }
        return accountType;
    }

    public static boolean isAccountTypes(){
        return SharedPreferenceHelper.getSharedPreferenceInstance().getBoolean("accountTypes", false);
    }



    public static CountriesResponse getCountries() {
        Gson gson = new Gson();
        String json =  SharedPreferenceHelper.getSharedPreferenceInstance().getString("countriesResponse", "");
        CountriesResponse countriesResponse = gson.fromJson(json, CountriesResponse.class);
        if (countriesResponse == null) {
            return new CountriesResponse();
        }
        return countriesResponse;
    }



    public static Trip getTripDetails() {
        Gson gson = new Gson();
        String json =  SharedPreferenceHelper.getSharedPreferenceInstance().getString("tripDetails", "");
        Trip trip = gson.fromJson(json, Trip.class);
        if (trip == null) {
            return new Trip();
        }
        return trip;
    }

    public static void clearUserDetails() {
        SharedPreferences.Editor prefsEditor = SharedPreferenceHelper.getSharedPreferenceInstance().edit();
        prefsEditor.putString("userDetails", null);
        prefsEditor.putInt("id", -1);
        prefsEditor.commit();
    }

    public static void clearTripDetails() {
        SharedPreferences.Editor prefsEditor = SharedPreferenceHelper.getSharedPreferenceInstance().edit();
        prefsEditor.putString("tripDetails", null);
        prefsEditor.commit();
    }


    public static void saveKey(String key,String value) {
        SharedPreferences.Editor prefsEditor = SharedPreferenceHelper.getSharedPreferenceInstance().edit();
        prefsEditor.putString(key, value);
        prefsEditor.commit();
    }

    public static String retrieveKey(String key) {
        SharedPreferences preferences = SharedPreferenceHelper.getSharedPreferenceInstance();
        if (preferences.getString(key, "").length() > 0) {
            return preferences.getString(key, "");
        } else {
            return "";
        }
    }

    public static void saveCurrency(String value) {
        SharedPreferences.Editor prefsEditor = SharedPreferenceHelper.getSharedPreferenceInstance().edit();
        prefsEditor.putString(Constants.CURRENCY, value);
        prefsEditor.commit();
    }

    public static String retrieveCurrency() {
        SharedPreferences preferences = SharedPreferenceHelper.getSharedPreferenceInstance();
        if (preferences.getString(Constants.CURRENCY, "").length() > 0) {
            return preferences.getString(Constants.CURRENCY, "");
        } else {
            return "";
        }
    }

    public static User getUserDetails() {
        Gson gson = new Gson();
        String json =  SharedPreferenceHelper.getSharedPreferenceInstance().getString("userDetails", "");
        User userItem = gson.fromJson(json, User.class);
        if (userItem == null) {
            return new User();
        }
        return userItem;
    }
}
