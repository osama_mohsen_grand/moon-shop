package grand.app.moonshop.utils.dialog;

import android.app.Dialog;
import android.view.View;

import grand.app.moonshop.models.reservation.doctor.schedule.AddScheduleRequest;

/**
 * Created by osama on 1/7/2018.
 */

public interface DialogScheduleInterface {
    void OnClickListenerContinue(Dialog dialog, AddScheduleRequest addScheduleRequest);
}
