package grand.app.moonshop.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.util.DisplayMetrics;

import java.util.Locale;

import grand.app.moonshop.utils.storage.user.UserHelper;
import grand.app.moonshop.vollyutils.MyApplication;
import timber.log.Timber;

/**
 * Created by mohamedatef on 1/12/19.
 */

public class LanguagesHelper {

//    public static void changeLanguage(String languageToLoad) {
//        Locale locale = new Locale(languageToLoad);
//        Locale.setDefault(locale);
//        Configuration config = new Configuration();
//        config.locale = locale;
//        MyApplication.getInstance().getResources().updateConfiguration(config,
//                MyApplication.getInstance().getResources().getDisplayMetrics());
//        boolean userSet = (UserHelper.retrieveKey(Constants.LANGUAGE_HAVE).equals("true"));
//        setLanguage(languageToLoad,userSet);
//    }
//
//
//    public static void setLanguage(String language , boolean userSet) {
//        Timber.e("language is Set "+userSet);
//        SharedPreferences userDetails = MyApplication.getInstance().getSharedPreferences(Constants.LANGUAGE_DATA, Context.MODE_PRIVATE);
//        SharedPreferences.Editor editor = userDetails.edit();
//        editor.putString(Constants.LANGUAGE, language);
//        if(userSet) {
//            editor.putBoolean(Constants.LANGUAGE_HAVE, true);
//            UserHelper.saveKey(Constants.LANGUAGE_HAVE, "true");
//        }
//        editor.commit();
//    }
//
//    public static String getCurrentLanguage() {
//        SharedPreferences preferences = MyApplication.getInstance().getApplicationContext().getSharedPreferences(Constants.LANGUAGE_DATA, Context.MODE_PRIVATE);
//        Timber.e("language_current:"+preferences.getString(Constants.LANGUAGE, ""));
//        if (preferences.getString(Constants.LANGUAGE, "").length() > 0) {
//            Timber.e("language_here_done");
//            return preferences.getString(Constants.LANGUAGE, "");
//        } else {
//            Timber.e("language_here_not_done");
//            setLanguage(Constants.DEFAULT_LANGUAGE , false);
//            return Constants.DEFAULT_LANGUAGE;
//        }
//    }

    public static void changeLanguage(Context context ,String languageToLoad) {
        Locale locale = new Locale(languageToLoad);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        DisplayMetrics dm = context.getResources().getDisplayMetrics();
        config.locale = locale;
        context.getResources().updateConfiguration(config,
                dm);

        MyApplication.getInstance().getResources().updateConfiguration(config,
                context.getResources().getDisplayMetrics());

        setLanguage(languageToLoad);
    }


    public static void setLanguage(String language) {
        Timber.e("languageSet:"+language);
        SharedPreferences userDetails = MyApplication.getInstance().getSharedPreferences(Constants.LANGUAGE_DATA, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = userDetails.edit();
        editor.putString(Constants.LANGUAGE, language);
        editor.putBoolean(Constants.LANGUAGE_HAVE, true);
        editor.apply();
    }

    public static String getCurrentLanguage() {
        SharedPreferences preferences = MyApplication.getInstance().getApplicationContext().getSharedPreferences(Constants.LANGUAGE_DATA, Context.MODE_PRIVATE);
        if (preferences.getString(Constants.LANGUAGE, "").length() > 0) {
            return preferences.getString(Constants.LANGUAGE, "");
        } else {
            setLanguage(Constants.DEFAULT_LANGUAGE);
            return Constants.DEFAULT_LANGUAGE;
        }
    }


}
